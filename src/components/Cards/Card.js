import React from "react";
import PropTypes from "prop-types";

export default function Card(props) {
    return (
        <>
        <div
            className={
            "relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded " +
            (props.color === "light" ? "bg-white" : "bg-blue-900 text-white")
            }
        >
            {props.children}
        </div>
        </>
    );
}

Card.defaultProps = {
  color: "light",
  children: null,
  
};

Card.propTypes = {
  color: PropTypes.oneOf(["light", "dark"]),
  children: PropTypes.node,

};