/*eslint-disable*/
import React, { useState } from "react";
import { Link } from "react-router-dom";
import { userInfo } from "../../utils/dummy/employeeInfo";

export default function Sidebar() {
  const [nik] = useState(localStorage.getItem('logged_user') || "960170");
  const [infoUser] = useState(userInfo[nik]);
  const [collapseShow, setCollapseShow] = React.useState("hidden");
  return (
    <>
      <nav className="md:left-0 md:block md:fixed md:top-0 md:bottom-0 md:overflow-y-auto md:flex-row md:flex-nowrap md:overflow-hidden shadow-xl bg-white flex flex-wrap items-center justify-between relative md:w-64 z-10 py-4 px-6">
        <div className="md:flex-col md:items-stretch md:min-h-full md:flex-nowrap px-0 flex flex-wrap items-center justify-between w-full mx-auto">
          {/* Toggler */}
          <button
            className="cursor-pointer text-black opacity-50 md:hidden px-3 py-1 text-xl leading-none bg-transparent rounded border border-solid border-transparent"
            type="button"
            onClick={() => setCollapseShow("bg-white m-2 py-3 px-6")}
          >
            <i className="fas fa-bars"></i>
          </button>
          {/* Brand */}
          <div className="md:flex md:flex-col items-center md:mb-5">
            <Link
              className="md:block text-center md:pb-2 mr-0 inline-block whitespace-nowrap text-sm px-4 font-bold"
              to="/"
            >
              <img
                alt="..."
                src={require("../../assets/img/telkom_logo.png")}
                className="md:h-20 h-12 align-middle border-none"
              />
            </Link>
          </div>

          {/* User */}
          <div className="md:flex md:flex-col items-center">
            <span className="md:w-20 md:h-20 w-12 h-12text-sm text-white bg-slate-200 inline-flex items-center justify-center rounded-full mb-4">
              <img
                alt="..."
                className="w-full rounded-full align-middle border-none shadow-lg"
                src={require("../../assets/img/team-1-800x800.jpg")}
              />
            </span>
            <label className="hidden md:block text-sm inline-flex items-center text-center">
              {infoUser.nama}
            </label>
            <label className="hidden md:block text-sm inline-flex items-center text-center">
              {nik}
            </label>
            <label className="hidden md:block text-sm inline-flex items-center text-center break-words">
              {infoUser.posisi}
            </label>
          </div>
          {/* Collapse */}
          <div
            className={
              "md:flex md:flex-col md:items-stretch md:opacity-100 md:relative md:mt-4 md:shadow-none shadow absolute top-0 left-0 right-0 z-40 overflow-y-auto overflow-x-hidden h-auto items-center flex-1 rounded " +
              collapseShow
            }
          >
            {/* Divider */}
            <hr className="my-6 md:min-w-full" />

            <ul className="md:flex-col md:min-w-full flex flex-col list-none">
              <li className="items-center">
                <Link
                  className={
                    "text-xs uppercase py-3 font-bold block " +
                    (window.location.href.indexOf("/admin/report") !== -1
                      ? "text-blue-500 hover:text-blue-600"
                      : "text-slate-700 hover:text-slate-500")
                  }
                  to="/admin/report"
                >
                  <i
                    className={
                      "fas fa-file mr-2 text-sm " +
                      (window.location.href.indexOf("/admin/report") !== -1
                        ? "opacity-75"
                        : "text-slate-300")
                    }
                  ></i>{" "}
                  CBA Report
                </Link>
              </li>

              <li className="items-center">
                <Link
                  className={
                    "text-xs uppercase py-3 font-bold block " +
                    (window.location.href.indexOf("/admin/playbook") !== -1
                      ? "text-blue-500 hover:text-blue-600"
                      : "text-slate-700 hover:text-slate-500")
                  }
                  to="/admin/playbook"
                >
                  <i
                    className={
                      "fas fa-book mr-2 text-sm " +
                      (window.location.href.indexOf("/admin/playbook") !== -1
                        ? "opacity-75"
                        : "text-slate-300")
                    }
                  ></i>{" "}
                  Playbook
                </Link>
              </li>

              <li className="items-center">
                <Link
                  className={
                    "text-xs uppercase py-3 font-bold block " +
                    (window.location.href.indexOf("/admin/faq") !== -1
                      ? "text-blue-500 hover:text-blue-600"
                      : "text-slate-700 hover:text-slate-500")
                  }
                  to="/admin/faq"
                >
                  <i
                    className={
                      "fas fa-comment mr-2 text-sm " +
                      (window.location.href.indexOf("/admin/faq") !== -1
                        ? "opacity-75"
                        : "text-slate-300")
                    }
                  ></i>{" "}
                  FAQ
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
}
