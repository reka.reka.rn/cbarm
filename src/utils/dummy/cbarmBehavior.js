export const cbaBehaviorInfo = {
    "720065": {
		"nama": "NIZAR",
		"band": "I",
		"posisi": "VP HC DEVELOPMENT",
		"overall" : 0.943065232,
		"feedback_overall": "Average",
		"behavior": [{
			"code": "Am 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 1.0012372655930972,
			"kriteria": 0.9066666666666665,
			"nilai_komparatif": 0.9436274509803922,
			"nilai_komparatif_unit": 0.9424613759471098,
			"summary_team": 0.8555555555555555
		}, {
			"code": "Am 2",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.9972096372228939,
			"kriteria": 0.9119999999999995,
			"nilai_komparatif": 0.8650097465886945,
			"nilai_komparatif_unit": 0.8674301915068131,
			"summary_team": 0.7888888888888889
		}, {
			"code": "Am 3",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 1.011603396839233,
			"kriteria": 0.8693333333333332,
			"nilai_komparatif": 0.9330265848670758,
			"nilai_komparatif_unit": 0.9223244878203538,
			"summary_team": 0.8111111111111111
		}, {
			"code": "Km 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 1.0061246339823984,
			"kriteria": 0.9359999999999996,
			"nilai_komparatif": 0.985280151946819,
			"nilai_komparatif_unit": 0.9792824056468296,
			"summary_team": 0.9222222222222222
		}, {
			"code": "Km 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9997924234722929,
			"kriteria": 0.925333333333333,
			"nilai_komparatif": 1.0086455331412107,
			"nilai_komparatif_unit": 1.0088549477482245,
			"summary_team": 0.9333333333333333
		}, {
			"code": "Km 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 1.0066955401805862,
			"kriteria": 0.9359999999999995,
			"nilai_komparatif": 0.9496676163342834,
			"nilai_komparatif_unit": 0.94335136933648,
			"summary_team": 0.8888888888888888
		}, {
			"code": "Ha 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 1.0069684772479182,
			"kriteria": 0.9146666666666662,
			"nilai_komparatif": 1.0204081632653066,
			"nilai_komparatif_unit": 1.0133466799816013,
			"summary_team": 0.9333333333333333
		}, {
			"code": "Ha 2",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 1.0043437824830403,
			"kriteria": 0.8959999999999997,
			"nilai_komparatif": 0.9176587301587305,
			"nilai_komparatif_unit": 0.9136898601492824,
			"summary_team": 0.8222222222222222
		}, {
			"code": "Ha 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 1.0024842520157955,
			"kriteria": 0.9333333333333329,
			"nilai_komparatif": 0.9642857142857147,
			"nilai_komparatif_unit": 0.9618961219058841,
			"summary_team": 0.9
		}, {
			"code": "Lo 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 1.0024342621560771,
			"kriteria": 0.9333333333333329,
			"nilai_komparatif": 0.9642857142857147,
			"nilai_komparatif_unit": 0.9619440901907015,
			"summary_team": 0.9
		}, {
			"code": "Lo 2",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 1.0116855339701765,
			"kriteria": 0.8746666666666663,
			"nilai_komparatif": 0.9273373983739841,
			"nilai_komparatif_unit": 0.9166261325639565,
			"summary_team": 0.811111111111111
		}, {
			"code": "Lo 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 1.0020373611538465,
			"kriteria": 0.9093333333333328,
			"nilai_komparatif": 0.9775171065493652,
			"nilai_komparatif_unit": 0.9755296004370074,
			"summary_team": 0.8888888888888888
		}, {
			"code": "Ad 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 1.0048454564520508,
			"kriteria": 0.9519999999999995,
			"nilai_komparatif": 0.9920634920634925,
			"nilai_komparatif_unit": 0.9872796714096819,
			"summary_team": 0.9444444444444444
		}, {
			"code": "Ad 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 1.0058925878922844,
			"kriteria": 0.9093333333333328,
			"nilai_komparatif": 1.0019550342130994,
			"nilai_komparatif_unit": 0.9960855127808073,
			"summary_team": 0.9111111111111111
		}, {
			"code": "Ad 3",
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"kriteria_bp": 1.0068856540842552,
			"kriteria": 0.9279999999999995,
			"nilai_komparatif": 0.8381226053639852,
			"nilai_komparatif_unit": 0.8323910485409021,
			"summary_team": 0.7777777777777778
		}, {
			"code": "Ko 1",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 1.0054056687625983,
			"kriteria": 0.8186666666666662,
			"nilai_komparatif": 0.882193268186754,
			"nilai_komparatif_unit": 0.877450063786205,
			"summary_team": 0.7222222222222222
		}, {
			"code": "Ko 2",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 1.00598319696728,
			"kriteria": 0.9226666666666663,
			"nilai_komparatif": 0.9031791907514455,
			"nilai_komparatif_unit": 0.8978074320468216,
			"summary_team": 0.8333333333333334
		}, {
			"code": "Ko 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 1.0013546025596747,
			"kriteria": 0.7946666666666666,
			"nilai_komparatif": 0.9787472035794182,
			"nilai_komparatif_unit": 0.9774231836329836,
			"summary_team": 0.7777777777777777
		}]
	},
    "730296": {
		"nama": "IMAM SUHADI",
		"band": "II",
		"posisi": "AVP REWARD MANAGEMENT",
		"overall" : 1.005878115,
		"feedback_overall": "Average",
		"behavior": [{
			"code": "Am 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9936896467565919,
			"kriteria": 0.9,
			"nilai_komparatif": 0.962962962962963,
			"nilai_komparatif_unit": 0.9690781886538509,
			"summary_team": 0.8666666666666667
		}, {
			"code": "Am 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9953211872139458,
			"kriteria": 0.9,
			"nilai_komparatif": 0.9481481481481481,
			"nilai_komparatif_unit": 0.9526052095828059,
			"summary_team": 0.8533333333333333
		}, {
			"code": "Am 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9951916610509839,
			"kriteria": 0.8833333333333333,
			"nilai_komparatif": 0.9962264150943396,
			"nilai_komparatif_unit": 1.0010397535307551,
			"summary_team": 0.88
		}, {
			"code": "Km 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9956833771185322,
			"kriteria": 0.9666666666666667,
			"nilai_komparatif": 0.9517241379310344,
			"nilai_komparatif_unit": 0.9558501827009365,
			"summary_team": 0.9199999999999999
		}, {
			"code": "Km 2",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9973470511172523,
			"kriteria": 0.9,
			"nilai_komparatif": 1.111111111111111,
			"nilai_komparatif_unit": 1.1140666730466766,
			"summary_team": 0.9999999999999999
		}, {
			"code": "Km 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9941274420678136,
			"kriteria": 0.8999999999999999,
			"nilai_komparatif": 0.962962962962963,
			"nilai_komparatif_unit": 0.9686514245698443,
			"summary_team": 0.8666666666666666
		}, {
			"code": "Ha 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9997400915902233,
			"kriteria": 0.9499999999999998,
			"nilai_komparatif": 1.0105263157894737,
			"nilai_komparatif_unit": 1.0107890283584542,
			"summary_team": 0.96
		}, {
			"code": "Ha 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9964047432431288,
			"kriteria": 0.9666666666666667,
			"nilai_komparatif": 1.006896551724138,
			"nilai_komparatif_unit": 1.0105296653314395,
			"summary_team": 0.9733333333333333
		}, {
			"code": "Ha 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9971302490492572,
			"kriteria": 0.9833333333333334,
			"nilai_komparatif": 0.9762711864406779,
			"nilai_komparatif_unit": 0.9790809047980763,
			"summary_team": 0.96
		}, {
			"code": "Lo 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9991557988742207,
			"kriteria": 0.85,
			"nilai_komparatif": 0.9725490196078431,
			"nilai_komparatif_unit": 0.9733707402825903,
			"summary_team": 0.8266666666666667
		}, {
			"code": "Lo 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9980343380795568,
			"kriteria": 0.8833333333333333,
			"nilai_komparatif": 0.9660377358490567,
			"nilai_komparatif_unit": 0.967940379394091,
			"summary_team": 0.8533333333333334
		}, {
			"code": "Lo 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.997673587510879,
			"kriteria": 0.8833333333333333,
			"nilai_komparatif": 0.981132075471698,
			"nilai_komparatif_unit": 0.9834199158459724,
			"summary_team": 0.8666666666666666
		}, {
			"code": "Ad 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9954020886166478,
			"kriteria": 0.9833333333333334,
			"nilai_komparatif": 0.9762711864406779,
			"nilai_komparatif_unit": 0.9807807293205936,
			"summary_team": 0.96
		}, {
			"code": "Ad 2",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9955568125575285,
			"kriteria": 0.8833333333333333,
			"nilai_komparatif": 1.0264150943396229,
			"nilai_komparatif_unit": 1.0309960028326473,
			"summary_team": 0.9066666666666667
		}, {
			"code": "Ad 3",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9986804941021062,
			"kriteria": 0.9333333333333335,
			"nilai_komparatif": 1.0714285714285712,
			"nilai_komparatif_unit": 1.0728441956722818,
			"summary_team": 0.9999999999999999
		}, {
			"code": "Ko 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9985561949221324,
			"kriteria": 0.8333333333333335,
			"nilai_komparatif": 1.0079999999999998,
			"nilai_komparatif_unit": 1.0094574598063595,
			"summary_team": 0.84
		}, {
			"code": "Ko 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9952832494058723,
			"kriteria": 0.9166666666666665,
			"nilai_komparatif": 0.9890909090909092,
			"nilai_komparatif_unit": 0.9937783135417384,
			"summary_team": 0.9066666666666666
		}, {
			"code": "Ko 3",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9928211680299203,
			"kriteria": 0.7833333333333333,
			"nilai_komparatif": 1.1234042553191488,
			"nilai_komparatif_unit": 1.1315272996730596,
			"summary_team": 0.8799999999999999
		}]
	},
    "820075": {
		"nama": "JULIUS ROMEDLY",
		"band": "II",
		"posisi": "AVP DIGITAL TALENT & LEADERSHIP",
		"overall" : 1.02140685,
		"feedback_overall": "Above Average",
		"behavior": [{
			"code": "Am 1",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9936896467565919,
			"kriteria": 0.9,
			"nilai_komparatif": 1.0555555555555556,
			"nilai_komparatif_unit": 1.0622587837167212,
			"summary_team": 0.95
		}, {
			"code": "Am 2",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9953211872139458,
			"kriteria": 0.9,
			"nilai_komparatif": 1.0555555555555556,
			"nilai_komparatif_unit": 1.0605175184808582,
			"summary_team": 0.95
		}, {
			"code": "Am 3",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9951916610509839,
			"kriteria": 0.8833333333333333,
			"nilai_komparatif": 1.0330188679245282,
			"nilai_komparatif_unit": 1.038009971700925,
			"summary_team": 0.9124999999999999
		}, {
			"code": "Km 1",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9956833771185322,
			"kriteria": 0.9666666666666667,
			"nilai_komparatif": 1.0344827586206897,
			"nilai_komparatif_unit": 1.0389675898923225,
			"summary_team": 1
		}, {
			"code": "Km 2",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.9973470511172523,
			"kriteria": 0.9,
			"nilai_komparatif": 0.9027777777777777,
			"nilai_komparatif_unit": 0.9051791718504247,
			"summary_team": 0.8124999999999999
		}, {
			"code": "Km 3",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9941274420678136,
			"kriteria": 0.8999999999999999,
			"nilai_komparatif": 1.027777777777778,
			"nilai_komparatif_unit": 1.0338491166081993,
			"summary_team": 0.9249999999999999
		}, {
			"code": "Ha 1",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9997400915902233,
			"kriteria": 0.9499999999999998,
			"nilai_komparatif": 1.0394736842105263,
			"nilai_komparatif_unit": 1.0397439223999723,
			"summary_team": 0.9874999999999999
		}, {
			"code": "Ha 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9964047432431288,
			"kriteria": 0.9666666666666667,
			"nilai_komparatif": 0.9956896551724138,
			"nilai_komparatif_unit": 0.9992823317275794,
			"summary_team": 0.9625
		}, {
			"code": "Ha 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9971302490492572,
			"kriteria": 0.9833333333333334,
			"nilai_komparatif": 1.0169491525423728,
			"nilai_komparatif_unit": 1.0198759424979962,
			"summary_team": 1
		}, {
			"code": "Lo 1",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9991557988742207,
			"kriteria": 0.85,
			"nilai_komparatif": 1.1470588235294117,
			"nilai_komparatif_unit": 1.1480279900510388,
			"summary_team": 0.975
		}, {
			"code": "Lo 2",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9980343380795568,
			"kriteria": 0.8833333333333333,
			"nilai_komparatif": 1.0754716981132075,
			"nilai_komparatif_unit": 1.0775898754973279,
			"summary_team": 0.95
		}, {
			"code": "Lo 3",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.997673587510879,
			"kriteria": 0.8833333333333333,
			"nilai_komparatif": 1.0613207547169812,
			"nilai_komparatif_unit": 1.0637955820449223,
			"summary_team": 0.9375
		}, {
			"code": "Ad 1",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9954020886166478,
			"kriteria": 0.9833333333333334,
			"nilai_komparatif": 1.0169491525423728,
			"nilai_komparatif_unit": 1.021646593042285,
			"summary_team": 1
		}, {
			"code": "Ad 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9955568125575285,
			"kriteria": 0.8833333333333333,
			"nilai_komparatif": 0.9905660377358491,
			"nilai_komparatif_unit": 0.9949869512631244,
			"summary_team": 0.875
		}, {
			"code": "Ad 3",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.9986804941021062,
			"kriteria": 0.9333333333333335,
			"nilai_komparatif": 0.9107142857142856,
			"nilai_komparatif_unit": 0.9119175663214396,
			"summary_team": 0.85
		}, {
			"code": "Ko 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9985561949221324,
			"kriteria": 0.8333333333333335,
			"nilai_komparatif": 0.9899999999999998,
			"nilai_komparatif_unit": 0.9914314337383888,
			"summary_team": 0.825
		}, {
			"code": "Ko 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9952832494058723,
			"kriteria": 0.9166666666666665,
			"nilai_komparatif": 1.0090909090909093,
			"nilai_komparatif_unit": 1.013873095617031,
			"summary_team": 0.9249999999999999
		}, {
			"code": "Ko 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9928211680299203,
			"kriteria": 0.7833333333333333,
			"nilai_komparatif": 0.9574468085106383,
			"nilai_komparatif_unit": 0.9643698576759033,
			"summary_team": 0.75
		}]
	},
    "660389": {
		"nama": "MUH ARIS SUBEKTI, ST.",
		"band": "III",
		"posisi": "SO COMPENSATION SYSTEM",
		"overall" : 0.816312301210028,
		"feedback_overall": "Down Below Average",
		"behavior": [{
			"code": "Am 1",
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"kriteria_bp": 0.9972570608441991,
			"kriteria": 0.7642857142857142,
			"nilai_komparatif": 0.7476635514018691,
			"nilai_komparatif_unit": 0.7497199877120511,
			"summary_team": 0.5714285714285714
		}, {
			"code": "Am 2",
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"kriteria_bp": 1.001574985736096,
			"kriteria": 0.7214285714285715,
			"nilai_komparatif": 0.8316831683168314,
			"nilai_komparatif_unit": 0.8303753390022969,
			"summary_team": 0.5999999999999999
		}, {
			"code": "Am 3",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 1.000358938109753,
			"kriteria": 0.7857142857142859,
			"nilai_komparatif": 1.0545454545454542,
			"nilai_komparatif_unit": 1.0541670738086175,
			"summary_team": 0.8285714285714285
		}, {
			"code": "Km 1",
			"feedback": "Down Below Average",
			"feedback_unit": "Down Below Average",
			"kriteria_bp": 0.994245987478868,
			"kriteria": 0.8500000000000002,
			"nilai_komparatif": 0.6386554621848738,
			"nilai_komparatif_unit": 0.6423515611104721,
			"summary_team": 0.5428571428571428
		}, {
			"code": "Km 2",
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"kriteria_bp": 0.9948284729176622,
			"kriteria": 0.7785714285714286,
			"nilai_komparatif": 0.7155963302752292,
			"nilai_komparatif_unit": 0.7193162939702633,
			"summary_team": 0.557142857142857
		}, {
			"code": "Km 3",
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"kriteria_bp": 0.994370302825613,
			"kriteria": 0.8428571428571429,
			"nilai_komparatif": 0.7966101694915252,
			"nilai_komparatif_unit": 0.8011202338081391,
			"summary_team": 0.6714285714285713
		}, {
			"code": "Ha 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9971556790311175,
			"kriteria": 0.8428571428571429,
			"nilai_komparatif": 1.0169491525423726,
			"nilai_komparatif_unit": 1.0198499330921802,
			"summary_team": 0.857142857142857
		}, {
			"code": "Ha 2",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.995950834808315,
			"kriteria": 0.7714285714285715,
			"nilai_komparatif": 0.8888888888888887,
			"nilai_komparatif_unit": 0.8925027800794686,
			"summary_team": 0.6857142857142856
		}, {
			"code": "Ha 3",
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"kriteria_bp": 0.99602033889229,
			"kriteria": 0.8642857142857145,
			"nilai_komparatif": 0.8264462809917352,
			"nilai_komparatif_unit": 0.8297483984221203,
			"summary_team": 0.7142857142857142
		}, {
			"code": "Lo 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9999071523533885,
			"kriteria": 0.7857142857142859,
			"nilai_komparatif": 0.945454545454545,
			"nilai_komparatif_unit": 0.9455423368352918,
			"summary_team": 0.7428571428571427
		}, {
			"code": "Lo 2",
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"kriteria_bp": 0.9968716486428508,
			"kriteria": 0.7071428571428573,
			"nilai_komparatif": 0.7272727272727268,
			"nilai_komparatif_unit": 0.7295550317464057,
			"summary_team": 0.5142857142857141
		}, {
			"code": "Lo 3",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 1.0000207563543277,
			"kriteria": 0.7714285714285715,
			"nilai_komparatif": 0.9074074074074073,
			"nilai_komparatif_unit": 0.9073885733286664,
			"summary_team": 0.7
		}, {
			"code": "Ad 1",
			"feedback": "Down Below Average",
			"feedback_unit": "Down Below Average",
			"kriteria_bp": 0.993761427780623,
			"kriteria": 0.8285714285714286,
			"nilai_komparatif": 0.586206896551724,
			"nilai_komparatif_unit": 0.5898869488836023,
			"summary_team": 0.48571428571428565
		}, {
			"code": "Ad 2",
			"feedback": "Down Below Average",
			"feedback_unit": "Down Below Average",
			"kriteria_bp": 0.9958177106063061,
			"kriteria": 0.7642857142857143,
			"nilai_komparatif": 0.6915887850467288,
			"nilai_komparatif_unit": 0.6944933572487412,
			"summary_team": 0.5285714285714285
		}, {
			"code": "Ad 3",
			"feedback": "Down Below Average",
			"feedback_unit": "Down Below Average",
			"kriteria_bp": 0.9922722638247236,
			"kriteria": 0.8214285714285715,
			"nilai_komparatif": 0.6956521739130433,
			"nilai_komparatif_unit": 0.7010698568068858,
			"summary_team": 0.5714285714285714
		}, {
			"code": "Ko 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 1.0008050217863884,
			"kriteria": 0.7000000000000002,
			"nilai_komparatif": 0.9591836734693875,
			"nilai_komparatif_unit": 0.9584121308237354,
			"summary_team": 0.6714285714285714
		}, {
			"code": "Ko 2",
			"feedback": "Down Below Average",
			"feedback_unit": "Down Below Average",
			"kriteria_bp": 0.9947526916295614,
			"kriteria": 0.7214285714285714,
			"nilai_komparatif": 0.6930693069306929,
			"nilai_komparatif_unit": 0.6967252391097695,
			"summary_team": 0.4999999999999999
		}, {
			"code": "Ko 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9987504912841111,
			"kriteria": 0.6142857142857144,
			"nilai_komparatif": 0.9302325581395344,
			"nilai_komparatif_unit": 0.9313963459917982,
			"summary_team": 0.5714285714285713
		}]
	},
    "730190": {
		"nama": "TRIANA WIJAYANTI",
		"band": "III",
		"posisi": "SO BENEFIT & FACILITY SYSTEM",
		"overall" : 1.02865358964825,
		"feedback_overall": "Above Average",
		"behavior": [{
			"code": "Am 1",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.9972570608441991,
			"kriteria": 0.7642857142857142,
			"nilai_komparatif": 0.9235843870258383,
			"nilai_komparatif_unit": 0.926124690703122,
			"summary_team": 0.7058823529411764
		}, {
			"code": "Am 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 1.001574985736096,
			"kriteria": 0.7214285714285715,
			"nilai_komparatif": 0.9784507862550959,
			"nilai_komparatif_unit": 0.9769121635321141,
			"summary_team": 0.7058823529411764
		}, {
			"code": "Am 3",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 1.000358938109753,
			"kriteria": 0.7857142857142859,
			"nilai_komparatif": 1.048128342245989,
			"nilai_komparatif_unit": 1.047752264028849,
			"summary_team": 0.8235294117647058
		}, {
			"code": "Km 1",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.994245987478868,
			"kriteria": 0.8500000000000002,
			"nilai_komparatif": 1.1211072664359858,
			"nilai_komparatif_unit": 1.1275954648595592,
			"summary_team": 0.9529411764705882
		}, {
			"code": "Km 2",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9948284729176622,
			"kriteria": 0.7785714285714286,
			"nilai_komparatif": 1.1181867242309769,
			"nilai_komparatif_unit": 1.1239995181797782,
			"summary_team": 0.8705882352941177
		}, {
			"code": "Km 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.994370302825613,
			"kriteria": 0.8428571428571429,
			"nilai_komparatif": 0.9491525423728814,
			"nilai_komparatif_unit": 0.9545262360267193,
			"summary_team": 0.8
		}, {
			"code": "Ha 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9971556790311175,
			"kriteria": 0.8428571428571429,
			"nilai_komparatif": 1.0049850448654036,
			"nilai_komparatif_unit": 1.0078516985852135,
			"summary_team": 0.8470588235294116
		}, {
			"code": "Ha 2",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.995950834808315,
			"kriteria": 0.7714285714285715,
			"nilai_komparatif": 1.0217864923747275,
			"nilai_komparatif_unit": 1.0259406957286048,
			"summary_team": 0.7882352941176469
		}, {
			"code": "Ha 3",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.99602033889229,
			"kriteria": 0.8642857142857145,
			"nilai_komparatif": 1.102576567817209,
			"nilai_komparatif_unit": 1.1069819809537464,
			"summary_team": 0.9529411764705882
		}, {
			"code": "Lo 1",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9999071523533885,
			"kriteria": 0.7857142857142859,
			"nilai_komparatif": 1.048128342245989,
			"nilai_komparatif_unit": 1.0482256675323371,
			"summary_team": 0.8235294117647058
		}, {
			"code": "Lo 2",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9968716486428508,
			"kriteria": 0.7071428571428573,
			"nilai_komparatif": 1.1146761734997024,
			"nilai_komparatif_unit": 1.1181742153237395,
			"summary_team": 0.7882352941176469
		}, {
			"code": "Lo 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 1.0000207563543277,
			"kriteria": 0.7714285714285715,
			"nilai_komparatif": 1.006535947712418,
			"nilai_komparatif_unit": 1.0065150561292768,
			"summary_team": 0.776470588235294
		}, {
			"code": "Ad 1",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.993761427780623,
			"kriteria": 0.8285714285714286,
			"nilai_komparatif": 1.022312373225152,
			"nilai_komparatif_unit": 1.0287301807520262,
			"summary_team": 0.8470588235294116
		}, {
			"code": "Ad 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9958177106063061,
			"kriteria": 0.7642857142857143,
			"nilai_komparatif": 0.9851566794942275,
			"nilai_komparatif_unit": 0.9892941941094946,
			"summary_team": 0.7529411764705882
		}, {
			"code": "Ad 3",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.9922722638247236,
			"kriteria": 0.8214285714285715,
			"nilai_komparatif": 0.9023017902813298,
			"nilai_komparatif_unit": 0.9093288436818725,
			"summary_team": 0.7411764705882353
		}, {
			"code": "Ko 1",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 1.0008050217863884,
			"kriteria": 0.7000000000000002,
			"nilai_komparatif": 1.0420168067226887,
			"nilai_komparatif_unit": 1.0411786352378,
			"summary_team": 0.7294117647058822
		}, {
			"code": "Ko 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9947526916295614,
			"kriteria": 0.7214285714285714,
			"nilai_komparatif": 0.9784507862550961,
			"nilai_komparatif_unit": 0.9836121022726159,
			"summary_team": 0.7058823529411764
		}, {
			"code": "Ko 3",
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9987504912841111,
			"kriteria": 0.6142857142857144,
			"nilai_komparatif": 1.0916552667578656,
			"nilai_komparatif_unit": 1.0930210060315517,
			"summary_team": 0.6705882352941176
		}]
	},
    "880044": {
		"nama": "ADITYA FERRY SANJAYA",
		"band": "III",
		"posisi": "SO BENEFIT & FACILITY SYSTEM",
		"overall" : 1.06484301582202,
		"feedback_overall": "Above Average",
		"behavior": [{
			"code": "Am 1",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9972570608441991,
			"kriteria": 0.7642857142857142,
			"nilai_komparatif": 1.2006597031335902,
			"nilai_komparatif_unit": 1.2039620979140588,
			"summary_team": 0.9176470588235295
		}, {
			"code": "Am 2",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 1.001574985736096,
			"kriteria": 0.7214285714285715,
			"nilai_komparatif": 1.141525917297612,
			"nilai_komparatif_unit": 1.1397308574541332,
			"summary_team": 0.8235294117647058
		}, {
			"code": "Am 3",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 1.000358938109753,
			"kriteria": 0.7857142857142859,
			"nilai_komparatif": 1.1229946524064167,
			"nilai_komparatif_unit": 1.122591711459481,
			"summary_team": 0.8823529411764706
		}, {
			"code": "Km 1",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.994245987478868,
			"kriteria": 0.8500000000000002,
			"nilai_komparatif": 1.1211072664359858,
			"nilai_komparatif_unit": 1.1275954648595592,
			"summary_team": 0.9529411764705882
		}, {
			"code": "Km 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9948284729176622,
			"kriteria": 0.7785714285714286,
			"nilai_komparatif": 0.9519697787371827,
			"nilai_komparatif_unit": 0.9569185087206217,
			"summary_team": 0.7411764705882351
		}, {
			"code": "Km 3",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.994370302825613,
			"kriteria": 0.8428571428571429,
			"nilai_komparatif": 1.102691924227318,
			"nilai_komparatif_unit": 1.108934891854571,
			"summary_team": 0.9294117647058824
		}, {
			"code": "Ha 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9971556790311175,
			"kriteria": 0.8428571428571429,
			"nilai_komparatif": 1.0049850448654036,
			"nilai_komparatif_unit": 1.0078516985852135,
			"summary_team": 0.8470588235294116
		}, {
			"code": "Ha 2",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.995950834808315,
			"kriteria": 0.7714285714285715,
			"nilai_komparatif": 1.0370370370370368,
			"nilai_komparatif_unit": 1.0412532434260466,
			"summary_team": 0.7999999999999998
		}, {
			"code": "Ha 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.99602033889229,
			"kriteria": 0.8642857142857145,
			"nilai_komparatif": 0.9800680602819638,
			"nilai_komparatif_unit": 0.983983983069997,
			"summary_team": 0.8470588235294119
		}, {
			"code": "Lo 1",
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9999071523533885,
			"kriteria": 0.7857142857142859,
			"nilai_komparatif": 1.0930481283422455,
			"nilai_komparatif_unit": 1.0931496247122943,
			"summary_team": 0.8588235294117645
		}, {
			"code": "Lo 2",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9968716486428508,
			"kriteria": 0.7071428571428573,
			"nilai_komparatif": 1.0314913844325604,
			"nilai_komparatif_unit": 1.0347283783592813,
			"summary_team": 0.7294117647058822
		}, {
			"code": "Lo 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 1.0000207563543277,
			"kriteria": 0.7714285714285715,
			"nilai_komparatif": 0.9912854030501088,
			"nilai_komparatif_unit": 0.991264828006106,
			"summary_team": 0.7647058823529411
		}, {
			"code": "Ad 1",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.993761427780623,
			"kriteria": 0.8285714285714286,
			"nilai_komparatif": 1.1217038539553752,
			"nilai_komparatif_unit": 1.1287456149918067,
			"summary_team": 0.9294117647058824
		}, {
			"code": "Ad 2",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9958177106063061,
			"kriteria": 0.7642857142857143,
			"nilai_komparatif": 1.062122045079714,
			"nilai_komparatif_unit": 1.0665828030242988,
			"summary_team": 0.8117647058823529
		}, {
			"code": "Ad 3",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9922722638247236,
			"kriteria": 0.8214285714285715,
			"nilai_komparatif": 1.1171355498721227,
			"nilai_komparatif_unit": 1.1258357112251756,
			"summary_team": 0.9176470588235295
		}, {
			"code": "Ko 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 1.0008050217863884,
			"kriteria": 0.7000000000000002,
			"nilai_komparatif": 0.9915966386554619,
			"nilai_komparatif_unit": 0.9907990238553259,
			"summary_team": 0.6941176470588235
		}, {
			"code": "Ko 2",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9947526916295614,
			"kriteria": 0.7214285714285714,
			"nilai_komparatif": 1.1741409435061154,
			"nilai_komparatif_unit": 1.1803345227271391,
			"summary_team": 0.8470588235294119
		}, {
			"code": "Ko 3",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.9987504912841111,
			"kriteria": 0.6142857142857144,
			"nilai_komparatif": 0.8618331053351569,
			"nilai_komparatif_unit": 0.8629113205512249,
			"summary_team": 0.5294117647058822
		}]
	},
    "950170": {
		"nama": "FITRIA PURNAMASARI",
		"band": "V",
		"posisi": "OFF 2 BENEFIT & FACILITY SYSTEM",
		"overall" : 0.94382670373483,
		"feedback_overall": "Average",
		"behavior": [{
			"code": "Am 1",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.993259239430207,
			"kriteria": 0.7642857142857142,
			"nilai_komparatif": 0.9235843870258383,
			"nilai_komparatif_unit": 0.9298522987368953,
			"summary_team": 0.7058823529411764
		}, {
			"code": "Am 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9873707210955264,
			"kriteria": 0.7214285714285715,
			"nilai_komparatif": 0.9295282469423409,
			"nilai_komparatif_unit": 0.9414176733040991,
			"summary_team": 0.6705882352941175
		}, {
			"code": "Am 3",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.9913246428508287,
			"kriteria": 0.7857142857142859,
			"nilai_komparatif": 0.8534759358288766,
			"nilai_komparatif_unit": 0.8609449406750044,
			"summary_team": 0.6705882352941175
		}, {
			"code": "Km 1",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.9908177470388672,
			"kriteria": 0.8500000000000002,
			"nilai_komparatif": 0.8719723183391002,
			"nilai_komparatif_unit": 0.8800531893428984,
			"summary_team": 0.7411764705882353
		}, {
			"code": "Km 2",
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"kriteria_bp": 0.9900113462666884,
			"kriteria": 0.7785714285714286,
			"nilai_komparatif": 0.8310847274689691,
			"nilai_komparatif_unit": 0.8394699016359477,
			"summary_team": 0.6470588235294117
		}, {
			"code": "Km 3",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.9923804208495381,
			"kriteria": 0.8428571428571429,
			"nilai_komparatif": 0.8793619142572283,
			"nilai_komparatif_unit": 0.8861137279436054,
			"summary_team": 0.7411764705882353
		}, {
			"code": "Ha 1",
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9925119977053664,
			"kriteria": 0.8428571428571429,
			"nilai_komparatif": 1.074775672981057,
			"nilai_komparatif_unit": 1.082884313203145,
			"summary_team": 0.9058823529411766
		}, {
			"code": "Ha 2",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.991224259450578,
			"kriteria": 0.7714285714285715,
			"nilai_komparatif": 0.8692810457516338,
			"nilai_komparatif_unit": 0.87697716986211,
			"summary_team": 0.6705882352941175
		}, {
			"code": "Ha 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9916103830635437,
			"kriteria": 0.8642857142857145,
			"nilai_komparatif": 0.9528439474963536,
			"nilai_komparatif_unit": 0.960905577201176,
			"summary_team": 0.8235294117647058
		}, {
			"code": "Lo 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.993028029372769,
			"kriteria": 0.7857142857142859,
			"nilai_komparatif": 0.9433155080213902,
			"nilai_komparatif_unit": 0.9499384509994355,
			"summary_team": 0.7411764705882353
		}, {
			"code": "Lo 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9907716388036664,
			"kriteria": 0.7071428571428573,
			"nilai_komparatif": 0.9483065953654185,
			"nilai_komparatif_unit": 0.9571394236824105,
			"summary_team": 0.6705882352941175
		}, {
			"code": "Lo 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9913155015095249,
			"kriteria": 0.7714285714285715,
			"nilai_komparatif": 0.9607843137254899,
			"nilai_komparatif_unit": 0.9692013413110724,
			"summary_team": 0.7411764705882351
		}, {
			"code": "Ad 1",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.992055385656031,
			"kriteria": 0.8285714285714286,
			"nilai_komparatif": 1.0365111561866125,
			"nilai_komparatif_unit": 1.0448117828634977,
			"summary_team": 0.8588235294117648
		}, {
			"code": "Ad 2",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9903260027970757,
			"kriteria": 0.7642857142857143,
			"nilai_komparatif": 1.0313358988455192,
			"nilai_komparatif_unit": 1.0414105011204544,
			"summary_team": 0.7882352941176469
		}, {
			"code": "Ad 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9922224896764225,
			"kriteria": 0.8214285714285715,
			"nilai_komparatif": 0.9595907928388744,
			"nilai_komparatif_unit": 0.9671125204507411,
			"summary_team": 0.7882352941176469
		}, {
			"code": "Ko 1",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.9983462988993106,
			"kriteria": 0.7000000000000002,
			"nilai_komparatif": 0.8739495798319327,
			"nilai_komparatif_unit": 0.8753972251867644,
			"summary_team": 0.611764705882353
		}, {
			"code": "Ko 2",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.9911621117071272,
			"kriteria": 0.7214285714285714,
			"nilai_komparatif": 0.9132207338380895,
			"nilai_komparatif_unit": 0.9213636427901836,
			"summary_team": 0.6588235294117646
		}, {
			"code": "Ko 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9920399863830217,
			"kriteria": 0.6142857142857144,
			"nilai_komparatif": 0.9958960328317368,
			"nilai_komparatif_unit": 1.0038869869175076,
			"summary_team": 0.6117647058823528
		}]
	},
    "960170": {
		"nama": "MARTULAN SURYANTO",
		"band": "VI",
		"posisi": "OFF 3 COMPENSATION SYSTEM",
		"overall" : 1.11097262041142,
		"feedback_overall": "Beyond Average",
		"behavior": [{
			"code": "Am 1",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9912961273666684,
			"kriteria": 0.7642857142857142,
			"nilai_komparatif": 1.2149532710280373,
			"nilai_komparatif_unit": 1.2256209194073053,
			"summary_team": 0.9285714285714284
		}, {
			"code": "Am 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9813510146700265,
			"kriteria": 0.7214285714285715,
			"nilai_komparatif": 0.9306930693069303,
			"nilai_komparatif_unit": 0.9483793824983922,
			"summary_team": 0.6714285714285713
		}, {
			"code": "Am 3",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.988924971294764,
			"kriteria": 0.7857142857142859,
			"nilai_komparatif": 0.9090909090909087,
			"nilai_komparatif_unit": 0.919271871455191,
			"summary_team": 0.7142857142857142
		}, {
			"code": "Km 1",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 1.000768464317509,
			"kriteria": 0.8500000000000002,
			"nilai_komparatif": 1.1764705882352935,
			"nilai_komparatif_unit": 1.1755672067840461,
			"summary_team": 0.9999999999999998
		}, {
			"code": "Km 2",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9915032087806006,
			"kriteria": 0.7785714285714286,
			"nilai_komparatif": 1.2293577981651371,
			"nilai_komparatif_unit": 1.2398929093503004,
			"summary_team": 0.9571428571428569
		}, {
			"code": "Km 3",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.994567188739857,
			"kriteria": 0.8428571428571429,
			"nilai_komparatif": 1.1864406779661014,
			"nilai_komparatif_unit": 1.192921595844473,
			"summary_team": 0.9999999999999998
		}, {
			"code": "Ha 1",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9923593764615964,
			"kriteria": 0.8428571428571429,
			"nilai_komparatif": 1.0169491525423728,
			"nilai_komparatif_unit": 1.0247791038851821,
			"summary_team": 0.8571428571428571
		}, {
			"code": "Ha 2",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9951243068557212,
			"kriteria": 0.7714285714285715,
			"nilai_komparatif": 1.185185185185185,
			"nilai_komparatif_unit": 1.1909920971883363,
			"summary_team": 0.9142857142857141
		}, {
			"code": "Ha 3",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9908359670352697,
			"kriteria": 0.8642857142857145,
			"nilai_komparatif": 1.057851239669421,
			"nilai_komparatif_unit": 1.0676350827621561,
			"summary_team": 0.9142857142857141
		}, {
			"code": "Lo 1",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.9934387077554033,
			"kriteria": 0.7857142857142859,
			"nilai_komparatif": 0.8909090909090904,
			"nilai_komparatif_unit": 0.8967932132642883,
			"summary_team": 0.6999999999999998
		}, {
			"code": "Lo 2",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9947855432689745,
			"kriteria": 0.7071428571428573,
			"nilai_komparatif": 1.0707070707070703,
			"nilai_komparatif_unit": 1.0763194921275285,
			"summary_team": 0.757142857142857
		}, {
			"code": "Lo 3",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9945153359842274,
			"kriteria": 0.7714285714285715,
			"nilai_komparatif": 1.1296296296296295,
			"nilai_komparatif_unit": 1.1358594370107784,
			"summary_team": 0.8714285714285713
		}, {
			"code": "Ad 1",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 1.0021725511249344,
			"kriteria": 0.8285714285714286,
			"nilai_komparatif": 1.155172413793103,
			"nilai_komparatif_unit": 1.1526681832349397,
			"summary_team": 0.9571428571428569
		}, {
			"code": "Ad 2",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9967445130116761,
			"kriteria": 0.7642857142857143,
			"nilai_komparatif": 1.0654205607476632,
			"nilai_komparatif_unit": 1.0689003519352032,
			"summary_team": 0.8142857142857141
		}, {
			"code": "Ad 3",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 1.0030148889865356,
			"kriteria": 0.8214285714285715,
			"nilai_komparatif": 1.2173913043478257,
			"nilai_komparatif_unit": 1.2137320369968785,
			"summary_team": 0.9999999999999998
		}, {
			"code": "Ko 1",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9893984023694355,
			"kriteria": 0.7000000000000002,
			"nilai_komparatif": 1.061224489795918,
			"nilai_komparatif_unit": 1.0725957180186176,
			"summary_team": 0.7428571428571428
		}, {
			"code": "Ko 2",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9870666526172562,
			"kriteria": 0.7214285714285714,
			"nilai_komparatif": 1.1881188118811878,
			"nilai_komparatif_unit": 1.2036865076242134,
			"summary_team": 0.857142857142857
		}, {
			"code": "Ko 3",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9950955740072351,
			"kriteria": 0.6142857142857144,
			"nilai_komparatif": 1.1860465116279066,
			"nilai_komparatif_unit": 1.1918920580178192,
			"summary_team": 0.7285714285714285
		}]
	},
    "720605": {
		"nama": "WIDIA DIAN WULANDARI",
		"band": "III",
		"posisi": "SO LEADERSHIP & CAPABILITY SUPPORT",
		"overall" : 0.956824305016358,
		"feedback_overall": "Average",
		"behavior": [{
			"code": "Am 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9972570608441991,
			"kriteria": 0.7130434782608696,
			"nilai_komparatif": 1.0064562410329987,
			"nilai_komparatif_unit": 1.0092244823827192,
			"summary_team": 0.7176470588235294
		}, {
			"code": "Am 2",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 1.001574985736096,
			"kriteria": 0.6347826086956523,
			"nilai_komparatif": 0.9081385979049151,
			"nilai_komparatif_unit": 0.9067105417349147,
			"summary_team": 0.5764705882352941
		}, {
			"code": "Am 3",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 1.000358938109753,
			"kriteria": 0.7304347826086957,
			"nilai_komparatif": 1.0791316526610641,
			"nilai_komparatif_unit": 1.0787444501672145,
			"summary_team": 0.7882352941176469
		}, {
			"code": "Km 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.994245987478868,
			"kriteria": 0.8695652173913045,
			"nilai_komparatif": 0.9741176470588232,
			"nilai_komparatif_unit": 0.9797551705779726,
			"summary_team": 0.8470588235294116
		}, {
			"code": "Km 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9948284729176622,
			"kriteria": 0.7391304347826086,
			"nilai_komparatif": 1.0027681660899654,
			"nilai_komparatif_unit": 1.0079809669590754,
			"summary_team": 0.7411764705882352
		}, {
			"code": "Km 3",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.994370302825613,
			"kriteria": 0.7999999999999999,
			"nilai_komparatif": 1.0147058823529411,
			"nilai_komparatif_unit": 1.0204507108363376,
			"summary_team": 0.8117647058823528
		}, {
			"code": "Ha 1",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9971556790311175,
			"kriteria": 0.8695652173913045,
			"nilai_komparatif": 1.0282352941176467,
			"nilai_komparatif_unit": 1.0311682676437521,
			"summary_team": 0.8941176470588235
		}, {
			"code": "Ha 2",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.995950834808315,
			"kriteria": 0.7130434782608696,
			"nilai_komparatif": 1.0229555236728836,
			"nilai_komparatif_unit": 1.0271144798726595,
			"summary_team": 0.7294117647058822
		}, {
			"code": "Ha 3",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.99602033889229,
			"kriteria": 0.8086956521739133,
			"nilai_komparatif": 0.8728652751423148,
			"nilai_komparatif_unit": 0.8763528625460195,
			"summary_team": 0.7058823529411765
		}, {
			"code": "Lo 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9999071523533885,
			"kriteria": 0.773913043478261,
			"nilai_komparatif": 0.9424983476536679,
			"nilai_komparatif_unit": 0.9425858645329191,
			"summary_team": 0.7294117647058822
		}, {
			"code": "Lo 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9968716486428508,
			"kriteria": 0.7652173913043481,
			"nilai_komparatif": 0.9378342245989301,
			"nilai_komparatif_unit": 0.9407773065626907,
			"summary_team": 0.7176470588235293
		}, {
			"code": "Lo 3",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 1.0000207563543277,
			"kriteria": 0.7217391304347827,
			"nilai_komparatif": 0.9128277817150956,
			"nilai_komparatif_unit": 0.9128088351314801,
			"summary_team": 0.6588235294117647
		}, {
			"code": "Ad 1",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.993761427780623,
			"kriteria": 0.8869565217391306,
			"nilai_komparatif": 0.9152249134948095,
			"nilai_komparatif_unit": 0.920970454185156,
			"summary_team": 0.8117647058823529
		}, {
			"code": "Ad 2",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.9958177106063061,
			"kriteria": 0.8000000000000003,
			"nilai_komparatif": 0.9117647058823526,
			"nilai_komparatif_unit": 0.915593984894306,
			"summary_team": 0.7294117647058823
		}, {
			"code": "Ad 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9922722638247236,
			"kriteria": 0.8434782608695653,
			"nilai_komparatif": 0.9345057610673132,
			"nilai_komparatif_unit": 0.9417836163889649,
			"summary_team": 0.7882352941176469
		}, {
			"code": "Ko 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 1.0008050217863884,
			"kriteria": 0.6173913043478262,
			"nilai_komparatif": 0.9337199668599832,
			"nilai_komparatif_unit": 0.932968906564176,
			"summary_team": 0.5764705882352941
		}, {
			"code": "Ko 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9947526916295614,
			"kriteria": 0.7478260869565219,
			"nilai_komparatif": 0.94391244870041,
			"nilai_komparatif_unit": 0.9488915754066802,
			"summary_team": 0.7058823529411763
		}, {
			"code": "Ko 3",
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"kriteria_bp": 0.9987504912841111,
			"kriteria": 0.5826086956521739,
			"nilai_komparatif": 0.8279192273924495,
			"nilai_komparatif_unit": 0.8289550139074067,
			"summary_team": 0.48235294117647054
		}]
	},
    "790041": {
		"nama": "ORYZA JANU HAPSARI",
		"band": "IV",
		"posisi": "OFF 1 LEADERSHIP MANAGEMENT",
		"overall" : 1.02225389166985,
		"feedback_overall": "Above Average",
		"behavior": [{
			"code": "Am 1",
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.982910728894198,
			"kriteria": 0.7130434782608696,
			"nilai_komparatif": 1.0818815331010452,
			"nilai_komparatif_unit": 1.1006915493924785,
			"summary_team": 0.7714285714285714
		}, {
			"code": "Am 2",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.9788411297031282,
			"kriteria": 0.6347826086956523,
			"nilai_komparatif": 0.8776908023483363,
			"nilai_komparatif_unit": 0.8966631823230908,
			"summary_team": 0.557142857142857
		}, {
			"code": "Am 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9857413816507626,
			"kriteria": 0.7304347826086957,
			"nilai_komparatif": 0.9387755102040815,
			"nilai_komparatif_unit": 0.9523547734518052,
			"summary_team": 0.6857142857142856
		}, {
			"code": "Km 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9715711472906389,
			"kriteria": 0.8695652173913045,
			"nilai_komparatif": 0.9528571428571426,
			"nilai_komparatif_unit": 0.9807384106808,
			"summary_team": 0.8285714285714285
		}, {
			"code": "Km 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.977592392036756,
			"kriteria": 0.7391304347826086,
			"nilai_komparatif": 0.9470588235294116,
			"nilai_komparatif_unit": 0.9687665649241302,
			"summary_team": 0.6999999999999998
		}, {
			"code": "Km 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9793561052549872,
			"kriteria": 0.7999999999999999,
			"nilai_komparatif": 0.9642857142857143,
			"nilai_komparatif_unit": 0.9846119395300557,
			"summary_team": 0.7714285714285714
		}, {
			"code": "Ha 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9850474420929819,
			"kriteria": 0.8695652173913045,
			"nilai_komparatif": 0.969285714285714,
			"nilai_komparatif_unit": 0.9839990165612956,
			"summary_team": 0.8428571428571427
		}, {
			"code": "Ha 2",
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9849118950768658,
			"kriteria": 0.7130434782608696,
			"nilai_komparatif": 1.0818815331010452,
			"nilai_komparatif_unit": 1.0984551394991646,
			"summary_team": 0.7714285714285714
		}, {
			"code": "Ha 3",
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9813848139973261,
			"kriteria": 0.8086956521739133,
			"nilai_komparatif": 1.0775729646697383,
			"nilai_komparatif_unit": 1.0980126748452765,
			"summary_team": 0.8714285714285712
		}, {
			"code": "Lo 1",
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9867698884449624,
			"kriteria": 0.773913043478261,
			"nilai_komparatif": 1.0706260032102723,
			"nilai_komparatif_unit": 1.0849804151375735,
			"summary_team": 0.8285714285714283
		}, {
			"code": "Lo 2",
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9777970829810686,
			"kriteria": 0.7652173913043481,
			"nilai_komparatif": 1.064123376623376,
			"nilai_komparatif_unit": 1.088286511736279,
			"summary_team": 0.8142857142857141
		}, {
			"code": "Lo 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9822047685624858,
			"kriteria": 0.7217391304347827,
			"nilai_komparatif": 0.9500860585197932,
			"nilai_komparatif_unit": 0.9672993747630647,
			"summary_team": 0.6857142857142856
		}, {
			"code": "Ad 1",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9769836249165459,
			"kriteria": 0.8869565217391306,
			"nilai_komparatif": 0.99859943977591,
			"nilai_komparatif_unit": 1.0221250533868576,
			"summary_team": 0.8857142857142857
		}, {
			"code": "Ad 2",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9785796644925541,
			"kriteria": 0.8000000000000003,
			"nilai_komparatif": 1.0535714285714282,
			"nilai_komparatif_unit": 1.0766332745303484,
			"summary_team": 0.8428571428571427
		}, {
			"code": "Ad 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9755500172855235,
			"kriteria": 0.8434782608695653,
			"nilai_komparatif": 0.9484536082474223,
			"nilai_komparatif_unit": 0.9722244799774622,
			"summary_team": 0.7999999999999998
		}, {
			"code": "Ko 1",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.9807100667221678,
			"kriteria": 0.6173913043478262,
			"nilai_komparatif": 0.9024144869215289,
			"nilai_komparatif_unit": 0.9201643967392661,
			"summary_team": 0.557142857142857
		}, {
			"code": "Ko 2",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9780590430332289,
			"kriteria": 0.7478260869565219,
			"nilai_komparatif": 1.0315614617940194,
			"nilai_komparatif_unit": 1.0547026471887269,
			"summary_team": 0.7714285714285712
		}, {
			"code": "Ko 3",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9809291014918489,
			"kriteria": 0.5826086956521739,
			"nilai_komparatif": 1.1279317697228144,
			"nilai_komparatif_unit": 1.1498606453895557,
			"summary_team": 0.657142857142857
		}]
	},
    "850144": {
		"nama": "NADYA DIAN PRATIWI",
		"band": "IV",
		"posisi": "OFF 1 LEADERSHIP & CAPABILITY SUPPORT",
		"overall" : 0.999432321808807,
		"feedback_overall": "Average",
		"behavior": [{
			"code": "Am 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.982910728894198,
			"kriteria": 0.7130434782608696,
			"nilai_komparatif": 0.9899569583931133,
			"nilai_komparatif_unit": 1.0071687380061896,
			"summary_team": 0.7058823529411764
		}, {
			"code": "Am 2",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9788411297031282,
			"kriteria": 0.6347826086956523,
			"nilai_komparatif": 1.0193392425463332,
			"nilai_komparatif_unit": 1.04137352999816,
			"summary_team": 0.6470588235294117
		}, {
			"code": "Am 3",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.9857413816507626,
			"kriteria": 0.7304347826086957,
			"nilai_komparatif": 0.8697478991596638,
			"nilai_komparatif_unit": 0.8823286871685843,
			"summary_team": 0.6352941176470588
		}, {
			"code": "Km 1",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9715711472906389,
			"kriteria": 0.8695652173913045,
			"nilai_komparatif": 1.0417647058823525,
			"nilai_komparatif_unit": 1.072247471312274,
			"summary_team": 0.9058823529411764
		}, {
			"code": "Km 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.977592392036756,
			"kriteria": 0.7391304347826086,
			"nilai_komparatif": 0.9391003460207611,
			"nilai_komparatif_unit": 0.960625669420566,
			"summary_team": 0.6941176470588234
		}, {
			"code": "Km 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9793561052549872,
			"kriteria": 0.7999999999999999,
			"nilai_komparatif": 0.9558823529411765,
			"nilai_komparatif_unit": 0.9760314433250008,
			"summary_team": 0.7647058823529411
		}, {
			"code": "Ha 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9850474420929819,
			"kriteria": 0.8695652173913045,
			"nilai_komparatif": 0.9335294117647055,
			"nilai_komparatif_unit": 0.9476999501477682,
			"summary_team": 0.8117647058823528
		}, {
			"code": "Ha 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9849118950768658,
			"kriteria": 0.7130434782608696,
			"nilai_komparatif": 0.9404591104734576,
			"nilai_komparatif_unit": 0.9548662323750908,
			"summary_team": 0.6705882352941176
		}, {
			"code": "Ha 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9813848139973261,
			"kriteria": 0.8086956521739133,
			"nilai_komparatif": 0.9892473118279568,
			"nilai_komparatif_unit": 1.008011635923533,
			"summary_team": 0.8
		}, {
			"code": "Lo 1",
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"kriteria_bp": 0.9867698884449624,
			"kriteria": 0.773913043478261,
			"nilai_komparatif": 0.912095175148711,
			"nilai_komparatif_unit": 0.9243240859184199,
			"summary_team": 0.7058823529411764
		}, {
			"code": "Lo 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9777970829810686,
			"kriteria": 0.7652173913043481,
			"nilai_komparatif": 0.9224598930481278,
			"nilai_komparatif_unit": 0.9434062640438331,
			"summary_team": 0.7058823529411763
		}, {
			"code": "Lo 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9822047685624858,
			"kriteria": 0.7217391304347827,
			"nilai_komparatif": 0.9943302622253719,
			"nilai_komparatif_unit": 1.0123451789799722,
			"summary_team": 0.7176470588235293
		}, {
			"code": "Ad 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9769836249165459,
			"kriteria": 0.8869565217391306,
			"nilai_komparatif": 0.9948096885813146,
			"nilai_komparatif_unit": 1.0182460209261863,
			"summary_team": 0.8823529411764706
		}, {
			"code": "Ad 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9785796644925541,
			"kriteria": 0.8000000000000003,
			"nilai_komparatif": 0.9852941176470583,
			"nilai_komparatif_unit": 1.0068614272277834,
			"summary_team": 0.7882352941176469
		}, {
			"code": "Ad 3",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9755500172855235,
			"kriteria": 0.8434782608695653,
			"nilai_komparatif": 1.004244996967859,
			"nilai_komparatif_unit": 1.0294141552702543,
			"summary_team": 0.8470588235294118
		}, {
			"code": "Ko 1",
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9807100667221678,
			"kriteria": 0.6173913043478262,
			"nilai_komparatif": 1.0861640430820212,
			"nilai_komparatif_unit": 1.1075281879305194,
			"summary_team": 0.6705882352941176
		}, {
			"code": "Ko 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9780590430332289,
			"kriteria": 0.7478260869565219,
			"nilai_komparatif": 0.94391244870041,
			"nilai_komparatif_unit": 0.9650873895844561,
			"summary_team": 0.7058823529411763
		}, {
			"code": "Ko 3",
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"kriteria_bp": 0.9809291014918489,
			"kriteria": 0.5826086956521739,
			"nilai_komparatif": 1.1106233538191395,
			"nilai_komparatif_unit": 1.1322157249999463,
			"summary_team": 0.6470588235294117
		}]
	},
    "950282": {
		"nama": "FARISYA SAKINA",
		"band": "V",
		"posisi": "OFF 2 LEADERSHIP & CAPABILITY REVIEW",
		"overall" : 0.995358738067954,
		"feedback_overall": "Above Average",
		"behavior": [{
			"code": "Am 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.993259239430207,
			"kriteria": 0.7130434782608696,
			"nilai_komparatif": 0.9404591104734576,
			"nilai_komparatif_unit": 0.9468415426097233,
			"summary_team": 0.6705882352941176
		}, {
			"code": "Am 2",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9873707210955264,
			"kriteria": 0.6347826086956523,
			"nilai_komparatif": 1.037872683319903,
			"nilai_komparatif_unit": 1.051147923617122,
			"summary_team": 0.6588235294117646
		}, {
			"code": "Am 3",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9913246428508287,
			"kriteria": 0.7304347826086957,
			"nilai_komparatif": 1.0469187675070026,
			"nilai_komparatif_unit": 1.0560806442744102,
			"summary_team": 0.7647058823529411
		}, {
			"code": "Km 1",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9908177470388672,
			"kriteria": 0.8695652173913045,
			"nilai_komparatif": 0.9741176470588232,
			"nilai_komparatif_unit": 0.9831451343802092,
			"summary_team": 0.8470588235294116
		}, {
			"code": "Km 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9900113462666884,
			"kriteria": 0.7391304347826086,
			"nilai_komparatif": 0.9550173010380624,
			"nilai_komparatif_unit": 0.9646528846759304,
			"summary_team": 0.7058823529411765
		}, {
			"code": "Km 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9923804208495381,
			"kriteria": 0.7999999999999999,
			"nilai_komparatif": 0.9558823529411765,
			"nilai_komparatif_unit": 0.9632216969001494,
			"summary_team": 0.7647058823529411
		}, {
			"code": "Ha 1",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9925119977053664,
			"kriteria": 0.8695652173913045,
			"nilai_komparatif": 1.0282352941176467,
			"nilai_komparatif_unit": 1.035992810661101,
			"summary_team": 0.8941176470588234
		}, {
			"code": "Ha 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.991224259450578,
			"kriteria": 0.7130434782608696,
			"nilai_komparatif": 0.9569583931133427,
			"nilai_komparatif_unit": 0.9654307630079308,
			"summary_team": 0.6823529411764705
		}, {
			"code": "Ha 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9916103830635437,
			"kriteria": 0.8086956521739133,
			"nilai_komparatif": 0.9892473118279567,
			"nilai_komparatif_unit": 0.9976169357683747,
			"summary_team": 0.7999999999999999
		}, {
			"code": "Lo 1",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.993028029372769,
			"kriteria": 0.773913043478261,
			"nilai_komparatif": 1.0185062789160606,
			"nilai_komparatif_unit": 1.0256571303021371,
			"summary_team": 0.788235294117647
		}, {
			"code": "Lo 2",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9907716388036664,
			"kriteria": 0.7652173913043481,
			"nilai_komparatif": 1.030080213903743,
			"nilai_komparatif_unit": 1.039674707632468,
			"summary_team": 0.788235294117647
		}, {
			"code": "Lo 3",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9913155015095249,
			"kriteria": 0.7217391304347827,
			"nilai_komparatif": 1.0432317505315376,
			"nilai_komparatif_unit": 1.052371065460953,
			"summary_team": 0.7529411764705881
		}, {
			"code": "Ad 1",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.992055385656031,
			"kriteria": 0.8869565217391306,
			"nilai_komparatif": 1.047866205305651,
			"nilai_komparatif_unit": 1.0562577659035772,
			"summary_team": 0.9294117647058822
		}, {
			"code": "Ad 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9903260027970757,
			"kriteria": 0.8000000000000003,
			"nilai_komparatif": 0.9999999999999994,
			"nilai_komparatif_unit": 1.0097684976215917,
			"summary_team": 0.7999999999999998
		}, {
			"code": "Ad 3",
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"kriteria_bp": 0.9922224896764225,
			"kriteria": 0.8434782608695653,
			"nilai_komparatif": 1.0181928441479682,
			"nilai_komparatif_unit": 1.0261739224234023,
			"summary_team": 0.8588235294117645
		}, {
			"code": "Ko 1",
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"kriteria_bp": 0.9983462988993106,
			"kriteria": 0.6173913043478262,
			"nilai_komparatif": 0.8003314001656998,
			"nilai_komparatif_unit": 0.8016571013966549,
			"summary_team": 0.49411764705882344
		}, {
			"code": "Ko 2",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9911621117071272,
			"kriteria": 0.7478260869565219,
			"nilai_komparatif": 0.9753761969904238,
			"nilai_komparatif_unit": 0.9840733271275729,
			"summary_team": 0.7294117647058822
		}, {
			"code": "Ko 3",
			"feedback": "Average",
			"feedback_unit": "Average",
			"kriteria_bp": 0.9920399863830217,
			"kriteria": 0.5826086956521739,
			"nilai_komparatif": 0.9490781387181737,
			"nilai_komparatif_unit": 0.956693431459868,
			"summary_team": 0.5529411764705882
		}]
	},
};