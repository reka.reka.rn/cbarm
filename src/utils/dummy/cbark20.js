export const cbark20 = {
	"660001": {
		"nik": "660001",
		"nama": "SURIANSYAH",
		"band": "V",
		"posisi": "OFF 2 TGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7166666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9640591966173362,
			"nilai_komparatif_unit": 0.9646448532930036,
			"summary_team": 0.6909090909090909
		}]
	},
	"660552": {
		"nik": "660552",
		"nama": "SABRIE",
		"band": "V",
		"posisi": "SPV PLASA DABO SINGKEP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8608695652173907,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.97907647907648,
			"nilai_komparatif_unit": 0.9796712586065878,
			"summary_team": 0.842857142857143
		}]
	},
	"660610": {
		"nik": "660610",
		"nama": "SUMARNO",
		"band": "V",
		"posisi": "SPV PLASA PANGKALAN BALAI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8208333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0299953853253345,
			"nilai_komparatif_unit": 1.0306210976005135,
			"summary_team": 0.8454545454545455
		}]
	},
	"670458": {
		"nik": "670458",
		"nama": "Rostidewi",
		"band": "V",
		"posisi": "OFF 2 PENGENDALIAN MUTU",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9589345172031072,
			"nilai_komparatif_unit": 0.9595170606853939,
			"summary_team": 0.8470588235294116
		}]
	},
	"670486": {
		"nik": "670486",
		"nama": "Siti Munawaroh",
		"band": "V",
		"posisi": "OFFICER PERBENDAHARAAN  & PERPAJAKAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0023529411764702,
			"nilai_komparatif_unit": 1.002961860933094,
			"summary_team": 0.8352941176470587
		}]
	},
	"670516": {
		"nik": "670516",
		"nama": "ABDUL MUTHALIB",
		"band": "V",
		"posisi": "SPV PLASA BEUREUNEN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545454,
			"nilai_komparatif_unit": 1.0260774992132422,
			"summary_team": 0.8545454545454547
		}]
	},
	"680364": {
		"nik": "680364",
		"nama": "KOS KOMARA",
		"band": "V",
		"posisi": "STAFF QC & BUILDING ASSESSMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.819047619047619,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9889534883720931,
			"nilai_komparatif_unit": 0.989554268090208,
			"summary_team": 0.81
		}]
	},
	"680433": {
		"nik": "680433",
		"nama": "Ni Ketut Kadiani",
		"band": "V",
		"posisi": "OFFICER PEMBINAAN KESEHATAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8222222222222224,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9872813990461047,
			"nilai_komparatif_unit": 0.9878811629860603,
			"summary_team": 0.8117647058823529
		}]
	},
	"680458": {
		"nik": "680458",
		"nama": "TEDI EFRIYAN",
		"band": "V",
		"posisi": "SPV PLASA MUARA AMAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.026785714285714,
			"nilai_komparatif_unit": 1.0274094767166533,
			"summary_team": 0.8214285714285716
		}]
	},
	"680460": {
		"nik": "680460",
		"nama": "RIFAI",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8250000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969697,
			"nilai_komparatif_unit": 0.9702860512654774,
			"summary_team": 0.8000000000000003
		}]
	},
	"680533": {
		"nik": "680533",
		"nama": "Paimin",
		"band": "V",
		"posisi": "OFFICER PIC MITRA KESEHATAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444447,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0030959752321977,
			"nilai_komparatif_unit": 1.0037053463748529,
			"summary_team": 0.8470588235294116
		}]
	},
	"690191": {
		"nik": "690191",
		"nama": "ADZRI",
		"band": "V",
		"posisi": "SPV PLASA TANJUNG PINANG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8608695652173907,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.003213957759413,
			"nilai_komparatif_unit": 1.003823400575317,
			"summary_team": 0.8636363636363636
		}]
	},
	"690517": {
		"nik": "690517",
		"nama": "MURSITO",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882353,
			"nilai_komparatif_unit": 0.9417482262282574,
			"summary_team": 0.8
		}]
	},
	"710129": {
		"nik": "710129",
		"nama": "SUGIONO",
		"band": "V",
		"posisi": "SPV PLASA Kuala Tungkal",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9807692307692307,
			"nilai_komparatif_unit": 0.9813650386296865,
			"summary_team": 0.8500000000000001
		}]
	},
	"740253": {
		"nik": "740253",
		"nama": "EMANUEL PINTO",
		"band": "V",
		"posisi": "SPV  PLASA TUA PEJAT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0096153846153844,
			"nilai_komparatif_unit": 1.0102287162364418,
			"summary_team": 0.875
		}]
	},
	"840045": {
		"nik": "840045",
		"nama": "VERONICA MEGASARI TARIGAN",
		"band": "V",
		"posisi": "OFF 2 GES TERRITORY SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8318181818181802,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0218579234972698,
			"nilai_komparatif_unit": 1.022478692342772,
			"summary_team": 0.8500000000000001
		}]
	},
	"855886": {
		"nik": "855886",
		"nama": "INTAN APRIANI NURKUSUMA DEWI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER-1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8270270270270257,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.036414565826332,
			"nilai_komparatif_unit": 1.0370441776918324,
			"summary_team": 0.857142857142857
		}]
	},
	"860026": {
		"nik": "860026",
		"nama": "PRINS HARYANTO HUTAURUK",
		"band": "V",
		"posisi": "OFF 2 CCAN WIFI FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7809523809523812,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9545454545454543,
			"nilai_komparatif_unit": 0.955125331714454,
			"summary_team": 0.7454545454545455
		}]
	},
	"886057": {
		"nik": "886057",
		"nama": "SYAFWAN NUGRAHA",
		"band": "V",
		"posisi": "ACCOUNT MANAGER-1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8270270270270257,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0099961553248766,
			"nilai_komparatif_unit": 1.0106097182604723,
			"summary_team": 0.8352941176470587
		}]
	},
	"896265": {
		"nik": "896265",
		"nama": "ANDRE HERDIAN",
		"band": "V",
		"posisi": "ACCOUNT MANAGER-1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8547619047619041,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0422891871359845,
			"nilai_komparatif_unit": 1.0429223677773436,
			"summary_team": 0.8909090909090909
		}]
	},
	"896267": {
		"nik": "896267",
		"nama": "REZA NAJIB HIDAYAT",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8292397660818702,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8742947813822296,
			"nilai_komparatif_unit": 0.8748259070402954,
			"summary_team": 0.725
		}]
	},
	"900127": {
		"nik": "900127",
		"nama": "Rahmi Maya Fitri",
		"band": "V",
		"posisi": "OFF 2 PEMBINAAN KESEHATAN PROMOTIF",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9638009049773754,
			"nilai_komparatif_unit": 0.9643864047433596,
			"summary_team": 0.8352941176470587
		}]
	},
	"900129": {
		"nik": "900129",
		"nama": "PATRICIA POLIHARNITA SINAGA",
		"band": "V",
		"posisi": "OFF 2 HR ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0102040816326532,
			"nilai_komparatif_unit": 1.010817770881478,
			"summary_team": 0.9428571428571427
		}]
	},
	"906526": {
		"nik": "906526",
		"nama": "BUDI JULIANSYAH",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8318181818181802,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9978142076502752,
			"nilai_komparatif_unit": 0.9984203701700008,
			"summary_team": 0.8300000000000001
		}]
	},
	"910173": {
		"nik": "910173",
		"nama": "ZICO VENANCIO SINAGA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8172839506172833,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.083728959861891,
			"nilai_komparatif_unit": 1.0843873147660135,
			"summary_team": 0.8857142857142856
		}]
	},
	"910180": {
		"nik": "910180",
		"nama": "CALVIN SWINGLY",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8844444444444447,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9691313711414209,
			"nilai_komparatif_unit": 0.9697201091142542,
			"summary_team": 0.857142857142857
		}]
	},
	"910206": {
		"nik": "910206",
		"nama": "AMANITA MUSKARIANNY",
		"band": "V",
		"posisi": "OFF 2 GOVERNMENT ACCOUNT PLAN & TEAM MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0139037433155078,
			"nilai_komparatif_unit": 1.014519680073168,
			"summary_team": 0.9294117647058824
		}]
	},
	"910214": {
		"nik": "910214",
		"nama": "MARIA MAYASARI SIANTURI",
		"band": "V",
		"posisi": "OFF 2 EMPLOYEE & INDUSTRIAL RELATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0255102040816328,
			"nilai_komparatif_unit": 1.0261331916524095,
			"summary_team": 0.9571428571428571
		}]
	},
	"910270": {
		"nik": "910270",
		"nama": "DIAN ASIH LESTARI",
		"band": "V",
		"posisi": "OFF 2 SEKRETARIAT & ADMINISTRASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7750000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9618768328445745,
			"nilai_komparatif_unit": 0.9624611637552716,
			"summary_team": 0.7454545454545455
		}]
	},
	"910273": {
		"nik": "910273",
		"nama": "FIKA JULIA FIANDRIANI",
		"band": "V",
		"posisi": "OFF 2 CDC",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8749999999999997,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0181818181818185,
			"nilai_komparatif_unit": 1.0188003538287516,
			"summary_team": 0.8909090909090909
		}]
	},
	"916255": {
		"nik": "916255",
		"nama": "ARIEFFAN HARDI",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190467,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0500000000000012,
			"nilai_komparatif_unit": 1.050637864885901,
			"summary_team": 0.89
		}]
	},
	"916316": {
		"nik": "916316",
		"nama": "DIMAS ANJAR SAPUTRO",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999997,
			"nilai_komparatif_unit": 0.9605831907528224,
			"summary_team": 0.8
		}]
	},
	"916435": {
		"nik": "916435",
		"nama": "ARYO ADI KUSUMO",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8385964912280698,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8889311525294792,
			"nilai_komparatif_unit": 0.8894711696420324,
			"summary_team": 0.7454545454545454
		}]
	},
	"920151": {
		"nik": "920151",
		"nama": "DEWI QURROTUL A'YUNI",
		"band": "V",
		"posisi": "OFF 2 PUSH CHANNEL INTEGRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8266666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9828629032258065,
			"nilai_komparatif_unit": 0.9834599829721123,
			"summary_team": 0.8125000000000001
		}]
	},
	"920200": {
		"nik": "920200",
		"nama": "DELTA EGAWASALIS",
		"band": "V",
		"posisi": "OFF 2 LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9111111111111112,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9684361549497846,
			"nilai_komparatif_unit": 0.9690244705854781,
			"summary_team": 0.8823529411764706
		}]
	},
	"920203": {
		"nik": "920203",
		"nama": "DHARMA PRASETYA TURNIP",
		"band": "V",
		"posisi": "OFF 2 PAYMENT & COLLECTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8400000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0034013605442174,
			"nilai_komparatif_unit": 1.004010917205508,
			"summary_team": 0.8428571428571427
		}]
	},
	"920213": {
		"nik": "920213",
		"nama": "NUR AFIFAH",
		"band": "V",
		"posisi": "OFF 2 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9047619047619049,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9947368421052631,
			"nilai_komparatif_unit": 0.9953411351550628,
			"summary_team": 0.9000000000000001
		}]
	},
	"920231": {
		"nik": "920231",
		"nama": "FEBRINA ULY NOVA SILALAHI",
		"band": "V",
		"posisi": "OFF 2 GES TERITORY SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8285714285714281,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.022312373225153,
			"nilai_komparatif_unit": 1.0229334181444873,
			"summary_team": 0.8470588235294119
		}]
	},
	"920254": {
		"nik": "920254",
		"nama": "BAMBANG EKO SURYA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.846666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9448818897637792,
			"nilai_komparatif_unit": 0.9454558964102581,
			"summary_team": 0.8
		}]
	},
	"920257": {
		"nik": "920257",
		"nama": "FAJAR LAZUARDI",
		"band": "V",
		"posisi": "OFF 2 CCAN FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0084033613445376,
			"nilai_komparatif_unit": 1.0090159566731327,
			"summary_team": 0.914285714285714
		}]
	},
	"920261": {
		"nik": "920261",
		"nama": "MEDIANA NOVIA KURDINA",
		"band": "V",
		"posisi": "OFF 2 PRODUCT SOLUTION & DELIVERY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8035087719298245,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0489706799750467,
			"nilai_komparatif_unit": 1.049607919558946,
			"summary_team": 0.8428571428571427
		}]
	},
	"920280": {
		"nik": "920280",
		"nama": "ALWIE AUGUSRA TA",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ-1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8424242424242427,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9496402877697842,
			"nilai_komparatif_unit": 0.9502171850972165,
			"summary_team": 0.8000000000000003
		}]
	},
	"920290": {
		"nik": "920290",
		"nama": "JIHAD BAGUS CAHYADIN",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190467,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9438202247191019,
			"nilai_komparatif_unit": 0.9443935864142926,
			"summary_team": 0.7999999999999998
		}]
	},
	"920344": {
		"nik": "920344",
		"nama": "FIDZAH AQMARINA",
		"band": "V",
		"posisi": "OFF 2 WORKFORCE & DEVELOPMENT PLANNING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333329,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9600000000000003,
			"nilai_komparatif_unit": 0.960583190752823,
			"summary_team": 0.7999999999999998
		}]
	},
	"920351": {
		"nik": "920351",
		"nama": "ADI CANDRA SWASTIKA",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CIJAWURA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8999999999999999,
			"nilai_komparatif_unit": 0.9005467413307711,
			"summary_team": 0.8
		}]
	},
	"930032": {
		"nik": "930032",
		"nama": "PUTRI KURNIA SARI",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9056603773584903,
			"nilai_komparatif_unit": 0.9062105573139833,
			"summary_team": 0.8
		}]
	},
	"930054": {
		"nik": "930054",
		"nama": "DIAN MASMAWATI",
		"band": "V",
		"posisi": "OFF 2 BILLING SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9594882729211087,
			"nilai_komparatif_unit": 0.96007115280466,
			"summary_team": 0.8571428571428571
		}]
	},
	"930065": {
		"nik": "930065",
		"nama": "ANNISA HARTIKA",
		"band": "V",
		"posisi": "OFF 2 DIGITAL SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8461538461538458,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0099173553719014,
			"nilai_komparatif_unit": 1.0105308704372846,
			"summary_team": 0.8545454545454546
		}]
	},
	"930080": {
		"nik": "930080",
		"nama": "TRIANSARI PRAHARA",
		"band": "V",
		"posisi": "SPV PLASA ISKANDAR MUDA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.050420168067228,
			"nilai_komparatif_unit": 1.0510582882011814,
			"summary_team": 0.892857142857143
		}]
	},
	"930118": {
		"nik": "930118",
		"nama": "VIDYA NOOR RACHMADINI",
		"band": "V",
		"posisi": "OFF 2 PROJECT DATA MGT & ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9898989898989896,
			"nilai_komparatif_unit": 0.9905003440001745,
			"summary_team": 0.8909090909090909
		}]
	},
	"930136": {
		"nik": "930136",
		"nama": "FAUZIAH RAHMADHANI",
		"band": "V",
		"posisi": "OFF 2 COMMAND CONTROL MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8245614035087716,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9040618955512576,
			"nilai_komparatif_unit": 0.90461110444445,
			"summary_team": 0.7454545454545455
		}]
	},
	"930138": {
		"nik": "930138",
		"nama": "KHAIRUL ABRAR",
		"band": "V",
		"posisi": "OFF 2 BUSINESS TERITORY SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0628571428571427,
			"nilai_komparatif_unit": 1.063502818333482,
			"summary_team": 0.8857142857142856
		}]
	},
	"930150": {
		"nik": "930150",
		"nama": "DEWI AMALIA HANIFA",
		"band": "V",
		"posisi": "OFF 2 ADVALJAR OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9415584415584413,
			"nilai_komparatif_unit": 0.9421304292421485,
			"summary_team": 0.8285714285714284
		}]
	},
	"930165": {
		"nik": "930165",
		"nama": "EMILIA YULISITA",
		"band": "V",
		"posisi": "OFF 2 CRISIS SUPPORT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9500000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9952153110047844,
			"nilai_komparatif_unit": 0.9958198947198318,
			"summary_team": 0.9454545454545454
		}]
	},
	"930168": {
		"nik": "930168",
		"nama": "WINO FRANS LIMBONG",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181816,
			"nilai_komparatif_unit": 0.9824146269062956,
			"summary_team": 0.8181818181818181
		}]
	},
	"930173": {
		"nik": "930173",
		"nama": "AHMAD PRASETYO PERMANA",
		"band": "V",
		"posisi": "OFF 2 ASSET ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9111111111111112,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0776053215077603,
			"nilai_komparatif_unit": 1.0782599563605684,
			"summary_team": 0.9818181818181818
		}]
	},
	"930174": {
		"nik": "930174",
		"nama": "SATRIA SEVENTINO SIMAMORA",
		"band": "V",
		"posisi": "OFF 2 OM TRANSPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8491228070175435,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0262691853600947,
			"nilai_komparatif_unit": 1.026892634004687,
			"summary_team": 0.8714285714285712
		}]
	},
	"930179": {
		"nik": "930179",
		"nama": "M. REZA AULIA KHAFID",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE MANONJAYA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0188679245283019,
			"nilai_komparatif_unit": 1.0194868769782315,
			"summary_team": 0.9000000000000001
		}]
	},
	"930188": {
		"nik": "930188",
		"nama": "PRAMESTI FESTA PERDANANTI",
		"band": "V",
		"posisi": "OFF 2 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9917355371900826,
			"nilai_komparatif_unit": 0.9923380069760563,
			"summary_team": 0.9090909090909092
		}]
	},
	"930189": {
		"nik": "930189",
		"nama": "CHANDRA BUANA PUTRA",
		"band": "V",
		"posisi": "OFF 2 PERFORMANCE EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0105263157894735,
			"nilai_komparatif_unit": 1.0111402007924446,
			"summary_team": 0.8421052631578947
		}]
	},
	"930201": {
		"nik": "930201",
		"nama": "CANA PARANITA",
		"band": "V",
		"posisi": "OFF 2 INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0208507089241032,
			"nilai_komparatif_unit": 1.0214708658964542,
			"summary_team": 0.9272727272727272
		}]
	},
	"930208": {
		"nik": "930208",
		"nama": "META HERAWATI HALIEM",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.871794871794872,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9941176470588234,
			"nilai_komparatif_unit": 0.9947215639535968,
			"summary_team": 0.8666666666666668
		}]
	},
	"930209": {
		"nik": "930209",
		"nama": "ADI CIPTA ERLANGGA",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER CARE",
		"category": [{
			"code": "DL-1-01",
			"kriteria": 0.8571428571428571,
			"kriteria_bp": 0.9966140404810326,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.05,
			"nilai_komparatif_unit": 1.0535673363513922,
			"summary_team": 0.8999999999999999
		}]
	},
	"930220": {
		"nik": "930220",
		"nama": "WIKE ERIYANDARI",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8972222222222216,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.053757388122714,
			"nilai_komparatif_unit": 1.0543975355857054,
			"summary_team": 0.9454545454545454
		}]
	},
	"930229": {
		"nik": "930229",
		"nama": "DEVO FAUZAN RAHMAN",
		"band": "V",
		"posisi": "OFF 2 TGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8245614035087716,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9922630560928438,
			"nilai_komparatif_unit": 0.9928658463414697,
			"summary_team": 0.8181818181818182
		}]
	},
	"930233": {
		"nik": "930233",
		"nama": "SRINYTA NOVA SARI SIREGAR",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8622222222222221,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9444035346097202,
			"nilai_komparatif_unit": 0.9449772506600508,
			"summary_team": 0.8142857142857142
		}]
	},
	"930236": {
		"nik": "930236",
		"nama": "OKTORIO SARAGIH",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9238095238095237,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0360824742268038,
			"nilai_komparatif_unit": 1.0367118843498566,
			"summary_team": 0.9571428571428569
		}]
	},
	"930239": {
		"nik": "930239",
		"nama": "RYANO SATRIADI",
		"band": "V",
		"posisi": "OFF 2 HOME SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9935064935064933,
			"nilai_komparatif_unit": 0.9941100391313706,
			"summary_team": 0.9272727272727272
		}]
	},
	"930251": {
		"nik": "930251",
		"nama": "MUHAMMAD IHSAN",
		"band": "V",
		"posisi": "OFF 2 INFRA OPERATION PERFORMANCE MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.92,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9782608695652173,
			"nilai_komparatif_unit": 0.9788551536204033,
			"summary_team": 0.8999999999999999
		}]
	},
	"930254": {
		"nik": "930254",
		"nama": "PUDI IKHWANTO",
		"band": "V",
		"posisi": "OFF 2 ASSET & FACILITY MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9111111111111112,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9296987087517932,
			"nilai_komparatif_unit": 0.9302634917620589,
			"summary_team": 0.8470588235294116
		}]
	},
	"930259": {
		"nik": "930259",
		"nama": "RIZKA NADHIRA PUTRI",
		"band": "V",
		"posisi": "OFF 2 ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0588235294117645,
			"nilai_komparatif_unit": 1.0594667545067895,
			"summary_team": 0.9176470588235295
		}]
	},
	"930275": {
		"nik": "930275",
		"nama": "MIAN MARTUA SIANTURI",
		"band": "V",
		"posisi": "OFF 2 ADVALJAR OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9877232142857142,
			"nilai_komparatif_unit": 0.9883232466241721,
			"summary_team": 0.8428571428571427
		}]
	},
	"930276": {
		"nik": "930276",
		"nama": "EZRA MELIORA NAINGGOLAN",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8523809523809527,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.022346368715083,
			"nilai_komparatif_unit": 1.0229674342863502,
			"summary_team": 0.8714285714285712
		}]
	},
	"930280": {
		"nik": "930280",
		"nama": "MUHAMMAD RIZKI PRATAMA",
		"band": "V",
		"posisi": "OFF 2 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8400000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0034013605442171,
			"nilai_komparatif_unit": 1.0040109172055078,
			"summary_team": 0.8428571428571427
		}]
	},
	"930281": {
		"nik": "930281",
		"nama": "GIOVANI FIRYAN DARA",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8461538461538456,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9961038961038966,
			"nilai_komparatif_unit": 0.9967090196258324,
			"summary_team": 0.8428571428571427
		}]
	},
	"930286": {
		"nik": "930286",
		"nama": "AFFIANTO GUNAWAN",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444436,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857152,
			"nilai_komparatif_unit": 0.9648715085686843,
			"summary_team": 0.8142857142857142
		}]
	},
	"930288": {
		"nik": "930288",
		"nama": "FARIS AL FARUQI",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006993006993007,
			"nilai_komparatif_unit": 1.0076047455449189,
			"summary_team": 0.8727272727272728
		}]
	},
	"930292": {
		"nik": "930292",
		"nama": "ATMAKA ARIF PRATAMA",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9157894736842102,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0919540229885059,
			"nilai_komparatif_unit": 1.09261737453925,
			"summary_team": 0.9999999999999998
		}]
	},
	"930293": {
		"nik": "930293",
		"nama": "DENDY SUARISTA SURYANEGARA",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE RANCAEKEK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.863888888888888,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0349914885568385,
			"nilai_komparatif_unit": 1.0356202359166056,
			"summary_team": 0.8941176470588235
		}]
	},
	"930295": {
		"nik": "930295",
		"nama": "YAFIS SUKMA KURNIAWAN",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8608695652173907,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9458874458874464,
			"nilai_komparatif_unit": 0.9464620633995845,
			"summary_team": 0.8142857142857142
		}]
	},
	"930300": {
		"nik": "930300",
		"nama": "MUHAMMAD REZA PAHLEVI",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE RAJAPOLAH",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8547619047619041,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0848316029374536,
			"nilai_komparatif_unit": 1.0854906276866232,
			"summary_team": 0.9272727272727274
		}]
	},
	"930310": {
		"nik": "930310",
		"nama": "GARIZAH GANIH PRANOTO",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9250000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.027027027027027,
			"nilai_komparatif_unit": 1.0276509360531323,
			"summary_team": 0.9500000000000002
		}]
	},
	"930319": {
		"nik": "930319",
		"nama": "MOCH ULIN NIAM",
		"band": "V",
		"posisi": "OFF 2 ACCESS DESIGN & RAB",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8222222222222222,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.972972972972973,
			"nilai_komparatif_unit": 0.9735640446819148,
			"summary_team": 0.8
		}]
	},
	"930322": {
		"nik": "930322",
		"nama": "MUHAMMAD SAGIF SHAHAB",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ/ SPV AREA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8266666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9677419354838707,
			"nilai_komparatif_unit": 0.9683298293879257,
			"summary_team": 0.7999999999999999
		}]
	},
	"930327": {
		"nik": "930327",
		"nama": "DWI SUKMA BESTRY",
		"band": "V",
		"posisi": "OFF 2 CAPEX MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8400000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9693877551020406,
			"nilai_komparatif_unit": 0.9699766488256603,
			"summary_team": 0.8142857142857142
		}]
	},
	"930336": {
		"nik": "930336",
		"nama": "ISYQI HARZAKI",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8266666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0195852534562209,
			"nilai_komparatif_unit": 1.0202046416765647,
			"summary_team": 0.8428571428571427
		}]
	},
	"930338": {
		"nik": "930338",
		"nama": "RYAN IRFANDI",
		"band": "V",
		"posisi": "OFF 2 ADVALJAR OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999997,
			"nilai_komparatif_unit": 1.0006074903675233,
			"summary_team": 0.8999999999999998
		}]
	},
	"930342": {
		"nik": "930342",
		"nama": "SYAUQI ABDURROHMAN",
		"band": "V",
		"posisi": "OFF 2 FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7904761904761907,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0120481927710843,
			"nilai_komparatif_unit": 1.0126630022996623,
			"summary_team": 0.8000000000000002
		}]
	},
	"930348": {
		"nik": "930348",
		"nama": "ARNOLD PARLINDUNGAN",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9157894736842102,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9529780564263326,
			"nilai_komparatif_unit": 0.9535569814160729,
			"summary_team": 0.8727272727272727
		}]
	},
	"930351": {
		"nik": "930351",
		"nama": "IFAN FADLINA ANHAR",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.046153846153846,
			"nilai_komparatif_unit": 1.046789374538332,
			"summary_team": 0.9066666666666667
		}]
	},
	"930352": {
		"nik": "930352",
		"nama": "JOANNE CINDY THERESIA NAINGGOLAN",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9836065573770488,
			"nilai_komparatif_unit": 0.9842040888860883,
			"summary_team": 0.7999999999999999
		}]
	},
	"930355": {
		"nik": "930355",
		"nama": "FERIQ MUHAMMAD DAROJAT",
		"band": "V",
		"posisi": "OFF 2 ACCESS PROVISIONING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0255102040816324,
			"nilai_komparatif_unit": 1.026133191652409,
			"summary_team": 0.957142857142857
		}]
	},
	"930360": {
		"nik": "930360",
		"nama": "CHAERANNY RIEZKA UTAMI",
		"band": "V",
		"posisi": "OFF 2 CCAN ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999999,
			"nilai_komparatif_unit": 1.0006074903675235,
			"summary_team": 0.8
		}]
	},
	"930361": {
		"nik": "930361",
		"nama": "AKHMAD FAUZI",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0681818181818181,
			"nilai_komparatif_unit": 1.0688307283471274,
			"summary_team": 0.8545454545454546
		}]
	},
	"930377": {
		"nik": "930377",
		"nama": "DESY NOVITASSARI",
		"band": "V",
		"posisi": "OFF 2 QUALITY & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7904761904761907,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.012048192771084,
			"nilai_komparatif_unit": 1.012663002299662,
			"summary_team": 0.7999999999999999
		}]
	},
	"930415": {
		"nik": "930415",
		"nama": "REHAN AUFA FAKHRI",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.875362318840579,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0771050141911074,
			"nilai_komparatif_unit": 1.0777593451120397,
			"summary_team": 0.9428571428571426
		}]
	},
	"940012": {
		"nik": "940012",
		"nama": "DYAH PURI SURASTIANI",
		"band": "V",
		"posisi": "OFF 2 TAX OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272725,
			"nilai_komparatif_unit": 1.0233485696940579,
			"summary_team": 0.8999999999999999
		}]
	},
	"940021": {
		"nik": "940021",
		"nama": "NI KETUT PUTRI LIDYA",
		"band": "V",
		"posisi": "OFF 2 BUSINESS SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0285714285714285,
			"nilai_komparatif_unit": 1.0291962758065956,
			"summary_team": 0.857142857142857
		}]
	},
	"940035": {
		"nik": "940035",
		"nama": "RIDHO ZULANDRA",
		"band": "V",
		"posisi": "OFF 2 QOS, SLG & DATA CUSTOMER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8740740740740743,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0830508474576268,
			"nilai_komparatif_unit": 1.0837087904149956,
			"summary_team": 0.9466666666666667
		}]
	},
	"940038": {
		"nik": "940038",
		"nama": "AL ASY'ARI PRATAMA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8172839506172833,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1012084592145024,
			"nilai_komparatif_unit": 1.1018774327461107,
			"summary_team": 0.9
		}]
	},
	"940045": {
		"nik": "940045",
		"nama": "MOCH. YOGI SUMARNA",
		"band": "V",
		"posisi": "OFF 2 PEOPLE DATA PRESENTATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777769,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9113924050632919,
			"nilai_komparatif_unit": 0.9119460671704019,
			"summary_team": 0.7999999999999998
		}]
	},
	"940053": {
		"nik": "940053",
		"nama": "FAISHAL TANJUNG MAOLUDYO",
		"band": "V",
		"posisi": "ACCOUNT MANAGER-1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8984126984126981,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0971731448763253,
			"nilai_komparatif_unit": 1.0978396669933432,
			"summary_team": 0.9857142857142855
		}]
	},
	"940058": {
		"nik": "940058",
		"nama": "NINA RIZKITA AMALIYAH",
		"band": "V",
		"posisi": "OFF 2 ACCOUNTING OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9365079365079362,
			"nilai_komparatif_unit": 0.9370768560584741,
			"summary_team": 0.8428571428571427
		}]
	},
	"940061": {
		"nik": "940061",
		"nama": "SEKAR RINI ABIDIN",
		"band": "V",
		"posisi": "OFF 2 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.138461538461538,
			"nilai_komparatif_unit": 1.1391531428799493,
			"summary_team": 0.9866666666666666
		}]
	},
	"940067": {
		"nik": "940067",
		"nama": "ICHSAN FERIANSYAH",
		"band": "V",
		"posisi": "OFF 2 REGULATORY SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999998,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0795847750865053,
			"nilai_komparatif_unit": 1.0802406124382955,
			"summary_team": 0.9176470588235293
		}]
	},
	"940077": {
		"nik": "940077",
		"nama": "IDON ARISTIDEA TARIGAN",
		"band": "V",
		"posisi": "OFF 2 SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8405797101449269,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9007389162561581,
			"nilai_komparatif_unit": 0.9012861064714373,
			"summary_team": 0.757142857142857
		}]
	},
	"940078": {
		"nik": "940078",
		"nama": "TESAR DAYANSYAH",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.876190476190476,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0434782608695652,
			"nilai_komparatif_unit": 1.0441121638617636,
			"summary_team": 0.914285714285714
		}]
	},
	"940081": {
		"nik": "940081",
		"nama": "ZILLYA FATIMAH",
		"band": "V",
		"posisi": "OFF 2 ADVALJAR OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8419047619047649,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0520361990950189,
			"nilai_komparatif_unit": 1.0526753009522551,
			"summary_team": 0.8857142857142857
		}]
	},
	"940082": {
		"nik": "940082",
		"nama": "BAGASKORO RIZKY PRADANA",
		"band": "V",
		"posisi": "OFF 2 LEGAL RESEARCH & ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8743589743589737,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0456640134059496,
			"nilai_komparatif_unit": 1.0462992442217598,
			"summary_team": 0.9142857142857141
		}]
	},
	"940084": {
		"nik": "940084",
		"nama": "YESSI CLAUDIA SIANIPAR",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8318181818181802,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.033879781420767,
			"nilai_komparatif_unit": 1.0345078534291574,
			"summary_team": 0.86
		}]
	},
	"940087": {
		"nik": "940087",
		"nama": "REZKA ANDRIA TIRANA",
		"band": "V",
		"posisi": "OFF 2 LEARNING DEVT EXECUTION & REPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888888,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0125,
			"nilai_komparatif_unit": 1.0131150839971175,
			"summary_team": 0.8999999999999999
		}]
	},
	"940089": {
		"nik": "940089",
		"nama": "FA. BRIAN GANDA PRATAMA",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.002960796204468,
			"nilai_komparatif_unit": 1.0035700852271658,
			"summary_team": 0.8352941176470587
		}]
	},
	"940091": {
		"nik": "940091",
		"nama": "LUTFI DWI SETYAWAN",
		"band": "V",
		"posisi": "OFF 2 CONSUMER ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.027361701339917,
			"summary_team": 0.8727272727272727
		}]
	},
	"940092": {
		"nik": "940092",
		"nama": "ANNISA ZASKIA PUTRI",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8471014492753652,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0624465355004242,
			"nilai_komparatif_unit": 1.0630919615367496,
			"summary_team": 0.9000000000000001
		}]
	},
	"940093": {
		"nik": "940093",
		"nama": "DILA LIESWANI HANUM",
		"band": "V",
		"posisi": "OFF 2 DIGITAL TALENT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8388888888888892,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9957148422282815,
			"nilai_komparatif_unit": 0.9963197294037355,
			"summary_team": 0.8352941176470587
		}]
	},
	"940095": {
		"nik": "940095",
		"nama": "VIDILLA ELFA",
		"band": "V",
		"posisi": "SPV PLASA LHOKSEUMAWE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8424242424242427,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0143884892086328,
			"nilai_komparatif_unit": 1.0150047204447539,
			"summary_team": 0.8545454545454546
		}]
	},
	"940096": {
		"nik": "940096",
		"nama": "ARIF VIARDIMAN",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0219780219780217,
			"nilai_komparatif_unit": 1.0225988637821941,
			"summary_team": 0.8857142857142857
		}]
	},
	"940099": {
		"nik": "940099",
		"nama": "HERI MISBAH DIJAYA",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE KOPO",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0059583325620023,
			"summary_team": 0.8545454545454546
		}]
	},
	"940103": {
		"nik": "940103",
		"nama": "RAFIKA NURMASARI",
		"band": "V",
		"posisi": "OFF 2 ADVALJAR OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9592760180995474,
			"nilai_komparatif_unit": 0.9598587690403393,
			"summary_team": 0.8153846153846154
		}]
	},
	"940106": {
		"nik": "940106",
		"nama": "ALFIAN GHIFARI",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8419047619047649,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0350678733031635,
			"nilai_komparatif_unit": 1.0356966670659282,
			"summary_team": 0.8714285714285712
		}]
	},
	"940110": {
		"nik": "940110",
		"nama": "RAZIV RAVSANZHANI",
		"band": "V",
		"posisi": "SPV PLASA PADANG SIDEMPUAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8405797101449269,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0057993730407528,
			"nilai_komparatif_unit": 1.0064103864715364,
			"summary_team": 0.8454545454545453
		}]
	},
	"940121": {
		"nik": "940121",
		"nama": "ALAMANDARI FARIS",
		"band": "V",
		"posisi": "OFF 2 LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8866666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0150375939849619,
			"nilai_komparatif_unit": 1.015654219545982,
			"summary_team": 0.8999999999999998
		}]
	},
	"940122": {
		"nik": "940122",
		"nama": "PUTRI HANDAYANI",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER HANDLING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8424242424242427,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0174717368961967,
			"nilai_komparatif_unit": 1.0180898411755885,
			"summary_team": 0.857142857142857
		}]
	},
	"940131": {
		"nik": "940131",
		"nama": "NOVITA WARDHANI",
		"band": "V",
		"posisi": "OFF 2 ACCOUNTING OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9926470588235292,
			"nilai_komparatif_unit": 0.993250082350115,
			"summary_team": 0.8823529411764706
		}]
	},
	"940134": {
		"nik": "940134",
		"nama": "SINTA AULIA UTAMI",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE BANJARAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.863888888888888,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9260450160771712,
			"nilai_komparatif_unit": 0.9266075795043313,
			"summary_team": 0.7999999999999998
		}]
	},
	"940139": {
		"nik": "940139",
		"nama": "PRIO GALIH RAGA PRAKOSO",
		"band": "V",
		"posisi": "OFF 2 MICRO DEMAND",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8491228070175435,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9421487603305788,
			"nilai_komparatif_unit": 0.9427211066272538,
			"summary_team": 0.7999999999999999
		}]
	},
	"940142": {
		"nik": "940142",
		"nama": "SUNTI YOGI UTAMI",
		"band": "V",
		"posisi": "OFF 2 ACCOUNTING ASSESSMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9222222222222225,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0077958894401131,
			"nilai_komparatif_unit": 1.0084081157353777,
			"summary_team": 0.9294117647058824
		}]
	},
	"940145": {
		"nik": "940145",
		"nama": "DENY ALFA RIZKI",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ DEMAK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882354,
			"nilai_komparatif_unit": 0.9417482262282575,
			"summary_team": 0.8
		}]
	},
	"940146": {
		"nik": "940146",
		"nama": "FIRMAN ADENDRO SASOTYO",
		"band": "V",
		"posisi": "OFF 2 LOGISTIK & LEGAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.01010101010101,
			"nilai_komparatif_unit": 1.010714636734872,
			"summary_team": 0.9090909090909092
		}]
	},
	"940149": {
		"nik": "940149",
		"nama": "DWINIA AQMARINA",
		"band": "V",
		"posisi": "OFF 2 PERFORMANSI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0085763293310464,
			"nilai_komparatif_unit": 1.0091890297360273,
			"summary_team": 0.8909090909090909
		}]
	},
	"940160": {
		"nik": "940160",
		"nama": "PUTRI NURFADILA",
		"band": "V",
		"posisi": "OFF 2 FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8370370370370374,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.055309734513274,
			"nilai_komparatif_unit": 1.0559508250117446,
			"summary_team": 0.8833333333333334
		}]
	},
	"940169": {
		"nik": "940169",
		"nama": "ADITYA INDRA BAGASKARA",
		"band": "V",
		"posisi": "OFF 2 MAINTENANCE & DATA MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272725,
			"nilai_komparatif_unit": 1.0233485696940579,
			"summary_team": 0.8999999999999999
		}]
	},
	"940176": {
		"nik": "940176",
		"nama": "ANNISA RACMATUL RISA",
		"band": "V",
		"posisi": "OFF 2 TPC ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0102040816326527,
			"nilai_komparatif_unit": 1.0108177708814776,
			"summary_team": 0.9428571428571426
		}]
	},
	"940179": {
		"nik": "940179",
		"nama": "EDWIN SANTOSA",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9801869293396148,
			"summary_team": 0.8
		}]
	},
	"940183": {
		"nik": "940183",
		"nama": "ARIFAN KESUMA PUTRA",
		"band": "V",
		"posisi": "SPV PLASA MUARA BUNGO",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545452,
			"nilai_komparatif_unit": 1.026077499213242,
			"summary_team": 0.8545454545454545
		}]
	},
	"940186": {
		"nik": "940186",
		"nama": "FAHD FARRAS MAHMOD",
		"band": "V",
		"posisi": "OFF 2 DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8277777777777782,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9664429530201336,
			"nilai_komparatif_unit": 0.9670300578048543,
			"summary_team": 0.7999999999999998
		}]
	},
	"940188": {
		"nik": "940188",
		"nama": "YOKA FERNANDO",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8250000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0389610389610384,
			"nilai_komparatif_unit": 1.0395921977844396,
			"summary_team": 0.857142857142857
		}]
	},
	"940202": {
		"nik": "940202",
		"nama": "JONAS VALENTIN PASARIBU",
		"band": "V",
		"posisi": "OFF 2 LEGAL SETTLEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8392156862745095,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9953271028037386,
			"nilai_komparatif_unit": 0.995931754431227,
			"summary_team": 0.8352941176470587
		}]
	},
	"940205": {
		"nik": "940205",
		"nama": "RAMANDA SUGRAHA",
		"band": "V",
		"posisi": "OFF 2 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.846666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9955005624296958,
			"nilai_komparatif_unit": 0.996105319432236,
			"summary_team": 0.8428571428571427
		}]
	},
	"940210": {
		"nik": "940210",
		"nama": "ICHDA MARLIYANA MORA",
		"band": "V",
		"posisi": "OFF 2 LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9133333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0479666319082372,
			"nilai_komparatif_unit": 1.0486032615426075,
			"summary_team": 0.957142857142857
		}]
	},
	"940216": {
		"nik": "940216",
		"nama": "RAYYAN RAMADHAN",
		"band": "V",
		"posisi": "OFF 2 SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8761904761904761,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0108695652173914,
			"nilai_komparatif_unit": 1.0114836587410836,
			"summary_team": 0.8857142857142856
		}]
	},
	"940217": {
		"nik": "940217",
		"nama": "AHMAD RAMADHAN",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0234541577825158,
			"nilai_komparatif_unit": 1.0240758963249705,
			"summary_team": 0.9142857142857141
		}]
	},
	"940218": {
		"nik": "940218",
		"nama": "FAISAL RAMADHAN",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8385964912280698,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0016736401673645,
			"nilai_komparatif_unit": 1.0022821472551684,
			"summary_team": 0.8400000000000001
		}]
	},
	"940219": {
		"nik": "940219",
		"nama": "MUHAMMAD ILMAN NAFIAN",
		"band": "V",
		"posisi": "OFF 2 APARTMENT OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8461538461538456,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8948051948051954,
			"nilai_komparatif_unit": 0.8953487803418495,
			"summary_team": 0.7571428571428571
		}]
	},
	"940230": {
		"nik": "940230",
		"nama": "UNGU SIWI MAHARUNTI",
		"band": "V",
		"posisi": "OFF 2 CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0045248868778278,
			"nilai_komparatif_unit": 1.0051351260705437,
			"summary_team": 0.8705882352941177
		}]
	},
	"940233": {
		"nik": "940233",
		"nama": "TIYO YOGA PRADIKNAS",
		"band": "V",
		"posisi": "OFF 2 SOLUTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0102040816326525,
			"nilai_komparatif_unit": 1.0108177708814774,
			"summary_team": 0.9428571428571426
		}]
	},
	"940235": {
		"nik": "940235",
		"nama": "ALI MUHAMMAD HANAFIYAH",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8693333333333324,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.018843120070115,
			"nilai_komparatif_unit": 1.0194620574515751,
			"summary_team": 0.8857142857142857
		}]
	},
	"940237": {
		"nik": "940237",
		"nama": "WILLY PRAKARSA",
		"band": "V",
		"posisi": "OFF 2 PROC MONIT&CONTROL OPERATION CAT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9429892141756547,
			"nilai_komparatif_unit": 0.943562071039945,
			"summary_team": 0.9272727272727272
		}]
	},
	"940242": {
		"nik": "940242",
		"nama": "JUNDA LUTFI FALASTIAN",
		"band": "V",
		"posisi": "OFF 2 CHANGE & QUALITY MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9801869293396148,
			"summary_team": 0.8
		}]
	},
	"940244": {
		"nik": "940244",
		"nama": "YURA PINATA",
		"band": "V",
		"posisi": "OFF 2 SURVEILLANCE & LOGIC NE ACCESS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444436,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8827751196172259,
			"nilai_komparatif_unit": 0.8833113969990828,
			"summary_team": 0.7454545454545455
		}]
	},
	"940245": {
		"nik": "940245",
		"nama": "FERDY FAJRIAN",
		"band": "V",
		"posisi": "SPV PLASA DURI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0308441558441557,
			"nilai_komparatif_unit": 1.031470383739249,
			"summary_team": 0.9071428571428573
		}]
	},
	"940248": {
		"nik": "940248",
		"nama": "AGIL SAPUTRO",
		"band": "V",
		"posisi": "OFF 2 ACCESS NETWORK & CPE CATEGORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346935,
			"nilai_komparatif_unit": 0.9801869293396145,
			"summary_team": 0.8
		}]
	},
	"940256": {
		"nik": "940256",
		"nama": "JULIOCAISAR WALIPUTRA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190467,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9438202247191022,
			"nilai_komparatif_unit": 0.9443935864142929,
			"summary_team": 0.8
		}]
	},
	"940259": {
		"nik": "940259",
		"nama": "ARISTIO AULIA MAYUZA",
		"band": "V",
		"posisi": "OFF 2 CORPORATE TAX",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0135135135135132,
			"nilai_komparatif_unit": 1.0141292132103275,
			"summary_team": 0.9999999999999998
		}]
	},
	"940261": {
		"nik": "940261",
		"nama": "SARAH NITALYA BAKARA",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER PROFILE DATA ANALYTIC",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9935064935064937,
			"nilai_komparatif_unit": 0.994110039131371,
			"summary_team": 0.9272727272727274
		}]
	},
	"940267": {
		"nik": "940267",
		"nama": "HARI HARBENO",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9395604395604391,
			"nilai_komparatif_unit": 0.9401312134771782,
			"summary_team": 0.8142857142857142
		}]
	},
	"940268": {
		"nik": "940268",
		"nama": "MUHAMMAD ADIKA MAHARDITYA",
		"band": "V",
		"posisi": "OFF 2 GROUP LEVERAGING PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969697,
			"nilai_komparatif_unit": 0.9702860512654774,
			"summary_team": 0.8727272727272727
		}]
	},
	"940271": {
		"nik": "940271",
		"nama": "MUHAMMAD HAFIZH MARTHAJAYA",
		"band": "V",
		"posisi": "OFF 2 RECIPROCAL ELIMINATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888888,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0125,
			"nilai_komparatif_unit": 1.0131150839971175,
			"summary_team": 0.8999999999999999
		}]
	},
	"940272": {
		"nik": "940272",
		"nama": "ADITYA SURYAPRATAMA YUDASUBRATA",
		"band": "V",
		"posisi": "OFF 2 ACCOUNTING CONTROL PARENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.051948051948052,
			"nilai_komparatif_unit": 1.0525871002567455,
			"summary_team": 0.9818181818181818
		}]
	},
	"940276": {
		"nik": "940276",
		"nama": "IRHAM MU'ALIMIN ARRIJAL",
		"band": "V",
		"posisi": "OFF 2 MICRO DEMAND",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9157894736842102,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0295566502463056,
			"nilai_komparatif_unit": 1.0301820959941501,
			"summary_team": 0.9428571428571426
		}]
	},
	"940277": {
		"nik": "940277",
		"nama": "MUHAMMAD KHAERUL ANAM",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE KLARI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9494949494949493,
			"nilai_komparatif_unit": 0.9500717585307797,
			"summary_team": 0.8545454545454546
		}]
	},
	"940281": {
		"nik": "940281",
		"nama": "NOVIAN PERMANA",
		"band": "V",
		"posisi": "OFF 2 ACCESS DESIGN & RAB",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.0006074903675233,
			"summary_team": 0.9
		}]
	},
	"940283": {
		"nik": "940283",
		"nama": "DEO DENADA WOLLAH",
		"band": "V",
		"posisi": "OFF 2 REVENUE ASSURANCE & FRAUD MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8571428571428573,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0166666666666662,
			"nilai_komparatif_unit": 1.0172842818736485,
			"summary_team": 0.8714285714285712
		}]
	},
	"940288": {
		"nik": "940288",
		"nama": "IFTI RAHMANIA A. YASIN",
		"band": "V",
		"posisi": "OFF 2 COLLECTION PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0074626865671643,
			"nilai_komparatif_unit": 1.0080747104448933,
			"summary_team": 0.8999999999999999
		}]
	},
	"940297": {
		"nik": "940297",
		"nama": "CHRISTINE RISMA MANIK SIHOTANG",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8628205128205122,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.053626908010267,
			"nilai_komparatif_unit": 1.0542669762078467,
			"summary_team": 0.9090909090909092
		}]
	},
	"940300": {
		"nik": "940300",
		"nama": "GIAN PRADIPTA HADISOEBROTO",
		"band": "V",
		"posisi": "OFF 2 BUDGET POLICY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9151515151515154,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9990539262062436,
			"nilai_komparatif_unit": 0.9996608418430504,
			"summary_team": 0.914285714285714
		}]
	},
	"940302": {
		"nik": "940302",
		"nama": "AMANDA LAILATUL FADHILAH",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8691358024691347,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.051948051948053,
			"nilai_komparatif_unit": 1.0525871002567466,
			"summary_team": 0.9142857142857141
		}]
	},
	"940303": {
		"nik": "940303",
		"nama": "ADILLA KASSANDRA",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.029195183227925,
			"nilai_komparatif_unit": 1.0298204093880374,
			"summary_team": 0.8571428571428571
		}]
	},
	"940304": {
		"nik": "940304",
		"nama": "ILHAM SYAHRIMANDA",
		"band": "V",
		"posisi": "AUDITOR PRATAMA II",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0309876049580107,
			"nilai_komparatif_unit": 1.0316139199970589,
			"summary_team": 0.8999999999999998
		}]
	},
	"940305": {
		"nik": "940305",
		"nama": "ILFAN",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9607843137254904,
			"nilai_komparatif_unit": 0.9613679809413463,
			"summary_team": 0.8166666666666669
		}]
	},
	"940311": {
		"nik": "940311",
		"nama": "DEWANGGA RIZKY RACHMADAN",
		"band": "V",
		"posisi": "OFF 2 SYSTEM QUALITY ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8874999999999998,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9657947686116701,
			"nilai_komparatif_unit": 0.9663814796306064,
			"summary_team": 0.8571428571428571
		}]
	},
	"940316": {
		"nik": "940316",
		"nama": "APUNG PRAKOSO HIDAYAT",
		"band": "V",
		"posisi": "OFF 2 BES SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8874999999999998,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0301810865191146,
			"nilai_komparatif_unit": 1.03080691160598,
			"summary_team": 0.914285714285714
		}]
	},
	"940317": {
		"nik": "940317",
		"nama": "RISALATUL HANIFAH",
		"band": "V",
		"posisi": "OFF 2 TGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0546874999999998,
			"nilai_komparatif_unit": 1.0553282124969972,
			"summary_team": 0.8999999999999999
		}]
	},
	"940322": {
		"nik": "940322",
		"nama": "WASSY PRAWINNETOU",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8488888888888891,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0602094240837692,
			"nilai_komparatif_unit": 1.0608534910964578,
			"summary_team": 0.8999999999999999
		}]
	},
	"940325": {
		"nik": "940325",
		"nama": "IMAM BHASKARA",
		"band": "V",
		"posisi": "OFF 2 CAREER & TALENT DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999997,
			"nilai_komparatif_unit": 0.9605831907528224,
			"summary_team": 0.8
		}]
	},
	"940326": {
		"nik": "940326",
		"nama": "LUQMAN KURNIAWAN",
		"band": "V",
		"posisi": "OFF 2 SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8318181818181802,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0304449648711962,
			"nilai_komparatif_unit": 1.0310709502616187,
			"summary_team": 0.857142857142857
		}]
	},
	"940329": {
		"nik": "940329",
		"nama": "DIATI LEVI PUTRI",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.001855287569573,
			"nilai_komparatif_unit": 1.0024639050064241,
			"summary_team": 0.8181818181818181
		}]
	},
	"940332": {
		"nik": "940332",
		"nama": "MUHAMAD NASHISH",
		"band": "V",
		"posisi": "OFF 2 ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9828947368421049,
			"nilai_komparatif_unit": 0.983491835927026,
			"summary_team": 0.8736842105263156
		}]
	},
	"940335": {
		"nik": "940335",
		"nama": "RIZAL LUTHFI NARTADHI",
		"band": "V",
		"posisi": "OFF 2 ACCESS DEPLOYMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.027361701339917,
			"summary_team": 0.8727272727272727
		}]
	},
	"940339": {
		"nik": "940339",
		"nama": "NUR HASANAH AZKA TIFFANY",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8461538461538456,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1311688311688315,
			"nilai_komparatif_unit": 1.1318560053378095,
			"summary_team": 0.9571428571428569
		}]
	},
	"940343": {
		"nik": "940343",
		"nama": "OKKY ANGGADA BASUKI",
		"band": "V",
		"posisi": "OFF 2 SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0006074903675237,
			"summary_team": 0.8
		}]
	},
	"940344": {
		"nik": "940344",
		"nama": "RAIHAN AMIR PERDANA",
		"band": "V",
		"posisi": "OFF 2 HC EFFECTIVENESS MEASUREMENT&SURVE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8484848484848487,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.026050420168067,
			"nilai_komparatif_unit": 1.0266737359149125,
			"summary_team": 0.8705882352941177
		}]
	},
	"940346": {
		"nik": "940346",
		"nama": "RULI ADI LESTARI",
		"band": "V",
		"posisi": "OFF 2 ADVALJAR OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0192307692307694,
			"nilai_komparatif_unit": 1.0198499421053606,
			"summary_team": 0.8833333333333334
		}]
	},
	"940347": {
		"nik": "940347",
		"nama": "LITA TIAMI ADELA",
		"band": "V",
		"posisi": "OFF 2 VERIFICATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8851851851851847,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9569283780457793,
			"nilai_komparatif_unit": 0.9575097028178521,
			"summary_team": 0.8470588235294116
		}]
	},
	"940350": {
		"nik": "940350",
		"nama": "SATRIA REGI GUNTARA",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9466666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0035211267605633,
			"nilai_komparatif_unit": 1.0041307561786768,
			"summary_team": 0.95
		}]
	},
	"940352": {
		"nik": "940352",
		"nama": "REZA RENALDY",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999996,
			"nilai_komparatif_unit": 1.000607490367523,
			"summary_team": 0.9333333333333331
		}]
	},
	"940353": {
		"nik": "940353",
		"nama": "OKTAVIAN ALMA RENATA",
		"band": "V",
		"posisi": "OFF 2 CCAN FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8099999999999994,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9347442680776019,
			"nilai_komparatif_unit": 0.9353121162165569,
			"summary_team": 0.757142857142857
		}]
	},
	"940356": {
		"nik": "940356",
		"nama": "RIDLO QOMARRULLAH",
		"band": "V",
		"posisi": "OFF 2 VAS & MES BILLING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8400000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0204081632653057,
			"nilai_komparatif_unit": 1.0210280513954317,
			"summary_team": 0.857142857142857
		}]
	},
	"940358": {
		"nik": "940358",
		"nama": "FARID NUR HAMMADI",
		"band": "V",
		"posisi": "OFF 2 NTE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0255102040816326,
			"nilai_komparatif_unit": 1.0261331916524092,
			"summary_team": 0.9571428571428571
		}]
	},
	"940378": {
		"nik": "940378",
		"nama": "FAIRUZ AZRINA RITONGA",
		"band": "V",
		"posisi": "OFF 2 ACCOUNTING OPERATION & REPORTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9773755656108593,
			"nilai_komparatif_unit": 0.9779693118524208,
			"summary_team": 0.8470588235294118
		}]
	},
	"940383": {
		"nik": "940383",
		"nama": "SYAFIURRUSDY",
		"band": "V",
		"posisi": "OFF 2 CASH BANK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8851851851851847,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9968003937976869,
			"nilai_komparatif_unit": 0.9974059404352627,
			"summary_team": 0.8823529411764706
		}]
	},
	"940384": {
		"nik": "940384",
		"nama": "MIRANDA AMALIA NASUTION",
		"band": "V",
		"posisi": "OFF 2 LOGISTIK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8400000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9693877551020403,
			"nilai_komparatif_unit": 0.96997664882566,
			"summary_team": 0.8142857142857142
		}]
	},
	"940390": {
		"nik": "940390",
		"nama": "ZAID ZAMANDA",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96,
			"nilai_komparatif_unit": 0.9605831907528226,
			"summary_team": 0.8
		}]
	},
	"940392": {
		"nik": "940392",
		"nama": "FADA WIBAWANTO",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9190476190476191,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.088082901554404,
			"nilai_komparatif_unit": 1.0887439014361653,
			"summary_team": 1
		}]
	},
	"940394": {
		"nik": "940394",
		"nama": "MUHAMMAD FAHMI AZZAM SOFYAN",
		"band": "V",
		"posisi": "OFF 2 LOGISTIK & LEGAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0219780219780217,
			"nilai_komparatif_unit": 1.0225988637821941,
			"summary_team": 0.8857142857142857
		}]
	},
	"940397": {
		"nik": "940397",
		"nama": "NOVIANA CYNTHIA RATNASARI",
		"band": "V",
		"posisi": "OFF 2 TRANSACT&STRAT CONTRACT COMPLIANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.856249999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9843587069864452,
			"nilai_komparatif_unit": 0.9849566954191274,
			"summary_team": 0.8428571428571427
		}]
	},
	"940399": {
		"nik": "940399",
		"nama": "RAFDLI MUHAMMAD",
		"band": "V",
		"posisi": "OFF 2 ACCESS FAULT HANDLING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9600000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9821428571428567,
			"nilai_komparatif_unit": 0.982739499468103,
			"summary_team": 0.9428571428571426
		}]
	},
	"940402": {
		"nik": "940402",
		"nama": "RIFA RIZKA ANISAH",
		"band": "V",
		"posisi": "OFF 2 GS BIDDING MGT & OBL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0588235294117645,
			"nilai_komparatif_unit": 1.0594667545067895,
			"summary_team": 0.9529411764705882
		}]
	},
	"950002": {
		"nik": "950002",
		"nama": "DEVINTA AMALIA SUCI",
		"band": "V",
		"posisi": "OFF 2 BUSINESS & PRODUCT LINE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.862745098039215,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1045454545454554,
			"nilai_komparatif_unit": 1.1052164552695836,
			"summary_team": 0.9529411764705882
		}]
	},
	"950004": {
		"nik": "950004",
		"nama": "ANNISA YUDITYA PRATIWI",
		"band": "V",
		"posisi": "OFF 2 DATA ACQUISITION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8753623188405791,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.028145695364239,
			"nilai_komparatif_unit": 1.0287702839705835,
			"summary_team": 0.8999999999999999
		}]
	},
	"950006": {
		"nik": "950006",
		"nama": "IZZUDDIN AL AZZAM",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.876190476190476,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0108695652173914,
			"nilai_komparatif_unit": 1.0114836587410836,
			"summary_team": 0.8857142857142856
		}]
	},
	"950007": {
		"nik": "950007",
		"nama": "FREDDY HASIHOLAN SITORUS",
		"band": "V",
		"posisi": "OFF 2 VERIFICATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.984126984126984,
			"nilai_komparatif_unit": 0.984724831790261,
			"summary_team": 0.8857142857142857
		}]
	},
	"950009": {
		"nik": "950009",
		"nama": "LIDA FEBRIANI",
		"band": "V",
		"posisi": "OFF 2 ACCOUNTING ASSESSMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8909090909090912,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0300120048019206,
			"nilai_komparatif_unit": 1.0306377271732714,
			"summary_team": 0.9176470588235295
		}]
	},
	"950010": {
		"nik": "950010",
		"nama": "RIZA RAHMA PUTRI",
		"band": "V",
		"posisi": "OFF 2 SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190467,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0296220633299298,
			"nilai_komparatif_unit": 1.0302475488155924,
			"summary_team": 0.8727272727272728
		}]
	},
	"950011": {
		"nik": "950011",
		"nama": "NABILA PRISCANDY POETRI",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8604938271604932,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0459110473457682,
			"nilai_komparatif_unit": 1.0465464282323171,
			"summary_team": 0.8999999999999999
		}]
	},
	"950013": {
		"nik": "950013",
		"nama": "FIKRI HARDIAN PUTRA",
		"band": "V",
		"posisi": "OFF 2 SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999993,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000009,
			"nilai_komparatif_unit": 1.0006074903675244,
			"summary_team": 0.7999999999999999
		}]
	},
	"950017": {
		"nik": "950017",
		"nama": "FADIAH SHABRINA WINARNO",
		"band": "V",
		"posisi": "OFF 2 PROGRAM & EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0234541577825156,
			"nilai_komparatif_unit": 1.0240758963249703,
			"summary_team": 0.9142857142857141
		}]
	},
	"950018": {
		"nik": "950018",
		"nama": "RYA SOFI AULIA",
		"band": "V",
		"posisi": "AUDITOR PRATAMA II",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9622550979608102,
			"nilai_komparatif_unit": 0.9628396586639218,
			"summary_team": 0.84
		}]
	},
	"950021": {
		"nik": "950021",
		"nama": "TAUFIK IHSAN",
		"band": "V",
		"posisi": "OFF 2 FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8428571428571432,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9661016949152537,
			"nilai_komparatif_unit": 0.9666885923889629,
			"summary_team": 0.8142857142857142
		}]
	},
	"950023": {
		"nik": "950023",
		"nama": "DONI PUTRA NUSANTARA",
		"band": "V",
		"posisi": "OFF 2 ACCOUNTING ASSESSMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8799999999999993,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.962566844919787,
			"nilai_komparatif_unit": 0.9631515950061732,
			"summary_team": 0.8470588235294119
		}]
	},
	"950027": {
		"nik": "950027",
		"nama": "ARINDRA KARUNIA RAHMADHANI",
		"band": "V",
		"posisi": "OFF 2 HOME SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8592592592592595,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9310344827586202,
			"nilai_komparatif_unit": 0.9316000772387283,
			"summary_team": 0.7999999999999998
		}]
	},
	"950030": {
		"nik": "950030",
		"nama": "FATTIYA MAHARANI PUSPARIDA",
		"band": "V",
		"posisi": "OFF 2 ACCOUNTING ASSESSMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8799999999999993,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9090909090909097,
			"nilai_komparatif_unit": 0.9096431730613856,
			"summary_team": 0.7999999999999999
		}]
	},
	"950031": {
		"nik": "950031",
		"nama": "MA'RUF SARAGIH",
		"band": "V",
		"posisi": "SPV PLASA BANGKINANG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8717948717948718,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9906417112299465,
			"nilai_komparatif_unit": 0.9912435165271857,
			"summary_team": 0.8636363636363636
		}]
	},
	"950034": {
		"nik": "950034",
		"nama": "FITRI PURNAMA SARI",
		"band": "V",
		"posisi": "OFF 2 MONITORING & EVALUATION PROJECT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8392857142857179,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.07234042553191,
			"nilai_komparatif_unit": 1.0729918620111267,
			"summary_team": 0.8999999999999998
		}]
	},
	"950036": {
		"nik": "950036",
		"nama": "BELLA LAILATUS SAADAH",
		"band": "V",
		"posisi": "OFF 2 STATUTORY ACCOUNTING POLICY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8904761904761893,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0437244416483185,
			"nilai_komparatif_unit": 1.0443584941929687,
			"summary_team": 0.9294117647058824
		}]
	},
	"950037": {
		"nik": "950037",
		"nama": "DWI HANDAYANI",
		"band": "V",
		"posisi": "OFF 2 TALENT ACQUISITION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8518518518518513,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0496163682864457,
			"nilai_komparatif_unit": 1.0502540001197747,
			"summary_team": 0.8941176470588235
		}]
	},
	"950038": {
		"nik": "950038",
		"nama": "MUHAMMAD SAHIRUL ALIM",
		"band": "V",
		"posisi": "OFF 2 BGES SALES ENGINEER & BIDDING MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0084033613445376,
			"nilai_komparatif_unit": 1.0090159566731327,
			"summary_team": 0.9142857142857141
		}]
	},
	"950041": {
		"nik": "950041",
		"nama": "VISOLINE IVAPRILDA BR SINISUKA",
		"band": "V",
		"posisi": "OFF 2 FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8099999999999994,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.040564373897708,
			"nilai_komparatif_unit": 1.0411965067316389,
			"summary_team": 0.8428571428571427
		}]
	},
	"950052": {
		"nik": "950052",
		"nama": "HAYUNINGTYAS ARDIATI",
		"band": "V",
		"posisi": "OFF 2 DATA VALIDATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9466666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.011066398390342,
			"nilai_komparatif_unit": 1.0116806114882908,
			"summary_team": 0.9571428571428571
		}]
	},
	"950064": {
		"nik": "950064",
		"nama": "TAQIYYAH NAJIATULLAH",
		"band": "V",
		"posisi": "OFF 2 EVENT SUPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8206349206349192,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9748549323017424,
			"nilai_komparatif_unit": 0.9754471472828484,
			"summary_team": 0.7999999999999998
		}]
	},
	"950067": {
		"nik": "950067",
		"nama": "RENALDI FIRMANSYAH",
		"band": "V",
		"posisi": "OFF 2 ACCOUNTING ASSESSMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8799999999999993,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0537190082644636,
			"nilai_komparatif_unit": 1.0543591324120607,
			"summary_team": 0.9272727272727272
		}]
	},
	"950071": {
		"nik": "950071",
		"nama": "VIVI OKTAVIANI DAMANIK",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8844444444444447,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0337401292175157,
			"nilai_komparatif_unit": 1.0343681163885379,
			"summary_team": 0.9142857142857141
		}]
	},
	"950075": {
		"nik": "950075",
		"nama": "NENGGALA YUDHABRAMA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8410256410256403,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9652080344332864,
			"nilai_komparatif_unit": 0.965794389016861,
			"summary_team": 0.8117647058823529
		}]
	},
	"950080": {
		"nik": "950080",
		"nama": "DAYU BHARA TANTRI",
		"band": "V",
		"posisi": "OFF 2 WORK FACILITIES MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0384615384615383,
			"nilai_komparatif_unit": 1.0390923938431973,
			"summary_team": 0.8999999999999999
		}]
	},
	"950081": {
		"nik": "950081",
		"nama": "BAGUS MUHAMMAD SATRIYO",
		"band": "V",
		"posisi": "OFF 2 PRICING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8600000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0465116279069762,
			"nilai_komparatif_unit": 1.047147373640431,
			"summary_team": 0.8999999999999999
		}]
	},
	"950084": {
		"nik": "950084",
		"nama": "ELDORADO ARIFIN",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8256410256410259,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0900621118012417,
			"nilai_komparatif_unit": 1.0907243140341634,
			"summary_team": 0.8999999999999998
		}]
	},
	"950086": {
		"nik": "950086",
		"nama": "AZIZA AGUNG KHOIRUNISA",
		"band": "V",
		"posisi": "OFF 2 PAYMENT & COLLECTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9898989898989898,
			"nilai_komparatif_unit": 0.9905003440001747,
			"summary_team": 0.8909090909090909
		}]
	},
	"950094": {
		"nik": "950094",
		"nama": "NAYYIROH",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9056603773584905,
			"nilai_komparatif_unit": 0.9062105573139835,
			"summary_team": 0.8000000000000002
		}]
	},
	"950095": {
		"nik": "950095",
		"nama": "ISTIGFARO ANJAZ AJIZI",
		"band": "V",
		"posisi": "OFF 2 PARLIAMENT COMM. CHANNEL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999996,
			"nilai_komparatif_unit": 1.000607490367523,
			"summary_team": 0.7999999999999998
		}]
	},
	"950100": {
		"nik": "950100",
		"nama": "MUHAMMAD ERIK HARRISANDRIAN",
		"band": "V",
		"posisi": "OFF 2 SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9309133489461361,
			"nilai_komparatif_unit": 0.9314788698386199,
			"summary_team": 0.7571428571428571
		}]
	},
	"950111": {
		"nik": "950111",
		"nama": "Melita Widya Ningrum",
		"band": "V",
		"posisi": "OFF 2 FINANCIAL APPLICATION SYSTEM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777779,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0454206999255398,
			"nilai_komparatif_unit": 1.0460557829307542,
			"summary_team": 0.9176470588235295
		}]
	},
	"950112": {
		"nik": "950112",
		"nama": "REVONANDA AVRY LAKSONO",
		"band": "V",
		"posisi": "OFF 2 CCAN ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.948717948717949,
			"nilai_komparatif_unit": 0.9492942857332919,
			"summary_team": 0.8222222222222224
		}]
	},
	"950114": {
		"nik": "950114",
		"nama": "LUQMAN ABDUL HAKIM",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE KARYAMULYA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090908,
			"nilai_komparatif_unit": 1.091571807673662,
			"summary_team": 0.9818181818181818
		}]
	},
	"950123": {
		"nik": "950123",
		"nama": "IGO PRAMANDA PUTRA",
		"band": "V",
		"posisi": "OFF 2 EVENT DATA & REPORTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9979716024340769,
			"nilai_komparatif_unit": 0.9985778605696176,
			"summary_team": 0.9647058823529411
		}]
	},
	"950124": {
		"nik": "950124",
		"nama": "RAHMAT HARDIAN PUTRA",
		"band": "V",
		"posisi": "OFF 2 DATA & REPORTING ATR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.971900826446281,
			"nilai_komparatif_unit": 0.9724912468365352,
			"summary_team": 0.8909090909090909
		}]
	},
	"950127": {
		"nik": "950127",
		"nama": "MAULIDA YUMNISARI",
		"band": "V",
		"posisi": "OFF 2 LEARNING EXPERIENCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9444444444444444,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0588235294117647,
			"nilai_komparatif_unit": 1.0594667545067897,
			"summary_team": 1
		}]
	},
	"950128": {
		"nik": "950128",
		"nama": "ARUQMAANA RASYID",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999994,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9915966386554628,
			"nilai_komparatif_unit": 0.9921990240619146,
			"summary_team": 0.8428571428571427
		}]
	},
	"950129": {
		"nik": "950129",
		"nama": "WINDI RAHMA AGUSTIN",
		"band": "V",
		"posisi": "OFF 2 COSTING MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999998,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9747899159663865,
			"nilai_komparatif_unit": 0.9753820914506952,
			"summary_team": 0.8285714285714283
		}]
	},
	"950139": {
		"nik": "950139",
		"nama": "RAHADIAN HILMY",
		"band": "V",
		"posisi": "OFF 2 OSS APLICATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777779,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0454206999255395,
			"nilai_komparatif_unit": 1.046055782930754,
			"summary_team": 0.9176470588235293
		}]
	},
	"950143": {
		"nik": "950143",
		"nama": "DHUHA KHANIF RIZKY",
		"band": "V",
		"posisi": "OFF 2 PROMO PLAN & EXECUTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7939393939393933,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9536532170119962,
			"nilai_komparatif_unit": 0.9542325521552889,
			"summary_team": 0.757142857142857
		}]
	},
	"950145": {
		"nik": "950145",
		"nama": "MAESY",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090915,
			"nilai_komparatif_unit": 1.0915718076736627,
			"summary_team": 0.9090909090909092
		}]
	},
	"950146": {
		"nik": "950146",
		"nama": "LATHIFUDDIN FIRAS NURSETO",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE PGNDARAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200683,
			"nilai_komparatif_unit": 1.0503804187048442,
			"summary_team": 0.9272727272727272
		}]
	},
	"950149": {
		"nik": "950149",
		"nama": "CHRISTOVAN HUTAHAEAN",
		"band": "V",
		"posisi": "OFF 2 MICRO DEMAND",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0467289719626163,
			"nilai_komparatif_unit": 1.0473648497304913,
			"summary_team": 0.9333333333333331
		}]
	},
	"950150": {
		"nik": "950150",
		"nama": "TEGUH ARIYANTO",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8693333333333324,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9859772129710789,
			"nilai_komparatif_unit": 0.9865761846305565,
			"summary_team": 0.857142857142857
		}]
	},
	"950153": {
		"nik": "950153",
		"nama": "PANDU WISNUMURTI",
		"band": "V",
		"posisi": "OFF 2 DISCIPLINARY ACTION ADM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9041666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0149091894822446,
			"nilai_komparatif_unit": 1.0155257370387663,
			"summary_team": 0.9176470588235295
		}]
	},
	"950155": {
		"nik": "950155",
		"nama": "QISTHI MAHRAN HARAHAP",
		"band": "V",
		"posisi": "OFF 2 PLANNING & EVALUATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.858333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.110222729868647,
			"nilai_komparatif_unit": 1.1108971794828477,
			"summary_team": 0.9529411764705882
		}]
	},
	"950156": {
		"nik": "950156",
		"nama": "MOHAMMAD NURFARIZA ILAHUDE",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER DATA MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.846666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0376827896512935,
			"nilai_komparatif_unit": 1.0383131719505516,
			"summary_team": 0.8785714285714288
		}]
	},
	"950158": {
		"nik": "950158",
		"nama": "SADIDA FATIN ARUNI",
		"band": "V",
		"posisi": "OFF 2 LEARNING DEVT EXECUTION & REPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333325,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0114285714285722,
			"nilai_komparatif_unit": 1.0120430045431532,
			"summary_team": 0.8428571428571427
		}]
	},
	"950160": {
		"nik": "950160",
		"nama": "DIAN ING TYAS DANANJAYA",
		"band": "V",
		"posisi": "OFF 2 SMALL & MICRO CUSTOMER MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9422222222222221,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0537398921832888,
			"nilai_komparatif_unit": 1.0543800290176655,
			"summary_team": 0.9928571428571431
		}]
	},
	"950164": {
		"nik": "950164",
		"nama": "MAHARANI AMALIA RIZKI",
		"band": "V",
		"posisi": "OFF 2 PUSH CHANNEL INTEGRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.038461538461538,
			"nilai_komparatif_unit": 1.039092393843197,
			"summary_team": 0.8999999999999999
		}]
	},
	"950170": {
		"nik": "950170",
		"nama": "FITRIA PURNAMASARI",
		"band": "V",
		"posisi": "OFF 2 BENEFIT & FACILITY SYSTEM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8238095238095232,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0139408364501876,
			"nilai_komparatif_unit": 1.0145567957415698,
			"summary_team": 0.8352941176470587
		}]
	},
	"950177": {
		"nik": "950177",
		"nama": "EDITA KARENINA",
		"band": "V",
		"posisi": "OFF 2 CAREER DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.080900750625521,
			"nilai_komparatif_unit": 1.081557387419775,
			"summary_team": 0.9818181818181818
		}]
	},
	"950183": {
		"nik": "950183",
		"nama": "BELLINDA",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8419047619047649,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0690045248868738,
			"nilai_komparatif_unit": 1.0696539348385816,
			"summary_team": 0.8999999999999999
		}]
	},
	"950191": {
		"nik": "950191",
		"nama": "GOLDI RILLO PANGAYOM",
		"band": "V",
		"posisi": "OFF 2 CONSUMER BILLING DATA MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9890109890109885,
			"nilai_komparatif_unit": 0.9896118036601876,
			"summary_team": 0.857142857142857
		}]
	},
	"950195": {
		"nik": "950195",
		"nama": "DIDEN RIDWAN NUGRAHA",
		"band": "V",
		"posisi": "OFF 2 NETWORK PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9629629629629631,
			"nilai_komparatif_unit": 0.9635479536872451,
			"summary_team": 0.8666666666666668
		}]
	},
	"950198": {
		"nik": "950198",
		"nama": "RISAL RAYADI",
		"band": "V",
		"posisi": "OFF 2 SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9724770642201834,
			"nilai_komparatif_unit": 0.9730678346693348,
			"summary_team": 0.8833333333333335
		}]
	},
	"950204": {
		"nik": "950204",
		"nama": "TAUFANI KURNIAWAN",
		"band": "V",
		"posisi": "OFF 2 ACCESS CAPEX QE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9133333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9697601668404586,
			"nilai_komparatif_unit": 0.9703492868006223,
			"summary_team": 0.8857142857142857
		}]
	},
	"950212": {
		"nik": "950212",
		"nama": "DEWI NUR CAHYANINGSIH",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER PAYMENT COLLECTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8851851851851847,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9835097218803843,
			"nilai_komparatif_unit": 0.9841071945627924,
			"summary_team": 0.8705882352941176
		}]
	},
	"950219": {
		"nik": "950219",
		"nama": "ASMA HANIFAH",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8980392156862743,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.012306470821755,
			"nilai_komparatif_unit": 1.012921437251761,
			"summary_team": 0.9090909090909092
		}]
	},
	"950221": {
		"nik": "950221",
		"nama": "KINTAN PERMATA DEROSE",
		"band": "V",
		"posisi": "OFF 2 COLLECTION PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9754797441364602,
			"nilai_komparatif_unit": 0.9760723386847374,
			"summary_team": 0.8714285714285712
		}]
	},
	"950225": {
		"nik": "950225",
		"nama": "PRAMUDIASTUTI AGENG NURSYACHBANI",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.097142857142857,
			"nilai_komparatif_unit": 1.0978093608603685,
			"summary_team": 0.9142857142857141
		}]
	},
	"950228": {
		"nik": "950228",
		"nama": "ANDINA NUR DAMAYANTI",
		"band": "V",
		"posisi": "OFF 2 IT SYSTEM DELIVERY & EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7733333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9790640394088667,
			"nilai_komparatif_unit": 0.9796588113819962,
			"summary_team": 0.757142857142857
		}]
	},
	"950230": {
		"nik": "950230",
		"nama": "DORY FERMANDO",
		"band": "V",
		"posisi": "OFF 2 WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0059583325620023,
			"summary_team": 0.8545454545454546
		}]
	},
	"950233": {
		"nik": "950233",
		"nama": "FRONDY FERNANDA FERDIANTO",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9724770642201834,
			"nilai_komparatif_unit": 0.9730678346693348,
			"summary_team": 0.8833333333333335
		}]
	},
	"950234": {
		"nik": "950234",
		"nama": "FASYA MAUDIA",
		"band": "V",
		"posisi": "OFF 2 INTERNAL NEWS MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8977777777777771,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9697146185206763,
			"nilai_komparatif_unit": 0.9703037108106743,
			"summary_team": 0.8705882352941177
		}]
	},
	"950236": {
		"nik": "950236",
		"nama": "DWI KRESNA WIJAYA",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CIMAHI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8574712643678148,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0152972717237043,
			"nilai_komparatif_unit": 1.0159140550364494,
			"summary_team": 0.8705882352941177
		}]
	},
	"950238": {
		"nik": "950238",
		"nama": "ARI PRATAMA",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE GERLONG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0686456400742113,
			"nilai_komparatif_unit": 1.0692948320068523,
			"summary_team": 0.8727272727272727
		}]
	},
	"950243": {
		"nik": "950243",
		"nama": "MILZAM DANAR AMADIS",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346935,
			"nilai_komparatif_unit": 0.9801869293396145,
			"summary_team": 0.7999999999999998
		}]
	},
	"950246": {
		"nik": "950246",
		"nama": "NATALIA TIARA PERMATA PUTRI",
		"band": "V",
		"posisi": "OFF 2 HOME SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0314049586776857,
			"nilai_komparatif_unit": 1.0320315272550984,
			"summary_team": 0.9454545454545454
		}]
	},
	"950247": {
		"nik": "950247",
		"nama": "CUT RIMA ASYIFA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7939393939393933,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8816793893129777,
			"nilai_komparatif_unit": 0.8822150010492295,
			"summary_team": 0.7
		}]
	},
	"950249": {
		"nik": "950249",
		"nama": "IRASANTI PRIMABARA RUDIYANTO",
		"band": "V",
		"posisi": "OFF 2 DC & CME OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8266666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9677419354838708,
			"nilai_komparatif_unit": 0.9683298293879258,
			"summary_team": 0.7999999999999999
		}]
	},
	"950251": {
		"nik": "950251",
		"nama": "MEGAN GRACIELA NAULI",
		"band": "V",
		"posisi": "OFF 2 IP OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8745098039215687,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9355890746025275,
			"nilai_komparatif_unit": 0.9361574359533088,
			"summary_team": 0.8181818181818182
		}]
	},
	"950253": {
		"nik": "950253",
		"nama": "ULIL ALBAB RABBANI",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CICADAS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8945454545454548,
			"nilai_komparatif_unit": 0.8950888822924031,
			"summary_team": 0.7454545454545454
		}]
	},
	"950255": {
		"nik": "950255",
		"nama": "ANDINI PUTRI PAMUNGKAS",
		"band": "V",
		"posisi": "OFF 2 HC PLANNING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0519480519480517,
			"nilai_komparatif_unit": 1.0525871002567453,
			"summary_team": 0.9818181818181818
		}]
	},
	"950260": {
		"nik": "950260",
		"nama": "ADRIANUS DESTIAN HERU WIDANTO",
		"band": "V",
		"posisi": "OFF 2 BUSINESS SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8285714285714281,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8965517241379315,
			"nilai_komparatif_unit": 0.8970963706743319,
			"summary_team": 0.7428571428571428
		}]
	},
	"950261": {
		"nik": "950261",
		"nama": "RUSMAWATI HARYA MEGASARI",
		"band": "V",
		"posisi": "OFF 2 SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777778,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.740506329113924,
			"nilai_komparatif_unit": 0.740956179575951,
			"summary_team": 0.6499999999999999
		}]
	},
	"950271": {
		"nik": "950271",
		"nama": "NISRINA NURAMALIA FATHINA",
		"band": "V",
		"posisi": "OFF 2 DEPLOYMENT & CAPEX MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9245283018867925,
			"nilai_komparatif_unit": 0.9250899439246916,
			"summary_team": 0.8166666666666668
		}]
	},
	"950276": {
		"nik": "950276",
		"nama": "SELLA GABRELIAN MAHARANI",
		"band": "V",
		"posisi": "OFF 2 HC & CDC",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8588235294117647,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1008717310087173,
			"nilai_komparatif_unit": 1.101540499981184,
			"summary_team": 0.9454545454545454
		}]
	},
	"950282": {
		"nik": "950282",
		"nama": "FARISYA SAKINA",
		"band": "V",
		"posisi": "OFF 2 LEADERSHIP & CAPABILITY REVIEW",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8289855072463761,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9650349650349657,
			"nilai_komparatif_unit": 0.9656212144805478,
			"summary_team": 0.7999999999999998
		}]
	},
	"950287": {
		"nik": "950287",
		"nama": "LELY FIRDA ANGGRAENI",
		"band": "V",
		"posisi": "OFF 2 HELPDESK OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777779,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9713524317121918,
			"nilai_komparatif_unit": 0.9719425189579275,
			"summary_team": 0.8526315789473684
		}]
	},
	"950294": {
		"nik": "950294",
		"nama": "TARI USTAMI",
		"band": "V",
		"posisi": "OFF 2 CAPEX MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9890109890109887,
			"nilai_komparatif_unit": 0.9896118036601879,
			"summary_team": 0.857142857142857
		}]
	},
	"950302": {
		"nik": "950302",
		"nama": "LATIFA AYU LESTARI",
		"band": "V",
		"posisi": "OFF 2 BIDDING MANAGEMENT & OBL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8099999999999994,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.040564373897708,
			"nilai_komparatif_unit": 1.0411965067316389,
			"summary_team": 0.8428571428571427
		}]
	},
	"950306": {
		"nik": "950306",
		"nama": "Muhammad Alfian K. Wardhana",
		"band": "V",
		"posisi": "OFF 2 SECRETARY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.0006074903675233,
			"summary_team": 0.8999999999999999
		}]
	},
	"950314": {
		"nik": "950314",
		"nama": "ANNISA RIZKI",
		"band": "V",
		"posisi": "OFF 2 LEARNING EXPERIENCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9594882729211086,
			"nilai_komparatif_unit": 0.9600711528046599,
			"summary_team": 0.857142857142857
		}]
	},
	"950317": {
		"nik": "950317",
		"nama": "NISA SALSABILA",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0058823529411762,
			"nilai_komparatif_unit": 1.0064934167814499,
			"summary_team": 0.8941176470588235
		}]
	},
	"950359": {
		"nik": "950359",
		"nama": "SARAH MUSTIKA PUTRI ADHITYA",
		"band": "V",
		"posisi": "OFF 2 QOS & SLG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8577777777777779,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9538389072067828,
			"nilai_komparatif_unit": 0.9544183551550801,
			"summary_team": 0.8181818181818182
		}]
	},
	"950369": {
		"nik": "950369",
		"nama": "BAGUS SATRIYO WIBOWO",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE WANARAJA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.083825265643447,
			"nilai_komparatif_unit": 1.0844836790524042,
			"summary_team": 0.9272727272727272
		}]
	},
	"960002": {
		"nik": "960002",
		"nama": "IDA FITRIANA",
		"band": "V",
		"posisi": "OFF 2 FAULT HANDLING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9843749999999997,
			"nilai_komparatif_unit": 0.9849729983305306,
			"summary_team": 0.84
		}]
	},
	"960008": {
		"nik": "960008",
		"nama": "MUHAMMAD BAYU FIRLYANSYAH",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8693333333333324,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.035276073619633,
			"nilai_komparatif_unit": 1.0359049938620843,
			"summary_team": 0.8999999999999999
		}]
	},
	"960014": {
		"nik": "960014",
		"nama": "MUHAMAD DICKY RYALDI SUHERMAN",
		"band": "V",
		"posisi": "OFF 2 LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9900687547746366,
			"nilai_komparatif_unit": 0.9906702120063482,
			"summary_team": 0.8470588235294116
		}]
	},
	"960017": {
		"nik": "960017",
		"nama": "AMANDA PUTRI SANTOSO",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9250000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9909909909909909,
			"nilai_komparatif_unit": 0.9915930084723205,
			"summary_team": 0.9166666666666667
		}]
	},
	"960019": {
		"nik": "960019",
		"nama": "FARIZA AULIA PUTRI",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999997,
			"nilai_komparatif_unit": 0.9605831907528224,
			"summary_team": 0.8
		}]
	},
	"960020": {
		"nik": "960020",
		"nama": "WITA RISANTI",
		"band": "V",
		"posisi": "AUDITOR PRATAMA II",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.024249123879854,
			"nilai_komparatif_unit": 1.0248713453565552,
			"summary_team": 0.8941176470588235
		}]
	},
	"960023": {
		"nik": "960023",
		"nama": "KRISANDI PUTRA SITUMORANG",
		"band": "V",
		"posisi": "OFF 2 SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0546874999999998,
			"nilai_komparatif_unit": 1.0553282124969972,
			"summary_team": 0.8999999999999999
		}]
	},
	"960024": {
		"nik": "960024",
		"nama": "AMADEA FARRAS",
		"band": "V",
		"posisi": "OFF 2 ACCOUNTING OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9395604395604392,
			"nilai_komparatif_unit": 0.9401312134771783,
			"summary_team": 0.8142857142857142
		}]
	},
	"960028": {
		"nik": "960028",
		"nama": "RIA WIDIYA ARIANI",
		"band": "V",
		"posisi": "OFF 2 ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8428571428571432,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9661016949152537,
			"nilai_komparatif_unit": 0.9666885923889629,
			"summary_team": 0.8142857142857142
		}]
	},
	"960035": {
		"nik": "960035",
		"nama": "SELVIA LIGAR UTAMI",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0980861244019136,
			"nilai_komparatif_unit": 1.098753201145199,
			"summary_team": 0.9272727272727274
		}]
	},
	"960040": {
		"nik": "960040",
		"nama": "ACHMAD DANU FIRDAUS",
		"band": "V",
		"posisi": "OFF 2 ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999997,
			"nilai_komparatif_unit": 1.0006074903675233,
			"summary_team": 0.8999999999999999
		}]
	},
	"960041": {
		"nik": "960041",
		"nama": "ARISAL AHMAD SAILA",
		"band": "V",
		"posisi": "OFF 2 BILLING & PAYMENT APPLICATION SYST",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9988901220865701,
			"nilai_komparatif_unit": 0.9994969382139521,
			"summary_team": 0.8823529411764706
		}]
	},
	"960043": {
		"nik": "960043",
		"nama": "NUR ANISA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8488888888888891,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9424083769633504,
			"nilai_komparatif_unit": 0.9429808809746292,
			"summary_team": 0.7999999999999999
		}]
	},
	"960044": {
		"nik": "960044",
		"nama": "FALDY MAULANA YUANTORO",
		"band": "V",
		"posisi": "OFF 2 BATTLE MANAGEMENT EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8571428571428573,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.039393939393939,
			"nilai_komparatif_unit": 1.0400253612001833,
			"summary_team": 0.8909090909090909
		}]
	},
	"960046": {
		"nik": "960046",
		"nama": "AULIA MAULIDINA KUSUMAWARDANI",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9935064935064936,
			"nilai_komparatif_unit": 0.9941100391313709,
			"summary_team": 0.9272727272727272
		}]
	},
	"960053": {
		"nik": "960053",
		"nama": "REZA TRIANTO",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CIAWI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0115702479338844,
			"nilai_komparatif_unit": 1.0121847671155777,
			"summary_team": 0.9272727272727272
		}]
	},
	"960055": {
		"nik": "960055",
		"nama": "WILDAN AULIA BHASKARA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190467,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9438202247191022,
			"nilai_komparatif_unit": 0.9443935864142929,
			"summary_team": 0.8
		}]
	},
	"960058": {
		"nik": "960058",
		"nama": "RANI DWIFENTARY",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8461538461538456,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9961038961038966,
			"nilai_komparatif_unit": 0.9967090196258324,
			"summary_team": 0.8428571428571427
		}]
	},
	"960061": {
		"nik": "960061",
		"nama": "JAWARA ANDRA PATRA",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8693333333333324,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.920245398773007,
			"nilai_komparatif_unit": 0.9208044389885195,
			"summary_team": 0.7999999999999999
		}]
	},
	"960076": {
		"nik": "960076",
		"nama": "CHANDRA HIDAYATUL AKBAR",
		"band": "V",
		"posisi": "OFF 2 OLO ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.857971014492753,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9490830115830121,
			"nilai_komparatif_unit": 0.9496595703705291,
			"summary_team": 0.8142857142857142
		}]
	},
	"960091": {
		"nik": "960091",
		"nama": "TRINANDA ADITYA ARYA WIBISONO",
		"band": "V",
		"posisi": "OFF 2 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8631578947368416,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9930313588850178,
			"nilai_komparatif_unit": 0.9936346158701893,
			"summary_team": 0.857142857142857
		}]
	},
	"960100": {
		"nik": "960100",
		"nama": "MARIA LAKSMI LARASATI",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER PROFILE DATA ANALYTIC",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9473684210526316,
			"nilai_komparatif_unit": 0.9479439382429171,
			"summary_team": 0.8421052631578949
		}]
	},
	"960102": {
		"nik": "960102",
		"nama": "SALMA NABILA HADI",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96,
			"nilai_komparatif_unit": 0.9605831907528226,
			"summary_team": 0.8
		}]
	},
	"960104": {
		"nik": "960104",
		"nama": "MUHAMAD FAZRUR RIZAL",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CIKAJANG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.020070838252656,
			"nilai_komparatif_unit": 1.0206905214610862,
			"summary_team": 0.8727272727272727
		}]
	},
	"960107": {
		"nik": "960107",
		"nama": "LINGGOM ENRICO CHRISTIAN",
		"band": "V",
		"posisi": "OFF 2 PORTFOLIO SYNERGY ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9111111111111112,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0177383592017737,
			"nilai_komparatif_unit": 1.018356625451648,
			"summary_team": 0.9272727272727272
		}]
	},
	"960122": {
		"nik": "960122",
		"nama": "INTAN SALSABILA FIRMAN",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE RSDENGKLOK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9836065573770487,
			"nilai_komparatif_unit": 0.9842040888860882,
			"summary_team": 0.7999999999999998
		}]
	},
	"960138": {
		"nik": "960138",
		"nama": "MUCHAMMAD HAIDAR TEGAR REVALDO",
		"band": "V",
		"posisi": "OFF 2 DESIGN & ENGINEERING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555557,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0211893369788105,
			"nilai_komparatif_unit": 1.021809699664443,
			"summary_team": 0.8736842105263158
		}]
	},
	"960168": {
		"nik": "960168",
		"nama": "ADRIAN GUSTI NURCAHYO",
		"band": "V",
		"posisi": "OFF 2 SOLUTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0255102040816324,
			"nilai_komparatif_unit": 1.026133191652409,
			"summary_team": 0.9571428571428571
		}]
	},
	"970001": {
		"nik": "970001",
		"nama": "DWI MARDAYATI NINGSIH",
		"band": "V",
		"posisi": "OFF 2 HOME SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8172839506172833,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9788519637462241,
			"nilai_komparatif_unit": 0.9794466068854314,
			"summary_team": 0.7999999999999998
		}]
	},
	"970002": {
		"nik": "970002",
		"nama": "ALVITA MAURIZKA",
		"band": "V",
		"posisi": "OFF 2 BACKBONE NODES CATEGORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545454,
			"nilai_komparatif_unit": 1.0260774992132422,
			"summary_team": 0.8545454545454546
		}]
	},
	"970012": {
		"nik": "970012",
		"nama": "GHEYI AMANATUNISSA MARGAATMADJA",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200685,
			"nilai_komparatif_unit": 1.0503804187048444,
			"summary_team": 0.9272727272727272
		}]
	}
};