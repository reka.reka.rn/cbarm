export const cbark22 = {
	"635645": {
		"nik": "635645",
		"nama": "SUPAAT",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.895238095238095,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9748549323017409,
			"nilai_komparatif_unit": 0.975447147282847,
			"summary_team": 0.8727272727272727
		}]
	},
	"651422": {
		"nik": "651422",
		"nama": "DJOKO PRIHARTONO",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.000834028356964,
			"nilai_komparatif_unit": 1.0014420253886804,
			"summary_team": 0.9090909090909092
		}]
	},
	"660169": {
		"nik": "660169",
		"nama": "MARDJUKI SUDJAL",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8791666666666664,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0133563119345113,
			"nilai_komparatif_unit": 1.0139719161328806,
			"summary_team": 0.8909090909090909
		}]
	},
	"660489": {
		"nik": "660489",
		"nama": "MOHAMMAD DIAH",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333318,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.983606557377051,
			"nilai_komparatif_unit": 0.9842040888860906,
			"summary_team": 0.8
		}]
	},
	"660491": {
		"nik": "660491",
		"nama": "SYAHRONI",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8905380333951761,
			"nilai_komparatif_unit": 0.891079026672377,
			"summary_team": 0.7272727272727273
		}]
	},
	"660498": {
		"nik": "660498",
		"nama": "SUTRIONO",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0369036903690365,
			"nilai_komparatif_unit": 1.0375335993729853,
			"summary_team": 0.8727272727272727
		}]
	},
	"660508": {
		"nik": "660508",
		"nama": "ADI SUHARJA",
		"band": "V",
		"posisi": "OFF 2 WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8599999999999993,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0782241014799163,
			"nilai_komparatif_unit": 1.0788791122355972,
			"summary_team": 0.9272727272727272
		}]
	},
	"660517": {
		"nik": "660517",
		"nama": "SALIRI",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9988193624557258,
			"nilai_komparatif_unit": 0.9994261355973136,
			"summary_team": 0.8545454545454546
		}]
	},
	"660536": {
		"nik": "660536",
		"nama": "PORDIN SIAHAAN",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9608006672226854,
			"nilai_komparatif_unit": 0.9613843443731334,
			"summary_team": 0.8727272727272728
		}]
	},
	"660539": {
		"nik": "660539",
		"nama": "AGUS AMPERANTO",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8060606060606053,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1165413533834598,
			"nilai_komparatif_unit": 1.117219641500582,
			"summary_team": 0.9000000000000001
		}]
	},
	"660542": {
		"nik": "660542",
		"nama": "DIAN SUSANTO",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0006074903675237,
			"summary_team": 0.8
		}]
	},
	"660561": {
		"nik": "660561",
		"nama": "SUPARNO",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9801869293396148,
			"summary_team": 0.8
		}]
	},
	"660562": {
		"nik": "660562",
		"nama": "SUHAENDI",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ MERUYA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190469,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9652706843718089,
			"nilai_komparatif_unit": 0.9658570770146175,
			"summary_team": 0.8181818181818182
		}]
	},
	"660563": {
		"nik": "660563",
		"nama": "MOHAMAD RASID",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8291666666666665,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9648241206030154,
			"nilai_komparatif_unit": 0.965410241962636,
			"summary_team": 0.8
		}]
	},
	"660571": {
		"nik": "660571",
		"nama": "ARIEF HADIJAN",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777781,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9113924050632909,
			"nilai_komparatif_unit": 0.9119460671704009,
			"summary_team": 0.8
		}]
	},
	"660618": {
		"nik": "660618",
		"nama": "SUYANI",
		"band": "V",
		"posisi": "OFF 2 BGES ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8624999999999998,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8853754940711465,
			"nilai_komparatif_unit": 0.8859133511554361,
			"summary_team": 0.7636363636363637
		}]
	},
	"660641": {
		"nik": "660641",
		"nama": "ABDUL MUKRI",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0076047455449186,
			"summary_team": 0.8727272727272728
		}]
	},
	"670324": {
		"nik": "670324",
		"nama": "DEDIT PERMADI",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.021276595744681,
			"nilai_komparatif_unit": 1.0218970114391732,
			"summary_team": 0.8
		}]
	},
	"670368": {
		"nik": "670368",
		"nama": "DEDI ROSADI",
		"band": "V",
		"posisi": "OFF 2 PLASA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0446428571428572,
			"nilai_komparatif_unit": 1.0452774676160737,
			"summary_team": 0.9750000000000001
		}]
	},
	"670383": {
		"nik": "670383",
		"nama": "SUKARSA",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8060606060606053,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9924812030075199,
			"nilai_komparatif_unit": 0.9930841257782951,
			"summary_team": 0.8
		}]
	},
	"670385": {
		"nik": "670385",
		"nama": "SUSILO KRIDO HARTAWAN",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0006074903675237,
			"summary_team": 0.8
		}]
	},
	"670390": {
		"nik": "670390",
		"nama": "ALTAF TAZANI",
		"band": "V",
		"posisi": "OFF 2 PAYMENT COLLECTION CONSUMER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200685,
			"nilai_komparatif_unit": 1.0503804187048444,
			"summary_team": 0.9272727272727272
		}]
	},
	"670399": {
		"nik": "670399",
		"nama": "ROHMANI",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969697,
			"nilai_komparatif_unit": 0.9702860512654774,
			"summary_team": 0.8727272727272728
		}]
	},
	"670410": {
		"nik": "670410",
		"nama": "MOHAMAD SOLEH",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8616666666666657,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9917355371900839,
			"nilai_komparatif_unit": 0.9923380069760577,
			"summary_team": 0.8545454545454546
		}]
	},
	"670414": {
		"nik": "670414",
		"nama": "KASIDIN PUTRA",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ CENGKARENG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190469,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.943820224719102,
			"nilai_komparatif_unit": 0.9443935864142927,
			"summary_team": 0.8
		}]
	},
	"670415": {
		"nik": "670415",
		"nama": "MUBARAK BAHANAN",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190469,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9652706843718089,
			"nilai_komparatif_unit": 0.9658570770146175,
			"summary_team": 0.8181818181818182
		}]
	},
	"670433": {
		"nik": "670433",
		"nama": "AMIRUDDIN MASNUN",
		"band": "V",
		"posisi": "OFF 2 LOGISTIK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969694,
			"nilai_komparatif_unit": 0.9702860512654771,
			"summary_team": 0.8727272727272727
		}]
	},
	"670441": {
		"nik": "670441",
		"nama": "RUDY MULYADI",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8291666666666665,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9648241206030154,
			"nilai_komparatif_unit": 0.965410241962636,
			"summary_team": 0.8
		}]
	},
	"670463": {
		"nik": "670463",
		"nama": "ACHMAD KAILANI",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333318,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0506706408345774,
			"nilai_komparatif_unit": 1.0513089131283242,
			"summary_team": 0.8545454545454546
		}]
	},
	"670467": {
		"nik": "670467",
		"nama": "IRMAN",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT PLAN & RVW",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9674099485420238,
			"nilai_komparatif_unit": 0.9679976407672095,
			"summary_team": 0.8545454545454546
		}]
	},
	"670468": {
		"nik": "670468",
		"nama": "MAHMUDA",
		"band": "V",
		"posisi": "OFF 2 WHOLESALE & WIFI ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9545454545454547,
			"nilai_komparatif_unit": 0.9551253317144545,
			"summary_team": 0.7636363636363637
		}]
	},
	"670469": {
		"nik": "670469",
		"nama": "RASID ABDULLAH, S.E.",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0413223140495864,
			"nilai_komparatif_unit": 1.0419549073248588,
			"summary_team": 0.8909090909090909
		}]
	},
	"670475": {
		"nik": "670475",
		"nama": "YAN YAN SURYANA",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111115,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0979472140762458,
			"nilai_komparatif_unit": 1.0986142064328466,
			"summary_team": 0.9454545454545454
		}]
	},
	"670524": {
		"nik": "670524",
		"nama": "N. CHAERUDIN",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0369036903690365,
			"nilai_komparatif_unit": 1.0375335993729853,
			"summary_team": 0.8727272727272727
		}]
	},
	"670525": {
		"nik": "670525",
		"nama": "AGUS NUR ALAM",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9399999999999996,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.005802707930368,
			"nilai_komparatif_unit": 1.0064137233870647,
			"summary_team": 0.9454545454545454
		}]
	},
	"670527": {
		"nik": "670527",
		"nama": "SUKARDI",
		"band": "V",
		"posisi": "OFF 2 FINANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9923753665689147,
			"nilai_komparatif_unit": 0.9929782250450729,
			"summary_team": 0.8545454545454546
		}]
	},
	"670532": {
		"nik": "670532",
		"nama": "SUWARSANA",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559437,
			"nilai_komparatif_unit": 0.9446294489483611,
			"summary_team": 0.8181818181818182
		}]
	},
	"670535": {
		"nik": "670535",
		"nama": "MOHAMAD ABDI",
		"band": "V",
		"posisi": "OFF 2 CONS ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8523809523809511,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.002539360081261,
			"nilai_komparatif_unit": 1.0031483930855736,
			"summary_team": 0.8545454545454546
		}]
	},
	"670544": {
		"nik": "670544",
		"nama": "SYAMSUDIN",
		"band": "V",
		"posisi": "OFF 2 OM TRANSPORT, RADIO & MUX",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.0006074903675233,
			"summary_team": 0.8
		}]
	},
	"670545": {
		"nik": "670545",
		"nama": "YURDIANTO",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300698,
			"nilai_komparatif_unit": 1.0705800421414762,
			"summary_team": 0.9272727272727272
		}]
	},
	"670568": {
		"nik": "670568",
		"nama": "DEDI RUCHYADI",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9532215357458075,
			"nilai_komparatif_unit": 0.9538006086468891,
			"summary_team": 0.8181818181818182
		}]
	},
	"670571": {
		"nik": "670571",
		"nama": "NACHRAWI",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8624999999999998,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0750988142292492,
			"nilai_komparatif_unit": 1.0757519264030295,
			"summary_team": 0.9272727272727272
		}]
	},
	"670579": {
		"nik": "670579",
		"nama": "IMAN PRIHADI",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7416666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1521961184882532,
			"nilai_komparatif_unit": 1.152896066531733,
			"summary_team": 0.8545454545454545
		}]
	},
	"670584": {
		"nik": "670584",
		"nama": "RADEN MAS LENDI",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777781,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9942462600690446,
			"nilai_komparatif_unit": 0.9948502550949828,
			"summary_team": 0.8727272727272728
		}]
	},
	"670592": {
		"nik": "670592",
		"nama": "IMAM SUSILO",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7333333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0165289256198349,
			"nilai_komparatif_unit": 1.017146457150458,
			"summary_team": 0.7454545454545455
		}]
	},
	"670602": {
		"nik": "670602",
		"nama": "ACHMAD MAKMUR",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333318,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.983606557377051,
			"nilai_komparatif_unit": 0.9842040888860906,
			"summary_team": 0.8
		}]
	},
	"680189": {
		"nik": "680189",
		"nama": "MUHAMMAD YUSUF",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882353,
			"nilai_komparatif_unit": 0.9417482262282574,
			"summary_team": 0.8
		}]
	},
	"680205": {
		"nik": "680205",
		"nama": "DEDY SUPRIADI",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0018552875695732,
			"nilai_komparatif_unit": 1.0024639050064243,
			"summary_team": 0.8181818181818182
		}]
	},
	"680207": {
		"nik": "680207",
		"nama": "DWI HARTO",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT PLAN & RVW",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0695187165775402,
			"nilai_komparatif_unit": 1.070168438895747,
			"summary_team": 0.9090909090909092
		}]
	},
	"680212": {
		"nik": "680212",
		"nama": "ACHMAD NURACHIM",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7750000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0322580645161288,
			"nilai_komparatif_unit": 1.0328851513471209,
			"summary_team": 0.8
		}]
	},
	"680232": {
		"nik": "680232",
		"nama": "MOCH. ZEIN",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333318,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.983606557377051,
			"nilai_komparatif_unit": 0.9842040888860906,
			"summary_team": 0.8
		}]
	},
	"680247": {
		"nik": "680247",
		"nama": "SUAIB",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200687,
			"nilai_komparatif_unit": 1.0503804187048447,
			"summary_team": 0.9272727272727274
		}]
	},
	"680251": {
		"nik": "680251",
		"nama": "SUDAYA",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9494949494949495,
			"nilai_komparatif_unit": 0.95007175853078,
			"summary_team": 0.8545454545454546
		}]
	},
	"680261": {
		"nik": "680261",
		"nama": "EKO SUWANTO",
		"band": "V",
		"posisi": "OFF 2 SERVICE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.012987012987013,
			"nilai_komparatif_unit": 1.0136023928398292,
			"summary_team": 0.9454545454545454
		}]
	},
	"680262": {
		"nik": "680262",
		"nama": "AHMAD SAYUTI",
		"band": "V",
		"posisi": "OFF 2 BS DELIVERY & QUALITY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7166666666666665,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9894291754756874,
			"nilai_komparatif_unit": 0.9900302441691357,
			"summary_team": 0.7090909090909091
		}]
	},
	"680263": {
		"nik": "680263",
		"nama": "BACHTIAR",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8458333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9458128078817737,
			"nilai_komparatif_unit": 0.9463873800520423,
			"summary_team": 0.8
		}]
	},
	"680265": {
		"nik": "680265",
		"nama": "MINANDAR",
		"band": "V",
		"posisi": "OFF 2 PROJECT DELIVERY & SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8055555555555559,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9931034482758617,
			"nilai_komparatif_unit": 0.9937067490546437,
			"summary_team": 0.8
		}]
	},
	"680267": {
		"nik": "680267",
		"nama": "MURSID",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8138888888888882,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9829351535836186,
			"nilai_komparatif_unit": 0.983532277221321,
			"summary_team": 0.8
		}]
	},
	"680269": {
		"nik": "680269",
		"nama": "SANUANIH",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8138888888888882,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0499534595097744,
			"nilai_komparatif_unit": 1.0505912961227746,
			"summary_team": 0.8545454545454546
		}]
	},
	"680270": {
		"nik": "680270",
		"nama": "TASWANTO",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8291666666666665,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0525354042941983,
			"nilai_komparatif_unit": 1.0531748094137845,
			"summary_team": 0.8727272727272727
		}]
	},
	"680274": {
		"nik": "680274",
		"nama": "MATSANI",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9988193624557258,
			"nilai_komparatif_unit": 0.9994261355973136,
			"summary_team": 0.8545454545454546
		}]
	},
	"680275": {
		"nik": "680275",
		"nama": "MUHAMAD KARTONO",
		"band": "V",
		"posisi": "OFF 2 WHOLESALE & WIFI ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006993006993007,
			"nilai_komparatif_unit": 1.0076047455449189,
			"summary_team": 0.8727272727272728
		}]
	},
	"680276": {
		"nik": "680276",
		"nama": "SULARNO",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0686456400742113,
			"nilai_komparatif_unit": 1.0692948320068523,
			"summary_team": 0.8727272727272727
		}]
	},
	"680277": {
		"nik": "680277",
		"nama": "AGUS SUPRIHATIN",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.895238095238095,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0332446808510642,
			"nilai_komparatif_unit": 1.0338723670419763,
			"summary_team": 0.9250000000000002
		}]
	},
	"680278": {
		"nik": "680278",
		"nama": "AHMAD KHOLIDI",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333318,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0506706408345774,
			"nilai_komparatif_unit": 1.0513089131283242,
			"summary_team": 0.8545454545454546
		}]
	},
	"680280": {
		"nik": "680280",
		"nama": "SOLEH",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333318,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.983606557377051,
			"nilai_komparatif_unit": 0.9842040888860906,
			"summary_team": 0.8
		}]
	},
	"680282": {
		"nik": "680282",
		"nama": "SUPRIYANTO",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200687,
			"nilai_komparatif_unit": 1.0503804187048447,
			"summary_team": 0.9272727272727272
		}]
	},
	"680283": {
		"nik": "680283",
		"nama": "ANDRIANSYAH",
		"band": "V",
		"posisi": "OFF 2 BGES ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969695,
			"nilai_komparatif_unit": 0.9702860512654772,
			"summary_team": 0.8727272727272728
		}]
	},
	"680307": {
		"nik": "680307",
		"nama": "DIDI SETIAWAN, S.E",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.015301530153015,
			"nilai_komparatif_unit": 1.015918316052715,
			"summary_team": 0.8545454545454546
		}]
	},
	"680316": {
		"nik": "680316",
		"nama": "AHMAD SADIKIN",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.99558693733451,
			"nilai_komparatif_unit": 0.9961917468089729,
			"summary_team": 0.8545454545454546
		}]
	},
	"680317": {
		"nik": "680317",
		"nama": "INDRA BUDIMAN",
		"band": "V",
		"posisi": "OFF 2 WHOLESALE & WIFI ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8599999999999993,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0147991543340389,
			"nilai_komparatif_unit": 1.0154156350452679,
			"summary_team": 0.8727272727272727
		}]
	},
	"680318": {
		"nik": "680318",
		"nama": "KARYONO",
		"band": "V",
		"posisi": "OFF 2 CONS FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272725,
			"nilai_komparatif_unit": 1.0233485696940579,
			"summary_team": 0.8181818181818181
		}]
	},
	"680319": {
		"nik": "680319",
		"nama": "SUMARDJONO",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9532215357458075,
			"nilai_komparatif_unit": 0.9538006086468891,
			"summary_team": 0.8181818181818182
		}]
	},
	"680366": {
		"nik": "680366",
		"nama": "SAMSU NURCAHYA",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8060606060606053,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9924812030075199,
			"nilai_komparatif_unit": 0.9930841257782951,
			"summary_team": 0.8
		}]
	},
	"680371": {
		"nik": "680371",
		"nama": "SYAHRUDIN",
		"band": "V",
		"posisi": "OFF 2 DATA CENTER & CME",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999996,
			"nilai_komparatif_unit": 1.000607490367523,
			"summary_team": 0.8
		}]
	},
	"680378": {
		"nik": "680378",
		"nama": "DJUMARA SIMAMORA",
		"band": "V",
		"posisi": "OFF 2 SECURITY & SAFETY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0059583325620023,
			"summary_team": 0.8545454545454546
		}]
	},
	"680382": {
		"nik": "680382",
		"nama": "HANDOYO",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111115,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0134897360703807,
			"nilai_komparatif_unit": 1.0141054213226275,
			"summary_team": 0.8727272727272727
		}]
	},
	"680383": {
		"nik": "680383",
		"nama": "DJUANDA",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828873,
			"nilai_komparatif_unit": 1.0059583325620018,
			"summary_team": 0.8545454545454546
		}]
	},
	"680391": {
		"nik": "680391",
		"nama": "RAHMAN ARIF AMIN",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365347,
			"nilai_komparatif_unit": 0.988593335251618,
			"summary_team": 0.8727272727272727
		}]
	},
	"680395": {
		"nik": "680395",
		"nama": "SUKARMAN",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8138888888888882,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9829351535836186,
			"nilai_komparatif_unit": 0.983532277221321,
			"summary_team": 0.8
		}]
	},
	"680401": {
		"nik": "680401",
		"nama": "YAN RIADI",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8060606060606053,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9924812030075201,
			"nilai_komparatif_unit": 0.9930841257782953,
			"summary_team": 0.8000000000000003
		}]
	},
	"680406": {
		"nik": "680406",
		"nama": "DEDY BUDIYANTO, S.E",
		"band": "V",
		"posisi": "OFF 2 PROJECT DELIVERY & SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.877777777777778,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.014959723820483,
			"nilai_komparatif_unit": 1.0155763020761284,
			"summary_team": 0.8909090909090909
		}]
	},
	"680412": {
		"nik": "680412",
		"nama": "NUROHMAN",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999996,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882357,
			"nilai_komparatif_unit": 0.9417482262282578,
			"summary_team": 0.8
		}]
	},
	"680418": {
		"nik": "680418",
		"nama": "WAHID HASIM",
		"band": "V",
		"posisi": "OFF 2 PAYMENT COLLECTION EBIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9787595581988106,
			"nilai_komparatif_unit": 0.9793541452025379,
			"summary_team": 0.8727272727272728
		}]
	},
	"680441": {
		"nik": "680441",
		"nama": "EDI BAHRUDIN",
		"band": "V",
		"posisi": "OFF 2 SECURITY & SAFETY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.011961722488038,
			"nilai_komparatif_unit": 1.012576479486752,
			"summary_team": 0.8545454545454546
		}]
	},
	"680461": {
		"nik": "680461",
		"nama": "SUMEDI",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.01010101010101,
			"nilai_komparatif_unit": 1.010714636734872,
			"summary_team": 0.9090909090909092
		}]
	},
	"680466": {
		"nik": "680466",
		"nama": "EDHI CAHYONO",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8523809523809511,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.002539360081261,
			"nilai_komparatif_unit": 1.0031483930855736,
			"summary_team": 0.8545454545454546
		}]
	},
	"680468": {
		"nik": "680468",
		"nama": "MOHAMAD RUSMIN RIYADI",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0167696381288611,
			"nilai_komparatif_unit": 1.0173873158900149,
			"summary_team": 0.8727272727272727
		}]
	},
	"680472": {
		"nik": "680472",
		"nama": "ABDUL ROJAK",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444444,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0361842105263162,
			"nilai_komparatif_unit": 1.0368136824531908,
			"summary_team": 0.8750000000000002
		}]
	},
	"680475": {
		"nik": "680475",
		"nama": "MARSUDI",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9801869293396148,
			"summary_team": 0.8
		}]
	},
	"680478": {
		"nik": "680478",
		"nama": "WIWIT WALUYO",
		"band": "V",
		"posisi": "OFF 2 BGES ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197857,
			"nilai_komparatif_unit": 0.963151595006172,
			"summary_team": 0.8181818181818182
		}]
	},
	"680507": {
		"nik": "680507",
		"nama": "DINA AMELIA",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER RELATIONSHIP MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9801869293396148,
			"summary_team": 0.8
		}]
	},
	"680517": {
		"nik": "680517",
		"nama": "HERI RACHMANTO",
		"band": "V",
		"posisi": "OFF 2 COMMUNITY DEVELOPMENT CENTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0505050505050504,
			"nilai_komparatif_unit": 1.051143222204267,
			"summary_team": 0.9454545454545454
		}]
	},
	"680528": {
		"nik": "680528",
		"nama": "SAMSUL EFENDI",
		"band": "V",
		"posisi": "OFF 2 OM TRANSPORT, RADIO & MUX",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7750000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9618768328445745,
			"nilai_komparatif_unit": 0.9624611637552716,
			"summary_team": 0.7454545454545455
		}]
	},
	"680536": {
		"nik": "680536",
		"nama": "SUTRISNO",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333318,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0506706408345774,
			"nilai_komparatif_unit": 1.0513089131283242,
			"summary_team": 0.8545454545454546
		}]
	},
	"680537": {
		"nik": "680537",
		"nama": "PUJIANTO",
		"band": "V",
		"posisi": "OFF 2 BGES ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090904,
			"nilai_komparatif_unit": 1.0915718076736616,
			"summary_team": 0.9272727272727272
		}]
	},
	"680539": {
		"nik": "680539",
		"nama": "MULYADI",
		"band": "V",
		"posisi": "OFF 2 CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0085763293310464,
			"nilai_komparatif_unit": 1.0091890297360273,
			"summary_team": 0.8909090909090909
		}]
	},
	"680549": {
		"nik": "680549",
		"nama": "MUHAMMAD YANI",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.827777777777778,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0103721781574129,
			"nilai_komparatif_unit": 1.0109859695232573,
			"summary_team": 0.8363636363636363
		}]
	},
	"680560": {
		"nik": "680560",
		"nama": "SUDIARIS",
		"band": "V",
		"posisi": "OFF 2 DATA CENTER & CME",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9090909090909092,
			"nilai_komparatif_unit": 0.9096431730613851,
			"summary_team": 0.7272727272727273
		}]
	},
	"680566": {
		"nik": "680566",
		"nama": "AGUS SALIM",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8466666666666657,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9448818897637807,
			"nilai_komparatif_unit": 0.9454558964102596,
			"summary_team": 0.8
		}]
	},
	"680574": {
		"nik": "680574",
		"nama": "KUSWANTORO",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300698,
			"nilai_komparatif_unit": 1.0705800421414762,
			"summary_team": 0.9272727272727272
		}]
	},
	"690062": {
		"nik": "690062",
		"nama": "FAHRUL ROZI",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.001855287569573,
			"nilai_komparatif_unit": 1.0024639050064241,
			"summary_team": 0.8181818181818182
		}]
	},
	"690065": {
		"nik": "690065",
		"nama": "DEDI SUKMANA",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0006074903675237,
			"summary_team": 0.8
		}]
	},
	"690097": {
		"nik": "690097",
		"nama": "AGUS SUPRIADI",
		"band": "V",
		"posisi": "OFF 2 CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0703259005145798,
			"nilai_komparatif_unit": 1.0709761131892532,
			"summary_team": 0.9454545454545454
		}]
	},
	"690101": {
		"nik": "690101",
		"nama": "AL AZHAR",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ TEGAL ALUR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190469,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0081716036772226,
			"nilai_komparatif_unit": 1.0087840582152672,
			"summary_team": 0.8545454545454546
		}]
	},
	"690109": {
		"nik": "690109",
		"nama": "JOKO MARTONO",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8083333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0571696344892219,
			"nilai_komparatif_unit": 1.0578118548590125,
			"summary_team": 0.8545454545454546
		}]
	},
	"690114": {
		"nik": "690114",
		"nama": "GOIRUMAMNUN",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190469,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.943820224719102,
			"nilai_komparatif_unit": 0.9443935864142927,
			"summary_team": 0.8
		}]
	},
	"690129": {
		"nik": "690129",
		"nama": "PONCO RIANTO",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0481283422459895,
			"nilai_komparatif_unit": 1.0487650701178324,
			"summary_team": 0.890909090909091
		}]
	},
	"690132": {
		"nik": "690132",
		"nama": "RUDY RACHMAT",
		"band": "V",
		"posisi": "SPV PLASA CENGKARENG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8616666666666657,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0655881835765795,
			"nilai_komparatif_unit": 1.0662355181338492,
			"summary_team": 0.9181818181818182
		}]
	},
	"690181": {
		"nik": "690181",
		"nama": "SRI DJATMIKO",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ PALMERAH",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190469,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0081716036772226,
			"nilai_komparatif_unit": 1.0087840582152672,
			"summary_team": 0.8545454545454546
		}]
	},
	"690183": {
		"nik": "690183",
		"nama": "UNTUNG WIDODO",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0085763293310464,
			"nilai_komparatif_unit": 1.0091890297360273,
			"summary_team": 0.8909090909090909
		}]
	},
	"690201": {
		"nik": "690201",
		"nama": "ALEK",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8055555555555559,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9931034482758617,
			"nilai_komparatif_unit": 0.9937067490546437,
			"summary_team": 0.8
		}]
	},
	"690202": {
		"nik": "690202",
		"nama": "ANWAR MUSADAT",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8138888888888882,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0499534595097744,
			"nilai_komparatif_unit": 1.0505912961227746,
			"summary_team": 0.8545454545454546
		}]
	},
	"690204": {
		"nik": "690204",
		"nama": "SUGENG SUPRIYATNO",
		"band": "V",
		"posisi": "OFF 2 WHOLESALE & WIFI ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8599999999999993,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0782241014799163,
			"nilai_komparatif_unit": 1.0788791122355972,
			"summary_team": 0.9272727272727272
		}]
	},
	"690207": {
		"nik": "690207",
		"nama": "WIRAHADI",
		"band": "V",
		"posisi": "OFF 2 OM TRANSPORT, RADIO & MUX",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.0006074903675233,
			"summary_team": 0.8
		}]
	},
	"690208": {
		"nik": "690208",
		"nama": "YUSUF RACHMAWAN",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8616666666666657,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9284332688588018,
			"nilai_komparatif_unit": 0.928997283126522,
			"summary_team": 0.8
		}]
	},
	"690211": {
		"nik": "690211",
		"nama": "BUDI SETIAWAN",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200687,
			"nilai_komparatif_unit": 1.0503804187048447,
			"summary_team": 0.9272727272727274
		}]
	},
	"690214": {
		"nik": "690214",
		"nama": "MUHAMAD FIRDAUS",
		"band": "V",
		"posisi": "OFF 2 WHOLESALE & WIFI ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0714285714285718,
			"nilai_komparatif_unit": 1.072079453965204,
			"summary_team": 1.0000000000000002
		}]
	},
	"690219": {
		"nik": "690219",
		"nama": "AGUS WIDADA",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333318,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.983606557377051,
			"nilai_komparatif_unit": 0.9842040888860906,
			"summary_team": 0.8
		}]
	},
	"690220": {
		"nik": "690220",
		"nama": "RUDI HARTONO",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333318,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.983606557377051,
			"nilai_komparatif_unit": 0.9842040888860906,
			"summary_team": 0.8
		}]
	},
	"690246": {
		"nik": "690246",
		"nama": "SUPRIYADI",
		"band": "V",
		"posisi": "OFF 2 WHOLESALE & WIFI ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7555555555555554,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9264705882352945,
			"nilai_komparatif_unit": 0.9270334101934413,
			"summary_team": 0.7000000000000002
		}]
	},
	"690253": {
		"nik": "690253",
		"nama": "RUDI ANSHORUDIN",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111115,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.865689149560117,
			"nilai_komparatif_unit": 0.8662150473797444,
			"summary_team": 0.7454545454545455
		}]
	},
	"690255": {
		"nik": "690255",
		"nama": "SA`AT",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666665,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0018552875695734,
			"nilai_komparatif_unit": 1.0024639050064246,
			"summary_team": 0.8181818181818182
		}]
	},
	"690259": {
		"nik": "690259",
		"nama": "SULAEMAN",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9801869293396148,
			"summary_team": 0.8
		}]
	},
	"690261": {
		"nik": "690261",
		"nama": "ZAMRONI",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0279720279720281,
			"nilai_komparatif_unit": 1.0285965110771047,
			"summary_team": 0.8909090909090909
		}]
	},
	"690268": {
		"nik": "690268",
		"nama": "MUHAMAD ICHWAN",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365353,
			"nilai_komparatif_unit": 0.9885933352516185,
			"summary_team": 0.8727272727272727
		}]
	},
	"690317": {
		"nik": "690317",
		"nama": "ASRIZAL",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882352,
			"nilai_komparatif_unit": 0.9417482262282573,
			"summary_team": 0.8
		}]
	},
	"690325": {
		"nik": "690325",
		"nama": "MURSAN",
		"band": "V",
		"posisi": "OFF 2 WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8599999999999993,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0359408033826647,
			"nilai_komparatif_unit": 1.0365701274420442,
			"summary_team": 0.8909090909090909
		}]
	},
	"690332": {
		"nik": "690332",
		"nama": "WAHYUDIN",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ KEDOYA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190469,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9652706843718089,
			"nilai_komparatif_unit": 0.9658570770146175,
			"summary_team": 0.8181818181818182
		}]
	},
	"690341": {
		"nik": "690341",
		"nama": "EDDY IRAWAN",
		"band": "V",
		"posisi": "OFF 2 CONS FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8523809523809511,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.959878110716101,
			"nilai_komparatif_unit": 0.9604612274223577,
			"summary_team": 0.8181818181818182
		}]
	},
	"690350": {
		"nik": "690350",
		"nama": "NICO BINONKAN",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8791666666666664,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9306333476949594,
			"nilai_komparatif_unit": 0.9311986984893803,
			"summary_team": 0.8181818181818182
		}]
	},
	"690351": {
		"nik": "690351",
		"nama": "PRAYITNO",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8125,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9174825174825175,
			"nilai_komparatif_unit": 0.9180398792742593,
			"summary_team": 0.7454545454545455
		}]
	},
	"690358": {
		"nik": "690358",
		"nama": "TURASMAN",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ CIKINI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8523809523809511,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.109192483494161,
			"nilai_komparatif_unit": 1.1098663072436132,
			"summary_team": 0.9454545454545454
		}]
	},
	"690366": {
		"nik": "690366",
		"nama": "JAMALUDDIN",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9056603773584905,
			"nilai_komparatif_unit": 0.9062105573139835,
			"summary_team": 0.7999999999999999
		}]
	},
	"690415": {
		"nik": "690415",
		"nama": "MOMON RUDIANA",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8466666666666657,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9448818897637807,
			"nilai_komparatif_unit": 0.9454558964102596,
			"summary_team": 0.8
		}]
	},
	"690432": {
		"nik": "690432",
		"nama": "MUHAMAD",
		"band": "V",
		"posisi": "OFF 2 BGES ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197857,
			"nilai_komparatif_unit": 0.963151595006172,
			"summary_team": 0.8181818181818182
		}]
	},
	"690435": {
		"nik": "690435",
		"nama": "SAIPUDIN",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8791666666666664,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0340370529943994,
			"nilai_komparatif_unit": 1.0346652205437559,
			"summary_team": 0.9090909090909092
		}]
	},
	"690447": {
		"nik": "690447",
		"nama": "SUMIDI",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9399999999999996,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9864603481624763,
			"nilai_komparatif_unit": 0.9870596133219289,
			"summary_team": 0.9272727272727274
		}]
	},
	"690451": {
		"nik": "690451",
		"nama": "ZAINAL ARIF RIDHO",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ PALMERAH",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190469,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0081716036772226,
			"nilai_komparatif_unit": 1.0087840582152672,
			"summary_team": 0.8545454545454546
		}]
	},
	"690456": {
		"nik": "690456",
		"nama": "SUMIYANA, S.E",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ KEMAYORAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8523809523809511,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.959878110716101,
			"nilai_komparatif_unit": 0.9604612274223577,
			"summary_team": 0.8181818181818182
		}]
	},
	"690475": {
		"nik": "690475",
		"nama": "BORDON ARIADI",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8518518518518521,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.955900621118012,
			"nilai_komparatif_unit": 0.956481321537651,
			"summary_team": 0.8142857142857142
		}]
	},
	"690476": {
		"nik": "690476",
		"nama": "HARIFUDDIN",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8518518518518521,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9391304347826085,
			"nilai_komparatif_unit": 0.9397009474755871,
			"summary_team": 0.8
		}]
	},
	"690480": {
		"nik": "690480",
		"nama": "SUPRIHATIN",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7899999999999998,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0356731875719218,
			"nilai_komparatif_unit": 1.0363023490572743,
			"summary_team": 0.8181818181818181
		}]
	},
	"690493": {
		"nik": "690493",
		"nama": "DUDUNG SUPIYANI",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190469,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.943820224719102,
			"nilai_komparatif_unit": 0.9443935864142927,
			"summary_team": 0.8
		}]
	},
	"690495": {
		"nik": "690495",
		"nama": "SARASWATI SINEKAR",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8466666666666657,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.966356478167503,
			"nilai_komparatif_unit": 0.9669435304195838,
			"summary_team": 0.8181818181818182
		}]
	},
	"690499": {
		"nik": "690499",
		"nama": "HAIRUDIN IRWAN",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8125,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006993006993007,
			"nilai_komparatif_unit": 1.0076047455449189,
			"summary_team": 0.8181818181818182
		}]
	},
	"690511": {
		"nik": "690511",
		"nama": "SAMSUDIN",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8616666666666657,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9917355371900839,
			"nilai_komparatif_unit": 0.9923380069760577,
			"summary_team": 0.8545454545454546
		}]
	},
	"690512": {
		"nik": "690512",
		"nama": "DODY DIRGANTARA",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999996,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090915,
			"nilai_komparatif_unit": 1.0915718076736627,
			"summary_team": 0.9272727272727274
		}]
	},
	"690524": {
		"nik": "690524",
		"nama": "AHMAD SAIFI",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8125,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8951048951048951,
			"nilai_komparatif_unit": 0.8956486627065945,
			"summary_team": 0.7272727272727273
		}]
	},
	"690539": {
		"nik": "690539",
		"nama": "FARIF KUSWINDIARTO",
		"band": "V",
		"posisi": "OFF 2 NETWORK PERF, QM & ADMIN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.015301530153015,
			"nilai_komparatif_unit": 1.015918316052715,
			"summary_team": 0.8545454545454545
		}]
	},
	"690549": {
		"nik": "690549",
		"nama": "SISWO TRIARSO",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT PLAN & RVW",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999999,
			"nilai_komparatif_unit": 1.0006074903675235,
			"summary_team": 0.8
		}]
	},
	"690561": {
		"nik": "690561",
		"nama": "ENCENG RAHMAT HIDAYAT",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ CIDENG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0006074903675237,
			"summary_team": 0.8000000000000003
		}]
	},
	"690563": {
		"nik": "690563",
		"nama": "AGUS BASUKI",
		"band": "V",
		"posisi": "OFF 2 SECURITY & SAFETY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9935064935064933,
			"nilai_komparatif_unit": 0.9941100391313706,
			"summary_team": 0.9272727272727272
		}]
	},
	"690566": {
		"nik": "690566",
		"nama": "BASUKI WIDODO",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7899999999999998,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.989643268124281,
			"nilai_komparatif_unit": 0.9902444668769511,
			"summary_team": 0.7818181818181819
		}]
	},
	"690573": {
		"nik": "690573",
		"nama": "DEDI SUKANDI",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ CEMPAKA PUTIH",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.027361701339917,
			"summary_team": 0.8727272727272728
		}]
	},
	"690576": {
		"nik": "690576",
		"nama": "BAMBANG SUWARDIYANTO",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0399320305862358,
			"nilai_komparatif_unit": 1.0405637792776963,
			"summary_team": 0.9272727272727272
		}]
	},
	"690583": {
		"nik": "690583",
		"nama": "NURHASAN",
		"band": "V",
		"posisi": "OFF 2 CONSUMER ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0006074903675237,
			"summary_team": 0.8
		}]
	},
	"700042": {
		"nik": "700042",
		"nama": "ANDRIAWAN",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8060606060606053,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9924812030075199,
			"nilai_komparatif_unit": 0.9930841257782951,
			"summary_team": 0.8
		}]
	},
	"700048": {
		"nik": "700048",
		"nama": "SARWANDI",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190469,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9652706843718089,
			"nilai_komparatif_unit": 0.9658570770146175,
			"summary_team": 0.8181818181818182
		}]
	},
	"700057": {
		"nik": "700057",
		"nama": "NURDAYANTO",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828873,
			"nilai_komparatif_unit": 1.0059583325620018,
			"summary_team": 0.8545454545454546
		}]
	},
	"700066": {
		"nik": "700066",
		"nama": "SELAMET HARIYANTO",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969694,
			"nilai_komparatif_unit": 0.9702860512654771,
			"summary_team": 0.8727272727272727
		}]
	},
	"700069": {
		"nik": "700069",
		"nama": "SAMSUL IMRON",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7899999999999998,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0356731875719218,
			"nilai_komparatif_unit": 1.0363023490572743,
			"summary_team": 0.8181818181818181
		}]
	},
	"700070": {
		"nik": "700070",
		"nama": "ABU JAZID BISTHOMI",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0006074903675237,
			"summary_team": 0.8
		}]
	},
	"700109": {
		"nik": "700109",
		"nama": "SOPYAN",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7899999999999998,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0126582278481016,
			"nilai_komparatif_unit": 1.0132734079671126,
			"summary_team": 0.8
		}]
	},
	"700125": {
		"nik": "700125",
		"nama": "AGUSMAWAN IMAN RAHARDJO",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8466666666666657,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0307802433786697,
			"nilai_komparatif_unit": 1.0314064324475558,
			"summary_team": 0.8727272727272727
		}]
	},
	"700127": {
		"nik": "700127",
		"nama": "AHMAD SUWANDI",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8791666666666664,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0753985351141753,
			"nilai_komparatif_unit": 1.0760518293655061,
			"summary_team": 0.9454545454545454
		}]
	},
	"700131": {
		"nik": "700131",
		"nama": "ENDARIS TAYASA",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT PLAN & RVW",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9898989898989898,
			"nilai_komparatif_unit": 0.9905003440001747,
			"summary_team": 0.8909090909090909
		}]
	},
	"700133": {
		"nik": "700133",
		"nama": "HARTONO",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.027361701339917,
			"summary_team": 0.8727272727272728
		}]
	},
	"700152": {
		"nik": "700152",
		"nama": "SUTANTO",
		"band": "V",
		"posisi": "OFF 2 WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0006074903675237,
			"summary_team": 0.8
		}]
	},
	"700154": {
		"nik": "700154",
		"nama": "TUGIMIN",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.99558693733451,
			"nilai_komparatif_unit": 0.9961917468089729,
			"summary_team": 0.8545454545454546
		}]
	},
	"700169": {
		"nik": "700169",
		"nama": "KASYANTO",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9952153110047846,
			"nilai_komparatif_unit": 0.9958198947198319,
			"summary_team": 0.9454545454545454
		}]
	},
	"700175": {
		"nik": "700175",
		"nama": "ADI SUTJIPTO",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8458333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0317957904164803,
			"nilai_komparatif_unit": 1.0324225964204097,
			"summary_team": 0.8727272727272727
		}]
	},
	"700176": {
		"nik": "700176",
		"nama": "ARIF SOFIAN",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8083333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0571696344892219,
			"nilai_komparatif_unit": 1.0578118548590125,
			"summary_team": 0.8545454545454546
		}]
	},
	"700180": {
		"nik": "700180",
		"nama": "MUSTANIR",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8055555555555559,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9931034482758617,
			"nilai_komparatif_unit": 0.9937067490546437,
			"summary_team": 0.8
		}]
	},
	"700181": {
		"nik": "700181",
		"nama": "SANARUL KAMAL",
		"band": "V",
		"posisi": "OFF 2 HUMAN CAPITAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365353,
			"nilai_komparatif_unit": 0.9885933352516185,
			"summary_team": 0.8727272727272728
		}]
	},
	"700188": {
		"nik": "700188",
		"nama": "SUHERI",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9801869293396148,
			"summary_team": 0.8
		}]
	},
	"700191": {
		"nik": "700191",
		"nama": "AYI RAHMAD AMIN",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0379523389232124,
			"nilai_komparatif_unit": 1.0385828849710568,
			"summary_team": 0.8909090909090909
		}]
	},
	"700194": {
		"nik": "700194",
		"nama": "HARSONO",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200687,
			"nilai_komparatif_unit": 1.0503804187048447,
			"summary_team": 0.9272727272727272
		}]
	},
	"700195": {
		"nik": "700195",
		"nama": "NANA SUKARNA",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.827777777777778,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9444783404514945,
			"nilai_komparatif_unit": 0.9450521019456534,
			"summary_team": 0.7818181818181817
		}]
	},
	"700196": {
		"nik": "700196",
		"nama": "SRI MAULANA",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.967409948542024,
			"nilai_komparatif_unit": 0.9679976407672097,
			"summary_team": 0.8545454545454546
		}]
	},
	"700215": {
		"nik": "700215",
		"nama": "BAJURI",
		"band": "V",
		"posisi": "OFF 2 SERVICE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8616666666666657,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9917355371900839,
			"nilai_komparatif_unit": 0.9923380069760577,
			"summary_team": 0.8545454545454546
		}]
	},
	"700219": {
		"nik": "700219",
		"nama": "CECEP SUDRAJAT",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8138888888888882,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9159168476574627,
			"nilai_komparatif_unit": 0.9164732583198671,
			"summary_team": 0.7454545454545455
		}]
	},
	"700232": {
		"nik": "700232",
		"nama": "SUPRIYANTO",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8458333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9673085535154504,
			"nilai_komparatif_unit": 0.9678961841441341,
			"summary_team": 0.8181818181818182
		}]
	},
	"700249": {
		"nik": "700249",
		"nama": "HADI WIJAYA",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777781,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9735327963176061,
			"nilai_komparatif_unit": 0.9741242081138374,
			"summary_team": 0.8545454545454546
		}]
	},
	"700252": {
		"nik": "700252",
		"nama": "SETIADINOTO",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT PLAN & RVW",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828875,
			"nilai_komparatif_unit": 1.005958332562002,
			"summary_team": 0.8545454545454545
		}]
	},
	"700255": {
		"nik": "700255",
		"nama": "MUSLIM",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.827777777777778,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0543014032946918,
			"nilai_komparatif_unit": 1.0549418812416598,
			"summary_team": 0.8727272727272728
		}]
	},
	"700314": {
		"nik": "700314",
		"nama": "ABDUL ROJAK",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ PIK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190469,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0081716036772226,
			"nilai_komparatif_unit": 1.0087840582152672,
			"summary_team": 0.8545454545454546
		}]
	},
	"700320": {
		"nik": "700320",
		"nama": "CECEP EDI SUGANDI",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8616666666666657,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9495340249692292,
			"nilai_komparatif_unit": 0.9501108577430338,
			"summary_team": 0.8181818181818182
		}]
	},
	"700327": {
		"nik": "700327",
		"nama": "IRFAN ARIF HERMAWAN",
		"band": "V",
		"posisi": "OFF 2 WHOLESALE & WIFI ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0076047455449186,
			"summary_team": 0.8727272727272727
		}]
	},
	"700341": {
		"nik": "700341",
		"nama": "SYAMSURI",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7666666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9960474308300397,
			"nilai_komparatif_unit": 0.9966525200498655,
			"summary_team": 0.7636363636363637
		}]
	},
	"700354": {
		"nik": "700354",
		"nama": "EVI HENDRATNA",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9801869293396148,
			"summary_team": 0.8
		}]
	},
	"700361": {
		"nik": "700361",
		"nama": "INDRIYUDO",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8250000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969694,
			"nilai_komparatif_unit": 0.9702860512654771,
			"summary_team": 0.8
		}]
	},
	"700374": {
		"nik": "700374",
		"nama": "SUPARI",
		"band": "V",
		"posisi": "OFF 2 BGES ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.877777777777778,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9942462600690447,
			"nilai_komparatif_unit": 0.9948502550949829,
			"summary_team": 0.8727272727272728
		}]
	},
	"700377": {
		"nik": "700377",
		"nama": "SURYA, S.E.",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0279720279720281,
			"nilai_komparatif_unit": 1.0285965110771047,
			"summary_team": 0.8909090909090909
		}]
	},
	"700378": {
		"nik": "700378",
		"nama": "TAUFIK",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8458333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0317957904164805,
			"nilai_komparatif_unit": 1.03242259642041,
			"summary_team": 0.8727272727272728
		}]
	},
	"700380": {
		"nik": "700380",
		"nama": "WAWAN",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999996,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882357,
			"nilai_komparatif_unit": 0.9417482262282578,
			"summary_team": 0.8
		}]
	},
	"700395": {
		"nik": "700395",
		"nama": "MUHAMMAD BASIR",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200687,
			"nilai_komparatif_unit": 1.0503804187048447,
			"summary_team": 0.9272727272727274
		}]
	},
	"700397": {
		"nik": "700397",
		"nama": "ROZY ISKANDAR",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8125,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.051748251748252,
			"nilai_komparatif_unit": 1.0523871786802488,
			"summary_team": 0.8545454545454546
		}]
	},
	"700403": {
		"nik": "700403",
		"nama": "MAHYUDIN",
		"band": "V",
		"posisi": "OFF 2 HUMAN CAPITAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200685,
			"nilai_komparatif_unit": 1.0503804187048444,
			"summary_team": 0.9272727272727272
		}]
	},
	"700467": {
		"nik": "700467",
		"nama": "JANWAR SADLY PANGGABEAN",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999996,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197866,
			"nilai_komparatif_unit": 0.9631515950061729,
			"summary_team": 0.8181818181818182
		}]
	},
	"700473": {
		"nik": "700473",
		"nama": "INDRA HERYUDI",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.027972027972028,
			"nilai_komparatif_unit": 1.0285965110771045,
			"summary_team": 0.8909090909090909
		}]
	},
	"700488": {
		"nik": "700488",
		"nama": "TARMUJI",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8624999999999998,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0118577075098818,
			"nilai_komparatif_unit": 1.0124724013204984,
			"summary_team": 0.8727272727272728
		}]
	},
	"700490": {
		"nik": "700490",
		"nama": "HASANUDDIN",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8060606060606063,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.082706766917293,
			"nilai_komparatif_unit": 1.0833645008490478,
			"summary_team": 0.8727272727272727
		}]
	},
	"700492": {
		"nik": "700492",
		"nama": "SUGENG WALUYO",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8250000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0358126721763083,
			"nilai_komparatif_unit": 1.0364419183972142,
			"summary_team": 0.8545454545454546
		}]
	},
	"700495": {
		"nik": "700495",
		"nama": "RACHMAD KUSNADI",
		"band": "V",
		"posisi": "OFF 2 LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9494949494949493,
			"nilai_komparatif_unit": 0.9500717585307797,
			"summary_team": 0.8545454545454546
		}]
	},
	"700503": {
		"nik": "700503",
		"nama": "EKO PRAYITNO",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8060606060606063,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1475563909774436,
			"nilai_komparatif_unit": 1.1482535204311524,
			"summary_team": 0.9250000000000002
		}]
	},
	"700511": {
		"nik": "700511",
		"nama": "GUSGINANTO",
		"band": "V",
		"posisi": "OFF 2 IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9486166007905136,
			"nilai_komparatif_unit": 0.9491928762379668,
			"summary_team": 0.7272727272727273
		}]
	},
	"700512": {
		"nik": "700512",
		"nama": "ARRY ERLANGGA BIMA SAKTI",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9262435677530018,
			"nilai_komparatif_unit": 0.9268062517983924,
			"summary_team": 0.8181818181818182
		}]
	},
	"700521": {
		"nik": "700521",
		"nama": "SUPARDI",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300696,
			"nilai_komparatif_unit": 1.070580042141476,
			"summary_team": 0.9272727272727272
		}]
	},
	"700527": {
		"nik": "700527",
		"nama": "SANUSI",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8060606060606053,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9924812030075199,
			"nilai_komparatif_unit": 0.9930841257782951,
			"summary_team": 0.8
		}]
	},
	"700539": {
		"nik": "700539",
		"nama": "SARIBAN",
		"band": "V",
		"posisi": "OFF 2 SERVICE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8616666666666657,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9917355371900839,
			"nilai_komparatif_unit": 0.9923380069760577,
			"summary_team": 0.8545454545454546
		}]
	},
	"700568": {
		"nik": "700568",
		"nama": "MOHAMAD ANTON SU'UDI, S.E",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ GAMBIR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8523809523809511,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.938547486033521,
			"nilai_komparatif_unit": 0.9391176445907498,
			"summary_team": 0.8
		}]
	},
	"700589": {
		"nik": "700589",
		"nama": "WAHYUNI MUSTAFA",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8291666666666665,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1183188670625859,
			"nilai_komparatif_unit": 1.1189982350021461,
			"summary_team": 0.9272727272727272
		}]
	},
	"700592": {
		"nik": "700592",
		"nama": "SANTA BR GURUSINGA",
		"band": "V",
		"posisi": "OFF 2 SERVICE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9952153110047846,
			"nilai_komparatif_unit": 0.9958198947198319,
			"summary_team": 0.9454545454545454
		}]
	},
	"700595": {
		"nik": "700595",
		"nama": "ONI SUKOCO, S.E",
		"band": "V",
		"posisi": "OFF 2 DEBT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0059583325620023,
			"summary_team": 0.8545454545454546
		}]
	},
	"700598": {
		"nik": "700598",
		"nama": "MARIJO",
		"band": "V",
		"posisi": "OFF 2 DATA CENTER & CME",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999996,
			"nilai_komparatif_unit": 1.000607490367523,
			"summary_team": 0.8
		}]
	},
	"700605": {
		"nik": "700605",
		"nama": "MA'MUN",
		"band": "V",
		"posisi": "OFF 2 DATA CENTER & CME",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0076047455449186,
			"summary_team": 0.8727272727272727
		}]
	},
	"700606": {
		"nik": "700606",
		"nama": "AHMAD FAHMI",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT PLAN & RVW",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.090909090909091,
			"nilai_komparatif_unit": 1.0915718076736622,
			"summary_team": 0.8727272727272727
		}]
	},
	"700618": {
		"nik": "700618",
		"nama": "AGUS HARIYANTO",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9318181818181819,
			"nilai_komparatif_unit": 0.9323842523879197,
			"summary_team": 0.7454545454545455
		}]
	},
	"700620": {
		"nik": "700620",
		"nama": "DENI ERYAWAN",
		"band": "V",
		"posisi": "OFF 2 OM TRANSPORT, RADIO & MUX",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0059583325620023,
			"summary_team": 0.8545454545454546
		}]
	},
	"700622": {
		"nik": "700622",
		"nama": "NURDIAHWATI",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190469,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9652706843718089,
			"nilai_komparatif_unit": 0.9658570770146175,
			"summary_team": 0.8181818181818182
		}]
	},
	"700634": {
		"nik": "700634",
		"nama": "SARIF SUKOTJO",
		"band": "V",
		"posisi": "OFF 2 BS TERRITORY SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969697,
			"nilai_komparatif_unit": 0.9702860512654774,
			"summary_team": 0.8727272727272728
		}]
	},
	"710034": {
		"nik": "710034",
		"nama": "HERMAN",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9399999999999996,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9284332688588012,
			"nilai_komparatif_unit": 0.9289972831265213,
			"summary_team": 0.8727272727272728
		}]
	},
	"710035": {
		"nik": "710035",
		"nama": "ISTIKMAL",
		"band": "V",
		"posisi": "OFF 2 NETWORK PERF, QM & ADMIN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1146245059288535,
			"nilai_komparatif_unit": 1.115301629579611,
			"summary_team": 0.8545454545454546
		}]
	},
	"710040": {
		"nik": "710040",
		"nama": "MUHAMAD FAOZAN",
		"band": "V",
		"posisi": "OFF 2 COMMUNICATION & SECRETARIATE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8466666666666657,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.052254831782392,
			"nilai_komparatif_unit": 1.05289406645688,
			"summary_team": 0.8909090909090909
		}]
	},
	"710044": {
		"nik": "710044",
		"nama": "PURWO JATMIKO",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333318,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.983606557377051,
			"nilai_komparatif_unit": 0.9842040888860906,
			"summary_team": 0.8
		}]
	},
	"710080": {
		"nik": "710080",
		"nama": "NAWAN GUNAWAN",
		"band": "V",
		"posisi": "OFF 2 BGES ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8624999999999998,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9275362318840582,
			"nilai_komparatif_unit": 0.9280997012104568,
			"summary_team": 0.8
		}]
	},
	"710086": {
		"nik": "710086",
		"nama": "ACHMAD MAULANA",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7833333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.044487427466151,
			"nilai_komparatif_unit": 1.0451219435173362,
			"summary_team": 0.8181818181818182
		}]
	},
	"710087": {
		"nik": "710087",
		"nama": "BUCE AFRIADY. R.",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.027361701339917,
			"summary_team": 0.8727272727272727
		}]
	},
	"710090": {
		"nik": "710090",
		"nama": "MUHAMMAD DAHLAN",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9399999999999996,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.005802707930368,
			"nilai_komparatif_unit": 1.0064137233870647,
			"summary_team": 0.9454545454545454
		}]
	},
	"710108": {
		"nik": "710108",
		"nama": "SUPARTONO",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0369036903690367,
			"nilai_komparatif_unit": 1.0375335993729855,
			"summary_team": 0.8727272727272728
		}]
	},
	"710168": {
		"nik": "710168",
		"nama": "ASEP RUHIYAT",
		"band": "V",
		"posisi": "OFF 2 DATA CENTER & CME",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999996,
			"nilai_komparatif_unit": 1.000607490367523,
			"summary_team": 0.8
		}]
	},
	"710171": {
		"nik": "710171",
		"nama": "JAMALUDIN",
		"band": "V",
		"posisi": "OFF 2 PAYMENT COLLECTION CONSUMER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.022727272727273,
			"nilai_komparatif_unit": 1.0233485696940583,
			"summary_team": 0.8181818181818182
		}]
	},
	"710176": {
		"nik": "710176",
		"nama": "SYAMSUDIN",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666665,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0018552875695734,
			"nilai_komparatif_unit": 1.0024639050064246,
			"summary_team": 0.8181818181818182
		}]
	},
	"710180": {
		"nik": "710180",
		"nama": "EDI DJUNAEDI",
		"band": "V",
		"posisi": "OFF 2 IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7750000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0322580645161288,
			"nilai_komparatif_unit": 1.0328851513471209,
			"summary_team": 0.8
		}]
	},
	"710183": {
		"nik": "710183",
		"nama": "HAMZAH",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9399999999999996,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.005802707930368,
			"nilai_komparatif_unit": 1.0064137233870647,
			"summary_team": 0.9454545454545454
		}]
	},
	"710197": {
		"nik": "710197",
		"nama": "DJOKO SABARNO",
		"band": "V",
		"posisi": "OFF 2 IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9801869293396148,
			"summary_team": 0.8
		}]
	},
	"710236": {
		"nik": "710236",
		"nama": "ARISANTI KRISNA YUNIARSIH",
		"band": "V",
		"posisi": "OFF 2 WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.967409948542024,
			"nilai_komparatif_unit": 0.9679976407672097,
			"summary_team": 0.8545454545454546
		}]
	},
	"710257": {
		"nik": "710257",
		"nama": "ADI SULISTIONO",
		"band": "V",
		"posisi": "OFF 2 CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.65,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.979020979020979,
			"nilai_komparatif_unit": 0.9796157248353377,
			"summary_team": 0.6363636363636364
		}]
	},
	"710297": {
		"nik": "710297",
		"nama": "BAMBANG SUHERMANTO",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7750000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9618768328445745,
			"nilai_komparatif_unit": 0.9624611637552716,
			"summary_team": 0.7454545454545455
		}]
	},
	"710303": {
		"nik": "710303",
		"nama": "ISNY MUSTOFA",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1114922813036021,
			"nilai_komparatif_unit": 1.1121675021580708,
			"summary_team": 0.9818181818181818
		}]
	},
	"710339": {
		"nik": "710339",
		"nama": "JUNAEDI",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.895238095238095,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0890957446808514,
			"nilai_komparatif_unit": 1.0897573598550558,
			"summary_team": 0.9750000000000001
		}]
	},
	"710344": {
		"nik": "710344",
		"nama": "EKO HENDARTO",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.027361701339917,
			"summary_team": 0.8727272727272728
		}]
	},
	"890052": {
		"nik": "890052",
		"nama": "KRISWARDANI SAPTANINGTIAS",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.031404958677686,
			"nilai_komparatif_unit": 1.0320315272550986,
			"summary_team": 0.9454545454545454
		}]
	},
	"890063": {
		"nik": "890063",
		"nama": "HAYA WARDA WAHYUNI",
		"band": "V",
		"posisi": "OFF 2 BGES BIDDING MGT & OBL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7333333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0165289256198349,
			"nilai_komparatif_unit": 1.017146457150458,
			"summary_team": 0.7454545454545455
		}]
	},
	"900145": {
		"nik": "900145",
		"nama": "DESTY DIANTI HAPSARI",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8616666666666657,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.012836293300511,
			"nilai_komparatif_unit": 1.0134515815925693,
			"summary_team": 0.8727272727272728
		}]
	},
	"910217": {
		"nik": "910217",
		"nama": "ANTONY WICAKSONO",
		"band": "V",
		"posisi": "OFF 2 NETWORK PROJECT SUPERVISION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8388888888888882,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.040337146297412,
			"nilai_komparatif_unit": 1.0409691410927646,
			"summary_team": 0.8727272727272728
		}]
	},
	"920227": {
		"nik": "920227",
		"nama": "NURUL ISMAYA",
		"band": "V",
		"posisi": "OFF 2 DEBT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.986013986013986,
			"nilai_komparatif_unit": 0.986612980012733,
			"summary_team": 0.8545454545454546
		}]
	},
	"920271": {
		"nik": "920271",
		"nama": "CAROLUS FERDY SETIAJI HARTOKO",
		"band": "V",
		"posisi": "OFF 2 DATA VALIDATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9320388349514567,
			"nilai_komparatif_unit": 0.9326050395658475,
			"summary_team": 0.8
		}]
	},
	"920287": {
		"nik": "920287",
		"nama": "ALVIN ALBERTHA DWI ATMAJA",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE LEGOK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.853846153846153,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0008190008190019,
			"nilai_komparatif_unit": 1.001426988721634,
			"summary_team": 0.8545454545454546
		}]
	},
	"920289": {
		"nik": "920289",
		"nama": "MAYANG ANGGRAINI",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9007506255212678,
			"nilai_komparatif_unit": 0.9012978228498129,
			"summary_team": 0.8181818181818182
		}]
	},
	"920314": {
		"nik": "920314",
		"nama": "DILIA PERMATA SARI",
		"band": "V",
		"posisi": "OFF 2 LOGISTIK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7416666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9315628192032688,
			"nilai_komparatif_unit": 0.9321287346426779,
			"summary_team": 0.6909090909090909
		}]
	},
	"920345": {
		"nik": "920345",
		"nama": "MUHAMAD IQBAL RAMDANI",
		"band": "V",
		"posisi": "OFF 2 BS ACCOUNT PLAN & TEAM MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828875,
			"nilai_komparatif_unit": 1.005958332562002,
			"summary_team": 0.8545454545454546
		}]
	},
	"926119": {
		"nik": "926119",
		"nama": "FERAYANTI BR GINTING",
		"band": "V",
		"posisi": "ACCOUNT MANAGER-1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7948717948717949,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8920821114369503,
			"nilai_komparatif_unit": 0.8926240427266883,
			"summary_team": 0.7090909090909092
		}]
	},
	"930152": {
		"nik": "930152",
		"nama": "ANNISA UTAMI",
		"band": "V",
		"posisi": "OFF 2 DIGITAL SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0115702479338842,
			"nilai_komparatif_unit": 1.0121847671155775,
			"summary_team": 0.9272727272727272
		}]
	},
	"930200": {
		"nik": "930200",
		"nama": "HADYAN ARIF",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CIPUTAT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8719298245614022,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.917505030181088,
			"nilai_komparatif_unit": 0.9180624056490774,
			"summary_team": 0.8
		}]
	},
	"930203": {
		"nik": "930203",
		"nama": "GIRINDRA CHANDRA ALAM",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333339,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9588068181818176,
			"nilai_komparatif_unit": 0.9593892840881789,
			"summary_team": 0.8181818181818182
		}]
	},
	"930206": {
		"nik": "930206",
		"nama": "ANNISA RAHMANIA",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9520661157024795,
			"nilai_komparatif_unit": 0.9526444866970143,
			"summary_team": 0.8727272727272728
		}]
	},
	"930226": {
		"nik": "930226",
		"nama": "HIDAYAT AKBAR",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE TRG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666656,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769242,
			"nilai_komparatif_unit": 0.9236376834161767,
			"summary_team": 0.8
		}]
	},
	"930267": {
		"nik": "930267",
		"nama": "MAHARDIANI BENINGRUM",
		"band": "V",
		"posisi": "ACCOUNT MANAGER-1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7948717948717949,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0979472140762463,
			"nilai_komparatif_unit": 1.098614206432847,
			"summary_team": 0.8727272727272728
		}]
	},
	"930318": {
		"nik": "930318",
		"nama": "MUHAMMAD ILHAM MAULANA KARIM PRIATNA",
		"band": "V",
		"posisi": "OFF 2 WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365353,
			"nilai_komparatif_unit": 0.9885933352516185,
			"summary_team": 0.8727272727272728
		}]
	},
	"930326": {
		"nik": "930326",
		"nama": "ADEG ZATNIKABUMI",
		"band": "V",
		"posisi": "OFF 2 SALES PROMOTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.846666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9744094488188975,
			"nilai_komparatif_unit": 0.9750013931730789,
			"summary_team": 0.8250000000000002
		}]
	},
	"930333": {
		"nik": "930333",
		"nama": "JOHANES NICOLAS PANJAITAN",
		"band": "V",
		"posisi": "OFF 2 CAPEX ENTERPRISE & OLO MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666663,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9583687340696692,
			"nilai_komparatif_unit": 0.9589509338441522,
			"summary_team": 0.8545454545454546
		}]
	},
	"930363": {
		"nik": "930363",
		"nama": "SARAH RIZA SEIZA AMANDA",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP MARKETING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8083333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9222118088097468,
			"nilai_komparatif_unit": 0.9227720436004152,
			"summary_team": 0.7454545454545455
		}]
	},
	"940049": {
		"nik": "940049",
		"nama": "SILMINA FARHANI KOMALIN",
		"band": "V",
		"posisi": "OFF 2 WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9898989898989898,
			"nilai_komparatif_unit": 0.9905003440001747,
			"summary_team": 0.8909090909090909
		}]
	},
	"940050": {
		"nik": "940050",
		"nama": "AUZAN HILMAN HUSTANTO",
		"band": "V",
		"posisi": "OFF 2 SALES & BIDDING SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333329,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9565807327001362,
			"nilai_komparatif_unit": 0.9571618462810102,
			"summary_team": 0.8545454545454546
		}]
	},
	"940071": {
		"nik": "940071",
		"nama": "KALAM ADHIANSYAH LUTFIE",
		"band": "V",
		"posisi": "OFF 2 IP NODE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8716666666666656,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9177820267686436,
			"nilai_komparatif_unit": 0.9183395705093917,
			"summary_team": 0.8
		}]
	},
	"940102": {
		"nik": "940102",
		"nama": "MOHAMMAD SYAHID WILDAN",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE PD AREN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8719298245614022,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.042619352478509,
			"nilai_komparatif_unit": 1.0432527336921333,
			"summary_team": 0.9090909090909091
		}]
	},
	"940105": {
		"nik": "940105",
		"nama": "GHASSANI HERNING PRADITA",
		"band": "V",
		"posisi": "OFF 2 WALK IN CHANNEL OPERATION & PARTNE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0389610389610389,
			"nilai_komparatif_unit": 1.03959219778444,
			"summary_team": 0.9090909090909092
		}]
	},
	"940109": {
		"nik": "940109",
		"nama": "ANINDITYA WAHYU ADI",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE JAGAKARSA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8676190476190493,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0268436283803992,
			"nilai_komparatif_unit": 1.0274674259935932,
			"summary_team": 0.8909090909090909
		}]
	},
	"940112": {
		"nik": "940112",
		"nama": "RAHMA ORYZA NABALA",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER DATA MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0303030303030303,
			"nilai_komparatif_unit": 1.0309289294695696,
			"summary_team": 0.9272727272727272
		}]
	},
	"940114": {
		"nik": "940114",
		"nama": "ADITYA HERDIYAN PRATAMA",
		"band": "V",
		"posisi": "OFF 2 BACKBONE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.870270270270269,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0028232636928305,
			"nilai_komparatif_unit": 1.0034324691658525,
			"summary_team": 0.8727272727272728
		}]
	},
	"940119": {
		"nik": "940119",
		"nama": "MUHAMMAD ABDUL AZIS",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.962566844919786,
			"nilai_komparatif_unit": 0.9631515950061722,
			"summary_team": 0.8181818181818182
		}]
	},
	"940168": {
		"nik": "940168",
		"nama": "SARAH ZANIAR MAJID",
		"band": "V",
		"posisi": "OFF 2 PUBLIC RELATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8755555555555555,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9760036917397324,
			"nilai_komparatif_unit": 0.9765966045811317,
			"summary_team": 0.8545454545454546
		}]
	},
	"940172": {
		"nik": "940172",
		"nama": "ANDI CHAERUNISA UTAMI PUTRI",
		"band": "V",
		"posisi": "OFF 2 ACCESS NE O&M",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888892,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181815,
			"nilai_komparatif_unit": 0.9824146269062954,
			"summary_team": 0.8727272727272727
		}]
	},
	"940184": {
		"nik": "940184",
		"nama": "FARID AZIANTO",
		"band": "V",
		"posisi": "OFF 2 FAULT HANDLING & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9613636363636362,
			"nilai_komparatif_unit": 0.9619476555124145,
			"summary_team": 0.8545454545454546
		}]
	},
	"940187": {
		"nik": "940187",
		"nama": "TAMARDI PRANATA TAMPUBOLON",
		"band": "V",
		"posisi": "OFF 2 ACCESS NEW FTTH PROJECT SUPERVISIO",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8999999999999996,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1111111111111116,
			"nilai_komparatif_unit": 1.11178610040836,
			"summary_team": 1
		}]
	},
	"940208": {
		"nik": "940208",
		"nama": "FAISAL HELMI AMARULLAH",
		"band": "V",
		"posisi": "OFF 2 HC PLANNING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333337,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999996,
			"nilai_komparatif_unit": 0.9605831907528222,
			"summary_team": 0.8
		}]
	},
	"940222": {
		"nik": "940222",
		"nama": "FAZZ FAUZAN RAZZAQ",
		"band": "V",
		"posisi": "OFF 2 OLO FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8959999999999992,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0551948051948061,
			"nilai_komparatif_unit": 1.0558358258748228,
			"summary_team": 0.9454545454545454
		}]
	},
	"940224": {
		"nik": "940224",
		"nama": "YASMIN ZAHRA QISTHI",
		"band": "V",
		"posisi": "OFF 2 GS DEVELOPMENT PROGRAM & EVAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0369036903690365,
			"nilai_komparatif_unit": 1.0375335993729853,
			"summary_team": 0.8727272727272727
		}]
	},
	"940228": {
		"nik": "940228",
		"nama": "ADHITYA WISENA",
		"band": "V",
		"posisi": "OFF 2 ACCESS OM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8222222222222224,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7739557739557739,
			"nilai_komparatif_unit": 0.7744259446333412,
			"summary_team": 0.6363636363636365
		}]
	},
	"940240": {
		"nik": "940240",
		"nama": "MOHAMMAD FAIZAL SHULTONI",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.895238095238095,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0560928433268861,
			"nilai_komparatif_unit": 1.0567344095564177,
			"summary_team": 0.9454545454545454
		}]
	},
	"940284": {
		"nik": "940284",
		"nama": "DHIYANI NINDYA PRATIWI",
		"band": "V",
		"posisi": "OFF 2 SECRETARY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8755555555555555,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9422588832487312,
			"nilai_komparatif_unit": 0.9428312964440183,
			"summary_team": 0.8250000000000002
		}]
	},
	"940292": {
		"nik": "940292",
		"nama": "MIFTAKHUL AFRIZAL RICKO PRIMANTARA",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE KALIBATA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8676190476190493,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0268436283803992,
			"nilai_komparatif_unit": 1.0274674259935932,
			"summary_team": 0.8909090909090909
		}]
	},
	"940294": {
		"nik": "940294",
		"nama": "MOHAMAD HELMI RIYANDANU",
		"band": "V",
		"posisi": "ACCOUNT MANAGER-1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8616666666666656,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0128362933005113,
			"nilai_komparatif_unit": 1.0134515815925695,
			"summary_team": 0.8727272727272728
		}]
	},
	"940306": {
		"nik": "940306",
		"nama": "YANUAR EGA ARISKA",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882348,
			"nilai_komparatif_unit": 0.9417482262282569,
			"summary_team": 0.8
		}]
	},
	"940310": {
		"nik": "940310",
		"nama": "AFRURSAH SATRIO BIA PRATAMA",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.090909090909091,
			"nilai_komparatif_unit": 1.0915718076736622,
			"summary_team": 0.8545454545454546
		}]
	},
	"940313": {
		"nik": "940313",
		"nama": "SLAMET MAMAT RACHMAT",
		"band": "V",
		"posisi": "OFF 2 SLA MONITORING & KPI REPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882352,
			"nilai_komparatif_unit": 0.9417482262282573,
			"summary_team": 0.8
		}]
	},
	"940327": {
		"nik": "940327",
		"nama": "MUSTIKA SARI",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0115702479338844,
			"nilai_komparatif_unit": 1.0121847671155777,
			"summary_team": 0.9272727272727272
		}]
	},
	"940340": {
		"nik": "940340",
		"nama": "CINTYA AMALIA PUTRI",
		"band": "V",
		"posisi": "OFF 2 MARKET PLAN & ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0200708382526562,
			"nilai_komparatif_unit": 1.0206905214610864,
			"summary_team": 0.8727272727272728
		}]
	},
	"940348": {
		"nik": "940348",
		"nama": "BERRY BUSTAN BASTIAN",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE PSR MINGGU",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8676190476190493,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.943019658716693,
			"nilai_komparatif_unit": 0.9435925340757487,
			"summary_team": 0.8181818181818181
		}]
	},
	"940371": {
		"nik": "940371",
		"nama": "MOHAMAD FAIQ HANIF",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333339,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9374999999999993,
			"nilai_komparatif_unit": 0.9380695222195526,
			"summary_team": 0.8
		}]
	},
	"940381": {
		"nik": "940381",
		"nama": "NISA SOFNIA",
		"band": "V",
		"posisi": "OFF 2 PRODUCT SOLUTION & DELIVERY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9613636363636362,
			"nilai_komparatif_unit": 0.9619476555124145,
			"summary_team": 0.8545454545454546
		}]
	},
	"950012": {
		"nik": "950012",
		"nama": "FRISKA AJENG RIZKI",
		"band": "V",
		"posisi": "OFF 2 PULL SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111115,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.013489736070381,
			"nilai_komparatif_unit": 1.0141054213226277,
			"summary_team": 0.8727272727272728
		}]
	},
	"950042": {
		"nik": "950042",
		"nama": "AZKA ARRAFI ADITAMA",
		"band": "V",
		"posisi": "OFF 2 INFRA & SERVICE SURVEILANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9974025974025972,
			"nilai_komparatif_unit": 0.9980085098730622,
			"summary_team": 0.8727272727272727
		}]
	},
	"950049": {
		"nik": "950049",
		"nama": "MUHAMMAD FACHREZA ADITYA MUHARRAM",
		"band": "V",
		"posisi": "OFF 2 OBL SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333329,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9158751696065134,
			"nilai_komparatif_unit": 0.9164315549499034,
			"summary_team": 0.8181818181818182
		}]
	},
	"950061": {
		"nik": "950061",
		"nama": "RANDY BASTIAN",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190469,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8794688457609813,
			"nilai_komparatif_unit": 0.8800031146133181,
			"summary_team": 0.7454545454545455
		}]
	},
	"950069": {
		"nik": "950069",
		"nama": "ANNISA OKTAVIANDRI",
		"band": "V",
		"posisi": "OFF 2 BS SALES EVALUATION & MARKETING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828875,
			"nilai_komparatif_unit": 1.005958332562002,
			"summary_team": 0.8545454545454546
		}]
	},
	"950093": {
		"nik": "950093",
		"nama": "ALFITA RAHMA KUSUMAWATI",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "DI-1-01",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 1.002038193769772,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0463821892393317,
			"nilai_komparatif_unit": 1.0442537976548907,
			"summary_team": 0.8545454545454546
		}]
	},
	"950105": {
		"nik": "950105",
		"nama": "ARIFIANA SATYA NASTITI",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT PLAN & RVW",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0291595197255572,
			"nilai_komparatif_unit": 1.0297847242204357,
			"summary_team": 0.9090909090909092
		}]
	},
	"950122": {
		"nik": "950122",
		"nama": "AZIZ ARDIANSYAH",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE GAMBIR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8577777777777781,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.996231747527084,
			"nilai_komparatif_unit": 0.9968369487175278,
			"summary_team": 0.8545454545454546
		}]
	},
	"950125": {
		"nik": "950125",
		"nama": "FAHMI LISMAR HALIM",
		"band": "V",
		"posisi": "OFF 2 SERVICE DELIVERY & VAT SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0597402597402596,
			"nilai_komparatif_unit": 1.0603840417401287,
			"summary_team": 0.9272727272727272
		}]
	},
	"950167": {
		"nik": "950167",
		"nama": "YOSEF FEBRI WIRYAWAN",
		"band": "V",
		"posisi": "OFF 2 CONS ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090906,
			"nilai_komparatif_unit": 1.0915718076736618,
			"summary_team": 0.9818181818181818
		}]
	},
	"950172": {
		"nik": "950172",
		"nama": "SYIFA FITRIA",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0463821892393321,
			"nilai_komparatif_unit": 1.0470178563400432,
			"summary_team": 0.8545454545454546
		}]
	},
	"950181": {
		"nik": "950181",
		"nama": "SASONGKO KURNIAWAN",
		"band": "V",
		"posisi": "OFF 2 ACCESS NEW FTTH DESIGN & ENGINEERI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8999999999999996,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9090909090909096,
			"nilai_komparatif_unit": 0.9096431730613855,
			"summary_team": 0.8181818181818182
		}]
	},
	"950184": {
		"nik": "950184",
		"nama": "AULIA RAHMAN",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE PSR KEMIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8935672514619886,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9970252260828174,
			"nilai_komparatif_unit": 0.9976309093038407,
			"summary_team": 0.8909090909090909
		}]
	},
	"950187": {
		"nik": "950187",
		"nama": "TIRZA DAMAYANTI",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE SERPONG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.853846153846153,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9582309582309592,
			"nilai_komparatif_unit": 0.9588130743079473,
			"summary_team": 0.8181818181818182
		}]
	},
	"950188": {
		"nik": "950188",
		"nama": "ARDHAN DWI MEITRIKA SURACHMAN",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8523809523809511,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0878618588115812,
			"nilai_komparatif_unit": 1.0885227244120055,
			"summary_team": 0.9272727272727274
		}]
	},
	"950192": {
		"nik": "950192",
		"nama": "RISKI MUKTIARTO NUGROHO AJINEGORO",
		"band": "V",
		"posisi": "OFF 2 SERVICE NODE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8716666666666656,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9803580740483239,
			"nilai_komparatif_unit": 0.9809536321350322,
			"summary_team": 0.8545454545454546
		}]
	},
	"950193": {
		"nik": "950193",
		"nama": "MUHAMMAD REZA TRIBOSNIA",
		"band": "V",
		"posisi": "OFF 2 INFRA FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882348,
			"nilai_komparatif_unit": 0.9417482262282569,
			"summary_team": 0.8
		}]
	},
	"950196": {
		"nik": "950196",
		"nama": "AUDINATA IBRAHIM SITABA",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE TEBET",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8676190476190493,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0687556132122522,
			"nilai_komparatif_unit": 1.0694048719525153,
			"summary_team": 0.9272727272727272
		}]
	},
	"950200": {
		"nik": "950200",
		"nama": "DAVID STEFANO",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE SEMANGGI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8427860696517424,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0139529891595995,
			"nilai_komparatif_unit": 1.0145689558336357,
			"summary_team": 0.8545454545454546
		}]
	},
	"950211": {
		"nik": "950211",
		"nama": "MUHAMMAD AFIQ",
		"band": "V",
		"posisi": "OFF 2 TPC FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8505747126436772,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.06879606879607,
			"nilai_komparatif_unit": 1.0694453521127107,
			"summary_team": 0.9090909090909092
		}]
	},
	"950214": {
		"nik": "950214",
		"nama": "REVALDA PUTAWARA",
		"band": "V",
		"posisi": "OFF 2 DATA VALIDATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9320388349514567,
			"nilai_komparatif_unit": 0.9326050395658475,
			"summary_team": 0.8
		}]
	},
	"950218": {
		"nik": "950218",
		"nama": "ALEXANDER HANTYA MAHARDHIKA",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE PALMERAH",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8427860696517424,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.035526457014059,
			"nilai_komparatif_unit": 1.0361555293620108,
			"summary_team": 0.8727272727272727
		}]
	},
	"950223": {
		"nik": "950223",
		"nama": "ALDHIAZ PRADIPTA ICHROMANTORO",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200687,
			"nilai_komparatif_unit": 1.0503804187048447,
			"summary_team": 0.9272727272727272
		}]
	},
	"950232": {
		"nik": "950232",
		"nama": "AMALINA KURNIASARI",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969697,
			"nilai_komparatif_unit": 0.9702860512654774,
			"summary_team": 0.8727272727272728
		}]
	},
	"950250": {
		"nik": "950250",
		"nama": "ARDANESIA",
		"band": "V",
		"posisi": "OFF 2 DESIGN & ENGINEERING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300696,
			"nilai_komparatif_unit": 1.070580042141476,
			"summary_team": 0.9272727272727272
		}]
	},
	"950254": {
		"nik": "950254",
		"nama": "CHANDRA SAHA DEWA PRASETYA",
		"band": "V",
		"posisi": "OFF 2 ASSET ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9974025974025972,
			"nilai_komparatif_unit": 0.9980085098730622,
			"summary_team": 0.8727272727272727
		}]
	},
	"950259": {
		"nik": "950259",
		"nama": "PUTRI INDAH SRIWIJAYANTI",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER PROFILE DATA ANALYTIC",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8722222222222223,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.083960625361899,
			"nilai_komparatif_unit": 1.084619121000581,
			"summary_team": 0.9454545454545454
		}]
	},
	"950266": {
		"nik": "950266",
		"nama": "RAHMADIANA",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE MERUYA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8427860696517424,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.035526457014059,
			"nilai_komparatif_unit": 1.0361555293620108,
			"summary_team": 0.8727272727272728
		}]
	},
	"950278": {
		"nik": "950278",
		"nama": "NI MADE CHYNTIA TRISNA EVA DEWI",
		"band": "V",
		"posisi": "OFF 2 PUBLIC RELATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8755555555555555,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9344716197508076,
			"nilai_komparatif_unit": 0.9350393022585304,
			"summary_team": 0.8181818181818182
		}]
	},
	"950297": {
		"nik": "950297",
		"nama": "SATYA PRIANGGONO",
		"band": "V",
		"posisi": "OFF 2 OLO ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1122994652406415,
			"nilai_komparatif_unit": 1.1129751764515767,
			"summary_team": 0.9454545454545454
		}]
	},
	"950307": {
		"nik": "950307",
		"nama": "PRITAMARA WAHYUNINGTYAS",
		"band": "V",
		"posisi": "OFF 2 DIGITAL SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.846666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0307802433786681,
			"nilai_komparatif_unit": 1.0314064324475543,
			"summary_team": 0.8727272727272728
		}]
	},
	"960013": {
		"nik": "960013",
		"nama": "ADI SAEPUL ANWAR",
		"band": "V",
		"posisi": "OFF 2 DESIGN & ENGINEERING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9935064935064937,
			"nilai_komparatif_unit": 0.994110039131371,
			"summary_team": 0.9272727272727274
		}]
	},
	"960039": {
		"nik": "960039",
		"nama": "MUHAMMAD ISNAIN HARTANTO",
		"band": "V",
		"posisi": "OFF 2 ACCESS CAPEX QE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7888888888888893,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.014084507042253,
			"nilai_komparatif_unit": 1.014700553612136,
			"summary_team": 0.8
		}]
	},
	"960052": {
		"nik": "960052",
		"nama": "SHAQILLAH AZ-ZAHRA",
		"band": "V",
		"posisi": "OFF 2 SERVICE DELIVERY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111115,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.950146627565982,
			"nilai_komparatif_unit": 0.9507238324899633,
			"summary_team": 0.8181818181818182
		}]
	},
	"960071": {
		"nik": "960071",
		"nama": "INTISHAR INDRAPUSPA",
		"band": "V",
		"posisi": "OFF 2 COMMAND CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8529914529914517,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0018218254691216,
			"nilai_komparatif_unit": 1.002430422578069,
			"summary_team": 0.8545454545454546
		}]
	},
	"960082": {
		"nik": "960082",
		"nama": "TASYA FIKRIYAH BACHERAMSYAH",
		"band": "V",
		"posisi": "OFF 2 DC & CME OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.870270270270269,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0654997176736323,
			"nilai_komparatif_unit": 1.066146998488718,
			"summary_team": 0.9272727272727272
		}]
	},
	"960097": {
		"nik": "960097",
		"nama": "ARIN WULANDARI",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9801869293396148,
			"summary_team": 0.8
		}]
	},
	"960109": {
		"nik": "960109",
		"nama": "FAIZ RAHMAN ARIFIN",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE KEDOYA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8427860696517424,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8845121820328421,
			"nilai_komparatif_unit": 0.8850495146633842,
			"summary_team": 0.7454545454545455
		}]
	},
	"970004": {
		"nik": "970004",
		"nama": "GURU PAMOSIK WIBOWO",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE KEMANG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8676190476190493,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0058876359644726,
			"nilai_komparatif_unit": 1.0064987030141321,
			"summary_team": 0.8727272727272727
		}]
	}
};