export const cbark16 = {
	"635389": {
		"nik": "635389",
		"nama": "ANANG YULI SUSILOWARDOYO",
		"band": "IV",
		"posisi": "OFF 1 ACCESS CAPEX QE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7888888888888893,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.898847631241997,
			"nilai_komparatif_unit": 0.9051466592983632,
			"summary_team": 0.7090909090909091
		}]
	},
	"635641": {
		"nik": "635641",
		"nama": "MAULANA AKMAL RAHMAT",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER PROFILE DATA ANALYTIC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8722222222222223,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8338158656629994,
			"nilai_komparatif_unit": 0.8396591580621764,
			"summary_team": 0.7272727272727273
		}]
	},
	"635647": {
		"nik": "635647",
		"nama": "TIORISTA AMBARITA",
		"band": "IV",
		"posisi": "OFF 1 PROJECT DATA MGT & ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8388888888888882,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.86694762191451,
			"nilai_komparatif_unit": 0.8730230981176278,
			"summary_team": 0.7272727272727273
		}]
	},
	"650319": {
		"nik": "650319",
		"nama": "JOKO WINARTOYO S.",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8280701754385963,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9990369799691836,
			"nilai_komparatif_unit": 1.0060381242648837,
			"summary_team": 0.8272727272727273
		}]
	},
	"650511": {
		"nik": "650511",
		"nama": "NORMA NELTJE POSUMAH",
		"band": "IV",
		"posisi": "ASMAN BUSINESS OBL & DELIVERY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0196078431372544,
			"nilai_komparatif_unit": 1.0267531458417165,
			"summary_team": 0.8666666666666667
		}]
	},
	"650756": {
		"nik": "650756",
		"nama": "MULYANTO",
		"band": "IV",
		"posisi": "OFF 1 INFRA SERVICE DELIVERY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8959999999999992,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9943181818181827,
			"nilai_komparatif_unit": 1.001286257281177,
			"summary_team": 0.8909090909090909
		}]
	},
	"651369": {
		"nik": "651369",
		"nama": "LUTFI MARUF",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777782,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9735327963176059,
			"nilai_komparatif_unit": 0.9803552100222779,
			"summary_team": 0.8545454545454545
		}]
	},
	"651421": {
		"nik": "651421",
		"nama": "FIRMAN",
		"band": "IV",
		"posisi": "ASMAN COMMUNITY DEVELOPMENT CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8352941176470587,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.013001083423619,
			"nilai_komparatif_unit": 1.0201000866626861,
			"summary_team": 0.8461538461538463
		}]
	},
	"651573": {
		"nik": "651573",
		"nama": "SEKAR",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8156862745098041,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9361888111888108,
			"nilai_komparatif_unit": 0.9427495222401223,
			"summary_team": 0.7636363636363636
		}]
	},
	"660036": {
		"nik": "660036",
		"nama": "EKO BUDIARSO",
		"band": "IV",
		"posisi": "ASMAN DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8637681159420282,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1042849767681993,
			"nilai_komparatif_unit": 1.1120236877678324,
			"summary_team": 0.9538461538461538
		}]
	},
	"660050": {
		"nik": "660050",
		"nama": "IDCHAM RUFFIAT SAMAUN",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8814814814814816,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9075630252100839,
			"nilai_komparatif_unit": 0.9139231298151546,
			"summary_team": 0.8
		}]
	},
	"660052": {
		"nik": "660052",
		"nama": "MAGDALENA BUDIMAN",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346935,
			"nilai_komparatif_unit": 0.9864567115465157,
			"summary_team": 0.7999999999999999
		}]
	},
	"660080": {
		"nik": "660080",
		"nama": "NIMBROD RUMERE",
		"band": "IV",
		"posisi": "ASMAN OM SWITCHING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8190476190476191,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8546511627906975,
			"nilai_komparatif_unit": 0.860640466723541,
			"summary_team": 0.7
		}]
	},
	"660110": {
		"nik": "660110",
		"nama": "LOUISSA S. SAHETAPY",
		"band": "IV",
		"posisi": "JM CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7860082304526756,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9252736792003797,
			"nilai_komparatif_unit": 0.9317578981742308,
			"summary_team": 0.7272727272727272
		}]
	},
	"660164": {
		"nik": "660164",
		"nama": "BAKHTIAR S.",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444444,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1546052631578947,
			"nilai_komparatif_unit": 1.1626966133421415,
			"summary_team": 0.9749999999999999
		}]
	},
	"660174": {
		"nik": "660174",
		"nama": "I KETUT SUARMA",
		"band": "IV",
		"posisi": "ASMAN HUBUNGAN ANTAR INSTANSI/PEMDA",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.038461538461538,
			"nilai_komparatif_unit": 1.0457389658461862,
			"summary_team": 0.8999999999999999
		}]
	},
	"660466": {
		"nik": "660466",
		"nama": "SAHRONI",
		"band": "IV",
		"posisi": "ASMAN OM TRANSPORT, RADIO & MUX",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.858666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9854276158623992,
			"nilai_komparatif_unit": 0.9923333871901364,
			"summary_team": 0.8461538461538461
		}]
	},
	"660486": {
		"nik": "660486",
		"nama": "ROHAYATI",
		"band": "IV",
		"posisi": "OFF 1 RUMAH KREATIF BUMN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8352941176470587,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0230473751600513,
			"nilai_komparatif_unit": 1.0302167817370267,
			"summary_team": 0.8545454545454546
		}]
	},
	"660497": {
		"nik": "660497",
		"nama": "PANCHA RIYADI",
		"band": "IV",
		"posisi": "ASMAN BS OBL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8616666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.991735537190084,
			"nilai_komparatif_unit": 0.9986855137557719,
			"summary_team": 0.8545454545454546
		}]
	},
	"660503": {
		"nik": "660503",
		"nama": "INE AMPERIANE",
		"band": "IV",
		"posisi": "OFF 1 SECURITY & SAFETY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9688995215311001,
			"nilai_komparatif_unit": 0.9756894657416569,
			"summary_team": 0.8181818181818182
		}]
	},
	"660534": {
		"nik": "660534",
		"nama": "FERDINAND LEPUR",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181816,
			"nilai_komparatif_unit": 0.9886986586182125,
			"summary_team": 0.8181818181818182
		}]
	},
	"660535": {
		"nik": "660535",
		"nama": "BENITHO MARTHEN L. LUANMASA",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9644859813084109,
			"nilai_komparatif_unit": 0.9712449959011723,
			"summary_team": 0.86
		}]
	},
	"660557": {
		"nik": "660557",
		"nama": "BAMBANG RISWANTO",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8773333333333325,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0521393500116913,
			"nilai_komparatif_unit": 1.0595126300366642,
			"summary_team": 0.923076923076923
		}]
	},
	"660564": {
		"nik": "660564",
		"nama": "KISMAN",
		"band": "IV",
		"posisi": "OFF 1 SECURITY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0603228547153778,
			"nilai_komparatif_unit": 1.0677534838659823,
			"summary_team": 0.9454545454545454
		}]
	},
	"660565": {
		"nik": "660565",
		"nama": "SULISTIYO",
		"band": "IV",
		"posisi": "ASMAN BGES FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181816,
			"nilai_komparatif_unit": 0.9886986586182125,
			"summary_team": 0.8400000000000001
		}]
	},
	"660629": {
		"nik": "660629",
		"nama": "JUMADIL",
		"band": "IV",
		"posisi": "JM CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8708333333333332,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.939538929969552,
			"nilai_komparatif_unit": 0.9461231182949404,
			"summary_team": 0.8181818181818181
		}]
	},
	"660630": {
		"nik": "660630",
		"nama": "SUWIDIARTA",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999997,
			"nilai_komparatif_unit": 0.9667275773155856,
			"summary_team": 0.8
		}]
	},
	"660631": {
		"nik": "660631",
		"nama": "SYAMSUDIN",
		"band": "IV",
		"posisi": "ASMAN ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8361111111111103,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8970099667774095,
			"nilai_komparatif_unit": 0.90329611667777,
			"summary_team": 0.75
		}]
	},
	"660632": {
		"nik": "660632",
		"nama": "ANSAR",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444444,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9473684210526316,
			"nilai_komparatif_unit": 0.954007477614065,
			"summary_team": 0.8
		}]
	},
	"660633": {
		"nik": "660633",
		"nama": "TOMAS",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8857142857142858,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9677419354838709,
			"nilai_komparatif_unit": 0.9745237674552275,
			"summary_team": 0.8571428571428571
		}]
	},
	"660635": {
		"nik": "660635",
		"nama": "SUGIYANA",
		"band": "IV",
		"posisi": "ASMAN BS SALES ENGINEER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8616666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9495340249692293,
			"nilai_komparatif_unit": 0.9561882578512708,
			"summary_team": 0.8181818181818182
		}]
	},
	"670014": {
		"nik": "670014",
		"nama": "KARSUT LAISA",
		"band": "IV",
		"posisi": "OFF 1 PKBL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.0070078930370683,
			"summary_team": 0.8499999999999999
		}]
	},
	"670052": {
		"nik": "670052",
		"nama": "DEDE TAUFIK HIDAYAT",
		"band": "IV",
		"posisi": "ASMAN WIFI ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8648648648648641,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0495192307692316,
			"nilai_komparatif_unit": 1.0568741492788087,
			"summary_team": 0.9076923076923077
		}]
	},
	"670067": {
		"nik": "670067",
		"nama": "MARISI MANULLANG",
		"band": "IV",
		"posisi": "ASMAN FINANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8156862745098041,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.02534965034965,
			"nilai_komparatif_unit": 1.0325351910248959,
			"summary_team": 0.8363636363636363
		}]
	},
	"670364": {
		"nik": "670364",
		"nama": "AHMAD RASYIDI",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8355555555555545,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9574468085106396,
			"nilai_komparatif_unit": 0.9641564933333647,
			"summary_team": 0.8
		}]
	},
	"670387": {
		"nik": "670387",
		"nama": "TRISNO DJULI WISESO",
		"band": "IV",
		"posisi": "ASMAN NE FTM ACCESS MTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8463768115942021,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9997365648050589,
			"nilai_komparatif_unit": 1.006742611716459,
			"summary_team": 0.8461538461538461
		}]
	},
	"670388": {
		"nik": "670388",
		"nama": "YAYA SUYATNA",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE GDS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8906666666666658,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.979858464888406,
			"nilai_komparatif_unit": 0.9867252082018101,
			"summary_team": 0.8727272727272728
		}]
	},
	"670396": {
		"nik": "670396",
		"nama": "IWAN SETIAWAN",
		"band": "IV",
		"posisi": "OFF 1 RUMAH KREATIF BUMN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333339,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9843749999999994,
			"nilai_komparatif_unit": 0.9912733947083637,
			"summary_team": 0.8400000000000001
		}]
	},
	"670409": {
		"nik": "670409",
		"nama": "MOCHAMAD HAMJANA",
		"band": "IV",
		"posisi": "OFF 1 PROMO PLAN & EXECUTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.083825265643447,
			"nilai_komparatif_unit": 1.0914205971759487,
			"summary_team": 0.9272727272727272
		}]
	},
	"670423": {
		"nik": "670423",
		"nama": "TARSIDI",
		"band": "IV",
		"posisi": "OFF 1 SECURITY & SAFETY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8352941176470587,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8924455825864278,
			"nilai_komparatif_unit": 0.8986997457705977,
			"summary_team": 0.7454545454545455
		}]
	},
	"670439": {
		"nik": "670439",
		"nama": "R. MAY HANIFAH",
		"band": "IV",
		"posisi": "OFF 1 WIFI SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.792592592592593,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0093457943925233,
			"nilai_komparatif_unit": 1.016419181757041,
			"summary_team": 0.8000000000000003
		}]
	},
	"670461": {
		"nik": "670461",
		"nama": "TRIMO WIYONO",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769229,
			"nilai_komparatif_unit": 0.9295457474188323,
			"summary_team": 0.8
		}]
	},
	"670472": {
		"nik": "670472",
		"nama": "SUPRIYATNO",
		"band": "IV",
		"posisi": "OFF 1 SEAT MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.849999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0481283422459906,
			"nilai_komparatif_unit": 1.0554735135575704,
			"summary_team": 0.8909090909090909
		}]
	},
	"670482": {
		"nik": "670482",
		"nama": "DESIWATI",
		"band": "IV",
		"posisi": "ASMAN SALES SUPPORT 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8464646464646456,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0541582522489454,
			"nilai_komparatif_unit": 1.061545680524849,
			"summary_team": 0.8923076923076922
		}]
	},
	"670528": {
		"nik": "670528",
		"nama": "OKTAFIANUS",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8464646464646456,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9814576831283286,
			"nilai_komparatif_unit": 0.988335633592101,
			"summary_team": 0.8307692307692308
		}]
	},
	"670530": {
		"nik": "670530",
		"nama": "AGUS SUPRIADI",
		"band": "IV",
		"posisi": "OFF 1 QUALITY, CHANGE & RISK MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333339,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.009615384615384,
			"nilai_komparatif_unit": 1.0166906612393474,
			"summary_team": 0.8615384615384616
		}]
	},
	"670531": {
		"nik": "670531",
		"nama": "HARTOYO",
		"band": "IV",
		"posisi": "ASMAN BS TERRITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7948717948717949,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.025806451612903,
			"nilai_komparatif_unit": 1.0329951935025412,
			"summary_team": 0.8153846153846154
		}]
	},
	"670540": {
		"nik": "670540",
		"nama": "RIYANTO",
		"band": "IV",
		"posisi": "OFF 1 CAPEX ACCESS NEW FTTH MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8999999999999996,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0303030303030307,
			"nilai_komparatif_unit": 1.037523283735162,
			"summary_team": 0.9272727272727272
		}]
	},
	"670541": {
		"nik": "670541",
		"nama": "SYAHRUJI",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE PSS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8410256410256415,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9695121951219506,
			"nilai_komparatif_unit": 0.9763064328834987,
			"summary_team": 0.8153846153846154
		}]
	},
	"670551": {
		"nik": "670551",
		"nama": "SUMARTO",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1017101710171013,
			"nilai_komparatif_unit": 1.1094308380534397,
			"summary_team": 0.9272727272727272
		}]
	},
	"670557": {
		"nik": "670557",
		"nama": "SUTONO",
		"band": "IV",
		"posisi": "ASMAN TGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7000000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9142857142857143,
			"nilai_komparatif_unit": 0.9206929307767483,
			"summary_team": 0.64
		}]
	},
	"670563": {
		"nik": "670563",
		"nama": "RAMSES MANURUNG",
		"band": "IV",
		"posisi": "ASMAN DATA CENTER & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8190476190476184,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0518783542039365,
			"nilai_komparatif_unit": 1.0592498051982053,
			"summary_team": 0.8615384615384615
		}]
	},
	"670565": {
		"nik": "670565",
		"nama": "ADING NURALAM",
		"band": "IV",
		"posisi": "OFF 1 APARTMENT OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8083333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8997188378631676,
			"nilai_komparatif_unit": 0.9060239712423483,
			"summary_team": 0.7272727272727273
		}]
	},
	"670570": {
		"nik": "670570",
		"nama": "RUSTA",
		"band": "IV",
		"posisi": "ASMAN WHOLESALE & WIFI ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8793650793650786,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9797278533740637,
			"nilai_komparatif_unit": 0.9865936813759458,
			"summary_team": 0.8615384615384616
		}]
	},
	"670573": {
		"nik": "670573",
		"nama": "ISMAIL MUTAWAKKIL",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8648648648648641,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0329166666666674,
			"nilai_komparatif_unit": 1.0401552361828728,
			"summary_team": 0.8933333333333332
		}]
	},
	"670574": {
		"nik": "670574",
		"nama": "YAYA SUNARYA",
		"band": "IV",
		"posisi": "ASMAN BGES FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.019230769230769,
			"nilai_komparatif_unit": 1.0263734294416273,
			"summary_team": 0.8833333333333334
		}]
	},
	"670580": {
		"nik": "670580",
		"nama": "TABRANI",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666665,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9677419354838711,
			"nilai_komparatif_unit": 0.9745237674552277,
			"summary_team": 0.8
		}]
	},
	"670586": {
		"nik": "670586",
		"nama": "MUHAMMAD KASIM",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444446,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8612440191387559,
			"nilai_komparatif_unit": 0.8672795251036953,
			"summary_team": 0.7272727272727273
		}]
	},
	"670587": {
		"nik": "670587",
		"nama": "RUDI HASDAR",
		"band": "IV",
		"posisi": "ASMAN CONSUMER ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8561403508771925,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9981371087928471,
			"nilai_komparatif_unit": 1.005131946887596,
			"summary_team": 0.8545454545454546
		}]
	},
	"670594": {
		"nik": "670594",
		"nama": "ACHMAD MURTOLO",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.853846153846153,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.936936936936938,
			"nilai_komparatif_unit": 0.9435028907734707,
			"summary_team": 0.8000000000000002
		}]
	},
	"670625": {
		"nik": "670625",
		"nama": "IMAN RESMANA",
		"band": "IV",
		"posisi": "ASMAN PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8319999999999993,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9245562130177521,
			"nilai_komparatif_unit": 0.9310354040653377,
			"summary_team": 0.7692307692307692
		}]
	},
	"680103": {
		"nik": "680103",
		"nama": "SRI WINARSIH",
		"band": "IV",
		"posisi": "ASMAN FINANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8906666666666658,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.979858464888406,
			"nilai_komparatif_unit": 0.9867252082018101,
			"summary_team": 0.8727272727272728
		}]
	},
	"680105": {
		"nik": "680105",
		"nama": "DASAHARI EENDARTI",
		"band": "IV",
		"posisi": "ASMAN PARTNERSHIP & AGENCY 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333339,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9218749999999994,
			"nilai_komparatif_unit": 0.928335401393547,
			"summary_team": 0.7866666666666667
		}]
	},
	"680109": {
		"nik": "680109",
		"nama": "SUSIWIYANTI",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION EBIS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9923753665689147,
			"nilai_komparatif_unit": 0.9993298269904513,
			"summary_team": 0.8545454545454546
		}]
	},
	"680197": {
		"nik": "680197",
		"nama": "HAMDANI",
		"band": "IV",
		"posisi": "ASMAN LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8871794871794874,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9222280609563845,
			"nilai_komparatif_unit": 0.92869093656335,
			"summary_team": 0.8181818181818182
		}]
	},
	"680203": {
		"nik": "680203",
		"nama": "CARSAN",
		"band": "IV",
		"posisi": "OFF 1 DC & CME OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.870270270270269,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9819311123658965,
			"nilai_komparatif_unit": 0.9888123805711264,
			"summary_team": 0.8545454545454546
		}]
	},
	"680206": {
		"nik": "680206",
		"nama": "DJOKO WIJAKSONO",
		"band": "IV",
		"posisi": "ASMAN DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8637681159420282,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9261744966442962,
			"nilai_komparatif_unit": 0.9326650284504402,
			"summary_team": 0.8
		}]
	},
	"680211": {
		"nik": "680211",
		"nama": "ABDI NUSANTO",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9384615384615397,
			"nilai_komparatif_unit": 0.9450381765424809,
			"summary_team": 0.8133333333333334
		}]
	},
	"680223": {
		"nik": "680223",
		"nama": "CATRIYONO SUHARTANTO",
		"band": "IV",
		"posisi": "OFF 1 DATA VALIDATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333331,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0803177405119158,
			"nilai_komparatif_unit": 1.0878884916834708,
			"summary_team": 0.9272727272727274
		}]
	},
	"680225": {
		"nik": "680225",
		"nama": "DWI MULYONO",
		"band": "IV",
		"posisi": "OFF 1 PERFORMANCE MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7888888888888893,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0371318822023043,
			"nilai_komparatif_unit": 1.0443999914981117,
			"summary_team": 0.8181818181818182
		}]
	},
	"680238": {
		"nik": "680238",
		"nama": "WILLY FRANS KUDING",
		"band": "IV",
		"posisi": "ASMAN BS DELIVERY & QUALITY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8616666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.928433268858802,
			"nilai_komparatif_unit": 0.9349396298990204,
			"summary_team": 0.8
		}]
	},
	"680240": {
		"nik": "680240",
		"nama": "ZALDY ANWAR",
		"band": "IV",
		"posisi": "OFF 1 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8200000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0643015521064298,
			"nilai_komparatif_unit": 1.0717600635427778,
			"summary_team": 0.8727272727272728
		}]
	},
	"680241": {
		"nik": "680241",
		"nama": "ABDUL AJIS",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8793650793650786,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9097472924187734,
			"nilai_komparatif_unit": 0.9161227041348069,
			"summary_team": 0.8
		}]
	},
	"680250": {
		"nik": "680250",
		"nama": "SUDARMANTO",
		"band": "IV",
		"posisi": "OFF 1 PKBL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.068645640074211,
			"nilai_komparatif_unit": 1.0761345944143808,
			"summary_team": 0.8727272727272727
		}]
	},
	"680301": {
		"nik": "680301",
		"nama": "KUS HENDRATNO",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8923076923076922,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0517241379310345,
			"nilai_komparatif_unit": 1.0590945081941583,
			"summary_team": 0.9384615384615385
		}]
	},
	"680322": {
		"nik": "680322",
		"nama": "BUNAWI",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333339,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9588068181818176,
			"nilai_komparatif_unit": 0.9655260338068478,
			"summary_team": 0.8181818181818182
		}]
	},
	"680323": {
		"nik": "680323",
		"nama": "DEDDY PURWANTO",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE PDA",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8719298245614022,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0843241265776493,
			"nilai_komparatif_unit": 1.0919229540742181,
			"summary_team": 0.9454545454545454
		}]
	},
	"680326": {
		"nik": "680326",
		"nama": "PARMINTO",
		"band": "IV",
		"posisi": "ASMAN WHOLESALE & WIFI ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8410256410256415,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0939024390243899,
			"nilai_komparatif_unit": 1.1015683903100613,
			"summary_team": 0.9200000000000002
		}]
	},
	"680332": {
		"nik": "680332",
		"nama": "ODY SURYANA",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0650887573964511,
			"nilai_komparatif_unit": 1.0725527854832697,
			"summary_team": 0.9230769230769231
		}]
	},
	"680333": {
		"nik": "680333",
		"nama": "SUDARYANTO",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE LENGKONG",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8719298245614022,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.917505030181088,
			"nilai_komparatif_unit": 0.9239348072935694,
			"summary_team": 0.8
		}]
	},
	"680375": {
		"nik": "680375",
		"nama": "BAHTIAR",
		"band": "IV",
		"posisi": "ASMAN DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8355555555555545,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9893617021276608,
			"nilai_komparatif_unit": 0.9962950431111436,
			"summary_team": 0.8266666666666667
		}]
	},
	"680403": {
		"nik": "680403",
		"nama": "AHMAD YULI",
		"band": "IV",
		"posisi": "ASMAN BGES ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.019230769230769,
			"nilai_komparatif_unit": 1.0263734294416273,
			"summary_team": 0.8833333333333334
		}]
	},
	"680420": {
		"nik": "680420",
		"nama": "DJUHARTONO",
		"band": "IV",
		"posisi": "ASMAN LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8319999999999993,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1279585798816578,
			"nilai_komparatif_unit": 1.1358631929597123,
			"summary_team": 0.9384615384615385
		}]
	},
	"680427": {
		"nik": "680427",
		"nama": "TATY NURYANTI",
		"band": "IV",
		"posisi": "ASMAN WIFI SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8923076923076922,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9576802507836992,
			"nilai_komparatif_unit": 0.9643915715449044,
			"summary_team": 0.8545454545454546
		}]
	},
	"680469": {
		"nik": "680469",
		"nama": "MUHAMAD",
		"band": "IV",
		"posisi": "ASMAN PROJECT DELIVERY & SUPERVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8463768115942021,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0082191780821927,
			"nilai_komparatif_unit": 1.015284670240114,
			"summary_team": 0.8533333333333334
		}]
	},
	"680477": {
		"nik": "680477",
		"nama": "AHMAD YANI",
		"band": "IV",
		"posisi": "OFF 1 MARKET PLAN & ANALYSIS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649348,
			"nilai_komparatif_unit": 0.9416177701125833,
			"summary_team": 0.8
		}]
	},
	"680481": {
		"nik": "680481",
		"nama": "ROCHADI RACHMAT",
		"band": "IV",
		"posisi": "ASMAN NE FTM ACCESS MTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8355555555555545,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9255319148936182,
			"nilai_komparatif_unit": 0.9320179435555859,
			"summary_team": 0.7733333333333333
		}]
	},
	"680482": {
		"nik": "680482",
		"nama": "RACHMAD ANWAR",
		"band": "IV",
		"posisi": "ASMAN IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8190476190476184,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0906976744186057,
			"nilai_komparatif_unit": 1.0983411670567107,
			"summary_team": 0.8933333333333334
		}]
	},
	"680486": {
		"nik": "680486",
		"nama": "SOLIKIN",
		"band": "IV",
		"posisi": "OFF 1 SERVICE NODE OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8716666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9803580740483239,
			"nilai_komparatif_unit": 0.9872283185692811,
			"summary_team": 0.8545454545454546
		}]
	},
	"680490": {
		"nik": "680490",
		"nama": "SLAMET MULYONO",
		"band": "IV",
		"posisi": "ASMAN NETWORK PERF, QM & ADMIN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8606060606060594,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9929577464788747,
			"nilai_komparatif_unit": 0.9999162881565272,
			"summary_team": 0.8545454545454546
		}]
	},
	"680493": {
		"nik": "680493",
		"nama": "PRIEYONO",
		"band": "IV",
		"posisi": "OFF 1 REPAIR CENTER MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8716666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9386407091952037,
			"nilai_komparatif_unit": 0.9452186028854819,
			"summary_team": 0.8181818181818182
		}]
	},
	"680496": {
		"nik": "680496",
		"nama": "DIAH AKHRIYANTI",
		"band": "IV",
		"posisi": "OFF 1 HUBUNGAN ANTAR INSTANSI/PEMDA",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9262435677530018,
			"nilai_komparatif_unit": 0.9327345836020875,
			"summary_team": 0.8181818181818182
		}]
	},
	"680502": {
		"nik": "680502",
		"nama": "KARDIATI",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.858666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0163749294184086,
			"nilai_komparatif_unit": 1.0234975762093308,
			"summary_team": 0.8727272727272728
		}]
	},
	"680504": {
		"nik": "680504",
		"nama": "RADEN RORO SOESMARINA JUNIARTI",
		"band": "IV",
		"posisi": "OFF 1 HELPDESK OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.047272727272727,
			"nilai_komparatif_unit": 1.0546119025260932,
			"summary_team": 0.8727272727272727
		}]
	},
	"680510": {
		"nik": "680510",
		"nama": "SUKANTO DARMAWAN",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333339,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9374999999999993,
			"nilai_komparatif_unit": 0.9440698997222511,
			"summary_team": 0.8
		}]
	},
	"680514": {
		"nik": "680514",
		"nama": "SUMIYATI",
		"band": "IV",
		"posisi": "ASMAN DIGITAL SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333331,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1112770724421213,
			"nilai_komparatif_unit": 1.1190647833003422,
			"summary_team": 0.9538461538461538
		}]
	},
	"680525": {
		"nik": "680525",
		"nama": "RACHMAT",
		"band": "IV",
		"posisi": "ASMAN OM TRANSPORT, RADIO & MUX",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8190476190476184,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1321353065539121,
			"nilai_komparatif_unit": 1.1400691896857307,
			"summary_team": 0.9272727272727272
		}]
	},
	"680529": {
		"nik": "680529",
		"nama": "SUDRADJAT",
		"band": "IV",
		"posisi": "ASMAN OM TRANSPORT, RADIO & MUX",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8606060606060594,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9915492957746493,
			"nilai_komparatif_unit": 0.9984979671804187,
			"summary_team": 0.8533333333333334
		}]
	},
	"680530": {
		"nik": "680530",
		"nama": "BEDJO",
		"band": "IV",
		"posisi": "ASMAN DATA CENTER & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8606060606060594,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9295774647887338,
			"nilai_komparatif_unit": 0.9360918442316426,
			"summary_team": 0.8
		}]
	},
	"680542": {
		"nik": "680542",
		"nama": "MARIDI",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SERVICE 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666665,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0607940446650126,
			"nilai_komparatif_unit": 1.0682279758643842,
			"summary_team": 0.8769230769230769
		}]
	},
	"680555": {
		"nik": "680555",
		"nama": "AGUS WINARNO",
		"band": "IV",
		"posisi": "OFF 1 BUSINESS APPLICATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.849999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144397,
			"nilai_komparatif_unit": 1.0339332377706811,
			"summary_team": 0.8727272727272727
		}]
	},
	"680556": {
		"nik": "680556",
		"nama": "ARIF HARTOYO",
		"band": "IV",
		"posisi": "OFF 1 BILLING SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444446,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9688995215311004,
			"nilai_komparatif_unit": 0.9756894657416572,
			"summary_team": 0.8181818181818182
		}]
	},
	"680563": {
		"nik": "680563",
		"nama": "IMAM FIRMANSYAH",
		"band": "IV",
		"posisi": "OFF 1 PULL SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8083333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8997188378631676,
			"nilai_komparatif_unit": 0.9060239712423483,
			"summary_team": 0.7272727272727273
		}]
	},
	"680568": {
		"nik": "680568",
		"nama": "DIOR MARULI NAPITUPULU",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333331,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9678864824495895,
			"nilai_komparatif_unit": 0.9746693273906207,
			"summary_team": 0.8307692307692307
		}]
	},
	"680580": {
		"nik": "680580",
		"nama": "TATANG SUTRIANA",
		"band": "IV",
		"posisi": "ASMAN NE FTM ACCESS MTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.858666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0163749294184086,
			"nilai_komparatif_unit": 1.0234975762093308,
			"summary_team": 0.8727272727272728
		}]
	},
	"680591": {
		"nik": "680591",
		"nama": "DEWI CHANDRA WAHYURIANTI",
		"band": "IV",
		"posisi": "ASMAN SALES SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8851851851851847,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9732861280978441,
			"nilai_komparatif_unit": 0.9801068131780164,
			"summary_team": 0.8615384615384616
		}]
	},
	"690041": {
		"nik": "690041",
		"nama": "IRMA KHAIRANY",
		"band": "IV",
		"posisi": "ASMAN LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8561403508771925,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8984867591424972,
			"nilai_komparatif_unit": 0.9047832582457902,
			"summary_team": 0.7692307692307692
		}]
	},
	"690070": {
		"nik": "690070",
		"nama": "SARIP",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.842666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.014096662830841,
			"nilai_komparatif_unit": 1.0212033437732078,
			"summary_team": 0.8545454545454546
		}]
	},
	"690074": {
		"nik": "690074",
		"nama": "ELLIS RACHMIYANTI",
		"band": "IV",
		"posisi": "ASMAN FINANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9501466275659821,
			"nilai_komparatif_unit": 0.9568051535014959,
			"summary_team": 0.8181818181818182
		}]
	},
	"690075": {
		"nik": "690075",
		"nama": "MAKBULAH",
		"band": "IV",
		"posisi": "OFF 1 REVENUE ASSURANCE & FRAUD MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365353,
			"nilai_komparatif_unit": 0.9949168891755601,
			"summary_team": 0.8727272727272728
		}]
	},
	"690077": {
		"nik": "690077",
		"nama": "ACHMAD SATIRI",
		"band": "IV",
		"posisi": "ASMAN BS SALES ENGINEER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333339,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9374999999999993,
			"nilai_komparatif_unit": 0.9440698997222511,
			"summary_team": 0.8
		}]
	},
	"690119": {
		"nik": "690119",
		"nama": "LALAN KUSTULANI",
		"band": "IV",
		"posisi": "ASMAN PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8561403508771925,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8805170239596474,
			"nilai_komparatif_unit": 0.8866875930808745,
			"summary_team": 0.7538461538461538
		}]
	},
	"690123": {
		"nik": "690123",
		"nama": "MUHAMAD",
		"band": "IV",
		"posisi": "ASMAN GES TERITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.842666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0356731875719227,
			"nilai_komparatif_unit": 1.0429310744917866,
			"summary_team": 0.8727272727272728
		}]
	},
	"690126": {
		"nik": "690126",
		"nama": "NOWO KURNIADI",
		"band": "IV",
		"posisi": "ASMAN PROJECT DELIVERY & SUPERVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8307692307692311,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9191919191919188,
			"nilai_komparatif_unit": 0.9256335178421535,
			"summary_team": 0.7636363636363637
		}]
	},
	"690128": {
		"nik": "690128",
		"nama": "NURY ANSRIONO",
		"band": "IV",
		"posisi": "OFF 1 SALES PROMOTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.846666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9448818897637794,
			"nilai_komparatif_unit": 0.9515035209799071,
			"summary_team": 0.8000000000000003
		}]
	},
	"690136": {
		"nik": "690136",
		"nama": "SISWONO",
		"band": "IV",
		"posisi": "ASMAN HUMAN CAPITAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8871794871794874,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0246978455070939,
			"nilai_komparatif_unit": 1.0318788184037222,
			"summary_team": 0.9090909090909092
		}]
	},
	"690142": {
		"nik": "690142",
		"nama": "SURADI",
		"band": "IV",
		"posisi": "ASMAN SALES SUPPORT 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8539682539682534,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0006759040216298,
			"nilai_komparatif_unit": 1.0076885337217851,
			"summary_team": 0.8545454545454546
		}]
	},
	"690150": {
		"nik": "690150",
		"nama": "ZULJAINI",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE SRP",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.853846153846153,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0221130221130232,
			"nilai_komparatif_unit": 1.029275880843786,
			"summary_team": 0.8727272727272728
		}]
	},
	"690169": {
		"nik": "690169",
		"nama": "MANSUR YULYUS",
		"band": "IV",
		"posisi": "ASMAN CONSTRUCTION SUPERVISION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8539682539682534,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0460966542750936,
			"nilai_komparatif_unit": 1.0534275877346886,
			"summary_team": 0.8933333333333333
		}]
	},
	"690209": {
		"nik": "690209",
		"nama": "AGUS WAHONO",
		"band": "IV",
		"posisi": "ASMAN DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8539682539682534,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9728338575922226,
			"nilai_komparatif_unit": 0.9796513732090677,
			"summary_team": 0.8307692307692308
		}]
	},
	"690234": {
		"nik": "690234",
		"nama": "VITRIA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8452380952380943,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0110115236875812,
			"nilai_komparatif_unit": 1.0180965843048275,
			"summary_team": 0.8545454545454546
		}]
	},
	"690239": {
		"nik": "690239",
		"nama": "IDA ELIDA",
		"band": "IV",
		"posisi": "ASMAN CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8773333333333325,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0182370820668702,
			"nilai_komparatif_unit": 1.0253727786243716,
			"summary_team": 0.8933333333333333
		}]
	},
	"690248": {
		"nik": "690248",
		"nama": "DJAIDIN",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8705882352941174,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9476351351351355,
			"nilai_komparatif_unit": 0.9542760608003306,
			"summary_team": 0.8250000000000002
		}]
	},
	"690250": {
		"nik": "690250",
		"nama": "PENY PRASETIARINI",
		"band": "IV",
		"posisi": "OFF 1 HC SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666661,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9457478005865109,
			"nilai_komparatif_unit": 0.9523755000130639,
			"summary_team": 0.7818181818181817
		}]
	},
	"690251": {
		"nik": "690251",
		"nama": "EDI PURNOMO",
		"band": "IV",
		"posisi": "ASMAN CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8111111111111113,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0520547945205476,
			"nilai_komparatif_unit": 1.0594274819896827,
			"summary_team": 0.8533333333333334
		}]
	},
	"690254": {
		"nik": "690254",
		"nama": "RUSMAN BASUKI",
		"band": "IV",
		"posisi": "ASMAN ACCESS ASSET MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8705882352941174,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0072765072765075,
			"nilai_komparatif_unit": 1.0143353932982533,
			"summary_team": 0.8769230769230769
		}]
	},
	"690256": {
		"nik": "690256",
		"nama": "ARIP JAMALUDIN",
		"band": "IV",
		"posisi": "ASMAN CONSTRUCTION SUPERVISION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8319999999999993,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9775641025641034,
			"nilai_komparatif_unit": 0.9844147672317506,
			"summary_team": 0.8133333333333334
		}]
	},
	"690276": {
		"nik": "690276",
		"nama": "VENNY TOISUTA",
		"band": "IV",
		"posisi": "OFF 1 COMMAND CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.049504950495049,
			"nilai_komparatif_unit": 1.0568597689299923,
			"summary_team": 0.8833333333333333
		}]
	},
	"690307": {
		"nik": "690307",
		"nama": "EKO SISWANTO",
		"band": "IV",
		"posisi": "ASMAN DATA CENTER & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.83859649122807,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1274248763788515,
			"nilai_komparatif_unit": 1.1353257493198448,
			"summary_team": 0.9454545454545454
		}]
	},
	"690308": {
		"nik": "690308",
		"nama": "ENDANG SYARIFUDIN",
		"band": "IV",
		"posisi": "ASMAN INTEGRATION SYSTEM-SITE ADVALJAR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8637681159420282,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9261744966442962,
			"nilai_komparatif_unit": 0.9326650284504402,
			"summary_team": 0.8
		}]
	},
	"690311": {
		"nik": "690311",
		"nama": "SLAMET RIYONO",
		"band": "IV",
		"posisi": "ASMAN PROJECT DELIVERY & SUPERVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8355555555555545,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.021276595744682,
			"nilai_komparatif_unit": 1.0284335928889223,
			"summary_team": 0.8533333333333333
		}]
	},
	"690326": {
		"nik": "690326",
		"nama": "RUDI KURNIADI",
		"band": "IV",
		"posisi": "ASMAN PARTNERSHIP & AGENCY 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8464646464646456,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0178079676886371,
			"nilai_komparatif_unit": 1.0249406570584751,
			"summary_team": 0.8615384615384616
		}]
	},
	"690331": {
		"nik": "690331",
		"nama": "WAHYUDIN",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE CPH",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0650887573964511,
			"nilai_komparatif_unit": 1.0725527854832697,
			"summary_team": 0.9230769230769231
		}]
	},
	"690354": {
		"nik": "690354",
		"nama": "SUBHIYANTO",
		"band": "IV",
		"posisi": "OFF 1 HC SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666661,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8797653958944288,
			"nilai_komparatif_unit": 0.8859306976865712,
			"summary_team": 0.7272727272727273
		}]
	},
	"690370": {
		"nik": "690370",
		"nama": "MUTIARA BUNGSU",
		"band": "IV",
		"posisi": "OFF 1 QUALITY & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8200000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7538802660753877,
			"nilai_komparatif_unit": 0.7591633783428008,
			"summary_team": 0.6181818181818182
		}]
	},
	"690374": {
		"nik": "690374",
		"nama": "SOMAT",
		"band": "IV",
		"posisi": "ASMAN PARTNERSHIP & AGENCY 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8545454545454549,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0141843971631201,
			"nilai_komparatif_unit": 1.0212916929383031,
			"summary_team": 0.8666666666666667
		}]
	},
	"690422": {
		"nik": "690422",
		"nama": "ANIS",
		"band": "IV",
		"posisi": "ASMAN NETWORK PERF, QM & ADMIN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8190476190476184,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0433403805496837,
			"nilai_komparatif_unit": 1.0506519983378302,
			"summary_team": 0.8545454545454545
		}]
	},
	"690425": {
		"nik": "690425",
		"nama": "ALI RACHMAT",
		"band": "IV",
		"posisi": "ASMAN IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8606060606060594,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1083423618634902,
			"nilai_komparatif_unit": 1.1161095065838815,
			"summary_team": 0.9538461538461538
		}]
	},
	"690434": {
		"nik": "690434",
		"nama": "UDIN SAEFUDIN",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9755244755244767,
			"nilai_komparatif_unit": 0.9823608467039946,
			"summary_team": 0.8454545454545455
		}]
	},
	"690444": {
		"nik": "690444",
		"nama": "NOER ILHAM NURDIN",
		"band": "IV",
		"posisi": "OFF 1 ACCESS NEW FTTH DESIGN & ENGINEERI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8999999999999996,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9090909090909096,
			"nilai_komparatif_unit": 0.9154617209427901,
			"summary_team": 0.8181818181818182
		}]
	},
	"690450": {
		"nik": "690450",
		"nama": "HANDOKO",
		"band": "IV",
		"posisi": "ASMAN HUBUNGAN ANTAR INSTANSI/PEMDA",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8319999999999993,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8959790209790218,
			"nilai_komparatif_unit": 0.9022579461215002,
			"summary_team": 0.7454545454545455
		}]
	},
	"690473": {
		"nik": "690473",
		"nama": "HADI SANTOSO",
		"band": "IV",
		"posisi": "OFF 1 IP NODE OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8716666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9177820267686436,
			"nilai_komparatif_unit": 0.9242137450435822,
			"summary_team": 0.8
		}]
	},
	"690478": {
		"nik": "690478",
		"nama": "HARDJITO",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333331,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0718446601941751,
			"nilai_komparatif_unit": 1.079356032925169,
			"summary_team": 0.9199999999999999
		}]
	},
	"690479": {
		"nik": "690479",
		"nama": "SITI PARIDAH",
		"band": "IV",
		"posisi": "ASMAN COMMUNITY DEVELOPMENT CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8871794871794874,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0404624277456644,
			"nilai_komparatif_unit": 1.0477538771483947,
			"summary_team": 0.923076923076923
		}]
	},
	"690483": {
		"nik": "690483",
		"nama": "SUTISNA",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE LGK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.853846153846153,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9837837837837848,
			"nilai_komparatif_unit": 0.9906780353121442,
			"summary_team": 0.8400000000000001
		}]
	},
	"690486": {
		"nik": "690486",
		"nama": "SUGENG",
		"band": "IV",
		"posisi": "OFF 1 SECURITY & SAFETY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.011961722488038,
			"nilai_komparatif_unit": 1.0190534419968418,
			"summary_team": 0.8545454545454546
		}]
	},
	"690489": {
		"nik": "690489",
		"nama": "SRI HARYANI",
		"band": "IV",
		"posisi": "ASMAN HUMAN CAPITAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8352941176470587,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795134443021769,
			"nilai_komparatif_unit": 0.9863777697482171,
			"summary_team": 0.8181818181818182
		}]
	},
	"690503": {
		"nik": "690503",
		"nama": "UTARIYANTO",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7948717948717949,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8920821114369503,
			"nilai_komparatif_unit": 0.8983337274541826,
			"summary_team": 0.7090909090909092
		}]
	},
	"690522": {
		"nik": "690522",
		"nama": "SAPRUDIN",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.858666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9472049689441001,
			"nilai_komparatif_unit": 0.9538428800506402,
			"summary_team": 0.8133333333333334
		}]
	},
	"690530": {
		"nik": "690530",
		"nama": "PELITA SITORUS",
		"band": "IV",
		"posisi": "OFF 1 ENTERPRISE & OLO DESIGN & ENGINEER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969696,
			"nilai_komparatif_unit": 0.9764925023389754,
			"summary_team": 0.8727272727272728
		}]
	},
	"690531": {
		"nik": "690531",
		"nama": "SRI AMBAR WINARSIH",
		"band": "IV",
		"posisi": "ASMAN DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8906666666666658,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9594447468698976,
			"nilai_komparatif_unit": 0.9661684330309391,
			"summary_team": 0.8545454545454546
		}]
	},
	"690550": {
		"nik": "690550",
		"nama": "IYANUDDIN",
		"band": "IV",
		"posisi": "ASMAN COMMUNICATION & SECRETARIATE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8200000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9756097560975605,
			"nilai_komparatif_unit": 0.9824467249142127,
			"summary_team": 0.7999999999999999
		}]
	},
	"690553": {
		"nik": "690553",
		"nama": "BUDI YANTO",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8719298245614022,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0398390342052328,
			"nilai_komparatif_unit": 1.0471261149327118,
			"summary_team": 0.9066666666666666
		}]
	},
	"690559": {
		"nik": "690559",
		"nama": "ABDUL WAHAB",
		"band": "IV",
		"posisi": "ASMAN CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8648648648648641,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0329166666666676,
			"nilai_komparatif_unit": 1.040155236182873,
			"summary_team": 0.8933333333333334
		}]
	},
	"690565": {
		"nik": "690565",
		"nama": "MOHAMAD DJADJOELI",
		"band": "IV",
		"posisi": "OFF 1 TPC ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8505747126436772,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9619164619164631,
			"nilai_komparatif_unit": 0.968657469592169,
			"summary_team": 0.8181818181818182
		}]
	},
	"690569": {
		"nik": "690569",
		"nama": "TEGUH WIDODO",
		"band": "IV",
		"posisi": "ASMAN DEBT MGT & COLLECTION PARTNERSHIP",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777773,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9990262901655311,
			"nilai_komparatif_unit": 1.0060273595482305,
			"summary_team": 0.8769230769230769
		}]
	},
	"690575": {
		"nik": "690575",
		"nama": "SRI LARASATI",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION CONSUMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777773,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9464459591041875,
			"nilai_komparatif_unit": 0.9530785511509554,
			"summary_team": 0.8307692307692308
		}]
	},
	"690581": {
		"nik": "690581",
		"nama": "MUHAMAD WAHYUDI",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION CONSUMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.848484848484849,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9972527472527467,
			"nilai_komparatif_unit": 1.0042413878364167,
			"summary_team": 0.8461538461538461
		}]
	},
	"690582": {
		"nik": "690582",
		"nama": "RAMSES SAUT PARLINDUNGAN",
		"band": "IV",
		"posisi": "ASMAN BGES PROJECT MGT & QUALITY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.842666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.014096662830841,
			"nilai_komparatif_unit": 1.0212033437732078,
			"summary_team": 0.8545454545454546
		}]
	},
	"690585": {
		"nik": "690585",
		"nama": "MUHAMMAD PANGERAN",
		"band": "IV",
		"posisi": "MGR HUBUNGAN ANTAR INSTANSI/PEMDA",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8454545454545446,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9644334160463203,
			"nilai_komparatif_unit": 0.9711920622673474,
			"summary_team": 0.8153846153846154
		}]
	},
	"700008": {
		"nik": "700008",
		"nama": "LILI SETIANINGSIH",
		"band": "IV",
		"posisi": "OFF 1 TAX OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8388888888888892,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9536423841059599,
			"nilai_komparatif_unit": 0.9603254079293895,
			"summary_team": 0.8
		}]
	},
	"700020": {
		"nik": "700020",
		"nama": "DWI MARIA PUTRIKUSUMA",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION EBIS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8906666666666658,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8982035928143721,
			"nilai_komparatif_unit": 0.9044981075183259,
			"summary_team": 0.8
		}]
	},
	"700050": {
		"nik": "700050",
		"nama": "IWAN SETIAWAN",
		"band": "IV",
		"posisi": "ASMAN DEBT MGT & COLLECTION PARTNERSHIP",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.848484848484849,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0057142857142851,
			"nilai_komparatif_unit": 1.0127622238544225,
			"summary_team": 0.8533333333333334
		}]
	},
	"700063": {
		"nik": "700063",
		"nama": "NOVIYADI",
		"band": "IV",
		"posisi": "OFF 1 HC DEVELOPMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999999,
			"nilai_komparatif_unit": 0.9667275773155857,
			"summary_team": 0.8000000000000003
		}]
	},
	"700064": {
		"nik": "700064",
		"nama": "OKTO HERY PRIHANTO",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8719298245614022,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696359978050133,
			"nilai_komparatif_unit": 0.976431103162522,
			"summary_team": 0.8454545454545455
		}]
	},
	"700072": {
		"nik": "700072",
		"nama": "AGUS PURWANTO",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8773333333333325,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.911854103343466,
			"nilai_komparatif_unit": 0.918244279365109,
			"summary_team": 0.8
		}]
	},
	"700090": {
		"nik": "700090",
		"nama": "IRWANTO",
		"band": "IV",
		"posisi": "ASMAN BS TERRITORY SALES",
		"category": [{
			"code": "LC-2-01",
			"kriteria": 0.818333333333332,
			"kriteria_bp": 0.9900885493452141,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8887243103129064,
			"nilai_komparatif_unit": 0.8976210369271073,
			"summary_team": 0.7272727272727273
		}]
	},
	"700095": {
		"nik": "700095",
		"nama": "MOHAMAD YUSUF",
		"band": "IV",
		"posisi": "ASMAN BS DELIVERY & QUALITY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8606060606060594,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0535211267605649,
			"nilai_komparatif_unit": 1.0609040901291948,
			"summary_team": 0.9066666666666667
		}]
	},
	"700099": {
		"nik": "700099",
		"nama": "NGADI",
		"band": "IV",
		"posisi": "OFF 1 OLO ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0481283422459893,
			"nilai_komparatif_unit": 1.055473513557569,
			"summary_team": 0.8909090909090909
		}]
	},
	"700103": {
		"nik": "700103",
		"nama": "RACHMAD SUNARSO",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8705882352941174,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0337837837837842,
			"nilai_komparatif_unit": 1.041028429963997,
			"summary_team": 0.9000000000000001
		}]
	},
	"700111": {
		"nik": "700111",
		"nama": "SUGIANTO",
		"band": "IV",
		"posisi": "ASMAN CONSUMER ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8648648648648641,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8894230769230778,
			"nilai_komparatif_unit": 0.8956560587108551,
			"summary_team": 0.7692307692307693
		}]
	},
	"700115": {
		"nik": "700115",
		"nama": "TIRTA JAYA",
		"band": "IV",
		"posisi": "ASMAN PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8539682539682534,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9836431226765807,
			"nilai_komparatif_unit": 0.9905363884669462,
			"summary_team": 0.8400000000000001
		}]
	},
	"700123": {
		"nik": "700123",
		"nama": "AGUS SURYANA",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SERVICE 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666665,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.967741935483871,
			"nilai_komparatif_unit": 0.9745237674552276,
			"summary_team": 0.7999999999999999
		}]
	},
	"700136": {
		"nik": "700136",
		"nama": "MARJANI",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8463768115942021,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9767123287671242,
			"nilai_komparatif_unit": 0.9835570242951104,
			"summary_team": 0.8266666666666667
		}]
	},
	"700138": {
		"nik": "700138",
		"nama": "MULYONO",
		"band": "IV",
		"posisi": "ASMAN DAVAL & INVENTORY MGT PL & REVIEW",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333339,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9218749999999993,
			"nilai_komparatif_unit": 0.9283354013935469,
			"summary_team": 0.7866666666666666
		}]
	},
	"700139": {
		"nik": "700139",
		"nama": "MUHAMAD NURDIN",
		"band": "IV",
		"posisi": "OFF 1 MAINTENANCE ENGINEERING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8716666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9803580740483239,
			"nilai_komparatif_unit": 0.9872283185692811,
			"summary_team": 0.8545454545454546
		}]
	},
	"700147": {
		"nik": "700147",
		"nama": "SUBCHAN",
		"band": "IV",
		"posisi": "ASMAN NTE MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8637681159420282,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9261744966442962,
			"nilai_komparatif_unit": 0.9326650284504402,
			"summary_team": 0.8
		}]
	},
	"700155": {
		"nik": "700155",
		"nama": "JULTJAHJA",
		"band": "IV",
		"posisi": "OFF 1 PERFORMANCE MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.155080213903743,
			"nilai_komparatif_unit": 1.1631748924920147,
			"summary_team": 0.9818181818181818
		}]
	},
	"700156": {
		"nik": "700156",
		"nama": "JUMARTINA",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.986456711546516,
			"summary_team": 0.8
		}]
	},
	"700179": {
		"nik": "700179",
		"nama": "HERU SUSANTO",
		"band": "IV",
		"posisi": "OFF 1 LAN MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.849999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197873,
			"nilai_komparatif_unit": 0.9693124104100137,
			"summary_team": 0.8181818181818182
		}]
	},
	"700189": {
		"nik": "700189",
		"nama": "SUNARNO",
		"band": "IV",
		"posisi": "ASMAN OM SWITCHING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8190476190476184,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9989429175475695,
			"nilai_komparatif_unit": 1.00594340266388,
			"summary_team": 0.8181818181818181
		}]
	},
	"700230": {
		"nik": "700230",
		"nama": "HIDAYATULLOH",
		"band": "IV",
		"posisi": "ASMAN SALES SUPPORT 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333339,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0866477272727264,
			"nilai_komparatif_unit": 1.0942628383144273,
			"summary_team": 0.9272727272727272
		}]
	},
	"700231": {
		"nik": "700231",
		"nama": "SUKARI",
		"band": "IV",
		"posisi": "OFF 1 PROJECT DATA MGT & ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8388888888888882,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.040337146297412,
			"nilai_komparatif_unit": 1.0476277177411535,
			"summary_team": 0.8727272727272728
		}]
	},
	"700236": {
		"nik": "700236",
		"nama": "SUTARMAN",
		"band": "IV",
		"posisi": "ASMAN  PARTNERSHIP INFRASTRUCTURE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8319999999999993,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0355029585798825,
			"nilai_komparatif_unit": 1.0427596525531784,
			"summary_team": 0.8615384615384616
		}]
	},
	"700248": {
		"nik": "700248",
		"nama": "DEDI",
		"band": "IV",
		"posisi": "MGR CNOP AREA TELKOM REGIONAL II",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8959999999999992,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.05654761904762,
			"nilai_komparatif_unit": 1.063951791750475,
			"summary_team": 0.9466666666666667
		}]
	},
	"700290": {
		"nik": "700290",
		"nama": "NUR DJANNAH",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.03030303030303,
			"nilai_komparatif_unit": 1.0375232837351613,
			"summary_team": 0.9272727272727272
		}]
	},
	"700291": {
		"nik": "700291",
		"nama": "WILHELMINA STANLY WANTAH",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9988518943742823,
			"nilai_komparatif_unit": 1.0058517416099306,
			"summary_team": 0.8923076923076924
		}]
	},
	"700300": {
		"nik": "700300",
		"nama": "TEDDY BUSRA, S.E",
		"band": "IV",
		"posisi": "OFF 1 COLLECTION PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7722222222222226,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9653368214519289,
			"nilai_komparatif_unit": 0.9721017986414078,
			"summary_team": 0.7454545454545455
		}]
	},
	"700316": {
		"nik": "700316",
		"nama": "AGUS SETIAWAN",
		"band": "IV",
		"posisi": "OFF 1 ACCESS NEW FTTH DESIGN & ENGINEERI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8916666666666663,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9175870858113854,
			"nilai_komparatif_unit": 0.9240174379609469,
			"summary_team": 0.8181818181818182
		}]
	},
	"700322": {
		"nik": "700322",
		"nama": "ERWIN NURYADI",
		"band": "IV",
		"posisi": "ASMAN WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8545454545454549,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.008183306055646,
			"nilai_komparatif_unit": 1.015248546826242,
			"summary_team": 0.8615384615384616
		}]
	},
	"700325": {
		"nik": "700325",
		"nama": "GUSTI RACHMATULLAH",
		"band": "IV",
		"posisi": "ASMAN BGES ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8307692307692311,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9629629629629626,
			"nilai_komparatif_unit": 0.9697113044060656,
			"summary_team": 0.8
		}]
	},
	"700330": {
		"nik": "700330",
		"nama": "MARYULIS",
		"band": "IV",
		"posisi": "OFF 1 PROJECT DATA MGT & ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8999999999999996,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.050505050505051,
			"nilai_komparatif_unit": 1.0578668775338906,
			"summary_team": 0.9454545454545454
		}]
	},
	"700342": {
		"nik": "700342",
		"nama": "TAUFIK",
		"band": "IV",
		"posisi": "OFF 1 COMMAND CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9988193624557258,
			"nilai_komparatif_unit": 1.0058189817111685,
			"summary_team": 0.8545454545454546
		}]
	},
	"700346": {
		"nik": "700346",
		"nama": "ANTON SUGIHARTONO",
		"band": "IV",
		"posisi": "ASMAN SALES OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8464646464646456,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8978520286396191,
			"nilai_komparatif_unit": 0.9041440796194405,
			"summary_team": 0.76
		}]
	},
	"700350": {
		"nik": "700350",
		"nama": "DJOKO LELONO",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333331,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9631067961165052,
			"nilai_komparatif_unit": 0.9698561455269634,
			"summary_team": 0.8266666666666667
		}]
	},
	"700355": {
		"nik": "700355",
		"nama": "HARNOLO",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7722222222222226,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0359712230215823,
			"nilai_komparatif_unit": 1.0432311985419986,
			"summary_team": 0.8
		}]
	},
	"700367": {
		"nik": "700367",
		"nama": "MULYONO SUDARSONO",
		"band": "IV",
		"posisi": "ASMAN WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8648648648648641,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.072159090909092,
			"nilai_komparatif_unit": 1.0796726671369035,
			"summary_team": 0.9272727272727272
		}]
	},
	"700394": {
		"nik": "700394",
		"nama": "MUHAMAD TAUFIK",
		"band": "IV",
		"posisi": "OFF 1 FAULT HANDLING & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181817,
			"nilai_komparatif_unit": 0.9886986586182126,
			"summary_team": 0.8727272727272728
		}]
	},
	"700468": {
		"nik": "700468",
		"nama": "CHUMAIDI HAMBALI",
		"band": "IV",
		"posisi": "OFF 1 SERVICE DELIVERY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8611111111111115,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0768328445747795,
			"nilai_komparatif_unit": 1.0843791739683617,
			"summary_team": 0.9272727272727272
		}]
	},
	"700474": {
		"nik": "700474",
		"nama": "SARIYANTO",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CILEDUG",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8719298245614022,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.917505030181088,
			"nilai_komparatif_unit": 0.9239348072935694,
			"summary_team": 0.8
		}]
	},
	"700477": {
		"nik": "700477",
		"nama": "DADANG",
		"band": "IV",
		"posisi": "ASMAN OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.83859649122807,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0273575796588352,
			"nilai_komparatif_unit": 1.034557191687906,
			"summary_team": 0.8615384615384616
		}]
	},
	"700507": {
		"nik": "700507",
		"nama": "SIGIT YULIS LESMONOHADI",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090904,
			"nilai_komparatif_unit": 1.098554065131347,
			"summary_team": 0.9272727272727272
		}]
	},
	"700524": {
		"nik": "700524",
		"nama": "SRIYONO",
		"band": "IV",
		"posisi": "OFF 1 REPAIR CENTER MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8716666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9177820267686436,
			"nilai_komparatif_unit": 0.9242137450435822,
			"summary_team": 0.8
		}]
	},
	"700532": {
		"nik": "700532",
		"nama": "SADIRAN",
		"band": "IV",
		"posisi": "ASMAN COMMUNICATION & SECRETARIATE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8906666666666658,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9594447468698976,
			"nilai_komparatif_unit": 0.9661684330309391,
			"summary_team": 0.8545454545454546
		}]
	},
	"700538": {
		"nik": "700538",
		"nama": "ABDUL HAMID",
		"band": "IV",
		"posisi": "OFF 1 ENTERPRISE & OLO PROJECT SUPERVISI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8916666666666663,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.978759558198811,
			"nilai_komparatif_unit": 0.9856186004916767,
			"summary_team": 0.8727272727272728
		}]
	},
	"700566": {
		"nik": "700566",
		"nama": "JAYADI",
		"band": "IV",
		"posisi": "ASMAN BUSINESS QUALITY & DELIVERY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7948717948717949,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7935483870967741,
			"nilai_komparatif_unit": 0.7991094893132865,
			"summary_team": 0.6307692307692307
		}]
	},
	"700569": {
		"nik": "700569",
		"nama": "RUDI EFENDI",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8121212121212124,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0999999999999996,
			"nilai_komparatif_unit": 1.107708682340775,
			"summary_team": 0.8933333333333333
		}]
	},
	"700574": {
		"nik": "700574",
		"nama": "RONNY BUDIMAN SANONI",
		"band": "IV",
		"posisi": "OFF 1 SPAREPART & WAREHOUSING MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8716666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.001216756474884,
			"nilai_komparatif_unit": 1.0082331764111807,
			"summary_team": 0.8727272727272728
		}]
	},
	"700579": {
		"nik": "700579",
		"nama": "NAS RUDI",
		"band": "IV",
		"posisi": "OFF 1 PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8355555555555545,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9893617021276608,
			"nilai_komparatif_unit": 0.9962950431111436,
			"summary_team": 0.8266666666666667
		}]
	},
	"700581": {
		"nik": "700581",
		"nama": "SURADI",
		"band": "IV",
		"posisi": "ASMAN PARTNERSHIP & AGENCY 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8464646464646456,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.992362768496421,
			"nilai_komparatif_unit": 0.9993171406320132,
			"summary_team": 0.84
		}]
	},
	"700584": {
		"nik": "700584",
		"nama": "WERDI MEGANINGSIH",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8851851851851847,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9037656903765695,
			"nilai_komparatif_unit": 0.9100991836653008,
			"summary_team": 0.8
		}]
	},
	"700600": {
		"nik": "700600",
		"nama": "RIYANTO",
		"band": "IV",
		"posisi": "ASMAN OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8190476190476184,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0706618962432926,
			"nilai_komparatif_unit": 1.0781649802910305,
			"summary_team": 0.8769230769230769
		}]
	},
	"700608": {
		"nik": "700608",
		"nama": "TARMUDI",
		"band": "IV",
		"posisi": "OFF 1 HEALTH & SAFETY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9175870858113847,
			"nilai_komparatif_unit": 0.9240174379609463,
			"summary_team": 0.8181818181818182
		}]
	},
	"700615": {
		"nik": "700615",
		"nama": "NURWULAN HERLINA",
		"band": "IV",
		"posisi": "OFF 1 HC SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666661,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1436950146627574,
			"nilai_komparatif_unit": 1.1517099069925425,
			"summary_team": 0.9454545454545454
		}]
	},
	"700629": {
		"nik": "700629",
		"nama": "EMAN",
		"band": "IV",
		"posisi": "OFF 1 SERVICE DELIVERY & VAT SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8750000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9766233766233766,
			"nilai_komparatif_unit": 0.983467448784254,
			"summary_team": 0.8545454545454546
		}]
	},
	"700661": {
		"nik": "700661",
		"nama": "EVI CHRISTIANA",
		"band": "IV",
		"posisi": "OFF 1 SPAREPART & WAREHOUSING MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8716666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9803580740483239,
			"nilai_komparatif_unit": 0.9872283185692811,
			"summary_team": 0.8545454545454546
		}]
	},
	"710019": {
		"nik": "710019",
		"nama": "HENDRIANA",
		"band": "IV",
		"posisi": "ASMAN BS TERRITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.842666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9676241480038956,
			"nilai_komparatif_unit": 0.9744051545331914,
			"summary_team": 0.8153846153846154
		}]
	},
	"710046": {
		"nik": "710046",
		"nama": "RICHARD",
		"band": "IV",
		"posisi": "OFF 1 SOLUTION INTEGRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8933333333333329,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9769335142469476,
			"nilai_komparatif_unit": 0.9837797598191177,
			"summary_team": 0.8727272727272728
		}]
	},
	"710053": {
		"nik": "710053",
		"nama": "SUWIJI",
		"band": "IV",
		"posisi": "ASMAN CONSUMER ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8793650793650786,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0147181338517088,
			"nilai_komparatif_unit": 1.0218291699965154,
			"summary_team": 0.8923076923076924
		}]
	},
	"710064": {
		"nik": "710064",
		"nama": "NAHWAN",
		"band": "IV",
		"posisi": "ASMAN PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8851851851851847,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0080463469584815,
			"nilai_komparatif_unit": 1.0151106279343742,
			"summary_team": 0.8923076923076924
		}]
	},
	"710079": {
		"nik": "710079",
		"nama": "GENTA WIBAWA",
		"band": "IV",
		"posisi": "ASMAN BGES ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0597402597402594,
			"nilai_komparatif_unit": 1.0671668061275943,
			"summary_team": 0.9066666666666666
		}]
	},
	"710082": {
		"nik": "710082",
		"nama": "NASUHADA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8616666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0761385616317931,
			"nilai_komparatif_unit": 1.0836800255647734,
			"summary_team": 0.9272727272727272
		}]
	},
	"710083": {
		"nik": "710083",
		"nama": "NUR WACHID",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CPK PUTIH",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8577777777777781,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0581905141490628,
			"nilai_komparatif_unit": 1.0656062000850601,
			"summary_team": 0.9076923076923076
		}]
	},
	"710107": {
		"nik": "710107",
		"nama": "ANDY",
		"band": "IV",
		"posisi": "ASMAN DAVAL & INVENTORY MGT PL & REVIEW",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8637681159420282,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9261744966442962,
			"nilai_komparatif_unit": 0.9326650284504402,
			"summary_team": 0.8
		}]
	},
	"710113": {
		"nik": "710113",
		"nama": "AMIR FAISAL",
		"band": "IV",
		"posisi": "OFF 1 CD ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8905380333951759,
			"nilai_komparatif_unit": 0.8967788286786507,
			"summary_team": 0.7272727272727273
		}]
	},
	"710125": {
		"nik": "710125",
		"nama": "WENDY MARIA TUPAMAHU",
		"band": "IV",
		"posisi": "JM CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8584615384615386,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9677419354838709,
			"nilai_komparatif_unit": 0.9745237674552275,
			"summary_team": 0.8307692307692308
		}]
	},
	"710150": {
		"nik": "710150",
		"nama": "ELLEN YVONNE MAWIKERE",
		"band": "IV",
		"posisi": "OFF 1 CASH BANK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.043478260869565,
			"nilai_komparatif_unit": 1.0507908449082453,
			"summary_team": 0.8
		}]
	},
	"710151": {
		"nik": "710151",
		"nama": "MARDHIYAH",
		"band": "IV",
		"posisi": "JM CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8429378531073439,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8843529125030474,
			"nilai_komparatif_unit": 0.8905503631208888,
			"summary_team": 0.7454545454545455
		}]
	},
	"710164": {
		"nik": "710164",
		"nama": "SUWARTO",
		"band": "IV",
		"posisi": "OFF 1 BILLING SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9674099485420241,
			"nilai_komparatif_unit": 0.9741894539844026,
			"summary_team": 0.8545454545454546
		}]
	},
	"710169": {
		"nik": "710169",
		"nama": "BENI MUNANDAR",
		"band": "IV",
		"posisi": "ASMAN ACCESS PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8705882352941174,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9719334719334721,
			"nilai_komparatif_unit": 0.9787446777439286,
			"summary_team": 0.8461538461538461
		}]
	},
	"710174": {
		"nik": "710174",
		"nama": "SISWANTO",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333339,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0456730769230762,
			"nilai_komparatif_unit": 1.0530010419978955,
			"summary_team": 0.8923076923076922
		}]
	},
	"710194": {
		"nik": "710194",
		"nama": "AHMAD DARMAWAN",
		"band": "IV",
		"posisi": "OFF 1 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8851851851851847,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9653860783567902,
			"nilai_komparatif_unit": 0.9721514007333897,
			"summary_team": 0.8545454545454546
		}]
	},
	"710198": {
		"nik": "710198",
		"nama": "IRVAN SETIADI",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8463768115942021,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9924657534246586,
			"nilai_komparatif_unit": 0.9994208472676123,
			"summary_team": 0.8400000000000001
		}]
	},
	"710203": {
		"nik": "710203",
		"nama": "LULUK ERIKAWATI",
		"band": "IV",
		"posisi": "OFF 1 HC PLANNING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0499999999999998,
			"nilai_komparatif_unit": 1.0573582876889218,
			"summary_team": 0.8750000000000002
		}]
	},
	"710266": {
		"nik": "710266",
		"nama": "AGUNG ANDRIAN MULYONO",
		"band": "IV",
		"posisi": "ASMAN WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8410256410256415,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0701219512195117,
			"nilai_komparatif_unit": 1.0776212513902772,
			"summary_team": 0.9000000000000001
		}]
	},
	"710274": {
		"nik": "710274",
		"nama": "NURMAULANA",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CIDENG",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8577777777777781,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0043842168194497,
			"nilai_komparatif_unit": 1.0114228339790403,
			"summary_team": 0.8615384615384616
		}]
	},
	"710278": {
		"nik": "710278",
		"nama": "DEDY AFRIANTO",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8616666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0761385616317933,
			"nilai_komparatif_unit": 1.0836800255647736,
			"summary_team": 0.9272727272727274
		}]
	},
	"710285": {
		"nik": "710285",
		"nama": "INDRA GUNAWAN",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.858666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9782608695652182,
			"nilai_komparatif_unit": 0.9851164171014809,
			"summary_team": 0.8400000000000001
		}]
	},
	"710296": {
		"nik": "710296",
		"nama": "EDY SUBEKTI",
		"band": "IV",
		"posisi": "OFF 1 ACCESS NEW FTTH PROJECT SUPERVISIO",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8916666666666663,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9175870858113854,
			"nilai_komparatif_unit": 0.9240174379609469,
			"summary_team": 0.8181818181818182
		}]
	},
	"710307": {
		"nik": "710307",
		"nama": "HERY SUKRISNO",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333331,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9499626587005231,
			"nilai_komparatif_unit": 0.9566198954019056,
			"summary_team": 0.8153846153846154
		}]
	},
	"710308": {
		"nik": "710308",
		"nama": "SLAMET",
		"band": "IV",
		"posisi": "ASMAN BGES BIDDING MGT & OBL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.842666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9493670886075958,
			"nilai_komparatif_unit": 0.956020151617471,
			"summary_team": 0.8
		}]
	},
	"710311": {
		"nik": "710311",
		"nama": "AGUS SUBIANTORO",
		"band": "IV",
		"posisi": "ASMAN CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.83859649122807,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9539748953974898,
			"nilai_komparatif_unit": 0.960660249424484,
			"summary_team": 0.8
		}]
	},
	"710321": {
		"nik": "710321",
		"nama": "M. DJONI TAUFIK",
		"band": "IV",
		"posisi": "OFF 1 WIRELESS OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.849999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882365,
			"nilai_komparatif_unit": 0.9477721346231245,
			"summary_team": 0.8
		}]
	},
	"710325": {
		"nik": "710325",
		"nama": "ANSORI",
		"band": "IV",
		"posisi": "OFF 1 PROJECT MGT SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8611111111111115,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.950146627565982,
			"nilai_komparatif_unit": 0.9568051535014958,
			"summary_team": 0.8181818181818182
		}]
	},
	"710327": {
		"nik": "710327",
		"nama": "AYUB",
		"band": "IV",
		"posisi": "ASMAN OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8606060606060594,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0225352112676072,
			"nilai_komparatif_unit": 1.0297010286548067,
			"summary_team": 0.88
		}]
	},
	"710329": {
		"nik": "710329",
		"nama": "SUHERMAN",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9763313609467468,
			"nilai_komparatif_unit": 0.9831733866929971,
			"summary_team": 0.8461538461538461
		}]
	},
	"710330": {
		"nik": "710330",
		"nama": "HARYONO",
		"band": "IV",
		"posisi": "ASMAN WHOLESALE & WIFI ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8648648648648641,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0483333333333342,
			"nilai_komparatif_unit": 1.0556799412005278,
			"summary_team": 0.9066666666666666
		}]
	},
	"710347": {
		"nik": "710347",
		"nama": "KOMARUDIN",
		"band": "IV",
		"posisi": "ASMAN TGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8648648648648641,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9605769230769238,
			"nilai_komparatif_unit": 0.9673085434077232,
			"summary_team": 0.8307692307692307
		}]
	},
	"710348": {
		"nik": "710348",
		"nama": "SUTOMO",
		"band": "IV",
		"posisi": "ASMAN DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8561403508771925,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.988335435056747,
			"nilai_komparatif_unit": 0.9952615840703694,
			"summary_team": 0.8461538461538461
		}]
	},
	"710351": {
		"nik": "710351",
		"nama": "IDA ADRIYANI",
		"band": "IV",
		"posisi": "OFF 1 FINANCE SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8388888888888892,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0186634557495482,
			"nilai_komparatif_unit": 1.0258021402882116,
			"summary_team": 0.8545454545454546
		}]
	},
	"710363": {
		"nik": "710363",
		"nama": "EDY APRIADI",
		"band": "IV",
		"posisi": "ASMAN CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8793650793650786,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9972229936128862,
			"nilai_komparatif_unit": 1.0042114256862307,
			"summary_team": 0.8769230769230769
		}]
	},
	"720048": {
		"nik": "720048",
		"nama": "LEGIARTININGSIH",
		"band": "IV",
		"posisi": "OFF 1 CD ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0686456400742113,
			"nilai_komparatif_unit": 1.076134594414381,
			"summary_team": 0.8727272727272728
		}]
	},
	"720062": {
		"nik": "720062",
		"nama": "HADY ANGKOTASAN",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8280701754385963,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0218383311603654,
			"nilai_komparatif_unit": 1.0289992648863138,
			"summary_team": 0.8461538461538461
		}]
	},
	"720105": {
		"nik": "720105",
		"nama": "FLORIANO GONCALVES",
		"band": "IV",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8844444444444443,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9723618090452264,
			"nilai_komparatif_unit": 0.9791760165963458,
			"summary_team": 0.8600000000000001
		}]
	},
	"720107": {
		"nik": "720107",
		"nama": "IMANUEL MOSES MARISAN",
		"band": "IV",
		"posisi": "ASMAN NETWORK PERF, QM & ADMIN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8923076923076922,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8557993730407525,
			"nilai_komparatif_unit": 0.8617967235082123,
			"summary_team": 0.7636363636363637
		}]
	},
	"720109": {
		"nik": "720109",
		"nama": "JONI REHABEAM BANO",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7958333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.984293193717277,
			"nilai_komparatif_unit": 0.9911910151359622,
			"summary_team": 0.7833333333333333
		}]
	},
	"720111": {
		"nik": "720111",
		"nama": "OBAJA DWAA",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8285714285714288,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96551724137931,
			"nilai_komparatif_unit": 0.9722834829323417,
			"summary_team": 0.8
		}]
	},
	"730025": {
		"nik": "730025",
		"nama": "JOHN FERDDY WAMBRAUW",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7111111111111111,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8839285714285715,
			"nilai_komparatif_unit": 0.8901230483095517,
			"summary_team": 0.6285714285714287
		}]
	},
	"730027": {
		"nik": "730027",
		"nama": "DALLE",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969695,
			"nilai_komparatif_unit": 0.9764925023389753,
			"summary_team": 0.8727272727272727
		}]
	},
	"730035": {
		"nik": "730035",
		"nama": "PAULUS TUANKOTTA, SE, MM",
		"band": "IV",
		"posisi": "ASMAN WIFI FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9807692307692305,
			"nilai_komparatif_unit": 0.9876423566325092,
			"summary_team": 0.8499999999999999
		}]
	},
	"730064": {
		"nik": "730064",
		"nama": "MELVERSON AYOMI",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8280701754385963,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.998305084745763,
			"nilai_komparatif_unit": 1.005301099998023,
			"summary_team": 0.8266666666666667
		}]
	},
	"730067": {
		"nik": "730067",
		"nama": "NELSON EXPOSTO DE LIMA R.",
		"band": "IV",
		"posisi": "ASMAN CONSTRUCTION SUPERVISION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8476190476190478,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0617977528089884,
			"nilai_komparatif_unit": 1.0692387178876734,
			"summary_team": 0.8999999999999999
		}]
	},
	"730156": {
		"nik": "730156",
		"nama": "FIRMAN FIRDAUS",
		"band": "IV",
		"posisi": "OFF 1 RUMAH KREATIF BUMN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8616666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9073325127483746,
			"nilai_komparatif_unit": 0.9136910019467698,
			"summary_team": 0.7818181818181817
		}]
	},
	"740006": {
		"nik": "740006",
		"nama": "LANE IMELDA PIETERSZ",
		"band": "IV",
		"posisi": "MGR FINANCE & PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8349206349206347,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7665399239543728,
			"nilai_komparatif_unit": 0.7719117537500877,
			"summary_team": 0.64
		}]
	},
	"740008": {
		"nik": "740008",
		"nama": "FRENKY MAURITS MAYER",
		"band": "IV",
		"posisi": "ASMAN OM TRANSPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7583333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9830169830169825,
			"nilai_komparatif_unit": 0.9899058608875873,
			"summary_team": 0.7454545454545455
		}]
	},
	"740015": {
		"nik": "740015",
		"nama": "RAMA DAUD SARAMBU",
		"band": "IV",
		"posisi": "ASMAN WIFI FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272727,
			"nilai_komparatif_unit": 1.0298944360606384,
			"summary_team": 0.8181818181818181
		}]
	},
	"740017": {
		"nik": "740017",
		"nama": "THEODORA BEATRIX KOPEU, SE",
		"band": "IV",
		"posisi": "ASMAN BGES SALES ENGINEER, BIDDING & PM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8222222222222221,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9287469287469288,
			"nilai_komparatif_unit": 0.9352554878820931,
			"summary_team": 0.7636363636363636
		}]
	},
	"740026": {
		"nik": "740026",
		"nama": "POPPY ALETHA KORIDAMA",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7575757575757579,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0229999999999997,
			"nilai_komparatif_unit": 1.0301690745769208,
			"summary_team": 0.775
		}]
	},
	"750018": {
		"nik": "750018",
		"nama": "VICTOR MEYER HUWAE",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777782,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.973532796317606,
			"nilai_komparatif_unit": 0.980355210022278,
			"summary_team": 0.8545454545454546
		}]
	},
	"750040": {
		"nik": "750040",
		"nama": "GILION SOSTENES RAUNSAI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8222222222222221,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9066339066339066,
			"nilai_komparatif_unit": 0.9129875000753767,
			"summary_team": 0.7454545454545454
		}]
	},
	"750043": {
		"nik": "750043",
		"nama": "IZAK HURU BOENGA",
		"band": "IV",
		"posisi": "MGR FINANCE & PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8156862745098041,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9807692307692306,
			"nilai_komparatif_unit": 0.9876423566325093,
			"summary_team": 0.8
		}]
	},
	"750044": {
		"nik": "750044",
		"nama": "PIET GOBAI",
		"band": "IV",
		"posisi": "ASMAN OLO FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8857142857142858,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879032258064515,
			"nilai_komparatif_unit": 0.9948263459438781,
			"summary_team": 0.875
		}]
	},
	"760004": {
		"nik": "760004",
		"nama": "LEONARD CHRISTOFOL",
		"band": "IV",
		"posisi": "ASMAN WHOLESALE FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9499999999999998,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0350877192982455,
			"nilai_komparatif_unit": 1.0423415033190708,
			"summary_team": 0.9833333333333332
		}]
	},
	"760007": {
		"nik": "760007",
		"nama": "MARIANUS FRANSISKUS DUA KOTA",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7958333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0261780104712035,
			"nilai_komparatif_unit": 1.0333693562055775,
			"summary_team": 0.8166666666666665
		}]
	},
	"760022": {
		"nik": "760022",
		"nama": "YESAYA OMRY TETTY",
		"band": "IV",
		"posisi": "OFF 1 HR DEVELOPMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7047619047619048,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.733108108108108,
			"nilai_komparatif_unit": 0.7382456513143373,
			"summary_team": 0.5166666666666666
		}]
	},
	"760024": {
		"nik": "760024",
		"nama": "LODOFIKUS DO KADJU",
		"band": "IV",
		"posisi": "ASMAN CCAN FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9262435677530013,
			"nilai_komparatif_unit": 0.932734583602087,
			"summary_team": 0.8181818181818181
		}]
	},
	"770003": {
		"nik": "770003",
		"nama": "DIAH ANGRIANA SANGADJI",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0279720279720277,
			"nilai_komparatif_unit": 1.035175945989154,
			"summary_team": 0.8909090909090909
		}]
	},
	"770019": {
		"nik": "770019",
		"nama": "NELTJI LEASA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8564102564102566,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8492106695699507,
			"nilai_komparatif_unit": 0.8551618471082343,
			"summary_team": 0.7272727272727272
		}]
	},
	"770024": {
		"nik": "770024",
		"nama": "SALEH BAUW",
		"band": "IV",
		"posisi": "ASMAN ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555556,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.935064935064935,
			"nilai_komparatif_unit": 0.9416177701125835,
			"summary_team": 0.8
		}]
	},
	"780009": {
		"nik": "780009",
		"nama": "REVELINO KAKISINA",
		"band": "IV",
		"posisi": "ASMAN BGES SALES ENGINEER, BIDDING & PM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8905380333951759,
			"nilai_komparatif_unit": 0.8967788286786507,
			"summary_team": 0.7272727272727272
		}]
	},
	"780011": {
		"nik": "780011",
		"nama": "JULIPHA SALURAPA",
		"band": "IV",
		"posisi": "JM CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8349206349206347,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9581749049429661,
			"nilai_komparatif_unit": 0.9648896921876097,
			"summary_team": 0.8
		}]
	},
	"790085": {
		"nik": "790085",
		"nama": "PRINA TIESSA PRINS",
		"band": "IV",
		"posisi": "OFF 1 BS SALES EVALUATION & MARKETING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0481283422459888,
			"nilai_komparatif_unit": 1.0554735135575686,
			"summary_team": 0.8909090909090909
		}]
	},
	"810012": {
		"nik": "810012",
		"nama": "ARIFUDDIN",
		"band": "IV",
		"posisi": "ASMAN OM IP NETWORK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8999999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0303030303030303,
			"nilai_komparatif_unit": 1.0375232837351616,
			"summary_team": 0.9272727272727272
		}]
	},
	"840011": {
		"nik": "840011",
		"nama": "HALIM",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8592592592592595,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9847480106100793,
			"nilai_komparatif_unit": 0.9916490193369007,
			"summary_team": 0.8461538461538461
		}]
	},
	"850042": {
		"nik": "850042",
		"nama": "WAHYUDI YUSUF",
		"band": "IV",
		"posisi": "ASMAN OM TRANSPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8606060606060609,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.929577464788732,
			"nilai_komparatif_unit": 0.9360918442316408,
			"summary_team": 0.8
		}]
	},
	"850054": {
		"nik": "850054",
		"nama": "FATKHU ROKHMAN",
		"band": "IV",
		"posisi": "ASMAN DATA CENTER & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7583333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649344,
			"nilai_komparatif_unit": 0.9416177701125829,
			"summary_team": 0.709090909090909
		}]
	},
	"860039": {
		"nik": "860039",
		"nama": "AZWARUDDIN",
		"band": "IV",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8725925925925916,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.062663991356692,
			"nilai_komparatif_unit": 1.070111026942464,
			"summary_team": 0.9272727272727272
		}]
	},
	"860134": {
		"nik": "860134",
		"nama": "ASRIANTI ARIFIN",
		"band": "IV",
		"posisi": "ASMAN ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8416666666666671,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9801980198019796,
			"nilai_komparatif_unit": 0.9870671426798983,
			"summary_team": 0.825
		}]
	},
	"880039": {
		"nik": "880039",
		"nama": "SEPTIRASYAHYANI",
		"band": "IV",
		"posisi": "OFF 1 QUALITY, CHANGE & RISK MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777773,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9942462600690454,
			"nilai_komparatif_unit": 1.0012138315121146,
			"summary_team": 0.8727272727272727
		}]
	},
	"880094": {
		"nik": "880094",
		"nama": "VENY SESANRIA",
		"band": "IV",
		"posisi": "OFF 1 PROGRAM & EVALUATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.794444444444444,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0985378258105538,
			"nilai_komparatif_unit": 1.106236261391008,
			"summary_team": 0.8727272727272728
		}]
	},
	"890072": {
		"nik": "890072",
		"nama": "NOVIA RESMITA PUTRI",
		"band": "IV",
		"posisi": "OFF 1 WIFI FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999997,
			"nilai_komparatif_unit": 0.9667275773155856,
			"summary_team": 0.8
		}]
	},
	"900019": {
		"nik": "900019",
		"nama": "NI MADE DWIDHYANA KSAMAWATI",
		"band": "IV",
		"posisi": "OFF 1 MAINTENANCE ENGINEERING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8716666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9386407091952037,
			"nilai_komparatif_unit": 0.9452186028854819,
			"summary_team": 0.8181818181818182
		}]
	},
	"900031": {
		"nik": "900031",
		"nama": "PAULA TIARMA",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8464646464646456,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.992362768496421,
			"nilai_komparatif_unit": 0.9993171406320132,
			"summary_team": 0.84
		}]
	},
	"900034": {
		"nik": "900034",
		"nama": "SITI AISHA NUR FLORENZA AGUSTINA",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER EXPERIENCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8722222222222223,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.063115228720324,
			"nilai_komparatif_unit": 1.0705654265292748,
			"summary_team": 0.9272727272727272
		}]
	},
	"900041": {
		"nik": "900041",
		"nama": "GITA EKA RAMADHA",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SERVICE 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333339,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9555288461538455,
			"nilai_komparatif_unit": 0.9622250901015251,
			"summary_team": 0.8153846153846154
		}]
	},
	"900049": {
		"nik": "900049",
		"nama": "YUGA HASTUNGKARA",
		"band": "IV",
		"posisi": "OFF 1 COMMAND CONTROL MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9055555555555556,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9637479085331846,
			"nilai_komparatif_unit": 0.9705017507908836,
			"summary_team": 0.8727272727272727
		}]
	},
	"900057": {
		"nik": "900057",
		"nama": "IRENE MAHADHIKA",
		"band": "IV",
		"posisi": "ASMAN CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333331,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9678864824495896,
			"nilai_komparatif_unit": 0.9746693273906208,
			"summary_team": 0.8307692307692308
		}]
	},
	"900074": {
		"nik": "900074",
		"nama": "RIA ANIANSARI",
		"band": "IV",
		"posisi": "OFF 1 PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0413223140495864,
			"nilai_komparatif_unit": 1.0486197894435585,
			"summary_team": 0.8909090909090909
		}]
	},
	"900080": {
		"nik": "900080",
		"nama": "MUHAMMAD LUKMANUL HAKIM",
		"band": "IV",
		"posisi": "ASMAN ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.008576329331046,
			"nilai_komparatif_unit": 1.015644324366717,
			"summary_team": 0.8909090909090909
		}]
	},
	"910077": {
		"nik": "910077",
		"nama": "FITRI APRILIANI",
		"band": "IV",
		"posisi": "OFF 1 BUDGET CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.794444444444444,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0298792116973943,
			"nilai_komparatif_unit": 1.03709649505407,
			"summary_team": 0.8181818181818182
		}]
	},
	"910105": {
		"nik": "910105",
		"nama": "ADAM MUHAMMAD KAMIL",
		"band": "IV",
		"posisi": "OFF 1 PUSH CHANNEL INTEGRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8611111111111115,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0557184750733135,
			"nilai_komparatif_unit": 1.0631168372238844,
			"summary_team": 0.9090909090909092
		}]
	},
	"910110": {
		"nik": "910110",
		"nama": "YANUAR VALENTINO DWI KURNIAWAN",
		"band": "IV",
		"posisi": "ASMAN DEPLOYMENT & CAPEX MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8476190476190478,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9438202247191011,
			"nilai_komparatif_unit": 0.9504344159001545,
			"summary_team": 0.8000000000000002
		}]
	},
	"910112": {
		"nik": "910112",
		"nama": "RIZKA FEBRINA br BUKIT",
		"band": "IV",
		"posisi": "OFF 1 QUALITY, CHANGE & RISK MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.020070838252656,
			"nilai_komparatif_unit": 1.0272193855773635,
			"summary_team": 0.8727272727272727
		}]
	},
	"910113": {
		"nik": "910113",
		"nama": "DANIKA TRIENTIN",
		"band": "IV",
		"posisi": "ASMAN BS OBL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7948717948717949,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1665689149560117,
			"nilai_komparatif_unit": 1.1747441051323926,
			"summary_team": 0.9272727272727274
		}]
	},
	"910127": {
		"nik": "910127",
		"nama": "KARINA RIZKY ISMANTIA",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SERVICE 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8121212121212124,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9471871412169918,
			"nilai_komparatif_unit": 0.9538249273887272,
			"summary_team": 0.7692307692307693
		}]
	},
	"910148": {
		"nik": "910148",
		"nama": "MUTHMAINNAH",
		"band": "IV",
		"posisi": "ASMAN HOME SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0169491525423728,
			"nilai_komparatif_unit": 1.0240758234275273,
			"summary_team": 1
		}]
	},
	"910199": {
		"nik": "910199",
		"nama": "MALINDA IRIANI",
		"band": "IV",
		"posisi": "OFF 1 CASH BANK & VERIFICATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8388888888888892,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9753160746538226,
			"nilai_komparatif_unit": 0.9821509853823301,
			"summary_team": 0.8181818181818182
		}]
	},
	"910212": {
		"nik": "910212",
		"nama": "RIZKY FAUZIA",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8616666666666657,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.012836293300511,
			"nilai_komparatif_unit": 1.019934141708022,
			"summary_team": 0.8727272727272728
		}]
	},
	"920116": {
		"nik": "920116",
		"nama": "FAKHYAR RUSYID SYAMSUL",
		"band": "IV",
		"posisi": "ASMAN FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8814814814814816,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9900687547746368,
			"nilai_komparatif_unit": 0.9970070507074411,
			"summary_team": 0.8727272727272727
		}]
	},
	"920117": {
		"nik": "920117",
		"nama": "DEVI FITRIANI",
		"band": "IV",
		"posisi": "ASMAN DAVAL & INVENTORY MGT PL & REVIEW",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8906666666666658,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9154767388300331,
			"nilai_komparatif_unit": 0.9218923018936783,
			"summary_team": 0.8153846153846154
		}]
	},
	"920130": {
		"nik": "920130",
		"nama": "LUTFI NURWIDI",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.83859649122807,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9539748953974898,
			"nilai_komparatif_unit": 0.960660249424484,
			"summary_team": 0.8
		}]
	},
	"920146": {
		"nik": "920146",
		"nama": "RIZALDY PAHLEVI",
		"band": "IV",
		"posisi": "ASMAN TGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0070078930370687,
			"summary_team": 0.8
		}]
	},
	"920157": {
		"nik": "920157",
		"nama": "YOHANA JAYANTI ARUAN",
		"band": "IV",
		"posisi": "OFF 1 CNOP FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197861,
			"nilai_komparatif_unit": 0.9693124104100125,
			"summary_team": 0.8181818181818182
		}]
	},
	"920194": {
		"nik": "920194",
		"nama": "ANGGITA CREMONANDRA ELFANI",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CIKINI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8577777777777781,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9792746113989633,
			"nilai_komparatif_unit": 0.9861372631295641,
			"summary_team": 0.84
		}]
	},
	"920204": {
		"nik": "920204",
		"nama": "THIFAN ANJAR PERMADI",
		"band": "IV",
		"posisi": "ASMAN DEPLOYMENT & CAPEX MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555556,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1396103896103893,
			"nilai_komparatif_unit": 1.147596657324711,
			"summary_team": 0.9749999999999999
		}]
	},
	"920253": {
		"nik": "920253",
		"nama": "CITHA ERNANDA RIZKA",
		"band": "IV",
		"posisi": "OFF 1 INFRA FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0695187165775397,
			"nilai_komparatif_unit": 1.077013789344458,
			"summary_team": 0.9090909090909092
		}]
	},
	"930053": {
		"nik": "930053",
		"nama": "GANANG AFIF RIJAZIM",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CENGKARENG",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8427860696517424,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9708060534506803,
			"nilai_komparatif_unit": 0.9776093584330012,
			"summary_team": 0.8181818181818182
		}]
	},
	"930079": {
		"nik": "930079",
		"nama": "JALU AHMAD DIRGANTORO",
		"band": "IV",
		"posisi": "OFF 1 CNOP ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8770053475935828,
			"nilai_komparatif_unit": 0.8831513072624557,
			"summary_team": 0.7454545454545455
		}]
	},
	"930094": {
		"nik": "930094",
		"nama": "MUHAMMAD HAFIZH PAHLEVIE",
		"band": "IV",
		"posisi": "ASMAN BGES SALES ENGINEER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.842666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8846375143843506,
			"nilai_komparatif_unit": 0.8908369594617342,
			"summary_team": 0.7454545454545455
		}]
	},
	"930106": {
		"nik": "930106",
		"nama": "SEPTIAN HADI WIBOWO",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8416666666666671,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.966336633663366,
			"nilai_komparatif_unit": 0.9731086174298798,
			"summary_team": 0.8133333333333335
		}]
	},
	"930108": {
		"nik": "930108",
		"nama": "NESYA AMALIA",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER CARE CONTROL CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8750000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9974025974025974,
			"nilai_komparatif_unit": 1.004392288120089,
			"summary_team": 0.8727272727272728
		}]
	},
	"930116": {
		"nik": "930116",
		"nama": "AMALIA HUSNA DITA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8452380952380943,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.05403329065301,
			"nilai_komparatif_unit": 1.0614198432114157,
			"summary_team": 0.8909090909090909
		}]
	},
	"930124": {
		"nik": "930124",
		"nama": "RIZKI DWI KURNIA DEWI",
		"band": "IV",
		"posisi": "OFF 1 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8250000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969694,
			"nilai_komparatif_unit": 0.9764925023389752,
			"summary_team": 0.8
		}]
	},
	"930129": {
		"nik": "930129",
		"nama": "DESI PUJI RAHAYU",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER PROFILING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882348,
			"nilai_komparatif_unit": 0.9477721346231228,
			"summary_team": 0.8
		}]
	},
	"930191": {
		"nik": "930191",
		"nama": "MUHAMMAD YASER NOVERAMADYA",
		"band": "IV",
		"posisi": "OFF 1 CAPEX ENTERPRISE & OLO MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.01010101010101,
			"nilai_komparatif_unit": 1.0171796899364327,
			"summary_team": 0.9090909090909092
		}]
	},
	"940007": {
		"nik": "940007",
		"nama": "FARRAS GHAZYAFI ELLI",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE SLIPI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8427860696517424,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.967487058396148,
			"nilai_komparatif_unit": 0.9742671042161363,
			"summary_team": 0.8153846153846154
		}]
	},
	"940008": {
		"nik": "940008",
		"nama": "LESTARI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8616666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9284332688588018,
			"nilai_komparatif_unit": 0.9349396298990202,
			"summary_team": 0.7999999999999999
		}]
	},
	"940024": {
		"nik": "940024",
		"nama": "LATHIFA HANUM",
		"band": "IV",
		"posisi": "OFF 1 SECRETARY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8755555555555555,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9137055837563453,
			"nilai_komparatif_unit": 0.920108734754682,
			"summary_team": 0.8
		}]
	}
};