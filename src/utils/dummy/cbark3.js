export const cbark3 = {
	"651026": {
		"nik": "651026",
		"nama": "DISRIZAL",
		"band": "II",
		"posisi": "SM SHARED SERVICE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8271929824561421,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0189365247689726,
			"nilai_komparatif_unit": 1.0224233698512846,
			"summary_team": 0.8428571428571431
		}]
	},
	"651151": {
		"nik": "651151",
		"nama": "AGUS PURNOMO HADI, IR.",
		"band": "II",
		"posisi": "OSM PLANNING, ENGINEERING & DEPLOYMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8526315789473679,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9695473251028813,
			"nilai_komparatif_unit": 0.9728651581969204,
			"summary_team": 0.8266666666666667
		}]
	},
	"651195": {
		"nik": "651195",
		"nama": "BINSAR ULI JOHNER SILALAHI",
		"band": "II",
		"posisi": "GM WITEL MEDAN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8349726775956329,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9461387434554924,
			"nilai_komparatif_unit": 0.9493764713655314,
			"summary_team": 0.79
		}]
	},
	"660121": {
		"nik": "660121",
		"nama": "M. AMIN JAUHARI",
		"band": "II",
		"posisi": "GM WITEL MADIUN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8673267326732697,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.983866057838658,
			"nilai_komparatif_unit": 0.9872328902585736,
			"summary_team": 0.8533333333333334
		}]
	},
	"660123": {
		"nik": "660123",
		"nama": "ZAHRIAL HAMID NST",
		"band": "II",
		"posisi": "GM WITEL TANGERANG",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8482758620689653,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.979362101313321,
			"nilai_komparatif_unit": 0.9827135209982139,
			"summary_team": 0.8307692307692307
		}]
	},
	"660180": {
		"nik": "660180",
		"nama": "M. NOOR ROCHMAN",
		"band": "II",
		"posisi": "KABID INTERNAL AUDIT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.826666666666666,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9354838709677427,
			"nilai_komparatif_unit": 0.9386851374409467,
			"summary_team": 0.7733333333333333
		}]
	},
	"660189": {
		"nik": "660189",
		"nama": "SUGENG WIDODO",
		"band": "II",
		"posisi": "GM WITEL PAPUA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8439215686274537,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.900557620817841,
			"nilai_komparatif_unit": 0.9036393681447407,
			"summary_team": 0.76
		}]
	},
	"660198": {
		"nik": "660198",
		"nama": "MUHARTONO",
		"band": "II",
		"posisi": "SVP BUSINESS EFFECTIVENESS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8475490196078479,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9716599190283348,
			"nilai_komparatif_unit": 0.9749849815106242,
			"summary_team": 0.8235294117647061
		}]
	},
	"660239": {
		"nik": "660239",
		"nama": "AGENG HARI MARHENDRA, IR",
		"band": "II",
		"posisi": "OSM MANAGED SERVICE OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8402777777777762,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9838016528925638,
			"nilai_komparatif_unit": 0.9871682649159542,
			"summary_team": 0.8266666666666667
		}]
	},
	"660259": {
		"nik": "660259",
		"nama": "Lina Herliana",
		"band": "II",
		"posisi": "SM KEUANGAN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0034220434713563,
			"summary_team": 0.9000000000000002
		}]
	},
	"660300": {
		"nik": "660300",
		"nama": "ATTAS BUDY, IR, MM",
		"band": "II",
		"posisi": "GM OPERATION TMS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8475490196078479,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9681007251857393,
			"nilai_komparatif_unit": 0.9714136079519763,
			"summary_team": 0.8205128205128199
		}]
	},
	"660316": {
		"nik": "660316",
		"nama": "IRWAN SUSILAWAN",
		"band": "II",
		"posisi": "KABID PENGELOLAAN INFORMASI TERPADU",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.826666666666666,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9491315136476435,
			"nilai_komparatif_unit": 0.9523794829473797,
			"summary_team": 0.7846153846153846
		}]
	},
	"660322": {
		"nik": "660322",
		"nama": "DECKY AMBARDI",
		"band": "II",
		"posisi": "OSM ACCESS MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8740740740740729,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9152542372881369,
			"nilai_komparatif_unit": 0.9183862770754798,
			"summary_team": 0.8
		}]
	},
	"660337": {
		"nik": "660337",
		"nama": "AGOES KOESRIJANTO, IR. M.T",
		"band": "II",
		"posisi": "OSM MANAGED SERVICE OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8526315789473679,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0164609053497948,
			"nilai_komparatif_unit": 1.0199392787548358,
			"summary_team": 0.8666666666666666
		}]
	},
	"660366": {
		"nik": "660366",
		"nama": "BAMBANG SUPRIYADI",
		"band": "II",
		"posisi": "OSM REGIONAL NETWORK OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8526315789473679,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9520697167755999,
			"nilai_komparatif_unit": 0.9553277407341677,
			"summary_team": 0.811764705882353
		}]
	},
	"660370": {
		"nik": "660370",
		"nama": "DJONI",
		"band": "II",
		"posisi": "GM WITEL PAPUA BARAT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8439215686274537,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9637546468401458,
			"nilai_komparatif_unit": 0.9670526571373542,
			"summary_team": 0.8133333333333335
		}]
	},
	"660414": {
		"nik": "660414",
		"nama": "MOH.ANWAR",
		"band": "II",
		"posisi": "CONSUMER EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.859047619047618,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9977827050997794,
			"nilai_komparatif_unit": 1.0011971608915982,
			"summary_team": 0.857142857142857
		}]
	},
	"660415": {
		"nik": "660415",
		"nama": "MOHAMAD JUSRAN HOLLE",
		"band": "II",
		"posisi": "GM WITEL KEDIRI",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8673267326732697,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.060730593607303,
			"nilai_komparatif_unit": 1.0643604598100247,
			"summary_team": 0.92
		}]
	},
	"660422": {
		"nik": "660422",
		"nama": "SAHAT CHARLES NAPITUPULU, IR",
		"band": "II",
		"posisi": "OSM REG ENTERPRISE,GOVERNMENT & BIZ SERV",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8294117647058815,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9886524822695046,
			"nilai_komparatif_unit": 0.992035694041895,
			"summary_team": 0.8200000000000001
		}]
	},
	"660429": {
		"nik": "660429",
		"nama": "MOUTIA DESYANTO, IR",
		"band": "II",
		"posisi": "OSM REGIONAL NETWORK OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8740740740740729,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9610169491525438,
			"nilai_komparatif_unit": 0.9643055909292538,
			"summary_team": 0.8400000000000001
		}]
	},
	"660438": {
		"nik": "660438",
		"nama": "WASITO ADI",
		"band": "II",
		"posisi": "OSM REGIONAL WHOLESALE SERVICE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.852777777777777,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.016286644951141,
			"nilai_komparatif_unit": 1.0197644220295223,
			"summary_team": 0.8666666666666667
		}]
	},
	"660645": {
		"nik": "660645",
		"nama": "Handrianus Eddy Sunaryo",
		"band": "II",
		"posisi": "SENIOR ADVISOR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8444444444444437,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9473684210526322,
			"nilai_komparatif_unit": 0.9506103569728643,
			"summary_team": 0.7999999999999998
		}]
	},
	"660651": {
		"nik": "660651",
		"nama": "Itut Triatmadi",
		"band": "II",
		"posisi": "SM LAYANAN KESEHATAN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8444444444444437,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0105263157894746,
			"nilai_komparatif_unit": 1.0139843807710556,
			"summary_team": 0.8533333333333333
		}]
	},
	"670037": {
		"nik": "670037",
		"nama": "HAMKA KARIM",
		"band": "II",
		"posisi": "OSM REGIONAL WHOLESALE SERVICE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8121212121212115,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0466417910447772,
			"nilai_komparatif_unit": 1.0502234447526704,
			"summary_team": 0.8500000000000001
		}]
	},
	"670045": {
		"nik": "670045",
		"nama": "AGUSTINUS",
		"band": "II",
		"posisi": "VP HUMAN RESOURCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8615384615384616,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9487577639751552,
			"nilai_komparatif_unit": 0.9520044542872648,
			"summary_team": 0.817391304347826
		}]
	},
	"670062": {
		"nik": "670062",
		"nama": "JEFFRY HASIBUAN",
		"band": "II",
		"posisi": "DIREKTUR UTAMA PT. SANDHY PUTRAMAKMUR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8619047619047623,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0607734806629832,
			"nilai_komparatif_unit": 1.0644034936270736,
			"summary_team": 0.9142857142857144
		}]
	},
	"670066": {
		"nik": "670066",
		"nama": "KETUT DARMA PUTRA, SE",
		"band": "II",
		"posisi": "VP TREASURY & TAX",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8615384615384616,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0093167701863353,
			"nilai_komparatif_unit": 1.0127706960502816,
			"summary_team": 0.8695652173913042
		}]
	},
	"670085": {
		"nik": "670085",
		"nama": "LINSON PARLINDUNGAN SITOMPUL",
		"band": "II",
		"posisi": "GM WITEL CIREBON",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8271929824561421,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9955710810305013,
			"nilai_komparatif_unit": 0.9989779685486126,
			"summary_team": 0.823529411764706
		}]
	},
	"670122": {
		"nik": "670122",
		"nama": "ARIEF YULIANTO",
		"band": "II",
		"posisi": "GM WITEL KALSEL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.865608465608466,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.00122249388753,
			"nilai_komparatif_unit": 1.0046487207861128,
			"summary_team": 0.8666666666666666
		}]
	},
	"670126": {
		"nik": "670126",
		"nama": "ANDIJOKO TJAHJONO",
		"band": "II",
		"posisi": "WAKIL REKTOR 2 BIDANG SUMBER DAYA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.925925925925926,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0151999999999999,
			"nilai_komparatif_unit": 1.0186740585321206,
			"summary_team": 0.9400000000000001
		}]
	},
	"670155": {
		"nik": "670155",
		"nama": "SETYA HERMAWAN",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8673267326732697,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9882583170254374,
			"nilai_komparatif_unit": 0.9916401799472276,
			"summary_team": 0.857142857142857
		}]
	},
	"670207": {
		"nik": "670207",
		"nama": "GATOT RAHMANTO",
		"band": "II",
		"posisi": "SM GENERAL AFFAIR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8271929824561421,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9671261930010585,
			"nilai_komparatif_unit": 0.9704357408757953,
			"summary_team": 0.8000000000000002
		}]
	},
	"670228": {
		"nik": "670228",
		"nama": "NURYADIN SALAM, IR,MM",
		"band": "II",
		"posisi": "GM WITEL MAKASSAR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8439215686274537,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9953531598512979,
			"nilai_komparatif_unit": 0.9987593016336607,
			"summary_team": 0.84
		}]
	},
	"670243": {
		"nik": "670243",
		"nama": "KOKOK MAYNARKO",
		"band": "II",
		"posisi": "GM WITEL BABEL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8349726775956329,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0299738219895236,
			"nilai_komparatif_unit": 1.0334984371827305,
			"summary_team": 0.8600000000000002
		}]
	},
	"670268": {
		"nik": "670268",
		"nama": "AGUS WASISA WIDOYOKO,IR",
		"band": "II",
		"posisi": "VP INTERNAL AUDIT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8475490196078479,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.013302486986692,
			"nilai_komparatif_unit": 1.0167700521467937,
			"summary_team": 0.8588235294117649
		}]
	},
	"670271": {
		"nik": "670271",
		"nama": "SONY BUDI WINARSO, MT",
		"band": "II",
		"posisi": "OSM REGIONAL WHOLESALE SERVICE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8271929824561421,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0382384130746656,
			"nilai_komparatif_unit": 1.0417913100578389,
			"summary_team": 0.8588235294117648
		}]
	},
	"670275": {
		"nik": "670275",
		"nama": "MALIK KAMIL M.P",
		"band": "II",
		"posisi": "OSM ACCESS MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.87037037037037,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9651063829787236,
			"nilai_komparatif_unit": 0.96840901897576,
			"summary_team": 0.8399999999999999
		}]
	},
	"670278": {
		"nik": "670278",
		"nama": "Ardya Prahasta",
		"band": "II",
		"posisi": "SM GENERAL AFFAIR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8444444444444437,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0105263157894746,
			"nilai_komparatif_unit": 1.0139843807710556,
			"summary_team": 0.8533333333333334
		}]
	},
	"670284": {
		"nik": "670284",
		"nama": "HARRY JULIAN SOPACUA,IR,MT",
		"band": "II",
		"posisi": "SM BUSINESS PLANNING & PERFORMANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8439215686274537,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0427509293680264,
			"nilai_komparatif_unit": 1.0463192683781208,
			"summary_team": 0.88
		}]
	},
	"670289": {
		"nik": "670289",
		"nama": "WIWIT WIDJANARKO, IR",
		"band": "II",
		"posisi": "OSM ACCESS MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8644444444444435,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0642673521850912,
			"nilai_komparatif_unit": 1.0679093213294135,
			"summary_team": 0.92
		}]
	},
	"680002": {
		"nik": "680002",
		"nama": "GUGUN GUNAWAN",
		"band": "II",
		"posisi": "VP ACCOUNTING & BUDGETING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8615384615384616,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.028061224489796,
			"nilai_komparatif_unit": 1.0315792946912157,
			"summary_team": 0.8857142857142858
		}]
	},
	"680011": {
		"nik": "680011",
		"nama": "FERRY TUMBELAKA",
		"band": "II",
		"posisi": "GM AREA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8475490196078479,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9548737676032607,
			"nilai_komparatif_unit": 0.9581413871456567,
			"summary_team": 0.8093023255813956
		}]
	},
	"680028": {
		"nik": "680028",
		"nama": "IWAN GUNAWAN",
		"band": "II",
		"posisi": "GM WITEL GORONTALO",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8439215686274537,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9479553903345694,
			"nilai_komparatif_unit": 0.9511993348892007,
			"summary_team": 0.8
		}]
	},
	"680039": {
		"nik": "680039",
		"nama": "FAISAL AGUNG PRABOWO",
		"band": "II",
		"posisi": "OSM PLANNING, ENGINEERING & DEPLOYMENT 1",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8740740740740729,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0067796610169506,
			"nilai_komparatif_unit": 1.0102249047830278,
			"summary_team": 0.88
		}]
	},
	"680057": {
		"nik": "680057",
		"nama": "SETYO PURWONO,IR",
		"band": "II",
		"posisi": "DEPUTY GM WITEL SEMARANG",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9618368630740147,
			"nilai_komparatif_unit": 0.9651283106318068,
			"summary_team": 0.8105263157894737
		}]
	},
	"680067": {
		"nik": "680067",
		"nama": "YANUARIADI KUSUMA BASKORO",
		"band": "II",
		"posisi": "VP PSE OPERATION CONTROL AND EVALUATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.925925925925926,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.99,
			"nilai_komparatif_unit": 0.9933878230366425,
			"summary_team": 0.9166666666666667
		}]
	},
	"680072": {
		"nik": "680072",
		"nama": "NUR ENDAH RINI, IR",
		"band": "II",
		"posisi": "SM BUSINESS PLANNING & PERFORMANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8673267326732697,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.014611872146116,
			"nilai_komparatif_unit": 1.0180839180791539,
			"summary_team": 0.88
		}]
	},
	"680079": {
		"nik": "680079",
		"nama": "SOERACHMAD ADI WAHJONO, IR.MT.",
		"band": "II",
		"posisi": "SM GENERAL AFFAIR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8338797814207654,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9726955002184356,
			"nilai_komparatif_unit": 0.9760241065045756,
			"summary_team": 0.8111111111111111
		}]
	},
	"680149": {
		"nik": "680149",
		"nama": "SABRI RASYID",
		"band": "II",
		"posisi": "AVP EXTERNAL COMMUNICATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9111111111111114,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.039024390243902,
			"nilai_komparatif_unit": 1.0425799768751158,
			"summary_team": 0.9466666666666667
		}]
	},
	"680150": {
		"nik": "680150",
		"nama": "SYAHRUDDIN PODDING, IR.",
		"band": "II",
		"posisi": "DIRECTOR OF TELKOMPCC",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8619047619047623,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.044198895027624,
			"nilai_komparatif_unit": 1.0477721890391505,
			"summary_team": 0.9000000000000001
		}]
	},
	"680159": {
		"nik": "680159",
		"nama": "ARIS TRIYANTO, IR. MT.",
		"band": "II",
		"posisi": "VP SUBSIDIARY CONTROLLER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8888888888888888,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.05,
			"nilai_komparatif_unit": 1.053593145644924,
			"summary_team": 0.9333333333333333
		}]
	},
	"680172": {
		"nik": "680172",
		"nama": "PRIHASWARA",
		"band": "II",
		"posisi": "OSM PLANNING, ENGINEERING & DEPLOYMENT 2",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8740740740740729,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0067796610169506,
			"nilai_komparatif_unit": 1.0102249047830278,
			"summary_team": 0.88
		}]
	},
	"680349": {
		"nik": "680349",
		"nama": "ARISTO EDWARD P. PANGARIBUAN",
		"band": "II",
		"posisi": "SM GENERAL AFFAIR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8439215686274537,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0743494423791786,
			"nilai_komparatif_unit": 1.0780259128744274,
			"summary_team": 0.9066666666666666
		}]
	},
	"680584": {
		"nik": "680584",
		"nama": "BONIFATIUS HENDRIANTO, Ir.",
		"band": "II",
		"posisi": "GM WITEL SINGARAJA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8673267326732697,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.091476407914761,
			"nilai_komparatif_unit": 1.095211487630605,
			"summary_team": 0.9466666666666665
		}]
	},
	"680587": {
		"nik": "680587",
		"nama": "SOEMARJANTO",
		"band": "II",
		"posisi": "GM WITEL KUDUS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8338797814207654,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1032765399737874,
			"nilai_komparatif_unit": 1.107052000254505,
			"summary_team": 0.92
		}]
	},
	"690016": {
		"nik": "690016",
		"nama": "ARIF NURJAYANTO,IR, MBA",
		"band": "II",
		"posisi": "OSM CONSUMER MARKETING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8511111111111103,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9869451697127947,
			"nilai_komparatif_unit": 0.9903225389873967,
			"summary_team": 0.84
		}]
	},
	"690018": {
		"nik": "690018",
		"nama": "ICHWAN MUHAMMADIAH",
		"band": "II",
		"posisi": "GM WITEL SULTENG",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8439215686274537,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9479553903345694,
			"nilai_komparatif_unit": 0.9511993348892007,
			"summary_team": 0.8
		}]
	},
	"690060": {
		"nik": "690060",
		"nama": "HENDAR ABY REINARDI",
		"band": "II",
		"posisi": "EGM PROPERTY DEVELOPMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8293333333333328,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9646302250803865,
			"nilai_komparatif_unit": 0.9679312316443957,
			"summary_team": 0.8
		}]
	},
	"690085": {
		"nik": "690085",
		"nama": "ABDUL MANAN",
		"band": "III",
		"posisi": "MGR SALES OPERATION MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8453333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0173501577287078,
			"nilai_komparatif_unit": 1.0227412352580862,
			"summary_team": 0.8600000000000001
		}]
	},
	"690293": {
		"nik": "690293",
		"nama": "SIGIT SHALAKO ABDURAJAK",
		"band": "II",
		"posisi": "DEPUTY GM WITEL JAKBAR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8338461538461546,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0193726937269365,
			"nilai_komparatif_unit": 1.0228610313983835,
			"summary_team": 0.8500000000000001
		}]
	},
	"690607": {
		"nik": "690607",
		"nama": "LUTHFY HANUM",
		"band": "II",
		"posisi": "SM PAYMENT COLLECTION & FINANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8673267326732697,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.983866057838658,
			"nilai_komparatif_unit": 0.9872328902585736,
			"summary_team": 0.8533333333333334
		}]
	},
	"700030": {
		"nik": "700030",
		"nama": "DWI HARTONO, IR",
		"band": "II",
		"posisi": "DEPUTY GM WITEL YOGYAKARTA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8807843137254925,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0596616206589469,
			"nilai_komparatif_unit": 1.0632878287897694,
			"summary_team": 0.9333333333333336
		}]
	},
	"700038": {
		"nik": "700038",
		"nama": "NASIMIN",
		"band": "III",
		"posisi": "SO CFU SYNERGY MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8565656565656555,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.946933962264152,
			"nilai_komparatif_unit": 0.9519518947498212,
			"summary_team": 0.8111111111111111
		}]
	},
	"700077": {
		"nik": "700077",
		"nama": "CHANDRA WIJAYA",
		"band": "III",
		"posisi": "SO PLASA & FACILITIES MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.877777777777777,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9493670886075958,
			"nilai_komparatif_unit": 0.9543979145622998,
			"summary_team": 0.8333333333333334
		}]
	},
	"700221": {
		"nik": "700221",
		"nama": "AHMAD MARGI DHAHONO",
		"band": "III",
		"posisi": "MGR PERFORMANCE & RISK MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8565656565656555,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9339622641509445,
			"nilai_komparatif_unit": 0.9389114578354402,
			"summary_team": 0.8
		}]
	},
	"700266": {
		"nik": "700266",
		"nama": "RAHAYU SYAHRAINI PUTRI",
		"band": "II",
		"posisi": "CONSUMER EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8499999999999989,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.005347593582889,
			"nilai_komparatif_unit": 1.008787936751953,
			"summary_team": 0.8545454545454546
		}]
	},
	"700296": {
		"nik": "700296",
		"nama": "TETRA RACHMAWATI, IR",
		"band": "II",
		"posisi": "SM SUPPLY SYSTEM",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0034220434713563,
			"summary_team": 0.8
		}]
	},
	"700297": {
		"nik": "700297",
		"nama": "WACHID SOESANTO",
		"band": "II",
		"posisi": "DEPUTY GM WITEL JAKPUS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8395480225988695,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9528936742934058,
			"nilai_komparatif_unit": 0.956154517870418,
			"summary_team": 0.8
		}]
	},
	"700386": {
		"nik": "700386",
		"nama": "EDY KUSNADI",
		"band": "III",
		"posisi": "SO MARKETING SUPPORT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8704761904761896,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9956236323851213,
			"nilai_komparatif_unit": 1.0008995780872902,
			"summary_team": 0.8666666666666667
		}]
	},
	"700466": {
		"nik": "700466",
		"nama": "I KOMANG WIDNYANA KARANG",
		"band": "II",
		"posisi": "GM WITEL SURABAYA SELATAN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8673267326732697,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.060730593607303,
			"nilai_komparatif_unit": 1.0643604598100247,
			"summary_team": 0.9199999999999999
		}]
	},
	"700645": {
		"nik": "700645",
		"nama": "MUSKAB MUZAKKAR",
		"band": "II",
		"posisi": "GM WITEL BOGOR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8482758620689653,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9430894308943092,
			"nilai_komparatif_unit": 0.946316723924206,
			"summary_team": 0.8
		}]
	},
	"700653": {
		"nik": "700653",
		"nama": "I NYOMAN HARDIANA ARTHA,ST",
		"band": "II",
		"posisi": "GM WITEL PASURUAN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8673267326732697,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.999238964992387,
			"nilai_komparatif_unit": 1.0026584041688638,
			"summary_team": 0.8666666666666667
		}]
	},
	"700657": {
		"nik": "700657",
		"nama": "HENDRAWAN",
		"band": "II",
		"posisi": "OSM CONSUMER MARKETING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8271929824561421,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9187698833510054,
			"nilai_komparatif_unit": 0.9219139538320054,
			"summary_team": 0.76
		}]
	},
	"700658": {
		"nik": "700658",
		"nama": "BIMO TEDJO LAKSITO, MH",
		"band": "II",
		"posisi": "VP LEGAL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8475490196078479,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1104684788895254,
			"nilai_komparatif_unit": 1.114268550297856,
			"summary_team": 0.9411764705882354
		}]
	},
	"700670": {
		"nik": "700670",
		"nama": "AGUNG WIJANARKO",
		"band": "II",
		"posisi": "SM PROCUREMENT CONTRACT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.980769230769231,
			"nilai_komparatif_unit": 0.9841254657122918,
			"summary_team": 0.8500000000000001
		}]
	},
	"710162": {
		"nik": "710162",
		"nama": "KASIRUN",
		"band": "II",
		"posisi": "GM WITEL KALTENG",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.865608465608466,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0320293398533003,
			"nilai_komparatif_unit": 1.035560989117993,
			"summary_team": 0.8933333333333333
		}]
	},
	"710189": {
		"nik": "710189",
		"nama": "PEPIH SUPRIATNA",
		"band": "III",
		"posisi": "SO MARKETING ACTIVATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8704761904761896,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9956236323851213,
			"nilai_komparatif_unit": 1.0008995780872902,
			"summary_team": 0.8666666666666667
		}]
	},
	"710245": {
		"nik": "710245",
		"nama": "NOLIS",
		"band": "III",
		"posisi": "SO CHANNEL MARKETING ACTIVATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8704761904761896,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9700948212983234,
			"nilai_komparatif_unit": 0.9752354863414622,
			"summary_team": 0.8444444444444444
		}]
	},
	"710365": {
		"nik": "710365",
		"nama": "FIRDAUS",
		"band": "II",
		"posisi": "DIREKTUR UTAMA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8559999999999994,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8920985556499583,
			"nilai_komparatif_unit": 0.8951513556881264,
			"summary_team": 0.7636363636363638
		}]
	},
	"710368": {
		"nik": "710368",
		"nama": "LILIK RETNO AGUSTININGSIH,ST",
		"band": "II",
		"posisi": "OSM CUSTOMER CARE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.852777777777777,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9381107491856687,
			"nilai_komparatif_unit": 0.9413210049503286,
			"summary_team": 0.8
		}]
	},
	"710372": {
		"nik": "710372",
		"nama": "DJOKO SRIE HANDONO",
		"band": "II",
		"posisi": "GM WITEL JAKSEL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8482758620689653,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9430894308943092,
			"nilai_komparatif_unit": 0.946316723924206,
			"summary_team": 0.8
		}]
	},
	"710379": {
		"nik": "710379",
		"nama": "SUDJI DJUWARTONO",
		"band": "II",
		"posisi": "KABID PRANDAL INVESTASI",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8559999999999994,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0591900311526488,
			"nilai_komparatif_unit": 1.06281462548368,
			"summary_team": 0.9066666666666667
		}]
	},
	"710389": {
		"nik": "710389",
		"nama": "AGUS DWI CAHYONO",
		"band": "II",
		"posisi": "KABID MANAJEMEN KEUANGAN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8181818181818186,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9981481481481479,
			"nilai_komparatif_unit": 1.0015638545019645,
			"summary_team": 0.8166666666666669
		}]
	},
	"710390": {
		"nik": "710390",
		"nama": "MOHAMMAD SYIBLI",
		"band": "II",
		"posisi": "DEPUTY EVP MARKETING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8439215686274537,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9742874845105299,
			"nilai_komparatif_unit": 0.9776215386361231,
			"summary_team": 0.8222222222222224
		}]
	},
	"710397": {
		"nik": "710397",
		"nama": "LONELY BARINGIN MANGARANAP,ST",
		"band": "II",
		"posisi": "GM WITEL SULUT & MALUT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8439215686274537,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9637546468401458,
			"nilai_komparatif_unit": 0.9670526571373542,
			"summary_team": 0.8133333333333335
		}]
	},
	"710399": {
		"nik": "710399",
		"nama": "PRIBADI NIRWANA, ST, MT",
		"band": "II",
		"posisi": "GM WITEL SOLO",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8338797814207654,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9913499344692,
			"nilai_komparatif_unit": 0.9947423770402796,
			"summary_team": 0.8266666666666665
		}]
	},
	"710403": {
		"nik": "710403",
		"nama": "AMIN SOEBAGYO, ST, MM",
		"band": "II",
		"posisi": "GM WITEL JAKBAR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8482758620689653,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.958807588075881,
			"nilai_komparatif_unit": 0.9620886693229427,
			"summary_team": 0.8133333333333334
		}]
	},
	"710432": {
		"nik": "710432",
		"nama": "KHADZIQOH EKO PUTRI, ST, MM",
		"band": "II",
		"posisi": "DEPUTY GM WITEL BOGOR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8571428571428559,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9800000000000015,
			"nilai_komparatif_unit": 0.9833536026019305,
			"summary_team": 0.8400000000000001
		}]
	},
	"710451": {
		"nik": "710451",
		"nama": "HERIBERTUS HANDOKO,ST",
		"band": "II",
		"posisi": "GM WITEL BENGKULU",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8349726775956329,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9581151832460685,
			"nilai_komparatif_unit": 0.9613938950537029,
			"summary_team": 0.8000000000000003
		}]
	},
	"710455": {
		"nik": "710455",
		"nama": "WANDIJANA",
		"band": "II",
		"posisi": "OSM PLANNING, ENGINEERING & DEPLOYMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.87037037037037,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0263829787234047,
			"nilai_komparatif_unit": 1.0298953058948561,
			"summary_team": 0.8933333333333333
		}]
	},
	"710461": {
		"nik": "710461",
		"nama": "SINAR JAKIN BARUMBUN KALASUSO",
		"band": "II",
		"posisi": "OSM REG ENTERPRISE,GOVERNMENT & BIZ SERV",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8121212121212115,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9850746268656725,
			"nilai_komparatif_unit": 0.9884455950613367,
			"summary_team": 0.8
		}]
	},
	"710462": {
		"nik": "710462",
		"nama": "REMIGIUS ADAM WIDODO",
		"band": "II",
		"posisi": "GM WITEL SUMUT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8349726775956329,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9341623036649168,
			"nilai_komparatif_unit": 0.9373590476773603,
			"summary_team": 0.7800000000000002
		}]
	},
	"710491": {
		"nik": "710491",
		"nama": "SRI PARWATI",
		"band": "III",
		"posisi": "SR EXPERT MARKETING & SALES PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8453333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1041009463722407,
			"nilai_komparatif_unit": 1.1099517281870699,
			"summary_team": 0.9333333333333331
		}]
	},
	"710515": {
		"nik": "710515",
		"nama": "TEDI RUKMANTARA",
		"band": "II",
		"posisi": "GM WITEL BANDUNG BARAT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8271929824561421,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0154825026511114,
			"nilai_komparatif_unit": 1.018957527919585,
			"summary_team": 0.8400000000000001
		}]
	},
	"710519": {
		"nik": "710519",
		"nama": "MUHAMMAD ZAIDAN JAUHARI",
		"band": "II",
		"posisi": "GM WITEL JAMBI",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8349726775956329,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0778795811518265,
			"nilai_komparatif_unit": 1.0815681319354151,
			"summary_team": 0.8999999999999998
		}]
	},
	"710529": {
		"nik": "710529",
		"nama": "ANT EDI WIDODO",
		"band": "II",
		"posisi": "OSM PLANNING, ENGINEERING & DEPLOYMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8645161290322574,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9510779436152579,
			"nilai_komparatif_unit": 0.9543325736829572,
			"summary_team": 0.8222222222222223
		}]
	},
	"710531": {
		"nik": "710531",
		"nama": "MOCHAMAD ARIEF WIDHIYANTO",
		"band": "II",
		"posisi": "SM HUMAN CAPITAL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.865608465608466,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.985819070904645,
			"nilai_komparatif_unit": 0.9891925866201726,
			"summary_team": 0.8533333333333334
		}]
	},
	"720067": {
		"nik": "720067",
		"nama": "SONYA SORAYA",
		"band": "II",
		"posisi": "OSM REGIONAL WHOLESALE SERVICE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8499999999999989,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882366,
			"nilai_komparatif_unit": 0.9443972173848071,
			"summary_team": 0.8
		}]
	},
	"720073": {
		"nik": "720073",
		"nama": "PRIO SESANTO",
		"band": "II",
		"posisi": "GM WITEL KALBAR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.865608465608466,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0320293398533003,
			"nilai_komparatif_unit": 1.035560989117993,
			"summary_team": 0.8933333333333333
		}]
	},
	"720092": {
		"nik": "720092",
		"nama": "MADE OKAGAMA",
		"band": "II",
		"posisi": "GM WITEL JAKTIM",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8482758620689653,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0216802168021684,
			"nilai_komparatif_unit": 1.02517645091789,
			"summary_team": 0.8666666666666667
		}]
	},
	"720093": {
		"nik": "720093",
		"nama": "KIKI SUDIANA",
		"band": "II",
		"posisi": "VP HUMAN CAPITAL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8633333333333332,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9675221439927323,
			"nilai_komparatif_unit": 0.9708330468289751,
			"summary_team": 0.8352941176470587
		}]
	},
	"720099": {
		"nik": "720099",
		"nama": "MUHAMAD NASRUN IHSAN",
		"band": "II",
		"posisi": "GM WITEL JAKPUS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8482758620689653,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9430894308943092,
			"nilai_komparatif_unit": 0.946316723924206,
			"summary_team": 0.8
		}]
	},
	"720104": {
		"nik": "720104",
		"nama": "MUHAMMAD ASHARI, ST, MM",
		"band": "II",
		"posisi": "SM BUSINESS PLANNING & PERFORMANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8338797814207654,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9593709043250324,
			"nilai_komparatif_unit": 0.9626539132647869,
			"summary_team": 0.7999999999999999
		}]
	},
	"720114": {
		"nik": "720114",
		"nama": "PUTRO DEWANTO",
		"band": "II",
		"posisi": "GM WITEL SURABAYA UTARA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8673267326732697,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.045357686453574,
			"nilai_komparatif_unit": 1.0489349458997344,
			"summary_team": 0.9066666666666667
		}]
	},
	"720131": {
		"nik": "720131",
		"nama": "HARI PURWANTO, M.T",
		"band": "II",
		"posisi": "GM WITEL JAKUT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8482758620689653,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9430894308943092,
			"nilai_komparatif_unit": 0.946316723924206,
			"summary_team": 0.8
		}]
	},
	"720133": {
		"nik": "720133",
		"nama": "YUNIARI PRASETYAWATI",
		"band": "III",
		"posisi": "SO MARKET INTELLIGENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8916666666666663,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9345794392523369,
			"nilai_komparatif_unit": 0.9395319034320455,
			"summary_team": 0.8333333333333334
		}]
	},
	"720138": {
		"nik": "720138",
		"nama": "SUSILA SHANE BONA PASKA S. ST, MM",
		"band": "II",
		"posisi": "GM WITEL LAMPUNG",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8349726775956329,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0299738219895234,
			"nilai_komparatif_unit": 1.0334984371827303,
			"summary_team": 0.8600000000000001
		}]
	},
	"720146": {
		"nik": "720146",
		"nama": "JOKO BUDIARTO",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8644444444444435,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9383033419023147,
			"nilai_komparatif_unit": 0.9415142567276231,
			"summary_team": 0.8111111111111111
		}]
	},
	"720152": {
		"nik": "720152",
		"nama": "DEDDY SUHENDRY",
		"band": "II",
		"posisi": "OSM ACCESS MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8402777777777762,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0155371900826466,
			"nilai_komparatif_unit": 1.0190124024938882,
			"summary_team": 0.8533333333333334
		}]
	},
	"720180": {
		"nik": "720180",
		"nama": "SLAMET RIYANTO PARDI, ST",
		"band": "II",
		"posisi": "DEPUTY EVP INFRASTRUCTURE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.865608465608466,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.016625916870415,
			"nilai_komparatif_unit": 1.0201048549520528,
			"summary_team": 0.88
		}]
	},
	"720201": {
		"nik": "720201",
		"nama": "SATRIA UTAMA",
		"band": "II",
		"posisi": "DEPUTY GM WITEL JAKUT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8656084656084662,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1090464547677255,
			"nilai_komparatif_unit": 1.112841659947694,
			"summary_team": 0.96
		}]
	},
	"720205": {
		"nik": "720205",
		"nama": "MEYLA KUSUMADIARTI RR,ST",
		"band": "II",
		"posisi": "GM WITEL MADURA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8673267326732697,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9838660578386579,
			"nilai_komparatif_unit": 0.9872328902585735,
			"summary_team": 0.8533333333333333
		}]
	},
	"720232": {
		"nik": "720232",
		"nama": "T. RACHMAD ZULKARNAEN, ST",
		"band": "II",
		"posisi": "GM AREA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8475490196078479,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0435319066897955,
			"nilai_komparatif_unit": 1.047102918238235,
			"summary_team": 0.8844444444444443
		}]
	},
	"720257": {
		"nik": "720257",
		"nama": "TAUFAN UMBARA, MM",
		"band": "II",
		"posisi": "DIREKTUR LOGISTIK & ASSET",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8491228070175436,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0049586776859507,
			"nilai_komparatif_unit": 1.0083976899679086,
			"summary_team": 0.8533333333333334
		}]
	},
	"720271": {
		"nik": "720271",
		"nama": "RIDWAN KURNIAWAN",
		"band": "II",
		"posisi": "OSM ACCESS MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769228,
			"nilai_komparatif_unit": 0.9262357324350976,
			"summary_team": 0.7999999999999999
		}]
	},
	"720278": {
		"nik": "720278",
		"nama": "DIDIT SULISTYO, ST",
		"band": "II",
		"posisi": "DIREKTUR UTAMA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8475490196078479,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0511593669488348,
			"nilai_komparatif_unit": 1.0547564799978568,
			"summary_team": 0.890909090909091
		}]
	},
	"720302": {
		"nik": "720302",
		"nama": "KHAIRULLAH",
		"band": "II",
		"posisi": "OSM REG ENTERPRISE,GOVERNMENT & BIZ SERV",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.852777777777777,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9537459283387631,
			"nilai_komparatif_unit": 0.9570096883661673,
			"summary_team": 0.8133333333333334
		}]
	},
	"720307": {
		"nik": "720307",
		"nama": "PRASETIYO RAHARJO, MTI",
		"band": "II",
		"posisi": "TRIBE LEADER INNOVATION  MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8411764705882361,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9726636999364262,
			"nilai_komparatif_unit": 0.9759921974006187,
			"summary_team": 0.8181818181818181
		}]
	},
	"720308": {
		"nik": "720308",
		"nama": "YOHANES BASO BOROSI",
		"band": "II",
		"posisi": "OSM REGIONAL NETWORK OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8645161290322574,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9767827529021568,
			"nilai_komparatif_unit": 0.9801253459446588,
			"summary_team": 0.8444444444444446
		}]
	},
	"720309": {
		"nik": "720309",
		"nama": "WIJAYANTO, M.T",
		"band": "II",
		"posisi": "OSM REG ENTERPRISE,GOVERNMENT & BIZ SERV",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8511111111111103,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.908616187989557,
			"nilai_komparatif_unit": 0.9117255120836352,
			"summary_team": 0.7733333333333333
		}]
	},
	"720329": {
		"nik": "720329",
		"nama": "ARI SUDRAJAT",
		"band": "II",
		"posisi": "KABID MANAJEMEN PROGRAM & PELAYANAN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9133333333333336,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9671532846715327,
			"nilai_komparatif_unit": 0.9704629252551435,
			"summary_team": 0.8833333333333334
		}]
	},
	"720339": {
		"nik": "720339",
		"nama": "SONNY HIDAYAT,ST., M.MT",
		"band": "II",
		"posisi": "GM WITEL MALANG",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8673267326732697,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.983866057838658,
			"nilai_komparatif_unit": 0.9872328902585736,
			"summary_team": 0.8533333333333334
		}]
	},
	"720344": {
		"nik": "720344",
		"nama": "Ronaldi Amri",
		"band": "II",
		"posisi": "KETUA  KOPTEL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8545454545454537,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9574468085106395,
			"nilai_komparatif_unit": 0.960723233110874,
			"summary_team": 0.8181818181818183
		}]
	},
	"720347": {
		"nik": "720347",
		"nama": "SAMSURIZAL ARUNI,ST",
		"band": "II",
		"posisi": "GM WITEL NTT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8673267326732697,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.014611872146116,
			"nilai_komparatif_unit": 1.0180839180791539,
			"summary_team": 0.88
		}]
	},
	"720360": {
		"nik": "720360",
		"nama": "ERNAWANTI",
		"band": "II",
		"posisi": "OSM CUSTOMER CARE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8271929824561421,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9510074231177075,
			"nilai_komparatif_unit": 0.9542618118611986,
			"summary_team": 0.7866666666666667
		}]
	},
	"720370": {
		"nik": "720370",
		"nama": "MUSTADI,ST",
		"band": "II",
		"posisi": "GM WITEL MAGELANG",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8338797814207654,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9593709043250324,
			"nilai_komparatif_unit": 0.9626539132647869,
			"summary_team": 0.7999999999999999
		}]
	},
	"720371": {
		"nik": "720371",
		"nama": "DANIEL GIAT MARDONGAN",
		"band": "II",
		"posisi": "OSM CUSTOMER CARE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8309523809523801,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9627507163323793,
			"nilai_komparatif_unit": 0.9660452911357479,
			"summary_team": 0.8
		}]
	},
	"720377": {
		"nik": "720377",
		"nama": "ICHWAN MUTTAQIN",
		"band": "II",
		"posisi": "SM PLANNING & CONTROLLING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8411764705882361,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.050116550116549,
			"nilai_komparatif_unit": 1.0537100946010383,
			"summary_team": 0.8833333333333333
		}]
	},
	"720393": {
		"nik": "720393",
		"nama": "EKO YULIYANTO",
		"band": "II",
		"posisi": "DEPUTY GM WITEL JAKSEL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0615384615384624,
			"nilai_komparatif_unit": 1.0651710923003634,
			"summary_team": 0.9199999999999999
		}]
	},
	"720399": {
		"nik": "720399",
		"nama": "BAYUN ROYONG ROHADI",
		"band": "II",
		"posisi": "GM WITEL DENPASAR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8673267326732697,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.091476407914761,
			"nilai_komparatif_unit": 1.095211487630605,
			"summary_team": 0.9466666666666665
		}]
	},
	"720412": {
		"nik": "720412",
		"nama": "YOGA ISTANTO",
		"band": "II",
		"posisi": "GM WITEL BALIKPAPAN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.865608465608466,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9396088019559897,
			"nilai_komparatif_unit": 0.9428241841223519,
			"summary_team": 0.8133333333333334
		}]
	},
	"720415": {
		"nik": "720415",
		"nama": "SUBHAN",
		"band": "II",
		"posisi": "GM WITEL RIKEP",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8349726775956329,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0299738219895234,
			"nilai_komparatif_unit": 1.0334984371827303,
			"summary_team": 0.8600000000000001
		}]
	},
	"720441": {
		"nik": "720441",
		"nama": "AGUNG TRI CAHYONO",
		"band": "II",
		"posisi": "GM WITEL ACEH",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8349726775956329,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9700916230366443,
			"nilai_komparatif_unit": 0.9734113187418741,
			"summary_team": 0.8100000000000003
		}]
	},
	"720455": {
		"nik": "720455",
		"nama": "HENRY SOEDIDARMA",
		"band": "II",
		"posisi": "DEPUTY GM WITEL JAKTIM",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9514925373134329,
			"nilai_komparatif_unit": 0.9547485861387903,
			"summary_team": 0.8500000000000001
		}]
	},
	"720459": {
		"nik": "720459",
		"nama": "CHANDRA G.T. PURBA",
		"band": "II",
		"posisi": "VP STRATEGIC BUSINESS PLANNING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9199999999999999,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9239130434782612,
			"nilai_komparatif_unit": 0.9270747140767968,
			"summary_team": 0.8500000000000002
		}]
	},
	"720460": {
		"nik": "720460",
		"nama": "INDRIJO",
		"band": "II",
		"posisi": "OSM REGIONAL NETWORK OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8402777777777762,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9838016528925638,
			"nilai_komparatif_unit": 0.9871682649159542,
			"summary_team": 0.8266666666666667
		}]
	},
	"720470": {
		"nik": "720470",
		"nama": "DODE SUPARMAN",
		"band": "II",
		"posisi": "GM WITEL BANDUNG",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8271929824561421,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9671261930010585,
			"nilai_komparatif_unit": 0.9704357408757953,
			"summary_team": 0.8000000000000002
		}]
	},
	"720483": {
		"nik": "720483",
		"nama": "BAMBANG IRAWAN, ST",
		"band": "II",
		"posisi": "SENIOR ADVISOR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8293333333333328,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9746784565916403,
			"nilai_komparatif_unit": 0.9780138486406912,
			"summary_team": 0.8083333333333332
		}]
	},
	"720489": {
		"nik": "720489",
		"nama": "MOHAMMAD NOUVRIYAL",
		"band": "II",
		"posisi": "OSM CUSTOMER CARE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8499999999999989,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0352941176470603,
			"nilai_komparatif_unit": 1.0388369391232877,
			"summary_team": 0.88
		}]
	},
	"720491": {
		"nik": "720491",
		"nama": "BAMBANG SUNARYADI, ST",
		"band": "II",
		"posisi": "GM WITEL BANTEN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8482758620689653,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9430894308943092,
			"nilai_komparatif_unit": 0.946316723924206,
			"summary_team": 0.8
		}]
	},
	"720498": {
		"nik": "720498",
		"nama": "FAHRUROZI, ST.",
		"band": "II",
		"posisi": "VP SUPPLY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8615384615384616,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9926108374384235,
			"nilai_komparatif_unit": 0.9960075948742769,
			"summary_team": 0.8551724137931034
		}]
	},
	"720514": {
		"nik": "720514",
		"nama": "MUTIA",
		"band": "II",
		"posisi": "SM OPERATION & GENERAL AFFAIRS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8533333333333334,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9542410714285716,
			"nilai_komparatif_unit": 0.9575065258571536,
			"summary_team": 0.8142857142857145
		}]
	},
	"720538": {
		"nik": "720538",
		"nama": "BUDI WAHYUDI",
		"band": "II",
		"posisi": "SM GENERAL AFFAIR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8703703703703696,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9804255319148946,
			"nilai_komparatif_unit": 0.9837805907055348,
			"summary_team": 0.8533333333333334
		}]
	},
	"720551": {
		"nik": "720551",
		"nama": "FORTI FITRAYANA",
		"band": "II",
		"posisi": "VP INFORMATION TECHNOLOGY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8615384615384616,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0612244897959184,
			"nilai_komparatif_unit": 1.0648560461328678,
			"summary_team": 0.9142857142857145
		}]
	},
	"720559": {
		"nik": "720559",
		"nama": "HARRY PRIBADI",
		"band": "II",
		"posisi": "OSM REGIONAL OPERATION CENTER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8740740740740729,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0220338983050863,
			"nilai_komparatif_unit": 1.0255313427342858,
			"summary_team": 0.8933333333333334
		}]
	},
	"720561": {
		"nik": "720561",
		"nama": "ANDREYADI WIBOWO",
		"band": "II",
		"posisi": "OSM MANAGED SERVICE OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8645161290322574,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0281923714759544,
			"nilai_komparatif_unit": 1.0317108904680619,
			"summary_team": 0.888888888888889
		}]
	},
	"720569": {
		"nik": "720569",
		"nama": "AFDOL MUFTIASA, M.ENG.",
		"band": "II",
		"posisi": "AVP SERVICE OPERATION & SUPPORT AUDIT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8962962962962957,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9901859504132238,
			"nilai_komparatif_unit": 0.9935744097802639,
			"summary_team": 0.8875
		}]
	},
	"720573": {
		"nik": "720573",
		"nama": "ASEP GUNAWAN, M.ENG.",
		"band": "II",
		"posisi": "OSM REGIONAL NETWORK OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8473118279569885,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.955964467005077,
			"nilai_komparatif_unit": 0.9592358189682402,
			"summary_team": 0.8099999999999999
		}]
	},
	"730036": {
		"nik": "730036",
		"nama": "EVISTA M. ARISANDY, ST",
		"band": "II",
		"posisi": "VP PROPERTY MANAGEMENT OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8615384615384616,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9517857142857145,
			"nilai_komparatif_unit": 0.9550427663754159,
			"summary_team": 0.8200000000000002
		}]
	},
	"730039": {
		"nik": "730039",
		"nama": "NOVEL GERALDY MAENGKOM",
		"band": "II",
		"posisi": "AUDITOR UTAMA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8690476190476184,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9205479452054799,
			"nilai_komparatif_unit": 0.9236981002914406,
			"summary_team": 0.7999999999999998
		}]
	},
	"730043": {
		"nik": "730043",
		"nama": "CARLA HAPSARI",
		"band": "II",
		"posisi": "AVP WORKING ENVIRONMENT MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9037037037037031,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9405737704918039,
			"nilai_komparatif_unit": 0.9437924548224442,
			"summary_team": 0.85
		}]
	},
	"730045": {
		"nik": "730045",
		"nama": "I GUSTI BAGUS ARI BUDAYANA",
		"band": "II",
		"posisi": "DIREKTUR UTAMA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8615384615384616,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0551948051948052,
			"nilai_komparatif_unit": 1.058805727688931,
			"summary_team": 0.9090909090909092
		}]
	},
	"730046": {
		"nik": "730046",
		"nama": "NGAKAN NYOMAN MERTA, ST",
		"band": "II",
		"posisi": "AVP GCT COMMUNICATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8222222222222226,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9729729729729726,
			"nilai_komparatif_unit": 0.9763025287829407,
			"summary_team": 0.8
		}]
	},
	"730049": {
		"nik": "730049",
		"nama": "MUHAMMAD YUSUF",
		"band": "II",
		"posisi": "GM WITEL RIDAR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8349726775956329,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0285648290729852,
			"nilai_komparatif_unit": 1.0320846226311808,
			"summary_team": 0.8588235294117649
		}]
	},
	"730072": {
		"nik": "730072",
		"nama": "ALFI SUMARTA, ST",
		"band": "II",
		"posisi": "GM WITEL SUMBAR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8349726775956329,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9581151832460685,
			"nilai_komparatif_unit": 0.9613938950537029,
			"summary_team": 0.8000000000000003
		}]
	},
	"730073": {
		"nik": "730073",
		"nama": "SETYAWAN NUGROHO,ST",
		"band": "II",
		"posisi": "GM WITEL JEMBER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8673267326732697,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.014611872146116,
			"nilai_komparatif_unit": 1.0180839180791539,
			"summary_team": 0.88
		}]
	},
	"730079": {
		"nik": "730079",
		"nama": "KAMARIAH LATIEF",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8471014492753652,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0329341317365235,
			"nilai_komparatif_unit": 1.0364688772383734,
			"summary_team": 0.875
		}]
	},
	"730113": {
		"nik": "730113",
		"nama": "ACHMAD CHARIRI",
		"band": "II",
		"posisi": "SM WHOLESALE & INTERNATIONAL BILLING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.839285714285713,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9531914893617037,
			"nilai_komparatif_unit": 0.9564533520748262,
			"summary_team": 0.8
		}]
	},
	"730122": {
		"nik": "730122",
		"nama": "KOKOH KABUL AMIN",
		"band": "II",
		"posisi": "SM HUMAN CAPITAL BUSINESS PARTNER 05",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8460317460317445,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.04409005628518,
			"nilai_komparatif_unit": 1.0476629778457986,
			"summary_team": 0.8833333333333333
		}]
	},
	"730124": {
		"nik": "730124",
		"nama": "MUHAMAD YUSUF",
		"band": "II",
		"posisi": "SM PRICE & COST ANALYSIS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.7833333333333333,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9255319148936169,
			"nilai_komparatif_unit": 0.9286991253405102,
			"summary_team": 0.7249999999999999
		}]
	},
	"730145": {
		"nik": "730145",
		"nama": "NURWIDJAJADI",
		"band": "II",
		"posisi": "DIREKTUR KEUANGAN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8491228070175436,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9698590179873605,
			"nilai_komparatif_unit": 0.973177917708,
			"summary_team": 0.8235294117647058
		}]
	},
	"730147": {
		"nik": "730147",
		"nama": "DWI IRMIYANTI",
		"band": "III",
		"posisi": "MGR SALES SUPERVISION",
		"category": [{
			"code": "MD-3-01",
			"kriteria": 0.8453333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9463722397476351,
			"nilai_komparatif_unit": 0.9513871955889174,
			"summary_team": 0.8
		}]
	},
	"730148": {
		"nik": "730148",
		"nama": "RISMAN HERIYONO",
		"band": "II",
		"posisi": "KABID MANAJEMEN DATA&ADM KEPESERTAAN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9133333333333336,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9306569343065693,
			"nilai_komparatif_unit": 0.9338416827926853,
			"summary_team": 0.8500000000000001
		}]
	},
	"730177": {
		"nik": "730177",
		"nama": "BAYU AJI WIJAYA, ST",
		"band": "II",
		"posisi": "AVP STRATEGIC BUDGETING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.817142857142856,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0442890442890458,
			"nilai_komparatif_unit": 1.0478626467952639,
			"summary_team": 0.8533333333333334
		}]
	},
	"730183": {
		"nik": "730183",
		"nama": "KUS INDARTAMA, MMT",
		"band": "II",
		"posisi": "DEPUTY GM WITEL SURABAYA UTARA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96,
			"nilai_komparatif_unit": 0.9632851617325018,
			"summary_team": 0.8
		}]
	},
	"730184": {
		"nik": "730184",
		"nama": "DADANG SETIAWAN",
		"band": "II",
		"posisi": "DIREKTUR DIREKTORAT SISTEM INFORMASI",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9047619047619049,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.019298245614035,
			"nilai_komparatif_unit": 1.0227863285208032,
			"summary_team": 0.9222222222222222
		}]
	},
	"730191": {
		"nik": "730191",
		"nama": "HERI KURNIAWAN",
		"band": "II",
		"posisi": "GM WITEL TASIKMALAYA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8271929824561421,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0382384130746656,
			"nilai_komparatif_unit": 1.0417913100578389,
			"summary_team": 0.8588235294117648
		}]
	},
	"730202": {
		"nik": "730202",
		"nama": "KUSNADI KADAR, ST., MM.",
		"band": "II",
		"posisi": "OSM MANAGED SERVICE OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8473118279569885,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9441624365482245,
			"nilai_komparatif_unit": 0.9473934014501141,
			"summary_team": 0.8000000000000003
		}]
	},
	"730207": {
		"nik": "730207",
		"nama": "ERNA WIYATI",
		"band": "II",
		"posisi": "OSM REGIONAL WHOLESALE SERVICE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8511111111111103,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.002610966057442,
			"nilai_komparatif_unit": 1.006041944368149,
			"summary_team": 0.8533333333333332
		}]
	},
	"730209": {
		"nik": "730209",
		"nama": "YOTA YOEDI GOESTINNENDA",
		"band": "III",
		"posisi": "SO MARKETING COMMUNCIATION MEDIA MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8704761904761896,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9190371991247275,
			"nilai_komparatif_unit": 0.9239073028498064,
			"summary_team": 0.8
		}]
	},
	"730222": {
		"nik": "730222",
		"nama": "Heru Agung Nugrahanto",
		"band": "II",
		"posisi": "SM KEPESERTAAN & SISFO",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8444444444444437,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.931578947368422,
			"nilai_komparatif_unit": 0.9347668510233168,
			"summary_team": 0.7866666666666667
		}]
	},
	"730227": {
		"nik": "730227",
		"nama": "SABAR SISWANTO, ST",
		"band": "II",
		"posisi": "DEPUTY GM WITEL MAKASSAR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1058981233243923,
			"nilai_komparatif_unit": 1.1096825547772995,
			"summary_team": 0.9090909090909093
		}]
	},
	"730228": {
		"nik": "730228",
		"nama": "SIDIK WIDODO",
		"band": "II",
		"posisi": "OSM ACCESS MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8645161290322574,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9562189054726375,
			"nilai_komparatif_unit": 0.9594911281352975,
			"summary_team": 0.8266666666666667
		}]
	},
	"730236": {
		"nik": "730236",
		"nama": "BHERI PRIYO HARTANTO",
		"band": "II",
		"posisi": "OSM CONSUMER MARKETING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8499999999999989,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9882352941176484,
			"nilai_komparatif_unit": 0.9916170782540474,
			"summary_team": 0.8400000000000001
		}]
	},
	"730243": {
		"nik": "730243",
		"nama": "HARI SANDI ATMAJA",
		"band": "II",
		"posisi": "SM BUSINESS PLANNING, PERFORM & RISK MGT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8482758620689653,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0806233062330626,
			"nilai_komparatif_unit": 1.0843212461631526,
			"summary_team": 0.9166666666666667
		}]
	},
	"730256": {
		"nik": "730256",
		"nama": "M.ARIF HIDAYAT, ST",
		"band": "II",
		"posisi": "OSM REGIONAL OPERATION CENTER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8644444444444435,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0025706940874046,
			"nilai_komparatif_unit": 1.0060015345856794,
			"summary_team": 0.8666666666666666
		}]
	},
	"730264": {
		"nik": "730264",
		"nama": "ISMONO ADI JATMIKO, RADEN",
		"band": "II",
		"posisi": "GM WITEL SIDOARJO",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8673267326732697,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.060730593607303,
			"nilai_komparatif_unit": 1.0643604598100247,
			"summary_team": 0.9199999999999999
		}]
	},
	"730275": {
		"nik": "730275",
		"nama": "ADIEK WIDIASMARA",
		"band": "II",
		"posisi": "OSM CONSUMER MARKETING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8309523809523801,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9787965616045855,
			"nilai_komparatif_unit": 0.9821460459880103,
			"summary_team": 0.8133333333333334
		}]
	},
	"730276": {
		"nik": "730276",
		"nama": "RICHARD ALBERTO",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8279569892473108,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9783116883116897,
			"nilai_komparatif_unit": 0.981659513437628,
			"summary_team": 0.81
		}]
	},
	"730296": {
		"nik": "730296",
		"nama": "IMAM SUHADI",
		"band": "II",
		"posisi": "AVP REWARD MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8611111111111115,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0219354838709673,
			"nilai_komparatif_unit": 1.025432591521695,
			"summary_team": 0.88
		}]
	},
	"730304": {
		"nik": "730304",
		"nama": "AHMAD ARI MAKA SUCI, ST",
		"band": "II",
		"posisi": "OSM REGIONAL NETWORK OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.87037037037037,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9497872340425536,
			"nilai_komparatif_unit": 0.9530374472459863,
			"summary_team": 0.8266666666666667
		}]
	},
	"730309": {
		"nik": "730309",
		"nama": "Muhammad Suny Arifianto",
		"band": "II",
		"posisi": "DIREKTUR LAYANAN KESEHATAN & UMUM",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8914285714285706,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9294871794871805,
			"nilai_komparatif_unit": 0.9326679250214538,
			"summary_team": 0.8285714285714287
		}]
	},
	"730310": {
		"nik": "730310",
		"nama": "NURKHOLIS MAJID",
		"band": "II",
		"posisi": "KABID FIXED INCOME & PROPENSA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8559999999999994,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.950155763239876,
			"nilai_komparatif_unit": 0.9534072375662425,
			"summary_team": 0.8133333333333334
		}]
	},
	"730313": {
		"nik": "730313",
		"nama": "MUSTAKIM WAHYUDI",
		"band": "II",
		"posisi": "GM WITEL SUMSEL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8349726775956329,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9940445026177961,
			"nilai_komparatif_unit": 0.9974461661182168,
			"summary_team": 0.8300000000000003
		}]
	},
	"730318": {
		"nik": "730318",
		"nama": "ULI GARNADI",
		"band": "II",
		"posisi": "OSM PLANNING, ENGINEERING & DEPLOYMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8473118279569885,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9719319199761134,
			"nilai_komparatif_unit": 0.9752579132574702,
			"summary_team": 0.8235294117647061
		}]
	},
	"730327": {
		"nik": "730327",
		"nama": "EKO BUDIHARJO, S.T, M.M.",
		"band": "II",
		"posisi": "PRINCIPAL EXPERT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9883409273653125,
			"nilai_komparatif_unit": 0.991723072983277,
			"summary_team": 0.8428571428571427
		}]
	},
	"730332": {
		"nik": "730332",
		"nama": "HADI LESTARI",
		"band": "II",
		"posisi": "PRINCIPAL EXPERT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8471014492753652,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9443969204448215,
			"nilai_komparatif_unit": 0.9476286877607984,
			"summary_team": 0.8
		}]
	},
	"730333": {
		"nik": "730333",
		"nama": "NOPRIANTO NARPIAH MARBUN",
		"band": "II",
		"posisi": "GM AM SEGMENT EXO-2",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.901754385964912,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9895240945824607,
			"nilai_komparatif_unit": 0.9929102890500762,
			"summary_team": 0.8923076923076924
		}]
	},
	"730334": {
		"nik": "730334",
		"nama": "MUHAMMAD NASHIR",
		"band": "II",
		"posisi": "SM OPERATIONAL PROCUREMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882353,
			"nilai_komparatif_unit": 0.9443972173848058,
			"summary_team": 0.8
		}]
	},
	"730335": {
		"nik": "730335",
		"nama": "JOKO WIYONO",
		"band": "II",
		"posisi": "GM WITEL SEMARANG",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8338797814207654,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9593709043250324,
			"nilai_komparatif_unit": 0.9626539132647869,
			"summary_team": 0.7999999999999999
		}]
	},
	"730339": {
		"nik": "730339",
		"nama": "SULKAN",
		"band": "II",
		"posisi": "CONSUMER EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8370370370370359,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0035398230088508,
			"nilai_komparatif_unit": 1.006973979908424,
			"summary_team": 0.84
		}]
	},
	"730343": {
		"nik": "730343",
		"nama": "SYAFRIZAL MARTINIS",
		"band": "II",
		"posisi": "PROJECT TEAM",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8644444444444433,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9798880991985497,
			"nilai_komparatif_unit": 0.9832413188710716,
			"summary_team": 0.8470588235294119
		}]
	},
	"730345": {
		"nik": "730345",
		"nama": "ERI SUSANTO",
		"band": "II",
		"posisi": "CONSUMER EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8499999999999989,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882366,
			"nilai_komparatif_unit": 0.9443972173848071,
			"summary_team": 0.8
		}]
	},
	"730348": {
		"nik": "730348",
		"nama": "LEVI LAZUARDI SUBARSYAH,ST",
		"band": "II",
		"posisi": "GM AREA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8592592592592586,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.958417849898581,
			"nilai_komparatif_unit": 0.9616975974446577,
			"summary_team": 0.8235294117647061
		}]
	},
	"730357": {
		"nik": "730357",
		"nama": "SUGENG YUNIANTO,ST",
		"band": "II",
		"posisi": "SM GENERAL AFFAIR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.865608465608466,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0130712807974416,
			"nilai_komparatif_unit": 1.0165380547599128,
			"summary_team": 0.8769230769230769
		}]
	},
	"730358": {
		"nik": "730358",
		"nama": "HANANTO",
		"band": "III",
		"posisi": "AVP PLANNING & BUSINESS DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8765957446808497,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0520496224379736,
			"nilai_komparatif_unit": 1.0576245771733017,
			"summary_team": 0.9222222222222222
		}]
	},
	"730360": {
		"nik": "730360",
		"nama": "DONY TRI SUSETYO NIRBOYO",
		"band": "II",
		"posisi": "SSO BUSINESS SUPPORT COORDINATOR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8156862745098039,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9644230769230769,
			"nilai_komparatif_unit": 0.9677233746170867,
			"summary_team": 0.7866666666666666
		}]
	},
	"730365": {
		"nik": "730365",
		"nama": "ANUNG ASMORO",
		"band": "II",
		"posisi": "AVP INVESTMENT MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.817142857142856,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9790209790209805,
			"nilai_komparatif_unit": 0.9823712313705599,
			"summary_team": 0.8
		}]
	},
	"730366": {
		"nik": "730366",
		"nama": "JIWANSYAH",
		"band": "II",
		"posisi": "OSM CONSUMER MARKETING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8294117647058815,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9645390070921993,
			"nilai_komparatif_unit": 0.9678397015042874,
			"summary_team": 0.7999999999999998
		}]
	},
	"730373": {
		"nik": "730373",
		"nama": "FRIDH ZURRIYADI RIDWAN",
		"band": "II",
		"posisi": "AVP HC PLANNING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8279569892473108,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0266233766233779,
			"nilai_komparatif_unit": 1.0301365264468936,
			"summary_team": 0.85
		}]
	},
	"730374": {
		"nik": "730374",
		"nama": "SRI WIDODO,ST",
		"band": "II",
		"posisi": "GM WITEL SULSELBAR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8439215686274537,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9479553903345694,
			"nilai_komparatif_unit": 0.9511993348892007,
			"summary_team": 0.8
		}]
	},
	"730392": {
		"nik": "730392",
		"nama": "ROBIT CAHYONO",
		"band": "II",
		"posisi": "SVP CORPORATE AFFAIR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8592592592592586,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9892241379310354,
			"nilai_komparatif_unit": 0.9926093059339501,
			"summary_team": 0.8500000000000001
		}]
	},
	"730394": {
		"nik": "730394",
		"nama": "ANTONIUS DWI ANANTO",
		"band": "II",
		"posisi": "SM SOURCING & SUPPLIER MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8400000000000001,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9956709956709958,
			"nilai_komparatif_unit": 0.9990782251013504,
			"summary_team": 0.8363636363636365
		}]
	},
	"730396": {
		"nik": "730396",
		"nama": "DENY ARYANTO",
		"band": "II",
		"posisi": "GM WITEL SAMARINDA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.865608465608466,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9704156479217599,
			"nilai_komparatif_unit": 0.9737364524542323,
			"summary_team": 0.8400000000000001
		}]
	},
	"730397": {
		"nik": "730397",
		"nama": "RIZKY KURNIAWAN",
		"band": "II",
		"posisi": "SM GENERAL AFFAIR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8349726775956329,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9847294938917922,
			"nilai_komparatif_unit": 0.9880992810274164,
			"summary_team": 0.8222222222222222
		}]
	},
	"730411": {
		"nik": "730411",
		"nama": "MUHAMAD SABANI, ST",
		"band": "II",
		"posisi": "SENIOR STAFF II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8645161290322574,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.925373134328359,
			"nilai_komparatif_unit": 0.9285398014212557,
			"summary_team": 0.8
		}]
	},
	"730425": {
		"nik": "730425",
		"nama": "YUDDY ARYADI, MENG",
		"band": "II",
		"posisi": "AVP CULTURAL DEVELOPMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8279569892473108,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0789610389610405,
			"nilai_komparatif_unit": 1.0826532905402646,
			"summary_team": 0.8933333333333334
		}]
	},
	"730452": {
		"nik": "730452",
		"nama": "MUNAWWIR",
		"band": "II",
		"posisi": "OSM REGIONAL WHOLESALE SERVICE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8294117647058815,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9886524822695046,
			"nilai_komparatif_unit": 0.992035694041895,
			"summary_team": 0.8200000000000001
		}]
	},
	"730454": {
		"nik": "730454",
		"nama": "MOCHAMAD MA'RUFIN, ST",
		"band": "II",
		"posisi": "SENIOR STAFF II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8511111111111103,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0052219321148834,
			"nilai_komparatif_unit": 1.0086618452649412,
			"summary_team": 0.8555555555555555
		}]
	},
	"730461": {
		"nik": "730461",
		"nama": "AGUS FAISAL, ST",
		"band": "II",
		"posisi": "GM WITEL YOGYAKARTA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8338797814207654,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9913499344692,
			"nilai_komparatif_unit": 0.9947423770402796,
			"summary_team": 0.8266666666666665
		}]
	},
	"730462": {
		"nik": "730462",
		"nama": "I GUSTI GDE DONNY WARDHANA",
		"band": "II",
		"posisi": "DEPUTY GM WITEL DENPASAR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8754098360655743,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9823970037453178,
			"nilai_komparatif_unit": 0.9857588089982643,
			"summary_team": 0.8600000000000001
		}]
	},
	"730465": {
		"nik": "730465",
		"nama": "USEP MAMAN IDAYAT",
		"band": "II",
		"posisi": "OSM PLANNING, ENGINEERING & DEPLOYMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8644444444444435,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0179948586118264,
			"nilai_komparatif_unit": 1.0214784812716131,
			"summary_team": 0.88
		}]
	},
	"730495": {
		"nik": "730495",
		"nama": "HERY SUDARMAJI",
		"band": "II",
		"posisi": "SM HUMAN CAPITAL BUDGET",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8478632478632463,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0056239388794588,
			"nilai_komparatif_unit": 1.0090652277141405,
			"summary_team": 0.8526315789473686
		}]
	},
	"730502": {
		"nik": "730502",
		"nama": "FERRY ZULJANNA",
		"band": "II",
		"posisi": "GM WITEL SUKABUMI",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8271929824561421,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9955710810305013,
			"nilai_komparatif_unit": 0.9989779685486126,
			"summary_team": 0.823529411764706
		}]
	},
	"730507": {
		"nik": "730507",
		"nama": "AMIR FAUZI",
		"band": "II",
		"posisi": "SM SCHOOL OF DIGITAL ENABLER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8471014492753652,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.045582304778195,
			"nilai_komparatif_unit": 1.0491603328780266,
			"summary_team": 0.8857142857142856
		}]
	},
	"730514": {
		"nik": "730514",
		"nama": "FERA PEBRAYENTI, ST, MM",
		"band": "II",
		"posisi": "DEPUTY EVP MARKETING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9846153846153846,
			"nilai_komparatif_unit": 0.9879847812641044,
			"summary_team": 0.8533333333333334
		}]
	},
	"730515": {
		"nik": "730515",
		"nama": "HENDRA MARTA MULYANA",
		"band": "II",
		"posisi": "AVP PRODUCT PROFITABILITY ANALYSIS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.817142857142856,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0402097902097918,
			"nilai_komparatif_unit": 1.04376943333122,
			"summary_team": 0.85
		}]
	},
	"730527": {
		"nik": "730527",
		"nama": "HENDRO SETYO BUDI",
		"band": "II",
		"posisi": "OSM CONSUMER MARKETING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.852777777777777,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9381107491856687,
			"nilai_komparatif_unit": 0.9413210049503286,
			"summary_team": 0.8
		}]
	},
	"730529": {
		"nik": "730529",
		"nama": "CATOER SOEPRASETIYO,MENG",
		"band": "II",
		"posisi": "SM BUSINESS PLANNING & PERFORMANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.865608465608466,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9550122249388748,
			"nilai_komparatif_unit": 0.9582803182882922,
			"summary_team": 0.8266666666666667
		}]
	},
	"730532": {
		"nik": "730532",
		"nama": "Setiyo Raharjo",
		"band": "II",
		"posisi": "SM INVESTASI",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0476190476190477,
			"nilai_komparatif_unit": 1.0512040455414207,
			"summary_team": 0.9428571428571431
		}]
	},
	"730551": {
		"nik": "730551",
		"nama": "MOCHADJIR",
		"band": "II",
		"posisi": "SM EMPLOYEE RELATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8478632478632463,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.943548387096776,
			"nilai_komparatif_unit": 0.9467772506947492,
			"summary_team": 0.8
		}]
	},
	"730555": {
		"nik": "730555",
		"nama": "INDAH ERITASARI",
		"band": "II",
		"posisi": "AVP INFORMATION SECURITY AUDIT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9111111111111114,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9329268292682923,
			"nilai_komparatif_unit": 0.9361193454336428,
			"summary_team": 0.85
		}]
	},
	"730583": {
		"nik": "730583",
		"nama": "SETYAJI NAWANDONO",
		"band": "II",
		"posisi": "SM GENERAL AFFAIR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8673267326732697,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9531202435311998,
			"nilai_komparatif_unit": 0.9563818624379932,
			"summary_team": 0.8266666666666667
		}]
	},
	"740038": {
		"nik": "740038",
		"nama": "INDRATMOKO SUSANTO",
		"band": "II",
		"posisi": "DEPUTY GM WITEL SURABAYA SELATAN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8495726495726486,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0005030181086532,
			"nilai_komparatif_unit": 1.003926782929844,
			"summary_team": 0.8500000000000001
		}]
	},
	"740055": {
		"nik": "740055",
		"nama": "AGUS SUNYOTO",
		"band": "II",
		"posisi": "SM ACCOUNT RECEIVABLE MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.839285714285713,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9531914893617037,
			"nilai_komparatif_unit": 0.9564533520748262,
			"summary_team": 0.8
		}]
	},
	"740062": {
		"nik": "740062",
		"nama": "Dr. I NYOMAN WISNU WARDHANA, S.T., LL.M.",
		"band": "II",
		"posisi": "VP LEGAL & INTERNAL AUDIT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8901960784313723,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.033480176211454,
			"nilai_komparatif_unit": 1.0370167903012344,
			"summary_team": 0.92
		}]
	},
	"740063": {
		"nik": "740063",
		"nama": "SUYARTI",
		"band": "II",
		"posisi": "DIREKTUR SUMBER DAYA MANUSIA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8491228070175436,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9892561983471079,
			"nilai_komparatif_unit": 0.9926414760621601,
			"summary_team": 0.8400000000000001
		}]
	},
	"740082": {
		"nik": "740082",
		"nama": "PURWANTA ARI WARDANA",
		"band": "II",
		"posisi": "AVP GCT CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8222222222222226,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9729729729729726,
			"nilai_komparatif_unit": 0.9763025287829407,
			"summary_team": 0.8
		}]
	},
	"740095": {
		"nik": "740095",
		"nama": "DWI NUGROHO IHSANUL WALAD",
		"band": "II",
		"posisi": "CONSUMER EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8800000000000001,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9740259740259737,
			"nilai_komparatif_unit": 0.9773591332513205,
			"summary_team": 0.857142857142857
		}]
	},
	"740100": {
		"nik": "740100",
		"nama": "ARIES YULIANTO, ST",
		"band": "II",
		"posisi": "OSM MANAGED SERVICE OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8644444444444435,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9562982005141397,
			"nilai_komparatif_unit": 0.9595706945278787,
			"summary_team": 0.8266666666666665
		}]
	},
	"740101": {
		"nik": "740101",
		"nama": "SULISTYAWAN",
		"band": "II",
		"posisi": "OSM CUSTOMER CARE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8511111111111103,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9869451697127946,
			"nilai_komparatif_unit": 0.9903225389873966,
			"summary_team": 0.8399999999999999
		}]
	},
	"740106": {
		"nik": "740106",
		"nama": "AAN PRIYATNA",
		"band": "II",
		"posisi": "PRINCIPAL CAPABILITY ACCOUNT MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.829885057471264,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8951325682627623,
			"nilai_komparatif_unit": 0.8981957508239842,
			"summary_team": 0.7428571428571427
		}]
	},
	"740108": {
		"nik": "740108",
		"nama": "I KETUT AGUNG ENRIKO",
		"band": "II",
		"posisi": "SM INNOVATION AND RESEARCH MGT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8411764705882361,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9650349650349642,
			"nilai_komparatif_unit": 0.9683373566366925,
			"summary_team": 0.811764705882353
		}]
	},
	"740110": {
		"nik": "740110",
		"nama": "BAGUS WAHYU HIDAYAT, ST",
		"band": "II",
		"posisi": "OSM CUSTOMER CARE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8294117647058815,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.000709219858157,
			"nilai_komparatif_unit": 1.0041336903106985,
			"summary_team": 0.83
		}]
	},
	"740114": {
		"nik": "740114",
		"nama": "MOHAMMAD SATRIA UTAMA",
		"band": "II",
		"posisi": "AVP RESOURCE & ADMINISTRATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8690476190476184,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0237128011336805,
			"nilai_komparatif_unit": 1.0272159908413436,
			"summary_team": 0.8896551724137931
		}]
	},
	"740117": {
		"nik": "740117",
		"nama": "WAHYU WINARTO, MM",
		"band": "II",
		"posisi": "AVP ORGANIZATION DESIGN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8279569892473108,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0306493506493521,
			"nilai_komparatif_unit": 1.0341762775309993,
			"summary_team": 0.8533333333333334
		}]
	},
	"740119": {
		"nik": "740119",
		"nama": "AGUNG PRASETYO BUDI",
		"band": "II",
		"posisi": "OSM SYNERGY GROUP",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8703703703703696,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.934468085106384,
			"nilai_komparatif_unit": 0.9376658755162129,
			"summary_team": 0.8133333333333335
		}]
	},
	"740120": {
		"nik": "740120",
		"nama": "SHAFWAN",
		"band": "II",
		"posisi": "OSM CUSTOMER CARE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8121212121212115,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0507462686567173,
			"nilai_komparatif_unit": 1.0543419680654258,
			"summary_team": 0.8533333333333333
		}]
	},
	"740121": {
		"nik": "740121",
		"nama": "TAUFIK SEFFTY NEGARA PURBA",
		"band": "II",
		"posisi": "AVP INFORMATION SYSTEM OPERATION AUDIT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9111111111111114,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0564024390243898,
			"nilai_komparatif_unit": 1.060017494093978,
			"summary_team": 0.9625
		}]
	},
	"740125": {
		"nik": "740125",
		"nama": "IWAN SAFARI",
		"band": "II",
		"posisi": "SM HUMAN CAPITAL SERVICE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8478632478632463,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0221774193548407,
			"nilai_komparatif_unit": 1.0256753549193116,
			"summary_team": 0.8666666666666667
		}]
	},
	"740130": {
		"nik": "740130",
		"nama": "SUBROTO MARZUKI",
		"band": "II",
		"posisi": "OSM REG ENTERPRISE,GOVERNMENT & BIZ SERV",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8309523809523801,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9306590257879666,
			"nilai_komparatif_unit": 0.9338437814312229,
			"summary_team": 0.7733333333333333
		}]
	},
	"740135": {
		"nik": "740135",
		"nama": "HADI MENDRA ISWANTO",
		"band": "II",
		"posisi": "SSO DELIVERY SERVICE COORDINATOR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8156862745098039,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9619082840236687,
			"nilai_komparatif_unit": 0.9651999759870552,
			"summary_team": 0.7846153846153846
		}]
	},
	"740140": {
		"nik": "740140",
		"nama": "YOU DEVY",
		"band": "II",
		"posisi": "VP BUSINESS PERFORMANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9199999999999999,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9683794466403165,
			"nilai_komparatif_unit": 0.9716932832034875,
			"summary_team": 0.8909090909090911
		}]
	},
	"740144": {
		"nik": "740144",
		"nama": "YUYUK WAHYUNINGSIH MOERNIATI P",
		"band": "II",
		"posisi": "AVP GOVERNMENT RELATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.868627450980391,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9209932279909719,
			"nilai_komparatif_unit": 0.9241449068539817,
			"summary_team": 0.8
		}]
	},
	"740145": {
		"nik": "740145",
		"nama": "YERRY FANDI EKA YUNIAR FIANTO, MM",
		"band": "II",
		"posisi": "SM HUMAN CAPITAL INFORMATION SYSTEM",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8478632478632463,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.943548387096776,
			"nilai_komparatif_unit": 0.9467772506947492,
			"summary_team": 0.8
		}]
	},
	"740153": {
		"nik": "740153",
		"nama": "TATA SAMBADA",
		"band": "II",
		"posisi": "WAREK II BIDANG SUMBER DAYA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0128205128205126,
			"nilai_komparatif_unit": 1.0162864286440656,
			"summary_team": 0.8777777777777778
		}]
	},
	"740160": {
		"nik": "740160",
		"nama": "NANANG SETIYO UTOMO, ST",
		"band": "II",
		"posisi": "GM WITEL PURWOKERTO",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8338797814207654,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9593709043250324,
			"nilai_komparatif_unit": 0.9626539132647869,
			"summary_team": 0.7999999999999999
		}]
	},
	"740162": {
		"nik": "740162",
		"nama": "HENDRI PURNARATMAN",
		"band": "II",
		"posisi": "AVP GOVERNANCE & QUALITY MGT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8333333333333326,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.005000000000001,
			"nilai_komparatif_unit": 1.0084391536887138,
			"summary_team": 0.8375
		}]
	},
	"740174": {
		"nik": "740174",
		"nama": "I MADE SURYA WIRAWAN",
		"band": "II",
		"posisi": "AVP AUDIT PARTNER 1",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8690476190476184,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.096535052377116,
			"nilai_komparatif_unit": 1.1002874429942162,
			"summary_team": 0.9529411764705882
		}]
	},
	"740179": {
		"nik": "740179",
		"nama": "BERNHARD SITOHANG",
		"band": "II",
		"posisi": "AUDITOR MADYA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8690476190476184,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9205479452054799,
			"nilai_komparatif_unit": 0.9236981002914406,
			"summary_team": 0.7999999999999998
		}]
	},
	"740196": {
		"nik": "740196",
		"nama": "TEUKU JOHN MAULANA HASAN",
		"band": "II",
		"posisi": "SM PROJECT MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.901754385964912,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9873227061629223,
			"nilai_komparatif_unit": 0.9907013673836688,
			"summary_team": 0.8903225806451612
		}]
	},
	"740199": {
		"nik": "740199",
		"nama": "DWI KIANSANTANG",
		"band": "II",
		"posisi": "GM WITEL PEKALONGAN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9749999999999999,
			"nilai_komparatif_unit": 0.9783364923845721,
			"summary_team": 0.8666666666666667
		}]
	},
	"740229": {
		"nik": "740229",
		"nama": "MOKHTAR ISMAIL",
		"band": "II",
		"posisi": "SM STRATEGIC PROCUREMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.02,
			"nilai_komparatif_unit": 1.0234904843407833,
			"summary_team": 0.8500000000000001
		}]
	},
	"740246": {
		"nik": "740246",
		"nama": "ARIF BUDIMAN, ST",
		"band": "II",
		"posisi": "AVP LEGAL ADMINISTRATION & COMPLIANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.875362318840579,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9842078451349983,
			"nilai_komparatif_unit": 0.9875758471659,
			"summary_team": 0.8615384615384616
		}]
	},
	"740254": {
		"nik": "740254",
		"nama": "JANAR WIDYATMOKO,M.ENG",
		"band": "II",
		"posisi": "SM DEBT & DUNNING MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.839285714285713,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0092615769712157,
			"nilai_komparatif_unit": 1.0127153139615805,
			"summary_team": 0.8470588235294119
		}]
	},
	"740263": {
		"nik": "740263",
		"nama": "HERU PRIYANTO",
		"band": "II",
		"posisi": "VP LOGISTIC AND FIXED ASSET MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8633333333333332,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0810810810810814,
			"nilai_komparatif_unit": 1.0847805875366014,
			"summary_team": 0.9333333333333333
		}]
	},
	"740266": {
		"nik": "740266",
		"nama": "FAHMI",
		"band": "III",
		"posisi": "SO PARTNERSHIP MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8600000000000002,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9457364341085268,
			"nilai_komparatif_unit": 0.9507480207288305,
			"summary_team": 0.8133333333333332
		}]
	},
	"740269": {
		"nik": "740269",
		"nama": "WIDODO YUWONO",
		"band": "II",
		"posisi": "TREG TRANSFORMATION IMPLEMENTATION COORD",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8944444444444447,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9363354037267079,
			"nilai_komparatif_unit": 0.9395395841820304,
			"summary_team": 0.8375
		}]
	},
	"740278": {
		"nik": "740278",
		"nama": "AGUSTINUS BUDI WIBOWO",
		"band": "II",
		"posisi": "AVP GROUP FINANCING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.857575757575757,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0046208208752385,
			"nilai_komparatif_unit": 1.008058676996503,
			"summary_team": 0.8615384615384616
		}]
	},
	"740288": {
		"nik": "740288",
		"nama": "HARIS SETYAWAN",
		"band": "II",
		"posisi": "GM WITEL MALUKU",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8439215686274537,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.011152416356874,
			"nilai_komparatif_unit": 1.0146126238818138,
			"summary_team": 0.8533333333333333
		}]
	},
	"740290": {
		"nik": "740290",
		"nama": "YUSUF HARYANTO",
		"band": "II",
		"posisi": "GM AREA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8475490196078479,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.986252613782972,
			"nilai_komparatif_unit": 0.9896276131010759,
			"summary_team": 0.8358974358974354
		}]
	},
	"740308": {
		"nik": "740308",
		"nama": "AFAN ISKANDAR BAKRY",
		"band": "II",
		"posisi": "SM HUMAN CAPITAL BUSINESS PARTNER 01",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8460317460317445,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.071669793621015,
			"nilai_komparatif_unit": 1.0753370942417255,
			"summary_team": 0.9066666666666667
		}]
	},
	"740319": {
		"nik": "740319",
		"nama": "DEDE FIRMANSYAH",
		"band": "II",
		"posisi": "EXECUTIVE STAKEHOLDER RELATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.868627450980391,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0196710738471475,
			"nilai_komparatif_unit": 1.0231604325883368,
			"summary_team": 0.8857142857142857
		}]
	},
	"750001": {
		"nik": "750001",
		"nama": "SUSKA BUDIWILOPO",
		"band": "II",
		"posisi": "PROJECT TEAM",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0045248868778278,
			"nilai_komparatif_unit": 1.007962414708783,
			"summary_team": 0.8705882352941178
		}]
	},
	"750026": {
		"nik": "750026",
		"nama": "RICKA FEBRILIANTINA, ST",
		"band": "II",
		"posisi": "OSM REG ENTERPRISE,GOVERNMENT & BIZ SERV",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8271929824561421,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9187698833510054,
			"nilai_komparatif_unit": 0.9219139538320054,
			"summary_team": 0.76
		}]
	},
	"750027": {
		"nik": "750027",
		"nama": "MUHAMMAD KAFIN LATIF",
		"band": "II",
		"posisi": "CONSUMER EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8309523809523801,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.072154206824695,
			"nilai_komparatif_unit": 1.0758231651284464,
			"summary_team": 0.8909090909090909
		}]
	},
	"750028": {
		"nik": "750028",
		"nama": "DIAH PURNAMA SARI",
		"band": "II",
		"posisi": "VP MARKETING & SOLUTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8285714285714286,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9956896551724139,
			"nilai_komparatif_unit": 0.9990969484563935,
			"summary_team": 0.8250000000000002
		}]
	},
	"750049": {
		"nik": "750049",
		"nama": "MUHAMMAD IHSAN",
		"band": "II",
		"posisi": "OSM ACCESS MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8473118279569885,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9677664974619299,
			"nilai_komparatif_unit": 0.9710782364863666,
			"summary_team": 0.8200000000000001
		}]
	},
	"750053": {
		"nik": "750053",
		"nama": "ANGKOSO SURYOCAHYONO",
		"band": "II",
		"posisi": "KABID GENERAL AFFAIR & COMPLIANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.826666666666666,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000009,
			"nilai_komparatif_unit": 1.003422043471357,
			"summary_team": 0.8266666666666667
		}]
	},
	"750064": {
		"nik": "750064",
		"nama": "MIA AMELIA",
		"band": "II",
		"posisi": "GM DEVELOPMENT MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.733333333333333,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9090909090909096,
			"nilai_komparatif_unit": 0.9122018577012333,
			"summary_team": 0.6666666666666667
		}]
	},
	"750066": {
		"nik": "750066",
		"nama": "MUHAMMAD PARUHUM PANE",
		"band": "II",
		"posisi": "DEPUTY GM WITEL TANGERANG",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8624999999999985,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9893719806763303,
			"nilai_komparatif_unit": 0.9927576546035464,
			"summary_team": 0.8533333333333334
		}]
	},
	"750080": {
		"nik": "750080",
		"nama": "HENDRA KURNIAWAN",
		"band": "II",
		"posisi": "DIREKTUR FINANCE & ADMINISTRASI",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9559748427672956,
			"nilai_komparatif_unit": 0.9592462302367681,
			"summary_team": 0.8444444444444446
		}]
	},
	"760005": {
		"nik": "760005",
		"nama": "FERONIKA",
		"band": "II",
		"posisi": "GM WITEL SULTRA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8439215686274537,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9479553903345694,
			"nilai_komparatif_unit": 0.9511993348892007,
			"summary_team": 0.8
		}]
	},
	"760032": {
		"nik": "760032",
		"nama": "DWI SAMEKTO BAWONO, ST.",
		"band": "II",
		"posisi": "VP PARTNERSHIP & INVESTMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8592592592592586,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9036511156186621,
			"nilai_komparatif_unit": 0.9067434490192485,
			"summary_team": 0.7764705882352942
		}]
	},
	"760034": {
		"nik": "760034",
		"nama": "NURINDA FIESTA PRAPTANTO",
		"band": "II",
		"posisi": "SENIOR STAFF II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.862745098039215,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7586776859504137,
			"nilai_komparatif_unit": 0.7612739139724839,
			"summary_team": 0.6545454545454544
		}]
	},
	"760039": {
		"nik": "760039",
		"nama": "ANDY PRASETYA",
		"band": "III",
		"posisi": "SO TARIFF MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8774774774774767,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9623545516769344,
			"nilai_komparatif_unit": 0.9674541999734692,
			"summary_team": 0.8444444444444444
		}]
	},
	"760040": {
		"nik": "760040",
		"nama": "TETI MULYATI",
		"band": "II",
		"posisi": "GM ASSESSMENT MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.733333333333333,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9318181818181823,
			"nilai_komparatif_unit": 0.9350069041437642,
			"summary_team": 0.6833333333333333
		}]
	},
	"770049": {
		"nik": "770049",
		"nama": "ALEXANDRE PARUHUM SOALOON, ST",
		"band": "II",
		"posisi": "CONSUMER EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8416666666666656,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8995756718529005,
			"nilai_komparatif_unit": 0.9026540589077555,
			"summary_team": 0.757142857142857
		}]
	},
	"770054": {
		"nik": "770054",
		"nama": "TATWANTO PRASTISTHO",
		"band": "II",
		"posisi": "AVP RISK STRATEGY & POLICY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8333333333333326,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9920000000000009,
			"nilai_komparatif_unit": 0.9953946671235862,
			"summary_team": 0.8266666666666667
		}]
	},
	"770055": {
		"nik": "770055",
		"nama": "DWI HARSO TRIPURHARNANI",
		"band": "II",
		"posisi": "SM FINANCE CENTER BUSINESS PARTNER 02",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8666666666666653,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000018,
			"nilai_komparatif_unit": 1.0034220434713579,
			"summary_team": 0.8666666666666667
		}]
	},
	"770061": {
		"nik": "770061",
		"nama": "AMBAR PERMANA SH.",
		"band": "II",
		"posisi": "AVP LEGAL COUNSELLOR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.875362318840579,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9748344370860934,
			"nilai_komparatif_unit": 0.978170362907177,
			"summary_team": 0.8533333333333333
		}]
	},
	"780045": {
		"nik": "780045",
		"nama": "MARIO BETTEGA",
		"band": "III",
		"posisi": "SO SALES & PARTNERSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.877777777777777,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0379746835443047,
			"nilai_komparatif_unit": 1.0434750532547812,
			"summary_team": 0.9111111111111111
		}]
	},
	"780054": {
		"nik": "780054",
		"nama": "AGUS WINDARTO",
		"band": "II",
		"posisi": "AVP LEGAL SETTLEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.875362318840579,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.944370860927153,
			"nilai_komparatif_unit": 0.9476025390663277,
			"summary_team": 0.8266666666666667
		}]
	},
	"780069": {
		"nik": "780069",
		"nama": "CHRISTIN ANWAR",
		"band": "II",
		"posisi": "SM GENERAL SUPPORT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8478632478632463,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0221774193548407,
			"nilai_komparatif_unit": 1.0256753549193116,
			"summary_team": 0.8666666666666667
		}]
	},
	"780071": {
		"nik": "780071",
		"nama": "NURCHOLIS FERI AHMADI",
		"band": "II",
		"posisi": "AVP ACCOUNTING POLICY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8807017543859643,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0219123505976104,
			"nilai_komparatif_unit": 1.0254093790852712,
			"summary_team": 0.9000000000000001
		}]
	},
	"790021": {
		"nik": "790021",
		"nama": "HERU SETIAWAN",
		"band": "III",
		"posisi": "SO BUSINESS PERFORMANCE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8565656565656555,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0636792452830202,
			"nilai_komparatif_unit": 1.0693158269792513,
			"summary_team": 0.9111111111111111
		}]
	},
	"790042": {
		"nik": "790042",
		"nama": "T. FAUZAN",
		"band": "II",
		"posisi": "DEPUTY GM WITEL MEDAN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8406779661016985,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9516129032258023,
			"nilai_komparatif_unit": 0.9548693639485444,
			"summary_team": 0.7999999999999999
		}]
	},
	"790056": {
		"nik": "790056",
		"nama": "DHANI ELEVANI, M.T",
		"band": "III",
		"posisi": "SO BUSINESS DEVELOPMENT & EVALUATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8925925925925925,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.995850622406639,
			"nilai_komparatif_unit": 1.001127770959955,
			"summary_team": 0.8888888888888888
		}]
	},
	"790070": {
		"nik": "790070",
		"nama": "ARMY ARISTOFANY",
		"band": "II",
		"posisi": "OSM MANAGED SERVICE OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8740740740740729,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0525423728813572,
			"nilai_komparatif_unit": 1.0561442186368015,
			"summary_team": 0.9199999999999999
		}]
	},
	"790094": {
		"nik": "790094",
		"nama": "DESSY SANDRA",
		"band": "II",
		"posisi": "AUDITOR MADYA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8690476190476184,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0520547945205485,
			"nilai_komparatif_unit": 1.0556549717616464,
			"summary_team": 0.9142857142857141
		}]
	},
	"790114": {
		"nik": "790114",
		"nama": "MUHAMMAD ZAKKIE RAMDHANI",
		"band": "II",
		"posisi": "SSO STRATEGY & ENGAGEMENT COORDINATOR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8156862745098039,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9317307692307693,
			"nilai_komparatif_unit": 0.934919192426677,
			"summary_team": 0.76
		}]
	},
	"800014": {
		"nik": "800014",
		"nama": "RABBY SALAM SIREGAR, S.T.",
		"band": "II",
		"posisi": "SM BUSINESS PLANNING & PERFORMANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8349726775956329,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006020942408372,
			"nilai_komparatif_unit": 1.009463589806388,
			"summary_team": 0.8400000000000003
		}]
	},
	"800020": {
		"nik": "800020",
		"nama": "SYARMAD HANING",
		"band": "II",
		"posisi": "PROJECT TEAM",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8644444444444433,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0071072130651761,
			"nilai_komparatif_unit": 1.0105535777286014,
			"summary_team": 0.8705882352941178
		}]
	},
	"800060": {
		"nik": "800060",
		"nama": "ARNANDO HARATUA",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.852631578947368,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.022045855379189,
			"nilai_komparatif_unit": 1.0255433407260157,
			"summary_team": 0.8714285714285712
		}]
	},
	"800070": {
		"nik": "800070",
		"nama": "SETIO NURANTO",
		"band": "II",
		"posisi": "AVP SEKRETARIAT DIT KEU&MANAJEMEN RISIKO",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8715151515151515,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9179415855354661,
			"nilai_komparatif_unit": 0.921082821545334,
			"summary_team": 0.8000000000000002
		}]
	},
	"800080": {
		"nik": "800080",
		"nama": "PUSPO HENDRIADI",
		"band": "II",
		"posisi": "AVP SEKRETARIAT DIREKTORAT HCM",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8542222222222235,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0265748819338814,
			"nilai_komparatif_unit": 1.0300878658064614,
			"summary_team": 0.8769230769230769
		}]
	},
	"800089": {
		"nik": "800089",
		"nama": "EINSTEIN",
		"band": "II",
		"posisi": "KOORD REMED IFAS REL IMPLEM IFRS 15,9,16",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.839285714285713,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0425531914893633,
			"nilai_komparatif_unit": 1.046120853831841,
			"summary_team": 0.875
		}]
	},
	"800091": {
		"nik": "800091",
		"nama": "SUTAN ALHAMSYAHBANA HAQIQIE",
		"band": "II",
		"posisi": "AVP INDUSTRIAL RELATION & COMPLIANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8545454545454537,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9507978723404265,
			"nilai_komparatif_unit": 0.9540515439920483,
			"summary_team": 0.8125
		}]
	},
	"800098": {
		"nik": "800098",
		"nama": "SURYA MARDI DOMINIC",
		"band": "II",
		"posisi": "SM ACCOUNTING CONTROL GROUP",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8740740740740739,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0067796610169493,
			"nilai_komparatif_unit": 1.0102249047830265,
			"summary_team": 0.88
		}]
	},
	"810032": {
		"nik": "810032",
		"nama": "JONSON PARMA SIMANJUNTAK",
		"band": "II",
		"posisi": "SM ACCOUNTING CONSOLIDATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8666666666666653,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9846153846153863,
			"nilai_komparatif_unit": 0.9879847812641062,
			"summary_team": 0.8533333333333334
		}]
	},
	"810112": {
		"nik": "810112",
		"nama": "MARIA SAWITRI WULANDARI",
		"band": "II",
		"posisi": "AVP FINANCIAL & ASSET MANAGEMENT AUDIT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.948148148148148,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9624023437500001,
			"nilai_komparatif_unit": 0.9656957264072477,
			"summary_team": 0.9125
		}]
	},
	"820043": {
		"nik": "820043",
		"nama": "ZUL RAMADHAN",
		"band": "II",
		"posisi": "AUDITOR MADYA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9164334266293431,
			"nilai_komparatif_unit": 0.9195695016538725,
			"summary_team": 0.8
		}]
	},
	"820049": {
		"nik": "820049",
		"nama": "DECKIE FADZKURI SUKAMTO",
		"band": "II",
		"posisi": "OSM CONSUMER MARKETING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8121212121212115,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9850746268656726,
			"nilai_komparatif_unit": 0.9884455950613368,
			"summary_team": 0.8000000000000002
		}]
	},
	"820066": {
		"nik": "820066",
		"nama": "IRWAN SYAH, M.M",
		"band": "II",
		"posisi": "SM GENERAL ACCOUNTING OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8666666666666653,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000016,
			"nilai_komparatif_unit": 1.0034220434713577,
			"summary_team": 0.8666666666666666
		}]
	},
	"820071": {
		"nik": "820071",
		"nama": "DEDEN MIFTAH PUSPANINGRAT",
		"band": "II",
		"posisi": "AVP PEOPLE ANALYTICS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9037037037037031,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0032786885245908,
			"nilai_komparatif_unit": 1.0067119518106071,
			"summary_team": 0.9066666666666667
		}]
	},
	"820075": {
		"nik": "820075",
		"nama": "JULIUS ROMEDLY",
		"band": "II",
		"posisi": "AVP DIGITAL TALENT & LEADERSHIP",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8611111111111115,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9870967741935479,
			"nilai_komparatif_unit": 0.9904746622652736,
			"summary_team": 0.85
		}]
	},
	"820078": {
		"nik": "820078",
		"nama": "DUDY RODIANSYAH",
		"band": "II",
		"posisi": "DEPUTY GM WITEL BANDUNG",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8567049808429145,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0738819320214636,
			"nilai_komparatif_unit": 1.077556802675945,
			"summary_team": 0.9199999999999999
		}]
	},
	"830013": {
		"nik": "830013",
		"nama": "RISNA MUTIAR",
		"band": "II",
		"posisi": "AVP CEO SECRETARIATE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.852631578947368,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.020370370370371,
			"nilai_komparatif_unit": 1.0238621221346622,
			"summary_team": 0.8700000000000001
		}]
	},
	"830030": {
		"nik": "830030",
		"nama": "ANANG SUPRIADI",
		"band": "II",
		"posisi": "PORTFOLIO RESTRUCTURING COORD",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8352941176470572,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000018,
			"nilai_komparatif_unit": 1.0034220434713579,
			"summary_team": 0.8352941176470587
		}]
	},
	"830051": {
		"nik": "830051",
		"nama": "JUNITIA PRIANDRIWIJAYANTI",
		"band": "II",
		"posisi": "AVP TREASURY & BUDGET POLICY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8807017543859643,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9083665338645425,
			"nilai_komparatif_unit": 0.911475003631352,
			"summary_team": 0.8
		}]
	},
	"830071": {
		"nik": "830071",
		"nama": "RADEN AYU SHERLY FRISKA DEWI",
		"band": "II",
		"posisi": "AVP SUBSIDIARY PERFORMANCE& REPORTING 1",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.862745098039215,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9659090909090918,
			"nilai_komparatif_unit": 0.9692144738075608,
			"summary_team": 0.8333333333333334
		}]
	},
	"830092": {
		"nik": "830092",
		"nama": "MHD. ADNA MIRAZA",
		"band": "II",
		"posisi": "SM HUMAN CAPITAL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8349726775956329,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0645724258289648,
			"nilai_komparatif_unit": 1.0682154389485585,
			"summary_team": 0.888888888888889
		}]
	},
	"830124": {
		"nik": "830124",
		"nama": "ANGGIA PERMATASARI",
		"band": "II",
		"posisi": "AVP TREASURY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.857575757575757,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0046208208752385,
			"nilai_komparatif_unit": 1.008058676996503,
			"summary_team": 0.8615384615384616
		}]
	},
	"830150": {
		"nik": "830150",
		"nama": "ANGGI VIDYA RISTYANTI",
		"band": "III",
		"posisi": "SO PRICING & BUNDLING PACKAGE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8916666666666663,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9844236760124615,
			"nilai_komparatif_unit": 0.9896402716150879,
			"summary_team": 0.8777777777777778
		}]
	},
	"830151": {
		"nik": "830151",
		"nama": "HERY SAEPUL AZIS",
		"band": "II",
		"posisi": "AVP CONSOLIDATED MGMT REPORTING&ANALYSIS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.817142857142856,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9926184926184941,
			"nilai_komparatif_unit": 0.9960152762507065,
			"summary_team": 0.8111111111111111
		}]
	},
	"830153": {
		"nik": "830153",
		"nama": "FAUZAN FEISAL",
		"band": "II",
		"posisi": "SM PARTNERSHIP OPERATION MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8411764705882361,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9351981351981344,
			"nilai_komparatif_unit": 0.9383984238711136,
			"summary_team": 0.7866666666666667
		}]
	},
	"840018": {
		"nik": "840018",
		"nama": "RAMA SUGIHARTO",
		"band": "II",
		"posisi": "AVP HC COMMUNICATION & EMPLOYER BRANDING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8545454545454537,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9946808510638308,
			"nilai_komparatif_unit": 0.9980846921762967,
			"summary_team": 0.85
		}]
	},
	"840091": {
		"nik": "840091",
		"nama": "SYAFUAN",
		"band": "II",
		"posisi": "EXECUTIVE STAKEHOLDER RELATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.868627450980391,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0196710738471473,
			"nilai_komparatif_unit": 1.0231604325883366,
			"summary_team": 0.8857142857142856
		}]
	},
	"840098": {
		"nik": "840098",
		"nama": "ADRIAN INDRA RAHMAWAN",
		"band": "II",
		"posisi": "AVP HC DATA GOVERNANCE & TECHNOLOGY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9037037037037031,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0327868852459021,
			"nilai_komparatif_unit": 1.0363211268638601,
			"summary_team": 0.9333333333333332
		}]
	},
	"840136": {
		"nik": "840136",
		"nama": "AVERYADI SETIAWAN",
		"band": "II",
		"posisi": "AVP PROGRAM & PARTNERSHIP",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8699999999999994,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.011494252873564,
			"nilai_komparatif_unit": 1.0149556301779241,
			"summary_team": 0.88
		}]
	},
	"840144": {
		"nik": "840144",
		"nama": "SYAIFUL ROHMAN",
		"band": "II",
		"posisi": "GM WITEL NTB",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8673267326732697,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.014611872146116,
			"nilai_komparatif_unit": 1.0180839180791539,
			"summary_team": 0.88
		}]
	},
	"840163": {
		"nik": "840163",
		"nama": "IDA ARSIYANTI",
		"band": "II",
		"posisi": "SM FINANCIAL REPORTING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8666666666666653,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0307692307692324,
			"nilai_komparatif_unit": 1.034296567885861,
			"summary_team": 0.8933333333333333
		}]
	},
	"850064": {
		"nik": "850064",
		"nama": "RAHMI MAULIDA KURNIATI",
		"band": "III",
		"posisi": "SO QUALITY ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8774774774774767,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0093869169844536,
			"nilai_komparatif_unit": 1.0147357962127739,
			"summary_team": 0.8857142857142856
		}]
	},
	"850090": {
		"nik": "850090",
		"nama": "WILLY ARIEF YUDHISTIRA",
		"band": "II",
		"posisi": "SM FINANCE CENTER BUSINESS PARTNER 03",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8666666666666653,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9846153846153861,
			"nilai_komparatif_unit": 0.9879847812641059,
			"summary_team": 0.8533333333333333
		}]
	},
	"850120": {
		"nik": "850120",
		"nama": "ANGGITA PRATAMI WAYAN SARI",
		"band": "III",
		"posisi": "SO PRODUCT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8774774774774767,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9496919917864485,
			"nilai_komparatif_unit": 0.9547245394475025,
			"summary_team": 0.8333333333333334
		}]
	},
	"850154": {
		"nik": "850154",
		"nama": "HATTA PERDANA",
		"band": "II",
		"posisi": "AVP DIGITAL TELCO REGULATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.868627450980391,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9209932279909719,
			"nilai_komparatif_unit": 0.9241449068539817,
			"summary_team": 0.8
		}]
	},
	"850158": {
		"nik": "850158",
		"nama": "MUHAMMAD FARUQ ULINUHA",
		"band": "II",
		"posisi": "AVP SERVICE DELIVERY AUDIT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8962962962962957,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9450656295576086,
			"nilai_komparatif_unit": 0.9482996852252393,
			"summary_team": 0.8470588235294116
		}]
	},
	"855912": {
		"nik": "855912",
		"nama": "NOVIANA IRAWAN",
		"band": "II",
		"posisi": "AUDITOR MADYA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8682242990654238,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9214208826695338,
			"nilai_komparatif_unit": 0.9245740249854443,
			"summary_team": 0.8
		}]
	},
	"860143": {
		"nik": "860143",
		"nama": "PUTRI CHAIRUNNISA",
		"band": "III",
		"posisi": "SO CX PLANNING & PERFORMANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8407407407407405,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0572687224669606,
			"nilai_komparatif_unit": 1.0628713339266487,
			"summary_team": 0.8888888888888888
		}]
	},
	"880012": {
		"nik": "880012",
		"nama": "FEBTRIANY",
		"band": "III",
		"posisi": "SO CHANNEL PARTNERSHIP MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.877777777777777,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9240506329113932,
			"nilai_komparatif_unit": 0.9289473035073051,
			"summary_team": 0.8111111111111111
		}]
	},
	"890008": {
		"nik": "890008",
		"nama": "JEKY ALI BUYUNG",
		"band": "III",
		"posisi": "SO CX BUSINESS PROCESS & IT TOOLS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8407407407407405,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0044052863436126,
			"nilai_komparatif_unit": 1.0097277672303162,
			"summary_team": 0.8444444444444444
		}]
	}
};