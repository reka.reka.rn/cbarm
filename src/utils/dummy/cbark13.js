export const cbark13 = {
	"650030": {
		"nik": "650030",
		"nama": "RATNAWATI",
		"band": "IV",
		"posisi": "OFF 1 APARTEMEN & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8466666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0123734533183348,
			"nilai_komparatif_unit": 1.0194680581927573,
			"summary_team": 0.857142857142857
		}]
	},
	"650080": {
		"nik": "650080",
		"nama": "IWAN JAUHARI",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8304347826086982,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0665669409124874,
			"nilai_komparatif_unit": 1.0740413279512755,
			"summary_team": 0.8857142857142858
		}]
	},
	"650115": {
		"nik": "650115",
		"nama": "SUROSO",
		"band": "IV",
		"posisi": "ASMAN CONSTRUCTION SUPERVISION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8392857142857179,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.953191489361698,
			"nilai_komparatif_unit": 0.9598713533629888,
			"summary_team": 0.7999999999999999
		}]
	},
	"650129": {
		"nik": "650129",
		"nama": "ASEP ROSIDI",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649349,
			"nilai_komparatif_unit": 0.9416177701125834,
			"summary_team": 0.8000000000000002
		}]
	},
	"650222": {
		"nik": "650222",
		"nama": "SULGIM",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.877777777777778,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.009041591320072,
			"nilai_komparatif_unit": 1.0161128468619967,
			"summary_team": 0.8857142857142857
		}]
	},
	"650474": {
		"nik": "650474",
		"nama": "EUIS ROHANA",
		"band": "IV",
		"posisi": "OFF 1 OFFICE ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8766666666666662,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0755024443237375,
			"nilai_komparatif_unit": 1.083039450414664,
			"summary_team": 0.9428571428571427
		}]
	},
	"650775": {
		"nik": "650775",
		"nama": "AGUNG ADI WIBOWO",
		"band": "IV",
		"posisi": "ASMAN WIFI SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333332,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365353,
			"nilai_komparatif_unit": 0.9949168891755601,
			"summary_team": 0.8727272727272727
		}]
	},
	"650931": {
		"nik": "650931",
		"nama": "MUHAMMAD PAISAL",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.024,
			"nilai_komparatif_unit": 1.0311760824699583,
			"summary_team": 0.8533333333333334
		}]
	},
	"651362": {
		"nik": "651362",
		"nama": "DANAN RIANTA",
		"band": "IV",
		"posisi": "ASMAN DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444449,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9891640866873059,
			"nilai_komparatif_unit": 0.9960960428029202,
			"summary_team": 0.8352941176470587
		}]
	},
	"651385": {
		"nik": "651385",
		"nama": "DANI HERMAWAN",
		"band": "IV",
		"posisi": "ASMAN PL DSG, DEPL, SOL PARTNER&INS REL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8222222222222225,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9586645468998405,
			"nilai_komparatif_unit": 0.9653827655029443,
			"summary_team": 0.7882352941176469
		}]
	},
	"651519": {
		"nik": "651519",
		"nama": "ADE HIDAYAT",
		"band": "IV",
		"posisi": "ASMAN NW OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.069930069930071,
			"nilai_komparatif_unit": 1.077428025417284,
			"summary_team": 0.9272727272727272
		}]
	},
	"651540": {
		"nik": "651540",
		"nama": "PANGIHUTAN NAIBAHO",
		"band": "IV",
		"posisi": "OFF 1 LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9133333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9854014598540142,
			"nilai_komparatif_unit": 0.9923070478832423,
			"summary_team": 0.8999999999999998
		}]
	},
	"651563": {
		"nik": "651563",
		"nama": "EDUAR EFFENDI",
		"band": "IV",
		"posisi": "ASMAN TGroup FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0199999999999996,
			"nilai_komparatif_unit": 1.0271480508978095,
			"summary_team": 0.85
		}]
	},
	"660002": {
		"nik": "660002",
		"nama": "YAMAN",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.880701754385964,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9489186112692101,
			"nilai_komparatif_unit": 0.9555685313978683,
			"summary_team": 0.8357142857142859
		}]
	},
	"660003": {
		"nik": "660003",
		"nama": "MARNANGKOK SITUMORANG",
		"band": "IV",
		"posisi": "ASMAN CONSTRUCTION SUPERVISION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.028571428571428,
			"nilai_komparatif_unit": 1.0357795471238413,
			"summary_team": 0.857142857142857
		}]
	},
	"660005": {
		"nik": "660005",
		"nama": "SRIYATI",
		"band": "IV",
		"posisi": "OFFICER 1 ADMINISTRASI KEPESERTAAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8800000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0389610389610386,
			"nilai_komparatif_unit": 1.0462419667917593,
			"summary_team": 0.9142857142857141
		}]
	},
	"660046": {
		"nik": "660046",
		"nama": "MEDIA SALFIANA",
		"band": "IV",
		"posisi": "OFF 1 CASH BANK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8631578947368416,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9764808362369343,
			"nilai_komparatif_unit": 0.9833239094900299,
			"summary_team": 0.8428571428571427
		}]
	},
	"660055": {
		"nik": "660055",
		"nama": "ASEP DJUHARA HENDRAWAN",
		"band": "IV",
		"posisi": "OFF 1 PKBL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0291595197255576,
			"nilai_komparatif_unit": 1.0363717595578752,
			"summary_team": 0.9090909090909092
		}]
	},
	"660059": {
		"nik": "660059",
		"nama": "DEDI ACHMAD HIDAYAT",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION CONSUMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.022222222222222,
			"nilai_komparatif_unit": 1.0293858462156698,
			"summary_team": 0.92
		}]
	},
	"660062": {
		"nik": "660062",
		"nama": "JULAEHA",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8000000000000004,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272723,
			"nilai_komparatif_unit": 1.0298944360606377,
			"summary_team": 0.8181818181818181
		}]
	},
	"660072": {
		"nik": "660072",
		"nama": "FADILA TJIPTO",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION CONSUMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.858333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9868646487721305,
			"nilai_komparatif_unit": 0.9937804906727898,
			"summary_team": 0.8470588235294116
		}]
	},
	"660084": {
		"nik": "660084",
		"nama": "TRI AGOESTIEN SOEMARIYATI",
		"band": "IV",
		"posisi": "OFF 1 PROCUREMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8740740740740743,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9152542372881354,
			"nilai_komparatif_unit": 0.9216682410847744,
			"summary_team": 0.8
		}]
	},
	"660144": {
		"nik": "660144",
		"nama": "HESTI MULIASARI",
		"band": "IV",
		"posisi": "OFF 1 CASH & BANK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8363636363636368,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9565217391304341,
			"nilai_komparatif_unit": 0.963224941165891,
			"summary_team": 0.7999999999999998
		}]
	},
	"660170": {
		"nik": "660170",
		"nama": "AHMAD YURNALIS",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96,
			"nilai_komparatif_unit": 0.9667275773155858,
			"summary_team": 0.8
		}]
	},
	"660172": {
		"nik": "660172",
		"nama": "ABDUL MUTHALIB",
		"band": "IV",
		"posisi": "ASMAN OLO FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8245614035087716,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0914893617021282,
			"nilai_komparatif_unit": 1.0991384024000348,
			"summary_team": 0.9000000000000001
		}]
	},
	"660210": {
		"nik": "660210",
		"nama": "ASEP TATANG SUDARMAN",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK & LEGAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0199999999999998,
			"nilai_komparatif_unit": 1.0271480508978097,
			"summary_team": 0.85
		}]
	},
	"660278": {
		"nik": "660278",
		"nama": "RIPKA RIANTI",
		"band": "IV",
		"posisi": "OFF 1 LEARNING DEVT MEDIA CLUSTER MAT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.911111111111111,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0152439024390245,
			"nilai_komparatif_unit": 1.0223586231138533,
			"summary_team": 0.925
		}]
	},
	"660387": {
		"nik": "660387",
		"nama": "BUDI HARYONO",
		"band": "IV",
		"posisi": "ASMAN CCAN WIFI FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9374999999999997,
			"nilai_komparatif_unit": 0.9440698997222514,
			"summary_team": 0.7999999999999999
		}]
	},
	"660445": {
		"nik": "660445",
		"nama": "MARFRIAN ZARDI",
		"band": "IV",
		"posisi": "ASMAN CDC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9850230414746541,
			"nilai_komparatif_unit": 0.9919259775883564,
			"summary_team": 0.8142857142857142
		}]
	},
	"660456": {
		"nik": "660456",
		"nama": "AMRI EFFENDY",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8424242424242427,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9496402877697838,
			"nilai_komparatif_unit": 0.9562952653301654,
			"summary_team": 0.8
		}]
	},
	"660457": {
		"nik": "660457",
		"nama": "YUSRIZAL",
		"band": "IV",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8637037037037024,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9841337907375658,
			"nilai_komparatif_unit": 0.9910304950772194,
			"summary_team": 0.85
		}]
	},
	"660458": {
		"nik": "660458",
		"nama": "SRI ARNITA",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8405797101449296,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0112068965517216,
			"nilai_komparatif_unit": 1.018293326321102,
			"summary_team": 0.85
		}]
	},
	"660459": {
		"nik": "660459",
		"nama": "ZULKIFLI",
		"band": "IV",
		"posisi": "ASMAN CCAN FULFILLMENT",
		"category": [{
			"code": "MD-2-01",
			"kriteria": 0.8000000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9821428571428567,
			"nilai_komparatif_unit": 0.9890256092328347,
			"summary_team": 0.7857142857142855
		}]
	},
	"660472": {
		"nik": "660472",
		"nama": "YETTI HERLINA",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK & LEGAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8400000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.020408163265306,
			"nilai_komparatif_unit": 1.0275590745276206,
			"summary_team": 0.857142857142857
		}]
	},
	"660487": {
		"nik": "660487",
		"nama": "Elisabeth Miriya Zebua",
		"band": "IV",
		"posisi": "KEPALA KLINIK PRATAMA TPKK SLIPI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777781,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9417721518987339,
			"nilai_komparatif_unit": 0.94837199040453,
			"summary_team": 0.8266666666666667
		}]
	},
	"660513": {
		"nik": "660513",
		"nama": "BAMBANG AGUS SUSILO",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006993006993008,
			"nilai_komparatif_unit": 1.014049906275091,
			"summary_team": 0.8727272727272729
		}]
	},
	"660529": {
		"nik": "660529",
		"nama": "JAJA",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8574712643678148,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9398359880145102,
			"nilai_komparatif_unit": 0.9464222580909035,
			"summary_team": 0.8058823529411765
		}]
	},
	"660556": {
		"nik": "660556",
		"nama": "ENTIS",
		"band": "IV",
		"posisi": "OFF 1 SECURITY & SAFETY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006993006993007,
			"nilai_komparatif_unit": 1.01404990627509,
			"summary_team": 0.8727272727272727
		}]
	},
	"660582": {
		"nik": "660582",
		"nama": "Maria Goreti  Sumiyarsih",
		"band": "IV",
		"posisi": "OFF 1 MITRA KEPESERTAAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0323529411764703,
			"nilai_komparatif_unit": 1.039587560164738,
			"summary_team": 0.9176470588235293
		}]
	},
	"660598": {
		"nik": "660598",
		"nama": "ADIN TARDIN",
		"band": "IV",
		"posisi": "ASMAN DAVAL & INVENTORY MGT PLAN & REV",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.043181818181818,
			"nilai_komparatif_unit": 1.0504923247818507,
			"summary_team": 0.9272727272727272
		}]
	},
	"660599": {
		"nik": "660599",
		"nama": "LEO DANDUN BINTORO",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8722222222222226,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.000579038795599,
			"nilai_komparatif_unit": 1.0075909896746114,
			"summary_team": 0.8727272727272729
		}]
	},
	"660600": {
		"nik": "660600",
		"nama": "ATEP KUSNADI",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.043181818181818,
			"nilai_komparatif_unit": 1.0504923247818507,
			"summary_team": 0.9272727272727272
		}]
	},
	"660605": {
		"nik": "660605",
		"nama": "LILI SOMANTRI",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8159999999999993,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9891456582633065,
			"nilai_komparatif_unit": 0.9960774852344965,
			"summary_team": 0.8071428571428574
		}]
	},
	"660624": {
		"nik": "660624",
		"nama": "AGUS SUTARMAN",
		"band": "IV",
		"posisi": "ASMAN NW OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666671,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346932,
			"nilai_komparatif_unit": 0.9864567115465154,
			"summary_team": 0.7999999999999998
		}]
	},
	"670032": {
		"nik": "670032",
		"nama": "NELSON BINSAR M. S.",
		"band": "IV",
		"posisi": "OFF 1 SERVICE NODE OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8611111111111115,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.024667931688804,
			"nilai_komparatif_unit": 1.0318486949525936,
			"summary_team": 0.8823529411764706
		}]
	},
	"670051": {
		"nik": "670051",
		"nama": "DIANA PERMATA SARI",
		"band": "IV",
		"posisi": "OFF 1 EVENT FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0050924685070979,
			"nilai_komparatif_unit": 1.0121360490187588,
			"summary_team": 0.8571428571428573
		}]
	},
	"670061": {
		"nik": "670061",
		"nama": "INDAH WINARSIH",
		"band": "IV",
		"posisi": "OFF 1 RESEARCH SUPPORT READINESS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8909090909090913,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9639855942376945,
			"nilai_komparatif_unit": 0.9707411021713872,
			"summary_team": 0.8588235294117645
		}]
	},
	"670078": {
		"nik": "670078",
		"nama": "YENY MEIZARNI",
		"band": "IV",
		"posisi": "OFF 1 FINANCE SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8363636363636368,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0248447204968938,
			"nilai_komparatif_unit": 1.0320267226777404,
			"summary_team": 0.8571428571428571
		}]
	},
	"670079": {
		"nik": "670079",
		"nama": "WURSANTI",
		"band": "IV",
		"posisi": "OFF 1 FINANCE SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8296296296296299,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8954081632653056,
			"nilai_komparatif_unit": 0.9016830878979868,
			"summary_team": 0.7428571428571427
		}]
	},
	"670150": {
		"nik": "670150",
		"nama": "NUNING HARDIANTI",
		"band": "IV",
		"posisi": "OFF 1 LEARNING DEVT HC & GENERAL AFF MAT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7866666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9806295399515736,
			"nilai_komparatif_unit": 0.9875016868765439,
			"summary_team": 0.7714285714285714
		}]
	},
	"670311": {
		"nik": "670311",
		"nama": "Mochamad Effendi Soemirat",
		"band": "IV",
		"posisi": "OFF 1 PENGADAAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333327,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9457452884066254,
			"nilai_komparatif_unit": 0.9523729702280905,
			"summary_team": 0.8117647058823529
		}]
	},
	"670329": {
		"nik": "670329",
		"nama": "Adang Aripin",
		"band": "IV",
		"posisi": "OFF 1 INTEGRATION SYSTEM MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9705882352941175,
			"nilai_komparatif_unit": 0.9773900138300958,
			"summary_team": 0.9058823529411764
		}]
	},
	"670349": {
		"nik": "670349",
		"nama": "Een Rohaeni",
		"band": "IV",
		"posisi": "MANAGER LOGISTIK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.0070078930370683,
			"summary_team": 0.9333333333333333
		}]
	},
	"670445": {
		"nik": "670445",
		"nama": "SLAMET",
		"band": "IV",
		"posisi": "ASMAN COMMUNITY DEVELOPMENT CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.858333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0591350397175645,
			"nilai_komparatif_unit": 1.0665573447877166,
			"summary_team": 0.9090909090909092
		}]
	},
	"670457": {
		"nik": "670457",
		"nama": "ROHMAN",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9898989898989896,
			"nilai_komparatif_unit": 0.9968360961377039,
			"summary_team": 0.8909090909090909
		}]
	},
	"670495": {
		"nik": "670495",
		"nama": "TRI WIYONO",
		"band": "IV",
		"posisi": "ASMAN NW OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8222222222222224,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0378378378378375,
			"nilai_komparatif_unit": 1.0451108943952274,
			"summary_team": 0.8533333333333333
		}]
	},
	"670502": {
		"nik": "670502",
		"nama": "EDI TRESNA",
		"band": "IV",
		"posisi": "OFF 1 RECONCILIATION & SETTLEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.0339332377706798,
			"summary_team": 0.8727272727272727
		}]
	},
	"670503": {
		"nik": "670503",
		"nama": "Nina Oktorina Damayanti",
		"band": "IV",
		"posisi": "OFF 1 PENGENDALIAN BIAYA",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9988901220865701,
			"nilai_komparatif_unit": 1.0058902372179372,
			"summary_team": 0.8823529411764706
		}]
	},
	"670513": {
		"nik": "670513",
		"nama": "YAYAT SUDRADJAT",
		"band": "IV",
		"posisi": "ASMAN DEVELOPMENT & SOLUTION PARTNER SPV",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.891666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0399320305862356,
			"nilai_komparatif_unit": 1.0472197630224054,
			"summary_team": 0.9272727272727272
		}]
	},
	"670514": {
		"nik": "670514",
		"nama": "RACHMAT",
		"band": "IV",
		"posisi": "ASMAN BGES FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9877232142857139,
			"nilai_komparatif_unit": 0.9946450729216576,
			"summary_team": 0.8428571428571427
		}]
	},
	"670515": {
		"nik": "670515",
		"nama": "HERI CUNIAWAN",
		"band": "IV",
		"posisi": "ASMAN OM TRANSPORT, RADIO & MUX",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559438,
			"nilai_komparatif_unit": 0.9506717871328967,
			"summary_team": 0.8181818181818181
		}]
	},
	"670550": {
		"nik": "670550",
		"nama": "DJUNAEDI",
		"band": "IV",
		"posisi": "OFF 1 HC CAPACITY SOLUTION ADVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8363636363636368,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0268542199488486,
			"nilai_komparatif_unit": 1.0340503044869125,
			"summary_team": 0.8588235294117648
		}]
	},
	"680014": {
		"nik": "680014",
		"nama": "RISMAWATI SINAGA",
		"band": "IV",
		"posisi": "OFF 1 EVENT FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0050924685070979,
			"nilai_komparatif_unit": 1.0121360490187588,
			"summary_team": 0.8571428571428573
		}]
	},
	"680090": {
		"nik": "680090",
		"nama": "RONNY TARMIZI",
		"band": "IV",
		"posisi": "OFF 1 SPIRITUAL PROG IMPLEM & MONITORING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8419753086419745,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9082283939968958,
			"nilai_komparatif_unit": 0.9145931614352546,
			"summary_team": 0.7647058823529411
		}]
	},
	"680091": {
		"nik": "680091",
		"nama": "DINDIN RAMDHANI",
		"band": "IV",
		"posisi": "MGR SHARED SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8749999999999997,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0487394957983198,
			"nilai_komparatif_unit": 1.0560889500086237,
			"summary_team": 0.9176470588235295
		}]
	},
	"680110": {
		"nik": "680110",
		"nama": "YANTI SUSANTI",
		"band": "IV",
		"posisi": "OFF 1 FINANCE SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9850230414746541,
			"nilai_komparatif_unit": 0.9919259775883564,
			"summary_team": 0.8142857142857141
		}]
	},
	"680117": {
		"nik": "680117",
		"nama": "ELIN ANALIA",
		"band": "IV",
		"posisi": "ASMAN NETWORK PERF, QM & ADMIN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.048951048951049,
			"nilai_komparatif_unit": 1.0563019857032188,
			"summary_team": 0.9090909090909092
		}]
	},
	"680119": {
		"nik": "680119",
		"nama": "SRI ROSANTI",
		"band": "IV",
		"posisi": "ASMAN FINANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365351,
			"nilai_komparatif_unit": 0.9949168891755599,
			"summary_team": 0.8727272727272727
		}]
	},
	"680144": {
		"nik": "680144",
		"nama": "YANA SURYANA",
		"band": "IV",
		"posisi": "ASMAN DAVAL & INVENTORY MGT PLAN & REV",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444449,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9473684210526309,
			"nilai_komparatif_unit": 0.9540074776140642,
			"summary_team": 0.7999999999999998
		}]
	},
	"680242": {
		"nik": "680242",
		"nama": "BUDI HARTONO",
		"band": "IV",
		"posisi": "OFF 1 CASH & BANK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96,
			"nilai_komparatif_unit": 0.9667275773155858,
			"summary_team": 0.8
		}]
	},
	"680266": {
		"nik": "680266",
		"nama": "MOHAMAD IMRON",
		"band": "IV",
		"posisi": "OFF 1 CONSUMER BILLING SUPPORT &SOLUTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.92,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.024844720496894,
			"nilai_komparatif_unit": 1.0320267226777406,
			"summary_team": 0.9428571428571426
		}]
	},
	"680297": {
		"nik": "680297",
		"nama": "TRIHARI PRIYANTHO",
		"band": "IV",
		"posisi": "ASMAN OPTIMA & QE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8507936507936501,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9402985074626874,
			"nilai_komparatif_unit": 0.9468880188259011,
			"summary_team": 0.8
		}]
	},
	"680298": {
		"nik": "680298",
		"nama": "ARIF MARGANA",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.880701754385964,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0219123505976104,
			"nilai_komparatif_unit": 1.0290738030438578,
			"summary_team": 0.8999999999999999
		}]
	},
	"680328": {
		"nik": "680328",
		"nama": "SUHANDA",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CIKEMBANG",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8159999999999993,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9978991596638663,
			"nilai_komparatif_unit": 1.0048923302365713,
			"summary_team": 0.8142857142857142
		}]
	},
	"680331": {
		"nik": "680331",
		"nama": "ADE ROZALI",
		"band": "IV",
		"posisi": "OFF 1 COMMAND CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8507936507936501,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.012629161882894,
			"nilai_komparatif_unit": 1.0197255587355856,
			"summary_team": 0.8615384615384615
		}]
	},
	"680365": {
		"nik": "680365",
		"nama": "ACHMAD SUWANDA",
		"band": "IV",
		"posisi": "ASMAN PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.891666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0399320305862356,
			"nilai_komparatif_unit": 1.0472197630224054,
			"summary_team": 0.9272727272727272
		}]
	},
	"680368": {
		"nik": "680368",
		"nama": "ZENAL MANSUR",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8222222222222224,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0216216216216214,
			"nilai_komparatif_unit": 1.0287810366703023,
			"summary_team": 0.8400000000000001
		}]
	},
	"680442": {
		"nik": "680442",
		"nama": "RUSDI EDY",
		"band": "IV",
		"posisi": "OFF 1 HC RELATION AREA",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9023809523809516,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0690672047182996,
			"nilai_komparatif_unit": 1.0765591133384034,
			"summary_team": 0.9647058823529411
		}]
	},
	"680449": {
		"nik": "680449",
		"nama": "SYARIAH SAGALA",
		"band": "IV",
		"posisi": "ASMAN CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8999999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969697,
			"nilai_komparatif_unit": 0.9764925023389756,
			"summary_team": 0.8727272727272727
		}]
	},
	"680450": {
		"nik": "680450",
		"nama": "EVA PETTY",
		"band": "IV",
		"posisi": "OFF 1 CASH & BANK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8296296296296299,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.964285714285714,
			"nilai_komparatif_unit": 0.9710433254286015,
			"summary_team": 0.8
		}]
	},
	"680453": {
		"nik": "680453",
		"nama": "DEDI SADIMAN",
		"band": "IV",
		"posisi": "ASMAN BGES ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8358974358974363,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.870050195203569,
			"nilai_komparatif_unit": 0.8761474139084362,
			"summary_team": 0.7272727272727272
		}]
	},
	"680484": {
		"nik": "680484",
		"nama": "AENG SUHARTO",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE JATIWANGI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666671,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0228091236494592,
			"nilai_komparatif_unit": 1.0299768605853323,
			"summary_team": 0.8352941176470587
		}]
	},
	"680494": {
		"nik": "680494",
		"nama": "YUDI RUSTANDI",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545454,
			"nilai_komparatif_unit": 1.0326408212234666,
			"summary_team": 0.8545454545454546
		}]
	},
	"680508": {
		"nik": "680508",
		"nama": "PATONI ABDULAH MAS'UD",
		"band": "IV",
		"posisi": "ASMAN MAINTENANCE & DATA MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8507936507936501,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0471506105834472,
			"nilai_komparatif_unit": 1.0544889300561169,
			"summary_team": 0.8909090909090909
		}]
	},
	"680513": {
		"nik": "680513",
		"nama": "ASBULLAH",
		"band": "IV",
		"posisi": "OFF 1 CREDIT CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8600000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0465116279069764,
			"nilai_komparatif_unit": 1.0538454694573969,
			"summary_team": 0.8999999999999999
		}]
	},
	"680540": {
		"nik": "680540",
		"nama": "DEDY CIPTADI",
		"band": "IV",
		"posisi": "OFF 1 ENTERPRISE & BUSINESS BILLING SOL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8722222222222225,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0116148370176092,
			"nilai_komparatif_unit": 1.0187041255901401,
			"summary_team": 0.8823529411764706
		}]
	},
	"680548": {
		"nik": "680548",
		"nama": "ENDANG YULIARTI",
		"band": "IV",
		"posisi": "OFF 1 TAX OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8800000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9902597402597397,
			"nilai_komparatif_unit": 0.9971993745983952,
			"summary_team": 0.8714285714285712
		}]
	},
	"680581": {
		"nik": "680581",
		"nama": "TITIK SUMARTINI",
		"band": "IV",
		"posisi": "ASMAN COMMUNICATION, SECRETARIATE & SAS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8588235294117647,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.037359900373599,
			"nilai_komparatif_unit": 1.0446296075963613,
			"summary_team": 0.8909090909090909
		}]
	},
	"690020": {
		"nik": "690020",
		"nama": "RIANA PUSPASARI",
		"band": "IV",
		"posisi": "OFF 1 HC RELATION AREA",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8400000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9183673469387753,
			"nilai_komparatif_unit": 0.9248031670748587,
			"summary_team": 0.7714285714285714
		}]
	},
	"690082": {
		"nik": "690082",
		"nama": "WIDIATMOKO",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139868,
			"nilai_komparatif_unit": 0.9929238665610264,
			"summary_team": 0.8545454545454545
		}]
	},
	"690084": {
		"nik": "690084",
		"nama": "ALI FATAH YASIN",
		"band": "IV",
		"posisi": "OFF 1 CAREER & TALENT DEVELOPMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8629629629629628,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.927038626609442,
			"nilai_komparatif_unit": 0.9335352141459519,
			"summary_team": 0.7999999999999998
		}]
	},
	"690098": {
		"nik": "690098",
		"nama": "AHMAD BAHTIAR",
		"band": "IV",
		"posisi": "OFF 1 HC CAPACITY SOLUTION ADVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769231,
			"nilai_komparatif_unit": 0.9295457474188326,
			"summary_team": 0.8
		}]
	},
	"690115": {
		"nik": "690115",
		"nama": "HARYANTO",
		"band": "IV",
		"posisi": "ASMAN NW OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0045248868778276,
			"nilai_komparatif_unit": 1.0115644898381408,
			"summary_team": 0.8705882352941177
		}]
	},
	"690135": {
		"nik": "690135",
		"nama": "SARJOWINOTO",
		"band": "IV",
		"posisi": "OFF 1 CASH BANK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8945454545454542,
			"nilai_komparatif_unit": 0.9008143334077046,
			"summary_team": 0.7454545454545454
		}]
	},
	"690140": {
		"nik": "690140",
		"nama": "SUKIMAN",
		"band": "IV",
		"posisi": "OFF 1 ENTERPRS & BUSINESS BILL DATA MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0788013318534957,
			"nilai_komparatif_unit": 1.086361456195372,
			"summary_team": 0.9529411764705882
		}]
	},
	"690159": {
		"nik": "690159",
		"nama": "DURADJI",
		"band": "IV",
		"posisi": "OFF 1 LEARNING DEVT CONS BUSINESS MAT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0074626865671639,
			"nilai_komparatif_unit": 1.0145228773134642,
			"summary_team": 0.8999999999999998
		}]
	},
	"690173": {
		"nik": "690173",
		"nama": "NANA KUSMANA",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE LOSARI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9336734693877546,
			"nilai_komparatif_unit": 0.9402165531927726,
			"summary_team": 0.7625
		}]
	},
	"690176": {
		"nik": "690176",
		"nama": "SAEFULLAH",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION CONSUMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8749999999999997,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9546218487394961,
			"nilai_komparatif_unit": 0.9613117365463111,
			"summary_team": 0.8352941176470587
		}]
	},
	"690287": {
		"nik": "690287",
		"nama": "Fathur Rachman",
		"band": "IV",
		"posisi": "ASISTEN MANAJER KEUANGAN DAN UMUM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555557,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.966233766233766,
			"nilai_komparatif_unit": 0.9730050291163361,
			"summary_team": 0.8266666666666667
		}]
	},
	"690296": {
		"nik": "690296",
		"nama": "GUNANDAR",
		"band": "IV",
		"posisi": "ASMAN NE FTM ACCESS MTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8000000000000004,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9852941176470582,
			"nilai_komparatif_unit": 0.9921989534335816,
			"summary_team": 0.7882352941176469
		}]
	},
	"690297": {
		"nik": "690297",
		"nama": "HERY SUTANTI",
		"band": "IV",
		"posisi": "OFF 1 FINANCE SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7750000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9853372434017592,
			"nilai_komparatif_unit": 0.9922423814089587,
			"summary_team": 0.7636363636363637
		}]
	},
	"690391": {
		"nik": "690391",
		"nama": "EDI KURNIA",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8547619047619041,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.169916434540391,
			"nilai_komparatif_unit": 1.1781150837759586,
			"summary_team": 1
		}]
	},
	"690397": {
		"nik": "690397",
		"nama": "Rokhmad",
		"band": "IV",
		"posisi": "ASISTEN MANAJER KEUANGAN DAN UMUM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.005,
			"nilai_komparatif_unit": 1.0120429325022537,
			"summary_team": 0.8933333333333334
		}]
	},
	"690399": {
		"nik": "690399",
		"nama": "Siti Zulaekhah",
		"band": "IV",
		"posisi": "ASISTEN MANAJER KEUANGAN DAN UMUM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9111111111111112,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0097560975609754,
			"nilai_komparatif_unit": 1.0168323602862104,
			"summary_team": 0.9199999999999999
		}]
	},
	"690402": {
		"nik": "690402",
		"nama": "RINI MARLIANI",
		"band": "IV",
		"posisi": "OFF 1 ASSET ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.877777777777778,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.005212211466865,
			"nilai_komparatif_unit": 1.0122566311243797,
			"summary_team": 0.8823529411764706
		}]
	},
	"690403": {
		"nik": "690403",
		"nama": "ETTY SUHERTY",
		"band": "IV",
		"posisi": "OFF 1 SERVICE DELIVERY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9890109890109887,
			"nilai_komparatif_unit": 0.9959418722344631,
			"summary_team": 0.857142857142857
		}]
	},
	"690404": {
		"nik": "690404",
		"nama": "INNA KARTINA",
		"band": "IV",
		"posisi": "ASMAN FINANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882352,
			"nilai_komparatif_unit": 0.9477721346231232,
			"summary_team": 0.8
		}]
	},
	"690406": {
		"nik": "690406",
		"nama": "NITA NURHAYATI",
		"band": "IV",
		"posisi": "OFF 1 GENERAL ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9166666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0314049586776857,
			"nilai_komparatif_unit": 1.038632934306001,
			"summary_team": 0.9454545454545454
		}]
	},
	"690408": {
		"nik": "690408",
		"nama": "ERNI SUKAESIH",
		"band": "IV",
		"posisi": "OFF 1 BUDGET CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0169491525423728,
			"nilai_komparatif_unit": 1.0240758234275273,
			"summary_team": 1
		}]
	},
	"690428": {
		"nik": "690428",
		"nama": "GENTUR WISANGGONO",
		"band": "IV",
		"posisi": "ASMAN OM IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8944444444444447,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9147374364765666,
			"nilai_komparatif_unit": 0.9211478185883967,
			"summary_team": 0.8181818181818181
		}]
	},
	"690445": {
		"nik": "690445",
		"nama": "BAMBANG SUPRIYANTO",
		"band": "IV",
		"posisi": "OFF 1 CASH & BANK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9249999999999998,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0614250614250615,
			"nilai_komparatif_unit": 1.0688634147223923,
			"summary_team": 0.9818181818181818
		}]
	},
	"690446": {
		"nik": "690446",
		"nama": "MUHAMMAD FARIKHIN",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666663,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.97737556561086,
			"nilai_komparatif_unit": 0.9842249090317053,
			"summary_team": 0.8470588235294116
		}]
	},
	"690481": {
		"nik": "690481",
		"nama": "ROHYADI",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE PELAB RATU",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8159999999999993,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9803921568627457,
			"nilai_komparatif_unit": 0.9872626402324207,
			"summary_team": 0.7999999999999998
		}]
	},
	"690501": {
		"nik": "690501",
		"nama": "ACHMAD RIYADI",
		"band": "IV",
		"posisi": "OFF 1 ENTERPRS & BUSINESS BILL DATA MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9589345172031074,
			"nilai_komparatif_unit": 0.9656546277292197,
			"summary_team": 0.8470588235294119
		}]
	},
	"690574": {
		"nik": "690574",
		"nama": "KUSWORO",
		"band": "IV",
		"posisi": "ASMAN WAN & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8400000000000004,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9103641456582626,
			"nilai_komparatif_unit": 0.916743880215818,
			"summary_team": 0.764705882352941
		}]
	},
	"690592": {
		"nik": "690592",
		"nama": "TITIK DARWATI",
		"band": "IV",
		"posisi": "OFF 1 HC SOLUTION AREA",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9023809523809516,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9023746701846972,
			"nilai_komparatif_unit": 0.9086984153527116,
			"summary_team": 0.8142857142857142
		}]
	},
	"690615": {
		"nik": "690615",
		"nama": "SOSIDAH",
		"band": "IV",
		"posisi": "INNOVATOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8471014492753652,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9443969204448215,
			"nilai_komparatif_unit": 0.9510151530478357,
			"summary_team": 0.8
		}]
	},
	"700011": {
		"nik": "700011",
		"nama": "ATIEK NURHAYATI",
		"band": "IV",
		"posisi": "OFF 1 OFFICE ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8303030303030305,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9979144942648586,
			"nilai_komparatif_unit": 1.004907772300807,
			"summary_team": 0.8285714285714283
		}]
	},
	"700031": {
		"nik": "700031",
		"nama": "Tri Sulaningsih",
		"band": "IV",
		"posisi": "ASISTEN MANAJER KEUANGAN DAN UMUM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777781,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0936708860759488,
			"nilai_komparatif_unit": 1.101335214663325,
			"summary_team": 0.9599999999999999
		}]
	},
	"700059": {
		"nik": "700059",
		"nama": "EDI SUMANTRI",
		"band": "IV",
		"posisi": "OFF 1 DUNNING MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.005494505494505,
			"nilai_komparatif_unit": 1.0125409034383706,
			"summary_team": 0.8714285714285712
		}]
	},
	"700080": {
		"nik": "700080",
		"nama": "DJOKO PURNOMO",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8133333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9660421545667441,
			"nilai_komparatif_unit": 0.9728120746552471,
			"summary_team": 0.7857142857142856
		}]
	},
	"700134": {
		"nik": "700134",
		"nama": "HENDRA GUNAWAN",
		"band": "IV",
		"posisi": "OFF 1 CAREER & TALENT ANALYSIS & ASSESSM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8814814814814814,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.027681660899654,
			"nilai_komparatif_unit": 1.0348835440553956,
			"summary_team": 0.9058823529411764
		}]
	},
	"700142": {
		"nik": "700142",
		"nama": "RISMAN SABHARADA",
		"band": "IV",
		"posisi": "OFF 1 CREDIT CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8600000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.029900332225913,
			"nilai_komparatif_unit": 1.0371177635929936,
			"summary_team": 0.8857142857142855
		}]
	},
	"700150": {
		"nik": "700150",
		"nama": "SUPRIJONO",
		"band": "IV",
		"posisi": "OFF 1 BOD BUDGET PLANNING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8368421052631614,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9700332963373989,
			"nilai_komparatif_unit": 0.9768311859205263,
			"summary_team": 0.8117647058823529
		}]
	},
	"700210": {
		"nik": "700210",
		"nama": "SUPRIADI",
		"band": "IV",
		"posisi": "ASMAN NW OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346933,
			"nilai_komparatif_unit": 0.9864567115465155,
			"summary_team": 0.7999999999999998
		}]
	},
	"700227": {
		"nik": "700227",
		"nama": "SURIPTO",
		"band": "IV",
		"posisi": "OFF 1 HC PRODUCTIVITY EVALUATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8629629629629628,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0633678364049486,
			"nilai_komparatif_unit": 1.0708198044615334,
			"summary_team": 0.9176470588235295
		}]
	},
	"700259": {
		"nik": "700259",
		"nama": "SRI WAHYUNI",
		"band": "IV",
		"posisi": "OFF 1 EVENT FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0553470919324528,
			"nilai_komparatif_unit": 1.0627428514696968,
			"summary_team": 0.9000000000000001
		}]
	},
	"700261": {
		"nik": "700261",
		"nama": "MAYA SYAFITRI",
		"band": "IV",
		"posisi": "OFF 1 INNOVATION SUPPORT READINESS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8909090909090913,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0102040816326523,
			"nilai_komparatif_unit": 1.017283483782344,
			"summary_team": 0.8999999999999998
		}]
	},
	"700331": {
		"nik": "700331",
		"nama": "MISBAH",
		"band": "IV",
		"posisi": "ASMAN OPTIMA & QE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777781,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0454206999255395,
			"nilai_komparatif_unit": 1.052746896369355,
			"summary_team": 0.9176470588235295
		}]
	},
	"700340": {
		"nik": "700340",
		"nama": "SYAMSUL FAJRI",
		"band": "IV",
		"posisi": "ASMAN MAINTENANCE & DATA MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777781,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9918093819806401,
			"nilai_komparatif_unit": 0.9987598760427214,
			"summary_team": 0.8705882352941177
		}]
	},
	"700385": {
		"nik": "700385",
		"nama": "DEWI ANNA MARLIANA",
		"band": "IV",
		"posisi": "OFF 1 CASH BANK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0368663594470044,
			"nilai_komparatif_unit": 1.0441326079877438,
			"summary_team": 0.857142857142857
		}]
	},
	"700387": {
		"nik": "700387",
		"nama": "ELLY SRI SUWARNI",
		"band": "IV",
		"posisi": "OFF 1 FINANCE SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181815,
			"nilai_komparatif_unit": 0.9886986586182124,
			"summary_team": 0.8181818181818181
		}]
	},
	"700396": {
		"nik": "700396",
		"nama": "RETNO WULANSARI",
		"band": "IV",
		"posisi": "OFF 1 FINANCE SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8463768115942021,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0633561643835625,
			"nilai_komparatif_unit": 1.07080805064387,
			"summary_team": 0.8999999999999999
		}]
	},
	"700445": {
		"nik": "700445",
		"nama": "KRISTIANI PUDIASTUTI",
		"band": "IV",
		"posisi": "OFF 1 PENGELOLA PROGRAM KERJA",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0303030303030303,
			"nilai_komparatif_unit": 1.0375232837351616,
			"summary_team": 0.9272727272727272
		}]
	},
	"700446": {
		"nik": "700446",
		"nama": "SRI KUSRINI SODIKIN",
		"band": "IV",
		"posisi": "OFF 1 EMPLOYEE DATA MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9722530521642614,
			"nilai_komparatif_unit": 0.9790664975587919,
			"summary_team": 0.8588235294117645
		}]
	},
	"700447": {
		"nik": "700447",
		"nama": "EUIS TATI ROHAENI",
		"band": "IV",
		"posisi": "OFF 1 CAREER & TALENT ANALYSIS & ASSESSM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8419753086419745,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0224403927068737,
			"nilai_komparatif_unit": 1.0296055456157418,
			"summary_team": 0.8608695652173916
		}]
	},
	"700449": {
		"nik": "700449",
		"nama": "LITA DWI APRIYANTI",
		"band": "IV",
		"posisi": "OFF 1 PROCUREMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.92,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9782608695652172,
			"nilai_komparatif_unit": 0.9851164171014799,
			"summary_team": 0.8999999999999998
		}]
	},
	"700452": {
		"nik": "700452",
		"nama": "ELNITA HASYIM",
		"band": "IV",
		"posisi": "OFF 1 CASH IN CASH OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0212053571428568,
			"nilai_komparatif_unit": 1.0283618550545952,
			"summary_team": 0.8714285714285712
		}]
	},
	"700457": {
		"nik": "700457",
		"nama": "DEWI SUPRIYATNI",
		"band": "IV",
		"posisi": "ASMAN PL DSG, DEPL, SOL PARTNER&INS REL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666663,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0316742081447967,
			"nilai_komparatif_unit": 1.0389040706445778,
			"summary_team": 0.8941176470588235
		}]
	},
	"700459": {
		"nik": "700459",
		"nama": "SRI MULYATI",
		"band": "IV",
		"posisi": "MGR SHARED SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0188679245283017,
			"nilai_komparatif_unit": 1.0260080419622961,
			"summary_team": 0.9000000000000001
		}]
	},
	"700460": {
		"nik": "700460",
		"nama": "YIYIN SURYA DEWI",
		"band": "IV",
		"posisi": "OFF 1 PAYMENT PARTNERSHIP & READINESS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0070078930370687,
			"summary_team": 0.8000000000000002
		}]
	},
	"700476": {
		"nik": "700476",
		"nama": "KUSHARTONO",
		"band": "IV",
		"posisi": "ASMAN BGES FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8944444444444447,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9167701863354035,
			"nilai_komparatif_unit": 0.9231948137408155,
			"summary_team": 0.8200000000000001
		}]
	},
	"700493": {
		"nik": "700493",
		"nama": "OYOK ROSIDI",
		"band": "IV",
		"posisi": "ASMAN CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8507936507936501,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0898914518317513,
			"nilai_komparatif_unit": 1.0975292945482036,
			"summary_team": 0.9272727272727272
		}]
	},
	"700508": {
		"nik": "700508",
		"nama": "ACHMAD AGUS",
		"band": "IV",
		"posisi": "OFF 1 ENTERPRISE & BUSINESS BILLING OPR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9542410714285711,
			"nilai_komparatif_unit": 0.9609282907887201,
			"summary_team": 0.8142857142857142
		}]
	},
	"700575": {
		"nik": "700575",
		"nama": "DEDEN SUDRAJAT",
		"band": "IV",
		"posisi": "OFF 1 PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8507936507936501,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9616689280868392,
			"nilai_komparatif_unit": 0.9684082010719441,
			"summary_team": 0.8181818181818181
		}]
	},
	"700596": {
		"nik": "700596",
		"nama": "ISRUKIYAH",
		"band": "IV",
		"posisi": "OFF 1 COMPETENCY DEVELOPMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8488888888888888,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.976065818997756,
			"nilai_komparatif_unit": 0.982905983854431,
			"summary_team": 0.8285714285714284
		}]
	},
	"700603": {
		"nik": "700603",
		"nama": "ABDUL MUCHLIS",
		"band": "IV",
		"posisi": "ASMAN HUMAN CAPITAL & CDC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.080900750625521,
			"nilai_komparatif_unit": 1.0884755874695917,
			"summary_team": 0.9818181818181818
		}]
	},
	"700612": {
		"nik": "700612",
		"nama": "IWAN WAHYUDI",
		"band": "IV",
		"posisi": "OFF 1 CONSUMER BILLING OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9266666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.032887975334018,
			"nilai_komparatif_unit": 1.0401263437844332,
			"summary_team": 0.9571428571428571
		}]
	},
	"700640": {
		"nik": "700640",
		"nama": "SOFIE MEI SYAHDIANI",
		"band": "IV",
		"posisi": "OFF 1 ACCOUNTING OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0634920634920628,
			"nilai_komparatif_unit": 1.0709449021187865,
			"summary_team": 0.9571428571428569
		}]
	},
	"700666": {
		"nik": "700666",
		"nama": "ANIS HENDRAYANI BUDIAWATI",
		"band": "IV",
		"posisi": "OFF 1 LOGISTIC SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0101010101010102,
			"nilai_komparatif_unit": 1.017179689936433,
			"summary_team": 0.9090909090909092
		}]
	},
	"710001": {
		"nik": "710001",
		"nama": "SRI KHUNAENI",
		"band": "IV",
		"posisi": "OFF 1 FINANCE SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9111111111111112,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9684361549497846,
			"nilai_komparatif_unit": 0.9752228519369026,
			"summary_team": 0.8823529411764706
		}]
	},
	"710015": {
		"nik": "710015",
		"nama": "JUMINTA",
		"band": "IV",
		"posisi": "OFF 1 SEKRETARIAT BOC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8368421052631614,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9981502034776132,
			"nilai_komparatif_unit": 1.0051451333385124,
			"summary_team": 0.8352941176470587
		}]
	},
	"710023": {
		"nik": "710023",
		"nama": "AGUS TOHARI",
		"band": "IV",
		"posisi": "ASMAN CONSUMER ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444447,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9891640866873062,
			"nilai_komparatif_unit": 0.9960960428029204,
			"summary_team": 0.8352941176470587
		}]
	},
	"710041": {
		"nik": "710041",
		"nama": "MUCHARAMSYAH",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK & LEGAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828875,
			"nilai_komparatif_unit": 1.0123929619837906,
			"summary_team": 0.8545454545454546
		}]
	},
	"710063": {
		"nik": "710063",
		"nama": "MUHAMAD SARIFUDIN",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8588235294117647,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8891656288916563,
			"nilai_komparatif_unit": 0.8953968065111668,
			"summary_team": 0.7636363636363636
		}]
	},
	"710096": {
		"nik": "710096",
		"nama": "GUNAWAN SETYASTOMO NUGROHO",
		"band": "IV",
		"posisi": "OFF 1 SECURITY & SAFETY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8206349206349192,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9748549323017424,
			"nilai_komparatif_unit": 0.9816866113939716,
			"summary_team": 0.7999999999999998
		}]
	},
	"710102": {
		"nik": "710102",
		"nama": "SITI ALFIAH WIDYA RAHAYU",
		"band": "IV",
		"posisi": "OFF 1 HC CAPABILITY SOLUTION ADVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555556,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0313216195569135,
			"nilai_komparatif_unit": 1.0385490111535847,
			"summary_team": 0.8823529411764706
		}]
	},
	"710154": {
		"nik": "710154",
		"nama": "SUDIRMAN",
		"band": "IV",
		"posisi": "OFF 1 CONSUMER BILLING OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9266666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9866392600205546,
			"nilai_komparatif_unit": 0.9935535224209511,
			"summary_team": 0.9142857142857141
		}]
	},
	"710179": {
		"nik": "710179",
		"nama": "ARIS IRWAN ARBIYANTO",
		"band": "IV",
		"posisi": "OFF 1 GCT BUDGET & RESOURCING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.875362318840579,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.01182592242195,
			"nilai_komparatif_unit": 1.0189166902584161,
			"summary_team": 0.8857142857142859
		}]
	},
	"710227": {
		"nik": "710227",
		"nama": "HERYANI",
		"band": "IV",
		"posisi": "ASMAN FINANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.858333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.000571102227299,
			"nilai_komparatif_unit": 1.0075829974876895,
			"summary_team": 0.8588235294117645
		}]
	},
	"710228": {
		"nik": "710228",
		"nama": "SRI LESTARI ANDAYANI",
		"band": "IV",
		"posisi": "ASMAN COMMUNITY DEVELOPMENT CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.833333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0285714285714287,
			"nilai_komparatif_unit": 1.035779547123842,
			"summary_team": 0.857142857142857
		}]
	},
	"710229": {
		"nik": "710229",
		"nama": "IDA FARIDA",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8611111111111126,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0246679316888028,
			"nilai_komparatif_unit": 1.0318486949525922,
			"summary_team": 0.8823529411764706
		}]
	},
	"710240": {
		"nik": "710240",
		"nama": "IIS AGUSTINI",
		"band": "IV",
		"posisi": "OFF 1 CASH OUT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8866666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9989258861439307,
			"nilai_komparatif_unit": 1.0059262519059864,
			"summary_team": 0.8857142857142855
		}]
	},
	"710251": {
		"nik": "710251",
		"nama": "YANTHI APRIANTI",
		"band": "IV",
		"posisi": "OFF 1 REPORTING MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8368421052631614,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9918238993710651,
			"nilai_komparatif_unit": 0.9987744951694657,
			"summary_team": 0.8300000000000001
		}]
	},
	"710261": {
		"nik": "710261",
		"nama": "WINATA",
		"band": "IV",
		"posisi": "OFF 1 TAX OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8646153846153859,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9583121504829678,
			"nilai_komparatif_unit": 0.9650278995296756,
			"summary_team": 0.8285714285714287
		}]
	},
	"710341": {
		"nik": "710341",
		"nama": "EUSIBIUS SUGIO SUSILO",
		"band": "IV",
		"posisi": "ASMAN DATA CENTER & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8944444444444447,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9757199322416711,
			"nilai_komparatif_unit": 0.9825576731609564,
			"summary_team": 0.8727272727272727
		}]
	},
	"710493": {
		"nik": "710493",
		"nama": "RATNA AGUSTINA",
		"band": "IV",
		"posisi": "OFF 1 CONSOLIDATION CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.962566844919786,
			"nilai_komparatif_unit": 0.9693124104100124,
			"summary_team": 0.8181818181818181
		}]
	},
	"720012": {
		"nik": "720012",
		"nama": "PERTA HAMIDA LINDAWATY P.",
		"band": "IV",
		"posisi": "OFF 1 HC CAPABILITY SOLUTION ADVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8133333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0011709601873533,
			"nilai_komparatif_unit": 1.0081870591881654,
			"summary_team": 0.8142857142857142
		}]
	},
	"720018": {
		"nik": "720018",
		"nama": "NOVIEYANTI",
		"band": "IV",
		"posisi": "OFF 1 TALENT ACQUISITION PROGRAM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8518518518518513,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9943734015345274,
			"nilai_komparatif_unit": 1.0013418639713874,
			"summary_team": 0.8470588235294116
		}]
	},
	"720033": {
		"nik": "720033",
		"nama": "RITA EMILYA",
		"band": "IV",
		"posisi": "OFF 1 COMPETENCY DEVELOPMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545452,
			"nilai_komparatif_unit": 1.0326408212234663,
			"summary_team": 0.8545454545454546
		}]
	},
	"720039": {
		"nik": "720039",
		"nama": "NENENG EKA RATNA",
		"band": "IV",
		"posisi": "JM CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8476190476190508,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9438202247190973,
			"nilai_komparatif_unit": 0.9504344159001508,
			"summary_team": 0.7999999999999998
		}]
	},
	"720518": {
		"nik": "720518",
		"nama": "AMIEN ARIE ZUNIANTO",
		"band": "IV",
		"posisi": "OFF 1 EVALUASI & ANALISA",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7666666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9723320158102766,
			"nilai_komparatif_unit": 0.9791460145735923,
			"summary_team": 0.7454545454545454
		}]
	},
	"720537": {
		"nik": "720537",
		"nama": "BUDI ASTUTI",
		"band": "IV",
		"posisi": "OFF 1 CAPEX MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0129870129870127,
			"nilai_komparatif_unit": 1.0200859176219652,
			"summary_team": 0.9454545454545454
		}]
	},
	"730008": {
		"nik": "730008",
		"nama": "LENCI BETHLY KIOLOL",
		"band": "IV",
		"posisi": "OFF 1 LEARNING DEVT CORP BUSINESS MAT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0219780219780217,
			"nilai_komparatif_unit": 1.0291399346422785,
			"summary_team": 0.8857142857142857
		}]
	},
	"730018": {
		"nik": "730018",
		"nama": "RADEN SITI FATIMAH",
		"band": "IV",
		"posisi": "ASMAN SALES SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8282051282051274,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227645237661638,
			"nilai_komparatif_unit": 1.0299319481508253,
			"summary_team": 0.8470588235294116
		}]
	},
	"730022": {
		"nik": "730022",
		"nama": "EMILDA NUR AINI,SAP",
		"band": "IV",
		"posisi": "SO SUBSIDIARY ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9075268817204291,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0357819905213284,
			"nilai_komparatif_unit": 1.0430406399206238,
			"summary_team": 0.9400000000000001
		}]
	},
	"730134": {
		"nik": "730134",
		"nama": "ERNI HIDAYATI",
		"band": "IV",
		"posisi": "OFF 1 LEARNING DEVT FIN & CORP STRGY MAT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333325,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9085714285714294,
			"nilai_komparatif_unit": 0.9149385999593945,
			"summary_team": 0.757142857142857
		}]
	},
	"730146": {
		"nik": "730146",
		"nama": "ADHI PRASETIO, ST",
		"band": "IV",
		"posisi": "DOSEN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346935,
			"nilai_komparatif_unit": 0.9864567115465157,
			"summary_team": 0.7999999999999998
		}]
	},
	"730274": {
		"nik": "730274",
		"nama": "DIWANTO WURYANTORO",
		"band": "IV",
		"posisi": "OFF 1 HC SOLUTION AREA",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9023809523809516,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0448548812664913,
			"nilai_komparatif_unit": 1.0521771125136659,
			"summary_team": 0.9428571428571426
		}]
	},
	"730324": {
		"nik": "730324",
		"nama": "ELISABETH MARGARETA VILLANUEVA",
		"band": "IV",
		"posisi": "OFF 1 LOGISTIC PLANNING & EVALUATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8111111111111113,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9427880741337628,
			"nilai_komparatif_unit": 0.949395032113916,
			"summary_team": 0.7647058823529411
		}]
	},
	"740002": {
		"nik": "740002",
		"nama": "HILMAN GUMILAR",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8222222222222225,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0588235294117643,
			"nilai_komparatif_unit": 1.0662436514510132,
			"summary_team": 0.8705882352941177
		}]
	},
	"740317": {
		"nik": "740317",
		"nama": "TAUFIK ISMAIL ACHMAD",
		"band": "IV",
		"posisi": "OFF 1 LEARNING DEVT DIG TRANSFORM MAT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8749999999999997,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649354,
			"nilai_komparatif_unit": 0.9416177701125839,
			"summary_team": 0.8181818181818181
		}]
	},
	"750061": {
		"nik": "750061",
		"nama": "Inne Dwi Widya Ratna Ernawati",
		"band": "IV",
		"posisi": "OFF 1 PENGEMBANGAN KOMPETENSI SDM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8694444444444438,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.981469648562301,
			"nilai_komparatif_unit": 0.9883476828785549,
			"summary_team": 0.8533333333333333
		}]
	},
	"790041": {
		"nik": "790041",
		"nama": "ORYZA JANU HAPSARI",
		"band": "IV",
		"posisi": "OFF 1 LEADERSHIP MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8289855072463761,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.982267732267733,
			"nilai_komparatif_unit": 0.9891513594692292,
			"summary_team": 0.8142857142857142
		}]
	},
	"790108": {
		"nik": "790108",
		"nama": "LOLA INDAH DJULIA",
		"band": "IV",
		"posisi": "OFF 1 PROC PLAN & INIT STRATEGIC CAT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0115702479338844,
			"nilai_komparatif_unit": 1.018659224030886,
			"summary_team": 0.9272727272727272
		}]
	},
	"810086": {
		"nik": "810086",
		"nama": "ANGGUN  MUHAMAD  BARKAH",
		"band": "IV",
		"posisi": "MGR WORKING ARRANGEMENT & CRISIS MGMT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.858333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9457452884066252,
			"nilai_komparatif_unit": 0.9523729702280903,
			"summary_team": 0.8117647058823529
		}]
	},
	"810098": {
		"nik": "810098",
		"nama": "RIEKE ARIESKA AFRIANA",
		"band": "IV",
		"posisi": "OFF 1 BUDGET PLANNING & CONTROLLING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9935064935064933,
			"nilai_komparatif_unit": 1.0004688807446198,
			"summary_team": 0.9272727272727272
		}]
	},
	"810106": {
		"nik": "810106",
		"nama": "HANI SULISTIANI HANIFAH",
		"band": "IV",
		"posisi": "MGR BUDGET CAPEX",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8463768115942021,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0360906217070611,
			"nilai_komparatif_unit": 1.043351433960694,
			"summary_team": 0.8769230769230769
		}]
	},
	"820081": {
		"nik": "820081",
		"nama": "DYAH AYU PRAMITASARI",
		"band": "IV",
		"posisi": "OFF 1 PRO-HIRE PROGRAM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9773755656108595,
			"nilai_komparatif_unit": 0.9842249090317047,
			"summary_team": 0.8470588235294119
		}]
	},
	"830004": {
		"nik": "830004",
		"nama": "DIDIEK RACHMAWAN",
		"band": "IV",
		"posisi": "OFF 1 LEARNING DEVT DIGITAL PLATFORM MAT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7600000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9398496240601499,
			"nilai_komparatif_unit": 0.9464359896964926,
			"summary_team": 0.7142857142857141
		}]
	},
	"830112": {
		"nik": "830112",
		"nama": "OKFARIMA MANDASARI",
		"band": "IV",
		"posisi": "MGR CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882352,
			"nilai_komparatif_unit": 0.9477721346231232,
			"summary_team": 0.8
		}]
	},
	"830141": {
		"nik": "830141",
		"nama": "ROKHAYAH",
		"band": "IV",
		"posisi": "OFF 1 IP OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8611111111111115,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9836812144212518,
			"nilai_komparatif_unit": 0.9905747471544896,
			"summary_team": 0.8470588235294116
		}]
	},
	"835546": {
		"nik": "835546",
		"nama": "AKHMAD SOFYAN",
		"band": "IV",
		"posisi": "EXPERT DATA CENTER INTEGRATOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.766666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8152173913043474,
			"nilai_komparatif_unit": 0.8209303475845664,
			"summary_team": 0.625
		}]
	},
	"840116": {
		"nik": "840116",
		"nama": "RENNI KUSUMOWARDANI",
		"band": "IV",
		"posisi": "RESEARCHER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9169193450576059,
			"nilai_komparatif_unit": 0.9233450177513884,
			"summary_team": 0.7636363636363636
		}]
	},
	"840177": {
		"nik": "840177",
		"nama": "ARIF RACHMAN YULLIANDI",
		"band": "IV",
		"posisi": "OFF 1 DIGITAL BUSINESS PARENTING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8289855072463764,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.050185109008639,
			"nilai_komparatif_unit": 1.0575446939216937,
			"summary_team": 0.8705882352941177
		}]
	},
	"845987": {
		"nik": "845987",
		"nama": "RADEN ARDIAN",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8421052631578936,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8255952380952392,
			"nilai_komparatif_unit": 0.8313809212157237,
			"summary_team": 0.6952380952380952
		}]
	},
	"850009": {
		"nik": "850009",
		"nama": "SRI DWI NINGSIH",
		"band": "IV",
		"posisi": "SENIOR AM ENTERPRISE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8318181818181802,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0304449648711964,
			"nilai_komparatif_unit": 1.0376662129655996,
			"summary_team": 0.8571428571428572
		}]
	},
	"850125": {
		"nik": "850125",
		"nama": "RINDANG SRI AYOMI",
		"band": "IV",
		"posisi": "OFF 1 ASSET ACCOUNTING POLICY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8904761904761893,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9908776344762517,
			"nilai_komparatif_unit": 0.9978215989514847,
			"summary_team": 0.8823529411764706
		}]
	},
	"850144": {
		"nik": "850144",
		"nama": "NADYA DIAN PRATIWI",
		"band": "IV",
		"posisi": "OFF 1 LEADERSHIP & CAPABILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8289855072463761,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9792266556972448,
			"nilai_komparatif_unit": 0.9860889713594174,
			"summary_team": 0.8117647058823529
		}]
	},
	"850170": {
		"nik": "850170",
		"nama": "IRMA RACHMAWATI HARRIRI",
		"band": "IV",
		"posisi": "OFF 1 LEARNING DEVT DIG TRANSFORM MAT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545454,
			"nilai_komparatif_unit": 1.0326408212234666,
			"summary_team": 0.8545454545454546
		}]
	},
	"855502": {
		"nik": "855502",
		"nama": "WHILDAN ZAINUDIN",
		"band": "IV",
		"posisi": "AUDITOR PRATAMA I",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.059626149540178,
			"nilai_komparatif_unit": 1.0670518962554363,
			"summary_team": 0.925
		}]
	},
	"860055": {
		"nik": "860055",
		"nama": "AYESHA RACHMA HARDIYANI",
		"band": "IV",
		"posisi": "ASMAN HOME SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8547619047619041,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0763231197771597,
			"nilai_komparatif_unit": 1.0838658770738818,
			"summary_team": 0.9199999999999999
		}]
	},
	"860117": {
		"nik": "860117",
		"nama": "RIDWAN NOOR MUCHLIS",
		"band": "IV",
		"posisi": "OFF 1 DIGI BIZ TECH PLATFORM EXPLORATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.877777777777778,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9918093819806402,
			"nilai_komparatif_unit": 0.9987598760427215,
			"summary_team": 0.8705882352941177
		}]
	},
	"860157": {
		"nik": "860157",
		"nama": "THERESIA ALFARIN PARINTAK",
		"band": "IV",
		"posisi": "OFF 1 SEKRETARIAT BOC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8368421052631614,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0413297394429424,
			"nilai_komparatif_unit": 1.048627266873277,
			"summary_team": 0.8714285714285712
		}]
	},
	"860162": {
		"nik": "860162",
		"nama": "MAHARDIKA PRATAMA",
		"band": "IV",
		"posisi": "OFF 1 POST ASSESSMENT DEVELOPMENT REVIEW",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0196078431372548,
			"nilai_komparatif_unit": 1.026753145841717,
			"summary_team": 0.9176470588235295
		}]
	},
	"870007": {
		"nik": "870007",
		"nama": "SHINTA DEVI LUKITASARI",
		"band": "IV",
		"posisi": "MGR CYBER SECURITY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9773755656108596,
			"nilai_komparatif_unit": 0.9842249090317048,
			"summary_team": 0.8470588235294116
		}]
	},
	"870016": {
		"nik": "870016",
		"nama": "FEBRYANTI",
		"band": "IV",
		"posisi": "OFF 1 QUALITY & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0313216195569133,
			"nilai_komparatif_unit": 1.0385490111535844,
			"summary_team": 0.8823529411764706
		}]
	},
	"870034": {
		"nik": "870034",
		"nama": "ZITA HAPSARI",
		"band": "IV",
		"posisi": "OFF 1 STRATEGIC PLANNING & EVALUATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8694444444444437,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9742529599699312,
			"nilai_komparatif_unit": 0.9810804205044479,
			"summary_team": 0.8470588235294116
		}]
	},
	"870044": {
		"nik": "870044",
		"nama": "ERVIANTI RIZQIA",
		"band": "IV",
		"posisi": "OFF 1 LEARING DESIGN MATERIAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9890109890109889,
			"nilai_komparatif_unit": 0.9959418722344633,
			"summary_team": 0.857142857142857
		}]
	},
	"870045": {
		"nik": "870045",
		"nama": "NOMARHINTA SOLIHAH",
		"band": "IV",
		"posisi": "ENG 1 TRANSMISSION QUALITY ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8138888888888886,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0696647259586431,
			"nilai_komparatif_unit": 1.0771608219436866,
			"summary_team": 0.8705882352941176
		}]
	},
	"870072": {
		"nik": "870072",
		"nama": "ERLINA DEWI FITRIANY",
		"band": "IV",
		"posisi": "OFF 1  INTERNAL ASSESSMENT EVALUATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9030303030303026,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.977102250296092,
			"nilai_komparatif_unit": 0.983949678352446,
			"summary_team": 0.8823529411764706
		}]
	},
	"875572": {
		"nik": "875572",
		"nama": "SHERLYANA",
		"band": "IV",
		"posisi": "OFF 1 MARKETING SEGMENTATION ANALYSIS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8442906574394471,
			"nilai_komparatif_unit": 0.850207356058979,
			"summary_team": 0.7176470588235293
		}]
	},
	"880017": {
		"nik": "880017",
		"nama": "ANGGITA AYU KARTIKA",
		"band": "IV",
		"posisi": "MGR BIDDING MGT & SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8611111111111113,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0519924098671727,
			"nilai_komparatif_unit": 1.0593646601513298,
			"summary_team": 0.9058823529411766
		}]
	},
	"880022": {
		"nik": "880022",
		"nama": "FADHILLAH UTAMI",
		"band": "IV",
		"posisi": "OFF 1 BS SALES EVAL & MARKETING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8611111111111113,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.970018975332068,
			"nilai_komparatif_unit": 0.976816764555122,
			"summary_team": 0.8352941176470587
		}]
	},
	"880029": {
		"nik": "880029",
		"nama": "LISA RAHMAYANTI",
		"band": "IV",
		"posisi": "OFF 1 HC DATA GOVERNANCE & TECHNOLOGY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8969696969696972,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0361685214626388,
			"nilai_komparatif_unit": 1.0434298796294263,
			"summary_team": 0.9294117647058824
		}]
	},
	"880034": {
		"nik": "880034",
		"nama": "RACHMI BASUKI PUTRI",
		"band": "IV",
		"posisi": "OFF 1 HUBUNGAN ANTAR INSTANSI/PEMDA",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0101010101010102,
			"nilai_komparatif_unit": 1.017179689936433,
			"summary_team": 0.9090909090909092
		}]
	},
	"880050": {
		"nik": "880050",
		"nama": "YUANNITA SARI",
		"band": "IV",
		"posisi": "OFF 1 RETIREMENT READINESS MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9041666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1059907834101383,
			"nilai_komparatif_unit": 1.1137414485202601,
			"summary_team": 1
		}]
	},
	"880051": {
		"nik": "880051",
		"nama": "HARLINA",
		"band": "IV",
		"posisi": "MGR REGIONAL ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9111111111111112,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0459110473457673,
			"nilai_komparatif_unit": 1.0532406800918548,
			"summary_team": 0.9529411764705882
		}]
	},
	"880066": {
		"nik": "880066",
		"nama": "BRAMIANHA ADIWAZSHA",
		"band": "IV",
		"posisi": "OFF 1 SYSTEM INTEGRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8753623188405791,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9542267238021042,
			"nilai_komparatif_unit": 0.9609138426156216,
			"summary_team": 0.8352941176470587
		}]
	},
	"880077": {
		"nik": "880077",
		"nama": "TRENDY SUGIANTO",
		"band": "IV",
		"posisi": "OFF 1 DISCIPLINARY ACTION ADM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9041666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0149091894822446,
			"nilai_komparatif_unit": 1.0220215645244741,
			"summary_team": 0.9176470588235295
		}]
	},
	"880082": {
		"nik": "880082",
		"nama": "SYAWALUDIN RACHMATULLAH",
		"band": "IV",
		"posisi": "ENG 1 INFRA SERVICES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8944444444444447,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9864815491413954,
			"nilai_komparatif_unit": 0.9933947063208199,
			"summary_team": 0.8823529411764706
		}]
	},
	"880085": {
		"nik": "880085",
		"nama": "DINI NUZULIA RAHMAH",
		"band": "IV",
		"posisi": "OFF 1 MARKET PLAN & ANALYSIS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333332,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9935064935064936,
			"nilai_komparatif_unit": 1.00046888074462,
			"summary_team": 0.9272727272727272
		}]
	},
	"880087": {
		"nik": "880087",
		"nama": "IRA ZURAIDAH TAMARA",
		"band": "IV",
		"posisi": "OFF 1 PROGRAM & PRODUCT MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.877777777777778,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.005212211466865,
			"nilai_komparatif_unit": 1.0122566311243797,
			"summary_team": 0.8823529411764706
		}]
	},
	"880091": {
		"nik": "880091",
		"nama": "FIRDHA NUR AISYA",
		"band": "IV",
		"posisi": "OFF 1 OPERATIONAL ACCOUNTING POLICY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8904761904761893,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9908776344762517,
			"nilai_komparatif_unit": 0.9978215989514847,
			"summary_team": 0.8823529411764706
		}]
	},
	"880098": {
		"nik": "880098",
		"nama": "SARY SARTIKA MANGIWA",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.858333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0614886731391593,
			"nilai_komparatif_unit": 1.0689274722205784,
			"summary_team": 0.9111111111111113
		}]
	},
	"885408": {
		"nik": "885408",
		"nama": "MOCHAMAD RACHMAT ARIYANTHO",
		"band": "IV",
		"posisi": "OFF 1 CFU MANAGEMENT REPORTING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.911111111111111,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0070078930370687,
			"summary_team": 0.9111111111111112
		}]
	},
	"888051": {
		"nik": "888051",
		"nama": "DANANG AJI IRAWAN",
		"band": "IV",
		"posisi": "AUDITOR PRATAMA I",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9164334266293431,
			"nilai_komparatif_unit": 0.9228556940587558,
			"summary_team": 0.8
		}]
	},
	"890017": {
		"nik": "890017",
		"nama": "SERODYA KARTIKAWATI",
		"band": "IV",
		"posisi": "AUDITOR PRATAMA I",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0830576860164964,
			"nilai_komparatif_unit": 1.090647638433075,
			"summary_team": 0.9454545454545454
		}]
	},
	"890021": {
		"nik": "890021",
		"nama": "NI KOMANG AYU KARTIKA SARI",
		"band": "IV",
		"posisi": "AUDITOR PRATAMA I",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9491631918661051,
			"nilai_komparatif_unit": 0.9558148259894254,
			"summary_team": 0.8285714285714284
		}]
	},
	"890022": {
		"nik": "890022",
		"nama": "DEDY SURYAWAN",
		"band": "IV",
		"posisi": "OFF 1 SYSTEM INTEGRATION SAP-ERP",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9988901220865701,
			"nilai_komparatif_unit": 1.0058902372179372,
			"summary_team": 0.8823529411764706
		}]
	},
	"890023": {
		"nik": "890023",
		"nama": "SUKMA PERMATA ADI",
		"band": "IV",
		"posisi": "OFF 1 LEARNING DEVT NETWORK&ACCESS MAT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9444444444444444,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0323529411764707,
			"nilai_komparatif_unit": 1.0395875601647384,
			"summary_team": 0.975
		}]
	},
	"890032": {
		"nik": "890032",
		"nama": "KARINA MAHARI",
		"band": "IV",
		"posisi": "ENG 1 CONS BROADBAND SERVICES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0316742081447965,
			"nilai_komparatif_unit": 1.0389040706445776,
			"summary_team": 0.8941176470588237
		}]
	},
	"890033": {
		"nik": "890033",
		"nama": "FAHMI RAHMADANI",
		"band": "IV",
		"posisi": "INNOVATOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8471014492753652,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9612611511670506,
			"nilai_komparatif_unit": 0.9679975664951186,
			"summary_team": 0.8142857142857145
		}]
	},
	"890043": {
		"nik": "890043",
		"nama": "ENDANG YUNIARTI",
		"band": "IV",
		"posisi": "OFF 1 UNIT PROGRAM & PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8756756756756745,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9966329966329979,
			"nilai_komparatif_unit": 1.003617294070615,
			"summary_team": 0.8727272727272727
		}]
	},
	"890057": {
		"nik": "890057",
		"nama": "DESI ARIANI BR SINULINGGA",
		"band": "IV",
		"posisi": "OFF 1 TELCO REGULATION INITIATIVE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8622222222222212,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0369921164342037,
			"nilai_komparatif_unit": 1.044259246266458,
			"summary_team": 0.8941176470588235
		}]
	},
	"890064": {
		"nik": "890064",
		"nama": "EKO SULISTYA RAMDANI",
		"band": "IV",
		"posisi": "ASMAN BS SALES ENGINEER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8461538461538458,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1262032085561502,
			"nilai_komparatif_unit": 1.1340955201797152,
			"summary_team": 0.9529411764705882
		}]
	},
	"890065": {
		"nik": "890065",
		"nama": "MUHAMMAD RIZKI",
		"band": "IV",
		"posisi": "OFF 1 PREPARATION RECRUIT & SELECT SUPP",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8727272727272729,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9705882352941175,
			"nilai_komparatif_unit": 0.9773900138300958,
			"summary_team": 0.8470588235294119
		}]
	},
	"890070": {
		"nik": "890070",
		"nama": "SULHAN SAURI NASUTION",
		"band": "IV",
		"posisi": "ASMAN GS SALES ENGINEER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8984126984126981,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0017667844522973,
			"nilai_komparatif_unit": 1.008787058925827,
			"summary_team": 0.9
		}]
	},
	"890075": {
		"nik": "890075",
		"nama": "GELAR BAGJA ANUGRAH",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9132352941176469,
			"nilai_komparatif_unit": 0.9196351493764992,
			"summary_team": 0.8117647058823529
		}]
	},
	"890076": {
		"nik": "890076",
		"nama": "THOMAS AGUNG NUGROHO",
		"band": "IV",
		"posisi": "OFF 1 BOD FACILITY POLICY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8206349206349192,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0608715439754257,
			"nilai_komparatif_unit": 1.0683060182816753,
			"summary_team": 0.8705882352941177
		}]
	},
	"890086": {
		"nik": "890086",
		"nama": "YONNY SEPTIAN IZMANTOKO",
		"band": "IV",
		"posisi": "OFF 1 CED DESIGN & PLANNING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0806451612903223,
			"nilai_komparatif_unit": 1.0882182069916706,
			"summary_team": 0.8933333333333332
		}]
	},
	"890092": {
		"nik": "890092",
		"nama": "WIBISANA JAKA SEMBADA",
		"band": "IV",
		"posisi": "OFF 1 LEARNING DEVT DIG SERVICES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8757575757575752,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.038062283737025,
			"nilai_komparatif_unit": 1.0453369131872692,
			"summary_team": 0.9090909090909092
		}]
	},
	"890098": {
		"nik": "890098",
		"nama": "KARINA MATOVANI",
		"band": "IV",
		"posisi": "OFF 1 EXECUTIVE DATA & REPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8368421052631614,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0122086570477205,
			"nilai_komparatif_unit": 1.0193021070475057,
			"summary_team": 0.8470588235294116
		}]
	},
	"900008": {
		"nik": "900008",
		"nama": "DANI HERMAWAN HANURANTO",
		"band": "IV",
		"posisi": "OFF 1 QUALITY & PROCESS MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9900687547746366,
			"nilai_komparatif_unit": 0.9970070507074409,
			"summary_team": 0.8470588235294116
		}]
	},
	"900012": {
		"nik": "900012",
		"nama": "STEFANUS EZRA SOEHARTONO",
		"band": "IV",
		"posisi": "ENG 1 MOBILE & WIRELESS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9882352941176472,
			"nilai_komparatif_unit": 0.9951607413542797,
			"summary_team": 0.9058823529411766
		}]
	},
	"900018": {
		"nik": "900018",
		"nama": "PRATAMI ANGGINA",
		"band": "IV",
		"posisi": "ASMAN ES TERITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8270270270270257,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.121212121212123,
			"nilai_komparatif_unit": 1.1290694558294423,
			"summary_team": 0.9272727272727272
		}]
	},
	"900023": {
		"nik": "900023",
		"nama": "ILMAN ARDYATMA BUJANG",
		"band": "IV",
		"posisi": "OFF 1 RA FRAMEWORK & PERFORMANCE MGMT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8222222222222222,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.972972972972973,
			"nilai_komparatif_unit": 0.9797914634955261,
			"summary_team": 0.8
		}]
	},
	"900027": {
		"nik": "900027",
		"nama": "ITA TRIANA M.M  M.SM",
		"band": "IV",
		"posisi": "OFF 1 TALENT MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8222222222222221,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8728139904610495,
			"nilai_komparatif_unit": 0.8789305775474574,
			"summary_team": 0.7176470588235294
		}]
	},
	"900038": {
		"nik": "900038",
		"nama": "TYA ANANDYA NUR INDAH SARI",
		"band": "IV",
		"posisi": "OFF 1 BUDGET CAPEX",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0169491525423728,
			"nilai_komparatif_unit": 1.0240758234275273,
			"summary_team": 1
		}]
	},
	"900043": {
		"nik": "900043",
		"nama": "PUTRI IRMAWATI YUNITASARI",
		"band": "IV",
		"posisi": "OFF 1 GROUP ADMINISTRATION & COMPLIANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9075268817204291,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9722609422916096,
			"nilai_komparatif_unit": 0.9790744429793087,
			"summary_team": 0.8823529411764706
		}]
	},
	"900047": {
		"nik": "900047",
		"nama": "WINDA SUSLIANI",
		"band": "IV",
		"posisi": "OFF 1 FINANCIAL & ASSET MNG AUDIT REPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0303030303030303,
			"nilai_komparatif_unit": 1.0375232837351616,
			"summary_team": 0.9272727272727272
		}]
	},
	"900053": {
		"nik": "900053",
		"nama": "IMA LOONY PARRAZKY",
		"band": "IV",
		"posisi": "OFF 1 DISCLOSURE REPORTING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.0339332377706798,
			"summary_team": 0.8727272727272727
		}]
	},
	"900058": {
		"nik": "900058",
		"nama": "IBRAHIM ZEIN ABDILLAH",
		"band": "IV",
		"posisi": "OFF 1 SDN NFV & NETWORK DEVELOPMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444446,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9891640866873063,
			"nilai_komparatif_unit": 0.9960960428029205,
			"summary_team": 0.8352941176470587
		}]
	},
	"900060": {
		"nik": "900060",
		"nama": "GST. NGURAH ANDIKA PRAMUDYA",
		"band": "IV",
		"posisi": "INNOVATOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8604938271604932,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9961057593769223,
			"nilai_komparatif_unit": 1.0030863619922437,
			"summary_team": 0.8571428571428571
		}]
	},
	"900062": {
		"nik": "900062",
		"nama": "NAZARUDIN",
		"band": "IV",
		"posisi": "OFF 1 FACILITIES ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888893,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0720588235294113,
			"nilai_komparatif_unit": 1.079571697094151,
			"summary_team": 0.9529411764705882
		}]
	},
	"900082": {
		"nik": "900082",
		"nama": "ARLET YUSYA PUTRINDANI",
		"band": "IV",
		"posisi": "ASMAN GS BIDDING MGT & OBL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8984126984126981,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0103288937211201,
			"nilai_komparatif_unit": 1.0174091705405774,
			"summary_team": 0.9076923076923077
		}]
	},
	"900087": {
		"nik": "900087",
		"nama": "RIFKI SINTAMI",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK & LEGAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8588235294117647,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0146771037181994,
			"nilai_komparatif_unit": 1.0217878523282191,
			"summary_team": 0.8714285714285712
		}]
	},
	"900090": {
		"nik": "900090",
		"nama": "SANDRO PIBA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8461538461538458,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9454545454545459,
			"nilai_komparatif_unit": 0.9520801897805016,
			"summary_team": 0.8
		}]
	},
	"900092": {
		"nik": "900092",
		"nama": "VEGGA FIRSTHYA SUYANA",
		"band": "IV",
		"posisi": "OFF 1 EMPLOYEE ACTUALIZATION ADM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0546874999999996,
			"nilai_komparatif_unit": 1.0620786371875328,
			"summary_team": 0.8999999999999999
		}]
	},
	"906370": {
		"nik": "906370",
		"nama": "FADHILA RACHMAWATI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8270270270270257,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Down Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.6595365418894841,
			"nilai_komparatif_unit": 0.6641585034290837,
			"summary_team": 0.5454545454545454
		}]
	},
	"910003": {
		"nik": "910003",
		"nama": "HERFINA PUSPAWARDANI",
		"band": "IV",
		"posisi": "OFF 1 WORKING ENV & CRISIS MONITORING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8944444444444447,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9864815491413954,
			"nilai_komparatif_unit": 0.9933947063208199,
			"summary_team": 0.8823529411764706
		}]
	},
	"910007": {
		"nik": "910007",
		"nama": "GERY AGUSTINUS KUMANAEN",
		"band": "IV",
		"posisi": "OFF 1 ASSET COMPLIANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9177777777777774,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254949437402083,
			"nilai_komparatif_unit": 1.0326815026159943,
			"summary_team": 0.9411764705882353
		}]
	},
	"910011": {
		"nik": "910011",
		"nama": "RIESYA RACHMASARIE",
		"band": "IV",
		"posisi": "ASMAN DEBT MGT & COLLECTION PARTNERSHIP",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8250000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9137529137529136,
			"nilai_komparatif_unit": 0.9201563964348037,
			"summary_team": 0.7538461538461538
		}]
	},
	"910012": {
		"nik": "910012",
		"nama": "MONICA DEWI SOEJANTONO",
		"band": "IV",
		"posisi": "ASMAN BES TERITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0140499062750898,
			"summary_team": 0.8727272727272727
		}]
	},
	"910014": {
		"nik": "910014",
		"nama": "KANIGIA INDERASTY",
		"band": "IV",
		"posisi": "OFF 1 INTL INTERCONNECTION BILLING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8111111111111113,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9863013698630132,
			"nilai_komparatif_unit": 0.9932132643653273,
			"summary_team": 0.7999999999999998
		}]
	},
	"910015": {
		"nik": "910015",
		"nama": "UCU PRATIWI",
		"band": "IV",
		"posisi": "OFF 1 CASH BANK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9934640522875815,
			"nilai_komparatif_unit": 1.0004261421021856,
			"summary_team": 0.8941176470588235
		}]
	},
	"910018": {
		"nik": "910018",
		"nama": "DYAH AYU PURNAMASARI",
		"band": "IV",
		"posisi": "OFF 1 ACCOUNTING OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9773755656108591,
			"nilai_komparatif_unit": 0.9842249090317043,
			"summary_team": 0.8470588235294116
		}]
	},
	"910021": {
		"nik": "910021",
		"nama": "AHMAD ABRORI",
		"band": "IV",
		"posisi": "OFF 1 PERSONNEL COST PAYMENT &TAX REPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8755555555555556,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0768672951414067,
			"nilai_komparatif_unit": 1.0844138659608749,
			"summary_team": 0.9428571428571428
		}]
	},
	"910023": {
		"nik": "910023",
		"nama": "MOCHAMAD GANI AMRI",
		"band": "IV",
		"posisi": "INNOVATOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8604938271604932,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.929698708751794,
			"nilai_komparatif_unit": 0.9362139378594272,
			"summary_team": 0.7999999999999998
		}]
	},
	"910026": {
		"nik": "910026",
		"nama": "RADEN ANNISA MAYA GANTIA",
		"band": "IV",
		"posisi": "OFF 1 PPH OPERATION CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9066666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0084033613445378,
			"nilai_komparatif_unit": 1.0154701442390606,
			"summary_team": 0.9142857142857141
		}]
	},
	"910027": {
		"nik": "910027",
		"nama": "YULISA VIDA WAHYUNI PERTIWI",
		"band": "IV",
		"posisi": "OFF 1 CAREER & TALENT DEVELOPMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8277777777777781,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9664429530201337,
			"nilai_komparatif_unit": 0.9732156818613275,
			"summary_team": 0.7999999999999998
		}]
	},
	"910029": {
		"nik": "910029",
		"nama": "RIZKY WILDAN PRATAMA",
		"band": "IV",
		"posisi": "ASMAN ES PROJECT MGT & QUALITY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8270270270270257,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0552584670231746,
			"nilai_komparatif_unit": 1.0626536054865339,
			"summary_team": 0.8727272727272727
		}]
	},
	"910032": {
		"nik": "910032",
		"nama": "CLARACIA DINASTY",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8345679012345671,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0099323753169913,
			"nilai_komparatif_unit": 1.0170098733778854,
			"summary_team": 0.8428571428571426
		}]
	},
	"910074": {
		"nik": "910074",
		"nama": "FAHMI RAMADHAN",
		"band": "IV",
		"posisi": "OFF 1 SYSTEM INTEGRATION BILLING & PAYM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888888,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9794117647058824,
			"nilai_komparatif_unit": 0.9862753775921878,
			"summary_team": 0.8705882352941177
		}]
	},
	"910080": {
		"nik": "910080",
		"nama": "DYANG INTAN MEIDIATY",
		"band": "IV",
		"posisi": "INNOVATOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8604938271604932,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9961057593769221,
			"nilai_komparatif_unit": 1.0030863619922434,
			"summary_team": 0.857142857142857
		}]
	},
	"910085": {
		"nik": "910085",
		"nama": "AMIEN KARIM",
		"band": "IV",
		"posisi": "OFF 1 EXECUTIVE REPORT & MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8133333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9836065573770487,
			"nilai_komparatif_unit": 0.9904995669217063,
			"summary_team": 0.7999999999999998
		}]
	},
	"910087": {
		"nik": "910087",
		"nama": "BAYU SANTIKO ADI",
		"band": "IV",
		"posisi": "OFF 1 WORKFORCE MANAGEMENT DEVELOPMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9552238805970148,
			"nilai_komparatif_unit": 0.9619179873786922,
			"summary_team": 0.8533333333333333
		}]
	},
	"910092": {
		"nik": "910092",
		"nama": "NURINA WINDA AHTAMI",
		"band": "IV",
		"posisi": "OFF 1 CHANGE MANAGEMENT SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8277777777777777,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0232925384919067,
			"nilai_komparatif_unit": 1.0304636631472883,
			"summary_team": 0.8470588235294116
		}]
	},
	"910094": {
		"nik": "910094",
		"nama": "ARIF JAUHARI",
		"band": "IV",
		"posisi": "OFF 1 EXECUTIVE DATA & REPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8368421052631614,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0122086570477205,
			"nilai_komparatif_unit": 1.0193021070475057,
			"summary_team": 0.8470588235294116
		}]
	},
	"910096": {
		"nik": "910096",
		"nama": "AMANDA YUDIANI",
		"band": "IV",
		"posisi": "ASMAN ES SALES ENGINEER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8270270270270257,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9893048128342261,
			"nilai_komparatif_unit": 0.9962377551436254,
			"summary_team": 0.8181818181818181
		}]
	},
	"910099": {
		"nik": "910099",
		"nama": "RADEN ASRI MELINDA HAKIM",
		"band": "IV",
		"posisi": "ASMAN CRM & LEVERAGING CUSTOMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7733333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9605911330049257,
			"nilai_komparatif_unit": 0.9673228529173807,
			"summary_team": 0.7428571428571427
		}]
	},
	"910107": {
		"nik": "910107",
		"nama": "BUDI AGUS SETIAWAN",
		"band": "IV",
		"posisi": "INNOVATOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8471014492753652,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9443969204448216,
			"nilai_komparatif_unit": 0.9510151530478358,
			"summary_team": 0.8000000000000002
		}]
	},
	"910140": {
		"nik": "910140",
		"nama": "DWI RAMADHANI",
		"band": "IV",
		"posisi": "ASMAN BGES SALES ENGINEER & BIDDING MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7939393939393942,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0669061517736862,
			"nilai_komparatif_unit": 1.0743829159659066,
			"summary_team": 0.8470588235294116
		}]
	},
	"910153": {
		"nik": "910153",
		"nama": "MUHAMMAD LUTHFI DARMAN",
		"band": "IV",
		"posisi": "OFF 1 SECRETARY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0879120879120874,
			"nilai_komparatif_unit": 1.095536059457909,
			"summary_team": 0.9428571428571426
		}]
	},
	"910160": {
		"nik": "910160",
		"nama": "RIZAL RUSTIO",
		"band": "IV",
		"posisi": "ASMAN BGES SALES ENGINEER & BIDDING MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8133333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9836065573770488,
			"nilai_komparatif_unit": 0.9904995669217064,
			"summary_team": 0.8
		}]
	},
	"910166": {
		"nik": "910166",
		"nama": "LINTANG KHAIRUNISA",
		"band": "IV",
		"posisi": "OFF 1 ACCOUNTING ASSESSMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9199999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9627329192546582,
			"nilai_komparatif_unit": 0.9694796485760596,
			"summary_team": 0.8857142857142855
		}]
	},
	"910168": {
		"nik": "910168",
		"nama": "AGUNG MEINASTESI CAESAR",
		"band": "IV",
		"posisi": "JM CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8476190476190508,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.008171603677218,
			"nilai_komparatif_unit": 1.0152367624387977,
			"summary_team": 0.8545454545454546
		}]
	},
	"910174": {
		"nik": "910174",
		"nama": "ISTI NURUL SHOFYAH",
		"band": "IV",
		"posisi": "OFF 1 INFRA OPERATION PERFORMANCE MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.926666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9712230215827334,
			"nilai_komparatif_unit": 0.9780292486331237,
			"summary_team": 0.8999999999999999
		}]
	},
	"910178": {
		"nik": "910178",
		"nama": "RIMA PRATITA RACHMAWATI",
		"band": "IV",
		"posisi": "OFF 1 EMPLOYEE ACTUALIZATION ADM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8212121212121206,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0171478185370095,
			"nilai_komparatif_unit": 1.0242758816522044,
			"summary_team": 0.8352941176470587
		}]
	},
	"910185": {
		"nik": "910185",
		"nama": "ALDILA NOOR MEDINA UTANG",
		"band": "IV",
		"posisi": "ASMAN BS TERITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8461538461538458,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9669421487603309,
			"nilai_komparatif_unit": 0.9737183759118765,
			"summary_team": 0.8181818181818181
		}]
	},
	"910188": {
		"nik": "910188",
		"nama": "WIEDYA PERMATA PUTRI HAMID",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8282051282051274,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0511746494263348,
			"nilai_komparatif_unit": 1.0585411689327926,
			"summary_team": 0.8705882352941174
		}]
	},
	"910191": {
		"nik": "910191",
		"nama": "VICKY HARDIAN KUSUMA CANDRA",
		"band": "IV",
		"posisi": "OFF 1 EMPLOYEE DATA MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0521642619311873,
			"nilai_komparatif_unit": 1.0595377165362272,
			"summary_team": 0.9294117647058824
		}]
	},
	"910201": {
		"nik": "910201",
		"nama": "GHANA GANDARA",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CICURUG",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8159999999999993,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0329131652661072,
			"nilai_komparatif_unit": 1.040151710244872,
			"summary_team": 0.8428571428571427
		}]
	},
	"910210": {
		"nik": "910210",
		"nama": "DIAN PUTRI PUSPITASARI RACHMAN",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8345679012345671,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9756973795435341,
			"nilai_komparatif_unit": 0.9825349624159232,
			"summary_team": 0.8142857142857142
		}]
	},
	"920050": {
		"nik": "920050",
		"nama": "MARINA ELVIRIA",
		"band": "IV",
		"posisi": "OFF 1 PULL SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828873,
			"nilai_komparatif_unit": 1.0123929619837904,
			"summary_team": 0.8545454545454546
		}]
	},
	"920054": {
		"nik": "920054",
		"nama": "DEVI ZARMA RISNA",
		"band": "IV",
		"posisi": "OFF 1 TPC ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.926666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0328879753340183,
			"nilai_komparatif_unit": 1.0401263437844335,
			"summary_team": 0.9571428571428573
		}]
	},
	"920058": {
		"nik": "920058",
		"nama": "METHA MARTINA HERDIANI",
		"band": "IV",
		"posisi": "MGR ACCESS OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8628205128205122,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8639740645684187,
			"nilai_komparatif_unit": 0.8700287023997155,
			"summary_team": 0.7454545454545454
		}]
	},
	"920073": {
		"nik": "920073",
		"nama": "PRATIWI WIDHI MAYA SARI",
		"band": "IV",
		"posisi": "OFF 1 QUALITY MANAGEMENT SYSTEM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8999999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969697,
			"nilai_komparatif_unit": 0.9764925023389756,
			"summary_team": 0.8727272727272727
		}]
	},
	"920074": {
		"nik": "920074",
		"nama": "MUHAMMAD FAUZAN ABDURRAHIM",
		"band": "IV",
		"posisi": "ASMAN INTEGRATION SYSTEM-SITE ADVALJAR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0557692307692306,
			"nilai_komparatif_unit": 1.0631679486102894,
			"summary_team": 0.9384615384615385
		}]
	},
	"920083": {
		"nik": "920083",
		"nama": "MELATI BUDIANA PUTRI",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.863888888888888,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0186495176848887,
			"nilai_komparatif_unit": 1.0257881045470858,
			"summary_team": 0.8800000000000001
		}]
	},
	"920085": {
		"nik": "920085",
		"nama": "CLARIVTIKA AYU MAHARDIPTA",
		"band": "IV",
		"posisi": "OFF 1 FINANCIAL PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9075268817204291,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0370783384443836,
			"nilai_komparatif_unit": 1.0443460725112625,
			"summary_team": 0.9411764705882353
		}]
	},
	"920087": {
		"nik": "920087",
		"nama": "JOHAN GIFARI",
		"band": "IV",
		"posisi": "ASMAN PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8282051282051274,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9508513931888554,
			"nilai_komparatif_unit": 0.9575148580464704,
			"summary_team": 0.7875
		}]
	},
	"920093": {
		"nik": "920093",
		"nama": "AULIYA ANNISA",
		"band": "IV",
		"posisi": "ASMAN WOC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090908,
			"nilai_komparatif_unit": 1.0985540651313475,
			"summary_team": 0.9272727272727272
		}]
	},
	"920108": {
		"nik": "920108",
		"nama": "DANI HAMDANI",
		"band": "IV",
		"posisi": "ASMAN CRM & LEVERAGING CUSTOMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.858333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.101500441306267,
			"nilai_komparatif_unit": 1.109219638579225,
			"summary_team": 0.9454545454545454
		}]
	},
	"920114": {
		"nik": "920114",
		"nama": "R. UTOMO AJIPRIYANTO",
		"band": "IV",
		"posisi": "OFF 1 PERSONNEL COST PLANNING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9890109890109887,
			"nilai_komparatif_unit": 0.9959418722344631,
			"summary_team": 0.8571428571428571
		}]
	},
	"920125": {
		"nik": "920125",
		"nama": "ANISA FITRI SAFARINI",
		"band": "IV",
		"posisi": "ASMAN HOME SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8277777777777782,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0517173312277928,
			"nilai_komparatif_unit": 1.0590876537902683,
			"summary_team": 0.8705882352941177
		}]
	},
	"920128": {
		"nik": "920128",
		"nama": "MUHAMMAD RIZAL NURJAMAN",
		"band": "IV",
		"posisi": "OFF 1 CONTRACT DRAFTING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.856249999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0844629822732024,
			"nilai_komparatif_unit": 1.0920627828556333,
			"summary_team": 0.9285714285714284
		}]
	},
	"920131": {
		"nik": "920131",
		"nama": "ROSA LESTARI FARDANI",
		"band": "IV",
		"posisi": "ASMAN GS TERITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.833333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9771428571428573,
			"nilai_komparatif_unit": 0.98399056976765,
			"summary_team": 0.8142857142857142
		}]
	},
	"920164": {
		"nik": "920164",
		"nama": "BAGUS DWI SANDI PUTRA",
		"band": "IV",
		"posisi": "OFF 1 QUALITY, CHANGE & RISK MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969694,
			"nilai_komparatif_unit": 0.9764925023389752,
			"summary_team": 0.8727272727272727
		}]
	},
	"920191": {
		"nik": "920191",
		"nama": "RENA PUSPITA PUTRI",
		"band": "IV",
		"posisi": "OFF 1 OBL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9166666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0010695187165772,
			"nilai_komparatif_unit": 1.0080849068264126,
			"summary_team": 0.9176470588235293
		}]
	},
	"920196": {
		"nik": "920196",
		"nama": "NADIYYA HARUM KAMILAH NAKULA",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION CONSUMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8250000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.0070078930370683,
			"summary_team": 0.825
		}]
	},
	"920224": {
		"nik": "920224",
		"nama": "HAIDLIR ACHMAD NAQVI",
		"band": "IV",
		"posisi": "PENDIDIKAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8604938271604932,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.929698708751794,
			"nilai_komparatif_unit": 0.9362139378594272,
			"summary_team": 0.7999999999999998
		}]
	},
	"930004": {
		"nik": "930004",
		"nama": "TIARA YUNITA NURAMALINA",
		"band": "IV",
		"posisi": "OFF 1 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969694,
			"nilai_komparatif_unit": 0.9764925023389752,
			"summary_team": 0.8727272727272727
		}]
	},
	"930007": {
		"nik": "930007",
		"nama": "MUHAMMAD LUTHFI SYAUQI",
		"band": "IV",
		"posisi": "OFF 1 OFFICE ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8355555555555556,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.025835866261398,
			"nilai_komparatif_unit": 1.0330248142857463,
			"summary_team": 0.857142857142857
		}]
	},
	"930015": {
		"nik": "930015",
		"nama": "ALIFA APRILIANTY",
		"band": "IV",
		"posisi": "OFF 1 CASH EVALUATION & MONITORING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9677419354838707,
			"nilai_komparatif_unit": 0.9745237674552273,
			"summary_team": 0.7999999999999998
		}]
	},
	"930019": {
		"nik": "930019",
		"nama": "NURI FAJARIYANAH",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8270270270270257,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0332739156268587,
			"nilai_komparatif_unit": 1.0405149887055647,
			"summary_team": 0.8545454545454546
		}]
	},
	"930098": {
		"nik": "930098",
		"nama": "ULFA NOVITA",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION CONSUMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9520661157024793,
			"nilai_komparatif_unit": 0.9587380932055396,
			"summary_team": 0.8727272727272727
		}]
	},
	"930100": {
		"nik": "930100",
		"nama": "SHERA AULIA",
		"band": "IV",
		"posisi": "OFF 1 BROADBAND&RES REGULATN INITIATIVE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0519031141868507,
			"nilai_komparatif_unit": 1.0592747386964314,
			"summary_team": 0.8941176470588235
		}]
	},
	"930113": {
		"nik": "930113",
		"nama": "DONI IMAM BAHTIAR",
		"band": "IV",
		"posisi": "INNOVATOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8604938271604932,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0459110473457682,
			"nilai_komparatif_unit": 1.0532406800918557,
			"summary_team": 0.8999999999999999
		}]
	},
	"930133": {
		"nik": "930133",
		"nama": "FAIDIL HADI",
		"band": "IV",
		"posisi": "MANAGER GAMES DISTRIBUTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9531249999999998,
			"nilai_komparatif_unit": 0.9598043980509557,
			"summary_team": 0.8133333333333332
		}]
	},
	"930137": {
		"nik": "930137",
		"nama": "RADEN GELOMBANG ALAM NUSANTARA PUTRA HER",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8592592592592595,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0156739811912223,
			"nilai_komparatif_unit": 1.022791715811944,
			"summary_team": 0.8727272727272727
		}]
	},
	"930178": {
		"nik": "930178",
		"nama": "TOMMY DARMADI",
		"band": "IV",
		"posisi": "OFF 1 GOVERNMENT COMM. MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8722222222222226,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9576620457100032,
			"nilai_komparatif_unit": 0.9643732388919992,
			"summary_team": 0.8352941176470587
		}]
	},
	"930187": {
		"nik": "930187",
		"nama": "MUHAMMAD NAUFAL FARID",
		"band": "IV",
		"posisi": "ASMAN OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8400000000000004,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0822510822510818,
			"nilai_komparatif_unit": 1.089835382074749,
			"summary_team": 0.9090909090909092
		}]
	},
	"930192": {
		"nik": "930192",
		"nama": "NOVANDIET EERSTA ANGGINANDA",
		"band": "IV",
		"posisi": "ASMAN OM TRANSPORT, RADIO & MUX",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0703259005145795,
			"nilai_komparatif_unit": 1.0778266299401897,
			"summary_team": 0.9454545454545454
		}]
	},
	"940001": {
		"nik": "940001",
		"nama": "FEMI YUMNA ANJANI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8984126984126981,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0653710247349824,
			"nilai_komparatif_unit": 1.0728370309211173,
			"summary_team": 0.9571428571428569
		}]
	},
	"940015": {
		"nik": "940015",
		"nama": "EDWINA",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0384615384615383,
			"nilai_komparatif_unit": 1.0457389658461864,
			"summary_team": 0.8999999999999999
		}]
	}
};