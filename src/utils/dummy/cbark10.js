export const cbark10 = {
	"650375": {
		"nik": "650375",
		"nama": "AKHMAD MUSLIMIN",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379084967320246,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.026365054602186,
			"nilai_komparatif_unit": 1.0318039033021837,
			"summary_team": 0.8600000000000001
		}]
	},
	"650393": {
		"nik": "650393",
		"nama": "BAMBANG PRISTIWANTORO",
		"band": "III",
		"posisi": "KAKANDATEL BANGKALAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8460784313725501,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9637222568856392,
			"nilai_komparatif_unit": 0.9688291528390023,
			"summary_team": 0.8153846153846154
		}]
	},
	"650512": {
		"nik": "650512",
		"nama": "SUPRATMAN HAMID",
		"band": "III",
		"posisi": "KAKANDATEL MARISA",
		"category": [{
			"code": "DL-3-01",
			"kriteria": 0.818333333333332,
			"kriteria_bp": 0.9938387235408043,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0339965533448239,
			"nilai_komparatif_unit": 1.0404067871907297,
			"summary_team": 0.8461538461538461
		}]
	},
	"650759": {
		"nik": "650759",
		"nama": "ABDUL RAHMAN",
		"band": "III",
		"posisi": "KAKANDATEL BANTAENG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9544751495153599,
			"nilai_komparatif_unit": 0.9595330437829446,
			"summary_team": 0.7846153846153846
		}]
	},
	"650790": {
		"nik": "650790",
		"nama": "I MADE KARIAWAN",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8460784313725501,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9637222568856392,
			"nilai_komparatif_unit": 0.9688291528390023,
			"summary_team": 0.8153846153846154
		}]
	},
	"651214": {
		"nik": "651214",
		"nama": "M.YAURI KASMAN",
		"band": "III",
		"posisi": "MGR PERF MGT, SLA MONITORING&KPI REPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8708333333333323,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9904306220095705,
			"nilai_komparatif_unit": 0.9956790492400187,
			"summary_team": 0.8625
		}]
	},
	"651271": {
		"nik": "651271",
		"nama": "M.RUKMAN, IR, MM",
		"band": "III",
		"posisi": "MGR IP & SERVICE NODE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8585858585858576,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0191176470588248,
			"nilai_komparatif_unit": 1.0245180907557303,
			"summary_team": 0.875
		}]
	},
	"660019": {
		"nik": "660019",
		"nama": "IVANNE PAULINA",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8321285140562269,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9934362934362909,
			"nilai_komparatif_unit": 0.9987006481304213,
			"summary_team": 0.8266666666666665
		}]
	},
	"660026": {
		"nik": "660026",
		"nama": "NUR HUDAYAH",
		"band": "III",
		"posisi": "MGR CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8708333333333323,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.939538929969553,
			"nilai_komparatif_unit": 0.9445176751683971,
			"summary_team": 0.8181818181818181
		}]
	},
	"660029": {
		"nik": "660029",
		"nama": "FATTAH YUSUF ROY",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8429378531073439,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9174262734584457,
			"nilai_komparatif_unit": 0.9222878406682501,
			"summary_team": 0.7733333333333333
		}]
	},
	"660031": {
		"nik": "660031",
		"nama": "ASNI HAMID",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8316666666666652,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0359179898258073,
			"nilai_komparatif_unit": 1.0414074608351764,
			"summary_team": 0.8615384615384616
		}]
	},
	"660033": {
		"nik": "660033",
		"nama": "YUSRIHANA NUR",
		"band": "III",
		"posisi": "MGR LOGISTIC & PROCUREMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8691358024691351,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181825,
			"nilai_komparatif_unit": 0.9870209705509746,
			"summary_team": 0.8533333333333333
		}]
	},
	"660049": {
		"nik": "660049",
		"nama": "EDOARDUS RAHAILJAAN",
		"band": "III",
		"posisi": "MGR FINANCE & PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8584615384615386,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9742587161942,
			"nilai_komparatif_unit": 0.9794214462854812,
			"summary_team": 0.8363636363636364
		}]
	},
	"660068": {
		"nik": "660068",
		"nama": "WIWIN SUNARYA",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8321285140562269,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0353430353430328,
			"nilai_komparatif_unit": 1.0408294595900174,
			"summary_team": 0.8615384615384615
		}]
	},
	"660079": {
		"nik": "660079",
		"nama": "JONI RIBA",
		"band": "III",
		"posisi": "KAKANDATEL MAMUJU",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8708333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9295967190704034,
			"nilai_komparatif_unit": 0.9345227791348683,
			"summary_team": 0.8095238095238095
		}]
	},
	"660081": {
		"nik": "660081",
		"nama": "SAMSUDIN RUMATA",
		"band": "III",
		"posisi": "MGR HUBUNGAN ANTAR INSTANSI/PEMDA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8405797101449269,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0166144200626968,
			"nilai_komparatif_unit": 1.022001598817628,
			"summary_team": 0.8545454545454546
		}]
	},
	"660085": {
		"nik": "660085",
		"nama": "JEFFRY ROBERT W.",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8584615384615386,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.009557945041816,
			"nilai_komparatif_unit": 1.014907730571187,
			"summary_team": 0.8666666666666667
		}]
	},
	"660099": {
		"nik": "660099",
		"nama": "ANTONIUS ISSUPRATANA,SE",
		"band": "III",
		"posisi": "MGR ASSET & FACILITY MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559437,
			"nilai_komparatif_unit": 0.9490586255297823,
			"summary_team": 0.8181818181818182
		}]
	},
	"660138": {
		"nik": "660138",
		"nama": "KARAMA BAJHER",
		"band": "III",
		"posisi": "KAKANDATEL MAUMERE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606965174129362,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0224277456647388,
			"nilai_komparatif_unit": 1.0278457300265558,
			"summary_team": 0.88
		}]
	},
	"660202": {
		"nik": "660202",
		"nama": "SETYOHADI , S.MB.",
		"band": "III",
		"posisi": "MGR ACCESS SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8358208955223883,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0688095238095234,
			"nilai_komparatif_unit": 1.0744732915528334,
			"summary_team": 0.8933333333333332
		}]
	},
	"660251": {
		"nik": "660251",
		"nama": "HARTATI MUCHLISI,IR,MBA",
		"band": "III",
		"posisi": "MGR SEKDIV & PUBLIC RELATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8691358024691351,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9413739669421494,
			"nilai_komparatif_unit": 0.94636243625271,
			"summary_team": 0.8181818181818181
		}]
	},
	"660304": {
		"nik": "660304",
		"nama": "WAHJOE WIJONO",
		"band": "III",
		"posisi": "MGR PERFORMANCE MGT & SLA MONITORING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8470588235294104,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9887152777777792,
			"nilai_komparatif_unit": 0.9939546151647032,
			"summary_team": 0.8374999999999999
		}]
	},
	"660391": {
		"nik": "660391",
		"nama": "DJONI TEGE",
		"band": "III",
		"posisi": "KAKANDATEL MINAHASA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8404761904761923,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9994334277620375,
			"nilai_komparatif_unit": 1.004729562090602,
			"summary_team": 0.84
		}]
	},
	"660392": {
		"nik": "660392",
		"nama": "HARYANI YULIWULANSIH",
		"band": "III",
		"posisi": "MGR DELIVERY&SALES SUPPORT BUSINESS SERV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8390804597701138,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0896281800391403,
			"nilai_komparatif_unit": 1.0954022686871445,
			"summary_team": 0.9142857142857141
		}]
	},
	"660409": {
		"nik": "660409",
		"nama": "ABDUL RAHMAN,IR",
		"band": "III",
		"posisi": "MGR PARTNERSHIP MARKETING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8166666666666655,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346953,
			"nilai_komparatif_unit": 0.9847828277606103,
			"summary_team": 0.8
		}]
	},
	"660424": {
		"nik": "660424",
		"nama": "ELLY SETYONO",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8404761904761923,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.079973850512092,
			"nilai_komparatif_unit": 1.085696779548453,
			"summary_team": 0.9076923076923077
		}]
	},
	"660478": {
		"nik": "660478",
		"nama": "MUSNIN ARSJAD",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8316666666666652,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0134554823933597,
			"nilai_komparatif_unit": 1.018825921505842,
			"summary_team": 0.8428571428571427
		}]
	},
	"660480": {
		"nik": "660480",
		"nama": "MUNIR B",
		"band": "III",
		"posisi": "SENIOR AM GOVERNMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8395833333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.039476652379879,
			"nilai_komparatif_unit": 1.0449849812284928,
			"summary_team": 0.8727272727272727
		}]
	},
	"660591": {
		"nik": "660591",
		"nama": "PURNOMO, SE",
		"band": "III",
		"posisi": "KAKANDATEL SANGGAU",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564625850340167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9340746624304969,
			"nilai_komparatif_unit": 0.9390244517288375,
			"summary_team": 0.7999999999999999
		}]
	},
	"670008": {
		"nik": "670008",
		"nama": "DEDI KUSNADI SOEDRADJAT",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8584615384615386,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9318996415770608,
			"nilai_komparatif_unit": 0.9368379051426341,
			"summary_team": 0.8
		}]
	},
	"670048": {
		"nik": "670048",
		"nama": "ALDON",
		"band": "III",
		"posisi": "MGR FINANCIAL SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8488888888888888,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9424083769633509,
			"nilai_komparatif_unit": 0.947402327753989,
			"summary_team": 0.8
		}]
	},
	"670086": {
		"nik": "670086",
		"nama": "I GEDE EKA SUDARSANA",
		"band": "III",
		"posisi": "MGR PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8321285140562269,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0353430353430328,
			"nilai_komparatif_unit": 1.0408294595900174,
			"summary_team": 0.8615384615384615
		}]
	},
	"670106": {
		"nik": "670106",
		"nama": "I WAYAN KARYANA",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8321285140562269,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9331137860549603,
			"nilai_komparatif_unit": 0.9380584835380619,
			"summary_team": 0.7764705882352941
		}]
	},
	"670144": {
		"nik": "670144",
		"nama": "IRHAM",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8316666666666652,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9779559118236492,
			"nilai_komparatif_unit": 0.983138233859875,
			"summary_team": 0.8133333333333335
		}]
	},
	"670183": {
		"nik": "670183",
		"nama": "BUDI MUHAENI, ST",
		"band": "III",
		"posisi": "MGR HR PLANNING & DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8871794871794874,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9537572254335258,
			"nilai_komparatif_unit": 0.9588113153232805,
			"summary_team": 0.8461538461538461
		}]
	},
	"670287": {
		"nik": "670287",
		"nama": "SITTI AISYAH SAID",
		"band": "III",
		"posisi": "CONSUMER EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8166666666666655,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0476190476190492,
			"nilai_komparatif_unit": 1.053170524132875,
			"summary_team": 0.8555555555555556
		}]
	},
	"670330": {
		"nik": "670330",
		"nama": "BHUDI KURNIAWAN",
		"band": "III",
		"posisi": "MGR WIFI FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8470588235294104,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.944444444444446,
			"nilai_komparatif_unit": 0.9494491846349404,
			"summary_team": 0.8
		}]
	},
	"670460": {
		"nik": "670460",
		"nama": "GELORA ATIKE BERENTIAN",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8404761904761923,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9518413597733691,
			"nilai_komparatif_unit": 0.956885297229145,
			"summary_team": 0.8
		}]
	},
	"670493": {
		"nik": "670493",
		"nama": "SAOR TUA JULIANTAJA",
		"band": "III",
		"posisi": "MGR SAS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.868292682926828,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.082584269662923,
			"nilai_komparatif_unit": 1.0883210316671363,
			"summary_team": 0.9400000000000002
		}]
	},
	"670558": {
		"nik": "670558",
		"nama": "ABUBAKAR BUGIS",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7860082304526756,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9160209424083761,
			"nilai_komparatif_unit": 0.9208750625768763,
			"summary_team": 0.72
		}]
	},
	"670559": {
		"nik": "670559",
		"nama": "NELSON SANDAG",
		"band": "III",
		"posisi": "MGR FINANCE & PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8429378531073439,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0597855227882047,
			"nilai_komparatif_unit": 1.0654014711167719,
			"summary_team": 0.8933333333333333
		}]
	},
	"670616": {
		"nik": "670616",
		"nama": "NUR RACHMAD HARIYANTO",
		"band": "III",
		"posisi": "MGR ACCESS DATA MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8515151515151502,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9575691212701903,
			"nilai_komparatif_unit": 0.962643410916964,
			"summary_team": 0.8153846153846154
		}]
	},
	"670623": {
		"nik": "670623",
		"nama": "AGUS HARSANTO, ST",
		"band": "III",
		"posisi": "MGR ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8495726495726486,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9416498993963794,
			"nilai_komparatif_unit": 0.9466398309107273,
			"summary_team": 0.8
		}]
	},
	"680003": {
		"nik": "680003",
		"nama": "BAMBANG WIDARTO",
		"band": "III",
		"posisi": "KAKANDATEL BAU-BAU",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8429378531073439,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1315735203134676,
			"nilai_komparatif_unit": 1.137569883052351,
			"summary_team": 0.9538461538461538
		}]
	},
	"680027": {
		"nik": "680027",
		"nama": "MUH. MAS'UDI",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8493827160493849,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.027484143763211,
			"nilai_komparatif_unit": 1.0329289226696212,
			"summary_team": 0.8727272727272727
		}]
	},
	"680038": {
		"nik": "680038",
		"nama": "SANTIAGO B.P.T CARDOSO",
		"band": "III",
		"posisi": "MGR FINANCE & PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606965174129362,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.072476656291684,
			"nilai_komparatif_unit": 1.0781598566712125,
			"summary_team": 0.9230769230769231
		}]
	},
	"680052": {
		"nik": "680052",
		"nama": "SUMARIYONO",
		"band": "III",
		"posisi": "MGR CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.941176470588236,
			"nilai_komparatif_unit": 0.9461638933386249,
			"summary_team": 0.7999999999999998
		}]
	},
	"680069": {
		"nik": "680069",
		"nama": "HIDAYAT HUSAIN",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8724637681159438,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1461794019933529,
			"nilai_komparatif_unit": 1.1522531632954773,
			"summary_team": 0.9999999999999999
		}]
	},
	"680084": {
		"nik": "680084",
		"nama": "ABDUL MUIS",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.973190348525465,
			"nilai_komparatif_unit": 0.9783474171904534,
			"summary_team": 0.8
		}]
	},
	"680087": {
		"nik": "680087",
		"nama": "YOHANIS KENDEK RANO, IR",
		"band": "III",
		"posisi": "MGR ACCESS OM & PARTNERSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8790960451977408,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.966902313624678,
			"nilai_komparatif_unit": 0.9720260611333269,
			"summary_team": 0.8500000000000001
		}]
	},
	"680126": {
		"nik": "680126",
		"nama": "ALFONS MURDIN",
		"band": "III",
		"posisi": "MGR IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8585858585858576,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440247678018588,
			"nilai_komparatif_unit": 0.949027284068466,
			"summary_team": 0.8105263157894738
		}]
	},
	"680128": {
		"nik": "680128",
		"nama": "LAURENTIUS SARE",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8725925925925916,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0123089983022084,
			"nilai_komparatif_unit": 1.017673362038799,
			"summary_team": 0.8833333333333335
		}]
	},
	"680142": {
		"nik": "680142",
		"nama": "DARMAWANGSA",
		"band": "III",
		"posisi": "MGR PROJECT MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8395833333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.039476652379879,
			"nilai_komparatif_unit": 1.0449849812284928,
			"summary_team": 0.8727272727272727
		}]
	},
	"680147": {
		"nik": "680147",
		"nama": "NURTINY, IR",
		"band": "III",
		"posisi": "MGR CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.075630252100841,
			"nilai_komparatif_unit": 1.0813301638155712,
			"summary_team": 0.914285714285714
		}]
	},
	"680153": {
		"nik": "680153",
		"nama": "JULIANTO",
		"band": "III",
		"posisi": "MGR ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8724637681159438,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8863787375415264,
			"nilai_komparatif_unit": 0.8910757796151693,
			"summary_team": 0.7733333333333333
		}]
	},
	"680154": {
		"nik": "680154",
		"nama": "HASBULLAH",
		"band": "III",
		"posisi": "MGR SECURITY AND SAFETY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8691358024691351,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0052332535885176,
			"nilai_komparatif_unit": 1.010560121986812,
			"summary_team": 0.8736842105263158
		}]
	},
	"680175": {
		"nik": "680175",
		"nama": "JUSUF SETIO BUDY",
		"band": "III",
		"posisi": "MGR HELPDESK OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8470588235294104,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9444444444444459,
			"nilai_komparatif_unit": 0.9494491846349403,
			"summary_team": 0.7999999999999999
		}]
	},
	"680343": {
		"nik": "680343",
		"nama": "SEYID ALWI",
		"band": "III",
		"posisi": "MGR CCAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8724637681159438,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9169435215946824,
			"nilai_komparatif_unit": 0.921802530636382,
			"summary_team": 0.7999999999999999
		}]
	},
	"680347": {
		"nik": "680347",
		"nama": "BAMBANG PUTRO",
		"band": "III",
		"posisi": "MGR CCAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8493827160493849,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9418604651162766,
			"nilai_komparatif_unit": 0.9468515124471528,
			"summary_team": 0.7999999999999999
		}]
	},
	"690005": {
		"nik": "690005",
		"nama": "MARTHEN LUTHER DIMARA",
		"band": "III",
		"posisi": "KAKANDATEL SENTANI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7860082304526756,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0627040344933776,
			"nilai_komparatif_unit": 1.0683354484143501,
			"summary_team": 0.835294117647059
		}]
	},
	"690110": {
		"nik": "690110",
		"nama": "DULROKIM",
		"band": "III",
		"posisi": "MGR COLLECTION PARTNERSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740731,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.060862865947613,
			"nilai_komparatif_unit": 1.0664845232648248,
			"summary_team": 0.9272727272727274
		}]
	},
	"690280": {
		"nik": "690280",
		"nama": "SYARIFUDDIN",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564625850340167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0274821286735465,
			"nilai_komparatif_unit": 1.0329268969017213,
			"summary_team": 0.8799999999999999
		}]
	},
	"690281": {
		"nik": "690281",
		"nama": "NUGRAHA BASUKI",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8358208955223883,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0369047619047616,
			"nilai_komparatif_unit": 1.0423994619542414,
			"summary_team": 0.8666666666666666
		}]
	},
	"690283": {
		"nik": "690283",
		"nama": "SITI SUHARWIYAH",
		"band": "III",
		"posisi": "MGR FINANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8493827160493849,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9846723044397437,
			"nilai_komparatif_unit": 0.989890217558387,
			"summary_team": 0.8363636363636363
		}]
	},
	"690285": {
		"nik": "690285",
		"nama": "WAHID IRSYADI",
		"band": "III",
		"posisi": "MGR SERVICE DELIVERY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8470588235294104,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0303030303030318,
			"nilai_komparatif_unit": 1.0357627468744803,
			"summary_team": 0.8727272727272727
		}]
	},
	"690303": {
		"nik": "690303",
		"nama": "IMAM SUGIONO",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8358208955223883,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8295238095238093,
			"nilai_komparatif_unit": 0.8339195695633932,
			"summary_team": 0.6933333333333334
		}]
	},
	"690304": {
		"nik": "690304",
		"nama": "SATRIA NUGROHO",
		"band": "III",
		"posisi": "SENIOR EXPERT TDM SWITCHING EWSD",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.862585034013604,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9906796673358205,
			"nilai_komparatif_unit": 0.9959294142914901,
			"summary_team": 0.8545454545454546
		}]
	},
	"690305": {
		"nik": "690305",
		"nama": "DADANG SUHENDAR",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564625850340167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0826774496353488,
			"nilai_komparatif_unit": 1.0884147054129711,
			"summary_team": 0.9272727272727274
		}]
	},
	"690306": {
		"nik": "690306",
		"nama": "EDY",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564625850340167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9963463065925299,
			"nilai_komparatif_unit": 1.0016260818440934,
			"summary_team": 0.8533333333333332
		}]
	},
	"690342": {
		"nik": "690342",
		"nama": "ERI SETIAWAN",
		"band": "III",
		"posisi": "KAKANDATEL SINTANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564625850340167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9963463065925299,
			"nilai_komparatif_unit": 1.0016260818440934,
			"summary_team": 0.8533333333333332
		}]
	},
	"690409": {
		"nik": "690409",
		"nama": "SRIE LESTARI WIDOWATI",
		"band": "III",
		"posisi": "MGR HOME SERVICE 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8495726495726486,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9573440643863189,
			"nilai_komparatif_unit": 0.9624171614259059,
			"summary_team": 0.8133333333333332
		}]
	},
	"700027": {
		"nik": "700027",
		"nama": "IMMANUEL HERY C RATUKORE",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8725925925925916,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.901528013582344,
			"nilai_komparatif_unit": 0.9063053337402134,
			"summary_team": 0.7866666666666667
		}]
	},
	"700198": {
		"nik": "700198",
		"nama": "YUDI",
		"band": "III",
		"posisi": "MGR CCAN & WAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8429378531073439,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9490616621983922,
			"nilai_komparatif_unit": 0.9540908696568106,
			"summary_team": 0.7999999999999999
		}]
	},
	"700267": {
		"nik": "700267",
		"nama": "HUSNIA MARASABESSY",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8584615384615386,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9318996415770608,
			"nilai_komparatif_unit": 0.9368379051426341,
			"summary_team": 0.8
		}]
	},
	"700273": {
		"nik": "700273",
		"nama": "ACHMAD DARWIN , SE.",
		"band": "III",
		"posisi": "KAKANDATEL TENGGARONG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8358208955223883,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0688095238095234,
			"nilai_komparatif_unit": 1.0744732915528334,
			"summary_team": 0.8933333333333332
		}]
	},
	"700275": {
		"nik": "700275",
		"nama": "RUSDIANAWATI , SH.",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8724637681159438,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0239202657807287,
			"nilai_komparatif_unit": 1.0293461592106266,
			"summary_team": 0.8933333333333333
		}]
	},
	"700276": {
		"nik": "700276",
		"nama": "RUDI SIANTURI",
		"band": "III",
		"posisi": "MGR PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564625850340167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.050833995234309,
			"nilai_komparatif_unit": 1.0564025081949424,
			"summary_team": 0.9
		}]
	},
	"700277": {
		"nik": "700277",
		"nama": "ERNAWANTO",
		"band": "III",
		"posisi": "KAKANDATEL BULUNGAN BERAU",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9531250000000001,
			"nilai_komparatif_unit": 0.9581757396407748,
			"summary_team": 0.8133333333333332
		}]
	},
	"700279": {
		"nik": "700279",
		"nama": "MUHAMMAD SABRANI",
		"band": "III",
		"posisi": "MGR CCAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564625850340167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879635852630255,
			"nilai_komparatif_unit": 0.9931989393285782,
			"summary_team": 0.846153846153846
		}]
	},
	"700282": {
		"nik": "700282",
		"nama": "TRI LAKSITOHADI, Amd",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8493827160493849,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9575581395348812,
			"nilai_komparatif_unit": 0.9626323709879386,
			"summary_team": 0.8133333333333332
		}]
	},
	"700283": {
		"nik": "700283",
		"nama": "MURSALIN",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564625850340167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9807783955520216,
			"nilai_komparatif_unit": 0.9859756743152793,
			"summary_team": 0.8399999999999999
		}]
	},
	"700288": {
		"nik": "700288",
		"nama": "BERTHA ESTER MAERANI SULELINO",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8725925925925916,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0084889643463508,
			"nilai_komparatif_unit": 1.0138330852009165,
			"summary_team": 0.88
		}]
	},
	"700305": {
		"nik": "700305",
		"nama": "MUHAMMAD ROFIQ",
		"band": "III",
		"posisi": "MGR FINANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564625850340167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0614484800346558,
			"nilai_komparatif_unit": 1.067073240600952,
			"summary_team": 0.9090909090909092
		}]
	},
	"700411": {
		"nik": "700411",
		"nama": "SABARUDIN",
		"band": "III",
		"posisi": "MGR DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8515151515151502,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0117711470024653,
			"nilai_komparatif_unit": 1.0171326605915092,
			"summary_team": 0.8615384615384616
		}]
	},
	"700535": {
		"nik": "700535",
		"nama": "TRIS SUHARI",
		"band": "III",
		"posisi": "MGR ENTERPRISE, OLO & WIFI DEPLOYMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8509803921568626,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.004189359028069,
			"nilai_komparatif_unit": 1.0095106956864164,
			"summary_team": 0.8545454545454546
		}]
	},
	"700642": {
		"nik": "700642",
		"nama": "AGUSTINUS KOMBER",
		"band": "III",
		"posisi": "MGR CCAN & WAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7860082304526756,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8807893677003616,
			"nilai_komparatif_unit": 0.8854567909393042,
			"summary_team": 0.6923076923076923
		}]
	},
	"710131": {
		"nik": "710131",
		"nama": "ARI IRYANTO S.S",
		"band": "III",
		"posisi": "MGR ACCESS SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9915865384615385,
			"nilai_komparatif_unit": 0.9968410910512474,
			"summary_team": 0.846153846153846
		}]
	},
	"710133": {
		"nik": "710133",
		"nama": "MOH YASIN",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9375000000000001,
			"nilai_komparatif_unit": 0.9424679406302704,
			"summary_team": 0.7999999999999999
		}]
	},
	"710140": {
		"nik": "710140",
		"nama": "SYAMSURIZAL , SE.",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8724637681159438,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.987477638640427,
			"nilai_komparatif_unit": 0.9927104176084112,
			"summary_team": 0.8615384615384614
		}]
	},
	"710142": {
		"nik": "710142",
		"nama": "SAHNA",
		"band": "III",
		"posisi": "MGR CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8871794871794874,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9837099316868101,
			"nilai_komparatif_unit": 0.9889227450607059,
			"summary_team": 0.8727272727272727
		}]
	},
	"710147": {
		"nik": "710147",
		"nama": "ZULPAN WARDANA , SE.",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8724637681159438,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.987477638640427,
			"nilai_komparatif_unit": 0.9927104176084112,
			"summary_team": 0.8615384615384614
		}]
	},
	"710161": {
		"nik": "710161",
		"nama": "RUDI ENDARYANTO",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8358208955223883,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0049999999999997,
			"nilai_komparatif_unit": 1.0103256323556493,
			"summary_team": 0.84
		}]
	},
	"710359": {
		"nik": "710359",
		"nama": "GUIDO BARRETO VICENTE",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8349206349206347,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0318806668616558,
			"nilai_komparatif_unit": 1.0373487435448476,
			"summary_team": 0.8615384615384616
		}]
	},
	"710360": {
		"nik": "710360",
		"nama": "KAREL KOIREWOA",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7860082304526756,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8651308900523551,
			"nilai_komparatif_unit": 0.8697153368781609,
			"summary_team": 0.6799999999999999
		}]
	},
	"710362": {
		"nik": "710362",
		"nama": "DAVID VICTOR",
		"band": "III",
		"posisi": "MGR ACCESS SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8708333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8880382775119618,
			"nilai_komparatif_unit": 0.8927441137147212,
			"summary_team": 0.7733333333333333
		}]
	},
	"710393": {
		"nik": "710393",
		"nama": "WIDODO, ST",
		"band": "III",
		"posisi": "MGR ACCESS DATA MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.868292682926828,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9674157303370801,
			"nilai_komparatif_unit": 0.9725421985110578,
			"summary_team": 0.8400000000000001
		}]
	},
	"710426": {
		"nik": "710426",
		"nama": "NAWAYOGA NURWAHJU MUH J.H, R.",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8321285140562269,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9774131274131249,
			"nilai_komparatif_unit": 0.9825925731605758,
			"summary_team": 0.8133333333333332
		}]
	},
	"710469": {
		"nik": "710469",
		"nama": "ANTONIUS JOKO SRITOMO",
		"band": "III",
		"posisi": "MGR ENTERPRISE, OLO & WIFI DEPLOYMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8405797101449269,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0382445141065837,
			"nilai_komparatif_unit": 1.043746313686088,
			"summary_team": 0.8727272727272727
		}]
	},
	"710475": {
		"nik": "710475",
		"nama": "MEI HENDRA DARMA",
		"band": "III",
		"posisi": "MGR GOVERNMENT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8495726495726486,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.957344064386319,
			"nilai_komparatif_unit": 0.962417161425906,
			"summary_team": 0.8133333333333334
		}]
	},
	"710484": {
		"nik": "710484",
		"nama": "RURUK YUSUF LOGEN",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0056300268096472,
			"nilai_komparatif_unit": 1.0109589977634683,
			"summary_team": 0.8266666666666667
		}]
	},
	"710506": {
		"nik": "710506",
		"nama": "IKHWAN HARAPAN",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8495726495726486,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0201207243460777,
			"nilai_komparatif_unit": 1.0255264834866213,
			"summary_team": 0.8666666666666667
		}]
	},
	"710508": {
		"nik": "710508",
		"nama": "I WAYAN SUYASA",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8321285140562269,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.105598455598453,
			"nilai_komparatif_unit": 1.11145717291934,
			"summary_team": 0.92
		}]
	},
	"710516": {
		"nik": "710516",
		"nama": "YAYAN NURYANA",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8493827160493849,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9418604651162766,
			"nilai_komparatif_unit": 0.9468515124471528,
			"summary_team": 0.7999999999999999
		}]
	},
	"710527": {
		"nik": "710527",
		"nama": "SUBIANTORO",
		"band": "III",
		"posisi": "MGR APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481477,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9432314410480356,
			"nilai_komparatif_unit": 0.9482297533677485,
			"summary_team": 0.8
		}]
	},
	"720021": {
		"nik": "720021",
		"nama": "FEBRI SUPRIADI",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564625850340167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9652104845115136,
			"nilai_komparatif_unit": 0.9703252667864657,
			"summary_team": 0.8266666666666667
		}]
	},
	"720055": {
		"nik": "720055",
		"nama": "YONS MAKO",
		"band": "III",
		"posisi": "MGR CCAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8404761904761923,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9518413597733691,
			"nilai_komparatif_unit": 0.956885297229145,
			"summary_team": 0.8
		}]
	},
	"720056": {
		"nik": "720056",
		"nama": "ANTONIO MANUEL LEMOS ANICETO",
		"band": "III",
		"posisi": "MGR ACCESS SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8429378531073439,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8857908847184994,
			"nilai_komparatif_unit": 0.8904848116796898,
			"summary_team": 0.7466666666666667
		}]
	},
	"720060": {
		"nik": "720060",
		"nama": "JUSTINO FERNANDES",
		"band": "III",
		"posisi": "MGR ACCESS DATA & INVENTORY MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8790960451977408,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0325291674906063,
			"nilai_komparatif_unit": 1.038000680667263,
			"summary_team": 0.9076923076923077
		}]
	},
	"720061": {
		"nik": "720061",
		"nama": "EUSTAQUIO DA COSTA MAGALHAENS",
		"band": "III",
		"posisi": "MGR NETWORK & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7860082304526756,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9579303972898052,
			"nilai_komparatif_unit": 0.9630066013875833,
			"summary_team": 0.7529411764705883
		}]
	},
	"720106": {
		"nik": "720106",
		"nama": "JOAO SOARES",
		"band": "III",
		"posisi": "KAKANDATEL TUAL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8584615384615386,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.009557945041816,
			"nilai_komparatif_unit": 1.014907730571187,
			"summary_team": 0.8666666666666667
		}]
	},
	"720108": {
		"nik": "720108",
		"nama": "BAHARUDIN",
		"band": "III",
		"posisi": "MGR PLANNING, ENGINEERING & CAPEX MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8405797101449269,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9517241379310352,
			"nilai_komparatif_unit": 0.9567674542122474,
			"summary_team": 0.8
		}]
	},
	"720110": {
		"nik": "720110",
		"nama": "SEMUEL ARNORLD",
		"band": "III",
		"posisi": "KAKANDATEL MERAUKE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7860082304526756,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9878657222051115,
			"nilai_komparatif_unit": 0.9931005576809451,
			"summary_team": 0.7764705882352941
		}]
	},
	"720124": {
		"nik": "720124",
		"nama": "ARI BASUKI",
		"band": "III",
		"posisi": "MGR CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8495726495726486,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8945674044265605,
			"nilai_komparatif_unit": 0.899307839365191,
			"summary_team": 0.76
		}]
	},
	"720145": {
		"nik": "720145",
		"nama": "WAHYU SETIYO UTOMO",
		"band": "III",
		"posisi": "MGR HOME SERVICE 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379084967320246,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0502340093603764,
			"nilai_komparatif_unit": 1.0557993429138623,
			"summary_team": 0.88
		}]
	},
	"720173": {
		"nik": "720173",
		"nama": "FIRMANSYAH SUTAN",
		"band": "III",
		"posisi": "MGR TELKOMGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8708333333333323,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8858509911141499,
			"nilai_komparatif_unit": 0.8905452365873457,
			"summary_team": 0.7714285714285714
		}]
	},
	"720199": {
		"nik": "720199",
		"nama": "Dr. E. ANSHARDAUD",
		"band": "III",
		"posisi": "MGR REVENUE ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8380952380952381,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9912587412587412,
			"nilai_komparatif_unit": 0.9965115568062717,
			"summary_team": 0.8307692307692308
		}]
	},
	"720218": {
		"nik": "720218",
		"nama": "TEGUH RIYANTO",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & CONSTRUCTION SPV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8515151515151502,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.993703805091707,
			"nilai_komparatif_unit": 0.9989695773666608,
			"summary_team": 0.8461538461538461
		}]
	},
	"720289": {
		"nik": "720289",
		"nama": "D. RUSYAD HAKIM, ST",
		"band": "III",
		"posisi": "KAKANDATEL KETAPANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564625850340167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9652104845115134,
			"nilai_komparatif_unit": 0.9703252667864655,
			"summary_team": 0.8266666666666665
		}]
	},
	"720298": {
		"nik": "720298",
		"nama": "STEPHENS CHRISTIANS SAROINSONG",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8404761904761923,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.015297450424927,
			"nilai_komparatif_unit": 1.0206776503777546,
			"summary_team": 0.8533333333333334
		}]
	},
	"720306": {
		"nik": "720306",
		"nama": "ROSNIDA ROKHMAH, ST",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8515151515151502,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9551601423487559,
			"nilai_komparatif_unit": 0.9602216664869843,
			"summary_team": 0.8133333333333334
		}]
	},
	"720335": {
		"nik": "720335",
		"nama": "IRWAN IRAWAN, ST",
		"band": "III",
		"posisi": "MGR WIRELESS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8585858585858576,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0191176470588248,
			"nilai_komparatif_unit": 1.0245180907557303,
			"summary_team": 0.875
		}]
	},
	"720337": {
		"nik": "720337",
		"nama": "MASYKUR ABIDIN",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8724637681159438,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0239202657807287,
			"nilai_komparatif_unit": 1.0293461592106266,
			"summary_team": 0.8933333333333333
		}]
	},
	"720357": {
		"nik": "720357",
		"nama": "SUMADI",
		"band": "III",
		"posisi": "MGR HR SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8708333333333323,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.014354066985647,
			"nilai_komparatif_unit": 1.0197292678206953,
			"summary_team": 0.8833333333333333
		}]
	},
	"720385": {
		"nik": "720385",
		"nama": "ARIF SUTRISNO",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8358208955223883,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0688095238095234,
			"nilai_komparatif_unit": 1.0744732915528334,
			"summary_team": 0.8933333333333332
		}]
	},
	"720392": {
		"nik": "720392",
		"nama": "ANA FAOZAN SUNARTO, ST",
		"band": "III",
		"posisi": "MGR DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481477,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0289797538705843,
			"nilai_komparatif_unit": 1.034432458219362,
			"summary_team": 0.8727272727272728
		}]
	},
	"720407": {
		"nik": "720407",
		"nama": "HARNINDYO KHRISNADI",
		"band": "III",
		"posisi": "MGR SAS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8495726495726486,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.995975855130786,
			"nilai_komparatif_unit": 1.0012536673094232,
			"summary_team": 0.8461538461538463
		}]
	},
	"720425": {
		"nik": "720425",
		"nama": "ANTON SKUNDRIANTO",
		"band": "III",
		"posisi": "MGR INFRA SERVICE DELIVERY & OPR SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.862585034013604,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0117579581301996,
			"nilai_komparatif_unit": 1.0171194018296068,
			"summary_team": 0.8727272727272728
		}]
	},
	"720428": {
		"nik": "720428",
		"nama": "WAHYU JATI NUGROHO,ST",
		"band": "III",
		"posisi": "KAKANDATEL BANJARBARU",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8321285140562269,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.961389961389959,
			"nilai_komparatif_unit": 0.9664844981907303,
			"summary_team": 0.7999999999999999
		}]
	},
	"720429": {
		"nik": "720429",
		"nama": "AHMAD FAZAL MUZAKKI",
		"band": "III",
		"posisi": "MGR HOME SERVICE 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379084967320246,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9865834633385353,
			"nilai_komparatif_unit": 0.9918115039493857,
			"summary_team": 0.8266666666666667
		}]
	},
	"720432": {
		"nik": "720432",
		"nama": "IVONE ANDAYANI",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379084967320246,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.907020280811234,
			"nilai_komparatif_unit": 0.91182670524379,
			"summary_team": 0.7599999999999999
		}]
	},
	"720447": {
		"nik": "720447",
		"nama": "MARGIYONO",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8515151515151502,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0647686832740229,
			"nilai_komparatif_unit": 1.0704110380510643,
			"summary_team": 0.9066666666666666
		}]
	},
	"720477": {
		"nik": "720477",
		"nama": "ANDY SUHERMAN,ST",
		"band": "III",
		"posisi": "MGR PERFORMANCE & WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8380952380952381,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1064049586776858,
			"nilai_komparatif_unit": 1.1122679497686163,
			"summary_team": 0.9272727272727272
		}]
	},
	"720490": {
		"nik": "720490",
		"nama": "WARDONO",
		"band": "III",
		"posisi": "SENIOR STAFF III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8460784313725501,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7091541135573571,
			"nilai_komparatif_unit": 0.712912018126813,
			"summary_team": 0.6
		}]
	},
	"720529": {
		"nik": "720529",
		"nama": "WAWAN ARDI SETYANTORO",
		"band": "III",
		"posisi": "MGR BILLING & PAYMENT COLLECTION (CONS)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740731,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9776579352850551,
			"nilai_komparatif_unit": 0.9828386783028777,
			"summary_team": 0.8545454545454546
		}]
	},
	"720553": {
		"nik": "720553",
		"nama": "DWI ANGGARA SEDYA LAKSANA",
		"band": "III",
		"posisi": "MGR HR SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8871794871794874,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.004203888596952,
			"nilai_komparatif_unit": 1.0095253022494706,
			"summary_team": 0.8909090909090909
		}]
	},
	"720568": {
		"nik": "720568",
		"nama": "IWAN NOVIANI",
		"band": "III",
		"posisi": "MGR ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8321285140562269,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0575289575289548,
			"nilai_komparatif_unit": 1.0631329480098033,
			"summary_team": 0.8799999999999998
		}]
	},
	"720585": {
		"nik": "720585",
		"nama": "GAMRUD SIONIBUNGAYA",
		"band": "III",
		"posisi": "MGR CUSTOMER DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9592760180995484,
			"nilai_komparatif_unit": 0.9643593528259063,
			"summary_team": 0.8153846153846154
		}]
	},
	"720586": {
		"nik": "720586",
		"nama": "MUCHAMMAD ARIF SUHARTONO , ST.",
		"band": "III",
		"posisi": "MGR FINANCE & PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9531250000000001,
			"nilai_komparatif_unit": 0.9581757396407748,
			"summary_team": 0.8133333333333332
		}]
	},
	"720595": {
		"nik": "720595",
		"nama": "MAS BUDITJAHJONO APRILIANTO",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8321285140562269,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7765072765072745,
			"nilai_komparatif_unit": 0.7806220946925129,
			"summary_team": 0.646153846153846
		}]
	},
	"730007": {
		"nik": "730007",
		"nama": "FARIDA",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8429378531073439,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0220664054444224,
			"nilai_komparatif_unit": 1.0274824750150269,
			"summary_team": 0.8615384615384615
		}]
	},
	"730024": {
		"nik": "730024",
		"nama": "AGOSTINHO ELU",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8404761904761923,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9994334277620376,
			"nilai_komparatif_unit": 1.004729562090602,
			"summary_team": 0.8400000000000001
		}]
	},
	"730030": {
		"nik": "730030",
		"nama": "JOHN SALMON LUTHER LEWAHERILLA",
		"band": "III",
		"posisi": "MGR  ACCESS SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8584615384615386,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0931899641577059,
			"nilai_komparatif_unit": 1.0989829271865514,
			"summary_team": 0.9384615384615385
		}]
	},
	"730034": {
		"nik": "730034",
		"nama": "HARY DJUNAIDI PURWANTO",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8404761904761923,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9518413597733691,
			"nilai_komparatif_unit": 0.956885297229145,
			"summary_team": 0.8
		}]
	},
	"730065": {
		"nik": "730065",
		"nama": "AJID LUKMAN",
		"band": "III",
		"posisi": "MGR CCAN & WAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8584615384615386,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9954382535027695,
			"nilai_komparatif_unit": 1.0007132168569046,
			"summary_team": 0.8545454545454546
		}]
	},
	"730120": {
		"nik": "730120",
		"nama": "SALEHUDIN",
		"band": "III",
		"posisi": "KAKANDATEL SINGKAWANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564625850340167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9807783955520218,
			"nilai_komparatif_unit": 0.9859756743152795,
			"summary_team": 0.84
		}]
	},
	"730128": {
		"nik": "730128",
		"nama": "ARIS DARMAWANTO",
		"band": "III",
		"posisi": "KAKANDATEL LAMONGAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379084967320246,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0184087363494558,
			"nilai_komparatif_unit": 1.023805423431624,
			"summary_team": 0.8533333333333333
		}]
	},
	"730158": {
		"nik": "730158",
		"nama": "ERWIN PARTOGI GULTOM",
		"band": "III",
		"posisi": "MGR WHOLESALE SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8142857142857147,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.921052631578947,
			"nilai_komparatif_unit": 0.9259334153560546,
			"summary_team": 0.7500000000000001
		}]
	},
	"730178": {
		"nik": "730178",
		"nama": "RAHADYAS BHARATA WIDYANDARU,ST",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8515151515151502,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9551601423487559,
			"nilai_komparatif_unit": 0.9602216664869843,
			"summary_team": 0.8133333333333334
		}]
	},
	"730206": {
		"nik": "730206",
		"nama": "TAUFIK",
		"band": "III",
		"posisi": "MGR GOVERNMENT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8493827160493849,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9961985688729849,
			"nilai_komparatif_unit": 1.0014775612421807,
			"summary_team": 0.846153846153846
		}]
	},
	"730221": {
		"nik": "730221",
		"nama": "NINING HERIYANTI",
		"band": "III",
		"posisi": "MGR CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8313725490196076,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200691,
			"nilai_komparatif_unit": 1.0553054402117337,
			"summary_team": 0.8727272727272728
		}]
	},
	"730226": {
		"nik": "730226",
		"nama": "BAYU AJI WIDODO",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.868292682926828,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.009939498703545,
			"nilai_komparatif_unit": 1.0152913061379174,
			"summary_team": 0.8769230769230769
		}]
	},
	"730361": {
		"nik": "730361",
		"nama": "BAHARUDIN RAMLI",
		"band": "III",
		"posisi": "MGR SALES BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8390804597701138,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1267745952677475,
			"nilai_komparatif_unit": 1.1327455278469336,
			"summary_team": 0.9454545454545454
		}]
	},
	"730409": {
		"nik": "730409",
		"nama": "ARIEF DJOKO PRIHATIN.MMSI",
		"band": "III",
		"posisi": "MGR BACKBONE & CME OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.862585034013604,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9809512254307222,
			"nilai_komparatif_unit": 0.9861494200431282,
			"summary_team": 0.846153846153846
		}]
	},
	"730428": {
		"nik": "730428",
		"nama": "ARIF MUNANDAR",
		"band": "III",
		"posisi": "MGR DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.973190348525465,
			"nilai_komparatif_unit": 0.9783474171904534,
			"summary_team": 0.8
		}]
	},
	"730476": {
		"nik": "730476",
		"nama": "YUSUF BUDI MULIA",
		"band": "III",
		"posisi": "KAKANDATEL GRESIK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379084967320246,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0661466458658366,
			"nilai_komparatif_unit": 1.0717963026549815,
			"summary_team": 0.8933333333333333
		}]
	},
	"730479": {
		"nik": "730479",
		"nama": "PANGUDI",
		"band": "III",
		"posisi": "SENIOR STAFF III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.862585034013604,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8431316317751664,
			"nilai_komparatif_unit": 0.8475995015246723,
			"summary_team": 0.7272727272727273
		}]
	},
	"730488": {
		"nik": "730488",
		"nama": "SRI YUNANTO",
		"band": "III",
		"posisi": "MGR ACCESS NEW FTTH & MODERNIZATION DEPL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8509803921568626,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9943282523927686,
			"nilai_komparatif_unit": 0.9995973336993154,
			"summary_team": 0.846153846153846
		}]
	},
	"730505": {
		"nik": "730505",
		"nama": "ERWIN RISWANTORO",
		"band": "III",
		"posisi": "MGR TELKOMGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.862585034013604,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440063091482664,
			"nilai_komparatif_unit": 0.9490087275999455,
			"summary_team": 0.8142857142857142
		}]
	},
	"730548": {
		"nik": "730548",
		"nama": "ANDI NAUFAL ISKANDAR",
		"band": "III",
		"posisi": "KAKANDATEL NUNUKAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9375000000000001,
			"nilai_komparatif_unit": 0.9424679406302704,
			"summary_team": 0.7999999999999999
		}]
	},
	"730552": {
		"nik": "730552",
		"nama": "SUNARDI, ST",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8358208955223883,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7831168831168829,
			"nilai_komparatif_unit": 0.7872667265108956,
			"summary_team": 0.6545454545454545
		}]
	},
	"730577": {
		"nik": "730577",
		"nama": "SUSENO",
		"band": "III",
		"posisi": "MGR ACCESS SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8316666666666652,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8744762251776294,
			"nilai_komparatif_unit": 0.8791101942115124,
			"summary_team": 0.7272727272727272
		}]
	},
	"730578": {
		"nik": "730578",
		"nama": "TEGUH BUDIYONO, ST",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8321285140562269,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0168547668547643,
			"nilai_komparatif_unit": 1.0222432192401956,
			"summary_team": 0.846153846153846
		}]
	},
	"730579": {
		"nik": "730579",
		"nama": "JONI SUPRAPTO",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8460784313725501,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.000089134503965,
			"nilai_komparatif_unit": 1.005388743512172,
			"summary_team": 0.8461538461538461
		}]
	},
	"730595": {
		"nik": "730595",
		"nama": "INDRA DJOKO WIBOWO",
		"band": "III",
		"posisi": "SENIOR AM ENTERPRISE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9384335503638414,
			"nilai_komparatif_unit": 0.9434064380050801,
			"summary_team": 0.7714285714285715
		}]
	},
	"740007": {
		"nik": "740007",
		"nama": "ANDREW MARTHINUS PATTIWAELLAPIA",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8404761904761923,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9798366938843506,
			"nilai_komparatif_unit": 0.9850289824417668,
			"summary_team": 0.823529411764706
		}]
	},
	"740009": {
		"nik": "740009",
		"nama": "CHARLES ARONGGEAR",
		"band": "III",
		"posisi": "KAKANDATEL ABEPURA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7860082304526756,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.135239629480466,
			"nilai_komparatif_unit": 1.141255419432881,
			"summary_team": 0.8923076923076922
		}]
	},
	"740010": {
		"nik": "740010",
		"nama": "PENTIARSO WISMONO RUMPOKO",
		"band": "III",
		"posisi": "MGR DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8166666666666655,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.979591836734695,
			"nilai_komparatif_unit": 0.98478282776061,
			"summary_team": 0.7999999999999998
		}]
	},
	"740011": {
		"nik": "740011",
		"nama": "BRIAN SAUYAI",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8349206349206347,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0220532319391638,
			"nilai_komparatif_unit": 1.0274692317015632,
			"summary_team": 0.8533333333333334
		}]
	},
	"740013": {
		"nik": "740013",
		"nama": "SALMON BRONZON TARESSY",
		"band": "III",
		"posisi": "MGR FULFILLMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8470588235294104,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7673611111111123,
			"nilai_komparatif_unit": 0.771427462515889,
			"summary_team": 0.6499999999999999
		}]
	},
	"740014": {
		"nik": "740014",
		"nama": "DOMINGGUS JULIAN SUTRAHITU",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8584615384615386,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9784946236559139,
			"nilai_komparatif_unit": 0.9836798003997659,
			"summary_team": 0.8400000000000001
		}]
	},
	"740023": {
		"nik": "740023",
		"nama": "AMRI",
		"band": "III",
		"posisi": "KAKANDATEL TERNATE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8404761904761923,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9994334277620376,
			"nilai_komparatif_unit": 1.004729562090602,
			"summary_team": 0.8400000000000001
		}]
	},
	"740024": {
		"nik": "740024",
		"nama": "YOSEPH ALEXANDER TIBA",
		"band": "III",
		"posisi": "MGR CCAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.973190348525465,
			"nilai_komparatif_unit": 0.9783474171904534,
			"summary_team": 0.8
		}]
	},
	"740025": {
		"nik": "740025",
		"nama": "G.ROBINSON M.MANDIBONDIBO",
		"band": "III",
		"posisi": "MGR CCAN & WAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8349206349206347,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9901140684410649,
			"nilai_komparatif_unit": 0.9953608182108894,
			"summary_team": 0.8266666666666667
		}]
	},
	"740028": {
		"nik": "740028",
		"nama": "SIPRIANUS AMOL",
		"band": "III",
		"posisi": "MGR OLO FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8708333333333323,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969708,
			"nilai_komparatif_unit": 0.9748355264700989,
			"summary_team": 0.8444444444444444
		}]
	},
	"740032": {
		"nik": "740032",
		"nama": "CLAUDIANO LEMOS SOARES XIMENES",
		"band": "III",
		"posisi": "MGR BILLING & PAYMENT COLLECTION (CONS)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8488888888888888,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0923369823893385,
			"nilai_komparatif_unit": 1.0981254253512145,
			"summary_team": 0.9272727272727272
		}]
	},
	"740033": {
		"nik": "740033",
		"nama": "HERRY WILLEM WAKUM",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8349206349206347,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0318806668616558,
			"nilai_komparatif_unit": 1.0373487435448476,
			"summary_team": 0.8615384615384616
		}]
	},
	"740075": {
		"nik": "740075",
		"nama": "ABRAHAM ALLOPASAU' TANGDIALLA'",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8470588235294104,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9989316239316254,
			"nilai_komparatif_unit": 1.0042250991331099,
			"summary_team": 0.846153846153846
		}]
	},
	"740164": {
		"nik": "740164",
		"nama": "JUNED DRAJAT PRASETYO",
		"band": "III",
		"posisi": "MGR GOVERNMENT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8724637681159438,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0628209000302002,
			"nilai_komparatif_unit": 1.0684529332376247,
			"summary_team": 0.9272727272727274
		}]
	},
	"740166": {
		"nik": "740166",
		"nama": "PUPON ARTIONO",
		"band": "III",
		"posisi": "MGR PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8515151515151502,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9756364631809487,
			"nilai_komparatif_unit": 0.9808064941418124,
			"summary_team": 0.8307692307692308
		}]
	},
	"740187": {
		"nik": "740187",
		"nama": "ILHAMDI",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.940750670241283,
			"nilai_komparatif_unit": 0.9457358366174383,
			"summary_team": 0.7733333333333334
		}]
	},
	"740188": {
		"nik": "740188",
		"nama": "I PUTU GEDE WIDIANA",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9068364611260016,
			"nilai_komparatif_unit": 0.9116419114729225,
			"summary_team": 0.7454545454545455
		}]
	},
	"740212": {
		"nik": "740212",
		"nama": "ASWOTO PURWO NUGROHO",
		"band": "III",
		"posisi": "MGR CCAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.868292682926828,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0630942091616262,
			"nilai_komparatif_unit": 1.068727690671492,
			"summary_team": 0.923076923076923
		}]
	},
	"740230": {
		"nik": "740230",
		"nama": "WISNU AJI PRASETYA, ST",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0625000000000002,
			"nilai_komparatif_unit": 1.0681303327143066,
			"summary_team": 0.9066666666666666
		}]
	},
	"740241": {
		"nik": "740241",
		"nama": "A. IDHAM CHALID",
		"band": "III",
		"posisi": "MGR HOME SERVICE 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.973190348525465,
			"nilai_komparatif_unit": 0.9783474171904534,
			"summary_team": 0.8
		}]
	},
	"740251": {
		"nik": "740251",
		"nama": "AGUS SULAKSONO,ST",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8493827160493849,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.032423971377457,
			"nilai_komparatif_unit": 1.0378949271055327,
			"summary_team": 0.8769230769230768
		}]
	},
	"740267": {
		"nik": "740267",
		"nama": "MINTO RAHADI, ST",
		"band": "III",
		"posisi": "MGR CUSTOMER RELATIONSHIP MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8313725490196076,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9622641509433966,
			"nilai_komparatif_unit": 0.967363320194089,
			"summary_team": 0.8
		}]
	},
	"740271": {
		"nik": "740271",
		"nama": "AGUSTINUS GUSTI",
		"band": "III",
		"posisi": "MGR CHANNEL ACTIVATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481477,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9432314410480354,
			"nilai_komparatif_unit": 0.9482297533677483,
			"summary_team": 0.7999999999999999
		}]
	},
	"740300": {
		"nik": "740300",
		"nama": "ZULKIFLI DJAFAR",
		"band": "III",
		"posisi": "SM PAYMENT COLLECTION & FINANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8439215686274537,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0111524163568741,
			"nilai_komparatif_unit": 1.0165106512076638,
			"summary_team": 0.8533333333333334
		}]
	},
	"740309": {
		"nik": "740309",
		"nama": "ARIF NUGROHO",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.868292682926828,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9745030250648243,
			"nilai_komparatif_unit": 0.9796670497822012,
			"summary_team": 0.8461538461538463
		}]
	},
	"750005": {
		"nik": "750005",
		"nama": "JANGKIR SYORS SIMBIAK",
		"band": "III",
		"posisi": "KAKANDATEL TIMIKA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7860082304526756,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0517277486910985,
			"nilai_komparatif_unit": 1.0573009977734507,
			"summary_team": 0.8266666666666667
		}]
	},
	"750006": {
		"nik": "750006",
		"nama": "IRHAM RUSYDI KIAT",
		"band": "III",
		"posisi": "SENIOR EXPERT TDM SWITCHING EWSD",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8585858585858576,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9952941176470601,
			"nilai_komparatif_unit": 1.0005683172055964,
			"summary_team": 0.8545454545454546
		}]
	},
	"750007": {
		"nik": "750007",
		"nama": "JOHAN WILHELM NIKYULUW",
		"band": "III",
		"posisi": "MGR ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8404761904761923,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9734741179500365,
			"nilai_komparatif_unit": 0.9786326903479889,
			"summary_team": 0.8181818181818181
		}]
	},
	"750008": {
		"nik": "750008",
		"nama": "JODRY JHONNY FANNY TITIOKA",
		"band": "III",
		"posisi": "MGR ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0705093833780115,
			"nilai_komparatif_unit": 1.0761821589094986,
			"summary_team": 0.88
		}]
	},
	"750011": {
		"nik": "750011",
		"nama": "WAGE IRJAYANTO",
		"band": "III",
		"posisi": "KAKANDATEL GOWA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9894101876675561,
			"nilai_komparatif_unit": 0.9946532074769608,
			"summary_team": 0.8133333333333332
		}]
	},
	"750012": {
		"nik": "750012",
		"nama": "MUHAMMAD IRIANTO",
		"band": "III",
		"posisi": "KAKANDATEL KOTAMOBAGO",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8404761904761923,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9884506428415757,
			"nilai_komparatif_unit": 0.9936885778918044,
			"summary_team": 0.8307692307692308
		}]
	},
	"750014": {
		"nik": "750014",
		"nama": "ABDUL WAHAB GOLIB",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8429378531073439,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1315735203134676,
			"nilai_komparatif_unit": 1.137569883052351,
			"summary_team": 0.9538461538461538
		}]
	},
	"750016": {
		"nik": "750016",
		"nama": "BAMBANG AGUS VICTORANY ROGITO",
		"band": "III",
		"posisi": "MGR GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9588787257530318,
			"nilai_komparatif_unit": 0.9639599551729467,
			"summary_team": 0.7882352941176471
		}]
	},
	"750020": {
		"nik": "750020",
		"nama": "MARIA HELENA",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7860082304526756,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0686910994764387,
			"nilai_komparatif_unit": 1.0743542396730223,
			"summary_team": 0.84
		}]
	},
	"750022": {
		"nik": "750022",
		"nama": "ENGELBERTHUS",
		"band": "III",
		"posisi": "KAKANDATEL BIAK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8349206349206347,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9299932900917024,
			"nilai_komparatif_unit": 0.9349214516402093,
			"summary_team": 0.7764705882352942
		}]
	},
	"750023": {
		"nik": "750023",
		"nama": "DEWI PUSPITARINI",
		"band": "III",
		"posisi": "MGR PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0380697050938295,
			"nilai_komparatif_unit": 1.0435705783364837,
			"summary_team": 0.8533333333333334
		}]
	},
	"750072": {
		"nik": "750072",
		"nama": "ARYANTO PRATOMO",
		"band": "III",
		"posisi": "MGR WIRELESS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.862585034013604,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9485230857470621,
			"nilai_komparatif_unit": 0.9535494392152564,
			"summary_team": 0.8181818181818182
		}]
	},
	"760001": {
		"nik": "760001",
		"nama": "IRFIN YOAS ELIEZER SINON",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8177777777777777,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9782608695652173,
			"nilai_komparatif_unit": 0.983444807614195,
			"summary_team": 0.7999999999999998
		}]
	},
	"760002": {
		"nik": "760002",
		"nama": "FERDINAND YOHANNIS MARKUS",
		"band": "III",
		"posisi": "KAKANDATEL WAINGAPU",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606965174129362,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.947354379724321,
			"nilai_komparatif_unit": 0.952374540059571,
			"summary_team": 0.8153846153846154
		}]
	},
	"760003": {
		"nik": "760003",
		"nama": "JACQUELINE ELIZABETH",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8584615384615386,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9318996415770608,
			"nilai_komparatif_unit": 0.9368379051426341,
			"summary_team": 0.8
		}]
	},
	"760011": {
		"nik": "760011",
		"nama": "SOFJAN SALVIUS",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8404761904761923,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9518413597733691,
			"nilai_komparatif_unit": 0.956885297229145,
			"summary_team": 0.8
		}]
	},
	"760013": {
		"nik": "760013",
		"nama": "KORNELIUS GREGORIUS GUNTUR",
		"band": "III",
		"posisi": "MGR ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8790960451977408,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0325291674906063,
			"nilai_komparatif_unit": 1.038000680667263,
			"summary_team": 0.9076923076923077
		}]
	},
	"760014": {
		"nik": "760014",
		"nama": "HENDRIKUS HENA AMA",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8460784313725501,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.000089134503965,
			"nilai_komparatif_unit": 1.005388743512172,
			"summary_team": 0.8461538461538461
		}]
	},
	"760016": {
		"nik": "760016",
		"nama": "CILVERO PAULO",
		"band": "III",
		"posisi": "MGR ACCESS SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8725925925925916,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0123089983022084,
			"nilai_komparatif_unit": 1.017673362038799,
			"summary_team": 0.8833333333333335
		}]
	},
	"760017": {
		"nik": "760017",
		"nama": "MUHAMAD",
		"band": "III",
		"posisi": "SENIOR STAFF III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8405797101449269,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9517241379310352,
			"nilai_komparatif_unit": 0.9567674542122474,
			"summary_team": 0.8
		}]
	},
	"760018": {
		"nik": "760018",
		"nama": "FERDINANDUS GELALANG LAMA",
		"band": "III",
		"posisi": "KAKANDATEL LUWUK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8725925925925916,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9437730949765317,
			"nilai_komparatif_unit": 0.9487742775944409,
			"summary_team": 0.823529411764706
		}]
	},
	"760021": {
		"nik": "760021",
		"nama": "LAN LATUAMURY",
		"band": "III",
		"posisi": "KAKANDATEL MASOHI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8584615384615386,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9784946236559139,
			"nilai_komparatif_unit": 0.9836798003997659,
			"summary_team": 0.8400000000000001
		}]
	},
	"760026": {
		"nik": "760026",
		"nama": "CICI CITRA AMBUWARU",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9194711538461539,
			"nilai_komparatif_unit": 0.9243435571566112,
			"summary_team": 0.7846153846153845
		}]
	},
	"760027": {
		"nik": "760027",
		"nama": "FRANSISKUS WIPARLO",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8724637681159438,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0756452849476081,
			"nilai_komparatif_unit": 1.081345276323448,
			"summary_team": 0.9384615384615383
		}]
	},
	"760028": {
		"nik": "760028",
		"nama": "JOKO SULISTIYO",
		"band": "III",
		"posisi": "SENIOR AM GOVERNMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8395833333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9745093616061367,
			"nilai_komparatif_unit": 0.979673419901712,
			"summary_team": 0.8181818181818181
		}]
	},
	"760037": {
		"nik": "760037",
		"nama": "M. BUSTANUL ARIFIN",
		"band": "III",
		"posisi": "KAKANDATEL FAK-FAK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8349206349206347,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9950277859023109,
			"nilai_komparatif_unit": 1.0003005741325317,
			"summary_team": 0.8307692307692308
		}]
	},
	"770007": {
		"nik": "770007",
		"nama": "TERSILA BAKASSEUW LERMATIN",
		"band": "III",
		"posisi": "KAKANDATEL BITUNG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8404761904761923,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0067552843756788,
			"nilai_komparatif_unit": 1.012090218223134,
			"summary_team": 0.8461538461538461
		}]
	},
	"770008": {
		"nik": "770008",
		"nama": "NOH MUHAMMAD ALI",
		"band": "III",
		"posisi": "MGR INFRA MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8585858585858576,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9983193277310934,
			"nilai_komparatif_unit": 1.0036095582913274,
			"summary_team": 0.857142857142857
		}]
	},
	"770012": {
		"nik": "770012",
		"nama": "SEBASTIANUS BAS BONE",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8316666666666652,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9779559118236492,
			"nilai_komparatif_unit": 0.983138233859875,
			"summary_team": 0.8133333333333335
		}]
	},
	"770013": {
		"nik": "770013",
		"nama": "JITZAK JAKHIN TAMESEN NINEF",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7860082304526756,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1195811518324597,
			"nilai_komparatif_unit": 1.1255139653717379,
			"summary_team": 0.88
		}]
	},
	"770016": {
		"nik": "770016",
		"nama": "PETSON KALE MOY",
		"band": "III",
		"posisi": "MGR ACCESS SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8349206349206347,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9901140684410649,
			"nilai_komparatif_unit": 0.9953608182108894,
			"summary_team": 0.8266666666666667
		}]
	},
	"770017": {
		"nik": "770017",
		"nama": "RIFAI ABDULLAH MARUAPEY",
		"band": "III",
		"posisi": "KAKANDATEL MANOKWARI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8349206349206347,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9741444866920155,
			"nilai_komparatif_unit": 0.9793066114655525,
			"summary_team": 0.8133333333333334
		}]
	},
	"770018": {
		"nik": "770018",
		"nama": "MARTHEN ERNEST SOAN",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8725925925925916,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0123089983022084,
			"nilai_komparatif_unit": 1.017673362038799,
			"summary_team": 0.8833333333333335
		}]
	},
	"770021": {
		"nik": "770021",
		"nama": "HAMSI WAJA BALI",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.025325545767901,
			"nilai_komparatif_unit": 1.0307588859685135,
			"summary_team": 0.842857142857143
		}]
	},
	"770022": {
		"nik": "770022",
		"nama": "JORGEN PITER SAMBONO",
		"band": "III",
		"posisi": "MGR HELPDESK OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8790960451977408,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8847472150814047,
			"nilai_komparatif_unit": 0.8894356114945475,
			"summary_team": 0.7777777777777779
		}]
	},
	"770040": {
		"nik": "770040",
		"nama": "WAHYU SULISTYOMO",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8493827160493849,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.014311270125221,
			"nilai_komparatif_unit": 1.0196862441738568,
			"summary_team": 0.8615384615384615
		}]
	},
	"770056": {
		"nik": "770056",
		"nama": "AGUNG PURNOMO",
		"band": "III",
		"posisi": "MGR HOME SERVICE 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8515151515151502,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9708185053380798,
			"nilai_komparatif_unit": 0.9759630052818529,
			"summary_team": 0.8266666666666667
		}]
	},
	"770069": {
		"nik": "770069",
		"nama": "FAUZI OKTRIYANDARU",
		"band": "III",
		"posisi": "MGR PLAN & BUDGET CONTROL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740731,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9152542372881369,
			"nilai_komparatif_unit": 0.9201042945814177,
			"summary_team": 0.8000000000000003
		}]
	},
	"770071": {
		"nik": "770071",
		"nama": "NOVA IKAWARDHANA",
		"band": "III",
		"posisi": "MGR PERFORMANCE & WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740731,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0032594524119958,
			"nilai_komparatif_unit": 1.008575861368092,
			"summary_team": 0.8769230769230769
		}]
	},
	"770072": {
		"nik": "770072",
		"nama": "MUHTAR LUTFI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8177777777777777,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9782608695652173,
			"nilai_komparatif_unit": 0.983444807614195,
			"summary_team": 0.7999999999999998
		}]
	},
	"780012": {
		"nik": "780012",
		"nama": "JOHAN TITALEY",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7860082304526756,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9395086588803858,
			"nilai_komparatif_unit": 0.9444872436685913,
			"summary_team": 0.7384615384615385
		}]
	},
	"780021": {
		"nik": "780021",
		"nama": "RIO TUKLOY",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7860082304526756,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9499476439790567,
			"nilai_komparatif_unit": 0.95498154637602,
			"summary_team": 0.7466666666666667
		}]
	},
	"780031": {
		"nik": "780031",
		"nama": "YUSUF KURNIAWAN, ST.",
		"band": "III",
		"posisi": "KAKANDATEL BONTANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8724637681159438,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0086378737541508,
			"nilai_komparatif_unit": 1.0139827837000204,
			"summary_team": 0.88
		}]
	},
	"780044": {
		"nik": "780044",
		"nama": "HUSNUL MUFIDAH",
		"band": "III",
		"posisi": "MGR WHOLESALE SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8177777777777777,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0346989966555185,
			"nilai_komparatif_unit": 1.0401820080534756,
			"summary_team": 0.8461538461538461
		}]
	},
	"780066": {
		"nik": "780066",
		"nama": "NADIAH LIESTYASARI",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8708333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9716599190283401,
			"nilai_komparatif_unit": 0.9768088777382559,
			"summary_team": 0.8461538461538461
		}]
	},
	"790001": {
		"nik": "790001",
		"nama": "TRIHADI SULISTIYANTOMO",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0546875000000004,
			"nilai_komparatif_unit": 1.0602764332090544,
			"summary_team": 0.9000000000000001
		}]
	},
	"790002": {
		"nik": "790002",
		"nama": "ANDI INDAH SARI A.P.",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9357599505052547,
			"nilai_komparatif_unit": 0.9407186703754358,
			"summary_team": 0.7692307692307692
		}]
	},
	"790010": {
		"nik": "790010",
		"nama": "ADI CAHYADI",
		"band": "III",
		"posisi": "MGR GOVERNMENT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564625850340167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0418525080955543,
			"nilai_komparatif_unit": 1.047373426928319,
			"summary_team": 0.8923076923076922
		}]
	},
	"790011": {
		"nik": "790011",
		"nama": "ROBERTH SAMILLY TINAMBUNAN",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8349206349206347,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0503071073413281,
			"nilai_komparatif_unit": 1.0558728282510055,
			"summary_team": 0.8769230769230769
		}]
	},
	"790028": {
		"nik": "790028",
		"nama": "MUHAMMAD NUR SYAHAD",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8316666666666652,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0174194542932036,
			"nilai_komparatif_unit": 1.0228108990345481,
			"summary_team": 0.8461538461538461
		}]
	},
	"790069": {
		"nik": "790069",
		"nama": "SARI WIMURTI",
		"band": "III",
		"posisi": "MGR GOVERNMENT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379084967320246,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0024960998439958,
			"nilai_komparatif_unit": 1.0078084636905051,
			"summary_team": 0.8400000000000001
		}]
	},
	"790083": {
		"nik": "790083",
		"nama": "MUHAMMAD CHAYRUDIN NOER",
		"band": "III",
		"posisi": "OSM REGIONAL WHOLESALE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8309523809523801,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9787965616045855,
			"nilai_komparatif_unit": 0.983983338358894,
			"summary_team": 0.8133333333333334
		}]
	},
	"790097": {
		"nik": "790097",
		"nama": "HANDRIL",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8460784313725501,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9928157589803,
			"nilai_komparatif_unit": 0.9980768253775382,
			"summary_team": 0.84
		}]
	},
	"790105": {
		"nik": "790105",
		"nama": "TUWO",
		"band": "III",
		"posisi": "MGR ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564625850340167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9340746624304969,
			"nilai_komparatif_unit": 0.9390244517288375,
			"summary_team": 0.7999999999999999
		}]
	},
	"800003": {
		"nik": "800003",
		"nama": "YULI UNTUNG SUBAGIO,S.KOM",
		"band": "III",
		"posisi": "MGR SOLUTION & SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8390804597701138,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9717597471022141,
			"nilai_komparatif_unit": 0.976909234814737,
			"summary_team": 0.8153846153846154
		}]
	},
	"800008": {
		"nik": "800008",
		"nama": "NIA ASDIANA",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8404761904761923,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0433645674438854,
			"nilai_komparatif_unit": 1.0488934988857934,
			"summary_team": 0.8769230769230769
		}]
	},
	"800016": {
		"nik": "800016",
		"nama": "MUSTIKA",
		"band": "III",
		"posisi": "MGR COLLECTION PARTNERSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8488888888888888,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9605316149818769,
			"nilai_komparatif_unit": 0.9656216032877196,
			"summary_team": 0.8153846153846154
		}]
	},
	"800018": {
		"nik": "800018",
		"nama": "AJI SETYO RAHARJO",
		"band": "III",
		"posisi": "MGR DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379084967320246,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9388455538221547,
			"nilai_komparatif_unit": 0.9438206247260285,
			"summary_team": 0.7866666666666667
		}]
	},
	"800025": {
		"nik": "800025",
		"nama": "RAHMAN",
		"band": "III",
		"posisi": "MGR FINANCE & PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8708333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9539933750460068,
			"nilai_komparatif_unit": 0.9590487163248331,
			"summary_team": 0.8307692307692308
		}]
	},
	"800031": {
		"nik": "800031",
		"nama": "HARIYADI. S.Kom",
		"band": "III",
		"posisi": "MGR BIDDING MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8390804597701138,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1001053740779783,
			"nilai_komparatif_unit": 1.1059349828091363,
			"summary_team": 0.923076923076923
		}]
	},
	"800037": {
		"nik": "800037",
		"nama": "AKHMAD ZAIMI",
		"band": "III",
		"posisi": "MGR CCAN & WAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8460784313725501,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1091897673589433,
			"nilai_komparatif_unit": 1.1150675155316818,
			"summary_team": 0.9384615384615385
		}]
	},
	"800051": {
		"nik": "800051",
		"nama": "KIKI MAULANA SULAIMAN",
		"band": "III",
		"posisi": "MGR CUSTOMER RELATIONSHIP MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0135746606334852,
			"nilai_komparatif_unit": 1.0189457312877503,
			"summary_team": 0.8615384615384616
		}]
	},
	"800052": {
		"nik": "800052",
		"nama": "DANI PRASETIAWAN",
		"band": "III",
		"posisi": "MGR IP & SERVICE NODE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.862585034013604,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.074992830513337,
			"nilai_komparatif_unit": 1.080689364443957,
			"summary_team": 0.9272727272727272
		}]
	},
	"800062": {
		"nik": "800062",
		"nama": "DWINANTO BASKORO",
		"band": "III",
		"posisi": "MGR SERVICE DELIVERY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8790960451977408,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0075284612559667,
			"nilai_komparatif_unit": 1.0128674922733825,
			"summary_team": 0.8857142857142856
		}]
	},
	"800117": {
		"nik": "800117",
		"nama": "MAHARANI RETNO WURYANINGRUM",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564625850340167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0586179507545632,
			"nilai_komparatif_unit": 1.0642277119593493,
			"summary_team": 0.9066666666666666
		}]
	},
	"810009": {
		"nik": "810009",
		"nama": "HAPPY YANUWARSO",
		"band": "III",
		"posisi": "MGR PERFORMANCE MGT & SLA MONITORING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8790960451977408,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9769393618629962,
			"nilai_komparatif_unit": 0.9821162970620463,
			"summary_team": 0.8588235294117645
		}]
	},
	"810010": {
		"nik": "810010",
		"nama": "FATMAWATY",
		"band": "III",
		"posisi": "MGR ACCESS NETWORK ELEMENT OM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8790960451977408,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9750275431509355,
			"nilai_komparatif_unit": 0.9801943473613378,
			"summary_team": 0.857142857142857
		}]
	},
	"810011": {
		"nik": "810011",
		"nama": "OSMAN NUR",
		"band": "III",
		"posisi": "MGR ACCESS SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7860082304526756,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9982279500604099,
			"nilai_komparatif_unit": 1.003517696397878,
			"summary_team": 0.7846153846153846
		}]
	},
	"810017": {
		"nik": "810017",
		"nama": "HALIS RIDHO SANJAYA",
		"band": "III",
		"posisi": "MGR PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.868292682926828,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.045375972342266,
			"nilai_komparatif_unit": 1.050915562493634,
			"summary_team": 0.9076923076923078
		}]
	},
	"810019": {
		"nik": "810019",
		"nama": "REYNOLD",
		"band": "III",
		"posisi": "MGR CCAN & WAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8725925925925916,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0225937051064398,
			"nilai_komparatif_unit": 1.0280125689100204,
			"summary_team": 0.8923076923076924
		}]
	},
	"810021": {
		"nik": "810021",
		"nama": "MOH. FIRAQ",
		"band": "III",
		"posisi": "MGR INFRA SERVICE DELIVERY & OPR SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8708333333333323,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9186602870813406,
			"nilai_komparatif_unit": 0.9235283934979882,
			"summary_team": 0.7999999999999999
		}]
	},
	"810033": {
		"nik": "810033",
		"nama": "SEPTIANA RAHMAYANTI",
		"band": "III",
		"posisi": "MGR FINANCIAL SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740731,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9984591679506946,
			"nilai_komparatif_unit": 1.0037501395433646,
			"summary_team": 0.8727272727272728
		}]
	},
	"810036": {
		"nik": "810036",
		"nama": "HARRY ARIYAMANSYAH",
		"band": "III",
		"posisi": "MGR TOP PRIORITY SOLUTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8470588235294104,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.030303030303032,
			"nilai_komparatif_unit": 1.0357627468744806,
			"summary_team": 0.8727272727272728
		}]
	},
	"810065": {
		"nik": "810065",
		"nama": "SRI MULYANTO",
		"band": "III",
		"posisi": "MGR REVENUE ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740731,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9152542372881369,
			"nilai_komparatif_unit": 0.9201042945814177,
			"summary_team": 0.8000000000000003
		}]
	},
	"820003": {
		"nik": "820003",
		"nama": "RISYANTHO VIADORES TANDIABANG",
		"band": "III",
		"posisi": "MGR ACCESS NEW FTTH & MODERNIZATION DEPL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8405797101449269,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.002709359605912,
			"nilai_komparatif_unit": 1.0080228535450464,
			"summary_team": 0.8428571428571427
		}]
	},
	"820026": {
		"nik": "820026",
		"nama": "ARDIAN AMPING",
		"band": "III",
		"posisi": "MGR CUSTOMER DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8313725490196076,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.027873070325901,
			"nilai_komparatif_unit": 1.0333199102073225,
			"summary_team": 0.8545454545454546
		}]
	},
	"820070": {
		"nik": "820070",
		"nama": "NURIL EKA NOORLIANA",
		"band": "III",
		"posisi": "MGR PARTNERSHIP MARKETING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8313725490196076,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9622641509433965,
			"nilai_komparatif_unit": 0.9673633201940889,
			"summary_team": 0.7999999999999999
		}]
	},
	"820072": {
		"nik": "820072",
		"nama": "TAUFIK NUGROHO",
		"band": "III",
		"posisi": "MGR ACCESS NETWORK ELEMENT OM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8470588235294104,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1079059829059845,
			"nilai_komparatif_unit": 1.1137769281294492,
			"summary_team": 0.9384615384615383
		}]
	},
	"830002": {
		"nik": "830002",
		"nama": "KARLINA RIVAI",
		"band": "III",
		"posisi": "MGR FINANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.973190348525465,
			"nilai_komparatif_unit": 0.9783474171904534,
			"summary_team": 0.8
		}]
	},
	"830006": {
		"nik": "830006",
		"nama": "ZAINAL ABIDIN",
		"band": "III",
		"posisi": "KAKANDATEL TANAHGROGOT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8493827160493849,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.130232558139532,
			"nilai_komparatif_unit": 1.1362218149365835,
			"summary_team": 0.96
		}]
	},
	"830007": {
		"nik": "830007",
		"nama": "AMIANTO, S.Kom",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8493827160493849,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0674418604651135,
			"nilai_komparatif_unit": 1.0730983807734398,
			"summary_team": 0.9066666666666666
		}]
	},
	"830008": {
		"nik": "830008",
		"nama": "AROFIK",
		"band": "III",
		"posisi": "MGR ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.868292682926828,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9390665514261033,
			"nilai_komparatif_unit": 0.9440427934264846,
			"summary_team": 0.8153846153846154
		}]
	},
	"830012": {
		"nik": "830012",
		"nama": "ASRUL SALIM",
		"band": "III",
		"posisi": "MGR CCAN & WAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8708333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.042326094957674,
			"nilai_komparatif_unit": 1.0478495233919471,
			"summary_team": 0.9076923076923077
		}]
	},
	"830021": {
		"nik": "830021",
		"nama": "MUHAMMAD NOOR",
		"band": "III",
		"posisi": "MGR ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8509803921568626,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9828236279849185,
			"nilai_komparatif_unit": 0.9880317447143648,
			"summary_team": 0.8363636363636364
		}]
	},
	"830022": {
		"nik": "830022",
		"nama": "HERMAN BATARI",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8708333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0564593301435408,
			"nilai_komparatif_unit": 1.0620576525226855,
			"summary_team": 0.9199999999999999
		}]
	},
	"830045": {
		"nik": "830045",
		"nama": "ALDA APRILIA ARUNG TASIK",
		"band": "III",
		"posisi": "MGR PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8404761904761923,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.878622793636956,
			"nilai_komparatif_unit": 0.8832787359038259,
			"summary_team": 0.7384615384615384
		}]
	},
	"830054": {
		"nik": "830054",
		"nama": "NUZUL NURWAHYUDDIN",
		"band": "III",
		"posisi": "KAKANDATEL MEMPAWAH",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564625850340167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9496425734710051,
			"nilai_komparatif_unit": 0.9546748592576515,
			"summary_team": 0.8133333333333332
		}]
	},
	"830057": {
		"nik": "830057",
		"nama": "AHBAN JACOB MUSU'",
		"band": "III",
		"posisi": "KAKANDATEL PALOPO",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8708333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9186602870813398,
			"nilai_komparatif_unit": 0.9235283934979874,
			"summary_team": 0.8
		}]
	},
	"830058": {
		"nik": "830058",
		"nama": "A. NUR QALBY",
		"band": "III",
		"posisi": "MGR QUALITY & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8380952380952381,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.083041958041958,
			"nilai_komparatif_unit": 1.088781145399445,
			"summary_team": 0.9076923076923077
		}]
	},
	"840002": {
		"nik": "840002",
		"nama": "ROHMAT HIDAYAT",
		"band": "III",
		"posisi": "MGR IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.862585034013604,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9987867022567353,
			"nilai_komparatif_unit": 1.0040794094984578,
			"summary_team": 0.8615384615384614
		}]
	},
	"840003": {
		"nik": "840003",
		"nama": "YUDI RAHMANI",
		"band": "III",
		"posisi": "MGR CCAN & WAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8358208955223883,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.104395604395604,
			"nilai_komparatif_unit": 1.1102479476435707,
			"summary_team": 0.9230769230769229
		}]
	},
	"840015": {
		"nik": "840015",
		"nama": "MAX AYUB WANGKO",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8724637681159438,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9169435215946824,
			"nilai_komparatif_unit": 0.921802530636382,
			"summary_team": 0.7999999999999999
		}]
	},
	"840016": {
		"nik": "840016",
		"nama": "KHAIRIL BAKRI",
		"band": "III",
		"posisi": "MGR BILLING & PAYMENT COLLECTION (EBIS)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740731,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9984591679506946,
			"nilai_komparatif_unit": 1.0037501395433646,
			"summary_team": 0.8727272727272728
		}]
	},
	"840017": {
		"nik": "840017",
		"nama": "FANUR",
		"band": "III",
		"posisi": "KAKANDATEL MAROS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9068364611260016,
			"nilai_komparatif_unit": 0.9116419114729225,
			"summary_team": 0.7454545454545455
		}]
	},
	"840020": {
		"nik": "840020",
		"nama": "HENDRA SATRIA NUSAPUTRA",
		"band": "III",
		"posisi": "MGR PERF MGT, SLA MONITORING&KPI REPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.862585034013604,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9063665041583039,
			"nilai_komparatif_unit": 0.9111694641390229,
			"summary_team": 0.7818181818181819
		}]
	},
	"840025": {
		"nik": "840025",
		"nama": "RAMDANITA, M.M",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8708333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0246595509753404,
			"nilai_komparatif_unit": 1.0300893619785243,
			"summary_team": 0.8923076923076922
		}]
	},
	"840026": {
		"nik": "840026",
		"nama": "QUDDUS",
		"band": "III",
		"posisi": "MGR DELIVERY & SALES SUPPORT BUSS SERV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8395833333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0261500286314191,
			"nilai_komparatif_unit": 1.0315877378794096,
			"summary_team": 0.8615384615384615
		}]
	},
	"840027": {
		"nik": "840027",
		"nama": "ERWAN SULISTYONO, SE",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8493827160493849,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0831395348837183,
			"nilai_komparatif_unit": 1.088879239314226,
			"summary_team": 0.92
		}]
	},
	"840033": {
		"nik": "840033",
		"nama": "FAUZIAH, M.M",
		"band": "III",
		"posisi": "MGR CHANNEL ACTIVATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8166666666666655,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.979591836734695,
			"nilai_komparatif_unit": 0.98478282776061,
			"summary_team": 0.7999999999999998
		}]
	},
	"840037": {
		"nik": "840037",
		"nama": "MUHAMMAD DIKA PRAYUDHA, S.E",
		"band": "III",
		"posisi": "MGR GOVERNMENT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8321285140562269,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.127784377784375,
			"nilai_komparatif_unit": 1.133760661339126,
			"summary_team": 0.9384615384615383
		}]
	},
	"840038": {
		"nik": "840038",
		"nama": "SUKMAWATY",
		"band": "III",
		"posisi": "MGR NTE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8790960451977408,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9912780022034511,
			"nilai_komparatif_unit": 0.99653091981736,
			"summary_team": 0.8714285714285712
		}]
	},
	"840039": {
		"nik": "840039",
		"nama": "KHAIRAN NOOR",
		"band": "III",
		"posisi": "MGR INFRA MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.862585034013604,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0117579581301996,
			"nilai_komparatif_unit": 1.0171194018296068,
			"summary_team": 0.8727272727272727
		}]
	},
	"840048": {
		"nik": "840048",
		"nama": "AGUS BUDI SANTOSO, S.T",
		"band": "III",
		"posisi": "MGR NETWORK DEPLOYMENT & PROJECT SUPERVI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.862585034013604,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9709187697160899,
			"nilai_komparatif_unit": 0.9760638009745054,
			"summary_team": 0.8374999999999999
		}]
	},
	"840083": {
		"nik": "840083",
		"nama": "FIRMAN HIDAYAT SAYID MUHAMMAD, S.T.,M.M.",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & CONSTRUCTION SPV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.868292682926828,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0134831460674172,
			"nilai_komparatif_unit": 1.0188537317734891,
			"summary_team": 0.88
		}]
	},
	"840107": {
		"nik": "840107",
		"nama": "NOOR FAJAR RIDWAN",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8460784313725501,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9612977983777508,
			"nilai_komparatif_unit": 0.9663918467941243,
			"summary_team": 0.8133333333333334
		}]
	},
	"840179": {
		"nik": "840179",
		"nama": "WAHYU MUHAMMAD IQBAL",
		"band": "III",
		"posisi": "MGR ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8493827160493849,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9780858676207488,
			"nilai_komparatif_unit": 0.9832688783105048,
			"summary_team": 0.8307692307692307
		}]
	},
	"850001": {
		"nik": "850001",
		"nama": "FRANSISCUS ANDRE DESAN",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8584615384615386,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0415348935273034,
			"nilai_komparatif_unit": 1.0470541292770619,
			"summary_team": 0.8941176470588237
		}]
	},
	"850003": {
		"nik": "850003",
		"nama": "AMRIAH. A",
		"band": "III",
		"posisi": "MGR GOVERNMENT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8404761904761923,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.038372392480039,
			"nilai_komparatif_unit": 1.0438748697045217,
			"summary_team": 0.8727272727272727
		}]
	},
	"850005": {
		"nik": "850005",
		"nama": "DWI HARIYANTO",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564625850340167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9520376367080065,
			"nilai_komparatif_unit": 0.9570826142620845,
			"summary_team": 0.8153846153846153
		}]
	},
	"850022": {
		"nik": "850022",
		"nama": "GATRA DI YOGA TAMA",
		"band": "III",
		"posisi": "KAKANDATEL PANGKALANBUN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8358208955223883,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9890476190476187,
			"nilai_komparatif_unit": 0.9942887175563534,
			"summary_team": 0.8266666666666665
		}]
	},
	"850039": {
		"nik": "850039",
		"nama": "ISKANDAR ZULKARNAIN",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8321285140562269,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.993436293436291,
			"nilai_komparatif_unit": 0.9987006481304214,
			"summary_team": 0.8266666666666667
		}]
	},
	"850043": {
		"nik": "850043",
		"nama": "ASRUL SANI, ST",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8708333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9492822966507178,
			"nilai_komparatif_unit": 0.9543126732812537,
			"summary_team": 0.8266666666666667
		}]
	},
	"850044": {
		"nik": "850044",
		"nama": "NAMUS AKBAR",
		"band": "III",
		"posisi": "MGR WIFI FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8790960451977408,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0237789203084824,
			"nilai_komparatif_unit": 1.0292040647294047,
			"summary_team": 0.8999999999999998
		}]
	},
	"850045": {
		"nik": "850045",
		"nama": "ADIL",
		"band": "III",
		"posisi": "MGR FULFILLMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8790960451977408,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0325291674906063,
			"nilai_komparatif_unit": 1.038000680667263,
			"summary_team": 0.9076923076923077
		}]
	},
	"850047": {
		"nik": "850047",
		"nama": "MUHAMMAD IDHAM",
		"band": "III",
		"posisi": "MGR ACCESS DATA MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0293359455557802,
			"nilai_komparatif_unit": 1.0347905374129793,
			"summary_team": 0.8461538461538461
		}]
	},
	"850048": {
		"nik": "850048",
		"nama": "DEDY ISNANDAR",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8429378531073439,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9648793565683655,
			"nilai_komparatif_unit": 0.9699923841510909,
			"summary_team": 0.8133333333333335
		}]
	},
	"850049": {
		"nik": "850049",
		"nama": "CHRISTIAN ARMAN LA'LANG",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & CONSTRUCTION SPV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0867292225201024,
			"nilai_komparatif_unit": 1.092487949196006,
			"summary_team": 0.8933333333333332
		}]
	},
	"850051": {
		"nik": "850051",
		"nama": "ANDI ADI SENOPATI TEMMATTUMPA",
		"band": "III",
		"posisi": "KAKANDATEL BONE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9596738159070559,
			"nilai_komparatif_unit": 0.9647592586183638,
			"summary_team": 0.788888888888889
		}]
	},
	"850052": {
		"nik": "850052",
		"nama": "MARKUS TABA TANGNGA",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8220385674931164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0293359455557802,
			"nilai_komparatif_unit": 1.0347905374129793,
			"summary_team": 0.8461538461538461
		}]
	},
	"850053": {
		"nik": "850053",
		"nama": "IQBAL KURNIAWAN",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8584615384615386,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9498207885304658,
			"nilai_komparatif_unit": 0.9548540187030693,
			"summary_team": 0.8153846153846154
		}]
	},
	"850055": {
		"nik": "850055",
		"nama": "ANDI ABDUL WARIS",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8725925925925916,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8658743633276752,
			"nilai_komparatif_unit": 0.8704627499199791,
			"summary_team": 0.7555555555555558
		}]
	},
	"850059": {
		"nik": "850059",
		"nama": "ZAHRATUL ULYA",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8493827160493849,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9632663847780103,
			"nilai_komparatif_unit": 0.96837086500277,
			"summary_team": 0.8181818181818182
		}]
	},
	"850071": {
		"nik": "850071",
		"nama": "ANDRI DWI ASTUTI",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564625850340167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9340746624304969,
			"nilai_komparatif_unit": 0.9390244517288375,
			"summary_team": 0.7999999999999999
		}]
	},
	"850162": {
		"nik": "850162",
		"nama": "RASMAYANA RAZAK",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8321285140562269,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.961389961389959,
			"nilai_komparatif_unit": 0.9664844981907303,
			"summary_team": 0.7999999999999999
		}]
	},
	"850181": {
		"nik": "850181",
		"nama": "CITRA RAMADHENA",
		"band": "III",
		"posisi": "MGR WHOLESALE & INTERNATIONAL MARKETING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8142857142857147,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9824561403508765,
			"nilai_komparatif_unit": 0.9876623097131245,
			"summary_team": 0.7999999999999998
		}]
	},
	"860008": {
		"nik": "860008",
		"nama": "MUHAMMAD JAUHARIANSYAH",
		"band": "III",
		"posisi": "MGR CCAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8321285140562269,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9798782298782274,
			"nilai_komparatif_unit": 0.9850707385405519,
			"summary_team": 0.8153846153846153
		}]
	},
	"860033": {
		"nik": "860033",
		"nama": "MUHAMMAD KURNIAWAN IDRUS",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8349206349206347,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.990114068441065,
			"nilai_komparatif_unit": 0.9953608182108895,
			"summary_team": 0.8266666666666668
		}]
	},
	"860036": {
		"nik": "860036",
		"nama": "MUHAMMAD HIDAYAT",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8708333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9539933750460068,
			"nilai_komparatif_unit": 0.9590487163248331,
			"summary_team": 0.8307692307692308
		}]
	},
	"860040": {
		"nik": "860040",
		"nama": "SUNARTO NOTO BUWONO",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8429378531073439,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9706312454301738,
			"nilai_komparatif_unit": 0.9757747530581016,
			"summary_team": 0.8181818181818181
		}]
	},
	"860042": {
		"nik": "860042",
		"nama": "YAN SALBIN SYARIF",
		"band": "III",
		"posisi": "MGR BACKBONE & CME OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8585858585858576,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9983193277310934,
			"nilai_komparatif_unit": 1.0036095582913274,
			"summary_team": 0.857142857142857
		}]
	},
	"860043": {
		"nik": "860043",
		"nama": "STEVEN HERMAWAN LEXY SAMOLA",
		"band": "III",
		"posisi": "KAKANDATEL KOLAKA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8429378531073439,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.976975240498345,
			"nilai_komparatif_unit": 0.9821523658231874,
			"summary_team": 0.823529411764706
		}]
	},
	"860046": {
		"nik": "860046",
		"nama": "ZAINUL TOYYIB",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8316666666666652,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9064282410975814,
			"nilai_komparatif_unit": 0.9112315282307792,
			"summary_team": 0.7538461538461538
		}]
	},
	"860049": {
		"nik": "860049",
		"nama": "PUTRI APRILLIANI",
		"band": "III",
		"posisi": "MGR CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481477,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9646685192536727,
			"nilai_komparatif_unit": 0.9697804295806518,
			"summary_team": 0.8181818181818182
		}]
	},
	"860052": {
		"nik": "860052",
		"nama": "AFFANDHI ARIEF",
		"band": "III",
		"posisi": "MGR ACCESS OM & PARTNERSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8470588235294104,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.944444444444446,
			"nilai_komparatif_unit": 0.9494491846349404,
			"summary_team": 0.8
		}]
	},
	"860075": {
		"nik": "860075",
		"nama": "RAYZA ARDIAN BAHRI",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8321285140562269,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9613899613899594,
			"nilai_komparatif_unit": 0.9664844981907307,
			"summary_team": 0.8000000000000003
		}]
	},
	"860083": {
		"nik": "860083",
		"nama": "YAN ALFI TUFEN",
		"band": "III",
		"posisi": "MGR ACCESS DATA & INVENTORY MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8470588235294104,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0088383838383854,
			"nilai_komparatif_unit": 1.0141843563145954,
			"summary_team": 0.8545454545454546
		}]
	},
	"860089": {
		"nik": "860089",
		"nama": "RATNA EKASARI PRIHANDINI",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379084967320246,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.973118924756992,
			"nilai_komparatif_unit": 0.9782756149376695,
			"summary_team": 0.8153846153846154
		}]
	},
	"860113": {
		"nik": "860113",
		"nama": "DIDIK KURNIAWAN HADI",
		"band": "III",
		"posisi": "MGR MARKETING & ACCOUNT TEAM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8390804597701138,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9216438356164395,
			"nilai_komparatif_unit": 0.926527752264543,
			"summary_team": 0.7733333333333332
		}]
	},
	"860159": {
		"nik": "860159",
		"nama": "FEBRIANTY",
		"band": "III",
		"posisi": "MGR SALES PROMOTION & PRICING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8166666666666655,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.979591836734695,
			"nilai_komparatif_unit": 0.98478282776061,
			"summary_team": 0.7999999999999998
		}]
	},
	"860167": {
		"nik": "860167",
		"nama": "SINTA WARDANI",
		"band": "III",
		"posisi": "MGR PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8493827160493849,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9575581395348813,
			"nilai_komparatif_unit": 0.9626323709879387,
			"summary_team": 0.8133333333333334
		}]
	},
	"870035": {
		"nik": "870035",
		"nama": "CHRISMAYANTI LILING PADANG",
		"band": "III",
		"posisi": "MGR ACCESS CAPEX QE & PERFORMANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8790960451977408,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.992755316662771,
			"nilai_komparatif_unit": 0.9980160627679078,
			"summary_team": 0.8727272727272727
		}]
	},
	"870038": {
		"nik": "870038",
		"nama": "BETA YUNASWATI",
		"band": "III",
		"posisi": "MGR SOLUTION & SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8395833333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9895018133231542,
			"nilai_komparatif_unit": 0.9947453186694307,
			"summary_team": 0.8307692307692308
		}]
	},
	"870039": {
		"nik": "870039",
		"nama": "CHARISMA",
		"band": "III",
		"posisi": "MGR TOP PRIORITY SOLUTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8790960451977408,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.040029379360998,
			"nilai_komparatif_unit": 1.045540637185427,
			"summary_team": 0.9142857142857141
		}]
	},
	"870048": {
		"nik": "870048",
		"nama": "ELVA APULINA BR SITEPU",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8493827160493849,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9732558139534859,
			"nilai_komparatif_unit": 0.9784132295287246,
			"summary_team": 0.8266666666666667
		}]
	},
	"880010": {
		"nik": "880010",
		"nama": "ADIL JUNAPA",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8724637681159438,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9627906976744166,
			"nilai_komparatif_unit": 0.967892657168201,
			"summary_team": 0.84
		}]
	},
	"880072": {
		"nik": "880072",
		"nama": "GDE ADHITYA PRABHAWA",
		"band": "III",
		"posisi": "MGR PROJECT MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8390804597701138,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9750933997509352,
			"nilai_komparatif_unit": 0.9802605529444617,
			"summary_team": 0.8181818181818181
		}]
	}
};