export const cbark23 = {
	"625405": {
		"nik": "625405",
		"nama": "R. BAMBANG TRIWIDIATMOKO",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7333333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8522727272727275,
			"nilai_komparatif_unit": 0.8527904747450487,
			"summary_team": 0.6250000000000001
		}]
	},
	"625406": {
		"nik": "625406",
		"nama": "AJANG ISMAR",
		"band": "V",
		"posisi": "OFF 2 CONSUMER ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7777777777777782,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.028571428571428,
			"nilai_komparatif_unit": 1.0291962758065951,
			"summary_team": 0.8
		}]
	},
	"625407": {
		"nik": "625407",
		"nama": "WAHYONO",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666664,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090913,
			"nilai_komparatif_unit": 1.0915718076736625,
			"summary_team": 0.9454545454545454
		}]
	},
	"625408": {
		"nik": "625408",
		"nama": "AWALUDDIN",
		"band": "V",
		"posisi": "OFF 2 LOGISTIK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7722222222222225,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8947024198822757,
			"nilai_komparatif_unit": 0.8952459429841542,
			"summary_team": 0.6909090909090909
		}]
	},
	"635390": {
		"nik": "635390",
		"nama": "DJOKO SUSILO IRIANTO",
		"band": "V",
		"posisi": "OFF 2 PLASA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139859,
			"nilai_komparatif_unit": 0.9866129800127329,
			"summary_team": 0.8545454545454546
		}]
	},
	"635642": {
		"nik": "635642",
		"nama": "HADI WIRATMO",
		"band": "V",
		"posisi": "OFF 2 ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7722222222222225,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.106605624591236,
			"nilai_komparatif_unit": 1.1072778768488225,
			"summary_team": 0.8545454545454546
		}]
	},
	"635643": {
		"nik": "635643",
		"nama": "ANIK SUHARDINI",
		"band": "V",
		"posisi": "OFF 2 SERVICE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8541666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0855875831485586,
			"nilai_komparatif_unit": 1.0862470671484246,
			"summary_team": 0.9272727272727274
		}]
	},
	"635644": {
		"nik": "635644",
		"nama": "SUHENDRI",
		"band": "V",
		"posisi": "OFF 2 BS TERRITORY SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9583687340696684,
			"nilai_komparatif_unit": 0.9589509338441514,
			"summary_team": 0.8545454545454546
		}]
	},
	"660493": {
		"nik": "660493",
		"nama": "AGUS UDAJAT",
		"band": "V",
		"posisi": "OFF 2 ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0695187165775402,
			"nilai_komparatif_unit": 1.070168438895747,
			"summary_team": 0.9090909090909092
		}]
	},
	"660495": {
		"nik": "660495",
		"nama": "MOH. ICHWANI",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT PLAN & RVW",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.962566844919786,
			"nilai_komparatif_unit": 0.9631515950061722,
			"summary_team": 0.8181818181818182
		}]
	},
	"660509": {
		"nik": "660509",
		"nama": "SUTOPO",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9212121212121214,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0855263157894737,
			"nilai_komparatif_unit": 1.086185762570009,
			"summary_team": 1.0000000000000002
		}]
	},
	"660512": {
		"nik": "660512",
		"nama": "ABDUL RAHMAN",
		"band": "V",
		"posisi": "OFF 2 NETWORK PERF, QM & ADMIN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7722222222222226,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9417920209287112,
			"nilai_komparatif_unit": 0.9423641505096358,
			"summary_team": 0.7272727272727273
		}]
	},
	"660530": {
		"nik": "660530",
		"nama": "SUWARDI",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8266666666666661,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0557184750733146,
			"nilai_komparatif_unit": 1.0563598138777384,
			"summary_team": 0.8727272727272728
		}]
	},
	"660537": {
		"nik": "660537",
		"nama": "SUKATMA",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.804761904761904,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9940828402366874,
			"nilai_komparatif_unit": 0.9946867359866516,
			"summary_team": 0.8
		}]
	},
	"660543": {
		"nik": "660543",
		"nama": "ITTAS ALNI",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.75,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0181818181818183,
			"nilai_komparatif_unit": 1.0188003538287513,
			"summary_team": 0.7636363636363637
		}]
	},
	"660555": {
		"nik": "660555",
		"nama": "YEDI RUSTENDI",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346936,
			"nilai_komparatif_unit": 0.9801869293396146,
			"summary_team": 0.8
		}]
	},
	"660558": {
		"nik": "660558",
		"nama": "JOSEP KANTOHE",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.013489736070381,
			"nilai_komparatif_unit": 1.0141054213226277,
			"summary_team": 0.8727272727272728
		}]
	},
	"660566": {
		"nik": "660566",
		"nama": "SUPNADIN",
		"band": "V",
		"posisi": "OFF 2 SECURITY & SAFETY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9720972097209719,
			"nilai_komparatif_unit": 0.972687749412174,
			"summary_team": 0.8181818181818182
		}]
	},
	"660567": {
		"nik": "660567",
		"nama": "AGUS RAHMAT",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9720972097209719,
			"nilai_komparatif_unit": 0.972687749412174,
			"summary_team": 0.8181818181818182
		}]
	},
	"660569": {
		"nik": "660569",
		"nama": "MAULANA",
		"band": "V",
		"posisi": "OFF 2 BS DELIVERY & QUALITY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.914285714285714,
			"nilai_komparatif_unit": 0.914841134050307,
			"summary_team": 0.8
		}]
	},
	"660616": {
		"nik": "660616",
		"nama": "HERMAN",
		"band": "V",
		"posisi": "OFF 2 CONSUMER ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8958333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9539112050739961,
			"nilai_komparatif_unit": 0.9544906969425513,
			"summary_team": 0.8545454545454546
		}]
	},
	"660619": {
		"nik": "660619",
		"nama": "GUSTI AMPERYADI PUTERA KASUMA",
		"band": "V",
		"posisi": "OFF 2 CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.03030303030303,
			"nilai_komparatif_unit": 1.0309289294695694,
			"summary_team": 0.9272727272727272
		}]
	},
	"660625": {
		"nik": "660625",
		"nama": "SUWAJI",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9801869293396148,
			"summary_team": 0.8
		}]
	},
	"660637": {
		"nik": "660637",
		"nama": "MOHAMAD MAKSUM",
		"band": "V",
		"posisi": "OFF 2 ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7722222222222225,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9653368214519291,
			"nilai_komparatif_unit": 0.9659232542723769,
			"summary_team": 0.7454545454545455
		}]
	},
	"660642": {
		"nik": "660642",
		"nama": "SUHANDA",
		"band": "V",
		"posisi": "OFF 2 IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559437,
			"nilai_komparatif_unit": 0.9446294489483611,
			"summary_team": 0.8181818181818182
		}]
	},
	"660643": {
		"nik": "660643",
		"nama": "AHMAD KARNOTO",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144382,
			"nilai_komparatif_unit": 1.0273617013399168,
			"summary_team": 0.8727272727272727
		}]
	},
	"670328": {
		"nik": "670328",
		"nama": "ZAHIRLINA",
		"band": "V",
		"posisi": "SPV PLASA YOS SUDARSO",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9532215357458075,
			"nilai_komparatif_unit": 0.9538006086468891,
			"summary_team": 0.8181818181818181
		}]
	},
	"670371": {
		"nik": "670371",
		"nama": "ENDANG CARLIA",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7000000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9610389610389609,
			"nilai_komparatif_unit": 0.9616227829506069,
			"summary_team": 0.6727272727272727
		}]
	},
	"670376": {
		"nik": "670376",
		"nama": "MUHAMMAD NUH",
		"band": "V",
		"posisi": "OFF 2 BS TERRITORY SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.98989898989899,
			"nilai_komparatif_unit": 0.9905003440001748,
			"summary_team": 0.8909090909090909
		}]
	},
	"670384": {
		"nik": "670384",
		"nama": "SURANTO",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8541666666666664,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9365853658536589,
			"nilai_komparatif_unit": 0.9371543324417785,
			"summary_team": 0.8
		}]
	},
	"670401": {
		"nik": "670401",
		"nama": "SARYANTO",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666664,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769235,
			"nilai_komparatif_unit": 0.9236376834161759,
			"summary_team": 0.8
		}]
	},
	"670402": {
		"nik": "670402",
		"nama": "SETYO JOKO SUSILO",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559437,
			"nilai_komparatif_unit": 0.9446294489483611,
			"summary_team": 0.8181818181818182
		}]
	},
	"670420": {
		"nik": "670420",
		"nama": "FARIED",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9125,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.016189290161893,
			"nilai_komparatif_unit": 1.0168066153672468,
			"summary_team": 0.9272727272727274
		}]
	},
	"670421": {
		"nik": "670421",
		"nama": "IRWAN SANUSI",
		"band": "V",
		"posisi": "OFF 2 ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.75,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.993939393939394,
			"nilai_komparatif_unit": 0.9945432025471144,
			"summary_team": 0.7454545454545455
		}]
	},
	"670422": {
		"nik": "670422",
		"nama": "KHOLID",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0703259005145795,
			"nilai_komparatif_unit": 1.070976113189253,
			"summary_team": 0.9454545454545454
		}]
	},
	"670426": {
		"nik": "670426",
		"nama": "MOCHAMAD DAUD",
		"band": "V",
		"posisi": "OFF 2 HUMAN CAPITAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8303030303030305,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8759124087591239,
			"nilai_komparatif_unit": 0.8764445171102394,
			"summary_team": 0.7272727272727273
		}]
	},
	"670430": {
		"nik": "670430",
		"nama": "UJANG SUPRIATNA",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8266666666666661,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8357771260997073,
			"nilai_komparatif_unit": 0.8362848526532094,
			"summary_team": 0.6909090909090909
		}]
	},
	"670431": {
		"nik": "670431",
		"nama": "SONI RUSPANDI",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197862,
			"nilai_komparatif_unit": 0.9631515950061724,
			"summary_team": 0.8181818181818182
		}]
	},
	"670436": {
		"nik": "670436",
		"nama": "ZAENAL ARIFIN",
		"band": "V",
		"posisi": "OFF 2 SERVICE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9175870858113849,
			"nilai_komparatif_unit": 0.9181445111273794,
			"summary_team": 0.8181818181818182
		}]
	},
	"670440": {
		"nik": "670440",
		"nama": "MUHAMMAD NAHARTO",
		"band": "V",
		"posisi": "OFF 2 BS SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9801869293396148,
			"summary_team": 0.8
		}]
	},
	"670443": {
		"nik": "670443",
		"nama": "SUTIAWANDI",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0006074903675237,
			"summary_team": 0.8
		}]
	},
	"670464": {
		"nik": "670464",
		"nama": "ACHRUDIN",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ JATINEGARA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.083825265643447,
			"nilai_komparatif_unit": 1.0844836790524042,
			"summary_team": 0.9272727272727272
		}]
	},
	"670466": {
		"nik": "670466",
		"nama": "HAIRUL ANWAR",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969697,
			"nilai_komparatif_unit": 0.9702860512654774,
			"summary_team": 0.8727272727272728
		}]
	},
	"670470": {
		"nik": "670470",
		"nama": "RASYID",
		"band": "V",
		"posisi": "OFF 2 WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346935,
			"nilai_komparatif_unit": 0.9801869293396145,
			"summary_team": 0.8
		}]
	},
	"670471": {
		"nik": "670471",
		"nama": "SUHANDI",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9262435677530018,
			"nilai_komparatif_unit": 0.9268062517983924,
			"summary_team": 0.8181818181818182
		}]
	},
	"670478": {
		"nik": "670478",
		"nama": "INDARTO",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0603228547153778,
			"nilai_komparatif_unit": 1.0609669906360824,
			"summary_team": 0.9454545454545454
		}]
	},
	"670484": {
		"nik": "670484",
		"nama": "HERMAN PERMANA",
		"band": "V",
		"posisi": "OFF 2 SERVICE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365351,
			"nilai_komparatif_unit": 0.9885933352516183,
			"summary_team": 0.8727272727272727
		}]
	},
	"670488": {
		"nik": "670488",
		"nama": "SUHERMAN",
		"band": "V",
		"posisi": "OFF 2 SECURITY & SAFETY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.015301530153015,
			"nilai_komparatif_unit": 1.015918316052715,
			"summary_team": 0.8545454545454546
		}]
	},
	"670517": {
		"nik": "670517",
		"nama": "SUGENG SUPONO",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0006074903675237,
			"summary_team": 0.8
		}]
	},
	"670522": {
		"nik": "670522",
		"nama": "SUMANTO",
		"band": "V",
		"posisi": "OFF 2 IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.027361701339917,
			"summary_team": 0.8727272727272727
		}]
	},
	"670526": {
		"nik": "670526",
		"nama": "GUNADI WINARNO",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9381818181818178,
			"nilai_komparatif_unit": 0.938751754599349,
			"summary_team": 0.7818181818181817
		}]
	},
	"670538": {
		"nik": "670538",
		"nama": "SANTO",
		"band": "V",
		"posisi": "OFF 2 MICRO DEMAND",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8722222222222225,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1256514186450488,
			"nilai_komparatif_unit": 1.1263352410390648,
			"summary_team": 0.9818181818181818
		}]
	},
	"670543": {
		"nik": "670543",
		"nama": "SUPRAPTONO",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9974025974025974,
			"nilai_komparatif_unit": 0.9980085098730624,
			"summary_team": 0.8727272727272728
		}]
	},
	"670547": {
		"nik": "670547",
		"nama": "SUWANDI",
		"band": "V",
		"posisi": "OFF 2 PROJECT DELIVERY & SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8541666666666665,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0217294900221732,
			"nilai_komparatif_unit": 1.0223501808455764,
			"summary_team": 0.8727272727272728
		}]
	},
	"670548": {
		"nik": "670548",
		"nama": "EDI SUWARNO",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346935,
			"nilai_komparatif_unit": 0.9801869293396145,
			"summary_team": 0.8
		}]
	},
	"670554": {
		"nik": "670554",
		"nama": "TETI HERAWATI",
		"band": "V",
		"posisi": "OFF 2 ADMINISTRASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181818,
			"nilai_komparatif_unit": 0.9824146269062958,
			"summary_team": 0.8181818181818182
		}]
	},
	"670561": {
		"nik": "670561",
		"nama": "SAMSURI",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT PLAN & RVW",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.75,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969697,
			"nilai_komparatif_unit": 0.9702860512654774,
			"summary_team": 0.7272727272727273
		}]
	},
	"670562": {
		"nik": "670562",
		"nama": "ISAP SAPARI",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090908,
			"nilai_komparatif_unit": 1.091571807673662,
			"summary_team": 0.9272727272727272
		}]
	},
	"670566": {
		"nik": "670566",
		"nama": "AHMAD YANI",
		"band": "V",
		"posisi": "OFF 2 LOGISTIK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9284332688588008,
			"nilai_komparatif_unit": 0.928997283126521,
			"summary_team": 0.7272727272727273
		}]
	},
	"670572": {
		"nik": "670572",
		"nama": "MUHAMAD SAFRI",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9600000000000003,
			"nilai_komparatif_unit": 0.960583190752823,
			"summary_team": 0.8
		}]
	},
	"670582": {
		"nik": "670582",
		"nama": "ADI WIBAWA",
		"band": "V",
		"posisi": "OFF 2 ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090908,
			"nilai_komparatif_unit": 1.091571807673662,
			"summary_team": 0.9818181818181818
		}]
	},
	"670588": {
		"nik": "670588",
		"nama": "YUDI",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9284332688588008,
			"nilai_komparatif_unit": 0.928997283126521,
			"summary_team": 0.7272727272727273
		}]
	},
	"670595": {
		"nik": "670595",
		"nama": "JENNY SULASTRI",
		"band": "V",
		"posisi": "SPV PLASA CIBINONG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7583333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9950049950049948,
			"nilai_komparatif_unit": 0.9956094509550982,
			"summary_team": 0.7545454545454545
		}]
	},
	"670599": {
		"nik": "670599",
		"nama": "RUDY HARTONO",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888893,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.104545454545454,
			"nilai_komparatif_unit": 1.1052164552695822,
			"summary_team": 0.9818181818181818
		}]
	},
	"670600": {
		"nik": "670600",
		"nama": "SLAMET SUDIONO",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8722222222222226,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.979733642154024,
			"nilai_komparatif_unit": 0.9803288209043713,
			"summary_team": 0.8545454545454546
		}]
	},
	"670601": {
		"nik": "670601",
		"nama": "MOHAMMAD TAFDIL",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8388888888888881,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0186634557495495,
			"nilai_komparatif_unit": 1.0192822839866655,
			"summary_team": 0.8545454545454546
		}]
	},
	"680113": {
		"nik": "680113",
		"nama": "SRI SUNARYO",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9125,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0361145703611456,
			"nilai_komparatif_unit": 1.0367439999822907,
			"summary_team": 0.9454545454545454
		}]
	},
	"680185": {
		"nik": "680185",
		"nama": "RADEN GOTOT SUSASMINTO",
		"band": "V",
		"posisi": "OFF 2 HUMAN CAPITAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9290322580645158,
			"nilai_komparatif_unit": 0.9295966362124086,
			"summary_team": 0.8
		}]
	},
	"680188": {
		"nik": "680188",
		"nama": "SUPRAPTO",
		"band": "V",
		"posisi": "OFF 2 BS DELIVERY & QUALITY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9974025974025971,
			"nilai_komparatif_unit": 0.9980085098730621,
			"summary_team": 0.8727272727272728
		}]
	},
	"680192": {
		"nik": "680192",
		"nama": "SUTARNO WIJAYA",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9250000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0614250614250613,
			"nilai_komparatif_unit": 1.0620698669257251,
			"summary_team": 0.9818181818181818
		}]
	},
	"680196": {
		"nik": "680196",
		"nama": "JATI DARMADI",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ CAWANG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8541666666666664,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0430155210643017,
			"nilai_komparatif_unit": 1.0436491429465258,
			"summary_team": 0.8909090909090909
		}]
	},
	"680200": {
		"nik": "680200",
		"nama": "RUSLAN",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090906,
			"nilai_komparatif_unit": 1.0915718076736618,
			"summary_team": 0.9818181818181818
		}]
	},
	"680215": {
		"nik": "680215",
		"nama": "AKHMAD RUSTANDI",
		"band": "V",
		"posisi": "OFF 2 WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7777777777777782,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649345,
			"nilai_komparatif_unit": 0.9356329780059954,
			"summary_team": 0.7272727272727273
		}]
	},
	"680220": {
		"nik": "680220",
		"nama": "BENNY YUNIAN EFFENDI",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8722222222222226,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.979733642154024,
			"nilai_komparatif_unit": 0.9803288209043713,
			"summary_team": 0.8545454545454546
		}]
	},
	"680221": {
		"nik": "680221",
		"nama": "BUDI HERTANTO",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7944444444444448,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0985378258105525,
			"nilai_komparatif_unit": 1.0992051769580926,
			"summary_team": 0.8727272727272727
		}]
	},
	"680230": {
		"nik": "680230",
		"nama": "LILI SUTARYONO",
		"band": "V",
		"posisi": "OFF 2 ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8356164383561654,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1967213114754085,
			"nilai_komparatif_unit": 1.19744830814474,
			"summary_team": 1
		}]
	},
	"680233": {
		"nik": "680233",
		"nama": "MOHAMAD NURSUSILO",
		"band": "V",
		"posisi": "OFF 2 NETWORK QUALITY MGT & ADMIN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0454545454545454,
			"nilai_komparatif_unit": 1.0460896490205926,
			"summary_team": 0.8363636363636363
		}]
	},
	"680290": {
		"nik": "680290",
		"nama": "EDDY SUPRIYADI",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.971900826446281,
			"nilai_komparatif_unit": 0.9724912468365352,
			"summary_team": 0.8909090909090909
		}]
	},
	"680291": {
		"nik": "680291",
		"nama": "EDIYANTO",
		"band": "V",
		"posisi": "OFF 2 BGES BIDDING MGT & OBL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0115702479338844,
			"nilai_komparatif_unit": 1.0121847671155777,
			"summary_team": 0.9272727272727274
		}]
	},
	"680292": {
		"nik": "680292",
		"nama": "MISTA",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9650349650349651,
			"nilai_komparatif_unit": 0.9656212144805473,
			"summary_team": 0.8363636363636363
		}]
	},
	"680294": {
		"nik": "680294",
		"nama": "SUPARNO",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8388888888888881,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9536423841059611,
			"nilai_komparatif_unit": 0.9542217126683676,
			"summary_team": 0.8
		}]
	},
	"680295": {
		"nik": "680295",
		"nama": "SUPENO",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.944055944055944,
			"nilai_komparatif_unit": 0.9446294489483613,
			"summary_team": 0.8181818181818182
		}]
	},
	"680296": {
		"nik": "680296",
		"nama": "TABRANI",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8041666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0626471973622234,
			"nilai_komparatif_unit": 1.0632927452986969,
			"summary_team": 0.8545454545454546
		}]
	},
	"680312": {
		"nik": "680312",
		"nama": "TRI JAYA ISKANDAR",
		"band": "V",
		"posisi": "OFF 2 CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8083333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9222118088097467,
			"nilai_komparatif_unit": 0.9227720436004151,
			"summary_team": 0.7454545454545455
		}]
	},
	"680313": {
		"nik": "680313",
		"nama": "AGUS MUHIDIN",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8722222222222225,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.958888245512449,
			"nilai_komparatif_unit": 0.9594707608851294,
			"summary_team": 0.8363636363636363
		}]
	},
	"680330": {
		"nik": "680330",
		"nama": "SUNARYO",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.015301530153015,
			"nilai_komparatif_unit": 1.015918316052715,
			"summary_team": 0.8545454545454546
		}]
	},
	"680367": {
		"nik": "680367",
		"nama": "ENDANG SUHENDANG",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9674099485420238,
			"nilai_komparatif_unit": 0.9679976407672095,
			"summary_team": 0.8545454545454546
		}]
	},
	"680370": {
		"nik": "680370",
		"nama": "DENI ISKANDAR",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9212121212121214,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006578947368421,
			"nilai_komparatif_unit": 1.0071904343830993,
			"summary_team": 0.9272727272727274
		}]
	},
	"680373": {
		"nik": "680373",
		"nama": "AGUS KUSNADI",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090906,
			"nilai_komparatif_unit": 1.0915718076736618,
			"summary_team": 0.9272727272727272
		}]
	},
	"680389": {
		"nik": "680389",
		"nama": "PENCAR SUBAGIYO",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8266666666666661,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0557184750733144,
			"nilai_komparatif_unit": 1.0563598138777381,
			"summary_team": 0.8727272727272727
		}]
	},
	"680390": {
		"nik": "680390",
		"nama": "PUJIYANTO",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ CIBUBUR/ KRANGG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0208507089241032,
			"nilai_komparatif_unit": 1.0214708658964542,
			"summary_team": 0.9272727272727274
		}]
	},
	"680399": {
		"nik": "680399",
		"nama": "USMAN",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ PONDOK KELAPA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888893,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0431818181818178,
			"nilai_komparatif_unit": 1.0438155410879388,
			"summary_team": 0.9272727272727272
		}]
	},
	"680404": {
		"nik": "680404",
		"nama": "ARIEF SUGIHARTO",
		"band": "V",
		"posisi": "OFF 2 DEBT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7791666666666665,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8867282450170153,
			"nilai_komparatif_unit": 0.8872669238844741,
			"summary_team": 0.6909090909090909
		}]
	},
	"680410": {
		"nik": "680410",
		"nama": "M. RACHMAD",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0291595197255574,
			"nilai_komparatif_unit": 1.0297847242204359,
			"summary_team": 0.9090909090909092
		}]
	},
	"680413": {
		"nik": "680413",
		"nama": "PRIYADI HAWANTO",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888893,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181815,
			"nilai_komparatif_unit": 0.9824146269062954,
			"summary_team": 0.8727272727272728
		}]
	},
	"680416": {
		"nik": "680416",
		"nama": "SUPARNO",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888888,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0125000000000002,
			"nilai_komparatif_unit": 1.0131150839971177,
			"summary_team": 0.9000000000000001
		}]
	},
	"680430": {
		"nik": "680430",
		"nama": "WISNU HADI SURYANA",
		"band": "V",
		"posisi": "OFF 2 SERVICE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9600000000000003,
			"nilai_komparatif_unit": 0.960583190752823,
			"summary_team": 0.8
		}]
	},
	"680464": {
		"nik": "680464",
		"nama": "SUBARI",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7944444444444447,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0527654164017797,
			"nilai_komparatif_unit": 1.0534049612515057,
			"summary_team": 0.8363636363636364
		}]
	},
	"680476": {
		"nik": "680476",
		"nama": "WULAN KURNIAWAN",
		"band": "V",
		"posisi": "OFF 2 OM TRANSPORT, RADIO & MUX",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7722222222222226,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0830608240680177,
			"nilai_komparatif_unit": 1.083718773086081,
			"summary_team": 0.8363636363636363
		}]
	},
	"680479": {
		"nik": "680479",
		"nama": "NURUL ICKSAN",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0519480519480517,
			"nilai_komparatif_unit": 1.0525871002567453,
			"summary_team": 0.9818181818181818
		}]
	},
	"680485": {
		"nik": "680485",
		"nama": "RICHARD DERA",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ RAWAMANGUN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7833333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0212765957446805,
			"nilai_komparatif_unit": 1.0218970114391728,
			"summary_team": 0.8
		}]
	},
	"680488": {
		"nik": "680488",
		"nama": "ZAINAL HANIF",
		"band": "V",
		"posisi": "OFF 2 BS SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8766666666666659,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9747666781887323,
			"nilai_komparatif_unit": 0.9753588395563149,
			"summary_team": 0.8545454545454546
		}]
	},
	"680499": {
		"nik": "680499",
		"nama": "LISMAWAN",
		"band": "V",
		"posisi": "OFF 2 BGES ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8761904761904756,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0790513833992101,
			"nilai_komparatif_unit": 1.0797068967206882,
			"summary_team": 0.9454545454545454
		}]
	},
	"680509": {
		"nik": "680509",
		"nama": "SUGIANTOKO",
		"band": "V",
		"posisi": "OFF 2 PAYMENT COLLECTION ENTERPRISE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7791666666666665,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0500729217306761,
			"nilai_komparatif_unit": 1.0507108309158248,
			"summary_team": 0.8181818181818182
		}]
	},
	"680516": {
		"nik": "680516",
		"nama": "ABDURACHMAN CHALIK",
		"band": "V",
		"posisi": "OFF 2 PROJECT DELIVERY & SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8761904761904756,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0583003952569177,
			"nilai_komparatif_unit": 1.0589433025529826,
			"summary_team": 0.9272727272727272
		}]
	},
	"680520": {
		"nik": "680520",
		"nama": "WINARTO",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8388888888888881,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9536423841059611,
			"nilai_komparatif_unit": 0.9542217126683676,
			"summary_team": 0.8
		}]
	},
	"680521": {
		"nik": "680521",
		"nama": "SUYANTO",
		"band": "V",
		"posisi": "OFF 2 DATA CENTER & CME",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333337,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.020850708924103,
			"nilai_komparatif_unit": 1.021470865896454,
			"summary_team": 0.9272727272727272
		}]
	},
	"680523": {
		"nik": "680523",
		"nama": "BUDI MULYONO",
		"band": "V",
		"posisi": "OFF 2 HUMAN CAPITAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.013489736070381,
			"nilai_komparatif_unit": 1.0141054213226277,
			"summary_team": 0.8727272727272727
		}]
	},
	"680527": {
		"nik": "680527",
		"nama": "RUDI HARTONO",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555557,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.051948051948052,
			"nilai_komparatif_unit": 1.0525871002567455,
			"summary_team": 0.9000000000000001
		}]
	},
	"680538": {
		"nik": "680538",
		"nama": "DJONI SUPRIJONO",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8958333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9133192389006345,
			"nilai_komparatif_unit": 0.9138740715407405,
			"summary_team": 0.8181818181818182
		}]
	},
	"680547": {
		"nik": "680547",
		"nama": "SUTANTO",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.804761904761904,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0166756320602484,
			"nilai_komparatif_unit": 1.017293252713621,
			"summary_team": 0.8181818181818181
		}]
	},
	"680550": {
		"nik": "680550",
		"nama": "MARSONGKO",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7944444444444448,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0756516211061662,
			"nilai_komparatif_unit": 1.0763050691047993,
			"summary_team": 0.8545454545454546
		}]
	},
	"680553": {
		"nik": "680553",
		"nama": "HADI YOSHARA",
		"band": "V",
		"posisi": "OFF 2 WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346935,
			"nilai_komparatif_unit": 0.9801869293396145,
			"summary_team": 0.8
		}]
	},
	"680557": {
		"nik": "680557",
		"nama": "SARIFUDIN",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.03030303030303,
			"nilai_komparatif_unit": 1.0309289294695694,
			"summary_team": 0.9272727272727272
		}]
	},
	"680558": {
		"nik": "680558",
		"nama": "SULAIMAN",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8541666666666664,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9365853658536589,
			"nilai_komparatif_unit": 0.9371543324417785,
			"summary_team": 0.8
		}]
	},
	"680562": {
		"nik": "680562",
		"nama": "SAIFUDIN ZUHRI",
		"band": "V",
		"posisi": "OFF 2 BS OBL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.967409948542024,
			"nilai_komparatif_unit": 0.9679976407672097,
			"summary_team": 0.8545454545454546
		}]
	},
	"680567": {
		"nik": "680567",
		"nama": "NURYUDIN",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8388888888888881,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9536423841059611,
			"nilai_komparatif_unit": 0.9542217126683676,
			"summary_team": 0.8
		}]
	},
	"680569": {
		"nik": "680569",
		"nama": "SUWARTO",
		"band": "V",
		"posisi": "OFF 2 LOGISTIK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0505050505050506,
			"nilai_komparatif_unit": 1.0511432222042671,
			"summary_team": 0.9454545454545454
		}]
	},
	"680571": {
		"nik": "680571",
		"nama": "RISAN SANJAYA",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444444,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9473684210526319,
			"nilai_komparatif_unit": 0.9479439382429173,
			"summary_team": 0.8000000000000003
		}]
	},
	"690039": {
		"nik": "690039",
		"nama": "RINA SRI UNTARI",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9614791987673342,
			"nilai_komparatif_unit": 0.9620632881191596,
			"summary_team": 0.9454545454545454
		}]
	},
	"690091": {
		"nik": "690091",
		"nama": "SLAMET WIDODO",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0463821892393321,
			"nilai_komparatif_unit": 1.0470178563400432,
			"summary_team": 0.8545454545454546
		}]
	},
	"690095": {
		"nik": "690095",
		"nama": "ACHMAD KODARI",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT PLAN & RVW",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8799999999999994,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0537190082644634,
			"nilai_komparatif_unit": 1.0543591324120605,
			"summary_team": 0.9272727272727272
		}]
	},
	"690104": {
		"nik": "690104",
		"nama": "ATET WIYONO DARMO",
		"band": "V",
		"posisi": "OFF 2 HUMAN CAPITAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9801869293396148,
			"summary_team": 0.8
		}]
	},
	"690107": {
		"nik": "690107",
		"nama": "BOEDI SANTOSO",
		"band": "V",
		"posisi": "OFF 2 DEBT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999999,
			"nilai_komparatif_unit": 1.0006074903675235,
			"summary_team": 0.8
		}]
	},
	"690111": {
		"nik": "690111",
		"nama": "DWINANTO SUYOSO",
		"band": "V",
		"posisi": "OFF 2 BS DELIVERY & QUALITY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9974025974025971,
			"nilai_komparatif_unit": 0.9980085098730621,
			"summary_team": 0.8727272727272728
		}]
	},
	"690117": {
		"nik": "690117",
		"nama": "IWAN SETIAWAN",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0557184750733135,
			"nilai_komparatif_unit": 1.0563598138777373,
			"summary_team": 0.9090909090909092
		}]
	},
	"690127": {
		"nik": "690127",
		"nama": "NURHAEDI, S.Kom",
		"band": "V",
		"posisi": "OFF 2 DATA CENTER & CME",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333337,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.020850708924103,
			"nilai_komparatif_unit": 1.021470865896454,
			"summary_team": 0.9272727272727272
		}]
	},
	"690131": {
		"nik": "690131",
		"nama": "RACHMATULLAH",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8958333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9539112050739961,
			"nilai_komparatif_unit": 0.9544906969425513,
			"summary_team": 0.8545454545454546
		}]
	},
	"690133": {
		"nik": "690133",
		"nama": "RUSMANTO",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300696,
			"nilai_komparatif_unit": 1.070580042141476,
			"summary_team": 0.9272727272727272
		}]
	},
	"690137": {
		"nik": "690137",
		"nama": "SLAMET SUHARYADI",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.804761904761904,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0166756320602484,
			"nilai_komparatif_unit": 1.017293252713621,
			"summary_team": 0.8181818181818182
		}]
	},
	"690144": {
		"nik": "690144",
		"nama": "SUYATMOKO",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9674099485420238,
			"nilai_komparatif_unit": 0.9679976407672095,
			"summary_team": 0.8545454545454546
		}]
	},
	"690146": {
		"nik": "690146",
		"nama": "TADI",
		"band": "V",
		"posisi": "OFF 2 CONSUMER ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9128014842300554,
			"nilai_komparatif_unit": 0.9133560023391863,
			"summary_team": 0.7454545454545455
		}]
	},
	"690154": {
		"nik": "690154",
		"nama": "AGUS HARAPAN",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545454,
			"nilai_komparatif_unit": 1.0260774992132422,
			"summary_team": 0.8545454545454546
		}]
	},
	"690158": {
		"nik": "690158",
		"nama": "DEDY SURYANTO",
		"band": "V",
		"posisi": "OFF 2 CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0505050505050504,
			"nilai_komparatif_unit": 1.051143222204267,
			"summary_team": 0.9454545454545454
		}]
	},
	"690162": {
		"nik": "690162",
		"nama": "FIRMAN SAPUTRA, S.E",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ CEMPAKA PUTIH",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190478,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9438202247191011,
			"nilai_komparatif_unit": 0.9443935864142918,
			"summary_team": 0.8000000000000002
		}]
	},
	"690171": {
		"nik": "690171",
		"nama": "MOCHAMAD AKROM",
		"band": "V",
		"posisi": "OFF 2 DATA CENTER & CME",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9558441558441556,
			"nilai_komparatif_unit": 0.9564248219616845,
			"summary_team": 0.8363636363636363
		}]
	},
	"690198": {
		"nik": "690198",
		"nama": "MUJIBAN, S.KOM",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333328,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9588068181818188,
			"nilai_komparatif_unit": 0.9593892840881801,
			"summary_team": 0.8181818181818182
		}]
	},
	"690212": {
		"nik": "690212",
		"nama": "ENDANG SUPRIATNA",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.804761904761904,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9940828402366874,
			"nilai_komparatif_unit": 0.9946867359866516,
			"summary_team": 0.8
		}]
	},
	"690213": {
		"nik": "690213",
		"nama": "JUHAEFI",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9125,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0361145703611456,
			"nilai_komparatif_unit": 1.0367439999822907,
			"summary_team": 0.9454545454545454
		}]
	},
	"690221": {
		"nik": "690221",
		"nama": "SURYADI",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7666666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9960474308300397,
			"nilai_komparatif_unit": 0.9966525200498655,
			"summary_team": 0.7636363636363637
		}]
	},
	"690223": {
		"nik": "690223",
		"nama": "ABDUL RAHMAN",
		"band": "V",
		"posisi": "OFF 2 CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666663,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139866,
			"nilai_komparatif_unit": 0.9866129800127336,
			"summary_team": 0.8545454545454546
		}]
	},
	"690224": {
		"nik": "690224",
		"nama": "ASEP SUPRIYANTO",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7833333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9284332688588005,
			"nilai_komparatif_unit": 0.9289972831265206,
			"summary_team": 0.7272727272727273
		}]
	},
	"690227": {
		"nik": "690227",
		"nama": "AGUS USMANSYAH",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888888,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0968750000000003,
			"nilai_komparatif_unit": 1.0975413409968777,
			"summary_team": 0.9750000000000001
		}]
	},
	"690228": {
		"nik": "690228",
		"nama": "ICHSAN",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9290322580645158,
			"nilai_komparatif_unit": 0.9295966362124086,
			"summary_team": 0.8
		}]
	},
	"690232": {
		"nik": "690232",
		"nama": "WARASTO",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0346041055718471,
			"nilai_komparatif_unit": 1.0352326176001823,
			"summary_team": 0.8909090909090909
		}]
	},
	"690243": {
		"nik": "690243",
		"nama": "BERKAH YULIANTO",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649348,
			"nilai_komparatif_unit": 0.9356329780059958,
			"summary_team": 0.8
		}]
	},
	"690247": {
		"nik": "690247",
		"nama": "ACEP JAYADI",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333328,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1505681818181825,
			"nilai_komparatif_unit": 1.1512671409058162,
			"summary_team": 0.9818181818181818
		}]
	},
	"690252": {
		"nik": "690252",
		"nama": "MOHAMAD ANWAR",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8418732782369187,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9502617801047076,
			"nilai_komparatif_unit": 0.950839054982747,
			"summary_team": 0.8000000000000002
		}]
	},
	"690262": {
		"nik": "690262",
		"nama": "ABDUL LATIF",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9600000000000003,
			"nilai_komparatif_unit": 0.960583190752823,
			"summary_team": 0.8
		}]
	},
	"690264": {
		"nik": "690264",
		"nama": "HELFI JUANDI",
		"band": "V",
		"posisi": "OFF 2 CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666663,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930075,
			"nilai_komparatif_unit": 1.0076047455449193,
			"summary_team": 0.8727272727272728
		}]
	},
	"690314": {
		"nik": "690314",
		"nama": "TUBAGUS AHMAD FAKIH",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0346041055718471,
			"nilai_komparatif_unit": 1.0352326176001823,
			"summary_team": 0.8909090909090909
		}]
	},
	"690322": {
		"nik": "690322",
		"nama": "IRFAN JAWAS",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8588505747126479,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9314775160599528,
			"nilai_komparatif_unit": 0.9320433796785239,
			"summary_team": 0.8000000000000002
		}]
	},
	"690334": {
		"nik": "690334",
		"nama": "WIDIYANTO",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365347,
			"nilai_komparatif_unit": 0.988593335251618,
			"summary_team": 0.8727272727272727
		}]
	},
	"690338": {
		"nik": "690338",
		"nama": "ASMAWI",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.804761904761904,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9940828402366874,
			"nilai_komparatif_unit": 0.9946867359866516,
			"summary_team": 0.8
		}]
	},
	"690339": {
		"nik": "690339",
		"nama": "CHOLIL RODI",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8388888888888881,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9536423841059611,
			"nilai_komparatif_unit": 0.9542217126683676,
			"summary_team": 0.8
		}]
	},
	"690348": {
		"nik": "690348",
		"nama": "MOHAMAD ANWAR",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181821,
			"nilai_komparatif_unit": 0.9824146269062961,
			"summary_team": 0.8181818181818182
		}]
	},
	"690352": {
		"nik": "690352",
		"nama": "SAHBANI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER-1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8419753086419747,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0149293521727547,
			"nilai_komparatif_unit": 1.0155459119779167,
			"summary_team": 0.8545454545454546
		}]
	},
	"690355": {
		"nik": "690355",
		"nama": "SUGIANTO",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7897435897435889,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1050767414403788,
			"nilai_komparatif_unit": 1.1057480649161782,
			"summary_team": 0.8727272727272727
		}]
	},
	"690357": {
		"nik": "690357",
		"nama": "TARDI",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139859,
			"nilai_komparatif_unit": 0.9866129800127329,
			"summary_team": 0.8545454545454546
		}]
	},
	"690359": {
		"nik": "690359",
		"nama": "YULI SANTOSO",
		"band": "V",
		"posisi": "OFF 2 CONSUMER ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999997,
			"nilai_komparatif_unit": 0.9605831907528224,
			"summary_team": 0.8
		}]
	},
	"690373": {
		"nik": "690373",
		"nama": "RACHMATULLAH",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181816,
			"nilai_komparatif_unit": 0.9824146269062956,
			"summary_team": 0.8181818181818182
		}]
	},
	"690378": {
		"nik": "690378",
		"nama": "SUYATNO",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7944444444444447,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930066,
			"nilai_komparatif_unit": 1.0076047455449184,
			"summary_team": 0.8
		}]
	},
	"690413": {
		"nik": "690413",
		"nama": "BAMBANG HARYANTO",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9600000000000003,
			"nilai_komparatif_unit": 0.960583190752823,
			"summary_team": 0.8
		}]
	},
	"690427": {
		"nik": "690427",
		"nama": "NURHADI",
		"band": "V",
		"posisi": "OFF 2 DATA CENTER & CME",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.967409948542024,
			"nilai_komparatif_unit": 0.9679976407672097,
			"summary_team": 0.8545454545454546
		}]
	},
	"690430": {
		"nik": "690430",
		"nama": "DARTA",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.015301530153015,
			"nilai_komparatif_unit": 1.015918316052715,
			"summary_team": 0.8545454545454546
		}]
	},
	"690431": {
		"nik": "690431",
		"nama": "ALFIAN NOVERI",
		"band": "V",
		"posisi": "OFF 2 ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.068181818181818,
			"nilai_komparatif_unit": 1.0688307283471272,
			"summary_team": 0.8545454545454546
		}]
	},
	"690439": {
		"nik": "690439",
		"nama": "TRI GUNAWAN",
		"band": "V",
		"posisi": "OFF 2 BS TERRITORY SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8766666666666659,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9747666781887323,
			"nilai_komparatif_unit": 0.9753588395563149,
			"summary_team": 0.8545454545454546
		}]
	},
	"690442": {
		"nik": "690442",
		"nama": "KAERUN",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9250000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9631449631449629,
			"nilai_komparatif_unit": 0.9637300644326022,
			"summary_team": 0.8909090909090909
		}]
	},
	"690443": {
		"nik": "690443",
		"nama": "GATOT WINDI HARTONO",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT PLAN & RVW",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7833333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9980657640232109,
			"nilai_komparatif_unit": 0.99867207936101,
			"summary_team": 0.7818181818181817
		}]
	},
	"690453": {
		"nik": "690453",
		"nama": "SUGIARTO",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT PLAN & RVW",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8799999999999994,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9917355371900833,
			"nilai_komparatif_unit": 0.9923380069760571,
			"summary_team": 0.8727272727272728
		}]
	},
	"690461": {
		"nik": "690461",
		"nama": "BASUKI",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8266666666666661,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9677419354838717,
			"nilai_komparatif_unit": 0.9683298293879267,
			"summary_team": 0.8
		}]
	},
	"690464": {
		"nik": "690464",
		"nama": "SODIKIN",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.911111111111111,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9878048780487808,
			"nilai_komparatif_unit": 0.9884049599971882,
			"summary_team": 0.9000000000000001
		}]
	},
	"690466": {
		"nik": "690466",
		"nama": "TULUS DUMADI",
		"band": "V",
		"posisi": "OFF 2 CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0076047455449186,
			"summary_team": 0.8727272727272727
		}]
	},
	"690468": {
		"nik": "690468",
		"nama": "RUDI ZAILANI",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0115702479338842,
			"nilai_komparatif_unit": 1.0121847671155775,
			"summary_team": 0.9272727272727274
		}]
	},
	"690484": {
		"nik": "690484",
		"nama": "RAHAYU WIRAWATI",
		"band": "V",
		"posisi": "OFF 2 COMMUNICATION & SECRETARIATE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0129870129870129,
			"nilai_komparatif_unit": 1.013602392839829,
			"summary_team": 0.9454545454545454
		}]
	},
	"690504": {
		"nik": "690504",
		"nama": "BAMBANG IRWANTO",
		"band": "V",
		"posisi": "OFF 2 WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.080317740511915,
			"nilai_komparatif_unit": 1.0809740231331408,
			"summary_team": 0.9272727272727272
		}]
	},
	"690507": {
		"nik": "690507",
		"nama": "DRAJAT LUKMAN HAKIM",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9501466275659821,
			"nilai_komparatif_unit": 0.9507238324899634,
			"summary_team": 0.8181818181818182
		}]
	},
	"690523": {
		"nik": "690523",
		"nama": "BAMBANG SUHERMAN",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8041666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9722091380122468,
			"nilai_komparatif_unit": 0.9727997456988077,
			"summary_team": 0.7818181818181819
		}]
	},
	"690527": {
		"nik": "690527",
		"nama": "ZAINAL ABIDIN",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9290322580645157,
			"nilai_komparatif_unit": 0.9295966362124085,
			"summary_team": 0.7999999999999999
		}]
	},
	"690532": {
		"nik": "690532",
		"nama": "SUKARYONO",
		"band": "V",
		"posisi": "OFF 2 CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8958333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9742071881606766,
			"nilai_komparatif_unit": 0.9747990096434564,
			"summary_team": 0.8727272727272727
		}]
	},
	"690534": {
		"nik": "690534",
		"nama": "TRI SAPTA GUNAWAN",
		"band": "V",
		"posisi": "OFF 2 SERVICE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9600000000000003,
			"nilai_komparatif_unit": 0.960583190752823,
			"summary_team": 0.8
		}]
	},
	"690538": {
		"nik": "690538",
		"nama": "ARIF BUDIMAN",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7777777777777777,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9000000000000004,
			"nilai_komparatif_unit": 0.9005467413307715,
			"summary_team": 0.7000000000000002
		}]
	},
	"690541": {
		"nik": "690541",
		"nama": "SUHERMAN",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT PLAN & RVW",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346936,
			"nilai_komparatif_unit": 0.9801869293396146,
			"summary_team": 0.8
		}]
	},
	"690543": {
		"nik": "690543",
		"nama": "SUKIYADI",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969695,
			"nilai_komparatif_unit": 0.9702860512654772,
			"summary_team": 0.8727272727272728
		}]
	},
	"690551": {
		"nik": "690551",
		"nama": "SARTONO",
		"band": "V",
		"posisi": "OFF 2 HUMAN CAPITAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.013489736070381,
			"nilai_komparatif_unit": 1.0141054213226277,
			"summary_team": 0.8727272727272727
		}]
	},
	"690562": {
		"nik": "690562",
		"nama": "JUPRIADI",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ PENGGILINGAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7944444444444448,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930066,
			"nilai_komparatif_unit": 1.0076047455449184,
			"summary_team": 0.8
		}]
	},
	"690570": {
		"nik": "690570",
		"nama": "SETIJO MARNO",
		"band": "V",
		"posisi": "OFF 2 DEBT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8461538461538464,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9454545454545452,
			"nilai_komparatif_unit": 0.9460288999838402,
			"summary_team": 0.8
		}]
	},
	"690580": {
		"nik": "690580",
		"nama": "NOLDY FERNANDO RAWUNG",
		"band": "V",
		"posisi": "OFF 2 SERVICE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9608006672226854,
			"nilai_komparatif_unit": 0.9613843443731334,
			"summary_team": 0.8727272727272728
		}]
	},
	"690587": {
		"nik": "690587",
		"nama": "UNDANG IBRAHIM",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999999,
			"nilai_komparatif_unit": 0.9605831907528225,
			"summary_team": 0.8
		}]
	},
	"700034": {
		"nik": "700034",
		"nama": "SUDARMANTO",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365352,
			"nilai_komparatif_unit": 0.9885933352516184,
			"summary_team": 0.8727272727272728
		}]
	},
	"700039": {
		"nik": "700039",
		"nama": "TARMIN",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969696,
			"nilai_komparatif_unit": 0.9702860512654773,
			"summary_team": 0.8727272727272727
		}]
	},
	"700040": {
		"nik": "700040",
		"nama": "MOHAMAD RACHMAT",
		"band": "V",
		"posisi": "OFF 2 BGES ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8761904761904756,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9960474308300401,
			"nilai_komparatif_unit": 0.9966525200498659,
			"summary_team": 0.8727272727272727
		}]
	},
	"700074": {
		"nik": "700074",
		"nama": "BAGIYO",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.012987012987013,
			"nilai_komparatif_unit": 1.0136023928398292,
			"summary_team": 0.9454545454545454
		}]
	},
	"700086": {
		"nik": "700086",
		"nama": "HENNY MAHMUDY",
		"band": "V",
		"posisi": "OFF 2 CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8925619834710741,
			"nilai_komparatif_unit": 0.8931042062784504,
			"summary_team": 0.7636363636363637
		}]
	},
	"700091": {
		"nik": "700091",
		"nama": "JOKO WIYONO",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8041666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9948186528497409,
			"nilai_komparatif_unit": 0.9954229955987799,
			"summary_team": 0.8
		}]
	},
	"700107": {
		"nik": "700107",
		"nama": "SLAMET SUHENDAR",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8722222222222226,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.979733642154024,
			"nilai_komparatif_unit": 0.9803288209043713,
			"summary_team": 0.8545454545454546
		}]
	},
	"700114": {
		"nik": "700114",
		"nama": "SYAFRIL CANDRA",
		"band": "V",
		"posisi": "OFF 2 BGES ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8541666666666665,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8727272727272729,
			"nilai_komparatif_unit": 0.8732574461389299,
			"summary_team": 0.7454545454545455
		}]
	},
	"700128": {
		"nik": "700128",
		"nama": "BAMBANG MULYADI",
		"band": "V",
		"posisi": "OFF 2 SERVICE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9262435677530017,
			"nilai_komparatif_unit": 0.9268062517983923,
			"summary_team": 0.8181818181818182
		}]
	},
	"700146": {
		"nik": "700146",
		"nama": "SOPIAN",
		"band": "V",
		"posisi": "OFF 2 ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0303030303030303,
			"nilai_komparatif_unit": 1.0309289294695696,
			"summary_team": 0.9272727272727272
		}]
	},
	"700151": {
		"nik": "700151",
		"nama": "SURYANTO",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0703259005145793,
			"nilai_komparatif_unit": 1.0709761131892528,
			"summary_team": 0.9454545454545454
		}]
	},
	"700153": {
		"nik": "700153",
		"nama": "THOMAS PETERSON",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7611111111111114,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9316522893165226,
			"nilai_komparatif_unit": 0.9322182591081636,
			"summary_team": 0.7090909090909091
		}]
	},
	"700173": {
		"nik": "700173",
		"nama": "ENDANG DJAKARIA",
		"band": "V",
		"posisi": "OFF 2 DATA CENTER & CME",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139859,
			"nilai_komparatif_unit": 0.9866129800127329,
			"summary_team": 0.8545454545454546
		}]
	},
	"700187": {
		"nik": "700187",
		"nama": "MARSONO",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.804761904761904,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9940828402366874,
			"nilai_komparatif_unit": 0.9946867359866516,
			"summary_team": 0.8
		}]
	},
	"700192": {
		"nik": "700192",
		"nama": "BAYU SUHARYO",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333328,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8948863636363642,
			"nilai_komparatif_unit": 0.8954299984823014,
			"summary_team": 0.7636363636363637
		}]
	},
	"700200": {
		"nik": "700200",
		"nama": "BUDIARSO",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8799999999999994,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9710743801652899,
			"nilai_komparatif_unit": 0.9716642984973892,
			"summary_team": 0.8545454545454546
		}]
	},
	"700204": {
		"nik": "700204",
		"nama": "SAIMAN",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7944444444444447,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930066,
			"nilai_komparatif_unit": 1.0076047455449184,
			"summary_team": 0.8
		}]
	},
	"700206": {
		"nik": "700206",
		"nama": "JOYO MINTO",
		"band": "V",
		"posisi": "OFF 2 HUMAN CAPITAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0006074903675237,
			"summary_team": 0.8
		}]
	},
	"700208": {
		"nik": "700208",
		"nama": "EDY SUGIANTO",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7944444444444448,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9612205975842335,
			"nilai_komparatif_unit": 0.9618045298383311,
			"summary_team": 0.7636363636363637
		}]
	},
	"700209": {
		"nik": "700209",
		"nama": "LANDUNG WIDIANTORO",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7916666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.941626794258373,
			"nilai_komparatif_unit": 0.9421988234656871,
			"summary_team": 0.7454545454545455
		}]
	},
	"700211": {
		"nik": "700211",
		"nama": "TARUKA",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969695,
			"nilai_komparatif_unit": 0.9702860512654772,
			"summary_team": 0.8727272727272728
		}]
	},
	"700222": {
		"nik": "700222",
		"nama": "SURATMAN",
		"band": "V",
		"posisi": "OFF 2 WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9520661157024795,
			"nilai_komparatif_unit": 0.9526444866970143,
			"summary_team": 0.8727272727272728
		}]
	},
	"700244": {
		"nik": "700244",
		"nama": "RIDWAN SJAFEI",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ PULO GEBANG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0085763293310464,
			"nilai_komparatif_unit": 1.0091890297360273,
			"summary_team": 0.8909090909090909
		}]
	},
	"700250": {
		"nik": "700250",
		"nama": "IWANSYURI CHANDRAWIRA",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555557,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857143,
			"nilai_komparatif_unit": 0.9648715085686834,
			"summary_team": 0.8250000000000002
		}]
	},
	"700308": {
		"nik": "700308",
		"nama": "DWIYANTO JUNED SETYAWAN",
		"band": "V",
		"posisi": "OFF 2 OM TRANSPORT, RADIO & MUX",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7777777777777782,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.098701298701298,
			"nilai_komparatif_unit": 1.0993687491570447,
			"summary_team": 0.8545454545454546
		}]
	},
	"700313": {
		"nik": "700313",
		"nama": "ABDUL RACHMAT",
		"band": "V",
		"posisi": "OFF 2 OM TRANSPORT, RADIO & MUX",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7722222222222226,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9417920209287112,
			"nilai_komparatif_unit": 0.9423641505096358,
			"summary_team": 0.7272727272727273
		}]
	},
	"700318": {
		"nik": "700318",
		"nama": "ANDRI SETIAWAN",
		"band": "V",
		"posisi": "OFF 2 SERVICE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8541666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9365853658536584,
			"nilai_komparatif_unit": 0.9371543324417779,
			"summary_team": 0.8
		}]
	},
	"700319": {
		"nik": "700319",
		"nama": "ATA SUKAWINATA",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006993006993007,
			"nilai_komparatif_unit": 1.0076047455449189,
			"summary_team": 0.8727272727272728
		}]
	},
	"700332": {
		"nik": "700332",
		"nama": "NANA SUYANA",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.804761904761904,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9263044647660041,
			"nilai_komparatif_unit": 0.9268671858057436,
			"summary_team": 0.7454545454545455
		}]
	},
	"700338": {
		"nik": "700338",
		"nama": "SUPARDI",
		"band": "V",
		"posisi": "OFF 2 BGES ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.87,
			"nilai_komparatif_unit": 0.8705285166197455,
			"summary_team": 0.7250000000000001
		}]
	},
	"700352": {
		"nik": "700352",
		"nama": "EDY SUDRAJAT",
		"band": "V",
		"posisi": "OFF 2 SECURITY & SAFETY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8484848484848487,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9428571428571427,
			"nilai_komparatif_unit": 0.9434299194893792,
			"summary_team": 0.8000000000000002
		}]
	},
	"700353": {
		"nik": "700353",
		"nama": "EDY SUPRIADI",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9056603773584906,
			"nilai_komparatif_unit": 0.9062105573139836,
			"summary_team": 0.8
		}]
	},
	"700358": {
		"nik": "700358",
		"nama": "HERI YUWONDO",
		"band": "V",
		"posisi": "OFF 2 SECURITY & SAFETY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197861,
			"nilai_komparatif_unit": 0.9631515950061723,
			"summary_team": 0.8181818181818182
		}]
	},
	"700368": {
		"nik": "700368",
		"nama": "RAKHMAT YUSUPHY",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0208507089241032,
			"nilai_komparatif_unit": 1.0214708658964542,
			"summary_team": 0.9272727272727272
		}]
	},
	"700370": {
		"nik": "700370",
		"nama": "SAIFUDDIN",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9504950495049503,
			"nilai_komparatif_unit": 0.9510724660919034,
			"summary_team": 0.8
		}]
	},
	"700375": {
		"nik": "700375",
		"nama": "SUPRIANTO",
		"band": "V",
		"posisi": "OFF 2 CONSUMER ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197862,
			"nilai_komparatif_unit": 0.9631515950061724,
			"summary_team": 0.8181818181818182
		}]
	},
	"700429": {
		"nik": "700429",
		"nama": "AMIR SYARIFUDIN",
		"band": "V",
		"posisi": "OFF 2 PAYMENT COLLECTION CONSUMER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8250000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0358126721763083,
			"nilai_komparatif_unit": 1.0364419183972142,
			"summary_team": 0.8545454545454546
		}]
	},
	"700478": {
		"nik": "700478",
		"nama": "SLAMET WIDODO",
		"band": "V",
		"posisi": "OFF 2 DATA CENTER & CME",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545454,
			"nilai_komparatif_unit": 1.0260774992132422,
			"summary_team": 0.8545454545454546
		}]
	},
	"700480": {
		"nik": "700480",
		"nama": "SUHARJONO",
		"band": "V",
		"posisi": "OFF 2 DEBT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.03030303030303,
			"nilai_komparatif_unit": 1.0309289294695694,
			"summary_team": 0.9272727272727272
		}]
	},
	"700481": {
		"nik": "700481",
		"nama": "MUHAMAD TOHIR",
		"band": "V",
		"posisi": "OFF 2 NETWORK PERF, QM & ADMIN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888893,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181815,
			"nilai_komparatif_unit": 0.9824146269062954,
			"summary_team": 0.8727272727272728
		}]
	},
	"700567": {
		"nik": "700567",
		"nama": "KUNARTO",
		"band": "V",
		"posisi": "OFF 2 SERVICE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9407839866555462,
			"nilai_komparatif_unit": 0.9413555038653598,
			"summary_team": 0.8545454545454546
		}]
	},
	"700570": {
		"nik": "700570",
		"nama": "TRIYANTO",
		"band": "V",
		"posisi": "OFF 2 COLLECTION PARTNERSHIP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969694,
			"nilai_komparatif_unit": 0.9702860512654771,
			"summary_team": 0.8727272727272727
		}]
	},
	"700572": {
		"nik": "700572",
		"nama": "EDI SUPRIYANTO",
		"band": "V",
		"posisi": "OFF 2 PROJECT DELIVERY & SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8761904761904756,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9960474308300401,
			"nilai_komparatif_unit": 0.9966525200498659,
			"summary_team": 0.8727272727272727
		}]
	},
	"700583": {
		"nik": "700583",
		"nama": "YOYOH SUHAERIAH",
		"band": "V",
		"posisi": "OFF 2 SERVICE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0195412064570943,
			"nilai_komparatif_unit": 1.0201605679193104,
			"summary_team": 0.9090909090909092
		}]
	},
	"700586": {
		"nik": "700586",
		"nama": "ERMAN",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE PADEMANGAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8041666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9948186528497409,
			"nilai_komparatif_unit": 0.9954229955987799,
			"summary_team": 0.8
		}]
	},
	"710009": {
		"nik": "710009",
		"nama": "MOHAMAD HARJANTO",
		"band": "V",
		"posisi": "OFF 2 BGES ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8761904761904756,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9130434782608702,
			"nilai_komparatif_unit": 0.9135981433790439,
			"summary_team": 0.8
		}]
	},
	"710030": {
		"nik": "710030",
		"nama": "ERWIN MAULANA",
		"band": "V",
		"posisi": "OFF 2 COMMUNICATION & SECRETARIATE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090908,
			"nilai_komparatif_unit": 1.091571807673662,
			"summary_team": 0.9090909090909092
		}]
	},
	"710058": {
		"nik": "710058",
		"nama": "BUDI WAHYONO",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8971962616822428,
			"nilai_komparatif_unit": 0.8977412997689929,
			"summary_team": 0.8
		}]
	},
	"710084": {
		"nik": "710084",
		"nama": "RACHMAT",
		"band": "V",
		"posisi": "OFF 2 OM TRANSPORT, RADIO & MUX",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.015301530153015,
			"nilai_komparatif_unit": 1.015918316052715,
			"summary_team": 0.8545454545454546
		}]
	},
	"710085": {
		"nik": "710085",
		"nama": "SUNARNO",
		"band": "V",
		"posisi": "OFF 2 BGES ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559442,
			"nilai_komparatif_unit": 0.9446294489483615,
			"summary_team": 0.8181818181818182
		}]
	},
	"710089": {
		"nik": "710089",
		"nama": "MAT DJAYA",
		"band": "V",
		"posisi": "OFF 2 CONSUMER ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0059583325620023,
			"summary_team": 0.8545454545454546
		}]
	},
	"710104": {
		"nik": "710104",
		"nama": "IWAN RIYANTO",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882352,
			"nilai_komparatif_unit": 0.9417482262282573,
			"summary_team": 0.8
		}]
	},
	"710111": {
		"nik": "710111",
		"nama": "SUYOTO",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.911111111111111,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0701219512195124,
			"nilai_komparatif_unit": 1.0707720399969538,
			"summary_team": 0.9750000000000001
		}]
	},
	"710181": {
		"nik": "710181",
		"nama": "FAHRU RODJI",
		"band": "V",
		"posisi": "OFF 2 BS TERRITORY SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8766666666666659,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0369858278603536,
			"nilai_komparatif_unit": 1.0376157867620373,
			"summary_team": 0.9090909090909092
		}]
	},
	"710182": {
		"nik": "710182",
		"nama": "FREDDY",
		"band": "V",
		"posisi": "OFF 2 BGES ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8761904761904756,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9960474308300401,
			"nilai_komparatif_unit": 0.9966525200498659,
			"summary_team": 0.8727272727272727
		}]
	},
	"710191": {
		"nik": "710191",
		"nama": "SLAMET RIADI",
		"band": "V",
		"posisi": "OFF 2 ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8409090909090906,
			"nilai_komparatif_unit": 0.8414199350817808,
			"summary_team": 0.6727272727272727
		}]
	},
	"710270": {
		"nik": "710270",
		"nama": "MULYANA",
		"band": "V",
		"posisi": "OFF 2 BGES ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8541666666666665,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9578713968957874,
			"nilai_komparatif_unit": 0.9584532945427279,
			"summary_team": 0.8181818181818182
		}]
	},
	"710272": {
		"nik": "710272",
		"nama": "HOT PARSAORAN",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999997,
			"nilai_komparatif_unit": 0.9605831907528224,
			"summary_team": 0.8
		}]
	},
	"710299": {
		"nik": "710299",
		"nama": "WAHYU",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888893,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272723,
			"nilai_komparatif_unit": 1.0233485696940576,
			"summary_team": 0.9090909090909092
		}]
	},
	"710310": {
		"nik": "710310",
		"nama": "IWAN SYARIF",
		"band": "V",
		"posisi": "OFF 2 ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7966666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0041841004184104,
			"nilai_komparatif_unit": 1.004794132586635,
			"summary_team": 0.8000000000000003
		}]
	},
	"710319": {
		"nik": "710319",
		"nama": "SUNTORO",
		"band": "V",
		"posisi": "OFF 2 BGES FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9056603773584903,
			"nilai_komparatif_unit": 0.9062105573139833,
			"summary_team": 0.8
		}]
	},
	"720003": {
		"nik": "720003",
		"nama": "SUPARDI",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197862,
			"nilai_komparatif_unit": 0.9631515950061724,
			"summary_team": 0.8181818181818182
		}]
	},
	"730006": {
		"nik": "730006",
		"nama": "FATMAWATY MUIS",
		"band": "V",
		"posisi": "OFF 2 COMMUNITY DEVELOPMENT CENTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.98989898989899,
			"nilai_komparatif_unit": 0.9905003440001748,
			"summary_team": 0.8909090909090909
		}]
	},
	"730013": {
		"nik": "730013",
		"nama": "MUHAMMAD IQBAL",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7916666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.033492822966507,
			"nilai_komparatif_unit": 1.034120659901364,
			"summary_team": 0.8181818181818182
		}]
	},
	"840059": {
		"nik": "840059",
		"nama": "MELINDA SINULINGGA",
		"band": "V",
		"posisi": "OFF 2 HR CAREER & TASKFORCE MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0212765957446812,
			"nilai_komparatif_unit": 1.0218970114391734,
			"summary_team": 0.8000000000000003
		}]
	},
	"900144": {
		"nik": "900144",
		"nama": "ADITYAR",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9163636363636363,
			"nilai_komparatif_unit": 0.9169203184458761,
			"summary_team": 0.7636363636363637
		}]
	},
	"910238": {
		"nik": "910238",
		"nama": "RILLYAN NUR RAMADHANI ALFIAN",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969697,
			"nilai_komparatif_unit": 0.9702860512654774,
			"summary_team": 0.8727272727272728
		}]
	},
	"910240": {
		"nik": "910240",
		"nama": "LULU HANIFAH",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999999,
			"nilai_komparatif_unit": 0.9605831907528225,
			"summary_team": 0.8
		}]
	},
	"920068": {
		"nik": "920068",
		"nama": "ARDHINI ORIENTA AGUISTI",
		"band": "V",
		"posisi": "OFF 2 HUMAN CAPITAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8303030303030305,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0291970802919705,
			"nilai_komparatif_unit": 1.0298223076045312,
			"summary_team": 0.8545454545454546
		}]
	},
	"920142": {
		"nik": "920142",
		"nama": "PUTRI AMANDA WIDIYANUARINI",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT PLAN & RVW",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197862,
			"nilai_komparatif_unit": 0.9631515950061724,
			"summary_team": 0.8181818181818182
		}]
	},
	"920233": {
		"nik": "920233",
		"nama": "SARAH KARIMAH",
		"band": "V",
		"posisi": "OFF 2 DEBT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8250000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969694,
			"nilai_komparatif_unit": 0.9702860512654771,
			"summary_team": 0.8
		}]
	},
	"920243": {
		"nik": "920243",
		"nama": "ADITYA SURYA WARDHANA",
		"band": "V",
		"posisi": "OFF 2 CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8473520249221214,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9441176470588204,
			"nilai_komparatif_unit": 0.9446911894352176,
			"summary_team": 0.8000000000000002
		}]
	},
	"920256": {
		"nik": "920256",
		"nama": "CITRA KHARISMA PUTRI",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9608006672226856,
			"nilai_komparatif_unit": 0.9613843443731336,
			"summary_team": 0.8727272727272728
		}]
	},
	"920265": {
		"nik": "920265",
		"nama": "NGURAH WIRA NUGRAHA",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8722222222222225,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9380428488708742,
			"nilai_komparatif_unit": 0.9386127008658876,
			"summary_team": 0.8181818181818182
		}]
	},
	"920274": {
		"nik": "920274",
		"nama": "PUTU LINDA INDITA SARI",
		"band": "V",
		"posisi": "OFF 2 SERVICE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181821,
			"nilai_komparatif_unit": 0.9824146269062961,
			"summary_team": 0.8181818181818182
		}]
	},
	"920283": {
		"nik": "920283",
		"nama": "RAKHMAT OKTARIZA",
		"band": "V",
		"posisi": "OFF 2 ACCESS DEPLOYMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8612244897959169,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0218009478673002,
			"nilai_komparatif_unit": 1.022421682100656,
			"summary_team": 0.88
		}]
	},
	"930034": {
		"nik": "930034",
		"nama": "MAFIKA SARI RIGA",
		"band": "V",
		"posisi": "OFF 2 PAYMENT COLLECTION CONSUMER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0389610389610386,
			"nilai_komparatif_unit": 1.0395921977844398,
			"summary_team": 0.9090909090909092
		}]
	},
	"930101": {
		"nik": "930101",
		"nama": "SRI RADINA PUTRI NUR HIDAYATIKA",
		"band": "V",
		"posisi": "OFF 2 DIGITAL SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8592592592592582,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0156739811912239,
			"nilai_komparatif_unit": 1.0162909933513418,
			"summary_team": 0.8727272727272728
		}]
	},
	"930198": {
		"nik": "930198",
		"nama": "MOCHAMAD RANDI SUGIANTARA",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8541666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0217294900221727,
			"nilai_komparatif_unit": 1.022350180845576,
			"summary_team": 0.8727272727272728
		}]
	},
	"930199": {
		"nik": "930199",
		"nama": "DIEN ARDIANSYAH PUTERA",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE MANGGA BSR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8739583333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9985913966843644,
			"nilai_komparatif_unit": 0.999198031338942,
			"summary_team": 0.8727272727272728
		}]
	},
	"930207": {
		"nik": "930207",
		"nama": "JATMIKO RENO RAMADHANI",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE TIGARAKSA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0472727272727276,
			"nilai_komparatif_unit": 1.047908935366716,
			"summary_team": 0.8727272727272727
		}]
	},
	"930211": {
		"nik": "930211",
		"nama": "HANGGARA MARIO JR",
		"band": "V",
		"posisi": "OFF 2 PROJECT DELIVERY & SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.99,
			"nilai_komparatif_unit": 0.9906014154638483,
			"summary_team": 0.8250000000000002
		}]
	},
	"930215": {
		"nik": "930215",
		"nama": "MOCHAMMAD ARFIN",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE PD GEDE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8352941176470584,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.95774647887324,
			"nilai_komparatif_unit": 0.9583283006336851,
			"summary_team": 0.8000000000000002
		}]
	},
	"930216": {
		"nik": "930216",
		"nama": "NANI SRI SULISTIYOWATI",
		"band": "V",
		"posisi": "OFF 2 CONSUMER ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9262435677530017,
			"nilai_komparatif_unit": 0.9268062517983923,
			"summary_team": 0.8181818181818182
		}]
	},
	"930244": {
		"nik": "930244",
		"nama": "ANAS WICAKSONO",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333328,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0014204545454553,
			"nilai_komparatif_unit": 1.0020288078254327,
			"summary_team": 0.8545454545454546
		}]
	},
	"930253": {
		"nik": "930253",
		"nama": "DEVYTA ASTERINA",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.03030303030303,
			"nilai_komparatif_unit": 1.0309289294695694,
			"summary_team": 0.9272727272727272
		}]
	},
	"930268": {
		"nik": "930268",
		"nama": "MAR'ATUS SHOLICHA",
		"band": "V",
		"posisi": "OFF 2 BGES SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8722222222222226,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.979733642154024,
			"nilai_komparatif_unit": 0.9803288209043713,
			"summary_team": 0.8545454545454546
		}]
	},
	"930299": {
		"nik": "930299",
		"nama": "MOCHAMAD WILDAN FIRDAUS",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE SERANG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8055555555555558,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9705329153605011,
			"nilai_komparatif_unit": 0.9711225047579471,
			"summary_team": 0.7818181818181817
		}]
	},
	"930307": {
		"nik": "930307",
		"nama": "TOFAN BIMATARA",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE PGGILINGAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8507246376811579,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9403747870528126,
			"nilai_komparatif_unit": 0.9409460556778092,
			"summary_team": 0.8
		}]
	},
	"930329": {
		"nik": "930329",
		"nama": "BANI AULIA RAHMAN",
		"band": "V",
		"posisi": "OFF 2 MAINTENANCE & QE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8350877192982452,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0298319327731098,
			"nilai_komparatif_unit": 1.0304575457524376,
			"summary_team": 0.8600000000000001
		}]
	},
	"930366": {
		"nik": "930366",
		"nama": "AJENG NURWULAN SARI",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9262435677530014,
			"nilai_komparatif_unit": 0.926806251798392,
			"summary_team": 0.8181818181818182
		}]
	},
	"930372": {
		"nik": "930372",
		"nama": "ANHAR TRIBOWO",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE BT GEBANG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8629629629629626,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9601471489883513,
			"nilai_komparatif_unit": 0.9607304291327668,
			"summary_team": 0.8285714285714287
		}]
	},
	"930390": {
		"nik": "930390",
		"nama": "TESI DWI NAFIA",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9520661157024792,
			"nilai_komparatif_unit": 0.9526444866970141,
			"summary_team": 0.8727272727272728
		}]
	},
	"940034": {
		"nik": "940034",
		"nama": "ABI DZARR ALGHIFARI WIJANARKO",
		"band": "V",
		"posisi": "ACCOUNT MANAGER-1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8419753086419747,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0149293521727547,
			"nilai_komparatif_unit": 1.0155459119779167,
			"summary_team": 0.8545454545454546
		}]
	},
	"940059": {
		"nik": "940059",
		"nama": "ARWAN SUBHI ROSYIDI",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE KDGHALANG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8196969696969681,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9759704251386343,
			"nilai_komparatif_unit": 0.9765633177708939,
			"summary_team": 0.8000000000000003
		}]
	},
	"940066": {
		"nik": "940066",
		"nama": "MUHAMMAD ADITYA TAUFIK",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE KLENDER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8507246376811579,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0686077125600144,
			"nilai_komparatif_unit": 1.069256881452056,
			"summary_team": 0.9090909090909092
		}]
	},
	"940075": {
		"nik": "940075",
		"nama": "ANINDITYA ESTI PRATIWI",
		"band": "V",
		"posisi": "OFF 2 COLLECTION PARTNERSHIP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7791666666666665,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.050072921730676,
			"nilai_komparatif_unit": 1.0507108309158246,
			"summary_team": 0.8181818181818181
		}]
	},
	"940076": {
		"nik": "940076",
		"nama": "FACHMI AGUNG CALISTA",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE PEKAYON",
		"category": [{
			"code": "MD-1-01",
			"kriteria": 0.8352941176470584,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1373239436619724,
			"nilai_komparatif_unit": 1.138014857002501,
			"summary_team": 0.95
		}]
	},
	"940079": {
		"nik": "940079",
		"nama": "ROBISMAN JAYA GALENGGANG MARPAUNG",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CILEGON",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8928571428571421,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0589090909090917,
			"nilai_komparatif_unit": 1.059552367981902,
			"summary_team": 0.9454545454545454
		}]
	},
	"940141": {
		"nik": "940141",
		"nama": "ADITYA PUTRA AVIANTORO",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CIKUPA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8928571428571421,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0589090909090917,
			"nilai_komparatif_unit": 1.059552367981902,
			"summary_team": 0.9454545454545454
		}]
	},
	"940157": {
		"nik": "940157",
		"nama": "JULI ARIANES",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8512820512820554,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.010240963855417,
			"nilai_komparatif_unit": 1.0108546755098369,
			"summary_team": 0.8600000000000001
		}]
	},
	"940158": {
		"nik": "940158",
		"nama": "REBECCA CHITTRA WIDYAPARAMITHA",
		"band": "V",
		"posisi": "OFF 2 WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365353,
			"nilai_komparatif_unit": 0.9885933352516185,
			"summary_team": 0.8727272727272728
		}]
	},
	"940167": {
		"nik": "940167",
		"nama": "ANDRIA PUJA PRATAMA",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333328,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0653409090909098,
			"nilai_komparatif_unit": 1.0659880934313113,
			"summary_team": 0.9090909090909092
		}]
	},
	"940177": {
		"nik": "940177",
		"nama": "MUHAMMAD DIAN PUTRA",
		"band": "V",
		"posisi": "OFF 2 FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8549999999999988,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0292397660818728,
			"nilai_komparatif_unit": 1.0298650193256398,
			"summary_team": 0.88
		}]
	},
	"940213": {
		"nik": "940213",
		"nama": "NURRAHMAT PRADESKA",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7944444444444448,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930066,
			"nilai_komparatif_unit": 1.0076047455449184,
			"summary_team": 0.8
		}]
	},
	"940215": {
		"nik": "940215",
		"nama": "FANNY TARIDA TAMPUBOLON",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE SEMPLAK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8904761904761906,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9596499756927563,
			"nilai_komparatif_unit": 0.9602329538091838,
			"summary_team": 0.8545454545454546
		}]
	},
	"940251": {
		"nik": "940251",
		"nama": "ASTIN AMANDA",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8450450450450468,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1833688699360316,
			"nilai_komparatif_unit": 1.184087755125745,
			"summary_team": 1
		}]
	},
	"940264": {
		"nik": "940264",
		"nama": "ARKANDIYAN HAIDAR",
		"band": "V",
		"posisi": "OFF 2 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666664,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769235,
			"nilai_komparatif_unit": 0.9236376834161759,
			"summary_team": 0.8
		}]
	},
	"940312": {
		"nik": "940312",
		"nama": "FARIDA FITRI KUSUMASTUTI",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CILINCING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8739583333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0610033589771373,
			"nilai_komparatif_unit": 1.061647908297626,
			"summary_team": 0.9272727272727274
		}]
	},
	"940318": {
		"nik": "940318",
		"nama": "ALDI SETIAWAN",
		"band": "V",
		"posisi": "OFF 2 QUALITY, CHANGE & RISK MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8431372549019593,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0350951374207205,
			"nilai_komparatif_unit": 1.035723947746174,
			"summary_team": 0.8727272727272727
		}]
	},
	"940333": {
		"nik": "940333",
		"nama": "MUHAMMAD YASYIR",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE PDK KELAPA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8507246376811579,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9403747870528126,
			"nilai_komparatif_unit": 0.9409460556778092,
			"summary_team": 0.8
		}]
	},
	"940336": {
		"nik": "940336",
		"nama": "MAULIDAWATI",
		"band": "V",
		"posisi": "OFF 2 DAVAL & INVENTORY MGT PLAN & REV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8928571428571421,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9774545454545464,
			"nilai_komparatif_unit": 0.9780483396756021,
			"summary_team": 0.8727272727272728
		}]
	},
	"940366": {
		"nik": "940366",
		"nama": "ABDULLAH ZAKY",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE RAWAMANGUN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8507246376811579,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0258634040576136,
			"nilai_komparatif_unit": 1.0264866061939735,
			"summary_team": 0.8727272727272727
		}]
	},
	"940398": {
		"nik": "940398",
		"nama": "FAJAR SETIAWAN",
		"band": "V",
		"posisi": "OFF 2 OM TRANSPORT, RADIO & MUX",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.012987012987013,
			"nilai_komparatif_unit": 1.0136023928398292,
			"summary_team": 0.9454545454545454
		}]
	},
	"950015": {
		"nik": "950015",
		"nama": "SHAFIRA KARAMINA ALIFAH",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE KLP GADING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8739583333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.061003358977137,
			"nilai_komparatif_unit": 1.0616479082976258,
			"summary_team": 0.9272727272727272
		}]
	},
	"950048": {
		"nik": "950048",
		"nama": "DAHLIA HAJARIANA",
		"band": "V",
		"posisi": "OFF 2 LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8694444444444431,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0187129164764965,
			"nilai_komparatif_unit": 1.0193317747605277,
			"summary_team": 0.8857142857142859
		}]
	},
	"950053": {
		"nik": "950053",
		"nama": "DIAN RATNA KUMALA",
		"band": "V",
		"posisi": "OFF 2 CONSUMER ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8450450450450468,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0112424888544271,
			"nilai_komparatif_unit": 1.0118568089256368,
			"summary_team": 0.8545454545454546
		}]
	},
	"950056": {
		"nik": "950056",
		"nama": "HANIF WIJDAN MUHAMMAD",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CIRUAS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8928571428571421,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0589090909090917,
			"nilai_komparatif_unit": 1.059552367981902,
			"summary_team": 0.9454545454545454
		}]
	},
	"950060": {
		"nik": "950060",
		"nama": "FADILA DIAS NURAINI, S.H.",
		"band": "V",
		"posisi": "OFF 2 LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8694444444444431,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0187129164764965,
			"nilai_komparatif_unit": 1.0193317747605277,
			"summary_team": 0.8857142857142859
		}]
	},
	"950062": {
		"nik": "950062",
		"nama": "AZMI WICAKSONO",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE KALIABANG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8352941176470584,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.09456740442656,
			"nilai_komparatif_unit": 1.0952323435813545,
			"summary_team": 0.9142857142857145
		}]
	},
	"950063": {
		"nik": "950063",
		"nama": "VANIA PUSPA ANDARI",
		"band": "V",
		"posisi": "OFF 2 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197862,
			"nilai_komparatif_unit": 0.9631515950061724,
			"summary_team": 0.8181818181818182
		}]
	},
	"950090": {
		"nik": "950090",
		"nama": "MARINA SAFITRI",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8450450450450468,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0112424888544271,
			"nilai_komparatif_unit": 1.0118568089256368,
			"summary_team": 0.8545454545454546
		}]
	},
	"950091": {
		"nik": "950091",
		"nama": "I PUTU DWI PRATAMA ARIJAYA",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE SUKMAJAYA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8431372549019593,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9784883720930252,
			"nilai_komparatif_unit": 0.9790827943538055,
			"summary_team": 0.8250000000000002
		}]
	},
	"950093": {
		"nik": "950093",
		"nama": "ALFITA RAHMA KUSUMAWATI",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0557184750733135,
			"nilai_komparatif_unit": 1.0563598138777373,
			"summary_team": 0.9090909090909092
		}]
	},
	"950099": {
		"nik": "950099",
		"nama": "FUJITSON SIMAMORA",
		"band": "V",
		"posisi": "OFF 2 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7944444444444448,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930066,
			"nilai_komparatif_unit": 1.0076047455449184,
			"summary_team": 0.8
		}]
	},
	"950104": {
		"nik": "950104",
		"nama": "FACHRI HUSAINI",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8541666666666664,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0004434589800448,
			"nilai_komparatif_unit": 1.0010512187446272,
			"summary_team": 0.8545454545454546
		}]
	},
	"950115": {
		"nik": "950115",
		"nama": "SAUSAN DWI AMANDA",
		"band": "V",
		"posisi": "OFF 2 VERIFICATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8608695652173922,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9624819624819616,
			"nilai_komparatif_unit": 0.9630666610030846,
			"summary_team": 0.8285714285714287
		}]
	},
	"950151": {
		"nik": "950151",
		"nama": "FARAH SALSABILA",
		"band": "V",
		"posisi": "OFF 2 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8418732782369187,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9502617801047076,
			"nilai_komparatif_unit": 0.950839054982747,
			"summary_team": 0.8000000000000002
		}]
	},
	"950185": {
		"nik": "950185",
		"nama": "MOHAMAD ABU BAKAR SALAM",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8488888888888888,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9424083769633512,
			"nilai_komparatif_unit": 0.94298088097463,
			"summary_team": 0.8000000000000003
		}]
	},
	"950189": {
		"nik": "950189",
		"nama": "EROS FATTA ZARKASYI ADHA",
		"band": "V",
		"posisi": "OFF 2 SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8805555555555546,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0530542013191866,
			"nilai_komparatif_unit": 1.0536939216029682,
			"summary_team": 0.9272727272727272
		}]
	},
	"950220": {
		"nik": "950220",
		"nama": "AGUS TRI WIBOWO",
		"band": "V",
		"posisi": "OFF 2 SERVICE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8541666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8727272727272726,
			"nilai_komparatif_unit": 0.8732574461389295,
			"summary_team": 0.7454545454545455
		}]
	},
	"950226": {
		"nik": "950226",
		"nama": "INMAS AL AZIZ BUDIMAN",
		"band": "V",
		"posisi": "OFF 2 OM TRANSPORT, RADIO & MUX",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8739583333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9777874092534401,
			"nilai_komparatif_unit": 0.9783814056860474,
			"summary_team": 0.8545454545454546
		}]
	},
	"950227": {
		"nik": "950227",
		"nama": "AGUSTANTIO FERNANDO SARAGIH",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8512820512820554,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.010240963855417,
			"nilai_komparatif_unit": 1.0108546755098369,
			"summary_team": 0.8600000000000001
		}]
	},
	"950239": {
		"nik": "950239",
		"nama": "NAINAUFAL HIDAYAH",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE MUARA KRG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8739583333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9361794343915916,
			"nilai_komparatif_unit": 0.936748154380258,
			"summary_team": 0.8181818181818182
		}]
	},
	"950295": {
		"nik": "950295",
		"nama": "ARIFANDI RACHMADIYAN",
		"band": "V",
		"posisi": "OFF 2 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8388888888888881,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9536423841059611,
			"nilai_komparatif_unit": 0.9542217126683676,
			"summary_team": 0.8
		}]
	},
	"950316": {
		"nik": "950316",
		"nama": "MUHAMMAD RIZKI JIHAD",
		"band": "V",
		"posisi": "OFF 2 LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8694444444444431,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0515746234596093,
			"nilai_komparatif_unit": 1.0522134449140932,
			"summary_team": 0.9142857142857145
		}]
	},
	"950361": {
		"nik": "950361",
		"nama": "ICHWAN FAKHRUDIN",
		"band": "V",
		"posisi": "OFF 2 PROCUREMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9962264150943396,
			"nilai_komparatif_unit": 0.9968316130453819,
			"summary_team": 0.8800000000000001
		}]
	},
	"960009": {
		"nik": "960009",
		"nama": "MALINDA PUTERI KUSAENI",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE SUNTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8739583333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.040199371546213,
			"nilai_komparatif_unit": 1.0408312826447315,
			"summary_team": 0.9090909090909092
		}]
	},
	"960033": {
		"nik": "960033",
		"nama": "ALIFIA FITRI UTAMI",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CAWANG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8507246376811579,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.047235558308814,
			"nilai_komparatif_unit": 1.0478717438230147,
			"summary_team": 0.8909090909090909
		}]
	},
	"960036": {
		"nik": "960036",
		"nama": "IHDA GHEA ADHIBA",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8370370370370372,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9557522123893804,
			"nilai_komparatif_unit": 0.9563328226521463,
			"summary_team": 0.8
		}]
	},
	"960054": {
		"nik": "960054",
		"nama": "LELAN ADINDHARI",
		"band": "V",
		"posisi": "OFF 2 LOGISTIC PLANNING & EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.940740740740741,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9718785151856018,
			"nilai_komparatif_unit": 0.9724689220219801,
			"summary_team": 0.9142857142857145
		}]
	},
	"960085": {
		"nik": "960085",
		"nama": "RAHEKO YUNANTORI",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545458,
			"nilai_komparatif_unit": 1.0260774992132426,
			"summary_team": 0.8545454545454546
		}]
	},
	"960093": {
		"nik": "960093",
		"nama": "SATRIA BAGUS PRATAMA",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE PASAR REBO",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8507246376811579,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1113520210624148,
			"nilai_komparatif_unit": 1.112027156710138,
			"summary_team": 0.9454545454545454
		}]
	}
};