export const cbark24 = {
	"670497": {
		"nik": "670497",
		"nama": "Sumanto Yudiono",
		"band": "VI",
		"posisi": "JUNIOR ENGINEER ENERGY SYSTEM OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7888888888888892,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.014084507042253,
			"nilai_komparatif_unit": 1.0173818734335172,
			"summary_team": 0.7999999999999999
		}]
	},
	"680285": {
		"nik": "680285",
		"nama": "SUHERMAN",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8312599681020792,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0655081679640317,
			"nilai_komparatif_unit": 1.0661554539125875,
			"summary_team": 0.8857142857142859
		}]
	},
	"680565": {
		"nik": "680565",
		"nama": "LUKMAN",
		"band": "V",
		"posisi": "OFF 2 OM TRANSPORT, RADIO & MUX",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8408888888888898,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1892177589851995,
			"nilai_komparatif_unit": 1.189940197318671,
			"summary_team": 1
		}]
	},
	"700285": {
		"nik": "700285",
		"nama": "WAHYUDI",
		"band": "V",
		"posisi": "OFF 2 COMMAND CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8466666666666655,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7086614173228356,
			"nilai_komparatif_unit": 0.7090919223076948,
			"summary_team": 0.6
		}]
	},
	"700500": {
		"nik": "700500",
		"nama": "SUNARTO",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8312599681020792,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9967657055147393,
			"nilai_komparatif_unit": 0.9973712310795173,
			"summary_team": 0.8285714285714287
		}]
	},
	"700632": {
		"nik": "700632",
		"nama": "HALIM SUTORO",
		"band": "V",
		"posisi": "OFF 2 IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8406249999999997,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9856611789697295,
			"nilai_komparatif_unit": 0.9862599586415955,
			"summary_team": 0.8285714285714286
		}]
	},
	"795919": {
		"nik": "795919",
		"nama": "ERNI ZULIANI",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8577777777777775,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8902496467263308,
			"nilai_komparatif_unit": 0.8931443554459344,
			"summary_team": 0.7636363636363636
		}]
	},
	"850186": {
		"nik": "850186",
		"nama": "DESI PRAMUDIWATI",
		"band": "VI",
		"posisi": "OFF 3 SOLUTION DATIN & IMES 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999997,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.916666666666667,
			"nilai_komparatif_unit": 0.9196472721661777,
			"summary_team": 0.7333333333333333
		}]
	},
	"860169": {
		"nik": "860169",
		"nama": "MARTHA TIUR CORY RUTH",
		"band": "VI",
		"posisi": "OFF 3 OFFERING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0167696381288611,
			"nilai_komparatif_unit": 1.0200757354108347,
			"summary_team": 0.8727272727272727
		}]
	},
	"880103": {
		"nik": "880103",
		"nama": "BIMOWISDA NUGROHO PUTRA",
		"band": "VI",
		"posisi": "OFF 3 DATA SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9533333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9740259740259739,
			"nilai_komparatif_unit": 0.9771930873076264,
			"summary_team": 0.9285714285714284
		}]
	},
	"890105": {
		"nik": "890105",
		"nama": "GISCA TAMARA",
		"band": "VI",
		"posisi": "OFF 3 CLOUD COMPUTING CAPACITY MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969697,
			"nilai_komparatif_unit": 0.9728500069195927,
			"summary_team": 0.8727272727272728
		}]
	},
	"890106": {
		"nik": "890106",
		"nama": "KINPU MANZU LAUTA",
		"band": "VI",
		"posisi": "OFF 3 PROCUREMENT PROCESS II",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8791666666666665,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0886932972241028,
			"nilai_komparatif_unit": 1.0922332592920883,
			"summary_team": 0.9571428571428569
		}]
	},
	"890107": {
		"nik": "890107",
		"nama": "YOGA PRATAMA",
		"band": "VI",
		"posisi": "OFF 3 CRM SYSTEM ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.03030303030303,
			"nilai_komparatif_unit": 1.033653132352067,
			"summary_team": 0.9066666666666665
		}]
	},
	"900143": {
		"nik": "900143",
		"nama": "AULIA VIRNANDA SURAPERWATA",
		"band": "VI",
		"posisi": "OFF 3 TARIFF POLICY & GOVERNANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8222222222222211,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9872813990461063,
			"nilai_komparatif_unit": 0.9904916132652644,
			"summary_team": 0.8117647058823529
		}]
	},
	"900147": {
		"nik": "900147",
		"nama": "FERDY SYARI HIDAYAT",
		"band": "VI",
		"posisi": "OFF 3 LIBRARY & QUALITY CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8041666666666654,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0500863557858393,
			"nilai_komparatif_unit": 1.0535007846953117,
			"summary_team": 0.8444444444444444
		}]
	},
	"910250": {
		"nik": "910250",
		"nama": "INTAN SUMINAR",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8771929824561402,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0570909090909093,
			"nilai_komparatif_unit": 1.0605281137932212,
			"summary_team": 0.9272727272727272
		}]
	},
	"910254": {
		"nik": "910254",
		"nama": "RIZKI RAMADHAN HIDAYAT",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9199999999999998,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.948616600790514,
			"nilai_komparatif_unit": 0.9517010937256887,
			"summary_team": 0.8727272727272727
		}]
	},
	"910255": {
		"nik": "910255",
		"nama": "SANTO BUDIONO",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8553191489361723,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9353233830845749,
			"nilai_komparatif_unit": 0.9383646521966944,
			"summary_team": 0.8
		}]
	},
	"910257": {
		"nik": "910257",
		"nama": "FARID ARIFIYANTO",
		"band": "VI",
		"posisi": "OFF 3 QUALITY & DATA SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8774774774774767,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0332648870636558,
			"nilai_komparatif_unit": 1.0366246197962012,
			"summary_team": 0.9066666666666666
		}]
	},
	"910260": {
		"nik": "910260",
		"nama": "AYU TIFANI",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85528455284553,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.999135845143448,
			"nilai_komparatif_unit": 1.0023846049195857,
			"summary_team": 0.8545454545454546
		}]
	},
	"910261": {
		"nik": "910261",
		"nama": "WISNU DHARMA EKO SAPUTRO",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8539007092198568,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0007550588945955,
			"nilai_komparatif_unit": 1.0040090836570004,
			"summary_team": 0.8545454545454546
		}]
	},
	"910263": {
		"nik": "910263",
		"nama": "DENIA FADILA RUSMAN",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8771929824561402,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9949090909090911,
			"nilai_komparatif_unit": 0.9981441070995022,
			"summary_team": 0.8727272727272727
		}]
	},
	"910264": {
		"nik": "910264",
		"nama": "RINA AGUSTINA ANGGRAENI",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8577777777777775,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9962317475270847,
			"nilai_komparatif_unit": 0.9994710644275935,
			"summary_team": 0.8545454545454546
		}]
	},
	"910266": {
		"nik": "910266",
		"nama": "DINA EKA PUTRI",
		"band": "VI",
		"posisi": "OFF 3 DATA ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8925925925925925,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9460580912863071,
			"nilai_komparatif_unit": 0.9491342650496649,
			"summary_team": 0.8444444444444444
		}]
	},
	"910268": {
		"nik": "910268",
		"nama": "MARGARETA KAYA HANJANI",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8577777777777775,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1022138483278383,
			"nilai_komparatif_unit": 1.1057977734092523,
			"summary_team": 0.9454545454545454
		}]
	},
	"910274": {
		"nik": "910274",
		"nama": "AZWAR AL ANHAR",
		"band": "VI",
		"posisi": "OFF 3 PRODUCT & SERVICES DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0327868852459012,
			"nilai_komparatif_unit": 1.0361450637222502,
			"summary_team": 0.84
		}]
	},
	"910277": {
		"nik": "910277",
		"nama": "ALVIN NATANAEL",
		"band": "VI",
		"posisi": "OFF 3 IPTV PRODUCT PLANNING & DEVELOPMEN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.046153846153846,
			"nilai_komparatif_unit": 1.0495554882344065,
			"summary_team": 0.9066666666666667
		}]
	},
	"910282": {
		"nik": "910282",
		"nama": "MUHAMMAD HAKIM HAEKAL",
		"band": "VI",
		"posisi": "OFF 3 SYNERGY ADMINISTRATION & SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8629629629629625,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9406715475889932,
			"nilai_komparatif_unit": 0.9437302066304227,
			"summary_team": 0.8117647058823529
		}]
	},
	"910288": {
		"nik": "910288",
		"nama": "EVELYN PAKPAHAN",
		"band": "VI",
		"posisi": "OFF 3 OFFERING 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0115702479338844,
			"nilai_komparatif_unit": 1.014859439036575,
			"summary_team": 0.9272727272727272
		}]
	},
	"920176": {
		"nik": "920176",
		"nama": "YUNITA SHINTA DEWI",
		"band": "V",
		"posisi": "OFF 2 LOGISTIK & LEGAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8711111111111112,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9948979591836734,
			"nilai_komparatif_unit": 0.9955023501105463,
			"summary_team": 0.8666666666666667
		}]
	},
	"920307": {
		"nik": "920307",
		"nama": "ADIB KHOIRIL IBAD",
		"band": "VI",
		"posisi": "OFF 3 PRODUCT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8527777777777763,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9794979881203312,
			"nilai_komparatif_unit": 0.9826828940368597,
			"summary_team": 0.8352941176470587
		}]
	},
	"920309": {
		"nik": "920309",
		"nama": "PINGKAN RILLY YUNITA",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8394557823129278,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9096802711065242,
			"nilai_komparatif_unit": 0.9126381598543677,
			"summary_team": 0.7636363636363636
		}]
	},
	"920321": {
		"nik": "920321",
		"nama": "AZZAHRA RATU KAMILA",
		"band": "VI",
		"posisi": "OFF 3 FUNCTIONAL DATA SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7822222222222212,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9926470588235309,
			"nilai_komparatif_unit": 0.9958747198590974,
			"summary_team": 0.7764705882352942
		}]
	},
	"920324": {
		"nik": "920324",
		"nama": "IDA BAGUS GEDE BAYU PRIYANTA",
		"band": "VI",
		"posisi": "OFF 3 CYBER SECURITY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9542410714285711,
			"nilai_komparatif_unit": 0.95734385272169,
			"summary_team": 0.8142857142857142
		}]
	},
	"920326": {
		"nik": "920326",
		"nama": "NADIA ASTRID NATHANIA",
		"band": "VI",
		"posisi": "OFF 3 OPERATIONAL AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9262435677530013,
			"nilai_komparatif_unit": 0.9292553132132897,
			"summary_team": 0.8181818181818181
		}]
	},
	"920329": {
		"nik": "920329",
		"nama": "HERU TRIATMAJA ERYADI",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8394557823129278,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9529983792544541,
			"nilai_komparatif_unit": 0.956097119847433,
			"summary_team": 0.8
		}]
	},
	"920331": {
		"nik": "920331",
		"nama": "ALDIO EGAR PRATAMA SUGIAPTO",
		"band": "VI",
		"posisi": "OFF 3 CONTENT PLANNING & RESEARCH",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888882,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9750000000000009,
			"nilai_komparatif_unit": 0.978170280394935,
			"summary_team": 0.8666666666666667
		}]
	},
	"920339": {
		"nik": "920339",
		"nama": "SHELA MANDRASARI PRASETYO",
		"band": "VI",
		"posisi": "OFF 3 ENTERPRISE SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8399999999999997,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9523809523809524,
			"nilai_komparatif_unit": 0.9554776853674571,
			"summary_team": 0.7999999999999998
		}]
	},
	"920341": {
		"nik": "920341",
		"nama": "AL BUKHARI PAHLEVI",
		"band": "VI",
		"posisi": "OFF 3 DATA ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8925925925925925,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.995850622406639,
			"nilai_komparatif_unit": 0.9990887000522788,
			"summary_team": 0.8888888888888888
		}]
	},
	"920346": {
		"nik": "920346",
		"nama": "PUTRI NUR OCTAVIANISYA",
		"band": "VI",
		"posisi": "OFF 3 EXECUTIVE SUPPORT SYSTEM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8266666666666662,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9979838709677424,
			"nilai_komparatif_unit": 1.0012288850196291,
			"summary_team": 0.825
		}]
	},
	"920352": {
		"nik": "920352",
		"nama": "PUTU GEVANI SARASWATI",
		"band": "VI",
		"posisi": "OFF 3 SALES OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9199999999999998,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0079051383399211,
			"nilai_komparatif_unit": 1.0111824120835442,
			"summary_team": 0.9272727272727272
		}]
	},
	"920354": {
		"nik": "920354",
		"nama": "MOHAMMAD RIZKY BANGGA PASACITO",
		"band": "VI",
		"posisi": "OFF 3 ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8699999999999994,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9359605911330054,
			"nilai_komparatif_unit": 0.9390039321714669,
			"summary_team": 0.8142857142857142
		}]
	},
	"920355": {
		"nik": "920355",
		"nama": "SEKAR PRASASTININGRUM",
		"band": "VI",
		"posisi": "OFF 3 CONNECTIVITY SERVICE HANDLING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190479,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9438202247191009,
			"nilai_komparatif_unit": 0.9468891219034796,
			"summary_team": 0.8
		}]
	},
	"920359": {
		"nik": "920359",
		"nama": "FILDZA NOVADIWANTI",
		"band": "VI",
		"posisi": "OFF 3 PROJECT INTEGRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8363636363636366,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9782608695652171,
			"nilai_komparatif_unit": 0.9814417529046159,
			"summary_team": 0.8181818181818182
		}]
	},
	"920369": {
		"nik": "920369",
		"nama": "BUDI CAHYO SURYO PUTRO S",
		"band": "VI",
		"posisi": "OFF 3 IT SERV ASSURANCE CONTROL & REVIEW",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999999,
			"nilai_komparatif_unit": 1.0032515696358297,
			"summary_team": 0.8533333333333333
		}]
	},
	"920379": {
		"nik": "920379",
		"nama": "ANDINA PUTRI ZATA DINI",
		"band": "VI",
		"posisi": "OFF 3 SYNERGY INTEGRATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8629629629629625,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497349154253983,
			"nilai_komparatif_unit": 1.0531482016020661,
			"summary_team": 0.9058823529411766
		}]
	},
	"920381": {
		"nik": "920381",
		"nama": "DAFI' IMAMUL KHAIR FATHARANI SADAT",
		"band": "VI",
		"posisi": "OFF 3 COLLECTION MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9558823529411763,
			"nilai_komparatif_unit": 0.9589904709754254,
			"summary_team": 0.8666666666666667
		}]
	},
	"920382": {
		"nik": "920382",
		"nama": "DIFA DINI ASFARI",
		"band": "VI",
		"posisi": "OFF 3 DATA SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8966666666666664,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0037174721189592,
			"nilai_komparatif_unit": 1.0069811293742532,
			"summary_team": 0.8999999999999998
		}]
	},
	"920386": {
		"nik": "920386",
		"nama": "MUHAMMAD ARIJAL",
		"band": "VI",
		"posisi": "OFF 3 BILLING & REVENUE MGT SYSTEM DEVT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8571428571428564,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.022222222222223,
			"nilai_komparatif_unit": 1.0255460489610715,
			"summary_team": 0.8761904761904762
		}]
	},
	"930210": {
		"nik": "930210",
		"nama": "YOGIE MAHENDRA",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.863636363636363,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0035087719298255,
			"nilai_komparatif_unit": 1.0041183938424982,
			"summary_team": 0.8666666666666667
		}]
	},
	"930223": {
		"nik": "930223",
		"nama": "MANGGALA WISMANTORO",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CILEUNGSI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9560439560439559,
			"nilai_komparatif_unit": 0.9566247435381816,
			"summary_team": 0.8285714285714287
		}]
	},
	"930291": {
		"nik": "930291",
		"nama": "AJI BAGUS PERDANA",
		"band": "V",
		"posisi": "OFF 2 SURVEILLANCE & LOGIC NE SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8571428571428573,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9333333333333332,
			"nilai_komparatif_unit": 0.9339003243430218,
			"summary_team": 0.8
		}]
	},
	"930369": {
		"nik": "930369",
		"nama": "RIFQI FADHLILLAH",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666663,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.038461538461539,
			"nilai_komparatif_unit": 1.039092393843198,
			"summary_team": 0.9
		}]
	},
	"930384": {
		"nik": "930384",
		"nama": "AHMAD EKO ARDIANTO",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8878048780487822,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9830169830169811,
			"nilai_komparatif_unit": 0.9862133311904643,
			"summary_team": 0.8727272727272727
		}]
	},
	"930386": {
		"nik": "930386",
		"nama": "SHERLY TRISIYA DAMAYANTI",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8617283950617275,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9916644959624912,
			"nilai_komparatif_unit": 0.9948889621264934,
			"summary_team": 0.8545454545454545
		}]
	},
	"930387": {
		"nik": "930387",
		"nama": "WILDA HERLINA NUR SAHID",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139858,
			"nilai_komparatif_unit": 0.9892200791514125,
			"summary_team": 0.8545454545454546
		}]
	},
	"930391": {
		"nik": "930391",
		"nama": "DINARRANI GUNITA",
		"band": "VI",
		"posisi": "OFF 3 CONSUMER DATA SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8527777777777769,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9794979881203305,
			"nilai_komparatif_unit": 0.982682894036859,
			"summary_team": 0.8352941176470587
		}]
	},
	"930393": {
		"nik": "930393",
		"nama": "ASTRID ARDIANI",
		"band": "VI",
		"posisi": "OFF 3 MARKETING SERVICE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9850746268656716,
			"nilai_komparatif_unit": 0.9882776656114145,
			"summary_team": 0.88
		}]
	},
	"930395": {
		"nik": "930395",
		"nama": "PUTREN PUSPITASARI",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8324074074074084,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8736980483365344,
			"nilai_komparatif_unit": 0.8765389383813893,
			"summary_team": 0.7272727272727272
		}]
	},
	"930397": {
		"nik": "930397",
		"nama": "ELOK NOVITA PRAMUNTI",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333338,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1650485436893196,
			"nilai_komparatif_unit": 1.1688367801582478,
			"summary_team": 1
		}]
	},
	"930402": {
		"nik": "930402",
		"nama": "LERRY BAYUNUARI NUGRAHA",
		"band": "V",
		"posisi": "OFF 2 OM RADIO & MUX",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.0006074903675233,
			"summary_team": 0.8666666666666667
		}]
	},
	"930404": {
		"nik": "930404",
		"nama": "RIZKYA LAILA NURSYAHBANI",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER RELATIONSHIP MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8407407407407402,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9515418502202647,
			"nilai_komparatif_unit": 0.9546358548076623,
			"summary_team": 0.7999999999999998
		}]
	},
	"930408": {
		"nik": "930408",
		"nama": "DITA APRILANI",
		"band": "VI",
		"posisi": "OFF 3 DEVELOPER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8422222222222243,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.005742666459721,
			"nilai_komparatif_unit": 1.00901290877544,
			"summary_team": 0.8470588235294116
		}]
	},
	"930411": {
		"nik": "930411",
		"nama": "NURUL SANDY PUTRI",
		"band": "VI",
		"posisi": "OFF 3 DATA MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.877777777777778,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.012658227848101,
			"nilai_komparatif_unit": 1.0159509565932452,
			"summary_team": 0.8888888888888888
		}]
	},
	"930417": {
		"nik": "930417",
		"nama": "MADE INDRA WIRA PRAMANA",
		"band": "VI",
		"posisi": "OFF 3 DEVELOPER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.875757575757575,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0276816608996546,
			"nilai_komparatif_unit": 1.0310232393835352,
			"summary_team": 0.8999999999999998
		}]
	},
	"930419": {
		"nik": "930419",
		"nama": "DANIELLA DHYANA",
		"band": "VI",
		"posisi": "OFF 3 DEVELOPER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8422222222222243,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0057426664597213,
			"nilai_komparatif_unit": 1.0090129087754403,
			"summary_team": 0.8470588235294118
		}]
	},
	"930424": {
		"nik": "930424",
		"nama": "MUHAMMAD AZHAR ASHARI",
		"band": "VI",
		"posisi": "OFF 3 BUSINESS INTELLIGENCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8787878787878791,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0114942528735629,
			"nilai_komparatif_unit": 1.014783196873023,
			"summary_team": 0.8888888888888888
		}]
	},
	"930425": {
		"nik": "930425",
		"nama": "PRATAMA DHANTY KUSWAHYUNINGTYAS",
		"band": "VI",
		"posisi": "OFF 3 BUSINESS DEVELOPMENT & INNOVATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.0300764779148628,
			"summary_team": 0.8727272727272727
		}]
	},
	"930431": {
		"nik": "930431",
		"nama": "AGISTA RULLY SARASWATI",
		"band": "VI",
		"posisi": "OFF 3 CONS & DIGI LINE&PRODUCT ALIGNMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8390804597701141,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9534246575342473,
			"nilai_komparatif_unit": 0.9565247842007372,
			"summary_team": 0.7999999999999998
		}]
	},
	"930432": {
		"nik": "930432",
		"nama": "SHEBA LYDIA WARDHANA",
		"band": "VI",
		"posisi": "OFF 3 DEBT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8400000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999999,
			"nilai_komparatif_unit": 1.0032515696358297,
			"summary_team": 0.8400000000000001
		}]
	},
	"930433": {
		"nik": "930433",
		"nama": "RAGASATYA SAFRIANDA",
		"band": "VI",
		"posisi": "OFF 3 DATA COLLECTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999989,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0380622837370255,
			"nilai_komparatif_unit": 1.041437615538925,
			"summary_team": 0.8823529411764706
		}]
	},
	"930435": {
		"nik": "930435",
		"nama": "RIZQY GALAN PRADIPTA",
		"band": "VI",
		"posisi": "OFF 3 DESIGN CAPABILITY PLANNING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8400000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0158730158730158,
			"nilai_komparatif_unit": 1.0191761977252876,
			"summary_team": 0.8533333333333333
		}]
	},
	"930438": {
		"nik": "930438",
		"nama": "KARUNIAWAN",
		"band": "VI",
		"posisi": "OFF 3 ENTERPRISE SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9504950495049503,
			"nilai_komparatif_unit": 0.9535856503469273,
			"summary_team": 0.8
		}]
	},
	"930439": {
		"nik": "930439",
		"nama": "GITA PUSPITA SIKNUN",
		"band": "VI",
		"posisi": "OFF 3 PRODUCT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.872222222222221,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0520794304983154,
			"nilai_komparatif_unit": 1.055500340029005,
			"summary_team": 0.9176470588235295
		}]
	},
	"930442": {
		"nik": "930442",
		"nama": "NADIRA APSARI",
		"band": "VI",
		"posisi": "OFF 3 PRODUCT MGR PERFORMANCE & SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0255102040816322,
			"nilai_komparatif_unit": 1.0288447219224577,
			"summary_team": 0.957142857142857
		}]
	},
	"930446": {
		"nik": "930446",
		"nama": "GILAS AMALANDA",
		"band": "VI",
		"posisi": "OFF 3 TRIBE SUPPORT INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777767,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0320178704393164,
			"nilai_komparatif_unit": 1.0353735484104707,
			"summary_team": 0.9058823529411766
		}]
	},
	"930447": {
		"nik": "930447",
		"nama": "YOSEPHINE BERTHA HANITYA",
		"band": "VI",
		"posisi": "OFF 3 SETTLEMENT & RECONCILIATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8740740740740743,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9915254237288132,
			"nilai_komparatif_unit": 0.9947494376897632,
			"summary_team": 0.8666666666666667
		}]
	},
	"930449": {
		"nik": "930449",
		"nama": "SITA RAHMAHDEWI",
		"band": "VI",
		"posisi": "OFF 3 DATA & PRODUCT CATALOG MGT SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8866666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9172932330827064,
			"nilai_komparatif_unit": 0.9202758759065504,
			"summary_team": 0.8133333333333332
		}]
	},
	"930456": {
		"nik": "930456",
		"nama": "BETARI BRITANIA",
		"band": "VI",
		"posisi": "OFF 3 PERFORMANCE MEASUREMENT&EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8999999999999999,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.98989898989899,
			"nilai_komparatif_unit": 0.9931177153970842,
			"summary_team": 0.8909090909090909
		}]
	},
	"930458": {
		"nik": "930458",
		"nama": "NOVARANI PUTRI SARASWATI",
		"band": "VI",
		"posisi": "OFF 3 WIRELESS PRODUCT DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9466666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9436619718309855,
			"nilai_komparatif_unit": 0.9467303544450786,
			"summary_team": 0.8933333333333332
		}]
	},
	"930461": {
		"nik": "930461",
		"nama": "ADITYA SETIADI",
		"band": "VI",
		"posisi": "OFF 3 QUALITY MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9250000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0614250614250613,
			"nilai_komparatif_unit": 1.0648763589255,
			"summary_team": 0.9818181818181818
		}]
	},
	"930462": {
		"nik": "930462",
		"nama": "I GUSTI AYU KUSDIAH GEMELIARANA",
		"band": "VI",
		"posisi": "OFF 3 IP CORE SERVICE ACTIVATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8711111111111115,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0076530612244892,
			"nilai_komparatif_unit": 1.0109295153218179,
			"summary_team": 0.8777777777777777
		}]
	},
	"930463": {
		"nik": "930463",
		"nama": "KHAIRUNNISA",
		"band": "VI",
		"posisi": "OFF 3 BUSINESS PERFORMANCE & SYNERGY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888882,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9875000000000008,
			"nilai_komparatif_unit": 0.9907109250153828,
			"summary_team": 0.8777777777777778
		}]
	},
	"940076": {
		"nik": "940076",
		"nama": "FACHMI AGUNG CALISTA",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE PEKAYON",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8352941176470584,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1373239436619724,
			"nilai_komparatif_unit": 1.138014857002501,
			"summary_team": 0.95
		}]
	},
	"940111": {
		"nik": "940111",
		"nama": "NURDANI FEBRIANTO",
		"band": "V",
		"posisi": "OFF 2 ACCESS DEPLOYMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8809523809523808,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.983783783783784,
			"nilai_komparatif_unit": 0.9843814229561585,
			"summary_team": 0.8666666666666667
		}]
	},
	"940301": {
		"nik": "940301",
		"nama": "ENISHAPUTRI ENDRIASTARI",
		"band": "VI",
		"posisi": "OFF 3 LEGAL COMPLIANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0357142857142854,
			"nilai_komparatif_unit": 1.0390819828371092,
			"summary_team": 0.8285714285714284
		}]
	},
	"940309": {
		"nik": "940309",
		"nama": "AHMAD RAMDHANI",
		"band": "VI",
		"posisi": "OFF 3 PAYMENT INTEGRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9199999999999999,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9710144927536233,
			"nilai_komparatif_unit": 0.9741718139942117,
			"summary_team": 0.8933333333333333
		}]
	},
	"940319": {
		"nik": "940319",
		"nama": "INTAN PRATAMEI PUTRI",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8394557823129278,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9529983792544541,
			"nilai_komparatif_unit": 0.956097119847433,
			"summary_team": 0.8
		}]
	},
	"940349": {
		"nik": "940349",
		"nama": "ARINI ARIRUSNI",
		"band": "VI",
		"posisi": "OFF 3 IPTV HEADEND O&M",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8260869565217385,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0329824561403518,
			"nilai_komparatif_unit": 1.0363412705290826,
			"summary_team": 0.8533333333333334
		}]
	},
	"940362": {
		"nik": "940362",
		"nama": "THEODORA ROSA VANIA",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL SOLUTION & PARTNERSHIP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.090909090909091,
			"nilai_komparatif_unit": 1.0944562577845418,
			"summary_team": 1
		}]
	},
	"940363": {
		"nik": "940363",
		"nama": "SUKEN ACHMAD AZIZ",
		"band": "V",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE KRANJI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8352941176470584,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.95774647887324,
			"nilai_komparatif_unit": 0.9583283006336851,
			"summary_team": 0.8000000000000002
		}]
	},
	"940367": {
		"nik": "940367",
		"nama": "INGRID NATHANIA",
		"band": "VI",
		"posisi": "OFF 3 OPEX MONITORING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7777777777777781,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0285714285714278,
			"nilai_komparatif_unit": 1.0319159001968528,
			"summary_team": 0.7999999999999998
		}]
	},
	"940368": {
		"nik": "940368",
		"nama": "SYAIFUL ANDY",
		"band": "VI",
		"posisi": "OFF 3 SYSTEM LIFECYCLE & SERVICE DESK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8313725490196076,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0023584905660379,
			"nilai_komparatif_unit": 1.0056177289981787,
			"summary_team": 0.8333333333333331
		}]
	},
	"940369": {
		"nik": "940369",
		"nama": "ELITA ROSALINA",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8878048780487822,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625374625374608,
			"nilai_komparatif_unit": 0.9656672201239963,
			"summary_team": 0.8545454545454546
		}]
	},
	"940379": {
		"nik": "940379",
		"nama": "MUHAMMAD FARIS AL HAKIM",
		"band": "VI",
		"posisi": "OFF 3 BIG DATA&AI PLTFRM CAPABILITY DEVT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0011709601873535,
			"nilai_komparatif_unit": 1.0044263372817734,
			"summary_team": 0.8142857142857142
		}]
	},
	"940387": {
		"nik": "940387",
		"nama": "ANGGA PUTRA YUNAS",
		"band": "VI",
		"posisi": "OFF 3 CX DIGITIZATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9133333333333329,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0443571027512637,
			"nilai_komparatif_unit": 1.047752902595533,
			"summary_team": 0.9538461538461537
		}]
	},
	"940389": {
		"nik": "940389",
		"nama": "RAHMA WIDIANDRAWILLI",
		"band": "VI",
		"posisi": "OFF 3 KNOWLEDGE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8407407407407402,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9655351127235041,
			"nilai_komparatif_unit": 0.9686746173783635,
			"summary_team": 0.8117647058823529
		}]
	},
	"940393": {
		"nik": "940393",
		"nama": "MOCHAMMAD ARIEF RIDWAN",
		"band": "VI",
		"posisi": "OFF 3 DEVELOPER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7636363636363636,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9727891156462584,
			"nilai_komparatif_unit": 0.9759522071967596,
			"summary_team": 0.7428571428571427
		}]
	},
	"940405": {
		"nik": "940405",
		"nama": "REINARD KANEDY",
		"band": "VI",
		"posisi": "OFF 3 ENT INTERNET & WIFI INTERFACE MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.84,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9682539682539681,
			"nilai_komparatif_unit": 0.9714023134569145,
			"summary_team": 0.8133333333333332
		}]
	},
	"940410": {
		"nik": "940410",
		"nama": "SHABRINA AUSTIN GHAISANI",
		"band": "VI",
		"posisi": "OFF 3 DESIGN & ENG CORE NW AREA 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9836065573770488,
			"nilai_komparatif_unit": 0.9868048225926193,
			"summary_team": 0.7999999999999999
		}]
	},
	"940412": {
		"nik": "940412",
		"nama": "M. SAIFUL HARIS",
		"band": "VI",
		"posisi": "OFF 3 IT ADS DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9111111111111112,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0121951219512193,
			"nilai_komparatif_unit": 1.015486344875291,
			"summary_team": 0.9222222222222222
		}]
	},
	"940415": {
		"nik": "940415",
		"nama": "NOVIA INGGRIT DEWIAYU SANTIKASARI",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER EXPERIENCE ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333326,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0703259005145807,
			"nilai_komparatif_unit": 1.0738061397131362,
			"summary_team": 0.9454545454545454
		}]
	},
	"940418": {
		"nik": "940418",
		"nama": " MARGARETHA SINAGA",
		"band": "VI",
		"posisi": "OFF 3 PRODUCT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.0300764779148628,
			"summary_team": 0.8727272727272727
		}]
	},
	"940419": {
		"nik": "940419",
		"nama": "KARTIKA NUGRAHENING JATI",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER EXPERIENCE ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7944444444444447,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9383343928798471,
			"nilai_komparatif_unit": 0.9413854524999901,
			"summary_team": 0.7454545454545455
		}]
	},
	"940420": {
		"nik": "940420",
		"nama": "ARIDHA MEITYA ARIFIN",
		"band": "VI",
		"posisi": "OFF 3 DATA INTEGRITY & SUPPORT SYSTEM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8222222222222221,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9459459459459462,
			"nilai_komparatif_unit": 0.9490217550609205,
			"summary_team": 0.7777777777777778
		}]
	},
	"940421": {
		"nik": "940421",
		"nama": "CAECILIA YOLANDARISA",
		"band": "VI",
		"posisi": "OFF 3 DATA SCIENTIST",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8155555555555545,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9953518192017965,
			"nilai_komparatif_unit": 0.9985882749540811,
			"summary_team": 0.8117647058823529
		}]
	},
	"940425": {
		"nik": "940425",
		"nama": "BENEDICTUS ARDITO ANTADIPUTRA",
		"band": "VI",
		"posisi": "OFF 3 MS BIZ PERFORMANCE & EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9750000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969696,
			"nilai_komparatif_unit": 0.9728500069195926,
			"summary_team": 0.9454545454545454
		}]
	},
	"940427": {
		"nik": "940427",
		"nama": "YULITA AYU RENGGANIS",
		"band": "VI",
		"posisi": "OFF 3 BROADCAST INFRA & FACILITY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7916666666666666,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0827067669172932,
			"nilai_komparatif_unit": 1.086227263365109,
			"summary_team": 0.857142857142857
		}]
	},
	"940429": {
		"nik": "940429",
		"nama": "GILANG WICAHYO PAMBUDI",
		"band": "VI",
		"posisi": "OFF 3 OPERATION SOCIAL MEDIA INTERACTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9846153846153844,
			"nilai_komparatif_unit": 0.9878169301029708,
			"summary_team": 0.8533333333333334
		}]
	},
	"940432": {
		"nik": "940432",
		"nama": "SHANISSALAM WIDHIANA PUTRI PERTIWI",
		"band": "VI",
		"posisi": "OFF 3 DESIGNER",
		"category": [{
			"code": "CF-1-01",
			"kriteria": 0.7848484848484845,
			"kriteria_bp": 0.9970241826690395,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0193050193050195,
			"nilai_komparatif_unit": 1.0223473382323929,
			"summary_team": 0.7999999999999998
		}]
	},
	"940433": {
		"nik": "940433",
		"nama": "ALI ZAENAL ABIDIN",
		"band": "VI",
		"posisi": "OFF 3 IOT PLATFORM DEPLOYMENT SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666666,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0399159663865545,
			"nilai_komparatif_unit": 1.0432973255666718,
			"summary_team": 0.9428571428571426
		}]
	},
	"940455": {
		"nik": "940455",
		"nama": "ARSYA JAVIDIAR",
		"band": "VI",
		"posisi": "OFF 3 PROJECT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197861,
			"nilai_komparatif_unit": 0.9656966980451839,
			"summary_team": 0.8181818181818181
		}]
	},
	"940456": {
		"nik": "940456",
		"nama": "AZIZAH SYIFALIANTI NOOR",
		"band": "VI",
		"posisi": "OFF 3 IOT PLATFORM OPERATION SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9577922077922075,
			"nilai_komparatif_unit": 0.9609065358524992,
			"summary_team": 0.8428571428571427
		}]
	},
	"940459": {
		"nik": "940459",
		"nama": "FITASIA RAMADHANTI MASSEWA",
		"band": "VI",
		"posisi": "OFF 3 CX MEASUREMENT & REPORTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9133333333333329,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.993823694553622,
			"nilai_komparatif_unit": 0.9970551815022008,
			"summary_team": 0.9076923076923076
		}]
	},
	"940460": {
		"nik": "940460",
		"nama": "FRANSISCA NATALIA DAMAYANTI",
		"band": "VI",
		"posisi": "OFF 3 SDN CLOUD COMPUTING DESIGN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8944444444444445,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0683229813664594,
			"nilai_komparatif_unit": 1.07179670793393,
			"summary_team": 0.9555555555555554
		}]
	},
	"946033": {
		"nik": "946033",
		"nama": "YUDA JAKAMANGGALA NATAWIGENA",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8553191489361723,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9990954319312504,
			"nilai_komparatif_unit": 1.0023440603010143,
			"summary_team": 0.8545454545454546
		}]
	},
	"950022": {
		"nik": "950022",
		"nama": "VALLIANT FERLYANDO",
		"band": "V",
		"posisi": "OFF 2 IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8564102564102567,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9341317365269458,
			"nilai_komparatif_unit": 0.934699212558884,
			"summary_team": 0.8
		}]
	},
	"950082": {
		"nik": "950082",
		"nama": "RIFQI JAUHARI",
		"band": "V",
		"posisi": "OFF 2 MONITORING & EVALUATION PROJECT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9285714285714288,
			"nilai_komparatif_unit": 0.9291355267698436,
			"summary_team": 0.8666666666666667
		}]
	},
	"950107": {
		"nik": "950107",
		"nama": "ANDINI RATIH NURDIANTI",
		"band": "VI",
		"posisi": "OFF 3 OFFERING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9532215357458074,
			"nilai_komparatif_unit": 0.9563210019476576,
			"summary_team": 0.8181818181818181
		}]
	},
	"950133": {
		"nik": "950133",
		"nama": "ABDUL TASRIEF S. SAUD",
		"band": "V",
		"posisi": "OFF 2 SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8190476190476191,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0581395348837208,
			"nilai_komparatif_unit": 1.0587823444586586,
			"summary_team": 0.8666666666666667
		}]
	},
	"950136": {
		"nik": "950136",
		"nama": "OKTARIA SARI GINTING",
		"band": "V",
		"posisi": "OFF 2 QUALITY ENHANCEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8848484848484851,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9794520547945202,
			"nilai_komparatif_unit": 0.980047062483259,
			"summary_team": 0.8666666666666667
		}]
	},
	"950157": {
		"nik": "950157",
		"nama": "YANRIKA HASTIN",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8293040293040309,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1181336331512988,
			"nilai_komparatif_unit": 1.1217693225216536,
			"summary_team": 0.9272727272727272
		}]
	},
	"950168": {
		"nik": "950168",
		"nama": "EGIDYUS DICKY DEOFAN YOGIE",
		"band": "VI",
		"posisi": "JUNIOR EXPERT-2 INHOUSE CHANNEL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8296296296296292,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9803571428571436,
			"nilai_komparatif_unit": 0.9835448423751268,
			"summary_team": 0.8133333333333335
		}]
	},
	"950178": {
		"nik": "950178",
		"nama": "AYUDIA PRILLIA",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7716049382716039,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0368000000000013,
			"nilai_komparatif_unit": 1.0401712273984298,
			"summary_team": 0.7999999999999999
		}]
	},
	"950203": {
		"nik": "950203",
		"nama": "NAKA PRIHASTYA PUTRA",
		"band": "VI",
		"posisi": "OFF 3 ENT &OLO DATACOMM PROD DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9075268817204296,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.016192733017378,
			"nilai_komparatif_unit": 1.0194969544522081,
			"summary_team": 0.922222222222222
		}]
	},
	"950209": {
		"nik": "950209",
		"nama": "GRIFIN AZIZAH ANGGARYANTI",
		"band": "VI",
		"posisi": "OFF 3 OPERATION SUPPORT SYSTEM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7948717948717953,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0750733137829906,
			"nilai_komparatif_unit": 1.0785689895263784,
			"summary_team": 0.8545454545454545
		}]
	},
	"950263": {
		"nik": "950263",
		"nama": "SYAHRUL RASYID",
		"band": "VI",
		"posisi": "OFF 3 PRODUCT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.829743589743589,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.049225623500328,
			"nilai_komparatif_unit": 1.0526372536788364,
			"summary_team": 0.8705882352941177
		}]
	},
	"950268": {
		"nik": "950268",
		"nama": "ANGGA HILMAN HIZRIAN",
		"band": "VI",
		"posisi": "OFF 3 SALES PERFORMANCE & OFFERING MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8250000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969695,
			"nilai_komparatif_unit": 0.9728500069195924,
			"summary_team": 0.8
		}]
	},
	"950270": {
		"nik": "950270",
		"nama": "ALIFIA CAHYA MARETA",
		"band": "VI",
		"posisi": "OFF 3 NON LITIGATION SERV&INVESTIGATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9605263157894733,
			"nilai_komparatif_unit": 0.96364953399231,
			"summary_team": 0.8111111111111111
		}]
	},
	"950272": {
		"nik": "950272",
		"nama": "AL FRITA MEGA PURI",
		"band": "VI",
		"posisi": "OFF 3 BUDGETING EXECUTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.893333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0074626865671645,
			"nilai_komparatif_unit": 1.010738521648038,
			"summary_team": 0.8999999999999999
		}]
	},
	"950274": {
		"nik": "950274",
		"nama": "FLAVIA DOMITILLA SARASWATI",
		"band": "VI",
		"posisi": "OFF 3 WHOLESALE DATA SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7822222222222212,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0828877005347606,
			"nilai_komparatif_unit": 1.086408785300833,
			"summary_team": 0.8470588235294116
		}]
	},
	"950277": {
		"nik": "950277",
		"nama": "HAROLD STEFAN P. SIRAIT",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8539007092198568,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9368770764119616,
			"nilai_komparatif_unit": 0.9399233974661279,
			"summary_team": 0.8
		}]
	},
	"950281": {
		"nik": "950281",
		"nama": "ILHAM HADYAN",
		"band": "VI",
		"posisi": "OFF 3 CX DESIGN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0158730158730154,
			"nilai_komparatif_unit": 1.0191761977252871,
			"summary_team": 0.9142857142857141
		}]
	},
	"950292": {
		"nik": "950292",
		"nama": "BEBY ZELVIA",
		"band": "VI",
		"posisi": "OFF 3 MANAGED SERVICE DELIVERY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969697,
			"nilai_komparatif_unit": 0.9728500069195927,
			"summary_team": 0.8727272727272728
		}]
	},
	"950296": {
		"nik": "950296",
		"nama": "NARDA VIRELIA",
		"band": "VI",
		"posisi": "OFF 3 MARKETING COMMUNICATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828873,
			"nilai_komparatif_unit": 1.008616551291636,
			"summary_team": 0.8545454545454546
		}]
	},
	"950310": {
		"nik": "950310",
		"nama": "NOVIANDA ADITYA ISTIQOMAH",
		"band": "VI",
		"posisi": "OFF 3 BUDGET CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8862745098039216,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9528023598820058,
			"nilai_komparatif_unit": 0.9559004631043452,
			"summary_team": 0.8444444444444444
		}]
	},
	"950318": {
		"nik": "950318",
		"nama": "ARYUN NADAA ANIESIYAH",
		"band": "VI",
		"posisi": "OFF 3 OFFERING 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 1,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1,
			"nilai_komparatif_unit": 1.00325156963583,
			"summary_team": 1
		}]
	},
	"950328": {
		"nik": "950328",
		"nama": "ASKARI MUFLIHIN",
		"band": "VI",
		"posisi": "OFF 3  MANAGED SERVICE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9200000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9627329192546581,
			"nilai_komparatif_unit": 0.9658633123823205,
			"summary_team": 0.8857142857142856
		}]
	},
	"950330": {
		"nik": "950330",
		"nama": "ARINING PANGESTU",
		"band": "VI",
		"posisi": "OFF 3 IH3P INTEGRATION 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888884,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0750000000000004,
			"nilai_komparatif_unit": 1.0784954373585176,
			"summary_team": 0.9555555555555554
		}]
	},
	"950332": {
		"nik": "950332",
		"nama": "NURHASANUDIN",
		"band": "VI",
		"posisi": "OFF 3 CONNECTIVITY SERVICE HANDLING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000003,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.997402597402597,
			"nilai_komparatif_unit": 1.0006457214030091,
			"summary_team": 0.8727272727272727
		}]
	},
	"950333": {
		"nik": "950333",
		"nama": "SUCI RAHMADANI",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8595238095238087,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9854978002281254,
			"nilai_komparatif_unit": 0.9887022149515243,
			"summary_team": 0.8470588235294118
		}]
	},
	"950334": {
		"nik": "950334",
		"nama": "ANDRE CHRISNATA",
		"band": "VI",
		"posisi": "OFF 3 IT PROCUREMENT PROCESS & ADM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8148148148148151,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.048760330578512,
			"nilai_komparatif_unit": 1.052170447824684,
			"summary_team": 0.8545454545454546
		}]
	},
	"950339": {
		"nik": "950339",
		"nama": "MUHAMMAD ABYAN SAFITRA",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL DELIVERY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.0300764779148628,
			"summary_team": 0.8727272727272727
		}]
	},
	"950342": {
		"nik": "950342",
		"nama": "ANANTASSA FITRI ANDINI",
		"band": "VI",
		"posisi": "OFF 3 OTT PLATFORM OPERATION & MAINTENAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7666666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0869565217391302,
			"nilai_komparatif_unit": 1.0904908365606845,
			"summary_team": 0.8333333333333334
		}]
	},
	"950346": {
		"nik": "950346",
		"nama": "FAHMI SALFIYARI",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL LIFE & COLLABORATION DEVT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9848484848484845,
			"nilai_komparatif_unit": 0.988050788277711,
			"summary_team": 0.8666666666666665
		}]
	},
	"950348": {
		"nik": "950348",
		"nama": "M BAHRUL WUSTO",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER EXPERIENCE ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333332,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9935064935064937,
			"nilai_komparatif_unit": 0.9967369490537792,
			"summary_team": 0.9272727272727274
		}]
	},
	"950350": {
		"nik": "950350",
		"nama": "BALINDO DHARMA BAKTI SAMUEL SITORUS",
		"band": "VI",
		"posisi": "OFF 3 ENT BIZ PORTFOLIO DATA ADM & SUPP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9277777777777777,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.029940119760479,
			"nilai_komparatif_unit": 1.033289041780615,
			"summary_team": 0.9555555555555554
		}]
	},
	"950352": {
		"nik": "950352",
		"nama": "ARZINAR YOGA RUSDYANTORO",
		"band": "VI",
		"posisi": "OFF 3 BUSINESS PLANNING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666666,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882353,
			"nilai_komparatif_unit": 0.9442367714219575,
			"summary_team": 0.8533333333333333
		}]
	},
	"950353": {
		"nik": "950353",
		"nama": "RIDLO AANISAH",
		"band": "VI",
		"posisi": "OFF 3 ACCESS NON FIBER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9846153846153841,
			"nilai_komparatif_unit": 0.9878169301029704,
			"summary_team": 0.8533333333333332
		}]
	},
	"950354": {
		"nik": "950354",
		"nama": "BERNAULLY CINDITHA SARNIEM",
		"band": "VI",
		"posisi": "OFF 3 MEDIA PLACEMENT & SPONSORSHIP EVAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8704761904761896,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9318016046681263,
			"nilai_komparatif_unit": 0.9348314224724829,
			"summary_team": 0.8111111111111111
		}]
	},
	"950355": {
		"nik": "950355",
		"nama": "ADE VREYYUNING MONIKA",
		"band": "VI",
		"posisi": "OFF 3 SCHEDULLING ANALYST",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888882,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.050000000000001,
			"nilai_komparatif_unit": 1.0534141481176222,
			"summary_team": 0.9333333333333333
		}]
	},
	"950357": {
		"nik": "950357",
		"nama": "ASTRID PERMATASARI",
		"band": "VI",
		"posisi": "OFF 3 INTERNET PROD PORTFOLIO & QUAL MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0447761194029848,
			"nilai_komparatif_unit": 1.0481732817090756,
			"summary_team": 0.9333333333333332
		}]
	},
	"950364": {
		"nik": "950364",
		"nama": "MUHAMMAD NAUFAL MAULANA",
		"band": "VI",
		"posisi": "OFF 3 LEGAL & COMPLIANCE - 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9056603773584906,
			"nilai_komparatif_unit": 0.9086051951418838,
			"summary_team": 0.8
		}]
	},
	"950365": {
		"nik": "950365",
		"nama": "ARGA JONATHAN ARITONANG",
		"band": "VI",
		"posisi": "OFF 3 ACCESS PLANNING & SINERGY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8733333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.977099236641221,
			"nilai_komparatif_unit": 0.9802763428502762,
			"summary_team": 0.8533333333333333
		}]
	},
	"950367": {
		"nik": "950367",
		"nama": "MAULANA ZAKY HAQ",
		"band": "VI",
		"posisi": "OFF 3 PRODUCT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9079365079365075,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9912587412587417,
			"nilai_komparatif_unit": 0.9944818880830696,
			"summary_team": 0.8999999999999999
		}]
	},
	"950371": {
		"nik": "950371",
		"nama": "ALIYA RAHMA NAJIHATI",
		"band": "VI",
		"posisi": "OFF 3 BUDGET CONTROL & ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0032515696358302,
			"summary_team": 0.8
		}]
	},
	"950373": {
		"nik": "950373",
		"nama": "GUNTUR KONDANG PRAKOSO",
		"band": "VI",
		"posisi": "OFF 3  TDM SWITCH",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.970149253731343,
			"nilai_komparatif_unit": 0.9733037615869988,
			"summary_team": 0.8666666666666665
		}]
	},
	"950380": {
		"nik": "950380",
		"nama": "NARPATI BROTO KUSUMO",
		"band": "VI",
		"posisi": "OFF 3 QUALITY SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8869565217391299,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9896514161220048,
			"nilai_komparatif_unit": 0.9928693366167232,
			"summary_team": 0.8777777777777777
		}]
	},
	"950394": {
		"nik": "950394",
		"nama": "MUHAMMAD BINTANG ADHITYA URIP",
		"band": "VI",
		"posisi": "OFF 3 CFU & CRO DATA ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8565656565656555,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.01179245283019,
			"nilai_komparatif_unit": 1.0150823664475745,
			"summary_team": 0.8666666666666667
		}]
	},
	"950395": {
		"nik": "950395",
		"nama": "MUHAMMAD ISRADI AZHAR",
		"band": "VI",
		"posisi": "OFF 3 BROADBAND OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8711111111111103,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0076530612244907,
			"nilai_komparatif_unit": 1.0109295153218194,
			"summary_team": 0.8777777777777777
		}]
	},
	"950397": {
		"nik": "950397",
		"nama": "ELLIN DEVIHANA PRATIWI",
		"band": "VI",
		"posisi": "OFF 3 SALES & CHANNEL DATA SUPP & EVAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.877777777777777,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9620253164556971,
			"nilai_komparatif_unit": 0.9651534087635841,
			"summary_team": 0.8444444444444444
		}]
	},
	"950399": {
		"nik": "950399",
		"nama": "ANANDA DWI PUTRI",
		"band": "VI",
		"posisi": "OFF 3 DATA SCIENTIST",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8155555555555545,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9665010418336284,
			"nilai_komparatif_unit": 0.9696436872742525,
			"summary_team": 0.788235294117647
		}]
	},
	"950402": {
		"nik": "950402",
		"nama": "FAZA FIKRIANSYAH RAHMAN",
		"band": "VI",
		"posisi": "OFF 3 ORDER & COMPLEX EVENT SYSTEM ADM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9545454545454543,
			"nilai_komparatif_unit": 0.9576492255614737,
			"summary_team": 0.84
		}]
	},
	"950404": {
		"nik": "950404",
		"nama": "GIAN PERMANA",
		"band": "VI",
		"posisi": "OFF 3 TRIBE SUPPORT INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8422222222222243,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.047648610895543,
			"nilai_komparatif_unit": 1.0510551133077504,
			"summary_team": 0.8823529411764706
		}]
	},
	"950408": {
		"nik": "950408",
		"nama": "JA'FAR SHODIQ YUSUF BACHTIAR",
		"band": "VI",
		"posisi": "OFF 3 MEDIATION SYSTEM DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0597014925373132,
			"nilai_komparatif_unit": 1.063147185733491,
			"summary_team": 0.9466666666666665
		}]
	},
	"950470": {
		"nik": "950470",
		"nama": "AFIFAH AYU RAKHMANIA",
		"band": "VI",
		"posisi": "OFF 3 OPEX MONITORING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7948717948717953,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0735483870967737,
			"nilai_komparatif_unit": 1.0770391044348517,
			"summary_team": 0.8533333333333333
		}]
	},
	"950471": {
		"nik": "950471",
		"nama": "AHMAD DZULFIQAR ADI PRAJA NAGARA",
		"band": "VI",
		"posisi": "OFF 3 SEGMENT ADMINISTRATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9898989898989896,
			"nilai_komparatif_unit": 0.9931177153970838,
			"summary_team": 0.8909090909090909
		}]
	},
	"950472": {
		"nik": "950472",
		"nama": "AHMAD RODIK WIJAYA",
		"band": "VI",
		"posisi": "OFF 3 QUALITY MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9128014842300556,
			"nilai_komparatif_unit": 0.9157695218197185,
			"summary_team": 0.7454545454545455
		}]
	},
	"950474": {
		"nik": "950474",
		"nama": "AMIRA LAKSMI SAFIRA",
		"band": "VI",
		"posisi": "OFF 3 UNIT PERFORMANCE MGT & DATA ANALYS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272727,
			"nilai_komparatif_unit": 1.0260527416730079,
			"summary_team": 0.8181818181818181
		}]
	},
	"950476": {
		"nik": "950476",
		"nama": "ANDRE RIZKI DEWO NUGRAHA",
		"band": "VI",
		"posisi": "OFF 3 FACILITY & ASSET SYSTEM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8695035460992895,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9857841994873003,
			"nilai_komparatif_unit": 0.9889895454578341,
			"summary_team": 0.857142857142857
		}]
	},
	"950478": {
		"nik": "950478",
		"nama": "ANISA NUR AFIYAH",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER INTERACTION MGT OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.92,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9855072463768114,
			"nilai_komparatif_unit": 0.9887116918150206,
			"summary_team": 0.9066666666666665
		}]
	},
	"950479": {
		"nik": "950479",
		"nama": "ARIF MUNANDAR",
		"band": "VI",
		"posisi": "OFF 3 DATA CENTRE PLANNING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0447761194029848,
			"nilai_komparatif_unit": 1.0481732817090756,
			"summary_team": 0.9333333333333332
		}]
	},
	"950480": {
		"nik": "950480",
		"nama": "AUDREY SARAH KRISTINA",
		"band": "VI",
		"posisi": "OFF 3 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8545454545454548,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9985815602836875,
			"nilai_komparatif_unit": 1.0018285177640056,
			"summary_team": 0.8533333333333333
		}]
	},
	"950482": {
		"nik": "950482",
		"nama": "BRUGUIERA ARDHANI FEBRIAN AGRADRIYA",
		"band": "VI",
		"posisi": "OFF 3 QUALITY ENHANCEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999997,
			"nilai_komparatif_unit": 1.0032515696358295,
			"summary_team": 0.8666666666666665
		}]
	},
	"950485": {
		"nik": "950485",
		"nama": "CITRA NADYA ULFA",
		"band": "VI",
		"posisi": "OFF 3 DATA WARE HOUSE ADMINISTRATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9466666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9577464788732393,
			"nilai_komparatif_unit": 0.9608606582427666,
			"summary_team": 0.9066666666666665
		}]
	},
	"950489": {
		"nik": "950489",
		"nama": "DIAN PRASTIWI",
		"band": "VI",
		"posisi": "OFF 3 BUSINESS PERFORMANCE SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.849382716049382,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1066860465116288,
			"nilai_komparatif_unit": 1.1102845132568626,
			"summary_team": 0.9400000000000001
		}]
	},
	"950492": {
		"nik": "950492",
		"nama": "EKO WAHYUDI",
		"band": "VI",
		"posisi": "OFF 3 REPAIR & CALIBARATION MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9531249999999998,
			"nilai_komparatif_unit": 0.9562241523091501,
			"summary_team": 0.8133333333333332
		}]
	},
	"950496": {
		"nik": "950496",
		"nama": "GOLDVINA ULFAH",
		"band": "VI",
		"posisi": "OFF 3 SOLUTION ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9500000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9186602870813396,
			"nilai_komparatif_unit": 0.9216473749764561,
			"summary_team": 0.8727272727272727
		}]
	},
	"950497": {
		"nik": "950497",
		"nama": "HARUM AMALIA SANDI",
		"band": "VI",
		"posisi": "OFF 3 SBC FOR SIP TRUNKING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9846153846153841,
			"nilai_komparatif_unit": 0.9878169301029704,
			"summary_team": 0.8533333333333332
		}]
	},
	"950502": {
		"nik": "950502",
		"nama": "KHAIRIN NISA",
		"band": "VI",
		"posisi": "OFF 3 SALES OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9898989898989898,
			"nilai_komparatif_unit": 0.993117715397084,
			"summary_team": 0.8909090909090909
		}]
	},
	"950503": {
		"nik": "950503",
		"nama": "KHAIRINA LAILA NUR PRATIWI",
		"band": "VI",
		"posisi": "OFF 3 ACCESS IT TOOL MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8484848484848488,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9428571428571424,
			"nilai_komparatif_unit": 0.945922908513782,
			"summary_team": 0.7999999999999999
		}]
	},
	"950506": {
		"nik": "950506",
		"nama": "MUHAMMAD HADYAN ARIF",
		"band": "VI",
		"posisi": "OFFICER 3 TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.071428571428571,
			"nilai_komparatif_unit": 1.0749123960383886,
			"summary_team": 0.9285714285714285
		}]
	},
	"950511": {
		"nik": "950511",
		"nama": "NABIEL GILDAS HALVAWY",
		"band": "VI",
		"posisi": "OFFICER 3 TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7851851851851855,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.073450134770889,
			"nilai_komparatif_unit": 1.0769405326346875,
			"summary_team": 0.8428571428571427
		}]
	},
	"950513": {
		"nik": "950513",
		"nama": "NISYA KURNIASARI",
		"band": "VI",
		"posisi": "OFF 3 INTERCON & MS BILLING VERIF&SETT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.048128342245989,
			"nilai_komparatif_unit": 1.0515364045380888,
			"summary_team": 0.8909090909090909
		}]
	},
	"950514": {
		"nik": "950514",
		"nama": "NURLAILA SAFITRI",
		"band": "VI",
		"posisi": "OFF 3 MANAGED SERVICE DEPLOYMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8839506172839499,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9873031995937033,
			"nilai_komparatif_unit": 0.9905134846988599,
			"summary_team": 0.8727272727272728
		}]
	},
	"950519": {
		"nik": "950519",
		"nama": "RADITYA ADHITAMA SANTOSO",
		"band": "VI",
		"posisi": "JUNIOR EXPERT-2 CREATIVE & PRODUCTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8296296296296292,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0580357142857149,
			"nilai_komparatif_unit": 1.06147599108791,
			"summary_team": 0.8777777777777778
		}]
	},
	"950524": {
		"nik": "950524",
		"nama": "SHIGHIA AJENG SAVITRI",
		"band": "VI",
		"posisi": "OFF 3 SOLUTION DESIGN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8770053475935828,
			"nilai_komparatif_unit": 0.8798569915522786,
			"summary_team": 0.7454545454545454
		}]
	},
	"950527": {
		"nik": "950527",
		"nama": "TEFANI RIZKA SUCIATY",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER PROFILING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000003,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0312499999999996,
			"nilai_komparatif_unit": 1.0346031811869492,
			"summary_team": 0.825
		}]
	},
	"950528": {
		"nik": "950528",
		"nama": "ULFA ZHAFIRAH",
		"band": "VI",
		"posisi": "OFF 3 PACKAGE & PRICING EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666663,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.034267912772586,
			"nilai_komparatif_unit": 1.0376309069130705,
			"summary_team": 0.9222222222222222
		}]
	},
	"950530": {
		"nik": "950530",
		"nama": "VALYA NURFADILA DJOHAN",
		"band": "VI",
		"posisi": "OFFICER 3 TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7888888888888891,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9096934548467273,
			"nilai_komparatif_unit": 0.9126513864624201,
			"summary_team": 0.7176470588235294
		}]
	},
	"955466": {
		"nik": "955466",
		"nama": "ISKANDAR AGUNG",
		"band": "VI",
		"posisi": "JUNIOR EXPERT-2 UI/UX EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8878787878787874,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0386803185438003,
			"nilai_komparatif_unit": 1.0420576599289115,
			"summary_team": 0.9222222222222222
		}]
	},
	"960059": {
		"nik": "960059",
		"nama": "FEISAL RAMADHAN MAULANA",
		"band": "VI",
		"posisi": "OFF 3 DATA SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8044444444444433,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0383490412739695,
			"nilai_komparatif_unit": 1.0417253054879692,
			"summary_team": 0.8352941176470587
		}]
	},
	"960060": {
		"nik": "960060",
		"nama": "SYANITA FEBRIANTI",
		"band": "VI",
		"posisi": "OFF 3 TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.829743589743589,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.049225623500328,
			"nilai_komparatif_unit": 1.0526372536788364,
			"summary_team": 0.8705882352941177
		}]
	},
	"960062": {
		"nik": "960062",
		"nama": "THERESIA VANIA HAMOLIN",
		"band": "VI",
		"posisi": "OFF 3 BUSINESS ALLIANCE SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999999,
			"nilai_komparatif_unit": 1.0032515696358297,
			"summary_team": 0.8933333333333333
		}]
	},
	"960068": {
		"nik": "960068",
		"nama": "I MADE FANDY ADITYA WIRANA",
		"band": "VI",
		"posisi": "OFF 3 DEVELOPER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.875757575757575,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9787444389520522,
			"nilai_komparatif_unit": 0.981926894650986,
			"summary_team": 0.8571428571428571
		}]
	},
	"960069": {
		"nik": "960069",
		"nama": "IVALIA RIZKITA PUTRI",
		"band": "VI",
		"posisi": "OFF 3 SALES - 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8390804597701145,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0515713134568903,
			"nilai_komparatif_unit": 1.0549905708096365,
			"summary_team": 0.8823529411764706
		}]
	},
	"960084": {
		"nik": "960084",
		"nama": "ABIKARAMI ANANDADIGA",
		"band": "VI",
		"posisi": "OFF 3 SALES PERFORMANCE & OFFERING MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7944444444444447,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9612205975842336,
			"nilai_komparatif_unit": 0.9643460732926727,
			"summary_team": 0.7636363636363637
		}]
	},
	"960089": {
		"nik": "960089",
		"nama": "ARDHI SURYA IBRAHIM",
		"band": "VI",
		"posisi": "OFF 3 IT INTERFACING & APPLICATION MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.980392156862745,
			"nilai_komparatif_unit": 0.9835799702312057,
			"summary_team": 0.8823529411764706
		}]
	},
	"960094": {
		"nik": "960094",
		"nama": "NAMIRA FITRI NADYA",
		"band": "VI",
		"posisi": "OFF 3 CONS PORT GOVERNANCE&INVEST",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0350404312668462,
			"nilai_komparatif_unit": 1.0384059373050099,
			"summary_team": 0.914285714285714
		}]
	},
	"960105": {
		"nik": "960105",
		"nama": "DEAN DZAKI AZKA",
		"band": "VI",
		"posisi": "OFF 3 OFFERING 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0686456400742113,
			"nilai_komparatif_unit": 1.0721204157889386,
			"summary_team": 0.8727272727272727
		}]
	},
	"960106": {
		"nik": "960106",
		"nama": "RAUUF ASHSHALLY WAHYUDI",
		"band": "VI",
		"posisi": "OFF 3 IOT PLATFORM DEV'T",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0074626865671639,
			"nilai_komparatif_unit": 1.0107385216480373,
			"summary_team": 0.8999999999999999
		}]
	},
	"960118": {
		"nik": "960118",
		"nama": "RACHMA PRATIWI",
		"band": "VI",
		"posisi": "OFF 3 ADVANCED MS OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9499999999999998,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9952153110047849,
			"nilai_komparatif_unit": 0.9984513228911611,
			"summary_team": 0.9454545454545454
		}]
	},
	"960128": {
		"nik": "960128",
		"nama": "DANIEL ABDULLAH KURNIAWAN",
		"band": "VI",
		"posisi": "OFF 3 DATA ANALISYS DIGITAL TELCO 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888886,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9926470588235297,
			"nilai_komparatif_unit": 0.9958747198590961,
			"summary_team": 0.8823529411764706
		}]
	},
	"960131": {
		"nik": "960131",
		"nama": "APRILLIA PUTRI HASPITASARI",
		"band": "VI",
		"posisi": "OFF 3 TRIBE SUPPORT INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8527777777777763,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0346809733665472,
			"nilai_komparatif_unit": 1.0380453106023169,
			"summary_team": 0.8823529411764706
		}]
	},
	"960132": {
		"nik": "960132",
		"nama": "MUTIARA ULFACH",
		"band": "VI",
		"posisi": "OFF 3 OSS FULFILLMENT DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9600000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.0032515696358297,
			"summary_team": 0.96
		}]
	},
	"960141": {
		"nik": "960141",
		"nama": "FITYA LUTHFI",
		"band": "VI",
		"posisi": "OFF 3 IOT PLATFORM RESEARCH",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8266666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9504608294930874,
			"nilai_komparatif_unit": 0.9535513190663129,
			"summary_team": 0.7857142857142856
		}]
	},
	"960144": {
		"nik": "960144",
		"nama": "RAGESA MARIO JUNIOR",
		"band": "VI",
		"posisi": "OFF 3 OPERATION GPON NODE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8388888888888892,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9536423841059598,
			"nilai_komparatif_unit": 0.9567432187255592,
			"summary_team": 0.7999999999999999
		}]
	},
	"960145": {
		"nik": "960145",
		"nama": "GRANITHIO KARYA NUGRAHA",
		"band": "VI",
		"posisi": "OFF 3 COMPLAIN HANDLING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144382,
			"nilai_komparatif_unit": 1.0300764779148626,
			"summary_team": 0.8727272727272727
		}]
	},
	"960152": {
		"nik": "960152",
		"nama": "CINDY SAFFANAH YUSUF",
		"band": "VI",
		"posisi": "OFF 3 BUSINESS LEGAL & COMPLIANCE 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0102673148780383,
			"summary_team": 0.8727272727272727
		}]
	},
	"960159": {
		"nik": "960159",
		"nama": "MUHAMMAD IRFAN AKBAR PRATAMA",
		"band": "VI",
		"posisi": "OFF 3 CFUC CFUW INTERFACE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9380952380952379,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.966497461928934,
			"nilai_komparatif_unit": 0.9696400957292488,
			"summary_team": 0.9066666666666665
		}]
	},
	"960164": {
		"nik": "960164",
		"nama": "ARIEF HUDA",
		"band": "VI",
		"posisi": "OFF 3 TRANSPORT BACKBONE 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8681481481481468,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9726962457337898,
			"nilai_komparatif_unit": 0.9758590353113035,
			"summary_team": 0.8444444444444443
		}]
	},
	"960165": {
		"nik": "960165",
		"nama": "ALFACANO ABRAR",
		"band": "VI",
		"posisi": "OFF 3 TRIBE SUPPORT INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8422222222222243,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.033679962750269,
			"nilai_komparatif_unit": 1.0370410451303136,
			"summary_team": 0.8705882352941177
		}]
	},
	"960172": {
		"nik": "960172",
		"nama": "SHABRINA NADHIRA FIRIANTI",
		"band": "VI",
		"posisi": "OFF 3 TRIBE SUPPORT INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.872222222222221,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9981266391907095,
			"nilai_komparatif_unit": 1.001372117463415,
			"summary_team": 0.8705882352941177
		}]
	},
	"960195": {
		"nik": "960195",
		"nama": "TIFFANY CLARA ALESI",
		"band": "VI",
		"posisi": "OFF 3 INTERNATIONAL & LOCAL ACQUISITION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9789473684210522,
			"nilai_komparatif_unit": 0.9821304839592857,
			"summary_team": 0.8266666666666667
		}]
	},
	"960196": {
		"nik": "960196",
		"nama": "WEDAR PANJI MARDYANINGSIH",
		"band": "VI",
		"posisi": "OFF 3 BIG DATA PLTFRM&AI CAPABILITY DEVT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8685185185185175,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9346544587984459,
			"nilai_komparatif_unit": 0.937693552856668,
			"summary_team": 0.8117647058823529
		}]
	},
	"960199": {
		"nik": "960199",
		"nama": "HEPTA SEPTIANI",
		"band": "VI",
		"posisi": "OFF 3 DATA MART MGT & DATA MINING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8972222222222217,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9309779639409949,
			"nilai_komparatif_unit": 0.9340051036201722,
			"summary_team": 0.8352941176470587
		}]
	},
	"960200": {
		"nik": "960200",
		"nama": "EVA DANTI RAHMANITA",
		"band": "VI",
		"posisi": "OFF 3 DEVELOPER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8422222222222243,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0336799627502689,
			"nilai_komparatif_unit": 1.0370410451303134,
			"summary_team": 0.8705882352941174
		}]
	},
	"960255": {
		"nik": "960255",
		"nama": "ADITYA DWI PUTRANTO",
		"band": "VI",
		"posisi": "OFF 3 GENERAL RESOURCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9388888888888888,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9295320064550835,
			"nilai_komparatif_unit": 0.9325544445028049,
			"summary_team": 0.8727272727272727
		}]
	},
	"960256": {
		"nik": "960256",
		"nama": "AHMAD ARYA SAWRAJA",
		"band": "VI",
		"posisi": "OFF 3 IT PROGRAM CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7555555555555555,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9595588235294119,
			"nilai_komparatif_unit": 0.9626788958637928,
			"summary_team": 0.7250000000000001
		}]
	},
	"960258": {
		"nik": "960258",
		"nama": "ALDIAN SYAH",
		"band": "VI",
		"posisi": "OFF 3 QUALITY & DATA SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8774774774774767,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0180698151950727,
			"nilai_komparatif_unit": 1.021380140093316,
			"summary_team": 0.8933333333333333
		}]
	},
	"960260": {
		"nik": "960260",
		"nama": "ALVINA RAHMATIANA SARI",
		"band": "VI",
		"posisi": "JUNIOR EXPERT-2 PRODUCT EVALUATION & RET",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8878787878787874,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9911262798634818,
			"nilai_komparatif_unit": 0.994348995980359,
			"summary_team": 0.88
		}]
	},
	"960261": {
		"nik": "960261",
		"nama": "ANA WIDYA PERMATASARI",
		"band": "VI",
		"posisi": "OFF 3 SEGMENT INDUSTRIAL ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.962566844919786,
			"nilai_komparatif_unit": 0.9656966980451838,
			"summary_team": 0.8181818181818181
		}]
	},
	"960267": {
		"nik": "960267",
		"nama": "ANNISAA ADI RAMADHAN",
		"band": "VI",
		"posisi": "OFF 3 DATA SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8433333333333329,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9655561829474876,
			"nilai_komparatif_unit": 0.9686957561136474,
			"summary_team": 0.8142857142857142
		}]
	},
	"960273": {
		"nik": "960273",
		"nama": "BIMA LAKSMANA PRAMUDITA",
		"band": "VI",
		"posisi": "OFF 3 ASSURANCE DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0123456790123453,
			"nilai_komparatif_unit": 1.0156373914831855,
			"summary_team": 0.911111111111111
		}]
	},
	"960281": {
		"nik": "960281",
		"nama": "FADHILA ISNAINI",
		"band": "VI",
		"posisi": "OFF 3 DATA QUALITY MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.987723214285714,
			"nilai_komparatif_unit": 0.9909348650978897,
			"summary_team": 0.8428571428571426
		}]
	},
	"960282": {
		"nik": "960282",
		"nama": "FANNY ISTIFADAH",
		"band": "VI",
		"posisi": "OFF 3 CREATIVE & EXECUTIVE REPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7972222222222217,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0772699323631896,
			"nilai_komparatif_unit": 1.0807727505648543,
			"summary_team": 0.8588235294117645
		}]
	},
	"960283": {
		"nik": "960283",
		"nama": "FEBRIAN SUKMA WARDHANA",
		"band": "VI",
		"posisi": "OFF 3 SEGMENT QUALITY & COMPLIANCE SUPP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545452,
			"nilai_komparatif_unit": 1.028788882317469,
			"summary_team": 0.8545454545454545
		}]
	},
	"960286": {
		"nik": "960286",
		"nama": "GARREY BALDO GUNAWAN",
		"band": "VI",
		"posisi": "OFF 3 FINANCIAL REPORTING PLATFORM DEVEL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8999999999999999,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9814814814814815,
			"nilai_komparatif_unit": 0.984672836864796,
			"summary_team": 0.8833333333333333
		}]
	},
	"960287": {
		"nik": "960287",
		"nama": "GHINA SALSABILA",
		"band": "VI",
		"posisi": "OFF 3 PRODUCT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7888888888888891,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.028997514498757,
			"nilai_komparatif_unit": 1.0323433715722456,
			"summary_team": 0.8117647058823529
		}]
	},
	"960288": {
		"nik": "960288",
		"nama": "GUSTOM MAUDAN",
		"band": "VI",
		"posisi": "OFF 3 FRAUD & AUDIT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0769230769230769,
			"nilai_komparatif_unit": 1.0804247673001244,
			"summary_team": 0.9333333333333331
		}]
	},
	"960291": {
		"nik": "960291",
		"nama": "ILHAM MUHAMMAD MISBAHUDDIN",
		"band": "VI",
		"posisi": "OFF 3 MANAGED OPERATION PROTOTYPING MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9075268817204296,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.98436018957346,
			"nilai_komparatif_unit": 0.9875609052765969,
			"summary_team": 0.8933333333333332
		}]
	},
	"960293": {
		"nik": "960293",
		"nama": "KANESA PERMATA PUTRI",
		"band": "VI",
		"posisi": "OFF 3 LEGAL & COMPLIANCE - 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0129870129870129,
			"nilai_komparatif_unit": 1.0162808107999315,
			"summary_team": 0.9454545454545454
		}]
	},
	"960294": {
		"nik": "960294",
		"nama": "LINDA RIZKI YULIANI",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER ANALYSIS & SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8399999999999989,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.969387755102042,
			"nilai_komparatif_unit": 0.9725397868918771,
			"summary_team": 0.8142857142857142
		}]
	},
	"960295": {
		"nik": "960295",
		"nama": "MASTALITA NICHOLE ANDASTIA ISWARI",
		"band": "VI",
		"posisi": "OFF 3 SEGMENT QUALITY & COMPLIANCE SUPP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200683,
			"nilai_komparatif_unit": 1.0531560216417284,
			"summary_team": 0.9272727272727272
		}]
	},
	"960296": {
		"nik": "960296",
		"nama": "MAULIDINDA NABILA",
		"band": "VI",
		"posisi": "OFF 3 PROGRAM & OPERATIONS SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200685,
			"nilai_komparatif_unit": 1.0531560216417286,
			"summary_team": 0.9272727272727272
		}]
	},
	"960297": {
		"nik": "960297",
		"nama": "MOHAMMAD ZOLAKUSUMA",
		"band": "VI",
		"posisi": "OFF 3 SOLUTION DESIGN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.015301530153015,
			"nilai_komparatif_unit": 1.0186028537796723,
			"summary_team": 0.8545454545454546
		}]
	},
	"960300": {
		"nik": "960300",
		"nama": "MUHAMMAD DIAN CORDOVA",
		"band": "VI",
		"posisi": "OFF 3 DATA SECURITY & GOVERNANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.071428571428571,
			"nilai_komparatif_unit": 1.0749123960383886,
			"summary_team": 0.9428571428571427
		}]
	},
	"960301": {
		"nik": "960301",
		"nama": "MUHAMMAD HILMY AZIZ",
		"band": "VI",
		"posisi": "OFFICER 3 TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9650349650349647,
			"nilai_komparatif_unit": 0.9681728434247865,
			"summary_team": 0.8363636363636363
		}]
	},
	"960304": {
		"nik": "960304",
		"nama": "MURSAL HABIB",
		"band": "VI",
		"posisi": "OFF 3 ASSET SETTLEMENT & VALUE PERFORM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139859,
			"nilai_komparatif_unit": 0.9892200791514126,
			"summary_team": 0.8545454545454546
		}]
	},
	"960306": {
		"nik": "960306",
		"nama": "NABILA AURIEL SALMA FAUZTINA",
		"band": "VI",
		"posisi": "OFF 3 SALES - 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8925925925925923,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9226263119355629,
			"nilai_komparatif_unit": 0.9256262956366703,
			"summary_team": 0.8235294117647058
		}]
	},
	"960307": {
		"nik": "960307",
		"nama": "NATHASIA MUNTHE",
		"band": "VI",
		"posisi": "OFF 3 ENTERPRISE SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000004,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969694,
			"nilai_komparatif_unit": 0.9728500069195923,
			"summary_team": 0.8727272727272728
		}]
	},
	"960308": {
		"nik": "960308",
		"nama": "NIAL ZADA ADRIANTO",
		"band": "VI",
		"posisi": "OFF 3 SECRETARIATE EVP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.846666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9955005624296956,
			"nilai_komparatif_unit": 0.9987375018309437,
			"summary_team": 0.8428571428571426
		}]
	},
	"960311": {
		"nik": "960311",
		"nama": "ONY NARAULITA MARINGGA",
		"band": "VI",
		"posisi": "OFF 3 CHAPTER DESIGNER CAPABILITY MGMT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999997,
			"nilai_komparatif_unit": 1.0032515696358295,
			"summary_team": 0.8999999999999999
		}]
	},
	"960312": {
		"nik": "960312",
		"nama": "ORIZA DEWI",
		"band": "VI",
		"posisi": "OFF 3 SALES - 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8390804597701145,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0375503626107982,
			"nilai_komparatif_unit": 1.0409240298655078,
			"summary_team": 0.8705882352941177
		}]
	},
	"960314": {
		"nik": "960314",
		"nama": "QONITA RAHMA MUZDALIFAH",
		"band": "VI",
		"posisi": "OFF 3 ACCOUNT TEAM DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9222222222222223,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9660460021905805,
			"nilai_komparatif_unit": 0.9691871680381182,
			"summary_team": 0.8909090909090909
		}]
	},
	"960315": {
		"nik": "960315",
		"nama": "RADEN DHENAKE AGHNI BUNGA IRAWAN",
		"band": "VI",
		"posisi": "OFF 3 OTT SERVICE PERFORMANCE MONITORING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9701492537313432,
			"nilai_komparatif_unit": 0.973303761586999,
			"summary_team": 0.8666666666666667
		}]
	},
	"960316": {
		"nik": "960316",
		"nama": "RADEN MARWAN ZANUAR",
		"band": "VI",
		"posisi": "OFF 3 SYSTEM EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769227,
			"nilai_komparatif_unit": 0.9260783719715349,
			"summary_team": 0.7999999999999998
		}]
	},
	"960317": {
		"nik": "960317",
		"nama": "RATIH PARAMITHA",
		"band": "VI",
		"posisi": "OFF 3 ACCESS OSP PROJECT CONTROL AREA 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.057142857142857,
			"nilai_komparatif_unit": 1.060580230757877,
			"summary_team": 0.9866666666666666
		}]
	},
	"960318": {
		"nik": "960318",
		"nama": "RENA WIDIYASARI",
		"band": "VI",
		"posisi": "OFF 3 PAYMENT SUPPORT & CREDIT CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9787595581988102,
			"nilai_komparatif_unit": 0.9819420630590278,
			"summary_team": 0.8727272727272727
		}]
	},
	"960320": {
		"nik": "960320",
		"nama": "RESSY ARYANI",
		"band": "VI",
		"posisi": "OFF 3 RESOURCE PROCESS OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0151515151515147,
			"nilai_komparatif_unit": 1.018452350993948,
			"summary_team": 0.8933333333333332
		}]
	},
	"960322": {
		"nik": "960322",
		"nama": "RHISMA AYU SYAHRA",
		"band": "VI",
		"posisi": "OFF 3 SECRETARIAT SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9982847341337905,
			"nilai_komparatif_unit": 1.0015307264632125,
			"summary_team": 0.881818181818182
		}]
	},
	"960323": {
		"nik": "960323",
		"nama": "RIMA DIAN SAFITRI",
		"band": "VI",
		"posisi": "OFF 3 ENTERPRISE SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9787595581988102,
			"nilai_komparatif_unit": 0.9819420630590278,
			"summary_team": 0.8727272727272727
		}]
	},
	"960324": {
		"nik": "960324",
		"nama": "RISCHA AGUSTINA",
		"band": "VI",
		"posisi": "OFF 3 PORTFOLIO EXPANSION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777771,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0415913200723332,
			"nilai_komparatif_unit": 1.0449781267816245,
			"summary_team": 0.914285714285714
		}]
	},
	"960327": {
		"nik": "960327",
		"nama": "SAID HADYAN RADIFAN ALDJUFRI",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL WORKPLACE DELIVERY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0468749999999996,
			"nilai_komparatif_unit": 1.050278986962509,
			"summary_team": 0.8933333333333332
		}]
	},
	"960328": {
		"nik": "960328",
		"nama": "SULTHAN RAIHAN RAMZY",
		"band": "VI",
		"posisi": "OFF 3 TECHNICAL PERFORMANCE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.849382716049382,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.831053351573188,
			"nilai_komparatif_unit": 0.8337555794169181,
			"summary_team": 0.7058823529411764
		}]
	},
	"960330": {
		"nik": "960330",
		"nama": "TIFANI DIAHNISA HARDIANTI",
		"band": "VI",
		"posisi": "OFF 3 DATA SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8844444444444437,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0508424475317775,
			"nilai_komparatif_unit": 1.054259334926213,
			"summary_team": 0.9294117647058824
		}]
	},
	"960331": {
		"nik": "960331",
		"nama": "TRIYUNITA UTAMI",
		"band": "VI",
		"posisi": "OFF 3 OPEX & CAPEX MGMT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8606060606060609,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0328638497652578,
			"nilai_komparatif_unit": 1.036222278497101,
			"summary_team": 0.8888888888888888
		}]
	},
	"960332": {
		"nik": "960332",
		"nama": "VIDIA WIDYA",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL SERVICE ASSURANCE OPRTN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9877232142857141,
			"nilai_komparatif_unit": 0.9909348650978899,
			"summary_team": 0.8428571428571427
		}]
	},
	"960333": {
		"nik": "960333",
		"nama": "VIDYA PUTRI PRIANDIRI",
		"band": "VI",
		"posisi": "OFFICER 3 TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0038197097020636,
			"nilai_komparatif_unit": 1.0070836993899783,
			"summary_team": 0.8588235294117645
		}]
	},
	"960335": {
		"nik": "960335",
		"nama": "RR YOFI KURNIA BAVARI",
		"band": "VI",
		"posisi": "OFF 3 DOCUMENT VERIFICATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0208507089241032,
			"nilai_komparatif_unit": 1.0241700760919563,
			"summary_team": 0.9272727272727272
		}]
	},
	"960337": {
		"nik": "960337",
		"nama": "YUDA MAULANA",
		"band": "VI",
		"posisi": "OFF 3 ES RECONCIL & ACCOUNT FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139858,
			"nilai_komparatif_unit": 0.9892200791514125,
			"summary_team": 0.8545454545454545
		}]
	},
	"960338": {
		"nik": "960338",
		"nama": "ZAHRAH CITRA HAFIZHA",
		"band": "VI",
		"posisi": "OFFICER 3 TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9079365079365075,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9912587412587417,
			"nilai_komparatif_unit": 0.9944818880830696,
			"summary_team": 0.8999999999999999
		}]
	},
	"960339": {
		"nik": "960339",
		"nama": "ZULFIKAR AWAN KURNIAWAN",
		"band": "VI",
		"posisi": "OFF 3 DEVELOPER PERFORMANCE & SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769231,
			"nilai_komparatif_unit": 0.9260783719715353,
			"summary_team": 0.8
		}]
	},
	"965071": {
		"nik": "965071",
		"nama": "DINEKE KUSUMAWATI",
		"band": "VI",
		"posisi": "OFF 3 TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8724637681159412,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9824394874228769,
			"nilai_komparatif_unit": 0.9856339578292215,
			"summary_team": 0.857142857142857
		}]
	},
	"965225": {
		"nik": "965225",
		"nama": "RIZQI AMINULLAH",
		"band": "VI",
		"posisi": "OFF 3 IPTV UI/UX DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0129870129870127,
			"nilai_komparatif_unit": 1.0162808107999313,
			"summary_team": 0.8666666666666667
		}]
	},
	"970007": {
		"nik": "970007",
		"nama": "ADIGUNA TRIRAHARJO",
		"band": "VI",
		"posisi": "OFF 3 BUSINESS PLANNING & ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8606060606060609,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.929577464788732,
			"nilai_komparatif_unit": 0.9326000506473908,
			"summary_team": 0.8
		}]
	},
	"970011": {
		"nik": "970011",
		"nama": "NANDA BAGUS NURHIDAYAT",
		"band": "VI",
		"posisi": "OFF 3 CONS BIZ PORT DATA ADM&SUPP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0521642619311877,
			"nilai_komparatif_unit": 1.0555854472971886,
			"summary_team": 0.9294117647058824
		}]
	},
	"970013": {
		"nik": "970013",
		"nama": "AZZA FLIRA NABILA",
		"band": "VI",
		"posisi": "OFF 3 OBL SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9666666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.921630094043887,
			"nilai_komparatif_unit": 0.9246268384731471,
			"summary_team": 0.8909090909090909
		}]
	},
	"970016": {
		"nik": "970016",
		"nama": "ANTONIO AFTA",
		"band": "VI",
		"posisi": "OFF 3 OLO SERVICE QUALITY & PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.024691358024691,
			"nilai_komparatif_unit": 1.0280232133305414,
			"summary_team": 0.922222222222222
		}]
	},
	"970017": {
		"nik": "970017",
		"nama": "JANUARDI BUDI LAKSONO",
		"band": "VI",
		"posisi": "OFF 3 WIRELESS PRODUCT READINESS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9848484848484845,
			"nilai_komparatif_unit": 0.988050788277711,
			"summary_team": 0.8666666666666665
		}]
	},
	"970024": {
		"nik": "970024",
		"nama": "HANY GHINA LAUDZA",
		"band": "VI",
		"posisi": "OFF 3 BUSINESS PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9890109890109897,
			"nilai_komparatif_unit": 0.99222682711236,
			"summary_team": 0.857142857142857
		}]
	},
	"970028": {
		"nik": "970028",
		"nama": "ARRIZKA PERMATA FAIDA",
		"band": "VI",
		"posisi": "OFF 3 CX CHANGE MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7944444444444441,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0514191690662282,
			"nilai_komparatif_unit": 1.0548379317108936,
			"summary_team": 0.8352941176470587
		}]
	},
	"970029": {
		"nik": "970029",
		"nama": "MARIA DENNA",
		"band": "VI",
		"posisi": "OFF 3 SUPPORT READINESS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9533333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0489510489510487,
			"nilai_komparatif_unit": 1.05236178633129,
			"summary_team": 0.9999999999999998
		}]
	},
	"970030": {
		"nik": "970030",
		"nama": "FIZRI ADIYESA",
		"band": "VI",
		"posisi": "OFF 3 WAR ROOM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144382,
			"nilai_komparatif_unit": 1.0300764779148626,
			"summary_team": 0.8727272727272727
		}]
	},
	"970035": {
		"nik": "970035",
		"nama": "ADHITIA WIRAGUNA",
		"band": "VI",
		"posisi": "OFF 3 DATA SCIENTIST",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0086165512916365,
			"summary_team": 0.8545454545454546
		}]
	},
	"970036": {
		"nik": "970036",
		"nama": "RIZKA AMALIA ZENY PUTRI",
		"band": "VI",
		"posisi": "OFF 3 CUST LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9555555555555557,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0095759233926125,
			"nilai_komparatif_unit": 1.012858629810181,
			"summary_team": 0.9647058823529411
		}]
	},
	"970084": {
		"nik": "970084",
		"nama": "ADINDA MUTIARA HAKIM",
		"band": "VI",
		"posisi": "OFF 3 WIRELESS OPERATION PLATFORM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969695,
			"nilai_komparatif_unit": 0.9728500069195924,
			"summary_team": 0.8533333333333333
		}]
	},
	"970085": {
		"nik": "970085",
		"nama": "AGUNG PRATAMA",
		"band": "VI",
		"posisi": "OFF 3 PROJECT ADMIN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8377777777777767,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9549071618037146,
			"nilai_komparatif_unit": 0.958012108936072,
			"summary_team": 0.7999999999999999
		}]
	},
	"970086": {
		"nik": "970086",
		"nama": "AGUNG WIRAYUDHA",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL BUSINESS INTERFACE MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8722222222222225,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9380428488708742,
			"nilai_komparatif_unit": 0.9410929605153701,
			"summary_team": 0.8181818181818182
		}]
	},
	"970088": {
		"nik": "970088",
		"nama": "AHMAD WAHRUDIN",
		"band": "VI",
		"posisi": "OFF 3 NETWORK, ASS IP BB & BROADBAND",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999997,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0555555555555558,
			"nilai_komparatif_unit": 1.058987767948932,
			"summary_team": 0.8444444444444443
		}]
	},
	"970091": {
		"nik": "970091",
		"nama": "ANIDA NORMA CAHYATI",
		"band": "VI",
		"posisi": "OFF 3 LOGISTIC SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9466666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0140845070422533,
			"nilai_komparatif_unit": 1.0173818734335174,
			"summary_team": 0.96
		}]
	},
	"970092": {
		"nik": "970092",
		"nama": "ANNISA ANGGORO RAHADIYANTI",
		"band": "VI",
		"posisi": "OFF 3 SEGMENT ADMINISTRATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8999999999999999,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969698,
			"nilai_komparatif_unit": 0.9728500069195928,
			"summary_team": 0.8727272727272728
		}]
	},
	"970093": {
		"nik": "970093",
		"nama": "ARIEF RAMADHANA",
		"band": "VI",
		"posisi": "OFF 3 COMPLIANCE CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7777777777777781,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857139,
			"nilai_komparatif_unit": 0.9674211564345498,
			"summary_team": 0.75
		}]
	},
	"970095": {
		"nik": "970095",
		"nama": "AULIYA NUR RAHMAH",
		"band": "VI",
		"posisi": "OFF 3 ENTERPRISE SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9890109890109885,
			"nilai_komparatif_unit": 0.9922268271123588,
			"summary_team": 0.857142857142857
		}]
	},
	"970101": {
		"nik": "970101",
		"nama": "CLARA DEWANTI",
		"band": "VI",
		"posisi": "OFF 3 EPG OPERATION & TRAFFIC",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8260869565217385,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818713450292406,
			"nilai_komparatif_unit": 0.9850639680810291,
			"summary_team": 0.8111111111111111
		}]
	},
	"970104": {
		"nik": "970104",
		"nama": "DAYANG SYAFUA DAULAY",
		"band": "VI",
		"posisi": "OFF 3 SEGMENT PERFORMANCE SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0115702479338842,
			"nilai_komparatif_unit": 1.0148594390365748,
			"summary_team": 0.9272727272727272
		}]
	},
	"970106": {
		"nik": "970106",
		"nama": "DHIMAS AULIA RAHMAN",
		"band": "VI",
		"posisi": "OFF 3 CAPACITY PLANNING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9142857142857144,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0142045454545454,
			"nilai_komparatif_unit": 1.0175023021590661,
			"summary_team": 0.9272727272727272
		}]
	},
	"970108": {
		"nik": "970108",
		"nama": "EARLYAN ABDIEL BERNATAPI",
		"band": "VI",
		"posisi": "OFF 3 SALES PERFORMANCE & OFFERING MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333326,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200696,
			"nilai_komparatif_unit": 1.0531560216417297,
			"summary_team": 0.9272727272727274
		}]
	},
	"970109": {
		"nik": "970109",
		"nama": "ERIKA RETNO HAPSARI",
		"band": "VI",
		"posisi": "OFF 3 SALES OPERATION & SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8577777777777775,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9326424870466324,
			"nilai_komparatif_unit": 0.9356750390385982,
			"summary_team": 0.8
		}]
	},
	"970110": {
		"nik": "970110",
		"nama": "FACHRI TAUFIK DASAHRUDDYN",
		"band": "VI",
		"posisi": "OFF 3 ICM TRANSPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8711111111111115,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9839650145772588,
			"nilai_komparatif_unit": 0.9871644453413772,
			"summary_team": 0.857142857142857
		}]
	},
	"970113": {
		"nik": "970113",
		"nama": "FIA SALSABILA",
		"band": "VI",
		"posisi": "OFF 3 MANAGED OPERATION HANDLING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.792592592592593,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8946474086660998,
			"nilai_komparatif_unit": 0.8975564170148924,
			"summary_team": 0.7090909090909091
		}]
	},
	"970115": {
		"nik": "970115",
		"nama": "HALIMAH TUSAK DIAH",
		"band": "VI",
		"posisi": "OFF 3 OFFERING 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9318181818181819,
			"nilai_komparatif_unit": 0.9348480535242961,
			"summary_team": 0.7454545454545455
		}]
	},
	"970116": {
		"nik": "970116",
		"nama": "IMAM SATRIA",
		"band": "VI",
		"posisi": "OFF 3 IT COMPLIANCE PLANNING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999996,
			"nilai_komparatif_unit": 0.9631215068503963,
			"summary_team": 0.7999999999999999
		}]
	},
	"970119": {
		"nik": "970119",
		"nama": "JANAH EKA WIDIARNI",
		"band": "VI",
		"posisi": "OFF 3 OTT SERVICE FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7833333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0354609929078011,
			"nilai_komparatif_unit": 1.0388278664314263,
			"summary_team": 0.8111111111111111
		}]
	},
	"970121": {
		"nik": "970121",
		"nama": "JHONNI MICHAEL GULTOM",
		"band": "VI",
		"posisi": "OFFICER 3 TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.828282828282828,
			"nilai_komparatif_unit": 0.8309760475771517,
			"summary_team": 0.7454545454545454
		}]
	},
	"970122": {
		"nik": "970122",
		"nama": "K. PUTRI NARIRATIH",
		"band": "VI",
		"posisi": "OFF 3 LOGISTIC SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000003,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144382,
			"nilai_komparatif_unit": 1.0300764779148626,
			"summary_team": 0.8727272727272727
		}]
	},
	"970123": {
		"nik": "970123",
		"nama": "KHALILA PUTRI ANGGRAINI",
		"band": "VI",
		"posisi": "OFF 3 SEGMENT INDUSTRIAL ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882353,
			"nilai_komparatif_unit": 0.9442367714219575,
			"summary_team": 0.8
		}]
	},
	"970124": {
		"nik": "970124",
		"nama": "M. AZZAM MUZAKKI",
		"band": "VI",
		"posisi": "OFFICER 3 TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.875757575757575,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9940972928963983,
			"nilai_komparatif_unit": 0.9973296694690409,
			"summary_team": 0.8705882352941177
		}]
	},
	"970126": {
		"nik": "970126",
		"nama": "MAULANA FALITHURRAHMAN",
		"band": "VI",
		"posisi": "OFF 3 NOC IP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8242424242424243,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0191176470588235,
			"nilai_komparatif_unit": 1.0224313790553383,
			"summary_team": 0.84
		}]
	},
	"970127": {
		"nik": "970127",
		"nama": "MEISY RIZKAPUTRI",
		"band": "VI",
		"posisi": "OFF 3 PROPOSAL SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139857,
			"nilai_komparatif_unit": 0.9892200791514124,
			"summary_team": 0.8545454545454546
		}]
	},
	"970129": {
		"nik": "970129",
		"nama": "MUHAMMAD OSANDA KUSUMA",
		"band": "VI",
		"posisi": "OFF 3 CONTENT & INFORMATION MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0441176470588234,
			"nilai_komparatif_unit": 1.047512668296234,
			"summary_team": 0.9466666666666665
		}]
	},
	"970131": {
		"nik": "970131",
		"nama": "MUHAMMAD YAZID FAHMI",
		"band": "VI",
		"posisi": "OFF 3 MANAGED OPERATION READINESS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0167696381288611,
			"nilai_komparatif_unit": 1.0200757354108347,
			"summary_team": 0.8727272727272727
		}]
	},
	"970135": {
		"nik": "970135",
		"nama": "NAIZA ASTRI WULANDARI",
		"band": "VI",
		"posisi": "OFFICER 3 TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9499999999999998,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9760765550239235,
			"nilai_komparatif_unit": 0.9792503359124848,
			"summary_team": 0.9272727272727272
		}]
	},
	"970136": {
		"nik": "970136",
		"nama": "NISA AULIA NURHASANAH",
		"band": "VI",
		"posisi": "OFFICER 3 TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8194444444444434,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0193419740777678,
			"nilai_komparatif_unit": 1.022656435489206,
			"summary_team": 0.8352941176470587
		}]
	},
	"970137": {
		"nik": "970137",
		"nama": "NISSA WAHYUNING DWI AFIFAH",
		"band": "VI",
		"posisi": "OFF 3 IP MULTIMEDIA SUB SYSTEM 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9934640522875826,
			"nilai_komparatif_unit": 0.9966943698342895,
			"summary_team": 0.8444444444444444
		}]
	},
	"970138": {
		"nik": "970138",
		"nama": "NITA ANNAFIA INDRASWARI",
		"band": "VI",
		"posisi": "OFFICER 3 TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.935064935064936,
			"nilai_komparatif_unit": 0.9381053638153225,
			"summary_team": 0.7999999999999999
		}]
	},
	"970141": {
		"nik": "970141",
		"nama": "PARAHITA FAQIH",
		"band": "VI",
		"posisi": "OFF 3 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.855555555555556,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0588235294117643,
			"nilai_komparatif_unit": 1.0622663678497017,
			"summary_team": 0.9058823529411766
		}]
	},
	"970142": {
		"nik": "970142",
		"nama": "PERMADI TRI PRASOJO",
		"band": "VI",
		"posisi": "OFF 3 PERFORMANCE MANAGEMENT IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.860606060606061,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9450704225352107,
			"nilai_komparatif_unit": 0.9481433848248471,
			"summary_team": 0.8133333333333332
		}]
	},
	"970143": {
		"nik": "970143",
		"nama": "PHAKSI GHAGONO AWANG MURTI",
		"band": "VI",
		"posisi": "OFF 3 OPERATION DATA CENTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8944444444444449,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9813664596273285,
			"nilai_komparatif_unit": 0.9845574410090746,
			"summary_team": 0.8777777777777777
		}]
	},
	"970144": {
		"nik": "970144",
		"nama": "RABBANI ZAHID MAHDI TANRA",
		"band": "VI",
		"posisi": "OFF 3 OLO CORE NETWORK SURVEILANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.92156862745098,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9920972644376902,
			"nilai_komparatif_unit": 0.9953231377785257,
			"summary_team": 0.9142857142857141
		}]
	},
	"970148": {
		"nik": "970148",
		"nama": "REGITA NURMALITA YUNIAR",
		"band": "VI",
		"posisi": "OFFICER 3 TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777767,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0052122114668665,
			"nilai_komparatif_unit": 1.0084807289712376,
			"summary_team": 0.8823529411764706
		}]
	},
	"970149": {
		"nik": "970149",
		"nama": "RENNO RIFALDO PALAPESSY",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL CONTENT STRATEGY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.939705882352941,
			"nilai_komparatif_unit": 0.9427614014666105,
			"summary_team": 0.8352941176470587
		}]
	},
	"970150": {
		"nik": "970150",
		"nama": "RIFQI NUR MUKHAMMAD",
		"band": "VI",
		"posisi": "OFFICER 3 TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8422222222222243,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9917740183144472,
			"nilai_komparatif_unit": 0.9949988405980035,
			"summary_team": 0.8352941176470587
		}]
	},
	"970152": {
		"nik": "970152",
		"nama": "RINA ARDIANNI",
		"band": "VI",
		"posisi": "OFF 3 CHPTR DATA SCNTST PLANNG,PRFRM&SUP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8155555555555545,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0386279852540483,
			"nilai_komparatif_unit": 1.0420051564738235,
			"summary_team": 0.8470588235294116
		}]
	},
	"970153": {
		"nik": "970153",
		"nama": "RIO ARMANDO MANIK",
		"band": "VI",
		"posisi": "OFF 3 BUSINESS CONTINUITY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9836065573770488,
			"nilai_komparatif_unit": 0.9868048225926193,
			"summary_team": 0.7999999999999999
		}]
	},
	"970154": {
		"nik": "970154",
		"nama": "RIZKIANA RANI SEJAHTERA",
		"band": "VI",
		"posisi": "OFF 3 SUPPORT 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8158730158730151,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9805447470817128,
			"nilai_komparatif_unit": 0.9837330566078962,
			"summary_team": 0.7999999999999998
		}]
	},
	"970155": {
		"nik": "970155",
		"nama": "RIZQI ARDHI",
		"band": "VI",
		"posisi": "OFF 3 MANAGED SERVICE CAPACITY PLANNING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9250000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.081081081081081,
			"nilai_komparatif_unit": 1.0845962914981944,
			"summary_team": 1
		}]
	},
	"970156": {
		"nik": "970156",
		"nama": "SEFRIZAL EKA PRATAMA",
		"band": "VI",
		"posisi": "OFF 3 TRAFFIC DELIVERY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333331,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9910714285714289,
			"nilai_komparatif_unit": 0.9942939663355104,
			"summary_team": 0.9250000000000002
		}]
	},
	"970158": {
		"nik": "970158",
		"nama": "SHINTA ULI AGNES MONICA GULTOM",
		"band": "VI",
		"posisi": "OFF 3 DATA SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999989,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090924,
			"nilai_komparatif_unit": 1.0944562577845431,
			"summary_team": 0.9272727272727272
		}]
	},
	"970160": {
		"nik": "970160",
		"nama": "SURYA DAREN HAFIZH",
		"band": "VI",
		"posisi": "OFF 3 API MGT OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999995,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9859943977591042,
			"nilai_komparatif_unit": 0.9892004272039562,
			"summary_team": 0.8380952380952381
		}]
	},
	"970163": {
		"nik": "970163",
		"nama": "UTARI HUSTITA DEWI",
		"band": "VI",
		"posisi": "OFF 3 CX CONSUMER DIGITIZATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7944444444444441,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.97737556561086,
			"nilai_komparatif_unit": 0.9805535703228024,
			"summary_team": 0.776470588235294
		}]
	},
	"970164": {
		"nik": "970164",
		"nama": "WIDIA KHAIRUNNISA",
		"band": "VI",
		"posisi": "OFFICER 3 TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7851851851851855,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.856775300171526,
			"nilai_komparatif_unit": 0.8595611647222928,
			"summary_team": 0.6727272727272726
		}]
	},
	"970166": {
		"nik": "970166",
		"nama": "WIDYASTUTI",
		"band": "VI",
		"posisi": "OFF 3 SEGMENT ADMINISTRATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96,
			"nilai_komparatif_unit": 0.9631215068503967,
			"summary_team": 0.8
		}]
	},
	"980018": {
		"nik": "980018",
		"nama": "BELINDA RAMADHANTY HARUN",
		"band": "VI",
		"posisi": "OFF 3 OFFERING 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9666666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9780564263322883,
			"nilai_komparatif_unit": 0.9812366449102786,
			"summary_team": 0.9454545454545454
		}]
	},
	"980021": {
		"nik": "980021",
		"nama": "DHAMIR RANIAH KIASATI DESRUL",
		"band": "VI",
		"posisi": "OFF 3 CAPEX MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0074074074074069,
			"nilai_komparatif_unit": 1.010683062744243,
			"summary_team": 0.9066666666666665
		}]
	},
	"980022": {
		"nik": "980022",
		"nama": "FACHRY ADITYA RACHMAN",
		"band": "VI",
		"posisi": "OFF 3 IT TOOLS SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.916666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090906,
			"nilai_komparatif_unit": 1.0944562577845414,
			"summary_team": 1
		}]
	},
	"980023": {
		"nik": "980023",
		"nama": "HAFIDH AZMI SAMHARI",
		"band": "VI",
		"posisi": "OFF 3 EUC TECHNICAL OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9473684210526311,
			"nilai_komparatif_unit": 0.95044885544447,
			"summary_team": 0.7999999999999999
		}]
	},
	"980027": {
		"nik": "980027",
		"nama": "MOHAMMAD SYAMAIDZAR RAHMANI",
		"band": "VI",
		"posisi": "OFF 3 PERFORMANCE & ORDER ALIGNMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0129870129870127,
			"nilai_komparatif_unit": 1.0162808107999313,
			"summary_team": 0.9454545454545454
		}]
	},
	"980028": {
		"nik": "980028",
		"nama": "MONICA SORAYA SIANIPAR",
		"band": "VI",
		"posisi": "OFF 3 ACCOUNT TEAM PLANNING&PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9037037037037039,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0117096018735359,
			"nilai_komparatif_unit": 1.0149992460952655,
			"summary_team": 0.9142857142857141
		}]
	},
	"980029": {
		"nik": "980029",
		"nama": "NATASYA PRIMA OKTAVIANI",
		"band": "VI",
		"posisi": "OFF 3 SALES - 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9826989619377171,
			"nilai_komparatif_unit": 0.9858942760435153,
			"summary_team": 0.8352941176470587
		}]
	},
	"980030": {
		"nik": "980030",
		"nama": "NATHYA MAHARANI HYACIANTHA",
		"band": "VI",
		"posisi": "OFF 3 ALIGNMENT DATA SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.859259259259259,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0474137931034486,
			"nilai_komparatif_unit": 1.0508195319892533,
			"summary_team": 0.9
		}]
	},
	"980032": {
		"nik": "980032",
		"nama": "REGITA AYU CAHYANI ZULAIKHAH",
		"band": "VI",
		"posisi": "OFF 3 TRIBE PLANNING & PERFORMANCE II",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8238095238095239,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9537572254335258,
			"nilai_komparatif_unit": 0.9568584334676988,
			"summary_team": 0.7857142857142856
		}]
	},
	"980033": {
		"nik": "980033",
		"nama": "SHIDA HABSARI",
		"band": "VI",
		"posisi": "OFF 3 SERVICE DELIVERY EVALUATION & SUPP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9200000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9855072463768114,
			"nilai_komparatif_unit": 0.9887116918150206,
			"summary_team": 0.9066666666666666
		}]
	},
	"990001": {
		"nik": "990001",
		"nama": "NANDA RAHMA ANANTA",
		"band": "VI",
		"posisi": "OFF 3 HCM & RISK MANAGEMENT DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.071428571428571,
			"nilai_komparatif_unit": 1.0749123960383886,
			"summary_team": 0.9999999999999999
		}]
	}
};