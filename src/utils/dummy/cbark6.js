export const cbark6 = {
	"650103": {
		"nik": "650103",
		"nama": "SYAHRIAL",
		"band": "III",
		"posisi": "MGR RETIRE EXPERIENCE & INT STAKEHOL REL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9735849056603774,
			"nilai_komparatif_unit": 0.9787440651375485,
			"summary_team": 0.8600000000000001
		}]
	},
	"650123": {
		"nik": "650123",
		"nama": "NONO SUKARDIONO",
		"band": "III",
		"posisi": "MGR ACCESS INTEGRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8733333333333323,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.068702290076337,
			"nilai_komparatif_unit": 1.074365489573439,
			"summary_team": 0.9333333333333332
		}]
	},
	"650776": {
		"nik": "650776",
		"nama": "LULY SURYANA",
		"band": "III",
		"posisi": "MGR PERFORMANCE & QUALITY MGT OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8746031746031734,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9401088929219613,
			"nilai_komparatif_unit": 0.9450906584323884,
			"summary_team": 0.8222222222222222
		}]
	},
	"650882": {
		"nik": "650882",
		"nama": "ASEP FIKI",
		"band": "III",
		"posisi": "MGR BUDGET OPEX GROUP BUSINESS 01",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8463768115942021,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9452054794520558,
			"nilai_komparatif_unit": 0.9502142524710679,
			"summary_team": 0.8
		}]
	},
	"651039": {
		"nik": "651039",
		"nama": "PARYANI SUHARTANTO",
		"band": "III",
		"posisi": "SENIOR STAFF BP III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8733333333333327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9160305343511456,
			"nilai_komparatif_unit": 0.9208847053486615,
			"summary_team": 0.7999999999999999
		}]
	},
	"651058": {
		"nik": "651058",
		"nama": "MAMAT RAHMAT MUSTAJAB",
		"band": "III",
		"posisi": "MGR TRANSPORT 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8681481481481468,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.01109215017065,
			"nilai_komparatif_unit": 1.016450065662682,
			"summary_team": 0.8777777777777777
		}]
	},
	"651103": {
		"nik": "651103",
		"nama": "CHRISMAN GANDA HAMONANGAN",
		"band": "III",
		"posisi": "MGR LOGISTIC SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8756756756756745,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625220458553804,
			"nilai_komparatif_unit": 0.9676225817264585,
			"summary_team": 0.8428571428571427
		}]
	},
	"651252": {
		"nik": "651252",
		"nama": "SRI ENDANG SUWONO",
		"band": "III",
		"posisi": "MANAGER LEARNING DEVT FIN & CORP STRAT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0479078229229781,
			"nilai_komparatif_unit": 1.053460829696607,
			"summary_team": 0.8727272727272727
		}]
	},
	"651338": {
		"nik": "651338",
		"nama": "SUPIYULOH",
		"band": "III",
		"posisi": "MGR SPIRITUAL MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200685,
			"nilai_komparatif_unit": 1.055305440211733,
			"summary_team": 0.9272727272727272
		}]
	},
	"660060": {
		"nik": "660060",
		"nama": "MOCH. WAHYU NUZUL KARIM, ST",
		"band": "III",
		"posisi": "MGR COMMON CATEGORY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8450980392156857,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8452104739807761,
			"nilai_komparatif_unit": 0.8496893597992498,
			"summary_team": 0.7142857142857142
		}]
	},
	"660066": {
		"nik": "660066",
		"nama": "AGUS SOFYAN",
		"band": "III",
		"posisi": "MGR CONTACT CENTER & COMPLAIN HANDLING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.852941176470587,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9793103448275875,
			"nilai_komparatif_unit": 0.9844998441894146,
			"summary_team": 0.8352941176470587
		}]
	},
	"660120": {
		"nik": "660120",
		"nama": "B. NICOLAAS FREDDY, SMB",
		"band": "III",
		"posisi": "SO RISK REVIEWER & ASSESSMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.826666666666666,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9962049335863385,
			"nilai_komparatif_unit": 1.0014839596830203,
			"summary_team": 0.8235294117647058
		}]
	},
	"660139": {
		"nik": "660139",
		"nama": "DEDY SUWARYA RUCHIYAT SAPUTRA",
		"band": "III",
		"posisi": "SENIOR EXPERT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96058217101273,
			"nilai_komparatif_unit": 0.9656724272218898,
			"summary_team": 0.8
		}]
	},
	"660148": {
		"nik": "660148",
		"nama": "HILMAN BUDIMAN",
		"band": "III",
		"posisi": "MGR PAYMENT PARTNERSHIP & READINESS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.852941176470587,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9965517241379325,
			"nilai_komparatif_unit": 1.001832587925144,
			"summary_team": 0.85
		}]
	},
	"660151": {
		"nik": "660151",
		"nama": "HERI TRIYANA",
		"band": "III",
		"posisi": "SO MEDIA PARTNERSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8799999999999992,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0026737967914447,
			"nilai_komparatif_unit": 1.0079871022783649,
			"summary_team": 0.8823529411764706
		}]
	},
	"660159": {
		"nik": "660159",
		"nama": "AGUS, ST.",
		"band": "III",
		"posisi": "SO PROCESS INTEGRATION & APPL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8071428571428563,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.991150442477877,
			"nilai_komparatif_unit": 0.9964026841353663,
			"summary_team": 0.7999999999999998
		}]
	},
	"660167": {
		"nik": "660167",
		"nama": "RIZKI HADIANSYAH",
		"band": "III",
		"posisi": "SENIOR TESTER ENGINEER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7537037037037038,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.061425061425061,
			"nilai_komparatif_unit": 1.0670496978929445,
			"summary_team": 0.7999999999999998
		}]
	},
	"660176": {
		"nik": "660176",
		"nama": "IMAM BUDI SANTOSO",
		"band": "III",
		"posisi": "SENIOR EXPERT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0635016893355222,
			"nilai_komparatif_unit": 1.0691373301385205,
			"summary_team": 0.8857142857142856
		}]
	},
	"660197": {
		"nik": "660197",
		"nama": "HAMIDANI",
		"band": "III",
		"posisi": "MGR QUALITY MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8756756756756745,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9462081128747808,
			"nilai_komparatif_unit": 0.9512221989853322,
			"summary_team": 0.8285714285714285
		}]
	},
	"660212": {
		"nik": "660212",
		"nama": "YULHASRIL",
		"band": "III",
		"posisi": "AUDITOR MUDA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8362455017992755,
			"nilai_komparatif_unit": 0.8406768810048961,
			"summary_team": 0.73
		}]
	},
	"660220": {
		"nik": "660220",
		"nama": "MOHAMAD LUTH NUR R.",
		"band": "III",
		"posisi": "MANAGER TCU AREA-3",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0102467888584161,
			"nilai_komparatif_unit": 1.0156002246653173,
			"summary_team": 0.8615384615384616
		}]
	},
	"660267": {
		"nik": "660267",
		"nama": "HERMIN WINASTUTI",
		"band": "III",
		"posisi": "SO BUDGETING & ADMINISTRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9703412752545985,
			"nilai_komparatif_unit": 0.9754832462909351,
			"summary_team": 0.8470588235294116
		}]
	},
	"660269": {
		"nik": "660269",
		"nama": "KURNIA HADIANA",
		"band": "III",
		"posisi": "MGR FINANCE SERVICE (HCM, HCBPC, & ACI)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.862015503875968,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0005620503597135,
			"nilai_komparatif_unit": 1.0058641654136746,
			"summary_team": 0.8625
		}]
	},
	"660271": {
		"nik": "660271",
		"nama": "LIES UTAMI",
		"band": "III",
		"posisi": "MGR RECONCILIATION & SETTLEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8811594202898547,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9504523026315793,
			"nilai_komparatif_unit": 0.9554888792837152,
			"summary_team": 0.8375
		}]
	},
	"660273": {
		"nik": "660273",
		"nama": "PURWANTO",
		"band": "III",
		"posisi": "SO COMPLIANCE & SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8083333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.97513644633111,
			"nilai_komparatif_unit": 0.980303827634348,
			"summary_team": 0.7882352941176471
		}]
	},
	"660281": {
		"nik": "660281",
		"nama": "SANTI WAHJUNI",
		"band": "III",
		"posisi": "MGR TAX OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8761904761904756,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9832775919732448,
			"nilai_komparatif_unit": 0.9884881143199096,
			"summary_team": 0.8615384615384616
		}]
	},
	"660285": {
		"nik": "660285",
		"nama": "JULIETTA PANGARIBUAN, IR.MM.",
		"band": "III",
		"posisi": "MGR SECRETARY & COMMUNICATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8695035460992895,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9200652528548138,
			"nilai_komparatif_unit": 0.9249408043771149,
			"summary_team": 0.8
		}]
	},
	"660295": {
		"nik": "660295",
		"nama": "I NYOMAN ALIT WIJANA, IR.",
		"band": "III",
		"posisi": "SENIOR EXPERT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.026076409945416,
			"nilai_komparatif_unit": 1.0315137290779277,
			"summary_team": 0.8545454545454545
		}]
	},
	"660299": {
		"nik": "660299",
		"nama": "Dr. Ir. Bambang Fajarisman SH., MH., MM",
		"band": "III",
		"posisi": "SENIOR EXPERT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96058217101273,
			"nilai_komparatif_unit": 0.9656724272218898,
			"summary_team": 0.8
		}]
	},
	"660308": {
		"nik": "660308",
		"nama": "OHA",
		"band": "III",
		"posisi": "MGR PERFORMANCE & QUALITY ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8562091503267962,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9693893129771006,
			"nilai_komparatif_unit": 0.9745262394352219,
			"summary_team": 0.8300000000000001
		}]
	},
	"660310": {
		"nik": "660310",
		"nama": "NYOMAN YUDIAWAN",
		"band": "III",
		"posisi": "MGR PROBLEM & FAULT MGT OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8746031746031734,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0862068965517255,
			"nilai_komparatif_unit": 1.0919628553509353,
			"summary_team": 0.95
		}]
	},
	"660315": {
		"nik": "660315",
		"nama": "YADI ABDULLAH",
		"band": "III",
		"posisi": "MGR COMM&CONVERG PROD PORTFOLIO&QUAL MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.866666666666666,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0240384615384623,
			"nilai_komparatif_unit": 1.0294649813038343,
			"summary_team": 0.8875
		}]
	},
	"660327": {
		"nik": "660327",
		"nama": "ALI NURBIJANTO",
		"band": "III",
		"posisi": "MGR TDM, SIGNALLING & SYNCHRONIZATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666656,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0673076923076934,
			"nilai_komparatif_unit": 1.0729635016406165,
			"summary_team": 0.9249999999999998
		}]
	},
	"660334": {
		"nik": "660334",
		"nama": "RARAS PADMAWATI, IR.",
		"band": "III",
		"posisi": "MGR ACCESS INVENTORY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8733333333333323,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9312977099236651,
			"nilai_komparatif_unit": 0.9362327837711397,
			"summary_team": 0.8133333333333332
		}]
	},
	"660342": {
		"nik": "660342",
		"nama": "BUDIASTO, IR",
		"band": "III",
		"posisi": "SENIOR LEARNING ANALYST",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.0321787927330446,
			"summary_team": 0.8727272727272727
		}]
	},
	"660365": {
		"nik": "660365",
		"nama": "ARMY FITHRY, IR. MSC.",
		"band": "III",
		"posisi": "MANAGER LEARNING DEVT IT & SERVICES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8733333333333334,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9323882224645581,
			"nilai_komparatif_unit": 0.9373290750870297,
			"summary_team": 0.8142857142857142
		}]
	},
	"660371": {
		"nik": "660371",
		"nama": "DIDIET HENDRO PRASTOWO, S.KOMP",
		"band": "III",
		"posisi": "MGR IT CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328042328042327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9323566783765603,
			"nilai_komparatif_unit": 0.9372973638425984,
			"summary_team": 0.776470588235294
		}]
	},
	"660372": {
		"nik": "660372",
		"nama": "HENRINA BHATARI KADANG",
		"band": "III",
		"posisi": "SENIOR TESTER ENGINEER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7537037037037038,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8272871802283567,
			"nilai_komparatif_unit": 0.8316710880636187,
			"summary_team": 0.6235294117647059
		}]
	},
	"660389": {
		"nik": "660389",
		"nama": "MUH ARIS SUBEKTI, ST.",
		"band": "III",
		"posisi": "SO COMPENSATION SYSTEM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8238095238095232,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8670520231213878,
			"nilai_komparatif_unit": 0.8716466502938921,
			"summary_team": 0.7142857142857142
		}]
	},
	"660423": {
		"nik": "660423",
		"nama": "TJAHJO RURUH DJATMIKO, IR, MM, MBA.",
		"band": "III",
		"posisi": "SENIOR STAFF III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8748159057437361,
			"nilai_komparatif_unit": 0.8794516747913639,
			"summary_team": 0.7285714285714284
		}]
	},
	"660437": {
		"nik": "660437",
		"nama": "ARIF EFFENDI",
		"band": "III",
		"posisi": "SENIOR EXPERT INNOVATOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666665,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9502262443438916,
			"nilai_komparatif_unit": 0.955261623082265,
			"summary_team": 0.8235294117647058
		}]
	},
	"660441": {
		"nik": "660441",
		"nama": "BAMBANG IRAWAN",
		"band": "III",
		"posisi": "MGR GOVERNANCE & RISK MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.891233766233767,
			"nilai_komparatif_unit": 0.8959565357679979,
			"summary_team": 0.7624999999999998
		}]
	},
	"660450": {
		"nik": "660450",
		"nama": "DONY ALFONSIUS NADAPDAP, IR.",
		"band": "III",
		"posisi": "MGR PROCUREMENT I",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8695035460992895,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9814029363784681,
			"nilai_komparatif_unit": 0.9866035246689226,
			"summary_team": 0.8533333333333334
		}]
	},
	"660617": {
		"nik": "660617",
		"nama": "HERU WIJAYA",
		"band": "III",
		"posisi": "MGR TELKOMGROUP & OLO PROJECT ADMIN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8231884057971008,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9394366197183105,
			"nilai_komparatif_unit": 0.9444148227611503,
			"summary_team": 0.7733333333333333
		}]
	},
	"660621": {
		"nik": "660621",
		"nama": "RASKO",
		"band": "III",
		"posisi": "MGR TRANSPORT OSP DEPLOYMENT AREA-1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111103,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0064516129032266,
			"nilai_komparatif_unit": 1.0117849375540457,
			"summary_team": 0.8666666666666666
		}]
	},
	"660653": {
		"nik": "660653",
		"nama": "DIKHYAH DARIRUDIN",
		"band": "III",
		"posisi": "MGR HC INTEGRATED SOLUTION",
		"category": [{
			"code": "LC-3-01",
			"kriteria": 0.7925925925925925,
			"kriteria_bp": 0.994288023883189,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9511143062544932,
			"nilai_komparatif_unit": 0.9565782584204514,
			"summary_team": 0.7538461538461538
		}]
	},
	"670028": {
		"nik": "670028",
		"nama": "MAHYAR KOSWARA",
		"band": "III",
		"posisi": "SO ENERGY FACILITY STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8541666666666652,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9916786226685812,
			"nilai_komparatif_unit": 0.9969336632250886,
			"summary_team": 0.8470588235294116
		}]
	},
	"670069": {
		"nik": "670069",
		"nama": "NENDI ROHENDI",
		"band": "III",
		"posisi": "MGR BUDGET PLANNING & CONTROLLING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8509803921568628,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.023041474654378,
			"nilai_komparatif_unit": 1.0284627112499907,
			"summary_team": 0.8705882352941177
		}]
	},
	"670071": {
		"nik": "670071",
		"nama": "SRI POERNAMADEWI, RD.",
		"band": "III",
		"posisi": "MGR CASH IN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9041666666666665,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9868840836582775,
			"nilai_komparatif_unit": 0.9921137172972887,
			"summary_team": 0.8923076923076924
		}]
	},
	"670080": {
		"nik": "670080",
		"nama": "DITYO KUNTJORO, IR.MT.",
		"band": "III",
		"posisi": "MGR ASSURANCE DBS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8562091503267962,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9781488549618332,
			"nilai_komparatif_unit": 0.9833321994301183,
			"summary_team": 0.8374999999999999
		}]
	},
	"670091": {
		"nik": "670091",
		"nama": "ADVENDY JD HASIBUAN",
		"band": "III",
		"posisi": "MGR TRANSPORT ISP DEPLOYMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8377777777777767,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9549071618037145,
			"nilai_komparatif_unit": 0.9599673453634593,
			"summary_team": 0.7999999999999998
		}]
	},
	"670096": {
		"nik": "670096",
		"nama": "PUTRANTO",
		"band": "III",
		"posisi": "MGR QUALITY & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8833333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9322974472807992,
			"nilai_komparatif_unit": 0.9372378188731656,
			"summary_team": 0.8235294117647058
		}]
	},
	"670100": {
		"nik": "670100",
		"nama": "PRANOWO BUDI SULISTYO",
		"band": "III",
		"posisi": "MGR ECOSYSTEM INTEGRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8777777777777767,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0052122114668665,
			"nilai_komparatif_unit": 1.0105389683600825,
			"summary_team": 0.8823529411764706
		}]
	},
	"670113": {
		"nik": "670113",
		"nama": "ERRY SAPUTRA",
		"band": "III",
		"posisi": "MGR BUDGET & PROJECT PERFORMANCE MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8583333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.019417475728156,
			"nilai_komparatif_unit": 1.0248195082581586,
			"summary_team": 0.8749999999999998
		}]
	},
	"670145": {
		"nik": "670145",
		"nama": "FAJAR ERI DIANTO, RP",
		"band": "III",
		"posisi": "MGR PRESALES, GTM & MARKETING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.872222222222221,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9846384413638078,
			"nilai_komparatif_unit": 0.9898561750373835,
			"summary_team": 0.8588235294117645
		}]
	},
	"670163": {
		"nik": "670163",
		"nama": "OKA GUMELAR",
		"band": "III",
		"posisi": "MGR ADM & SECRETARY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8714285714285706,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0184426229508206,
			"nilai_komparatif_unit": 1.0238394896027208,
			"summary_team": 0.8875
		}]
	},
	"670166": {
		"nik": "670166",
		"nama": "FIKRI",
		"band": "III",
		"posisi": "MGR DIG LIFE & COLLABORATION OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8589147286821694,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0683796984497786,
			"nilai_komparatif_unit": 1.074041188489762,
			"summary_team": 0.9176470588235296
		}]
	},
	"670220": {
		"nik": "670220",
		"nama": "EKO KARDIYANTO, IR, MT",
		"band": "III",
		"posisi": "MGR OM DCN & EUC APPLICATION SYSTEM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.882962962962962,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0067114093959741,
			"nilai_komparatif_unit": 1.0120461107439154,
			"summary_team": 0.8888888888888887
		}]
	},
	"670231": {
		"nik": "670231",
		"nama": "SARIF HIDAYAT,IR",
		"band": "III",
		"posisi": "MGR HCM & RISK MANAGEMENT OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8589147286821694,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9572803850782204,
			"nilai_komparatif_unit": 0.9623531446724507,
			"summary_team": 0.8222222222222224
		}]
	},
	"670239": {
		"nik": "670239",
		"nama": "BAMBANG WIJANARKO",
		"band": "III",
		"posisi": "MGR RADIO IP OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8738095238095228,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0757493188010911,
			"nilai_komparatif_unit": 1.081449861466539,
			"summary_team": 0.9400000000000001
		}]
	},
	"670242": {
		"nik": "670242",
		"nama": "MOHAMMAD UNTUNG RAHARDJO",
		"band": "III",
		"posisi": "MGR INTERFACE OPS & FULFILLMENT QUAL MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8711111111111115,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9614158163265301,
			"nilai_komparatif_unit": 0.9665104901361439,
			"summary_team": 0.8374999999999999
		}]
	},
	"670266": {
		"nik": "670266",
		"nama": "CAHYO YUNANTO,IR",
		"band": "III",
		"posisi": "MGR RESOURCES DOMAIN MGT OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8746031746031734,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0290381125226875,
			"nilai_komparatif_unit": 1.0344911261219387,
			"summary_team": 0.9
		}]
	},
	"670273": {
		"nik": "670273",
		"nama": "ILHAM RISWANTO, IR.MT.",
		"band": "III",
		"posisi": "MGR CHAPTER DEVELOPR CAPABILTY&NTWRK MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0334928229665068,
			"nilai_komparatif_unit": 1.0389694426852354,
			"summary_team": 0.8727272727272727
		}]
	},
	"670295": {
		"nik": "670295",
		"nama": "JENNY OETOMO, IR",
		"band": "III",
		"posisi": "MGR CFUC CFUW COMM&DATACOM INTERFACE MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9083333333333328,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9633027522935785,
			"nilai_komparatif_unit": 0.9684074252347736,
			"summary_team": 0.875
		}]
	},
	"670304": {
		"nik": "670304",
		"nama": "I. B. PRAMUDHITO, IR.",
		"band": "III",
		"posisi": "SO POLICY & GOVERNANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8375,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.97228144989339,
			"nilai_komparatif_unit": 0.9774337021803057,
			"summary_team": 0.8142857142857142
		}]
	},
	"670354": {
		"nik": "670354",
		"nama": "MEDIE YUNIANTO",
		"band": "III",
		"posisi": "SENIOR EXPERT TDM SWITCHING AT&T",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666656,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9725274725274736,
			"nilai_komparatif_unit": 0.9776810285219517,
			"summary_team": 0.8428571428571427
		}]
	},
	"670391": {
		"nik": "670391",
		"nama": "ASWADI",
		"band": "III",
		"posisi": "MGR TRANSPORT 3",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8681481481481468,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0238907849829366,
			"nilai_komparatif_unit": 1.0293165221900578,
			"summary_team": 0.8888888888888887
		}]
	},
	"670444": {
		"nik": "670444",
		"nama": "FACHRI",
		"band": "III",
		"posisi": "MGR ENT PROVISION& INTEGRATION TR4,5,6,7",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8705882352941167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9342342342342352,
			"nilai_komparatif_unit": 0.939184869125373,
			"summary_team": 0.8133333333333332
		}]
	},
	"670487": {
		"nik": "670487",
		"nama": "SODIKUN",
		"band": "III",
		"posisi": "MGR BB & CORE LOGISTIC SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8714285714285706,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0184426229508206,
			"nilai_komparatif_unit": 1.0238394896027208,
			"summary_team": 0.8875
		}]
	},
	"680032": {
		"nik": "680032",
		"nama": "DODDY HERTANTO , ST.",
		"band": "III",
		"posisi": "MGR WHOLESALE PROV&INTEGRATION TR4,5,6,7",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8705882352941167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9719334719334729,
			"nilai_komparatif_unit": 0.97708388023762,
			"summary_team": 0.846153846153846
		}]
	},
	"680049": {
		"nik": "680049",
		"nama": "YADI NURYADI, SKOM, MT",
		"band": "III",
		"posisi": "SENIOR EXPERT REENGINEERING SERVICE 3P",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9811320754716977,
			"nilai_komparatif_unit": 0.986331228433188,
			"summary_team": 0.8666666666666666
		}]
	},
	"680061": {
		"nik": "680061",
		"nama": "BAMBANG CAHYONO",
		"band": "III",
		"posisi": "SENIOR TESTER ENGINEER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7537037037037038,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0145974851857202,
			"nilai_komparatif_unit": 1.0199739759270794,
			"summary_team": 0.7647058823529411
		}]
	},
	"680114": {
		"nik": "680114",
		"nama": "WIM MUDJIONO",
		"band": "III",
		"posisi": "MGR MANAGED OPERATION PROTOTYPING MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8705882352941167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0602910602910613,
			"nilai_komparatif_unit": 1.0659096875319491,
			"summary_team": 0.9230769230769229
		}]
	},
	"680162": {
		"nik": "680162",
		"nama": "LAMHOT SINTA P. SIMAMORA",
		"band": "III",
		"posisi": "SENIOR EXPERT INNOVATOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.872222222222221,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0520794304983152,
			"nilai_komparatif_unit": 1.057654543190629,
			"summary_team": 0.9176470588235293
		}]
	},
	"680165": {
		"nik": "680165",
		"nama": "HERRY SARTONO HAMIDJAJA",
		"band": "III",
		"posisi": "MGR CHPTR DATA SCNTST CPBLTY&NTWRKNG MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9615384615384616,
			"nilai_komparatif_unit": 0.9666337852618156,
			"summary_team": 0.8333333333333335
		}]
	},
	"680249": {
		"nik": "680249",
		"nama": "SUBHANA",
		"band": "III",
		"posisi": "MGR IP SERVICE NODE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666656,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9807692307692321,
			"nilai_komparatif_unit": 0.9859664609670532,
			"summary_team": 0.8500000000000001
		}]
	},
	"680518": {
		"nik": "680518",
		"nama": "SODIKIN",
		"band": "III",
		"posisi": "MGR PROJECT ADMIN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111103,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.929032258064517,
			"nilai_komparatif_unit": 0.9339553269729653,
			"summary_team": 0.7999999999999999
		}]
	},
	"680596": {
		"nik": "680596",
		"nama": "RATIH RUFFIANTI",
		"band": "III",
		"posisi": "SENIOR EXPERT INNOVATOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666665,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9638009049773757,
			"nilai_komparatif_unit": 0.968908217697726,
			"summary_team": 0.8352941176470587
		}]
	},
	"690013": {
		"nik": "690013",
		"nama": "EVITA TUNJUNG SEKAR",
		"band": "III",
		"posisi": "MGR SERVICE DESK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328042328042327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9606099110546381,
			"nilai_komparatif_unit": 0.9657003142620713,
			"summary_team": 0.8
		}]
	},
	"690057": {
		"nik": "690057",
		"nama": "BAMBANG HERI SURYANTORO",
		"band": "III",
		"posisi": "MGR SERVICE ASSURANCE & SLA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328042328042327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9906289707750954,
			"nilai_komparatif_unit": 0.9958784490827608,
			"summary_team": 0.825
		}]
	},
	"690369": {
		"nik": "690369",
		"nama": "MUHAMAD ALI",
		"band": "III",
		"posisi": "SO CFU MOBILE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328042328042327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0806861499364675,
			"nilai_komparatif_unit": 1.0864128535448299,
			"summary_team": 0.8999999999999998
		}]
	},
	"690599": {
		"nik": "690599",
		"nama": "ROBIN POSMA",
		"band": "III",
		"posisi": "MGR METRO OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8577777777777769,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9909326424870478,
			"nilai_komparatif_unit": 0.9961837299926184,
			"summary_team": 0.8500000000000001
		}]
	},
	"690604": {
		"nik": "690604",
		"nama": "AJI WIDODO",
		"band": "III",
		"posisi": "MGR IOT PLATFORM STRATEGY PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8472222222222218,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0327868852459021,
			"nilai_komparatif_unit": 1.038259764104167,
			"summary_team": 0.875
		}]
	},
	"700065": {
		"nik": "700065",
		"nama": "SAHROMI",
		"band": "III",
		"posisi": "MGR CAPITALIZATION MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8714285714285706,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9467213114754107,
			"nilai_komparatif_unit": 0.951738117095487,
			"summary_team": 0.825
		}]
	},
	"700171": {
		"nik": "700171",
		"nama": "BAKHTIAR, S.Kom, M.M",
		"band": "III",
		"posisi": "MGR ACCESS NON FIBER OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8466666666666656,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0187007874015759,
			"nilai_komparatif_unit": 1.0240990221021844,
			"summary_team": 0.8624999999999998
		}]
	},
	"700271": {
		"nik": "700271",
		"nama": "ERLANDA FAJARUDDIN, ST",
		"band": "III",
		"posisi": "SENIOR EXPERT DATA CENTER (CNDC)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8681481481481468,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0028106805862294,
			"nilai_komparatif_unit": 1.008124711439086,
			"summary_team": 0.8705882352941177
		}]
	},
	"700293": {
		"nik": "700293",
		"nama": "NUR ACHMAD SUBANDI, ST",
		"band": "III",
		"posisi": "MGR GOV, RISK, COMPLI, POLICY& ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8361111111111127,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9966777408637852,
			"nilai_komparatif_unit": 1.00195927243085,
			"summary_team": 0.8333333333333331
		}]
	},
	"700383": {
		"nik": "700383",
		"nama": "ASTUTI YULIANTI",
		"band": "III",
		"posisi": "MGR LOGISTIC MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0665584415584426,
			"nilai_komparatif_unit": 1.0722102805092435,
			"summary_team": 0.9124999999999999
		}]
	},
	"700453": {
		"nik": "700453",
		"nama": "ERNY MURNIASIH",
		"band": "III",
		"posisi": "SO CFU ENTERPRISE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328042328042327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9606099110546378,
			"nilai_komparatif_unit": 0.9657003142620709,
			"summary_team": 0.7999999999999998
		}]
	},
	"700483": {
		"nik": "700483",
		"nama": "SUPRIYATNO",
		"band": "III",
		"posisi": "MGR CME DEPLOYMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8377777777777767,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9996684350132637,
			"nilai_komparatif_unit": 1.0049658146773714,
			"summary_team": 0.8374999999999999
		}]
	},
	"710253": {
		"nik": "710253",
		"nama": "VITRY JUNITA",
		"band": "III",
		"posisi": "MGR ASSET MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8038095238095228,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.041913507109006,
			"nilai_komparatif_unit": 1.0474347491838798,
			"summary_team": 0.8374999999999999
		}]
	},
	"710404": {
		"nik": "710404",
		"nama": "IRSYAD",
		"band": "III",
		"posisi": "MGR DIGITAL EXPLORATION ALIGNMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666663,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0219780219780223,
			"nilai_komparatif_unit": 1.0273936231925587,
			"summary_team": 0.8857142857142856
		}]
	},
	"710435": {
		"nik": "710435",
		"nama": "WIWIN HADI PURNOMO",
		"band": "III",
		"posisi": "MGR SATELLITE SERVICE & RESOURCES MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.882962962962962,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9909815436241619,
			"nilai_komparatif_unit": 0.9962328902635416,
			"summary_team": 0.8749999999999998
		}]
	},
	"710444": {
		"nik": "710444",
		"nama": "ARIF RACHMAN",
		"band": "III",
		"posisi": "MGR BUSINESS ARCHITECTURE & CX IMPROVEME",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8777777777777767,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0052122114668665,
			"nilai_komparatif_unit": 1.0105389683600825,
			"summary_team": 0.8823529411764706
		}]
	},
	"710452": {
		"nik": "710452",
		"nama": "DONY SAVIUS",
		"band": "III",
		"posisi": "TRIBE LEADER OF SMART CITY SOLUTION DEVE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.87037037037037,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9519756838905781,
			"nilai_komparatif_unit": 0.9570203331482093,
			"summary_team": 0.8285714285714287
		}]
	},
	"710465": {
		"nik": "710465",
		"nama": "PAULUS PARSAORAN",
		"band": "III",
		"posisi": "MGR ENT SERVICE QUALITY MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8705882352941167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9719334719334729,
			"nilai_komparatif_unit": 0.97708388023762,
			"summary_team": 0.846153846153846
		}]
	},
	"710470": {
		"nik": "710470",
		"nama": "INDRA SUYITNO",
		"band": "III",
		"posisi": "MGR ACCESS NETWORK PROCESS & IT TOOL MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8466666666666656,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9448818897637806,
			"nilai_komparatif_unit": 0.9498889480368089,
			"summary_team": 0.7999999999999999
		}]
	},
	"710472": {
		"nik": "710472",
		"nama": "NINA WIYANA",
		"band": "III",
		"posisi": "MGR PERFORMANCE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328042328042327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.065676620076239,
			"nilai_komparatif_unit": 1.0713237861344853,
			"summary_team": 0.8875
		}]
	},
	"710480": {
		"nik": "710480",
		"nama": "SHAIPUL NUR BAHRIP, ST",
		"band": "III",
		"posisi": "MGR TRANSPORT OSP PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8745098039215675,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.014854260089687,
			"nilai_komparatif_unit": 1.0202321115163562,
			"summary_team": 0.8874999999999997
		}]
	},
	"710485": {
		"nik": "710485",
		"nama": "ASLAM",
		"band": "III",
		"posisi": "MGR DATA ACQUISITION & INTEGRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8972222222222217,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0030959752321988,
			"nilai_komparatif_unit": 1.0084115179003765,
			"summary_team": 0.9
		}]
	},
	"710503": {
		"nik": "710503",
		"nama": "RIDWAN SAHRONI",
		"band": "III",
		"posisi": "SR EXPERT SERVICE NODE OPERATION & MAINT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0566037735849052,
			"nilai_komparatif_unit": 1.062202861389587,
			"summary_team": 0.9333333333333332
		}]
	},
	"710505": {
		"nik": "710505",
		"nama": "RUDIYANSYAH",
		"band": "III",
		"posisi": "MGR INTERNAL USE CASE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8777777777777767,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9918093819806416,
			"nilai_komparatif_unit": 0.9970651154486148,
			"summary_team": 0.8705882352941177
		}]
	},
	"710511": {
		"nik": "710511",
		"nama": "HARY MAHENDRA EKA WIBAWA",
		"band": "III",
		"posisi": "MGR EXPOSURE & API FACTORY MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8803921568627461,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0080734966592415,
			"nilai_komparatif_unit": 1.0134154158937503,
			"summary_team": 0.8875
		}]
	},
	"710512": {
		"nik": "710512",
		"nama": "DEDDY KURNIAWAN",
		"band": "III",
		"posisi": "MGR DIVISION SECRETARY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9200000000000002,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0200668896321068,
			"nilai_komparatif_unit": 1.0254723634951433,
			"summary_team": 0.9384615384615385
		}]
	},
	"710517": {
		"nik": "710517",
		"nama": "DUDY SOLEHUDDIN",
		"band": "III",
		"posisi": "MGR ACCESS FIBER OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8466666666666656,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9842519685039381,
			"nilai_komparatif_unit": 0.9894676542050093,
			"summary_team": 0.8333333333333333
		}]
	},
	"710521": {
		"nik": "710521",
		"nama": "FUAD NUR",
		"band": "III",
		"posisi": "MGR CFU/FU IT SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328042328042327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0056385006353241,
			"nilai_komparatif_unit": 1.0109675164931058,
			"summary_team": 0.8375
		}]
	},
	"710532": {
		"nik": "710532",
		"nama": "ABDUL BARI RAHMAT",
		"band": "III",
		"posisi": "MGR CFUC CFUW INTERNT&WIFI INTERFACE MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649358,
			"nilai_komparatif_unit": 0.9400199719533092,
			"summary_team": 0.7999999999999998
		}]
	},
	"720094": {
		"nik": "720094",
		"nama": "TRI WAHYU SARI YULIASTUTI, ST",
		"band": "III",
		"posisi": "MGR BUDGET PLANNING & OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8490196078431361,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9976905311778305,
			"nilai_komparatif_unit": 1.0029774296591896,
			"summary_team": 0.8470588235294118
		}]
	},
	"720098": {
		"nik": "720098",
		"nama": "ADIPRIANA",
		"band": "III",
		"posisi": "MGR OLO ACCESS NETWORK SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8361111111111127,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.980730897009965,
			"nilai_komparatif_unit": 0.9859279240719566,
			"summary_team": 0.8200000000000001
		}]
	},
	"720101": {
		"nik": "720101",
		"nama": "PRA SETIAWAN",
		"band": "III",
		"posisi": "MGR PERFOMANCE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8527777777777763,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.048476719678101,
			"nilai_komparatif_unit": 1.0540327411133878,
			"summary_team": 0.8941176470588235
		}]
	},
	"720113": {
		"nik": "720113",
		"nama": "ALEXANDER KUMAGAP",
		"band": "III",
		"posisi": "MGR SUBMARINE DEPLOYMENT AREA-2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111103,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0016129032258072,
			"nilai_komparatif_unit": 1.0069205868927282,
			"summary_team": 0.8624999999999998
		}]
	},
	"720175": {
		"nik": "720175",
		"nama": "FREDY PASA`BUAN PASAU`",
		"band": "III",
		"posisi": "MGR TRANSPORT & DCME PERFORMANCE & QM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8681481481481468,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9934940273037556,
			"nilai_komparatif_unit": 0.9987586879375403,
			"summary_team": 0.8624999999999998
		}]
	},
	"720186": {
		"nik": "720186",
		"nama": "ERA AJIWIBOWO",
		"band": "III",
		"posisi": "MGR TRANSPORT ISP PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8745098039215675,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9576793721973105,
			"nilai_komparatif_unit": 0.9627542460788152,
			"summary_team": 0.8374999999999999
		}]
	},
	"720187": {
		"nik": "720187",
		"nama": "BEKTI SUPRAYITNO",
		"band": "III",
		"posisi": "MGR DIGI CONNECTIVITY EXPERIENCE CENTER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8705882352941167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9365904365904375,
			"nilai_komparatif_unit": 0.9415535573198883,
			"summary_team": 0.8153846153846153
		}]
	},
	"720197": {
		"nik": "720197",
		"nama": "SONJA YVONNE MOMUAT, ST, MM",
		"band": "III",
		"posisi": "MGR IoT PLATFORM DEPLOYMENT & OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8422222222222243,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9917740183144472,
			"nilai_komparatif_unit": 0.99702956438552,
			"summary_team": 0.8352941176470587
		}]
	},
	"720208": {
		"nik": "720208",
		"nama": "MOCHAMMAD SOEHARIJANTO",
		"band": "III",
		"posisi": "SENIOR ENGINEER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7537037037037038,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7392067392067391,
			"nilai_komparatif_unit": 0.7431238967468721,
			"summary_team": 0.557142857142857
		}]
	},
	"720241": {
		"nik": "720241",
		"nama": "ANDRI PRIHANANTO",
		"band": "III",
		"posisi": "MGR MARKET ACCELERATION & GTM STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666665,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9773755656108598,
			"nilai_komparatif_unit": 0.9825548123131868,
			"summary_team": 0.8470588235294116
		}]
	},
	"720247": {
		"nik": "720247",
		"nama": "RITA HARTATI HAFNI",
		"band": "III",
		"posisi": "SR EXPERT ITGC & IT FINANCIAL MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8594202898550712,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0129947425850625,
			"nilai_komparatif_unit": 1.0183627401743303,
			"summary_team": 0.8705882352941177
		}]
	},
	"720275": {
		"nik": "720275",
		"nama": "HERDI WIDIANTORO",
		"band": "III",
		"posisi": "MGR MARKET ACCELERATION & GTM STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8422222222222243,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.005742666459721,
			"nilai_komparatif_unit": 1.0110722343064429,
			"summary_team": 0.8470588235294116
		}]
	},
	"720292": {
		"nik": "720292",
		"nama": "HADI HARIYANTO",
		"band": "III",
		"posisi": "SENIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8194444444444434,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0193419740777678,
			"nilai_komparatif_unit": 1.024743606514206,
			"summary_team": 0.8352941176470587
		}]
	},
	"720305": {
		"nik": "720305",
		"nama": "DEOT SISWANTORO, M.M",
		"band": "III",
		"posisi": "MGR DIGITAL CONNECTIVITY DTP MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8705882352941167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0072765072765082,
			"nilai_komparatif_unit": 1.0126142031553516,
			"summary_team": 0.8769230769230768
		}]
	},
	"720312": {
		"nik": "720312",
		"nama": "MUHAMAD GAZI",
		"band": "III",
		"posisi": "MGR CORE NETWORK & SERVICE PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555543,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9788961038961053,
			"nilai_komparatif_unit": 0.9840834081386212,
			"summary_team": 0.8374999999999999
		}]
	},
	"720316": {
		"nik": "720316",
		"nama": "MAMAN SULAEMAN",
		"band": "III",
		"posisi": "MGR IT & DIGITAL CATEGORY ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8111111111111113,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9589041095890408,
			"nilai_komparatif_unit": 0.963985473521372,
			"summary_team": 0.7777777777777778
		}]
	},
	"720328": {
		"nik": "720328",
		"nama": "AGUNG HENDRIYANTO",
		"band": "III",
		"posisi": "SENIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8194444444444434,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9906281156530421,
			"nilai_komparatif_unit": 0.9958775894292989,
			"summary_team": 0.8117647058823529
		}]
	},
	"720349": {
		"nik": "720349",
		"nama": "MUH. ZULKIFLI",
		"band": "III",
		"posisi": "MGR INTERNET PRODUCT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9496753246753254,
			"nilai_komparatif_unit": 0.9547077840150797,
			"summary_team": 0.8124999999999998
		}]
	},
	"720410": {
		"nik": "720410",
		"nama": "ISWANTORO",
		"band": "III",
		"posisi": "MGR SPAREPART, REPAIR & CALIBRATION MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.882962962962962,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9768246644295311,
			"nilai_komparatif_unit": 0.9820009918312053,
			"summary_team": 0.8624999999999998
		}]
	},
	"720411": {
		"nik": "720411",
		"nama": "BAYU IRAWAN DWISETYO, ST",
		"band": "III",
		"posisi": "MGR SERVICE/PRODUCT OFFERING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8361111111111127,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9568106312292338,
			"nilai_komparatif_unit": 0.9618809015336159,
			"summary_team": 0.7999999999999998
		}]
	},
	"720416": {
		"nik": "720416",
		"nama": "FIDAR ADJIE LAKSONO",
		"band": "III",
		"posisi": "SO SDN NFV & NETWORK DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8541666666666652,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9658536585365871,
			"nilai_komparatif_unit": 0.9709718490786021,
			"summary_team": 0.8250000000000001
		}]
	},
	"720419": {
		"nik": "720419",
		"nama": "LESMIN NAINGGOLAN",
		"band": "III",
		"posisi": "SO ACCESS NW, ENERGY & SATELIT SERV OPT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8541666666666652,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.019225251076042,
			"nilai_komparatif_unit": 1.0246262649813414,
			"summary_team": 0.8705882352941177
		}]
	},
	"720448": {
		"nik": "720448",
		"nama": "ADI KURNIA SETIAWAN",
		"band": "III",
		"posisi": "MGR EXPOSURE & API FACTORY OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8807017543859639,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0332669322709174,
			"nilai_komparatif_unit": 1.038742354963977,
			"summary_team": 0.9099999999999999
		}]
	},
	"720457": {
		"nik": "720457",
		"nama": "ANTONYUS GORGA MARTUA RADJA S.",
		"band": "III",
		"posisi": "MGR TRIBE PLANNING & PERFORMANCE II",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8833333333333327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0264150943396233,
			"nilai_komparatif_unit": 1.0318542082070286,
			"summary_team": 0.9066666666666666
		}]
	},
	"720468": {
		"nik": "720468",
		"nama": "RAHARJO",
		"band": "III",
		"posisi": "MGR ASSURANCE CONSUMER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8562091503267962,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9862595419847339,
			"nilai_komparatif_unit": 0.9914858660920594,
			"summary_team": 0.8444444444444442
		}]
	},
	"720482": {
		"nik": "720482",
		"nama": "MUHAMMAD ZAINAL AQLI",
		"band": "III",
		"posisi": "GM AREA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9494949494949495,
			"nilai_komparatif_unit": 0.9545264530019707,
			"summary_team": 0.8545454545454546
		}]
	},
	"720517": {
		"nik": "720517",
		"nama": "THEODORUS SETYO WIJANARKO",
		"band": "III",
		"posisi": "MGR ACCESS OSP PROJECT MANAGEMENT AREA 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8733333333333323,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.001908396946566,
			"nilai_komparatif_unit": 1.0072176464750993,
			"summary_team": 0.875
		}]
	},
	"720524": {
		"nik": "720524",
		"nama": "TRIHARTOTO WIDIJARTO",
		"band": "III",
		"posisi": "MGR SOA PLATFORM OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8807017543859639,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0502988047808777,
			"nilai_komparatif_unit": 1.0558644816941525,
			"summary_team": 0.9249999999999999
		}]
	},
	"720526": {
		"nik": "720526",
		"nama": "BUDI SANTOSO",
		"band": "III",
		"posisi": "MGR ACCESS PARTNERSHIP & OPERATION CTRL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8466666666666656,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0367454068241482,
			"nilai_komparatif_unit": 1.0422392624292764,
			"summary_team": 0.8777777777777777
		}]
	},
	"720532": {
		"nik": "720532",
		"nama": "RAHMADI JAYANTO",
		"band": "III",
		"posisi": "MGR OPTIMIZATION COVERAGE & PERFORMANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8738095238095228,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0172570390554052,
			"nilai_komparatif_unit": 1.0226476231362072,
			"summary_team": 0.8888888888888888
		}]
	},
	"720546": {
		"nik": "720546",
		"nama": "BEKTI WIDIHARSO",
		"band": "III",
		"posisi": "MGR ACCESS  PLANNING & SINERGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.871264367816091,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0100263852242755,
			"nilai_komparatif_unit": 1.0153786530821962,
			"summary_team": 0.88
		}]
	},
	"720560": {
		"nik": "720560",
		"nama": "DEDI SETIAJI",
		"band": "III",
		"posisi": "MGR IP-METRO DEPLOYMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8377777777777767,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0295092838196298,
			"nilai_komparatif_unit": 1.0349647942199796,
			"summary_team": 0.8624999999999998
		}]
	},
	"720574": {
		"nik": "720574",
		"nama": "DAVID MARTIN",
		"band": "III",
		"posisi": "MGR BROADBAND NODE & DC DEPLOYMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8377777777777767,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9681697612732107,
			"nilai_komparatif_unit": 0.9733002251601742,
			"summary_team": 0.8111111111111111
		}]
	},
	"720576": {
		"nik": "720576",
		"nama": "ROSLINAWATI",
		"band": "III",
		"posisi": "SO DIGITAL BUSINESS SCALE UP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.941176470588236,
			"nilai_komparatif_unit": 0.9461638933386249,
			"summary_team": 0.7999999999999998
		}]
	},
	"720581": {
		"nik": "720581",
		"nama": "ANDRIYANTO BUDI RAHARJO",
		"band": "III",
		"posisi": "MGR SDN CLOUD & DATA CENTER DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8803921568627461,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9844097995545646,
			"nilai_komparatif_unit": 0.9896263216239441,
			"summary_team": 0.8666666666666667
		}]
	},
	"720591": {
		"nik": "720591",
		"nama": "HADI PRAMUDI",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8833333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.962264150943397,
			"nilai_komparatif_unit": 0.9673633201940894,
			"summary_team": 0.85
		}]
	},
	"720593": {
		"nik": "720593",
		"nama": "BIMO SULISTYO",
		"band": "III",
		"posisi": "MGR ITGC COMPLIANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328042328042327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9205844980940281,
			"nilai_komparatif_unit": 0.9254628011678182,
			"summary_team": 0.7666666666666666
		}]
	},
	"730050": {
		"nik": "730050",
		"nama": "TYAS INDAH TWI HANDAYANI",
		"band": "III",
		"posisi": "MGR TRIBE SUPPORT MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8833333333333327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.033018867924529,
			"nilai_komparatif_unit": 1.0384929760907136,
			"summary_team": 0.9125
		}]
	},
	"730055": {
		"nik": "730055",
		"nama": "AHMAD ARIF RAHMAN",
		"band": "III",
		"posisi": "MGR SYSTEM INTEGRATION & READINESS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7537037037037038,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9166852803216439,
			"nilai_komparatif_unit": 0.9215429209075431,
			"summary_team": 0.6909090909090909
		}]
	},
	"730083": {
		"nik": "730083",
		"nama": "SRI DAMAR SETIAWAN, ST, MM",
		"band": "III",
		"posisi": "MGR WIRELESS PROJECT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8363636363636366,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9864130434782608,
			"nilai_komparatif_unit": 0.9916401810109798,
			"summary_team": 0.8250000000000002
		}]
	},
	"730108": {
		"nik": "730108",
		"nama": "AGUNG WAHONO",
		"band": "III",
		"posisi": "MGR ACCESS PERFORMANCE & QM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8466666666666656,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9580052493438332,
			"nilai_komparatif_unit": 0.9630818500928758,
			"summary_team": 0.8111111111111111
		}]
	},
	"730110": {
		"nik": "730110",
		"nama": "MUHAMMAD BAHRONI",
		"band": "III",
		"posisi": "MGR SDN CLOUD & DATA CENTER OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8807017543859639,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9935258964143437,
			"nilai_komparatif_unit": 0.9987907259269011,
			"summary_team": 0.875
		}]
	},
	"730119": {
		"nik": "730119",
		"nama": "M. JAKA HADI SANTOSA",
		"band": "III",
		"posisi": "MGR DATA CENTRE & CME PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8745098039215675,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9576793721973105,
			"nilai_komparatif_unit": 0.9627542460788152,
			"summary_team": 0.8374999999999999
		}]
	},
	"730125": {
		"nik": "730125",
		"nama": "MOHAMMAD SOVAN HADIBOWO",
		"band": "III",
		"posisi": "SO DIGITAL BIZ TECH PLATFORM EXPLORATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8541666666666652,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0390243902439043,
			"nilai_komparatif_unit": 1.0445303224936477,
			"summary_team": 0.8875
		}]
	},
	"730133": {
		"nik": "730133",
		"nama": "YUSUF HERNATA",
		"band": "III",
		"posisi": "MGR OSS FULFILLMENT DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8803921568627461,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.022271714922048,
			"nilai_komparatif_unit": 1.0276888724556343,
			"summary_team": 0.9
		}]
	},
	"730153": {
		"nik": "730153",
		"nama": "EVA KURNIA DAMAYANTI",
		"band": "III",
		"posisi": "MGR  IT SYSTEM DELIVERY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328042328042327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0356575603557816,
			"nilai_komparatif_unit": 1.0411456513137956,
			"summary_team": 0.8625
		}]
	},
	"730169": {
		"nik": "730169",
		"nama": "BINSAR SAHAT MANGAPUL SINAGA",
		"band": "III",
		"posisi": "MGR WHOLESALE PROV & INTEGRATION TR1,2,3",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8705882352941167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9907094594594604,
			"nilai_komparatif_unit": 0.995959364287665,
			"summary_team": 0.8624999999999998
		}]
	},
	"730170": {
		"nik": "730170",
		"nama": "GATOT IMAM SUKOCO",
		"band": "III",
		"posisi": "MGR CHAPTER DEVELOPR PLANNING,PERF&SUPPT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0202429149797567,
			"nilai_komparatif_unit": 1.0256493216251683,
			"summary_team": 0.8615384615384616
		}]
	},
	"730200": {
		"nik": "730200",
		"nama": "MUNGKI SULISTIONO",
		"band": "III",
		"posisi": "MGR CHAPTER DSGNR STNDRDZTN EXPRNCE MGMT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8588235294117645,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9315068493150686,
			"nilai_komparatif_unit": 0.9364430314207617,
			"summary_team": 0.7999999999999999
		}]
	},
	"730217": {
		"nik": "730217",
		"nama": "HERU WIJAYA, M.M",
		"band": "III",
		"posisi": "MGR COMM & CONVERGENCE PRODUCT MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.866666666666666,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.950226244343892,
			"nilai_komparatif_unit": 0.9552616230822655,
			"summary_team": 0.8235294117647058
		}]
	},
	"730223": {
		"nik": "730223",
		"nama": "RAHMAT SUBAGYO",
		"band": "III",
		"posisi": "MGR RESOURCE & INVENTORY MGT DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8803921568627461,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9796770601336292,
			"nilai_komparatif_unit": 0.9848685027699828,
			"summary_team": 0.8625
		}]
	},
	"730229": {
		"nik": "730229",
		"nama": "WIDYA KRISTIANTO",
		"band": "III",
		"posisi": "MGR BIG DATA ANALYTICS PLANNING&EVALUATN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8158730158730153,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9805447470817127,
			"nilai_komparatif_unit": 0.985740787709793,
			"summary_team": 0.7999999999999999
		}]
	},
	"730259": {
		"nik": "730259",
		"nama": "FEBRINA INDRIANI S",
		"band": "III",
		"posisi": "MGR ENT PROVISIONING&INTEGRATION TR1,2,3",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8705882352941167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.056756756756758,
			"nilai_komparatif_unit": 1.0623566552401762,
			"summary_team": 0.9199999999999999
		}]
	},
	"730294": {
		"nik": "730294",
		"nama": "ARINTO SETIAWAN,ST",
		"band": "III",
		"posisi": "MGR INVENTORY MANAGEMENT OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8746031746031734,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9575771324863898,
			"nilai_komparatif_unit": 0.9626514645856931,
			"summary_team": 0.8375
		}]
	},
	"730302": {
		"nik": "730302",
		"nama": "DHONI MUHAMAD ROOM ANUGROHO",
		"band": "III",
		"posisi": "GM AREA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0472727272727271,
			"nilai_komparatif_unit": 1.0528223685877054,
			"summary_team": 0.8727272727272727
		}]
	},
	"730319": {
		"nik": "730319",
		"nama": "BADRUN NAJIB",
		"band": "III",
		"posisi": "MGR BILLING & REVENUE MGT SYSTEM OPR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8594202898550712,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9308600337268141,
			"nilai_komparatif_unit": 0.9357927882683034,
			"summary_team": 0.8
		}]
	},
	"730347": {
		"nik": "730347",
		"nama": "NUR WAHID",
		"band": "III",
		"posisi": "MGR SETTLEMENT & BUSINESS ANALYST",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111104,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9580645161290331,
			"nilai_komparatif_unit": 0.9631414309408706,
			"summary_team": 0.8250000000000001
		}]
	},
	"730385": {
		"nik": "730385",
		"nama": "FENDY PURWANTO",
		"band": "III",
		"posisi": "MGR IP NETWORK PERFORMANCE & QM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8577777777777769,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9763601036269439,
			"nilai_komparatif_unit": 0.9815339692574326,
			"summary_team": 0.8374999999999999
		}]
	},
	"730447": {
		"nik": "730447",
		"nama": "HEZEKIELI GULO",
		"band": "III",
		"posisi": "MGR PARTNERSHIP SYST DEV'T&STANDARDIZATN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111104,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0741935483870977,
			"nilai_komparatif_unit": 1.0798858468124912,
			"summary_team": 0.9249999999999999
		}]
	},
	"730448": {
		"nik": "730448",
		"nama": "EKA KELANA",
		"band": "III",
		"posisi": "MGR API CAPABILITY & SERVICE CREATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8753623188405792,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9291390728476826,
			"nilai_komparatif_unit": 0.9340627077822656,
			"summary_team": 0.8133333333333334
		}]
	},
	"730460": {
		"nik": "730460",
		"nama": "PROKLAMANTORO DWIYOGA",
		"band": "III",
		"posisi": "MGR SESSION BORDER CONTROLLER & PERFORM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666656,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.971659919028341,
			"nilai_komparatif_unit": 0.9768088777382568,
			"summary_team": 0.8421052631578945
		}]
	},
	"730469": {
		"nik": "730469",
		"nama": "MUHAMMAD HARY ZULFAHRI",
		"band": "III",
		"posisi": "MGR BUSINESS PLANNING & PERFORMANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8714285714285706,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0040983606557385,
			"nilai_komparatif_unit": 1.009419215101274,
			"summary_team": 0.8749999999999998
		}]
	},
	"730471": {
		"nik": "730471",
		"nama": "WAWAN SUTARWAN",
		"band": "III",
		"posisi": "MGR BIG DATA OPR & DATA ACQUISITION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8589147286821694,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0272881715863253,
			"nilai_komparatif_unit": 1.0327319120093863,
			"summary_team": 0.8823529411764706
		}]
	},
	"730472": {
		"nik": "730472",
		"nama": "ANTON WAHYU PRAMONO",
		"band": "III",
		"posisi": "TRIBE LEADER OF VGA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8176100628930801,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9972781065088778,
			"nilai_komparatif_unit": 1.0025628194955492,
			"summary_team": 0.8153846153846155
		}]
	},
	"730474": {
		"nik": "730474",
		"nama": "SUGENG RIYANTO",
		"band": "III",
		"posisi": "MGR ORDER MANAGEMENT OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8746031746031734,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9861615245009089,
			"nilai_komparatif_unit": 0.9913873292001913,
			"summary_team": 0.8625
		}]
	},
	"730483": {
		"nik": "730483",
		"nama": "TWINTO GANDIDI",
		"band": "III",
		"posisi": "SENIOR EXPERT DIGITAL BUSINESS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8422222222222243,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9358994257333515,
			"nilai_komparatif_unit": 0.9408588847018287,
			"summary_team": 0.7882352941176469
		}]
	},
	"730490": {
		"nik": "730490",
		"nama": "EDIYANUS PABENDON",
		"band": "III",
		"posisi": "MGR SUBMARINE DEPLOYMENT AREA-1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111103,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0016129032258072,
			"nilai_komparatif_unit": 1.0069205868927282,
			"summary_team": 0.8624999999999998
		}]
	},
	"730506": {
		"nik": "730506",
		"nama": "BURHAN",
		"band": "III",
		"posisi": "MGR FINANCE & SCM OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8589147286821694,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.975067689530687,
			"nilai_komparatif_unit": 0.9802347064822424,
			"summary_team": 0.8374999999999999
		}]
	},
	"730546": {
		"nik": "730546",
		"nama": "YOVITA MISRIGA, ST",
		"band": "III",
		"posisi": "MGR COHERENCT CAPABILITY,QUALITY&RISK MG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8777777777777767,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9918093819806416,
			"nilai_komparatif_unit": 0.9970651154486148,
			"summary_team": 0.8705882352941177
		}]
	},
	"730557": {
		"nik": "730557",
		"nama": "TONGAM RONI HORAS SIMATUPANG",
		"band": "III",
		"posisi": "MGR CEM OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8594202898550712,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.047217537942666,
			"nilai_komparatif_unit": 1.0527668868018414,
			"summary_team": 0.9
		}]
	},
	"730576": {
		"nik": "730576",
		"nama": "WULAN TRI WAHYUDI",
		"band": "III",
		"posisi": "MGR IOT PLATFORM DEV'T",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8472222222222218,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0622950819672137,
			"nilai_komparatif_unit": 1.0679243287928575,
			"summary_team": 0.9
		}]
	},
	"730584": {
		"nik": "730584",
		"nama": "HENGKI HIKMAWAN",
		"band": "III",
		"posisi": "MGR PROCUREMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8038095238095228,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.967614533965246,
			"nilai_komparatif_unit": 0.9727420556268204,
			"summary_team": 0.7777777777777777
		}]
	},
	"730589": {
		"nik": "730589",
		"nama": "SRI PONCO KISWORO",
		"band": "III",
		"posisi": "MGR INFRA SERVICE RESEARCH",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8541666666666652,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.93658536585366,
			"nilai_komparatif_unit": 0.9415484597125837,
			"summary_team": 0.7999999999999999
		}]
	},
	"730590": {
		"nik": "730590",
		"nama": "DIAN AGUNG NUGROHO",
		"band": "III",
		"posisi": "SENIOR ENGINEER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7537037037037038,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8908388908388906,
			"nilai_komparatif_unit": 0.8955595678744355,
			"summary_team": 0.6714285714285713
		}]
	},
	"730599": {
		"nik": "730599",
		"nama": "KASIONO",
		"band": "III",
		"posisi": "MGR DESIGN ECOSYSTEM DEVELOPMENT & INTEG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.87037037037037,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9191489361702133,
			"nilai_komparatif_unit": 0.9240196320051677,
			"summary_team": 0.8000000000000002
		}]
	},
	"730600": {
		"nik": "730600",
		"nama": "DENI RISNANDAR",
		"band": "III",
		"posisi": "MGR DIGITAL PLATFORM ASSURANCE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8833333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0388457269700342,
			"nilai_komparatif_unit": 1.044350712458671,
			"summary_team": 0.9176470588235295
		}]
	},
	"740043": {
		"nik": "740043",
		"nama": "SRI LOKOPOLO",
		"band": "III",
		"posisi": "MGR IP TV OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666656,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.966346153846155,
			"nilai_komparatif_unit": 0.9714669541881258,
			"summary_team": 0.8374999999999999
		}]
	},
	"740068": {
		"nik": "740068",
		"nama": "RIAN HANDONO",
		"band": "III",
		"posisi": "MGR CAPEX, OPEX & RISK MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8833333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9962264150943397,
			"nilai_komparatif_unit": 1.0015055550244685,
			"summary_team": 0.88
		}]
	},
	"740092": {
		"nik": "740092",
		"nama": "ROBBY CAHAYADI, ST. MT",
		"band": "III",
		"posisi": "MGR NFV NETWORK & SECURITY DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8803921568627461,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9228841870824043,
			"nilai_komparatif_unit": 0.9277746765224476,
			"summary_team": 0.8125
		}]
	},
	"740111": {
		"nik": "740111",
		"nama": "ANDREAS WIDIASMONO YANUARDI",
		"band": "III",
		"posisi": "MGR DIGITAL SERVICE ASSURANCE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8833333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0188679245283028,
			"nilai_komparatif_unit": 1.0242670449113889,
			"summary_team": 0.9
		}]
	},
	"740116": {
		"nik": "740116",
		"nama": "BRAHMANDI ANDRIA SASTRA",
		"band": "III",
		"posisi": "MGR FINANCIAL REPORTING PLATFORM DEVELOP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8748299319727879,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9859059097978241,
			"nilai_komparatif_unit": 0.9911303599598594,
			"summary_team": 0.8625
		}]
	},
	"740131": {
		"nik": "740131",
		"nama": "YUDHA INDAH PRIHATINI",
		"band": "III",
		"posisi": "MGR INFRASTRUCTURE USER RELATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7537037037037038,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1082526376644022,
			"nilai_komparatif_unit": 1.1141254198588098,
			"summary_team": 0.8352941176470587
		}]
	},
	"740134": {
		"nik": "740134",
		"nama": "EKO SUTANTO",
		"band": "III",
		"posisi": "GM AREA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8933333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9644087256027554,
			"nilai_komparatif_unit": 0.9695192592476717,
			"summary_team": 0.8615384615384615
		}]
	},
	"740143": {
		"nik": "740143",
		"nama": "HANI BUNTARI",
		"band": "III",
		"posisi": "SENIOR EXPERT SOCIAL NETWORK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8422222222222243,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.033679962750269,
			"nilai_komparatif_unit": 1.0391575741482886,
			"summary_team": 0.8705882352941177
		}]
	},
	"740151": {
		"nik": "740151",
		"nama": "YONGKY HARIMURTI",
		"band": "III",
		"posisi": "MGR NETWORK ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8562091503267962,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9460305343511464,
			"nilai_komparatif_unit": 0.951043679448831,
			"summary_team": 0.81
		}]
	},
	"740165": {
		"nik": "740165",
		"nama": "EKO RUDI WIHARTONO",
		"band": "III",
		"posisi": "MGR STANDARDIZATION & REUSABILITY MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8694444444444439,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0351437699680517,
			"nilai_komparatif_unit": 1.0406291382805801,
			"summary_team": 0.9
		}]
	},
	"740173": {
		"nik": "740173",
		"nama": "EKO NUR PRIHADI",
		"band": "III",
		"posisi": "MGR NMS IP & ACCESS INFRASTRUCTURE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.882962962962962,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9437919463087258,
			"nilai_komparatif_unit": 0.9487932288224207,
			"summary_team": 0.8333333333333331
		}]
	},
	"740186": {
		"nik": "740186",
		"nama": "JEFFRY IRMAWAN",
		"band": "III",
		"posisi": "MGR OPEN INNOVATION ACCELERATION MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8608695652173906,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0686868686868696,
			"nilai_komparatif_unit": 1.074349986463921,
			"summary_team": 0.9199999999999999
		}]
	},
	"740191": {
		"nik": "740191",
		"nama": "M. AZWIR",
		"band": "III",
		"posisi": "MGR ECOSYSTEM SOLUTION & ORCHESTRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666665,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9638009049773757,
			"nilai_komparatif_unit": 0.968908217697726,
			"summary_team": 0.8352941176470587
		}]
	},
	"740203": {
		"nik": "740203",
		"nama": "SANTI FRILMI",
		"band": "III",
		"posisi": "MGR PRM & PLM OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8594202898550712,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9744940978077585,
			"nilai_komparatif_unit": 0.9796580752183801,
			"summary_team": 0.8375
		}]
	},
	"740209": {
		"nik": "740209",
		"nama": "SALAHUDIN ARDI, ST",
		"band": "III",
		"posisi": "MGR  CUSTOMER MGT SYSTEM DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8717948717948707,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.018014705882354,
			"nilai_komparatif_unit": 1.023409304943224,
			"summary_team": 0.8875
		}]
	},
	"740210": {
		"nik": "740210",
		"nama": "MURDIANTO, ST",
		"band": "III",
		"posisi": "MGR ACCESS ISP PROJECT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8733333333333323,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.958969465648856,
			"nilai_komparatif_unit": 0.9640511759118805,
			"summary_team": 0.8374999999999999
		}]
	},
	"740216": {
		"nik": "740216",
		"nama": "CHRISTIANUS ARDHI YUDANTO",
		"band": "III",
		"posisi": "MGR DESIGN ECOSYSTEM DEVELOPMENT & ITG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9079365079365075,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.085664335664336,
			"nilai_komparatif_unit": 1.0914174193592505,
			"summary_team": 0.9857142857142855
		}]
	},
	"740227": {
		"nik": "740227",
		"nama": "INDRA KURNIAWAN BEKTI",
		"band": "III",
		"posisi": "MGR DATA MANAGEMENT OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8589147286821694,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9896209386281599,
			"nilai_komparatif_unit": 0.9948650752357088,
			"summary_team": 0.8499999999999999
		}]
	},
	"740240": {
		"nik": "740240",
		"nama": "URIP DWISAHSONO",
		"band": "III",
		"posisi": "MGR PARTNER & CHANNEL RELATION MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8717948717948707,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0323529411764718,
			"nilai_komparatif_unit": 1.0378235205058046,
			"summary_team": 0.9
		}]
	},
	"740242": {
		"nik": "740242",
		"nama": "FIDIA TRIANI, S.T, M.T",
		"band": "III",
		"posisi": "MGR OPTIMALISASI RESOURCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8583333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0048543689320395,
			"nilai_komparatif_unit": 1.0101792295687564,
			"summary_team": 0.8624999999999998
		}]
	},
	"740248": {
		"nik": "740248",
		"nama": "MUHAMMAD RIZAL",
		"band": "III",
		"posisi": "MGR RESOURCES ALLOCATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8711111111111115,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0102040816326525,
			"nilai_komparatif_unit": 1.0155572911281274,
			"summary_team": 0.8799999999999999
		}]
	},
	"740268": {
		"nik": "740268",
		"nama": "WINDARTO",
		"band": "III",
		"posisi": "MGR DATACOMM PROD INFRA INTEGRA&READINES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9075268817204296,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0284360189573465,
			"nilai_komparatif_unit": 1.0338858419805055,
			"summary_team": 0.9333333333333332
		}]
	},
	"740283": {
		"nik": "740283",
		"nama": "RAKHMAN IMANSYAH",
		"band": "III",
		"posisi": "TRIBE LEADER OF PRE-VACCINATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8972222222222217,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0170278637770904,
			"nilai_komparatif_unit": 1.0224172334267705,
			"summary_team": 0.9125
		}]
	},
	"740285": {
		"nik": "740285",
		"nama": "ROMZI IMRON ROSIDI",
		"band": "III",
		"posisi": "MGR API OPERATION & INTEGRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8753623188405792,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0205298013245039,
			"nilai_komparatif_unit": 1.0259377282198656,
			"summary_team": 0.8933333333333334
		}]
	},
	"740302": {
		"nik": "740302",
		"nama": "ERWIN SURYANTO",
		"band": "III",
		"posisi": "MGR TGROUP&OLO WLESS ACC PROJ CTRL&INTEG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8231884057971008,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0477552816901414,
			"nilai_komparatif_unit": 1.0533074801269293,
			"summary_team": 0.8624999999999998
		}]
	},
	"740315": {
		"nik": "740315",
		"nama": "SOEDARMANTO HARJONO",
		"band": "III",
		"posisi": "SO DIG PLATFORM ORCHESTRATION PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0313216195569148,
			"nilai_komparatif_unit": 1.0367867337720327,
			"summary_team": 0.8823529411764706
		}]
	},
	"750031": {
		"nik": "750031",
		"nama": "IBNU ALINURSAFA",
		"band": "III",
		"posisi": "SM IOT PLATFORM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8755555555555548,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9441624365482242,
			"nilai_komparatif_unit": 0.9491656823403339,
			"summary_team": 0.8266666666666667
		}]
	},
	"750034": {
		"nik": "750034",
		"nama": "ADI PERMADI",
		"band": "III",
		"posisi": "MGR INFRASTRUCTURE QUALITY ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7537037037037038,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0835380835380835,
			"nilai_komparatif_unit": 1.089279899932381,
			"summary_team": 0.8166666666666667
		}]
	},
	"750070": {
		"nik": "750070",
		"nama": "ELNOFIAN",
		"band": "III",
		"posisi": "MGR PAYMENT COMPLIANCE & EVALUATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649358,
			"nilai_komparatif_unit": 0.9400199719533092,
			"summary_team": 0.7999999999999998
		}]
	},
	"750071": {
		"nik": "750071",
		"nama": "MUHAMMAD ZAINI MAULANA",
		"band": "III",
		"posisi": "MGR SYSTEM LIFECYCLE & SERVICE DESK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9890109890109888,
			"nilai_komparatif_unit": 0.994251893412153,
			"summary_team": 0.8571428571428572
		}]
	},
	"750079": {
		"nik": "750079",
		"nama": "DESY ANDIONO",
		"band": "III",
		"posisi": "MGR BILLING & REVENUE MGT SYSTEM DEVT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8717948717948707,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0323529411764718,
			"nilai_komparatif_unit": 1.0378235205058046,
			"summary_team": 0.9
		}]
	},
	"755548": {
		"nik": "755548",
		"nama": "DENNY ARIFIANDI",
		"band": "III",
		"posisi": "SENIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.749999999999999,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8571428571428581,
			"nilai_komparatif_unit": 0.8616849742905337,
			"summary_team": 0.6428571428571427
		}]
	},
	"760012": {
		"nik": "760012",
		"nama": "TIBERIUS LEO AGUNG",
		"band": "III",
		"posisi": "MGR INET PRODUCT PORTFOLIO & QUALITY MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649362,
			"nilai_komparatif_unit": 0.9400199719533096,
			"summary_team": 0.8
		}]
	},
	"760030": {
		"nik": "760030",
		"nama": "ARISANDY",
		"band": "III",
		"posisi": "MGR VALUE ADDED SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8738095238095228,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9727520435967312,
			"nilai_komparatif_unit": 0.9779067896239979,
			"summary_team": 0.8499999999999999
		}]
	},
	"760038": {
		"nik": "760038",
		"nama": "ALFINA FAIRUZ",
		"band": "III",
		"posisi": "MGR LEGAL & COMPLIANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8038095238095228,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8570300157977895,
			"nilai_komparatif_unit": 0.8615715349837553,
			"summary_team": 0.6888888888888889
		}]
	},
	"760047": {
		"nik": "760047",
		"nama": "BAGUS BUDI SANTOSO",
		"band": "III",
		"posisi": "MGR PLAYCOURT CAPABILITY&SERVICE CREATN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8753623188405792,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.007985975847293,
			"nilai_komparatif_unit": 1.0133274312970577,
			"summary_team": 0.8823529411764706
		}]
	},
	"760053": {
		"nik": "760053",
		"nama": "WISDARMANTO ERLANGGA",
		"band": "III",
		"posisi": "MGR ASSURANCE DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8562091503267962,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9992366412213752,
			"nilai_komparatif_unit": 1.0045317327511656,
			"summary_team": 0.8555555555555554
		}]
	},
	"765365": {
		"nik": "765365",
		"nama": "RIZA AGUSTIANSYAH",
		"band": "III",
		"posisi": "SENIOR DATA SCIENTIST",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8155555555555545,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8366725436768723,
			"nilai_komparatif_unit": 0.8411061858357671,
			"summary_team": 0.6823529411764705
		}]
	},
	"770028": {
		"nik": "770028",
		"nama": "EDY MULYONO",
		"band": "III",
		"posisi": "MGR ASSET DATABASE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8111111111111113,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9427880741337628,
			"nilai_komparatif_unit": 0.947784036991601,
			"summary_team": 0.7647058823529411
		}]
	},
	"770038": {
		"nik": "770038",
		"nama": "ANGELIN SONY TIKUPASANG",
		"band": "III",
		"posisi": "MGR CHAPTER DESIGNER CAPABILITY MGMT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8588235294117645,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.071232876712329,
			"nilai_komparatif_unit": 1.0769094861338762,
			"summary_team": 0.92
		}]
	},
	"770046": {
		"nik": "770046",
		"nama": "I WAYAN SUKADANA",
		"band": "III",
		"posisi": "MGR PLANNING MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649362,
			"nilai_komparatif_unit": 0.9400199719533096,
			"summary_team": 0.8
		}]
	},
	"770057": {
		"nik": "770057",
		"nama": "HARUN TRIYANTORO",
		"band": "III",
		"posisi": "SENIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8685185185185175,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0565659099460691,
			"nilai_komparatif_unit": 1.0621647971061539,
			"summary_team": 0.9176470588235293
		}]
	},
	"770063": {
		"nik": "770063",
		"nama": "KATHRIN EUNICE SIMANGUNSONG",
		"band": "III",
		"posisi": "MGR ENT INTERNET & WIFI INTERFACE MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9083333333333328,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0715596330275234,
			"nilai_komparatif_unit": 1.0772379739754434,
			"summary_team": 0.9733333333333333
		}]
	},
	"770079": {
		"nik": "770079",
		"nama": "MUHAMMAD QADRIANSYAH, ST, MT",
		"band": "III",
		"posisi": "MGR ASSURANCE DES & DGS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8562091503267962,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0511450381679406,
			"nilai_komparatif_unit": 1.05671519938759,
			"summary_team": 0.9000000000000001
		}]
	},
	"780035": {
		"nik": "780035",
		"nama": "ARIF WIJONARKO",
		"band": "III",
		"posisi": "MGR ORDER & COMPLEX EVENT PROCESSING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8803921568627461,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9591685226429092,
			"nilai_komparatif_unit": 0.9642512877361508,
			"summary_team": 0.8444444444444446
		}]
	},
	"780061": {
		"nik": "780061",
		"nama": "MOHAMAD FAJAR ADITYA MASY",
		"band": "III",
		"posisi": "MGR BROADBAND NODE PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555543,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9788961038961053,
			"nilai_komparatif_unit": 0.9840834081386212,
			"summary_team": 0.8374999999999999
		}]
	},
	"780073": {
		"nik": "780073",
		"nama": "RADEN GUGGIE YUDHISTIRA",
		"band": "III",
		"posisi": "MGR PARTNERSHIP STRATEGY,DESIGN&CONTROL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111104,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0306451612903234,
			"nilai_komparatif_unit": 1.0361066908606333,
			"summary_team": 0.8875
		}]
	},
	"790023": {
		"nik": "790023",
		"nama": "ABDUL USMAN",
		"band": "III",
		"posisi": "MGR SERVICE INTEGRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8711111111111115,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.02641056422569,
			"nilai_komparatif_unit": 1.0318496540874025,
			"summary_team": 0.8941176470588237
		}]
	},
	"790036": {
		"nik": "790036",
		"nama": "ANDRIYONO HUTAGALUNG",
		"band": "III",
		"posisi": "MGR IOT PLATFORM OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8472222222222218,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9000000000000005,
			"nilai_komparatif_unit": 0.9047692230050599,
			"summary_team": 0.7625
		}]
	},
	"790060": {
		"nik": "790060",
		"nama": "DIAN LESTARI",
		"band": "III",
		"posisi": "MGR VIDEO PLATFORM PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555543,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1250000000000016,
			"nilai_komparatif_unit": 1.130961528756326,
			"summary_team": 0.9624999999999999
		}]
	},
	"790072": {
		"nik": "790072",
		"nama": "SONNY KHOERONI",
		"band": "III",
		"posisi": "KEPALA YAKES REGIONAL III JAWA BARAT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090908,
			"nilai_komparatif_unit": 1.09668996727886,
			"summary_team": 0.9272727272727272
		}]
	},
	"790073": {
		"nik": "790073",
		"nama": "IRNA WAHYUNI",
		"band": "III",
		"posisi": "SR EXPERT BIG DATA ANALYTICS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8748299319727879,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9548074284146019,
			"nilai_komparatif_unit": 0.9598670834734869,
			"summary_team": 0.8352941176470587
		}]
	},
	"790099": {
		"nik": "790099",
		"nama": "NATALINA HOTDINAR Br SIHALOHO",
		"band": "III",
		"posisi": "SR EXPERT CRM & CX SYSTEM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8717948717948707,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9716262975778558,
			"nilai_komparatif_unit": 0.9767750781231103,
			"summary_team": 0.8470588235294116
		}]
	},
	"790101": {
		"nik": "790101",
		"nama": "DODDY NUR PRATOMO",
		"band": "III",
		"posisi": "MGR SOA PLATFORM DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8803921568627461,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9796770601336292,
			"nilai_komparatif_unit": 0.9848685027699828,
			"summary_team": 0.8625
		}]
	},
	"790102": {
		"nik": "790102",
		"nama": "DIANING MURTI PRAMESTI",
		"band": "III",
		"posisi": "SO DIGITAL BUSINESS POLICY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8472222222222214,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.985920925747349,
			"nilai_komparatif_unit": 0.9911454554809532,
			"summary_team": 0.8352941176470589
		}]
	},
	"790124": {
		"nik": "790124",
		"nama": "DHENNY PRASETYO",
		"band": "III",
		"posisi": "MGR WIRELESS PROD INFRA INTEGRATE READI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8888888888888881,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0406250000000008,
			"nilai_komparatif_unit": 1.0461394140996008,
			"summary_team": 0.9249999999999998
		}]
	},
	"790126": {
		"nik": "790126",
		"nama": "YUDHI WIDYATAMA",
		"band": "III",
		"posisi": "MGR FINANCE APPLICATION PLATFORM DEV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8748299319727879,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227551772120829,
			"nilai_komparatif_unit": 1.02817489667842,
			"summary_team": 0.8947368421052632
		}]
	},
	"795616": {
		"nik": "795616",
		"nama": "FEBBY KOSA DEVA",
		"band": "III",
		"posisi": "MGR BIG DATA EXTERNAL USE CASES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8158730158730153,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9958657587548645,
			"nilai_komparatif_unit": 1.0011429875177587,
			"summary_team": 0.8125
		}]
	},
	"795668": {
		"nik": "795668",
		"nama": "SUYANTO",
		"band": "III",
		"posisi": "SENIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7333333333333337,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9545454545454538,
			"nilai_komparatif_unit": 0.9596037213690016,
			"summary_team": 0.6999999999999998
		}]
	},
	"800032": {
		"nik": "800032",
		"nama": "ARIF NURKHAMID, S.KOM",
		"band": "III",
		"posisi": "SO CX CHANGE MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7944444444444441,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0514191690662282,
			"nilai_komparatif_unit": 1.0569907829429739,
			"summary_team": 0.8352941176470587
		}]
	},
	"800045": {
		"nik": "800045",
		"nama": "MARLINA",
		"band": "III",
		"posisi": "MGR LEGAL COMPLIANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8695035460992895,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0063213703099525,
			"nilai_komparatif_unit": 1.0116540047874694,
			"summary_team": 0.875
		}]
	},
	"800064": {
		"nik": "800064",
		"nama": "DWI AGUSTINA",
		"band": "III",
		"posisi": "MGR WORKFORCE MANAGEMENT DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8803921568627461,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9735921094495692,
			"nilai_komparatif_unit": 0.9787513071006039,
			"summary_team": 0.857142857142857
		}]
	},
	"800072": {
		"nik": "800072",
		"nama": "CAKRA TRIAJI",
		"band": "III",
		"posisi": "MGR IP METRO PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555543,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857156,
			"nilai_komparatif_unit": 0.9693955960768508,
			"summary_team": 0.825
		}]
	},
	"800081": {
		"nik": "800081",
		"nama": "RANI WISHNU HENINGTYAS",
		"band": "III",
		"posisi": "SENIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8685185185185175,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.015928759563528,
			"nilai_komparatif_unit": 1.0213123049097634,
			"summary_team": 0.8823529411764706
		}]
	},
	"800086": {
		"nik": "800086",
		"nama": "FREDDY JUNJUNGAN SIAHAAN, ST",
		"band": "III",
		"posisi": "MGR SECRETARY & ADMINISTRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8038095238095228,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0920221169036348,
			"nilai_komparatif_unit": 1.0978088913502688,
			"summary_team": 0.8777777777777778
		}]
	},
	"800115": {
		"nik": "800115",
		"nama": "LAKSMI ADYAWARDHANI",
		"band": "III",
		"posisi": "MGR PRODUCT PARTNERSHIP MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9083333333333328,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9325418240690776,
			"nilai_komparatif_unit": 0.9374834906474446,
			"summary_team": 0.8470588235294116
		}]
	},
	"810004": {
		"nik": "810004",
		"nama": "MOCHAMAD FADHOLY",
		"band": "III",
		"posisi": "MGR IOT PLATFORM DEPLOYMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8472222222222218,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.988524590163935,
			"nilai_komparatif_unit": 0.9937629170711314,
			"summary_team": 0.8375
		}]
	},
	"810023": {
		"nik": "810023",
		"nama": "HARI AGUNG WIBOWO",
		"band": "III",
		"posisi": "MGR  NFV NETWORK & SECURITY OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8807017543859639,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0077191235059773,
			"nilai_komparatif_unit": 1.013059164868714,
			"summary_team": 0.8875
		}]
	},
	"810038": {
		"nik": "810038",
		"nama": "SHINTA IRAWATI",
		"band": "III",
		"posisi": "MGR PRODUCT & SERVICES DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8361111111111127,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0166112956810613,
			"nilai_komparatif_unit": 1.0219984578794674,
			"summary_team": 0.8500000000000001
		}]
	},
	"810039": {
		"nik": "810039",
		"nama": "BRIAN PRAKOSA",
		"band": "III",
		"posisi": "MGR PLAYCOURT OPERATION & INTEGRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8753623188405792,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0771050141911072,
			"nilai_komparatif_unit": 1.0828127408717128,
			"summary_team": 0.9428571428571426
		}]
	},
	"810045": {
		"nik": "810045",
		"nama": "MUHAMMAD RIZAL",
		"band": "III",
		"posisi": "MGR GLOBAL & ENHANCED SERVICES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8705882352941167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0024570024570036,
			"nilai_komparatif_unit": 1.0077691591211158,
			"summary_team": 0.8727272727272728
		}]
	},
	"810057": {
		"nik": "810057",
		"nama": "VINO HARSADITYA SAPUTRA",
		"band": "III",
		"posisi": "VP MARKETING&DIGITAL PRODUCT INCUBATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9725274725274723,
			"nilai_komparatif_unit": 0.9776810285219504,
			"summary_team": 0.8428571428571427
		}]
	},
	"810077": {
		"nik": "810077",
		"nama": "RULI HAKIM CAHYONO",
		"band": "III",
		"posisi": "MGR IP BROADBAND OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8577777777777769,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9909326424870478,
			"nilai_komparatif_unit": 0.9961837299926184,
			"summary_team": 0.8500000000000001
		}]
	},
	"810079": {
		"nik": "810079",
		"nama": "TUBAGUS ARIEF FAHMI",
		"band": "III",
		"posisi": "MGR DIGITIZATION ALIGNMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666663,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9663461538461543,
			"nilai_komparatif_unit": 0.9714669541881251,
			"summary_team": 0.8375
		}]
	},
	"810087": {
		"nik": "810087",
		"nama": "WELLY BERMANA",
		"band": "III",
		"posisi": "MGR OSP ACCESS DESIGN AREA 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.871264367816091,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9722179109110672,
			"nilai_komparatif_unit": 0.9773698264962315,
			"summary_team": 0.8470588235294116
		}]
	},
	"810092": {
		"nik": "810092",
		"nama": "HENGKI KRISTIAN AUDITA",
		"band": "III",
		"posisi": "SENIOR EXPERT ENGINEERING IPTV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8466666666666656,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9448818897637808,
			"nilai_komparatif_unit": 0.9498889480368091,
			"summary_team": 0.8
		}]
	},
	"810102": {
		"nik": "810102",
		"nama": "DIANA SUTANTI",
		"band": "III",
		"posisi": "MGR PARTNERSHIP INITIATION MGMT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111104,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9725806451612912,
			"nilai_komparatif_unit": 0.9777344829248231,
			"summary_team": 0.8375
		}]
	},
	"820079": {
		"nik": "820079",
		"nama": "RONI SETYO WIBOWO",
		"band": "III",
		"posisi": "MGR OLO READINESS & INTEGRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8361111111111127,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0764119601328883,
			"nilai_komparatif_unit": 1.0821160142253181,
			"summary_team": 0.8999999999999999
		}]
	},
	"820082": {
		"nik": "820082",
		"nama": "LOSYE RATIH FARASTUTI",
		"band": "III",
		"posisi": "MGR SYSTEM ACHITECTURE & CX IMPROVEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.829743589743589,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006689449574639,
			"nilai_komparatif_unit": 1.0120240345544858,
			"summary_team": 0.8352941176470587
		}]
	},
	"820083": {
		"nik": "820083",
		"nama": "WARDHANI PRIHARTIWI",
		"band": "III",
		"posisi": "MGR LEGAL & COMPLIANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9200000000000002,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9698996655518393,
			"nilai_komparatif_unit": 0.9750392964380051,
			"summary_team": 0.8923076923076922
		}]
	},
	"820086": {
		"nik": "820086",
		"nama": "INEKE ADILLA PUSPITA, M.T",
		"band": "III",
		"posisi": "MGR SERVICE OPERATION PROCUREMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8038095238095228,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0077014218009492,
			"nilai_komparatif_unit": 1.0130413693599316,
			"summary_team": 0.81
		}]
	},
	"820092": {
		"nik": "820092",
		"nama": "YULIA NOVIANTI",
		"band": "III",
		"posisi": "MGR PROGRAM PLANNING & BUDGET CONTROL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8695035460992895,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.11941272430669,
			"nilai_komparatif_unit": 1.1253446453254896,
			"summary_team": 0.9733333333333333
		}]
	},
	"830016": {
		"nik": "830016",
		"nama": "EKO SLAMET RAHARJO",
		"band": "III",
		"posisi": "SO CFU CONSUMER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328042328042327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9263024142312579,
			"nilai_komparatif_unit": 0.9312110173241399,
			"summary_team": 0.7714285714285712
		}]
	},
	"830038": {
		"nik": "830038",
		"nama": "ATIK ARIYANI",
		"band": "III",
		"posisi": "MGR BIG DATA&AI SERVICE CREATION&INTGRTN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8694444444444439,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0351437699680517,
			"nilai_komparatif_unit": 1.0406291382805801,
			"summary_team": 0.9
		}]
	},
	"830047": {
		"nik": "830047",
		"nama": "ERIKA YULANDA, MBA",
		"band": "III",
		"posisi": "MGR WIRELESS PRODUCT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8888888888888881,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.050000000000001,
			"nilai_komparatif_unit": 1.0555640935059036,
			"summary_team": 0.9333333333333332
		}]
	},
	"830053": {
		"nik": "830053",
		"nama": "DAFRY REKSAVAGITA",
		"band": "III",
		"posisi": "MGR COMM & CONVERGENCE PROD DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.866666666666666,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9663461538461544,
			"nilai_komparatif_unit": 0.9714669541881252,
			"summary_team": 0.8374999999999999
		}]
	},
	"830056": {
		"nik": "830056",
		"nama": "I MADE ADI WIRAWAN",
		"band": "III",
		"posisi": "MGR WIRELESS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8738095238095228,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9536784741144424,
			"nilai_komparatif_unit": 0.9587321466901941,
			"summary_team": 0.8333333333333333
		}]
	},
	"830063": {
		"nik": "830063",
		"nama": "ARRY FAJAR FIRDAUS",
		"band": "III",
		"posisi": "MGR BIG DATA INTERNAL USE CASES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8158730158730153,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9239748578269985,
			"nilai_komparatif_unit": 0.9288711268803819,
			"summary_team": 0.7538461538461538
		}]
	},
	"830069": {
		"nik": "830069",
		"nama": "IKA PUSPITASARI",
		"band": "III",
		"posisi": "MGR WIRELESS PRODUCT DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8888888888888881,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0406250000000008,
			"nilai_komparatif_unit": 1.0461394140996008,
			"summary_team": 0.9249999999999998
		}]
	},
	"830073": {
		"nik": "830073",
		"nama": "MUHAMMAD RACHMAD",
		"band": "III",
		"posisi": "MGR PROCESS ORCHESTRATION & INTEGRAT OPR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8807017543859639,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0786852589641447,
			"nilai_komparatif_unit": 1.0844013595777784,
			"summary_team": 0.95
		}]
	},
	"830082": {
		"nik": "830082",
		"nama": "RATNA INDAH MURDIANTO",
		"band": "III",
		"posisi": "MGR DIGITAL BUSINESS INTERFACE MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9083333333333328,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0045871559633033,
			"nilai_komparatif_unit": 1.0099106006019782,
			"summary_team": 0.9124999999999999
		}]
	},
	"830090": {
		"nik": "830090",
		"nama": "HOPY FAMILIANTO",
		"band": "III",
		"posisi": "MGR TRIBE SYSTEM DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8833333333333327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0113207547169818,
			"nilai_komparatif_unit": 1.0166798816157487,
			"summary_team": 0.8933333333333333
		}]
	},
	"830103": {
		"nik": "830103",
		"nama": "YULINE RENA CHRISANTI",
		"band": "III",
		"posisi": "MGR WIRELESS PRODUCT PORTFOLIO &QUAL MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8888888888888881,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9750000000000009,
			"nilai_komparatif_unit": 0.9801666582554819,
			"summary_team": 0.8666666666666666
		}]
	},
	"830127": {
		"nik": "830127",
		"nama": "MOCHAMAD TAUFAN AJI KURNIAWAN",
		"band": "III",
		"posisi": "MGR IP BACKBONE & VPN OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8577777777777769,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0142487046632136,
			"nilai_komparatif_unit": 1.0196233471689153,
			"summary_team": 0.8700000000000001
		}]
	},
	"830133": {
		"nik": "830133",
		"nama": "SITI MAHMUDAH",
		"band": "III",
		"posisi": "MGR OLO SERV QUALITY & PERFORMANCE MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8361111111111127,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9169435215946824,
			"nilai_komparatif_unit": 0.921802530636382,
			"summary_team": 0.7666666666666665
		}]
	},
	"830136": {
		"nik": "830136",
		"nama": "ACHMAD KHALIF HAKIM",
		"band": "III",
		"posisi": "MGR ASSURANCE WINS (NON TSEL)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8562091503267962,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.986259541984734,
			"nilai_komparatif_unit": 0.9914858660920595,
			"summary_team": 0.8444444444444443
		}]
	},
	"830145": {
		"nik": "830145",
		"nama": "RATIH WIWEKA",
		"band": "III",
		"posisi": "SENIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9079365079365075,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9588646647470181,
			"nilai_komparatif_unit": 0.9639458196557404,
			"summary_team": 0.8705882352941177
		}]
	},
	"840032": {
		"nik": "840032",
		"nama": "MIHYA ZAKI, S.Kom",
		"band": "III",
		"posisi": "MGR OLO CORE NETWORK SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8361111111111127,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0524916943521576,
			"nilai_komparatif_unit": 1.0580689916869779,
			"summary_team": 0.8800000000000001
		}]
	},
	"840041": {
		"nik": "840041",
		"nama": "HAFIZ FAUZAN ALANURI",
		"band": "III",
		"posisi": "SO CFU DIGITAL SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328042328042327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.960609911054638,
			"nilai_komparatif_unit": 0.9657003142620711,
			"summary_team": 0.7999999999999999
		}]
	},
	"840043": {
		"nik": "840043",
		"nama": "ANDRI SEMBIRING",
		"band": "III",
		"posisi": "MGR DATA MANAGEMENT & PRODUCT CATALOG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0597402597402608,
			"nilai_komparatif_unit": 1.0653559682137506,
			"summary_team": 0.9066666666666666
		}]
	},
	"840051": {
		"nik": "840051",
		"nama": "DARU PUSPITANINGRUM",
		"band": "III",
		"posisi": "MGR BIG DATA&AI CUSTOMR IDENTIFY&PROFILE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8694444444444439,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9776357827476044,
			"nilai_komparatif_unit": 0.9828164083761035,
			"summary_team": 0.85
		}]
	},
	"840054": {
		"nik": "840054",
		"nama": "DODDY SULISTIO NUGROHO",
		"band": "III",
		"posisi": "MGR ISP ACCESS DESIGN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.871264367816091,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9947229551451199,
			"nilai_komparatif_unit": 0.9999941280354964,
			"summary_team": 0.8666666666666667
		}]
	},
	"840056": {
		"nik": "840056",
		"nama": "HANIF FAUZAN",
		"band": "III",
		"posisi": "MGR ACCESS PROJECT REPORT & ADM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8583333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.990291262135923,
			"nilai_komparatif_unit": 0.9955389508793541,
			"summary_team": 0.8499999999999999
		}]
	},
	"840065": {
		"nik": "840065",
		"nama": "ANDI WAHYUDI",
		"band": "III",
		"posisi": "MGR BROADBAND ACCESS NETWORK RESEARCH",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8541666666666652,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9658536585365871,
			"nilai_komparatif_unit": 0.9709718490786021,
			"summary_team": 0.8250000000000001
		}]
	},
	"840066": {
		"nik": "840066",
		"nama": "SENDYLENVI REGIA",
		"band": "III",
		"posisi": "MGR KNOWLEDGE & OUTSOURCE MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8695035460992895,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9507340946166409,
			"nilai_komparatif_unit": 0.9557721645230187,
			"summary_team": 0.8266666666666667
		}]
	},
	"840069": {
		"nik": "840069",
		"nama": "ACHMAD HILMAN",
		"band": "III",
		"posisi": "MGR TRIBE PLANNING & PERFORMANCE I",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8833333333333327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9339622641509441,
			"nilai_komparatif_unit": 0.9389114578354397,
			"summary_team": 0.8250000000000001
		}]
	},
	"840074": {
		"nik": "840074",
		"nama": "WAHYU ARIS EKO WICAKSONO",
		"band": "III",
		"posisi": "MGR OSS ASSURANCE DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8803921568627461,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9717891610987369,
			"nilai_komparatif_unit": 0.9769388046800475,
			"summary_team": 0.8555555555555556
		}]
	},
	"840077": {
		"nik": "840077",
		"nama": "IKA NUGRAHANTI BUDHYSULISTYANI",
		"band": "III",
		"posisi": "SO CX DESIGN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.952380952380952,
			"nilai_komparatif_unit": 0.9574277492117027,
			"summary_team": 0.857142857142857
		}]
	},
	"840099": {
		"nik": "840099",
		"nama": "SUGENG YUNIANTO",
		"band": "III",
		"posisi": "MGR CUSTOMER MGT SYSTEM OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8594202898550712,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0035834738617215,
			"nilai_komparatif_unit": 1.0089015998517645,
			"summary_team": 0.8625
		}]
	},
	"840100": {
		"nik": "840100",
		"nama": "SAYFUL HAKAM",
		"band": "III",
		"posisi": "MGR CA & COHERENT IT ENABLEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8717948717948707,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0036764705882366,
			"nilai_komparatif_unit": 1.0089950893806436,
			"summary_team": 0.8750000000000001
		}]
	},
	"840101": {
		"nik": "840101",
		"nama": "MUHAMMAD RAKHMADANI MY",
		"band": "III",
		"posisi": "MGR MEDIATION SYSTEM DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8803921568627461,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9796770601336292,
			"nilai_komparatif_unit": 0.9848685027699828,
			"summary_team": 0.8625
		}]
	},
	"840103": {
		"nik": "840103",
		"nama": "WIDHIANTANTRI SARASWATI",
		"band": "III",
		"posisi": "MGR INTERNAL CAPABILITY ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8833333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9589345172031083,
			"nilai_komparatif_unit": 0.9640160422695423,
			"summary_team": 0.8470588235294116
		}]
	},
	"840104": {
		"nik": "840104",
		"nama": "KHRISNA DINI YUNITA SARI",
		"band": "III",
		"posisi": "MGR PROCUREMENT II",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8695035460992895,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9967373572593816,
			"nilai_komparatif_unit": 1.0020192047418746,
			"summary_team": 0.8666666666666667
		}]
	},
	"840130": {
		"nik": "840130",
		"nama": "ASEP ROFI HERMAWAN",
		"band": "III",
		"posisi": "MGR INET PROD INFRA INTEGRATION&READINES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857153,
			"nilai_komparatif_unit": 0.9693955960768504,
			"summary_team": 0.825
		}]
	},
	"840135": {
		"nik": "840135",
		"nama": "TOUFIK GOZALI",
		"band": "III",
		"posisi": "MGR INFRAST & SERVICE NODE INTEGRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8711111111111115,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9858943577430969,
			"nilai_komparatif_unit": 0.9911187466892154,
			"summary_team": 0.8588235294117648
		}]
	},
	"840161": {
		"nik": "840161",
		"nama": "WAHYUDI",
		"band": "III",
		"posisi": "MGR BIG DATA PLATFORM&AI CAPABILITY DEVT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8694444444444439,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.920127795527157,
			"nilai_komparatif_unit": 0.9250036784716267,
			"summary_team": 0.7999999999999998
		}]
	},
	"840173": {
		"nik": "840173",
		"nama": "MOFREN HASIHOLAN DAMANIK",
		"band": "III",
		"posisi": "MGR DATA COMMUNICATION NW & VAS OPR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666656,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0512820512820524,
			"nilai_komparatif_unit": 1.0568529385529195,
			"summary_team": 0.911111111111111
		}]
	},
	"850037": {
		"nik": "850037",
		"nama": "HENDY IRAWAN S.T, MSC.",
		"band": "III",
		"posisi": "MGR WIRELESS & OLO PROJECT REPORT & ADM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8583333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.077669902912622,
			"nilai_komparatif_unit": 1.0833806230157677,
			"summary_team": 0.9249999999999998
		}]
	},
	"850074": {
		"nik": "850074",
		"nama": "JANUAR CIPTA LESMANA",
		"band": "III",
		"posisi": "MGR IP BACKBONE PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555543,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9788961038961053,
			"nilai_komparatif_unit": 0.9840834081386212,
			"summary_team": 0.8374999999999999
		}]
	},
	"850077": {
		"nik": "850077",
		"nama": "DAVID GUNAWAN",
		"band": "III",
		"posisi": "SO NEW TECHNOLOGY ACQUISITION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8541666666666652,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9512195121951236,
			"nilai_komparatif_unit": 0.9562601543955929,
			"summary_team": 0.8125
		}]
	},
	"850093": {
		"nik": "850093",
		"nama": "DHIAANI ZAHRA",
		"band": "III",
		"posisi": "MGR CONSUMER DIGITALIZATION ALIGNMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666663,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.024038461538462,
			"nilai_komparatif_unit": 1.029464981303834,
			"summary_team": 0.8875
		}]
	},
	"850095": {
		"nik": "850095",
		"nama": "ANDHI RAHMAN",
		"band": "III",
		"posisi": "MGR ANALYTICS PLATFORM OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8589147286821694,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9605144404332142,
			"nilai_komparatif_unit": 0.9656043377287763,
			"summary_team": 0.825
		}]
	},
	"850102": {
		"nik": "850102",
		"nama": "AGUSTINA WULANDARI, M.T",
		"band": "III",
		"posisi": "MGR DATA QUALITY MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8972222222222217,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0448916408668736,
			"nilai_komparatif_unit": 1.0504286644795586,
			"summary_team": 0.9375
		}]
	},
	"850137": {
		"nik": "850137",
		"nama": "HANANTO EDY WIBOWO",
		"band": "III",
		"posisi": "MGR ACCESS OSP PROJECT MANGEMENT AREA 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8733333333333323,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9541984732824437,
			"nilai_komparatif_unit": 0.9592549014048561,
			"summary_team": 0.8333333333333331
		}]
	},
	"850143": {
		"nik": "850143",
		"nama": "RAHADIANY RACHMAWATY",
		"band": "III",
		"posisi": "MGR ORDER & COMPLEX EVENT PROCESSING OPR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8807017543859639,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9935258964143437,
			"nilai_komparatif_unit": 0.9987907259269011,
			"summary_team": 0.875
		}]
	},
	"850152": {
		"nik": "850152",
		"nama": "AHMAD DIKKO ADHA",
		"band": "III",
		"posisi": "MGR OSP ACCESS DESIGN AREA 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.871264367816091,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0186345646438006,
			"nilai_komparatif_unit": 1.024032448420965,
			"summary_team": 0.8875
		}]
	},
	"850165": {
		"nik": "850165",
		"nama": "R.EKO PERMONO JATI , M.T.",
		"band": "III",
		"posisi": "MGR SERVICE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.872222222222221,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0385912326714142,
			"nilai_komparatif_unit": 1.0440948695599803,
			"summary_team": 0.9058823529411766
		}]
	},
	"860067": {
		"nik": "860067",
		"nama": "NENI ADININGSIH",
		"band": "III",
		"posisi": "MGR CEM DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8717948717948707,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0180147058823543,
			"nilai_komparatif_unit": 1.0234093049432242,
			"summary_team": 0.8875000000000001
		}]
	},
	"860085": {
		"nik": "860085",
		"nama": "FERDI GANDA KURNIA",
		"band": "III",
		"posisi": "MGR INTERNET PRODUCT DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9788961038961048,
			"nilai_komparatif_unit": 0.9840834081386207,
			"summary_team": 0.8374999999999999
		}]
	},
	"860096": {
		"nik": "860096",
		"nama": "FERRY AFIT KURNIAWAN",
		"band": "III",
		"posisi": "MGR VULNERABILITY ASSESSMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8361111111111127,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9867109634551475,
			"nilai_komparatif_unit": 0.9919396797065416,
			"summary_team": 0.825
		}]
	},
	"860104": {
		"nik": "860104",
		"nama": "CITA NURANI LESTARI",
		"band": "III",
		"posisi": "SO DIGITAL BUSINESS GOVERNANCE CONTROL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8444444444444438,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9891640866873072,
			"nilai_komparatif_unit": 0.9944058023739824,
			"summary_team": 0.8352941176470587
		}]
	},
	"860107": {
		"nik": "860107",
		"nama": "SANG PUTU EKA KESUMA PUTRA, M.T.",
		"band": "III",
		"posisi": "MGR PAYMENT & SETTLEMENT OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8594202898550712,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.930860033726814,
			"nilai_komparatif_unit": 0.9357927882683033,
			"summary_team": 0.7999999999999999
		}]
	},
	"860111": {
		"nik": "860111",
		"nama": "ALFEUS CHRISTANTYAS WISMANA",
		"band": "III",
		"posisi": "MGR COMM&CONVERGE PROD INFRA INTEGRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.866666666666666,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.011834319526628,
			"nilai_komparatif_unit": 1.0171961678755113,
			"summary_team": 0.8769230769230769
		}]
	},
	"860130": {
		"nik": "860130",
		"nama": "RIZKY DESTRIANA",
		"band": "III",
		"posisi": "MGR ENT COMM & DATACOMM INTERFACE MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8705882352941167,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9719334719334729,
			"nilai_komparatif_unit": 0.97708388023762,
			"summary_team": 0.846153846153846
		}]
	},
	"860133": {
		"nik": "860133",
		"nama": "IRVAN SETIYANTO",
		"band": "III",
		"posisi": "MGR CA & COHERENT IT ENABLEMENT OPR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8594202898550712,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.98903878583474,
			"nilai_komparatif_unit": 0.9942798375350723,
			"summary_team": 0.85
		}]
	},
	"860140": {
		"nik": "860140",
		"nama": "PRIBADI TANGGUH PRAMUDYA",
		"band": "III",
		"posisi": "SO CX ENTERPRISE DIGITIZATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8176100628930801,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9784615384615402,
			"nilai_komparatif_unit": 0.9836465398824253,
			"summary_team": 0.7999999999999998
		}]
	},
	"860146": {
		"nik": "860146",
		"nama": "ANDI HAKIM KUSUMA",
		"band": "III",
		"posisi": "SO DIGITAL BUSINESS STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8472222222222214,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9581485053037617,
			"nilai_komparatif_unit": 0.9632258651857151,
			"summary_team": 0.8117647058823529
		}]
	},
	"860161": {
		"nik": "860161",
		"nama": "ARIS SOFAN LUTFIANTO",
		"band": "III",
		"posisi": "SO DIG BUSINESS PERFORMANCE MEASUREMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8633333333333331,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9928295642581358,
			"nilai_komparatif_unit": 0.9980907038114281,
			"summary_team": 0.857142857142857
		}]
	},
	"870002": {
		"nik": "870002",
		"nama": "AKHMAD ARYANDI",
		"band": "III",
		"posisi": "MGR SCANNING, MONITORING & THREAT INTELL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8361111111111127,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0016611295681044,
			"nilai_komparatif_unit": 1.0069690687930044,
			"summary_team": 0.8374999999999999
		}]
	},
	"870015": {
		"nik": "870015",
		"nama": "BAYU DHARMAWAN",
		"band": "III",
		"posisi": "MGR MOBILITY & FMC RESEARCH",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8541666666666652,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9209756097560992,
			"nilai_komparatif_unit": 0.9258559853840407,
			"summary_team": 0.7866666666666666
		}]
	},
	"870024": {
		"nik": "870024",
		"nama": "YUDHI SURYANTO",
		"band": "III",
		"posisi": "MGR DATA CENTER & CME OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8681481481481468,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9598976109215032,
			"nilai_komparatif_unit": 0.9649842395531791,
			"summary_team": 0.8333333333333333
		}]
	},
	"870033": {
		"nik": "870033",
		"nama": "ALI MURTADO FAUZARROHMAN",
		"band": "III",
		"posisi": "MGR PROCESS ORCHESTRATION & INTEGRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8803921568627461,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.022271714922048,
			"nilai_komparatif_unit": 1.0276888724556343,
			"summary_team": 0.9
		}]
	},
	"870054": {
		"nik": "870054",
		"nama": "FIKRIAN HADI",
		"band": "III",
		"posisi": "MGR DIGITAL LIFESTYLE ALIGNMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666663,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.024038461538462,
			"nilai_komparatif_unit": 1.029464981303834,
			"summary_team": 0.8875
		}]
	},
	"870064": {
		"nik": "870064",
		"nama": "ALINE ZIVANA",
		"band": "III",
		"posisi": "MGR OPEN INNOVATION INCUBATION MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8608695652173906,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.960269360269361,
			"nilai_komparatif_unit": 0.9653579588516392,
			"summary_team": 0.8266666666666667
		}]
	},
	"880007": {
		"nik": "880007",
		"nama": "LUNEL CANDRA",
		"band": "III",
		"posisi": "MGR PERFORMANCE & QUALITY MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272738,
			"nilai_komparatif_unit": 1.0281468443239323,
			"summary_team": 0.875
		}]
	},
	"880021": {
		"nik": "880021",
		"nama": "IXORA ADISTI",
		"band": "III",
		"posisi": "MGR BUSINESS PLANNING & PERFORMANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8833333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0122086570477251,
			"nilai_komparatif_unit": 1.0175724890622944,
			"summary_team": 0.8941176470588237
		}]
	},
	"880028": {
		"nik": "880028",
		"nama": "SUSATYA TRI SETIAWAN",
		"band": "III",
		"posisi": "MGR SUBMARINE PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8745098039215675,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9275037369207785,
			"nilai_komparatif_unit": 0.9324187059867798,
			"summary_team": 0.8111111111111111
		}]
	},
	"880038": {
		"nik": "880038",
		"nama": "ILHAM ANANTO YUWONO",
		"band": "III",
		"posisi": "SENIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.829743589743589,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96415327564895,
			"nilai_komparatif_unit": 0.9692624556296482,
			"summary_team": 0.7999999999999998
		}]
	},
	"880046": {
		"nik": "880046",
		"nama": "ADI NUGROHO",
		"band": "III",
		"posisi": "MGR NMS TRANSPORT,SERV NODE & CME INFRAS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.882962962962962,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9563758389261756,
			"nilai_komparatif_unit": 0.9614438052067198,
			"summary_team": 0.8444444444444444
		}]
	},
	"880065": {
		"nik": "880065",
		"nama": "IBNU MAS'UD",
		"band": "III",
		"posisi": "MGR OPEN INNOVATION PLANNING & DISCOVERY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8608695652173906,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9292929292929302,
			"nilai_komparatif_unit": 0.9342173795338445,
			"summary_team": 0.8
		}]
	},
	"890020": {
		"nik": "890020",
		"nama": "RAGIL WIDIHARSO",
		"band": "III",
		"posisi": "MGR START UP COMMUNITY & ENGAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8608695652173906,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9292929292929302,
			"nilai_komparatif_unit": 0.9342173795338445,
			"summary_team": 0.8
		}]
	}
};