export const cbark8 = {
	"650108": {
		"nik": "650108",
		"nama": "ERMALADIANI",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8392857142857179,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0127659574468042,
			"nilai_komparatif_unit": 1.0181327426723557,
			"summary_team": 0.85
		}]
	},
	"650113": {
		"nik": "650113",
		"nama": "MATTOMI, ST",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8637037037037024,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9551886792452845,
			"nilai_komparatif_unit": 0.9602503546044278,
			"summary_team": 0.8250000000000001
		}]
	},
	"650122": {
		"nik": "650122",
		"nama": "DADANG SAMAS",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8628205128205122,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9996285289747407,
			"nilai_komparatif_unit": 1.0049256971712963,
			"summary_team": 0.8625
		}]
	},
	"650413": {
		"nik": "650413",
		"nama": "BESLI",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8405797101449296,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.026077586206894,
			"nilai_komparatif_unit": 1.031514911572576,
			"summary_team": 0.8624999999999999
		}]
	},
	"650489": {
		"nik": "650489",
		"nama": "DANY RACHMAN",
		"band": "III",
		"posisi": "MGR ACCESS CAPEX QE & PERFORMANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.851851851851851,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9391304347826095,
			"nilai_komparatif_unit": 0.944107015309628,
			"summary_team": 0.7999999999999998
		}]
	},
	"650556": {
		"nik": "650556",
		"nama": "SUDARSANA",
		"band": "III",
		"posisi": "MGR CUSTOMER RELATIONSHIP MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8242424242424247,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9250919117647054,
			"nilai_komparatif_unit": 0.929994100239575,
			"summary_team": 0.7625
		}]
	},
	"650687": {
		"nik": "650687",
		"nama": "EDI SYAIFUL",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8406779661016985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9516129032258024,
			"nilai_komparatif_unit": 0.9566556300591089,
			"summary_team": 0.8
		}]
	},
	"650860": {
		"nik": "650860",
		"nama": "SUGIHARTO",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8476190476190508,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9733146067415694,
			"nilai_komparatif_unit": 0.9784723338678275,
			"summary_team": 0.8250000000000001
		}]
	},
	"651079": {
		"nik": "651079",
		"nama": "AJAT SUDRAJAT",
		"band": "III",
		"posisi": "KAKANDATEL SINGAPARNA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8628205128205122,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9806835066864792,
			"nilai_komparatif_unit": 0.9858802826206697,
			"summary_team": 0.8461538461538461
		}]
	},
	"651185": {
		"nik": "651185",
		"nama": "DRS. I NYOMAN SUGITA, MM.",
		"band": "III",
		"posisi": "MGR COMMUNITY DEVELOPMENT CENTER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8691358024691354,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9492187500000006,
			"nilai_komparatif_unit": 0.9542487898881492,
			"summary_team": 0.8250000000000001
		}]
	},
	"651341": {
		"nik": "651341",
		"nama": "JOVINUS ERIXON TAMBUNAN",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8405797101449296,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9517241379310322,
			"nilai_komparatif_unit": 0.9567674542122444,
			"summary_team": 0.8
		}]
	},
	"660047": {
		"nik": "660047",
		"nama": "MULIA HARTONO SE",
		"band": "III",
		"posisi": "MGR CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333329,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9600000000000004,
			"nilai_komparatif_unit": 0.9650871712053971,
			"summary_team": 0.7999999999999999
		}]
	},
	"660048": {
		"nik": "660048",
		"nama": "MEDI ELMANSYAH",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8405797101449296,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0382445141065806,
			"nilai_komparatif_unit": 1.043746313686085,
			"summary_team": 0.8727272727272728
		}]
	},
	"660088": {
		"nik": "660088",
		"nama": "PURWITO",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8631578947368416,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.970274390243903,
			"nilai_komparatif_unit": 0.9754160068474267,
			"summary_team": 0.8375
		}]
	},
	"660094": {
		"nik": "660094",
		"nama": "SOPIYAN SOPIYANA",
		"band": "III",
		"posisi": "MGR PROJECT DELIVERY & QUALITY ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8421052631578936,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0390625000000013,
			"nilai_komparatif_unit": 1.044568634198551,
			"summary_team": 0.8749999999999999
		}]
	},
	"660095": {
		"nik": "660095",
		"nama": "IRAWAN",
		"band": "III",
		"posisi": "MGR ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8836879432624103,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8449438202247201,
			"nilai_komparatif_unit": 0.8494212930084963,
			"summary_team": 0.7466666666666667
		}]
	},
	"660101": {
		"nik": "660101",
		"nama": "VICTOR ADRIANI",
		"band": "III",
		"posisi": "MGR DELIVERY&SALES SUPPORT BUSINESS SEV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8853333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0306852409638563,
			"nilai_komparatif_unit": 1.0361469829218342,
			"summary_team": 0.9125000000000001
		}]
	},
	"660111": {
		"nik": "660111",
		"nama": "UTUY KUSWANDA",
		"band": "III",
		"posisi": "KAKANDATEL MAJALENGKA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111126,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.97548387096774,
			"nilai_komparatif_unit": 0.9806530933216109,
			"summary_team": 0.8399999999999999
		}]
	},
	"660126": {
		"nik": "660126",
		"nama": "AGUS WIDODO",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8476190476190508,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9733146067415694,
			"nilai_komparatif_unit": 0.9784723338678275,
			"summary_team": 0.8250000000000001
		}]
	},
	"660130": {
		"nik": "660130",
		"nama": "PAHKRUDDIN SIHALOHO",
		"band": "III",
		"posisi": "MGR SHARED SERVICE HC & FINANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8567049808429145,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.058318425760283,
			"nilai_komparatif_unit": 1.0639265997411875,
			"summary_team": 0.9066666666666666
		}]
	},
	"660132": {
		"nik": "660132",
		"nama": "DEWI GUSWIARTI RATMALASARI",
		"band": "III",
		"posisi": "MGR BUSINESS SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7925925925925928,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9978759558198809,
			"nilai_komparatif_unit": 1.0031638368917606,
			"summary_team": 0.790909090909091
		}]
	},
	"660143": {
		"nik": "660143",
		"nama": "ROSRITA HUTABARAT",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379084967320288,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9995124804992168,
			"nilai_komparatif_unit": 1.00480903373904,
			"summary_team": 0.8375
		}]
	},
	"660158": {
		"nik": "660158",
		"nama": "AGUS USMAN",
		"band": "III",
		"posisi": "MGR SHARED SERVICE HC & FINANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8319727891156488,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9916189697465219,
			"nilai_komparatif_unit": 0.9968736941940424,
			"summary_team": 0.825
		}]
	},
	"660185": {
		"nik": "660185",
		"nama": "DENNY KUSDINAR",
		"band": "III",
		"posisi": "MGR SECURITY & SAFETY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8836879432624103,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9749351771823694,
			"nilai_komparatif_unit": 0.9801014919328804,
			"summary_team": 0.8615384615384616
		}]
	},
	"660188": {
		"nik": "660188",
		"nama": "HOMBAR SITUMEANG",
		"band": "III",
		"posisi": "MGR APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8416666666666656,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9900990099009913,
			"nilai_komparatif_unit": 0.9953456798735539,
			"summary_team": 0.8333333333333333
		}]
	},
	"660201": {
		"nik": "660201",
		"nama": "BUNGARAN B. SIMANJUNTAK",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481503,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9432314410480326,
			"nilai_komparatif_unit": 0.9482297533677455,
			"summary_team": 0.8
		}]
	},
	"660216": {
		"nik": "660216",
		"nama": "SAPTA PRAYITNA",
		"band": "III",
		"posisi": "KABAG EQUITY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9066666666666667,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0661764705882355,
			"nilai_komparatif_unit": 1.0718262854226606,
			"summary_team": 0.9666666666666669
		}]
	},
	"660224": {
		"nik": "660224",
		"nama": "MAMAY  M. JUMARDI",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8567049808429145,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0427549194991024,
			"nilai_komparatif_unit": 1.048280620333229,
			"summary_team": 0.8933333333333333
		}]
	},
	"660248": {
		"nik": "660248",
		"nama": "Hesti Dinaryanti, Dra.",
		"band": "III",
		"posisi": "MGR ANGGARAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8694444444444438,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9968051118210869,
			"nilai_komparatif_unit": 1.0020873183442625,
			"summary_team": 0.8666666666666666
		}]
	},
	"660260": {
		"nik": "660260",
		"nama": "BUDI SETIANI",
		"band": "III",
		"posisi": "KABAG AKUNTANSI & ANALISA LAP KEUANGAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8444444444444447,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0105263157894735,
			"nilai_komparatif_unit": 1.0158812328477858,
			"summary_team": 0.8533333333333334
		}]
	},
	"660268": {
		"nik": "660268",
		"nama": "YUNARDO SIANIPAR",
		"band": "III",
		"posisi": "MGR BUDGETING MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7866666666666667,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0381355932203389,
			"nilai_komparatif_unit": 1.0436368156131806,
			"summary_team": 0.8166666666666667
		}]
	},
	"660277": {
		"nik": "660277",
		"nama": "R.ENDRAYANA",
		"band": "III",
		"posisi": "SENIOR AM GOVERNMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8853333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0649741824440626,
			"nilai_komparatif_unit": 1.0706176261892921,
			"summary_team": 0.9428571428571426
		}]
	},
	"660283": {
		"nik": "660283",
		"nama": "YANTI LESTARI, MM",
		"band": "III",
		"posisi": "MGR CORPORATE COMMUNICATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9308176100628933,
			"nilai_komparatif_unit": 0.9357501397955893,
			"summary_team": 0.8222222222222224
		}]
	},
	"660286": {
		"nik": "660286",
		"nama": "WAHYU HIDAYAT",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481503,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0680708964808605,
			"nilai_komparatif_unit": 1.073730750137006,
			"summary_team": 0.9058823529411766
		}]
	},
	"660313": {
		"nik": "660313",
		"nama": "COSMAS SARAGI TURNIP, ST",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8405797101449296,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.015172413793101,
			"nilai_komparatif_unit": 1.0205519511597274,
			"summary_team": 0.8533333333333334
		}]
	},
	"660321": {
		"nik": "660321",
		"nama": "HENDRIESJAF ARIEF",
		"band": "III",
		"posisi": "MGR ACCESS OM & PARTNERSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.851851851851851,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9391304347826095,
			"nilai_komparatif_unit": 0.944107015309628,
			"summary_team": 0.7999999999999998
		}]
	},
	"660343": {
		"nik": "660343",
		"nama": "AGUS SUBROTO, IR",
		"band": "III",
		"posisi": "MGR NTE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9277777777777774,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9835329341317369,
			"nilai_komparatif_unit": 0.9887448095713977,
			"summary_team": 0.9125
		}]
	},
	"660356": {
		"nik": "660356",
		"nama": "HERU YULIANTO",
		"band": "III",
		"posisi": "MGR INFRA SERVICE DELIVERY & OPR SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8552845528455273,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9520640956002184,
			"nilai_komparatif_unit": 0.9571092133635825,
			"summary_team": 0.8142857142857142
		}]
	},
	"660357": {
		"nik": "660357",
		"nama": "AZRIL AZIZ",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379084967320288,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9547581903276101,
			"nilai_komparatif_unit": 0.9598175844671427,
			"summary_team": 0.8
		}]
	},
	"660380": {
		"nik": "660380",
		"nama": "RICARDO PANGGABEAN",
		"band": "III",
		"posisi": "MGR IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8746666666666663,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9432164634146346,
			"nilai_komparatif_unit": 0.9482146963658211,
			"summary_team": 0.8250000000000001
		}]
	},
	"660381": {
		"nik": "660381",
		"nama": "SINGGIH WIDODO",
		"band": "III",
		"posisi": "MGR WHOLESALE SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8345679012345671,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.029063696484512,
			"nilai_komparatif_unit": 1.0345168456566736,
			"summary_team": 0.8588235294117648
		}]
	},
	"660386": {
		"nik": "660386",
		"nama": "EKO DANU SUKSMONO",
		"band": "III",
		"posisi": "MGR PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8304347826086982,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0536649214659655,
			"nilai_komparatif_unit": 1.0592484358916094,
			"summary_team": 0.875
		}]
	},
	"660390": {
		"nik": "660390",
		"nama": "POERTJAHJA DJATMIKA",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8836879432624103,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9807383627608359,
			"nilai_komparatif_unit": 0.9859354293848618,
			"summary_team": 0.8666666666666667
		}]
	},
	"660396": {
		"nik": "660396",
		"nama": "ASEP TATANG",
		"band": "III",
		"posisi": "MGR PLAN & BUDGET CONTROL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8691358024691354,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0642755681818188,
			"nilai_komparatif_unit": 1.0699153098745915,
			"summary_team": 0.925
		}]
	},
	"660416": {
		"nik": "660416",
		"nama": "MAROLOP SIMANJUNTAK",
		"band": "III",
		"posisi": "MGR PROJECT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8853333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.960090361445784,
			"nilai_komparatif_unit": 0.9651780114888318,
			"summary_team": 0.8500000000000001
		}]
	},
	"660426": {
		"nik": "660426",
		"nama": "RIKANDAR SOLIHAT, IR",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8392857142857179,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9680851063829746,
			"nilai_komparatif_unit": 0.9732151216721047,
			"summary_team": 0.8125
		}]
	},
	"660435": {
		"nik": "660435",
		"nama": "SAMSUL ANWAR TARIGAN",
		"band": "III",
		"posisi": "KAKANDATEL KABANJAHE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8405797101449296,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9649425287356298,
			"nilai_komparatif_unit": 0.9700558910763033,
			"summary_team": 0.8111111111111111
		}]
	},
	"660436": {
		"nik": "660436",
		"nama": "DASE",
		"band": "III",
		"posisi": "MGR SERVICE DELIVERY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8552845528455273,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0084363117870736,
			"nilai_komparatif_unit": 1.0137801536285316,
			"summary_team": 0.8625
		}]
	},
	"660439": {
		"nik": "660439",
		"nama": "SIMSON HARRY GM TAMPUBOLON",
		"band": "III",
		"posisi": "MGR SAS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8406779661016985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.002592165898613,
			"nilai_komparatif_unit": 1.0079050388122752,
			"summary_team": 0.8428571428571427
		}]
	},
	"660444": {
		"nik": "660444",
		"nama": "NANAN WIRYANA, IR, MT",
		"band": "III",
		"posisi": "KAKANDATEL SINDANGLAYA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8476190476190508,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0382022471910073,
			"nilai_komparatif_unit": 1.0437038227923492,
			"summary_team": 0.88
		}]
	},
	"660461": {
		"nik": "660461",
		"nama": "SUNHADI",
		"band": "III",
		"posisi": "KAKANDATEL DUMAI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379084967320288,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9680187207488269,
			"nilai_komparatif_unit": 0.9731483842514086,
			"summary_team": 0.8111111111111111
		}]
	},
	"660464": {
		"nik": "660464",
		"nama": "KURNAENI",
		"band": "III",
		"posisi": "MGR CCAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8392857142857179,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9382978723404215,
			"nilai_komparatif_unit": 0.9432700410052707,
			"summary_team": 0.7875
		}]
	},
	"660526": {
		"nik": "660526",
		"nama": "Ade Rafiani",
		"band": "III",
		"posisi": "KEPALA YAKES REGIONAL IV JATENG & DIY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8914285714285706,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.017094017094018,
			"nilai_komparatif_unit": 1.0224837372991658,
			"summary_team": 0.9066666666666666
		}]
	},
	"660604": {
		"nik": "660604",
		"nama": "ALOYSIUS SUBARKAH NITISASTRO",
		"band": "III",
		"posisi": "MGR GOVERNMENT SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111126,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9870967741935467,
			"nilai_komparatif_unit": 0.9923275349087731,
			"summary_team": 0.85
		}]
	},
	"670015": {
		"nik": "670015",
		"nama": "SYAMSURI",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481503,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9432314410480326,
			"nilai_komparatif_unit": 0.9482297533677455,
			"summary_team": 0.8
		}]
	},
	"670016": {
		"nik": "670016",
		"nama": "DANA SANTOSA",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111126,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.103225806451611,
			"nilai_komparatif_unit": 1.1090719507803934,
			"summary_team": 0.95
		}]
	},
	"670024": {
		"nik": "670024",
		"nama": "KUSTANTO",
		"band": "III",
		"posisi": "MGR SHARED SERVICE HC & FINANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111126,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9754838709677404,
			"nilai_komparatif_unit": 0.9806530933216112,
			"summary_team": 0.8400000000000001
		}]
	},
	"670031": {
		"nik": "670031",
		"nama": "BINSAR SIMANJUNTAK",
		"band": "III",
		"posisi": "KAKANDATEL BANGKINANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379084967320288,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9414976599063933,
			"nilai_komparatif_unit": 0.9464867846828768,
			"summary_team": 0.788888888888889
		}]
	},
	"670033": {
		"nik": "670033",
		"nama": "JACOBUS MELVIN N. P.",
		"band": "III",
		"posisi": "KAKANDATEL LUBUK PAKAM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8406779661016985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9754032258064478,
			"nilai_komparatif_unit": 0.980572020810587,
			"summary_team": 0.8200000000000003
		}]
	},
	"670034": {
		"nik": "670034",
		"nama": "GANI MAY SAFARIE, SE.",
		"band": "III",
		"posisi": "MGR WIRELESS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.851851851851851,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0081841432225072,
			"nilai_komparatif_unit": 1.013526648788277,
			"summary_team": 0.8588235294117645
		}]
	},
	"670065": {
		"nik": "670065",
		"nama": "KATRIEN HENDRIJANTI, SE",
		"band": "III",
		"posisi": "MGR FINANCING & CASH PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9733333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9641728134878821,
			"nilai_komparatif_unit": 0.9692820970022591,
			"summary_team": 0.9384615384615385
		}]
	},
	"670070": {
		"nik": "670070",
		"nama": "FERRI YUDISTIRA S.A.",
		"band": "III",
		"posisi": "MGR FINANCIAL SERVICE & TAX",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8691358024691354,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0642755681818186,
			"nilai_komparatif_unit": 1.0699153098745913,
			"summary_team": 0.9249999999999999
		}]
	},
	"670073": {
		"nik": "670073",
		"nama": "R. Judy Darmadie",
		"band": "III",
		"posisi": "PENGURUS BENDAHARA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8800000000000001,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.013986013986014,
			"nilai_komparatif_unit": 1.0193592644579146,
			"summary_team": 0.8923076923076924
		}]
	},
	"670081": {
		"nik": "670081",
		"nama": "YUSWARDI",
		"band": "III",
		"posisi": "MGR PERF MGT,SLA MONITORING & KPI REPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999999,
			"nilai_komparatif_unit": 0.9650871712053966,
			"summary_team": 0.8
		}]
	},
	"670089": {
		"nik": "670089",
		"nama": "ASEP TARYAMAN",
		"band": "III",
		"posisi": "MGR ASSET MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8293333333333328,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9525723472668817,
			"nilai_komparatif_unit": 0.9576201583252913,
			"summary_team": 0.79
		}]
	},
	"670099": {
		"nik": "670099",
		"nama": "SUYANTO",
		"band": "III",
		"posisi": "MGR IP & SERVICE NODE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.851851851851851,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9391304347826097,
			"nilai_komparatif_unit": 0.9441070153096283,
			"summary_team": 0.8
		}]
	},
	"670105": {
		"nik": "670105",
		"nama": "MUNTALIP, ST",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8392857142857179,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.032624113475173,
			"nilai_komparatif_unit": 1.0380961297835785,
			"summary_team": 0.8666666666666667
		}]
	},
	"670108": {
		"nik": "670108",
		"nama": "JONGGI SIREGAR",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8631578947368416,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0298102981029815,
			"nilai_komparatif_unit": 1.0352674036191591,
			"summary_team": 0.8888888888888888
		}]
	},
	"670114": {
		"nik": "670114",
		"nama": "ERWIN ULONG NAINGGOLAN M.T",
		"band": "III",
		"posisi": "SENIOR AM BUSINESS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8853333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0165662650602416,
			"nilai_komparatif_unit": 1.0219531886352335,
			"summary_team": 0.8999999999999999
		}]
	},
	"670120": {
		"nik": "670120",
		"nama": "BELLY GUSTI HARAHAP",
		"band": "III",
		"posisi": "MGR ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481503,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.008733624454146,
			"nilai_komparatif_unit": 1.0140790417960612,
			"summary_team": 0.8555555555555556
		}]
	},
	"670123": {
		"nik": "670123",
		"nama": "ASEP LESMANA",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8567049808429145,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.021355098389979,
			"nilai_komparatif_unit": 1.026767398647286,
			"summary_team": 0.875
		}]
	},
	"670124": {
		"nik": "670124",
		"nama": "AGUNG SETIYONO",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111126,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9870967741935467,
			"nilai_komparatif_unit": 0.9923275349087731,
			"summary_team": 0.85
		}]
	},
	"670131": {
		"nik": "670131",
		"nama": "TUSLI KOMARA DJAJA, RADEN, MBA",
		"band": "III",
		"posisi": "MGR PLANNING, ENGINEERING & CAPEX MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8444444444444437,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9473684210526325,
			"nilai_komparatif_unit": 0.9523886557948004,
			"summary_team": 0.8
		}]
	},
	"670149": {
		"nik": "670149",
		"nama": "SONDANG MERIATI PANJAITAN, IR",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8406779661016985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9912634408602108,
			"nilai_komparatif_unit": 0.9965162813115718,
			"summary_team": 0.8333333333333334
		}]
	},
	"670151": {
		"nik": "670151",
		"nama": "SATRIA ERWANTO, IR",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8406779661016985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9516129032258024,
			"nilai_komparatif_unit": 0.9566556300591089,
			"summary_team": 0.8
		}]
	},
	"670152": {
		"nik": "670152",
		"nama": "MUKHAMMAD IRFANI",
		"band": "III",
		"posisi": "MGR WIFI FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8552845528455273,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0355784899511147,
			"nilai_komparatif_unit": 1.0410661619042476,
			"summary_team": 0.8857142857142855
		}]
	},
	"670171": {
		"nik": "670171",
		"nama": "SILVIA WANNENDA ROZA",
		"band": "III",
		"posisi": "MGR COLLECTION PARTNERSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8466666666666655,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9448818897637808,
			"nilai_komparatif_unit": 0.9498889480368091,
			"summary_team": 0.7999999999999999
		}]
	},
	"670172": {
		"nik": "670172",
		"nama": "DODI LOSADA",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8836879432624103,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0109149277688616,
			"nilai_komparatif_unit": 1.016271904135165,
			"summary_team": 0.8933333333333333
		}]
	},
	"670173": {
		"nik": "670173",
		"nama": "YUDI MULYADI",
		"band": "III",
		"posisi": "KAKANDATEL PADALARANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8319727891156488,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0417007358953363,
			"nilai_komparatif_unit": 1.0472208504664688,
			"summary_team": 0.8666666666666667
		}]
	},
	"670194": {
		"nik": "670194",
		"nama": "JOHANNES SINABANG, S.T",
		"band": "III",
		"posisi": "MGR ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8405797101449296,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9517241379310322,
			"nilai_komparatif_unit": 0.9567674542122444,
			"summary_team": 0.8
		}]
	},
	"670211": {
		"nik": "670211",
		"nama": "NGADI WULYO UTOMO",
		"band": "III",
		"posisi": "MGR PERFORMANCE & WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8691358024691354,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9923650568181824,
			"nilai_komparatif_unit": 0.9976237348830651,
			"summary_team": 0.8625
		}]
	},
	"670213": {
		"nik": "670213",
		"nama": "ADRIAN SAMOSIR",
		"band": "III",
		"posisi": "MGR MARKETING & ACCOUNT MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8853333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1295180722891576,
			"nilai_komparatif_unit": 1.1355035429280373,
			"summary_team": 1
		}]
	},
	"670245": {
		"nik": "670245",
		"nama": "HENDRI,IR",
		"band": "III",
		"posisi": "MGR ENTERPRISE, OLO & WIFI DEPLOYMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8805555555555549,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.951104100946373,
			"nilai_komparatif_unit": 0.9561441315668616,
			"summary_team": 0.8375
		}]
	},
	"670251": {
		"nik": "670251",
		"nama": "IR. ARISAKTI PRIBADI, MT",
		"band": "III",
		"posisi": "MGR NETWORK DEPLOYMENT & PROJECT SUPERVI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8444444444444437,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0030959752321993,
			"nilai_komparatif_unit": 1.008411517900377,
			"summary_team": 0.8470588235294119
		}]
	},
	"670312": {
		"nik": "670312",
		"nama": "FIRDAUS RIZA, SE",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8207407407407394,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9747292418772576,
			"nilai_komparatif_unit": 0.9798944653484412,
			"summary_team": 0.7999999999999998
		}]
	},
	"670313": {
		"nik": "670313",
		"nama": "HENDRIK",
		"band": "III",
		"posisi": "MGR FINANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481503,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9095446038677456,
			"nilai_komparatif_unit": 0.9143644050331831,
			"summary_team": 0.7714285714285714
		}]
	},
	"670314": {
		"nik": "670314",
		"nama": "SUPRIANTO",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8304347826086982,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9633507853403113,
			"nilai_komparatif_unit": 0.9684557128151858,
			"summary_team": 0.8
		}]
	},
	"670323": {
		"nik": "670323",
		"nama": "MOH. IKHSAN KAMIL",
		"band": "III",
		"posisi": "FACILITY MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8148148148148151,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0031620553359681,
			"nilai_komparatif_unit": 1.008477948171647,
			"summary_team": 0.8173913043478261
		}]
	},
	"670343": {
		"nik": "670343",
		"nama": "NASRUL FATAH",
		"band": "III",
		"posisi": "MGR LOGISTIC & PROCUREMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8650793650793634,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0548165137614698,
			"nilai_komparatif_unit": 1.0604061306320784,
			"summary_team": 0.9125
		}]
	},
	"670407": {
		"nik": "670407",
		"nama": "LUQMAN",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.835294117647058,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0774647887323956,
			"nilai_komparatif_unit": 1.0831744219074668,
			"summary_team": 0.9
		}]
	},
	"670455": {
		"nik": "670455",
		"nama": "HAMINUDIN",
		"band": "III",
		"posisi": "MGR SECURITY AND SAFETY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8650793650793634,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9247706422018364,
			"nilai_komparatif_unit": 0.9296711282253837,
			"summary_team": 0.7999999999999998
		}]
	},
	"670512": {
		"nik": "670512",
		"nama": "ASEP TARYO",
		"band": "III",
		"posisi": "KAKANDATEL INDRAMAYU",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111126,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9909677419354822,
			"nilai_komparatif_unit": 0.9962190154378272,
			"summary_team": 0.8533333333333334
		}]
	},
	"680007": {
		"nik": "680007",
		"nama": "Anastasia Muriani S.",
		"band": "III",
		"posisi": "KEPALA YAKES REG II JAKARTA & BANTEN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8914285714285706,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9871794871794881,
			"nilai_komparatif_unit": 0.9924106862021317,
			"summary_team": 0.88
		}]
	},
	"680026": {
		"nik": "680026",
		"nama": "RAMON RISALDO",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8207407407407394,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9594990974729257,
			"nilai_komparatif_unit": 0.9645836143273719,
			"summary_team": 0.7875
		}]
	},
	"680033": {
		"nik": "680033",
		"nama": "ADRIAN NUR",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8419047619047649,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0452488687782768,
			"nilai_komparatif_unit": 1.0507877853904877,
			"summary_team": 0.8800000000000001
		}]
	},
	"680045": {
		"nik": "680045",
		"nama": "MUSLIM PASARIBU",
		"band": "III",
		"posisi": "MGR BACKBONE & CME OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8746666666666663,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9654471544715452,
			"nilai_komparatif_unit": 0.9705631908929617,
			"summary_team": 0.8444444444444444
		}]
	},
	"680053": {
		"nik": "680053",
		"nama": "HAZNAN",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8392857142857179,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0553191489361657,
			"nilai_komparatif_unit": 1.0609114293392614,
			"summary_team": 0.8857142857142857
		}]
	},
	"680071": {
		"nik": "680071",
		"nama": "SUHARIJONO",
		"band": "III",
		"posisi": "MGR GOVERNMENT SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8567049808429145,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0738819320214636,
			"nilai_komparatif_unit": 1.0795725791491464,
			"summary_team": 0.92
		}]
	},
	"680077": {
		"nik": "680077",
		"nama": "ELISTATY BR SINAGA",
		"band": "III",
		"posisi": "MGR CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8916666666666663,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0342679127725862,
			"nilai_komparatif_unit": 1.0397486397981304,
			"summary_team": 0.9222222222222223
		}]
	},
	"680095": {
		"nik": "680095",
		"nama": "ZUL HUSNI",
		"band": "III",
		"posisi": "MGR CUSTOMER DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8916666666666663,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9532710280373836,
			"nilai_komparatif_unit": 0.9583225415006864,
			"summary_team": 0.85
		}]
	},
	"680101": {
		"nik": "680101",
		"nama": "HERI MULIA",
		"band": "III",
		"posisi": "MGR PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8392857142857179,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9978723404255277,
			"nilai_komparatif_unit": 1.0031602023389388,
			"summary_team": 0.8375
		}]
	},
	"680255": {
		"nik": "680255",
		"nama": "NURSYAIFUDDIN",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9733333333333332,
			"nilai_komparatif_unit": 0.9784911596943604,
			"summary_team": 0.8111111111111111
		}]
	},
	"680337": {
		"nik": "680337",
		"nama": "MIDUK SILABAN",
		"band": "III",
		"posisi": "MGR BILLING & PAYMENT COLLECTION (EBIS)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8466666666666655,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9448818897637808,
			"nilai_komparatif_unit": 0.9498889480368091,
			"summary_team": 0.7999999999999999
		}]
	},
	"680369": {
		"nik": "680369",
		"nama": "DWI KARTONO",
		"band": "III",
		"posisi": "MGR ACCESS DATA & INVENTORY MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.851851851851851,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0271739130434794,
			"nilai_komparatif_unit": 1.032617047994906,
			"summary_team": 0.875
		}]
	},
	"680385": {
		"nik": "680385",
		"nama": "KASUM",
		"band": "III",
		"posisi": "MGR OPERATION BGES, WAN & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8476190476190508,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0617977528089848,
			"nilai_komparatif_unit": 1.067424364219448,
			"summary_team": 0.9000000000000001
		}]
	},
	"680428": {
		"nik": "680428",
		"nama": "UJANG RAHMAN",
		"band": "III",
		"posisi": "MGR WHOLESALE SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7939393939393933,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9446564885496191,
			"nilai_komparatif_unit": 0.9496623523908074,
			"summary_team": 0.75
		}]
	},
	"680434": {
		"nik": "680434",
		"nama": "MAMAN SUPARMAN",
		"band": "III",
		"posisi": "MGR PROGRAM & DOCUMENT MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8475490196078479,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9151208226946301,
			"nilai_komparatif_unit": 0.9199701730057458,
			"summary_team": 0.775609756097561
		}]
	},
	"680447": {
		"nik": "680447",
		"nama": "SRI MULYATI",
		"band": "III",
		"posisi": "MGR SEKDIV & PUBLIC RELATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8650793650793634,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.940183486238534,
			"nilai_komparatif_unit": 0.9451656470291404,
			"summary_team": 0.8133333333333335
		}]
	},
	"690017": {
		"nik": "690017",
		"nama": "Ayu Wigati Mustikasari, Ir",
		"band": "III",
		"posisi": "MGR SDM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8583333333333327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0097087378640783,
			"nilai_komparatif_unit": 1.0150593224652238,
			"summary_team": 0.8666666666666667
		}]
	},
	"690023": {
		"nik": "690023",
		"nama": "SURI AGUS, S.T",
		"band": "III",
		"posisi": "MGR HOME SERVICE 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8406779661016985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9159274193548348,
			"nilai_komparatif_unit": 0.9207810439318923,
			"summary_team": 0.77
		}]
	},
	"690025": {
		"nik": "690025",
		"nama": "YUSKARNAINI",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8419047619047649,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.950226244343888,
			"nilai_komparatif_unit": 0.9552616230822615,
			"summary_team": 0.8
		}]
	},
	"690027": {
		"nik": "690027",
		"nama": "SOFYAN NURYANTO",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379084967320288,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9945397815912603,
			"nilai_komparatif_unit": 0.9998099838199401,
			"summary_team": 0.8333333333333333
		}]
	},
	"690029": {
		"nik": "690029",
		"nama": "APRIZAL",
		"band": "III",
		"posisi": "KAKANDATEL BATURAJA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8392857142857179,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9889361702127621,
			"nilai_komparatif_unit": 0.9941766781388889,
			"summary_team": 0.8300000000000003
		}]
	},
	"690055": {
		"nik": "690055",
		"nama": "DEDY DARMAYANTO",
		"band": "III",
		"posisi": "AVP BUDGETING & TREASURY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8400000000000003,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.005299136672288,
			"summary_team": 0.8400000000000001
		}]
	},
	"690061": {
		"nik": "690061",
		"nama": "IWAN",
		"band": "III",
		"posisi": "MGR BGES OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8836879432624103,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0326043338683801,
			"nilai_komparatif_unit": 1.0380762453619459,
			"summary_team": 0.9125
		}]
	},
	"690083": {
		"nik": "690083",
		"nama": "BAGUS WIDODO",
		"band": "III",
		"posisi": "MGR HR ADMIN. & INDUSTRIAL RELATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7277777777777781,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0809160305343506,
			"nilai_komparatif_unit": 1.0866439523114195,
			"summary_team": 0.7866666666666666
		}]
	},
	"690152": {
		"nik": "690152",
		"nama": "ACENG SUPRIATNA",
		"band": "III",
		"posisi": "MGR OPERATION BGES, WAN & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8628205128205122,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8692421991084701,
			"nilai_komparatif_unit": 0.8738484323228662,
			"summary_team": 0.75
		}]
	},
	"690289": {
		"nik": "690289",
		"nama": "Abdul Azis",
		"band": "III",
		"posisi": "KEPALA YAKES REGIONAL VII KEPULAUAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8914285714285706,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9722222222222232,
			"nilai_komparatif_unit": 0.9773741606536146,
			"summary_team": 0.8666666666666667
		}]
	},
	"690295": {
		"nik": "690295",
		"nama": "PEPEN SETIANA",
		"band": "III",
		"posisi": "MGR ACCESS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8476190476190508,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.017556179775277,
			"nilai_komparatif_unit": 1.0229483490436377,
			"summary_team": 0.8625
		}]
	},
	"690300": {
		"nik": "690300",
		"nama": "MAMAN KOSTAMAN",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111126,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9580645161290307,
			"nilai_komparatif_unit": 0.9631414309408681,
			"summary_team": 0.8250000000000001
		}]
	},
	"690390": {
		"nik": "690390",
		"nama": "MARJAN BAHTIAR",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8836879432624103,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0410914927768873,
			"nilai_komparatif_unit": 1.0466083788854685,
			"summary_team": 0.9199999999999999
		}]
	},
	"690405": {
		"nik": "690405",
		"nama": "HENDAWATI SUKAWIDJAJA",
		"band": "III",
		"posisi": "MGR COLLECTION PARTNERSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8691358024691354,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0067471590909096,
			"nilai_komparatif_unit": 1.0120820498813703,
			"summary_team": 0.875
		}]
	},
	"690591": {
		"nik": "690591",
		"nama": "AIDIL FITRIANSYAH",
		"band": "III",
		"posisi": "MGR ACCESS SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111126,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9435483870967726,
			"nilai_komparatif_unit": 0.9485483789569156,
			"summary_team": 0.8125
		}]
	},
	"690600": {
		"nik": "690600",
		"nama": "AGUSTIAWAN,ST",
		"band": "III",
		"posisi": "KAKANDATEL PRABUMULIH",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8392857142857179,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9531914893616982,
			"nilai_komparatif_unit": 0.9582425813386878,
			"summary_team": 0.8
		}]
	},
	"690605": {
		"nik": "690605",
		"nama": "SUSILAWATI",
		"band": "III",
		"posisi": "KABAG HUKUM & KEPATUHAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8266666666666665,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9274193548387099,
			"nilai_komparatif_unit": 0.9323338767525255,
			"summary_team": 0.7666666666666667
		}]
	},
	"690616": {
		"nik": "690616",
		"nama": "RACHMAD AZIZ MUCHAROM",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER (SAM)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7809523809523811,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9987804878048779,
			"nilai_komparatif_unit": 1.0040731621153707,
			"summary_team": 0.78
		}]
	},
	"700005": {
		"nik": "700005",
		"nama": "JUNIAR FIRNANDO SIRAIT",
		"band": "III",
		"posisi": "MGR PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8406779661016985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0044802867383469,
			"nilai_komparatif_unit": 1.0098031650623926,
			"summary_team": 0.8444444444444444
		}]
	},
	"700158": {
		"nik": "700158",
		"nama": "RENI YULFITA",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481503,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9694323144104778,
			"nilai_komparatif_unit": 0.9745694687390717,
			"summary_team": 0.8222222222222222
		}]
	},
	"700165": {
		"nik": "700165",
		"nama": "JONGGI SIMAMORA",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8405797101449296,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.907112068965515,
			"nilai_komparatif_unit": 0.9119189797960454,
			"summary_team": 0.7625
		}]
	},
	"700260": {
		"nik": "700260",
		"nama": "VARIDA DELIANA MANIK",
		"band": "III",
		"posisi": "MGR PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8405797101449296,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9368534482758599,
			"nilai_komparatif_unit": 0.9418179627401783,
			"summary_team": 0.7875000000000001
		}]
	},
	"700265": {
		"nik": "700265",
		"nama": "Sry Indah Dwiberty",
		"band": "III",
		"posisi": "KEPALA YAKES REGIONAL I SUMATERA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8914285714285706,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9423076923076933,
			"nilai_komparatif_unit": 0.9473011095565803,
			"summary_team": 0.8400000000000001
		}]
	},
	"700302": {
		"nik": "700302",
		"nama": "WAGIMAN",
		"band": "III",
		"posisi": "MGR ACCESS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8476190476190508,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0164217804667204,
			"nilai_komparatif_unit": 1.021807938398104,
			"summary_team": 0.8615384615384615
		}]
	},
	"700426": {
		"nik": "700426",
		"nama": "HASRIL EFFENDI",
		"band": "III",
		"posisi": "KAKANDATEL MUARA BUNGO",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.835294117647058,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.942781690140846,
			"nilai_komparatif_unit": 0.9477776191690332,
			"summary_team": 0.7875
		}]
	},
	"700434": {
		"nik": "700434",
		"nama": "YEKTI ANDRIYANI, S.Sos. M.Si.",
		"band": "III",
		"posisi": "MGR BUSINESS SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8615384615384616,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9801587301587301,
			"nilai_komparatif_unit": 0.9853527252303778,
			"summary_team": 0.8444444444444444
		}]
	},
	"700456": {
		"nik": "700456",
		"nama": "ERNI KUSTININGSIH",
		"band": "III",
		"posisi": "MGR SHARED SERVICE PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8319727891156488,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0216680294358105,
			"nilai_komparatif_unit": 1.0270819879574982,
			"summary_team": 0.85
		}]
	},
	"700639": {
		"nik": "700639",
		"nama": "SITI NURHAYATI,RADEN",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8567049808429145,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0449782775364138,
			"nilai_komparatif_unit": 1.0505157602486517,
			"summary_team": 0.8952380952380952
		}]
	},
	"710017": {
		"nik": "710017",
		"nama": "ROSIHAN ANWAR",
		"band": "III",
		"posisi": "KAKANDATEL BELITUNG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8637037037037024,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9551886792452845,
			"nilai_komparatif_unit": 0.9602503546044278,
			"summary_team": 0.8250000000000001
		}]
	},
	"710039": {
		"nik": "710039",
		"nama": "MOH. TSAURI",
		"band": "III",
		"posisi": "MGR BILLING & PAYMENT COLLECTION CONS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8691358024691354,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0355113636363642,
			"nilai_komparatif_unit": 1.040998679877981,
			"summary_team": 0.9
		}]
	},
	"710077": {
		"nik": "710077",
		"nama": "RADEN BUDI PURNOMO",
		"band": "III",
		"posisi": "MGR OPERATION PERFORMANCE&SUPPORT SYSTEM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.92,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9782608695652174,
			"nilai_komparatif_unit": 0.9834448076141951,
			"summary_team": 0.9
		}]
	},
	"710128": {
		"nik": "710128",
		"nama": "ROSFIANA",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8631578947368416,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.901084010840109,
			"nilai_komparatif_unit": 0.9058589781667644,
			"summary_team": 0.7777777777777778
		}]
	},
	"710145": {
		"nik": "710145",
		"nama": "KARYAWANTO S.E.",
		"band": "III",
		"posisi": "MGR PROJECT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8592592592592586,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0037715517241386,
			"nilai_komparatif_unit": 1.0090906743644796,
			"summary_team": 0.8624999999999998
		}]
	},
	"710242": {
		"nik": "710242",
		"nama": "TRIE SETIAWATY",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111126,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9445161290322566,
			"nilai_komparatif_unit": 0.9495212490891791,
			"summary_team": 0.8133333333333335
		}]
	},
	"710315": {
		"nik": "710315",
		"nama": "SARI TEJOWATI",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8476190476190508,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9438202247190978,
			"nilai_komparatif_unit": 0.948821657083954,
			"summary_team": 0.8000000000000002
		}]
	},
	"710394": {
		"nik": "710394",
		"nama": "DENDEN RUKANDA",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8319727891156488,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0096484055600952,
			"nilai_komparatif_unit": 1.0149986704521161,
			"summary_team": 0.8400000000000001
		}]
	},
	"710425": {
		"nik": "710425",
		"nama": "LISNI LIDIYAWATI SIREGAR,",
		"band": "III",
		"posisi": "MGR CHANNEL ACTIVATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8416666666666656,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0099009900990112,
			"nilai_komparatif_unit": 1.015252593471025,
			"summary_team": 0.85
		}]
	},
	"710437": {
		"nik": "710437",
		"nama": "FLORIA SITUMORANG, ST",
		"band": "III",
		"posisi": "MGR PLAN & BUDGET CONTROL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8466666666666655,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0292463442069752,
			"nilai_komparatif_unit": 1.034700461254381,
			"summary_team": 0.8714285714285712
		}]
	},
	"710440": {
		"nik": "710440",
		"nama": "ACENG AJID",
		"band": "III",
		"posisi": "KAKANDATEL GARUT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8628205128205122,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0508172362555728,
			"nilai_komparatif_unit": 1.0563856604080872,
			"summary_team": 0.9066666666666666
		}]
	},
	"710445": {
		"nik": "710445",
		"nama": "Aldevira",
		"band": "III",
		"posisi": "MGR KEPESERTAAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8888888888888893,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9899999999999995,
			"nilai_komparatif_unit": 0.9952461453055649,
			"summary_team": 0.88
		}]
	},
	"710447": {
		"nik": "710447",
		"nama": "BUDDY SETIAWAN,ST, MM",
		"band": "III",
		"posisi": "SM BUSINESS PLANNING, PERF & RISK MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8319727891156488,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.890866240200084,
			"nilai_komparatif_unit": 0.8955870621636318,
			"summary_team": 0.7411764705882353
		}]
	},
	"710448": {
		"nik": "710448",
		"nama": "DADAN SYAMSUL BACHRO",
		"band": "III",
		"posisi": "MGR ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8567049808429145,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.011627906976741,
			"nilai_komparatif_unit": 1.0169886615173118,
			"summary_team": 0.8666666666666667
		}]
	},
	"710453": {
		"nik": "710453",
		"nama": "MARVEN AIRES",
		"band": "III",
		"posisi": "MGR OPERATION BGES, WAN & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8476190476190508,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9438202247190977,
			"nilai_komparatif_unit": 0.9488216570839539,
			"summary_team": 0.8
		}]
	},
	"710477": {
		"nik": "710477",
		"nama": "AZRUL",
		"band": "III",
		"posisi": "OSM REGIONAL OPERATION CENTER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8476190476190508,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9910112359550524,
			"nilai_komparatif_unit": 0.9962627399381515,
			"summary_team": 0.84
		}]
	},
	"710479": {
		"nik": "710479",
		"nama": "MUHAMMAD IRSAN",
		"band": "III",
		"posisi": "MGR CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8916666666666663,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9672897196261687,
			"nilai_komparatif_unit": 0.9724155200521671,
			"summary_team": 0.8625
		}]
	},
	"710499": {
		"nik": "710499",
		"nama": "SETYA WIBAWA",
		"band": "III",
		"posisi": "MGR BUSINESS & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8319727891156488,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9775960752248538,
			"nilai_komparatif_unit": 0.9827764904377629,
			"summary_team": 0.8133333333333332
		}]
	},
	"710528": {
		"nik": "710528",
		"nama": "WIBOWO SIGIT SUDJENDRO",
		"band": "III",
		"posisi": "MGR ENERGY MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.788888888888889,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9971830985915492,
			"nilai_komparatif_unit": 1.0024673081182818,
			"summary_team": 0.7866666666666666
		}]
	},
	"715477": {
		"nik": "715477",
		"nama": "GUNTORO",
		"band": "III",
		"posisi": "SENIOR RESEARCHER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8766666666666664,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7673695126166611,
			"nilai_komparatif_unit": 0.771435908542164,
			"summary_team": 0.6727272727272726
		}]
	},
	"720009": {
		"nik": "720009",
		"nama": "ARIZONA",
		"band": "III",
		"posisi": "MGR SHARED SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8628205128205122,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9628528974739978,
			"nilai_komparatif_unit": 0.9679551865730213,
			"summary_team": 0.8307692307692308
		}]
	},
	"720010": {
		"nik": "720010",
		"nama": "EVI NOVIARNI",
		"band": "III",
		"posisi": "MGR GOVERNMENT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8392857142857179,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0042553191489318,
			"nilai_komparatif_unit": 1.0095770053389745,
			"summary_team": 0.8428571428571427
		}]
	},
	"720100": {
		"nik": "720100",
		"nama": "NGADI, ST",
		"band": "III",
		"posisi": "MGR DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8567049808429145,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.10889982110912,
			"nilai_komparatif_unit": 1.1147760328170533,
			"summary_team": 0.95
		}]
	},
	"720147": {
		"nik": "720147",
		"nama": "ROBY KRISTIAN",
		"band": "III",
		"posisi": "MGR DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8406779661016985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9962197580645119,
			"nilai_komparatif_unit": 1.0014988627181296,
			"summary_team": 0.8375
		}]
	},
	"720169": {
		"nik": "720169",
		"nama": "DANANG TJATUR WIDJAJADI,MM",
		"band": "III",
		"posisi": "MGR BACKBONE, DC & CME OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.851851851851851,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9391304347826097,
			"nilai_komparatif_unit": 0.9441070153096283,
			"summary_team": 0.8
		}]
	},
	"720202": {
		"nik": "720202",
		"nama": "BARNET",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379084967320288,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0570537107198539,
			"nilai_komparatif_unit": 1.0626551828029078,
			"summary_team": 0.8857142857142857
		}]
	},
	"720217": {
		"nik": "720217",
		"nama": "SUWITO, ST",
		"band": "III",
		"posisi": "MGR GOVERNMENT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8419047619047649,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.950226244343888,
			"nilai_komparatif_unit": 0.9552616230822615,
			"summary_team": 0.8
		}]
	},
	"720228": {
		"nik": "720228",
		"nama": "FAZLIANSYAH",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8319727891156488,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0216680294358105,
			"nilai_komparatif_unit": 1.0270819879574982,
			"summary_team": 0.85
		}]
	},
	"720244": {
		"nik": "720244",
		"nama": "CESMAWATI",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379084967320288,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.034321372854911,
			"nilai_komparatif_unit": 1.0398023831727379,
			"summary_team": 0.8666666666666667
		}]
	},
	"720245": {
		"nik": "720245",
		"nama": "SETYAWAN SUBAGYO, ST",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & CONSTRUCTION SPV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8836879432624103,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9354735152487973,
			"nilai_komparatif_unit": 0.9404307172594066,
			"summary_team": 0.8266666666666667
		}]
	},
	"720259": {
		"nik": "720259",
		"nama": "DANAN SURYADI",
		"band": "III",
		"posisi": "AVP HC SYSTEM, POLICY & CULTURE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8484848484848487,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9972527472527469,
			"nilai_komparatif_unit": 1.002537325857254,
			"summary_team": 0.8461538461538461
		}]
	},
	"720281": {
		"nik": "720281",
		"nama": "DEVY YUNARWATY, ST",
		"band": "III",
		"posisi": "MGR DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8691358024691354,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0498934659090915,
			"nilai_komparatif_unit": 1.0554569948762862,
			"summary_team": 0.9125
		}]
	},
	"720295": {
		"nik": "720295",
		"nama": "VIRA SMARA YUDHA",
		"band": "III",
		"posisi": "MGR HUBUNGAN ANTAR INSTANSI/PEMDA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8444444444444437,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006578947368422,
			"nilai_komparatif_unit": 1.0119129467819752,
			"summary_team": 0.85
		}]
	},
	"720324": {
		"nik": "720324",
		"nama": "BARMAWI, ST",
		"band": "III",
		"posisi": "MGR SALES PROMOTION & PRICING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333323,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9512867647058834,
			"nilai_komparatif_unit": 0.9563277632865987,
			"summary_team": 0.8117647058823528
		}]
	},
	"720334": {
		"nik": "720334",
		"nama": "KRISDIAN",
		"band": "III",
		"posisi": "KAKANDATEL SUBANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8476190476190508,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0224719101123556,
			"nilai_komparatif_unit": 1.0278901285076165,
			"summary_team": 0.8666666666666667
		}]
	},
	"720351": {
		"nik": "720351",
		"nama": "HENDRO WIDIJANTA",
		"band": "III",
		"posisi": "MGR ASSET & FACILITY MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8650793650793634,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9970183486238552,
			"nilai_komparatif_unit": 1.0023016851179922,
			"summary_team": 0.8625
		}]
	},
	"720363": {
		"nik": "720363",
		"nama": "MAHFUDIN ALLADIF",
		"band": "III",
		"posisi": "AVP LOGISTIC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9555555555555556,
			"nilai_komparatif_unit": 0.9606191750424088,
			"summary_team": 0.8600000000000001
		}]
	},
	"720369": {
		"nik": "720369",
		"nama": "IRFAN HUSAIN",
		"band": "III",
		"posisi": "KAKANDATEL CIBADAK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8476190476190508,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9752808988764008,
			"nilai_komparatif_unit": 0.980449045653419,
			"summary_team": 0.8266666666666667
		}]
	},
	"720388": {
		"nik": "720388",
		"nama": "ANDI AUDY OCEANTO (DICKY)",
		"band": "III",
		"posisi": "MGR NTE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.851851851851851,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9894409937888208,
			"nilai_komparatif_unit": 0.9946841768440724,
			"summary_team": 0.8428571428571427
		}]
	},
	"720398": {
		"nik": "720398",
		"nama": "AGOES WINDARTO,ST",
		"band": "III",
		"posisi": "KEPALA SEKOLAH SMK TELKOM MALANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9023809523809518,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9915289543119015,
			"nilai_komparatif_unit": 0.9967832017553313,
			"summary_team": 0.8947368421052629
		}]
	},
	"720424": {
		"nik": "720424",
		"nama": "HERI SAKTIANTO",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8476190476190508,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9438202247190977,
			"nilai_komparatif_unit": 0.9488216570839539,
			"summary_team": 0.8
		}]
	},
	"720444": {
		"nik": "720444",
		"nama": "NOFRIL, ST.",
		"band": "III",
		"posisi": "MGR CCAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481503,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0218340611353687,
			"nilai_komparatif_unit": 1.0272488994817244,
			"summary_team": 0.8666666666666667
		}]
	},
	"720445": {
		"nik": "720445",
		"nama": "JAKA PRABAWA",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8319727891156488,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9765944399018777,
			"nilai_komparatif_unit": 0.9817695473123145,
			"summary_team": 0.8125
		}]
	},
	"720450": {
		"nik": "720450",
		"nama": "SUSANTI NATALIA RETNAWATI",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8476190476190508,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9752808988764008,
			"nilai_komparatif_unit": 0.980449045653419,
			"summary_team": 0.8266666666666667
		}]
	},
	"720463": {
		"nik": "720463",
		"nama": "KURNIA DEWANTI",
		"band": "III",
		"posisi": "MGR PERFORMANCE & DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7809523809523811,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.921951219512195,
			"nilai_komparatif_unit": 0.926836765029573,
			"summary_team": 0.7200000000000001
		}]
	},
	"720494": {
		"nik": "720494",
		"nama": "AGUNG RAHMAT ZULKARNAIN",
		"band": "III",
		"posisi": "KAKANDATEL METRO",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8631578947368416,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9930313588850178,
			"nilai_komparatif_unit": 0.9982935677756176,
			"summary_team": 0.857142857142857
		}]
	},
	"720516": {
		"nik": "720516",
		"nama": "HELDI FUAD ABDILLAH",
		"band": "III",
		"posisi": "MGR INFRA MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.851851851851851,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9391304347826095,
			"nilai_komparatif_unit": 0.944107015309628,
			"summary_team": 0.7999999999999998
		}]
	},
	"720534": {
		"nik": "720534",
		"nama": "AZNAWAR AZIZ",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481503,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.048034934497814,
			"nilai_komparatif_unit": 1.0535886148530504,
			"summary_team": 0.8888888888888888
		}]
	},
	"720549": {
		"nik": "720549",
		"nama": "DONNY SUHENDRI",
		"band": "III",
		"posisi": "KAKANDATEL BUKITTINGGI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8419047619047649,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0135746606334806,
			"nilai_komparatif_unit": 1.0189457312877455,
			"summary_team": 0.8533333333333334
		}]
	},
	"720550": {
		"nik": "720550",
		"nama": "MOHAMAD VENI RAHARJA",
		"band": "III",
		"posisi": "AVP SECRETARY FOUNDATION OFFICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.056,
			"nilai_komparatif_unit": 1.0615958883259364,
			"summary_team": 0.8800000000000001
		}]
	},
	"720558": {
		"nik": "720558",
		"nama": "CAMELIA SINTADEWI",
		"band": "III",
		"posisi": "MGR HR SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333329,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9600000000000003,
			"nilai_komparatif_unit": 0.965087171205397,
			"summary_team": 0.7999999999999998
		}]
	},
	"720565": {
		"nik": "720565",
		"nama": "M. SATRIA KESUMA SIMBOLON",
		"band": "III",
		"posisi": "MGR TELKOMGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.857971014492753,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9615709459459467,
			"nilai_komparatif_unit": 0.9666664418086158,
			"summary_team": 0.8250000000000001
		}]
	},
	"720587": {
		"nik": "720587",
		"nama": "AZRIL SYAFRI",
		"band": "III",
		"posisi": "MGR DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8416666666666656,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0033003300330046,
			"nilai_komparatif_unit": 1.0086169556052014,
			"summary_team": 0.8444444444444444
		}]
	},
	"730002": {
		"nik": "730002",
		"nama": "SITI AISYAH APRIYANTI",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8631578947368416,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0684281842818435,
			"nilai_komparatif_unit": 1.0740899312548777,
			"summary_team": 0.9222222222222223
		}]
	},
	"730003": {
		"nik": "730003",
		"nama": "NURIL APRIZAMULDINI, M.M",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8392857142857179,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.032624113475173,
			"nilai_komparatif_unit": 1.0380961297835785,
			"summary_team": 0.8666666666666667
		}]
	},
	"730090": {
		"nik": "730090",
		"nama": "SONNY YUHENSKY S.T, M.T",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8406779661016985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9516129032258024,
			"nilai_komparatif_unit": 0.9566556300591089,
			"summary_team": 0.8
		}]
	},
	"730109": {
		"nik": "730109",
		"nama": "IRFAN SUKMA",
		"band": "III",
		"posisi": "VP FINANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8633333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9947762888939362,
			"nilai_komparatif_unit": 1.000047744407137,
			"summary_team": 0.8588235294117648
		}]
	},
	"730154": {
		"nik": "730154",
		"nama": "KUSWANDARI SETYARINI",
		"band": "III",
		"posisi": "SENIOR STAFF III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8552845528455273,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0203940546145882,
			"nilai_komparatif_unit": 1.0258012621695813,
			"summary_team": 0.8727272727272727
		}]
	},
	"730175": {
		"nik": "730175",
		"nama": "AGUNG PRIYAMBODO, ST",
		"band": "III",
		"posisi": "MGR ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8836879432624103,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9807383627608357,
			"nilai_komparatif_unit": 0.9859354293848616,
			"summary_team": 0.8666666666666666
		}]
	},
	"730187": {
		"nik": "730187",
		"nama": "YUNIARTI, ST, M.M",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8631578947368416,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9783197831978327,
			"nilai_komparatif_unit": 0.9835040334382015,
			"summary_team": 0.8444444444444446
		}]
	},
	"730196": {
		"nik": "730196",
		"nama": "YULKHAIRI",
		"band": "III",
		"posisi": "MGR PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481503,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0520658380920362,
			"nilai_komparatif_unit": 1.0576408787563314,
			"summary_team": 0.8923076923076924
		}]
	},
	"730253": {
		"nik": "730253",
		"nama": "AANG DARMAWANGSA",
		"band": "III",
		"posisi": "KAKANDATEL KUNINGAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111126,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9909677419354822,
			"nilai_komparatif_unit": 0.9962190154378272,
			"summary_team": 0.8533333333333334
		}]
	},
	"730262": {
		"nik": "730262",
		"nama": "FEBRUARTO",
		"band": "III",
		"posisi": "MGR ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8319727891156488,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0283455982556524,
			"nilai_komparatif_unit": 1.033794942127155,
			"summary_team": 0.8555555555555556
		}]
	},
	"730312": {
		"nik": "730312",
		"nama": "Dr. DADUK MERDIKA MANSUR, ST. MM.",
		"band": "III",
		"posisi": "KEPALA SEKOLAH SMK TELKOM JAKARTA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9023809523809518,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0690672047182994,
			"nilai_komparatif_unit": 1.0747323379479627,
			"summary_team": 0.9647058823529411
		}]
	},
	"730342": {
		"nik": "730342",
		"nama": "WAHYU WIDODO",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8319727891156488,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9916189697465219,
			"nilai_komparatif_unit": 0.9968736941940424,
			"summary_team": 0.825
		}]
	},
	"730351": {
		"nik": "730351",
		"nama": "Sri Maizawati",
		"band": "III",
		"posisi": "MGR SISFO & DATA ANALITIK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8888888888888893,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9899999999999995,
			"nilai_komparatif_unit": 0.9952461453055649,
			"summary_team": 0.88
		}]
	},
	"730383": {
		"nik": "730383",
		"nama": "AIDIL FITRI, M.M",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8392857142857179,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9531914893616982,
			"nilai_komparatif_unit": 0.9582425813386878,
			"summary_team": 0.8
		}]
	},
	"730388": {
		"nik": "730388",
		"nama": "WIDYATMOKO",
		"band": "III",
		"posisi": "KEPALA SEKOLAH SMP TELKOM PURWOKERTO",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9023809523809518,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9376902780596719,
			"nilai_komparatif_unit": 0.9426592269993861,
			"summary_team": 0.8461538461538463
		}]
	},
	"730390": {
		"nik": "730390",
		"nama": "AGUNG SUBIANTORO, ST",
		"band": "III",
		"posisi": "MGR FINANCE & PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8207407407407394,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.974729241877258,
			"nilai_komparatif_unit": 0.9798944653484415,
			"summary_team": 0.8
		}]
	},
	"730399": {
		"nik": "730399",
		"nama": "HUSNUL RAKHMAWATI",
		"band": "III",
		"posisi": "KABAG MANAJEMEN DATA PESERTA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8400000000000001,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9722222222222222,
			"nilai_komparatif_unit": 0.9773741606536136,
			"summary_team": 0.8166666666666668
		}]
	},
	"730429": {
		"nik": "730429",
		"nama": "Gin Gin Hastagina",
		"band": "III",
		"posisi": "MANAGER INTERNAL AUDIT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8583333333333327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0873786407766997,
			"nilai_komparatif_unit": 1.0931408088087025,
			"summary_team": 0.9333333333333332
		}]
	},
	"730433": {
		"nik": "730433",
		"nama": "AGUS GOUSUR ALAM",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8319727891156488,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.051717089125099,
			"nilai_komparatif_unit": 1.0572902817209542,
			"summary_team": 0.875
		}]
	},
	"730439": {
		"nik": "730439",
		"nama": "TARMIZY HASAN ZA",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8304347826086982,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.873036649214657,
			"nilai_komparatif_unit": 0.877662989738762,
			"summary_team": 0.725
		}]
	},
	"730444": {
		"nik": "730444",
		"nama": "AGUS SATRIAWAN",
		"band": "III",
		"posisi": "KAKANDATEL PURWAKARTA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8319727891156488,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0464143138858129,
			"nilai_komparatif_unit": 1.0519594063509325,
			"summary_team": 0.8705882352941177
		}]
	},
	"730451": {
		"nik": "730451",
		"nama": "RINI MARLINI",
		"band": "III",
		"posisi": "MGR PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8419047619047649,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0294117647058787,
			"nilai_komparatif_unit": 1.0348667583391167,
			"summary_team": 0.8666666666666667
		}]
	},
	"730458": {
		"nik": "730458",
		"nama": "AGUS SULISTYO",
		"band": "III",
		"posisi": "MGR ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8552845528455273,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9520640956002184,
			"nilai_komparatif_unit": 0.9571092133635825,
			"summary_team": 0.8142857142857142
		}]
	},
	"730463": {
		"nik": "730463",
		"nama": "HENDRAWAN SAPTA NUGROHO",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & CONSTRUCTION SPV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8406779661016985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0195852534562166,
			"nilai_komparatif_unit": 1.0249881750633307,
			"summary_team": 0.857142857142857
		}]
	},
	"730480": {
		"nik": "730480",
		"nama": "DWI JOKO PURWANTO",
		"band": "III",
		"posisi": "AVP HC PARTNER 01",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8484848484848487,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0528571428571425,
			"nilai_komparatif_unit": 1.0584363767535374,
			"summary_team": 0.8933333333333333
		}]
	},
	"730482": {
		"nik": "730482",
		"nama": "TUBAGUS SYAMSUDIN",
		"band": "III",
		"posisi": "MGR SYSTEM, POLICY & ORGANIZATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7866666666666667,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9533898305084745,
			"nilai_komparatif_unit": 0.9584419735223086,
			"summary_team": 0.75
		}]
	},
	"730496": {
		"nik": "730496",
		"nama": "TEGUH BUDI PRASAJO",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8319727891156488,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9164963205233005,
			"nilai_komparatif_unit": 0.9213529597854028,
			"summary_team": 0.7625
		}]
	},
	"730516": {
		"nik": "730516",
		"nama": "Mochamad Lutfi Ariffianto C.",
		"band": "III",
		"posisi": "MGR FIXED INCOME",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740738,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9915254237288139,
			"nilai_komparatif_unit": 0.9967796524632014,
			"summary_team": 0.8666666666666667
		}]
	},
	"730521": {
		"nik": "730521",
		"nama": "SUHERMAN",
		"band": "III",
		"posisi": "KAKANDATEL TANJUNGPINANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481503,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9432314410480328,
			"nilai_komparatif_unit": 0.9482297533677457,
			"summary_team": 0.8000000000000003
		}]
	},
	"730538": {
		"nik": "730538",
		"nama": "BAMBANG SUKRISNO PRASETYO ADHI",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA, DATA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8476190476190508,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9910112359550525,
			"nilai_komparatif_unit": 0.9962627399381516,
			"summary_team": 0.8400000000000001
		}]
	},
	"730542": {
		"nik": "730542",
		"nama": "HARI WIDAYANTO",
		"band": "III",
		"posisi": "MGR FULFILLMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8552845528455273,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9893243638490801,
			"nilai_komparatif_unit": 0.994566928866341,
			"summary_team": 0.8461538461538461
		}]
	},
	"730553": {
		"nik": "730553",
		"nama": "SRI WIJAYANTI",
		"band": "III",
		"posisi": "KAKANDATEL CIANJUR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8476190476190508,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0696629213483104,
			"nilai_komparatif_unit": 1.0753312113618143,
			"summary_team": 0.9066666666666666
		}]
	},
	"730561": {
		"nik": "730561",
		"nama": "LATIFAH HANUM, ST.",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8419047619047649,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0030165912518818,
			"nilai_komparatif_unit": 1.0083317132534981,
			"summary_team": 0.8444444444444446
		}]
	},
	"730568": {
		"nik": "730568",
		"nama": "Zen Agus Wahyudin",
		"band": "III",
		"posisi": "MGR KINERJA BISNIS & INVESTASI LANGSUNG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740738,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9581567796610173,
			"nilai_komparatif_unit": 0.9632341833899206,
			"summary_team": 0.8375
		}]
	},
	"730594": {
		"nik": "730594",
		"nama": "NORMANSYAH",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8304347826086982,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9633507853403113,
			"nilai_komparatif_unit": 0.9684557128151858,
			"summary_team": 0.8
		}]
	},
	"730596": {
		"nik": "730596",
		"nama": "FRANSISKUS NADEAK",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8405797101449296,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0409482758620665,
			"nilai_komparatif_unit": 1.0464644030446424,
			"summary_team": 0.875
		}]
	},
	"730605": {
		"nik": "730605",
		"nama": "NYOMAN BOGI ADITYA KARNA,",
		"band": "III",
		"posisi": "DOSEN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.005299136672288,
			"summary_team": 0.8666666666666665
		}]
	},
	"740004": {
		"nik": "740004",
		"nama": "MUHAMAD IRJAN MARSAOLY",
		"band": "III",
		"posisi": "KEPALA SEKOLAH SMP TELKOM MAKASSAR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9023809523809518,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0381891403971675,
			"nilai_komparatif_unit": 1.0436906465438176,
			"summary_team": 0.9368421052631577
		}]
	},
	"740112": {
		"nik": "740112",
		"nama": "RISMA ULI SIBAGARIANG",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7939393939393933,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0796074154852788,
			"nilai_komparatif_unit": 1.0853284027323513,
			"summary_team": 0.857142857142857
		}]
	},
	"740154": {
		"nik": "740154",
		"nama": "AFIANTO (TITO)",
		"band": "III",
		"posisi": "MGR ACCESS OM & PARTNERSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9277777777777774,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0508982035928147,
			"nilai_komparatif_unit": 1.0564670568023153,
			"summary_team": 0.975
		}]
	},
	"740201": {
		"nik": "740201",
		"nama": "DODY OSCAR",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481503,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0758733624454122,
			"nilai_komparatif_unit": 1.0815745624350848,
			"summary_team": 0.9125000000000001
		}]
	},
	"740218": {
		"nik": "740218",
		"nama": "DIAN RISDIANSAH",
		"band": "III",
		"posisi": "KAKANDATEL SUMEDANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8567049808429145,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9960644007155606,
			"nilai_komparatif_unit": 1.0013426821093532,
			"summary_team": 0.8533333333333334
		}]
	},
	"740224": {
		"nik": "740224",
		"nama": "LENNY RATNA MARLINA, RD",
		"band": "III",
		"posisi": "MGR HR PLANNING, RECRUIT & CAREER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7277777777777781,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0259541984732818,
			"nilai_komparatif_unit": 1.0313908699904997,
			"summary_team": 0.7466666666666666
		}]
	},
	"740225": {
		"nik": "740225",
		"nama": "SURYA RAHMADIANSYAH",
		"band": "III",
		"posisi": "MGR PLANNING, ENGINEERING & CAPEX MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8805555555555549,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9842271293375402,
			"nilai_komparatif_unit": 0.9894426834124738,
			"summary_team": 0.8666666666666667
		}]
	},
	"740233": {
		"nik": "740233",
		"nama": "NOVAN HUTAJULU",
		"band": "III",
		"posisi": "MGR BILLING & PAYMENT COLLECTION (CONS)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8466666666666655,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9955005624296975,
			"nilai_komparatif_unit": 1.0007758559673523,
			"summary_team": 0.8428571428571427
		}]
	},
	"740264": {
		"nik": "740264",
		"nama": "Bayu Vidya Arta",
		"band": "III",
		"posisi": "MGR PERFORMANSI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8694444444444438,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9488817891373809,
			"nilai_komparatif_unit": 0.9539100434238652,
			"summary_team": 0.825
		}]
	},
	"740280": {
		"nik": "740280",
		"nama": "EKO WIDIYATMOKO",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8419047619047649,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9898190045248834,
			"nilai_komparatif_unit": 0.995064190710689,
			"summary_team": 0.8333333333333334
		}]
	},
	"740294": {
		"nik": "740294",
		"nama": "ARYONO",
		"band": "III",
		"posisi": "MGR CCAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379084967320288,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9547581903276101,
			"nilai_komparatif_unit": 0.9598175844671427,
			"summary_team": 0.8
		}]
	},
	"740303": {
		"nik": "740303",
		"nama": "TYAS PRATITIS, ST. MSEM.",
		"band": "III",
		"posisi": "MGR QUALITY, CHANGE & RISK MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8650793650793634,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.040366972477066,
			"nilai_komparatif_unit": 1.045880019253557,
			"summary_team": 0.9
		}]
	},
	"740304": {
		"nik": "740304",
		"nama": "KHUSNAWAN",
		"band": "III",
		"posisi": "MGR ENTERPRISE, OLO & WIFI DEPLOYMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8444444444444437,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9613003095975241,
			"nilai_komparatif_unit": 0.9663943713211944,
			"summary_team": 0.8117647058823529
		}]
	},
	"740316": {
		"nik": "740316",
		"nama": "KEN WIDURI",
		"band": "III",
		"posisi": "ANALYST BUSINESS DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8619047619047623,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9492717227523852,
			"nilai_komparatif_unit": 0.9543020433503887,
			"summary_team": 0.8181818181818181
		}]
	},
	"750054": {
		"nik": "750054",
		"nama": "AGUS SULISTYA",
		"band": "III",
		"posisi": "WAKIL REKTOR II BIDANG SUMBER DAYA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0271739130434778,
			"nilai_komparatif_unit": 1.0326170479949044,
			"summary_team": 0.9130434782608694
		}]
	},
	"760043": {
		"nik": "760043",
		"nama": "MONA MAISYARAH, SE",
		"band": "III",
		"posisi": "MGR FINANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8304347826086982,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9633507853403112,
			"nilai_komparatif_unit": 0.9684557128151857,
			"summary_team": 0.7999999999999999
		}]
	},
	"770051": {
		"nik": "770051",
		"nama": "ARIS HARNOTO",
		"band": "III",
		"posisi": "MGR ACCESS NEW FTTH & MODERNIZATION DEPL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8444444444444437,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9769736842105273,
			"nilai_komparatif_unit": 0.9821508012883879,
			"summary_team": 0.8250000000000001
		}]
	},
	"780013": {
		"nik": "780013",
		"nama": "DONALD ROMALDY BARTELS",
		"band": "III",
		"posisi": "KAKANDATEL CIAMIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333323,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0202205882352955,
			"nilai_komparatif_unit": 1.0256268765682366,
			"summary_team": 0.8705882352941177
		}]
	},
	"780042": {
		"nik": "780042",
		"nama": "Retno Susilowati",
		"band": "III",
		"posisi": "MGR KOMUNIKASI & SEKRETARIAT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8583333333333327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9786407766990298,
			"nilai_komparatif_unit": 0.9838267279278323,
			"summary_team": 0.84
		}]
	},
	"780062": {
		"nik": "780062",
		"nama": "ANDI INDRIANI RAHMI",
		"band": "III",
		"posisi": "MGR PARTNERSHIP MARKETING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333323,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9814453125000012,
			"nilai_komparatif_unit": 0.9866461253473153,
			"summary_team": 0.8375
		}]
	},
	"790003": {
		"nik": "790003",
		"nama": "JANNES SIANTURI",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8392857142857179,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9796690307328563,
			"nilai_komparatif_unit": 0.9848604308203178,
			"summary_team": 0.8222222222222222
		}]
	},
	"790020": {
		"nik": "790020",
		"nama": "ZULKARNAEN BINTANG",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379084967320288,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.034321372854911,
			"nilai_komparatif_unit": 1.0398023831727379,
			"summary_team": 0.8666666666666667
		}]
	},
	"790022": {
		"nik": "790022",
		"nama": "HENRI SURYADI",
		"band": "III",
		"posisi": "MGR ACCESS NEW FTTH & MODERNIZATION DEPL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8805555555555549,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0078864353312311,
			"nilai_komparatif_unit": 1.0132273633021966,
			"summary_team": 0.8875
		}]
	},
	"790055": {
		"nik": "790055",
		"nama": "YULIA SYAHRIL",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8419047619047649,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8710407239818974,
			"nilai_komparatif_unit": 0.8756564878254064,
			"summary_team": 0.7333333333333334
		}]
	},
	"790095": {
		"nik": "790095",
		"nama": "KEN ANNISA",
		"band": "III",
		"posisi": "ANALYST FINANCIAL PLANNING AND POLICY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8400000000000003,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8874458874458871,
			"nilai_komparatif_unit": 0.892148584492723,
			"summary_team": 0.7454545454545455
		}]
	},
	"790107": {
		"nik": "790107",
		"nama": "HOLLYANTO, ST",
		"band": "III",
		"posisi": "MGR CCAN & WAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8207407407407394,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9921351211964945,
			"nilai_komparatif_unit": 0.9973925808010919,
			"summary_team": 0.8142857142857142
		}]
	},
	"790112": {
		"nik": "790112",
		"nama": "DEFI YANTI",
		"band": "III",
		"posisi": "MGR BIDDING MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8853333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9538152610441776,
			"nilai_komparatif_unit": 0.958869658472565,
			"summary_team": 0.8444444444444446
		}]
	},
	"790118": {
		"nik": "790118",
		"nama": "AZIZAH KUSUMA WARDHANY",
		"band": "III",
		"posisi": "MGR REGIONAL GOVERNMENT SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8421052631578936,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0687500000000014,
			"nilai_komparatif_unit": 1.0744134523185096,
			"summary_team": 0.9
		}]
	},
	"790121": {
		"nik": "790121",
		"nama": "R. KIKI ERIK HERMAYA",
		"band": "III",
		"posisi": "KAKANDATEL SOREANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8319727891156488,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0256745707277157,
			"nilai_komparatif_unit": 1.0311097604592925,
			"summary_team": 0.8533333333333334
		}]
	},
	"800012": {
		"nik": "800012",
		"nama": "HASNUL ARIF RANGKUTI, M.Si",
		"band": "III",
		"posisi": "MGR SOLUTION & SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8853333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9538152610441776,
			"nilai_komparatif_unit": 0.958869658472565,
			"summary_team": 0.8444444444444446
		}]
	},
	"800040": {
		"nik": "800040",
		"nama": "DESTIANA PAKPAHAN, M.M",
		"band": "III",
		"posisi": "MGR PERFORMANCE MGT & SLA MONITORING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8552845528455273,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9353612167300392,
			"nilai_komparatif_unit": 0.9403178236554496,
			"summary_team": 0.7999999999999999
		}]
	},
	"800043": {
		"nik": "800043",
		"nama": "FEBRIZA MATILLYA SINDA RAHMAH , M. T.",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7939393939393933,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9356597600872417,
			"nilai_komparatif_unit": 0.9406179490347044,
			"summary_team": 0.7428571428571428
		}]
	},
	"800096": {
		"nik": "800096",
		"nama": "BERTHA INDRIATY",
		"band": "III",
		"posisi": "MGR ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8392857142857179,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.923404255319145,
			"nilai_komparatif_unit": 0.9282975006718538,
			"summary_team": 0.775
		}]
	},
	"800104": {
		"nik": "800104",
		"nama": "ANWIRMAN",
		"band": "III",
		"posisi": "MGR FINANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379084967320288,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.125250724314683,
			"nilai_komparatif_unit": 1.131213581693418,
			"summary_team": 0.9428571428571426
		}]
	},
	"800106": {
		"nik": "800106",
		"nama": "DIAN FITRI",
		"band": "III",
		"posisi": "MGR SALES BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8853333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9789156626506033,
			"nilai_komparatif_unit": 0.9841030705376325,
			"summary_team": 0.8666666666666667
		}]
	},
	"810026": {
		"nik": "810026",
		"nama": "DARMAWAN DAMANIK",
		"band": "III",
		"posisi": "KAKANDATEL LHOKSEUMAWE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8304347826086982,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9499709133216959,
			"nilai_komparatif_unit": 0.9550049390260861,
			"summary_team": 0.788888888888889
		}]
	},
	"810049": {
		"nik": "810049",
		"nama": "RIZKI PUSPITASARI",
		"band": "III",
		"posisi": "MGR GOVERNMENT SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8319727891156488,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.009648405560095,
			"nilai_komparatif_unit": 1.014998670452116,
			"summary_team": 0.84
		}]
	},
	"810068": {
		"nik": "810068",
		"nama": "LYDIA OKTARINI",
		"band": "III",
		"posisi": "MGR CUSTOMER DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8242424242424247,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0225840336134446,
			"nilai_komparatif_unit": 1.028002846166462,
			"summary_team": 0.8428571428571427
		}]
	},
	"810105": {
		"nik": "810105",
		"nama": "TRESNA HUZAIRY",
		"band": "III",
		"posisi": "MGR ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8406779661016985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9962197580645119,
			"nilai_komparatif_unit": 1.0014988627181296,
			"summary_team": 0.8375
		}]
	},
	"810119": {
		"nik": "810119",
		"nama": "NOVAN RESTU PRAMONO",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8476190476190508,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8530682800345689,
			"nilai_komparatif_unit": 0.8575888054412659,
			"summary_team": 0.7230769230769231
		}]
	},
	"820004": {
		"nik": "820004",
		"nama": "TULUS HENDRA F TAMPUBOLON",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.835294117647058,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9945828819068266,
			"nilai_komparatif_unit": 0.9998533125299692,
			"summary_team": 0.8307692307692308
		}]
	},
	"820006": {
		"nik": "820006",
		"nama": "Ratna Widyastuti",
		"band": "III",
		"posisi": "MGR AKUNTANSI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8694444444444438,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.027476038338659,
			"nilai_komparatif_unit": 1.0329207742933169,
			"summary_team": 0.8933333333333333
		}]
	},
	"820007": {
		"nik": "820007",
		"nama": "MAWADDAH",
		"band": "III",
		"posisi": "MGR HC PLANNING, DEVELOPMENT & SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8691358024691354,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.021129261363637,
			"nilai_komparatif_unit": 1.0265403648796756,
			"summary_team": 0.8875
		}]
	},
	"820017": {
		"nik": "820017",
		"nama": "LENGKA TSANI ALIYATI",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8628205128205122,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9985141158989607,
			"nilai_komparatif_unit": 1.0038053786683183,
			"summary_team": 0.8615384615384616
		}]
	},
	"820034": {
		"nik": "820034",
		"nama": "SAKDIAH KAMAL",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8304347826086982,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0356020942408346,
			"nilai_komparatif_unit": 1.0410898912763247,
			"summary_team": 0.8600000000000001
		}]
	},
	"820037": {
		"nik": "820037",
		"nama": "YURI ROFHANDA",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379084967320288,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0078003120124772,
			"nilai_komparatif_unit": 1.0131407836042061,
			"summary_team": 0.8444444444444443
		}]
	},
	"820042": {
		"nik": "820042",
		"nama": "ASMANUR ARUMSARI",
		"band": "III",
		"posisi": "MGR LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8650793650793634,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0548165137614698,
			"nilai_komparatif_unit": 1.0604061306320784,
			"summary_team": 0.9125
		}]
	},
	"820048": {
		"nik": "820048",
		"nama": "LYANIE NOVRIDHA",
		"band": "III",
		"posisi": "MGR GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8406779661016985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0441308243727554,
			"nilai_komparatif_unit": 1.0496638163148557,
			"summary_team": 0.8777777777777778
		}]
	},
	"820051": {
		"nik": "820051",
		"nama": "WIDA AISYIYYAH",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8567049808429145,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.916517590936193,
			"nilai_komparatif_unit": 0.9213743429131203,
			"summary_team": 0.7851851851851853
		}]
	},
	"820055": {
		"nik": "820055",
		"nama": "HIDA TRIWARANCE",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8406779661016985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0044802867383469,
			"nilai_komparatif_unit": 1.0098031650623926,
			"summary_team": 0.8444444444444443
		}]
	},
	"820057": {
		"nik": "820057",
		"nama": "SYARIFAH HAFNI",
		"band": "III",
		"posisi": "MGR FINANCIAL SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8466666666666655,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9617547806524197,
			"nilai_komparatif_unit": 0.9668512506803235,
			"summary_team": 0.8142857142857142
		}]
	},
	"830024": {
		"nik": "830024",
		"nama": "RICKI DWI WENDY",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379084967320288,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.034321372854911,
			"nilai_komparatif_unit": 1.0398023831727379,
			"summary_team": 0.8666666666666667
		}]
	},
	"830055": {
		"nik": "830055",
		"nama": "FRANSISCA MARIA ELDA KOSARIA SUNARDJO",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481503,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.957969432314408,
			"nilai_komparatif_unit": 0.9630458432641165,
			"summary_team": 0.8125
		}]
	},
	"830076": {
		"nik": "830076",
		"nama": "ADRI SYAFMA PUTRA",
		"band": "III",
		"posisi": "SENIOR AM ENTERPRISE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8406779661016985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.021001344086017,
			"nilai_komparatif_unit": 1.026411769750919,
			"summary_team": 0.8583333333333333
		}]
	},
	"830086": {
		"nik": "830086",
		"nama": "DITO BARATA JUNANTO",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8628205128205122,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.014115898959882,
			"nilai_komparatif_unit": 1.0194898377100108,
			"summary_team": 0.875
		}]
	},
	"830100": {
		"nik": "830100",
		"nama": "MUHAMMAD IQBAL",
		"band": "III",
		"posisi": "KAKANDATEL LUBUK LINGGAU",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8392857142857179,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.032624113475173,
			"nilai_komparatif_unit": 1.0380961297835785,
			"summary_team": 0.8666666666666667
		}]
	},
	"830109": {
		"nik": "830109",
		"nama": "DEWI ARSANTI BR KETAREN",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8406779661016985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0309139784946193,
			"nilai_komparatif_unit": 1.0363769325640346,
			"summary_team": 0.8666666666666667
		}]
	},
	"830111": {
		"nik": "830111",
		"nama": "DELVI ERISANDI",
		"band": "III",
		"posisi": "MGR TAX OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8466666666666655,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0123734533183364,
			"nilai_komparatif_unit": 1.0177381586108667,
			"summary_team": 0.857142857142857
		}]
	},
	"840006": {
		"nik": "840006",
		"nama": "HALIMA",
		"band": "III",
		"posisi": "MGR SHARED SERVICE PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8567049808429145,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9158524838310141,
			"nilai_komparatif_unit": 0.9207057113144893,
			"summary_team": 0.7846153846153846
		}]
	},
	"840024": {
		"nik": "840024",
		"nama": "SRI PURNAMA DEWI SIHITE",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8405797101449296,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.907112068965515,
			"nilai_komparatif_unit": 0.9119189797960454,
			"summary_team": 0.7625
		}]
	},
	"840031": {
		"nik": "840031",
		"nama": "AGUS FRIADI, M.M.",
		"band": "III",
		"posisi": "MGR ACCESS SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8304347826086982,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9633507853403113,
			"nilai_komparatif_unit": 0.9684557128151858,
			"summary_team": 0.8
		}]
	},
	"840047": {
		"nik": "840047",
		"nama": "JAYA RISMANDA SEMBIRING S.Kom",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481503,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0611353711790363,
			"nilai_komparatif_unit": 1.0667584725387134,
			"summary_team": 0.8999999999999998
		}]
	},
	"840053": {
		"nik": "840053",
		"nama": "TENNY FARYANI",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8567049808429145,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9649373881931992,
			"nilai_komparatif_unit": 0.9700507232934358,
			"summary_team": 0.8266666666666667
		}]
	},
	"840070": {
		"nik": "840070",
		"nama": "Herlina Ratna Ekawati",
		"band": "III",
		"posisi": "MGR RISET INVESTASI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740738,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9915254237288139,
			"nilai_komparatif_unit": 0.9967796524632014,
			"summary_team": 0.8666666666666667
		}]
	},
	"840071": {
		"nik": "840071",
		"nama": "ROESDIADE IBRAHIM",
		"band": "III",
		"posisi": "MGR HOME SERVICE 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8406779661016985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9780465949820746,
			"nilai_komparatif_unit": 0.9832293975607508,
			"summary_team": 0.8222222222222222
		}]
	},
	"840072": {
		"nik": "840072",
		"nama": "SONNI ERIKA SINAGA",
		"band": "III",
		"posisi": "MGR QUALITY & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8466666666666655,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0629921259842534,
			"nilai_komparatif_unit": 1.0686250665414103,
			"summary_team": 0.8999999999999999
		}]
	},
	"840073": {
		"nik": "840073",
		"nama": "MIA PUTRI MALINDA",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8567049808429145,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9629919499105516,
			"nilai_komparatif_unit": 0.968094975867441,
			"summary_team": 0.825
		}]
	},
	"840079": {
		"nik": "840079",
		"nama": "LEDY LESTARI",
		"band": "III",
		"posisi": "MGR UNIT PROGRAM & PERFORMANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9847828277606089,
			"summary_team": 0.8
		}]
	},
	"840088": {
		"nik": "840088",
		"nama": "LEDY CAROLINE",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8392857142857179,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0212765957446763,
			"nilai_komparatif_unit": 1.0266884800057365,
			"summary_team": 0.857142857142857
		}]
	},
	"840110": {
		"nik": "840110",
		"nama": "MUTHIA RAMADHANIA",
		"band": "III",
		"posisi": "MGR BILLING & PAYMENT COLLECTION EBIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8691358024691354,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9636008522727278,
			"nilai_komparatif_unit": 0.9687071048864545,
			"summary_team": 0.8375
		}]
	},
	"840131": {
		"nik": "840131",
		"nama": "TANTRI SUJIWATI",
		"band": "III",
		"posisi": "SENIOR OFFICER AKTUARIA&MANAJEMEN RISIKO",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0106750678844396,
			"summary_team": 0.8545454545454546
		}]
	},
	"840133": {
		"nik": "840133",
		"nama": "AGUS WIDIANTARA",
		"band": "III",
		"posisi": "MGR GOVERNMENT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481503,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0349344978165913,
			"nilai_komparatif_unit": 1.0404187571673875,
			"summary_team": 0.8777777777777778
		}]
	},
	"840174": {
		"nik": "840174",
		"nama": "TIMBUL RINALDI MANALU",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8637037037037024,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.027551457975988,
			"nilai_komparatif_unit": 1.0329965935896117,
			"summary_team": 0.8875000000000001
		}]
	},
	"840187": {
		"nik": "840187",
		"nama": "SHINTA ANINDHITA",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8319727891156488,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9615699100572332,
			"nilai_komparatif_unit": 0.9666654004305865,
			"summary_team": 0.7999999999999998
		}]
	},
	"850010": {
		"nik": "850010",
		"nama": "UCOK ERIKSON SIRAIT S.E",
		"band": "III",
		"posisi": "MGR IP & SERVICE NODE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8746666666666663,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9756097560975614,
			"nilai_komparatif_unit": 0.9807796455339401,
			"summary_team": 0.8533333333333333
		}]
	},
	"850016": {
		"nik": "850016",
		"nama": "ZULFIRMAN NASUTION",
		"band": "III",
		"posisi": "MGR INFRA MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8746666666666663,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9289253048780493,
			"nilai_komparatif_unit": 0.933847807026945,
			"summary_team": 0.8125
		}]
	},
	"850019": {
		"nik": "850019",
		"nama": "JAKA TRI MULYA",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8406779661016985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9813508064516088,
			"nilai_komparatif_unit": 0.9865511184984561,
			"summary_team": 0.8250000000000001
		}]
	},
	"850026": {
		"nik": "850026",
		"nama": "MASTAMAN ARABY",
		"band": "III",
		"posisi": "MGR GOVERNMENT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8304347826086982,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0837696335078502,
			"nilai_komparatif_unit": 1.089512676917084,
			"summary_team": 0.9
		}]
	},
	"850027": {
		"nik": "850027",
		"nama": "ZULHELMI",
		"band": "III",
		"posisi": "KAKANDATEL LANGSA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8304347826086982,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9633507853403113,
			"nilai_komparatif_unit": 0.9684557128151858,
			"summary_team": 0.8
		}]
	},
	"850029": {
		"nik": "850029",
		"nama": "SATRIA SITORUS",
		"band": "III",
		"posisi": "MGR CCAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8405797101449296,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0112068965517216,
			"nilai_komparatif_unit": 1.0165654201005097,
			"summary_team": 0.85
		}]
	},
	"850035": {
		"nik": "850035",
		"nama": "YUDI KURNIAWAN",
		"band": "III",
		"posisi": "MGR PERF MGT, SLA MONITORING & KPI REPOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.857971014492753,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.063555743243244,
			"nilai_komparatif_unit": 1.069191670485287,
			"summary_team": 0.9125000000000001
		}]
	},
	"850041": {
		"nik": "850041",
		"nama": "HENDRA MAULANA",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8392857142857179,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0127659574468044,
			"nilai_komparatif_unit": 1.0181327426723559,
			"summary_team": 0.8500000000000002
		}]
	},
	"850072": {
		"nik": "850072",
		"nama": "ROMAULI SITINJAK",
		"band": "III",
		"posisi": "MGR INFRA SERV DELIVERY&OPERATION SUPP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.857971014492753,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9470016891891898,
			"nilai_komparatif_unit": 0.9520199805690912,
			"summary_team": 0.8125
		}]
	},
	"850083": {
		"nik": "850083",
		"nama": "FIDYA ULFAH",
		"band": "III",
		"posisi": "MGR LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9111111111111112,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0376940133037693,
			"nilai_komparatif_unit": 1.0431928957042813,
			"summary_team": 0.9454545454545454
		}]
	},
	"850096": {
		"nik": "850096",
		"nama": "MARIO CAROLUS BEATO GELU",
		"band": "III",
		"posisi": "KAKANDATEL SIBOLGA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8405797101449296,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9649425287356298,
			"nilai_komparatif_unit": 0.9700558910763033,
			"summary_team": 0.8111111111111111
		}]
	},
	"850129": {
		"nik": "850129",
		"nama": "ERYANTO PRABOWO",
		"band": "III",
		"posisi": "MGR IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.851851851851851,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9831521739130445,
			"nilai_komparatif_unit": 0.9883620316522671,
			"summary_team": 0.8375
		}]
	},
	"850131": {
		"nik": "850131",
		"nama": "ARTIKA ARUMNINGTYAS",
		"band": "III",
		"posisi": "MGR REVENUE ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8650793650793634,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.060766324878577,
			"nilai_komparatif_unit": 1.0663874706114695,
			"summary_team": 0.9176470588235293
		}]
	},
	"850180": {
		"nik": "850180",
		"nama": "EKO ARDIANSYAH PUTRA",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8421052631578936,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0568750000000016,
			"nilai_komparatif_unit": 1.0624755250705262,
			"summary_team": 0.89
		}]
	},
	"860007": {
		"nik": "860007",
		"nama": "DENI ARIF ARDIYANSYAH",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8405797101449296,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0310344827586182,
			"nilai_komparatif_unit": 1.0364980753965982,
			"summary_team": 0.8666666666666667
		}]
	},
	"860014": {
		"nik": "860014",
		"nama": "KAWAKIF ROFII SIREGAR",
		"band": "III",
		"posisi": "MGR ACCESS NETWORK ELEMENT OM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9277777777777774,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0538922155688628,
			"nilai_komparatif_unit": 1.059476934457023,
			"summary_team": 0.9777777777777779
		}]
	},
	"860017": {
		"nik": "860017",
		"nama": "JUIS BAKARA",
		"band": "III",
		"posisi": "MGR CCAN & WAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.835294117647058,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0535211267605644,
			"nilai_komparatif_unit": 1.0591038791984118,
			"summary_team": 0.88
		}]
	},
	"860029": {
		"nik": "860029",
		"nama": "ADE IRHAM",
		"band": "III",
		"posisi": "MGR ACCESS SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8405797101449296,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9368534482758599,
			"nilai_komparatif_unit": 0.9418179627401783,
			"summary_team": 0.7875000000000001
		}]
	},
	"860030": {
		"nik": "860030",
		"nama": "T. KHAIRUL NOVA, S.E",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8304347826086982,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9633507853403112,
			"nilai_komparatif_unit": 0.9684557128151857,
			"summary_team": 0.7999999999999999
		}]
	},
	"860110": {
		"nik": "860110",
		"nama": "HERNI RAHMADIAH",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8419047619047649,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0690045248868743,
			"nilai_komparatif_unit": 1.0746693259675444,
			"summary_team": 0.9000000000000001
		}]
	},
	"860147": {
		"nik": "860147",
		"nama": "AGEAK RAPORTE BERMANO",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & DAMAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111126,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0016129032258045,
			"nilai_komparatif_unit": 1.0069205868927256,
			"summary_team": 0.8624999999999999
		}]
	},
	"860153": {
		"nik": "860153",
		"nama": "OCTA JULIAN ALADIYAH",
		"band": "III",
		"posisi": "SENIOR EXPERT CHANNEL MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333323,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9588068181818193,
			"nilai_komparatif_unit": 0.9638876665536866,
			"summary_team": 0.8181818181818181
		}]
	},
	"870014": {
		"nik": "870014",
		"nama": "PUTRI NURINA AYUNINGTIAS",
		"band": "III",
		"posisi": "MGR OLO FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8552845528455273,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.035578489951115,
			"nilai_komparatif_unit": 1.0410661619042478,
			"summary_team": 0.8857142857142857
		}]
	},
	"870032": {
		"nik": "870032",
		"nama": "EKADIAH FINANDIARSI",
		"band": "III",
		"posisi": "MGR WHOLESALE & INTERNATIONAL MARKETING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8345679012345671,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8848429676832053,
			"nilai_komparatif_unit": 0.8895318715024717,
			"summary_team": 0.7384615384615385
		}]
	},
	"870047": {
		"nik": "870047",
		"nama": "SHANDY ASRI ACHMAD",
		"band": "III",
		"posisi": "MGR REGIONAL BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8421052631578936,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9945312500000014,
			"nilai_komparatif_unit": 0.9998014070186131,
			"summary_team": 0.8375
		}]
	},
	"870050": {
		"nik": "870050",
		"nama": "ANGGI AGUSTINA",
		"band": "III",
		"posisi": "MGR SOLUTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8421052631578936,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9500000000000014,
			"nilai_komparatif_unit": 0.9550341798386752,
			"summary_team": 0.8
		}]
	},
	"870061": {
		"nik": "870061",
		"nama": "SANDI FUJIYANA",
		"band": "III",
		"posisi": "KAKANDATEL BANJAR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8628205128205122,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9271916790490349,
			"nilai_komparatif_unit": 0.9321049944777241,
			"summary_team": 0.8
		}]
	},
	"870066": {
		"nik": "870066",
		"nama": "ANGGI FITRINING TYAS",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8345679012345671,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9585798816568054,
			"nilai_komparatif_unit": 0.9636595274610107,
			"summary_team": 0.7999999999999998
		}]
	},
	"880006": {
		"nik": "880006",
		"nama": "AYU TRI HASTUTI",
		"band": "III",
		"posisi": "MGR CHANNEL ACTIVATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333323,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9375000000000012,
			"nilai_komparatif_unit": 0.9424679406302715,
			"summary_team": 0.8
		}]
	},
	"880020": {
		"nik": "880020",
		"nama": "ALDI WILMAN",
		"band": "III",
		"posisi": "MGR SEKDIV & PUBLIC RELATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9111111111111112,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0177383592017737,
			"nilai_komparatif_unit": 1.0231314938638143,
			"summary_team": 0.9272727272727272
		}]
	},
	"880025": {
		"nik": "880025",
		"nama": "IQRA PRASETIA RAHADI PUTRA",
		"band": "III",
		"posisi": "MGR CUSTOMER RELATIONSHIP MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8916666666666663,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.052480230050324,
			"nilai_komparatif_unit": 1.058057466634242,
			"summary_team": 0.9384615384615385
		}]
	},
	"880035": {
		"nik": "880035",
		"nama": "SRY HAZZAYANI BR BANGUN",
		"band": "III",
		"posisi": "MGR SALES PROMOTION & PRICING",
		"category": [{
			"code": "SO-3-01",
			"kriteria": 0.8458333333333323,
			"kriteria_bp": 0.9946474533854951,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9720853858784906,
			"nilai_komparatif_unit": 0.9773165180987398,
			"summary_team": 0.8222222222222223
		}]
	},
	"890016": {
		"nik": "890016",
		"nama": "KHOIRINA FAJAR",
		"band": "III",
		"posisi": "MGR APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333323,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9521484375000013,
			"nilai_komparatif_unit": 0.9571940022026195,
			"summary_team": 0.8125000000000001
		}]
	},
	"890028": {
		"nik": "890028",
		"nama": "FERIDA ARYANTI",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8567049808429145,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8871198568872961,
			"nilai_komparatif_unit": 0.8918208262536427,
			"summary_team": 0.76
		}]
	}
};