export const cbark1 = {
	"650863": {
		"nik": "650863",
		"nama": "HARDI PURWANTO",
		"band": "I",
		"posisi": "VP CORPORATE OFFICE SUPPORT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.877777777777777,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0177215189873428,
			"nilai_komparatif_unit": 1.0243177650087736,
			"summary_team": 0.8933333333333333
		}]
	},
	"651228": {
		"nik": "651228",
		"nama": "WERIZA",
		"band": "I",
		"posisi": "SGM ASSET MANAGEMENT CENTER",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8715151515151515,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9791376912378303,
			"nilai_komparatif_unit": 0.9854838605776386,
			"summary_team": 0.8533333333333333
		}]
	},
	"651265": {
		"nik": "651265",
		"nama": "ROSYIDUL UMAM ALY",
		"band": "I",
		"posisi": "EGM DIVISI SOLUTION,DELIVERY & ASSURANCE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8558558558558567,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9970526315789465,
			"nilai_komparatif_unit": 1.0035149145625606,
			"summary_team": 0.8533333333333334
		}]
	},
	"660105": {
		"nik": "660105",
		"nama": "RADY MUHARADY P. U.",
		"band": "II",
		"posisi": "SENIOR STAFF II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8407407407407393,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9235553252137874,
			"nilai_komparatif_unit": 0.9267157716848714,
			"summary_team": 0.776470588235294
		}]
	},
	"660109": {
		"nik": "660109",
		"nama": "HARI SUWARTOYO",
		"band": "II",
		"posisi": "GM TA JAKARTA PUSAT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8592592592592608,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9465517241379293,
			"nilai_komparatif_unit": 0.9497908652858164,
			"summary_team": 0.8133333333333334
		}]
	},
	"660179": {
		"nik": "660179",
		"nama": "ROKHMAT SABRONI",
		"band": "I",
		"posisi": "SENIOR ADVISOR I",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8306666666666651,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.98027975235038,
			"nilai_komparatif_unit": 0.9866333238291126,
			"summary_team": 0.8142857142857142
		}]
	},
	"660187": {
		"nik": "660187",
		"nama": "Puguh Indaryono",
		"band": "I",
		"posisi": "DIREKTUR KOMERSIAL (CMO)",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8388888888888881,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9706717123935678,
			"nilai_komparatif_unit": 0.9769630104564824,
			"summary_team": 0.8142857142857144
		}]
	},
	"660192": {
		"nik": "660192",
		"nama": "AZHAR AFI",
		"band": "II",
		"posisi": "EVP SUBMARINE BUSINESS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8535714285714283,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0041841004184102,
			"nilai_komparatif_unit": 1.0076204620632867,
			"summary_team": 0.857142857142857
		}]
	},
	"660194": {
		"nik": "660194",
		"nama": "SURADI BAMBANG PUJO SUPENO",
		"band": "II",
		"posisi": "OSM REG KALIMANTAN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8694444444444462,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9814696485622982,
			"nilai_komparatif_unit": 0.984828280365495,
			"summary_team": 0.8533333333333333
		}]
	},
	"660195": {
		"nik": "660195",
		"nama": "DJADJA SUTEDJA",
		"band": "II",
		"posisi": "OSM PAYMENT COLLECTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.865882352941179,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9554100790513808,
			"nilai_komparatif_unit": 0.9586795338748664,
			"summary_team": 0.8272727272727274
		}]
	},
	"660231": {
		"nik": "660231",
		"nama": "LUKMAN ISKANDAR SOLEMAN",
		"band": "I",
		"posisi": "SENIOR ADVISOR I",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8377192982456159,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9549738219895267,
			"nilai_komparatif_unit": 0.9611633759650946,
			"summary_team": 0.8
		}]
	},
	"660238": {
		"nik": "660238",
		"nama": "KUKUH PRIBADIJANTO",
		"band": "I",
		"posisi": "SENIOR ADVISOR I",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.849999999999999,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9965397923875443,
			"nilai_komparatif_unit": 1.0029987514623948,
			"summary_team": 0.8470588235294116
		}]
	},
	"660240": {
		"nik": "660240",
		"nama": "MOCHAMMAD SULTHONUL ARIFIN, IR",
		"band": "I",
		"posisi": "PRESIDEN DIREKTUR",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8542222222222235,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0855169804181235,
			"nilai_komparatif_unit": 1.0925526349952257,
			"summary_team": 0.9272727272727274
		}]
	},
	"660241": {
		"nik": "660241",
		"nama": "DAHRIN EFFENDI S,IR",
		"band": "I",
		"posisi": "DEPUTY EGM SERVICE OPERATION",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8555555555555552,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0129870129870133,
			"nilai_komparatif_unit": 1.0195525728474606,
			"summary_team": 0.8666666666666666
		}]
	},
	"660250": {
		"nik": "660250",
		"nama": "IMAM SANTOSO, IR.MSC.",
		"band": "I",
		"posisi": "DIREKTUR BISNIS",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8266666666666668,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9677419354838707,
			"nilai_komparatif_unit": 0.9740142445316925,
			"summary_team": 0.7999999999999998
		}]
	},
	"660255": {
		"nik": "660255",
		"nama": "TOTOK WIDJANARKO, IR.",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8306666666666651,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8942903003898204,
			"nilai_komparatif_unit": 0.8973506006737665,
			"summary_team": 0.7428571428571428
		}]
	},
	"660288": {
		"nik": "660288",
		"nama": "IR WINTOKO M PRIBADI, MSI",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8535714285714283,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8683238985970958,
			"nilai_komparatif_unit": 0.8712953407253125,
			"summary_team": 0.7411764705882351
		}]
	},
	"660292": {
		"nik": "660292",
		"nama": "M. RIVAI TADJUDIN",
		"band": "II",
		"posisi": "OSM BUSINESS COMPLIANCE & SUPPORT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.865882352941179,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9659090909090883,
			"nilai_komparatif_unit": 0.9692144738075572,
			"summary_team": 0.8363636363636365
		}]
	},
	"660293": {
		"nik": "660293",
		"nama": "M. AMPERANDUS SIMANJUNTAK",
		"band": "I",
		"posisi": "VP INTEGRATED INFRA PROGRAM & BUDGET MGT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8377192982456159,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9549738219895271,
			"nilai_komparatif_unit": 0.9611633759650949,
			"summary_team": 0.8000000000000003
		}]
	},
	"660297": {
		"nik": "660297",
		"nama": "DANIEL TOVANIRENZA PESIRERON",
		"band": "II",
		"posisi": "AVP INFRASTRUCTURE CAPEX & PARTNERSHIP",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.865882352941179,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9869071146245033,
			"nilai_komparatif_unit": 0.990284353672939,
			"summary_team": 0.8545454545454547
		}]
	},
	"660302": {
		"nik": "660302",
		"nama": "Dr. DWI HERIYANTO B.",
		"band": "I",
		"posisi": "VP HC STRATEGIC MANAGEMENT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8542222222222235,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9142262524156372,
			"nilai_komparatif_unit": 0.9201517056635794,
			"summary_team": 0.7809523809523811
		}]
	},
	"660303": {
		"nik": "660303",
		"nama": "Dudy Effendi",
		"band": "I",
		"posisi": "DIREKTUR UTAMA",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8782608695652168,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9936993699369945,
			"nilai_komparatif_unit": 1.0001399191374942,
			"summary_team": 0.8727272727272728
		}]
	},
	"660330": {
		"nik": "660330",
		"nama": "ANAK AGUNG GEDE MAYUN WIRAYUDA",
		"band": "I",
		"posisi": "EGM DIVISI TV VIDEO",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8548148148148135,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9358752166377832,
			"nilai_komparatif_unit": 0.9419409851796976,
			"summary_team": 0.8
		}]
	},
	"660331": {
		"nik": "660331",
		"nama": "ARIEF HIDAYAT, IR.MSE.",
		"band": "I",
		"posisi": "VP WHOLESALE SOLUTION & CUSTOMER MGMT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8711111111111103,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8992346938775517,
			"nilai_komparatif_unit": 0.905062981047626,
			"summary_team": 0.7833333333333332
		}]
	},
	"660335": {
		"nik": "660335",
		"nama": "I KETUT BUDI UTAMA",
		"band": "I",
		"posisi": "DIREKTUR UTAMA",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8548148148148135,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0918544194107471,
			"nilai_komparatif_unit": 1.0989311493763139,
			"summary_team": 0.9333333333333335
		}]
	},
	"660336": {
		"nik": "660336",
		"nama": "ASEP KARTIWAN, IR.",
		"band": "I",
		"posisi": "OPERATION DIRECTOR",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8475490196078479,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.026489300173505,
			"nilai_komparatif_unit": 1.033142373569308,
			"summary_team": 0.87
		}]
	},
	"660338": {
		"nik": "660338",
		"nama": "SUPARWIYANTO, IR.",
		"band": "I",
		"posisi": "VP PLANNING & RESOURCE MANAGEMENT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8548148148148135,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9358752166377831,
			"nilai_komparatif_unit": 0.9419409851796975,
			"summary_team": 0.7999999999999999
		}]
	},
	"660339": {
		"nik": "660339",
		"nama": "ERIK ORBANDI, IR.MSC.",
		"band": "I",
		"posisi": "EVP DIVISI WHOLESALE SERVICE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8711111111111103,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0561224489795926,
			"nilai_komparatif_unit": 1.0629675862516799,
			"summary_team": 0.9199999999999998
		}]
	},
	"660340": {
		"nik": "660340",
		"nama": "ABDI MULYANTA GINTING, IR.MST.",
		"band": "I",
		"posisi": "EGM DIGITAL CONNECTIVITY SERVICE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8377192982456159,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9231413612565424,
			"nilai_komparatif_unit": 0.929124596766258,
			"summary_team": 0.7733333333333333
		}]
	},
	"660346": {
		"nik": "660346",
		"nama": "MIMI MARLINA",
		"band": "II",
		"posisi": "AVP CFU RESOURCE & CRO SUPPORT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.865882352941179,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9659090909090883,
			"nilai_komparatif_unit": 0.9692144738075572,
			"summary_team": 0.8363636363636365
		}]
	},
	"660348": {
		"nik": "660348",
		"nama": "MOHAMAD KHAMDAN",
		"band": "I",
		"posisi": "OVP CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8548148148148135,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9748700173310241,
			"nilai_komparatif_unit": 0.9811885262288516,
			"summary_team": 0.8333333333333334
		}]
	},
	"660353": {
		"nik": "660353",
		"nama": "SEMLY SAALINO",
		"band": "I",
		"posisi": "EVP TELKOM REGIONAL I",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8769230769230771,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0548245614035086,
			"nilai_komparatif_unit": 1.0616612865652097,
			"summary_team": 0.925
		}]
	},
	"660355": {
		"nik": "660355",
		"nama": "ASLI BRAHMANA, IR.MBT.",
		"band": "I",
		"posisi": "SENIOR ADVISOR I",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8571428571428563,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9833333333333342,
			"nilai_komparatif_unit": 0.9897066962491488,
			"summary_team": 0.8428571428571427
		}]
	},
	"660360": {
		"nik": "660360",
		"nama": "DARJOTO, IR",
		"band": "II",
		"posisi": "GM BASIC MANAGED SERVICE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8724637681159415,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9780730897009974,
			"nilai_komparatif_unit": 0.9814200983321177,
			"summary_team": 0.8533333333333334
		}]
	},
	"660364": {
		"nik": "660364",
		"nama": "OKI WIRANTO SARWOTO, DRS,MBA.",
		"band": "I",
		"posisi": "VP GCT CHANGE MANAGEMENT & COMMUNICATION",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8352941176470572,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9577464788732414,
			"nilai_komparatif_unit": 0.9639540035083627,
			"summary_team": 0.8
		}]
	},
	"660367": {
		"nik": "660367",
		"nama": "PARYANTO, IR.",
		"band": "II",
		"posisi": "OSM REG JAWA BARAT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8694444444444462,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0734824281150137,
			"nilai_komparatif_unit": 1.07715593164976,
			"summary_team": 0.9333333333333332
		}]
	},
	"660377": {
		"nik": "660377",
		"nama": "HARRY SUSENO HADISOEBROTO, IR.",
		"band": "I",
		"posisi": "SVP INTERNAL AUDIT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8757575757575752,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0113692535837873,
			"nilai_komparatif_unit": 1.017924328121061,
			"summary_team": 0.8857142857142859
		}]
	},
	"660403": {
		"nik": "660403",
		"nama": "SINDHU ARYANTO",
		"band": "I",
		"posisi": "DIREKTUR UTAMA YAYASAN PENDIDIKAN TELKOM",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8542222222222235,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.021663040393528,
			"nilai_komparatif_unit": 1.0282848329366832,
			"summary_team": 0.8727272727272728
		}]
	},
	"660404": {
		"nik": "660404",
		"nama": "M. TAKDIR, IR.",
		"band": "II",
		"posisi": "GM TA SULBAGSEL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8606060606060597,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9915492957746489,
			"nilai_komparatif_unit": 0.9949424205687822,
			"summary_team": 0.8533333333333333
		}]
	},
	"660407": {
		"nik": "660407",
		"nama": "AHMAD JUMADI",
		"band": "II",
		"posisi": "SM GENERAL AFFAIRS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8318840579710131,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9616724738675976,
			"nilai_komparatif_unit": 0.964963358878379,
			"summary_team": 0.8
		}]
	},
	"660412": {
		"nik": "660412",
		"nama": "HERMAWAN ALFIANTO, IR.MT.",
		"band": "II",
		"posisi": "VP PROGRAM MANAGEMENT OFFICE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8592592592592593,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9931034482758619,
			"nilai_komparatif_unit": 0.9965018914474156,
			"summary_team": 0.8533333333333333
		}]
	},
	"660418": {
		"nik": "660418",
		"nama": "NATAL IMAN GINTING",
		"band": "I",
		"posisi": "HEAD OF DIG VER ECOSYSTEM LOGISTICS",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8496296296296325,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0200523103748875,
			"nilai_komparatif_unit": 1.026663663155024,
			"summary_team": 0.8666666666666666
		}]
	},
	"660421": {
		"nik": "660421",
		"nama": "SUHARTONO,IR",
		"band": "I",
		"posisi": "VP SYNERGY",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.849999999999999,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9529411764705894,
			"nilai_komparatif_unit": 0.9591175560859152,
			"summary_team": 0.81
		}]
	},
	"660427": {
		"nik": "660427",
		"nama": "R.M. SOEGIHARDJITO",
		"band": "I",
		"posisi": "DEPUTY EVP CARRIER SERVICE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8222222222222223,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0378378378378377,
			"nilai_komparatif_unit": 1.0445644654869612,
			"summary_team": 0.8533333333333333
		}]
	},
	"660431": {
		"nik": "660431",
		"nama": "JOKO SUPRIYANTO",
		"band": "II",
		"posisi": "ENGINE TEAM - EGM KOMITEL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8592592592592593,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.997536945812808,
			"nilai_komparatif_unit": 1.0009505606056632,
			"summary_team": 0.8571428571428573
		}]
	},
	"660433": {
		"nik": "660433",
		"nama": "ALIP PRIYONO, IR",
		"band": "I",
		"posisi": "VP INFRASTRUCTURE & OPERATION AUDIT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.9066666666666663,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.044117647058824,
			"nilai_komparatif_unit": 1.050884976575616,
			"summary_team": 0.9466666666666667
		}]
	},
	"660443": {
		"nik": "660443",
		"nama": "LASMA LYDIA PINARSINTA",
		"band": "II",
		"posisi": "SM BUSINESS PARTNERSHIP AND SUPPORT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8173913043478244,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0565764023210855,
			"nilai_komparatif_unit": 1.0601920527006372,
			"summary_team": 0.8636363636363638
		}]
	},
	"660652": {
		"nik": "660652",
		"nama": "MUCHAMAD NOOR HIDAYAT",
		"band": "I",
		"posisi": "SGM SSO FINANCE CENTER",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8715151515151515,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9332406119610572,
			"nilai_komparatif_unit": 0.9392893046130619,
			"summary_team": 0.8133333333333335
		}]
	},
	"670025": {
		"nik": "670025",
		"nama": "SNATAKA PRIBADI",
		"band": "I",
		"posisi": "DEPUTY EGM PRODUCT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8490196078431361,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9422632794457287,
			"nilai_komparatif_unit": 0.9483704514885964,
			"summary_team": 0.8
		}]
	},
	"670039": {
		"nik": "670039",
		"nama": "JUDI ACHMADI, IR.",
		"band": "I",
		"posisi": "SENIOR STAFF I",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9695251594613711,
			"nilai_komparatif_unit": 0.9758090262721443,
			"summary_team": 0.8
		}]
	},
	"670042": {
		"nik": "670042",
		"nama": "ERA KAMALI NASUTION",
		"band": "I",
		"posisi": "DIREKTUR PLANNING & PERFORMANCE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8535714285714283,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9665271966527201,
			"nilai_komparatif_unit": 0.9727916325092686,
			"summary_team": 0.8250000000000002
		}]
	},
	"670057": {
		"nik": "670057",
		"nama": "HENRY INSETIJADIE",
		"band": "I",
		"posisi": "DEPUTY SGM FINANCE & ASSET MGT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8740740740740739,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9315980629539956,
			"nilai_komparatif_unit": 0.9376361096118353,
			"summary_team": 0.8142857142857145
		}]
	},
	"670075": {
		"nik": "670075",
		"nama": "Saul Rudy Nikson",
		"band": "I",
		"posisi": "SVP INTERNAL AUDIT & RISK ADVISORY",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.811764705882353,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0471014492753625,
			"nilai_komparatif_unit": 1.0538881179661157,
			"summary_team": 0.8500000000000002
		}]
	},
	"670084": {
		"nik": "670084",
		"nama": "NATO JOKO PRAYOTO ",
		"band": "II",
		"posisi": "OVP SUBMARINE OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.859649122807017,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9664050235478813,
			"nilai_komparatif_unit": 0.9697121035493991,
			"summary_team": 0.8307692307692307
		}]
	},
	"670111": {
		"nik": "670111",
		"nama": "IMAM SANTOSO",
		"band": "I",
		"posisi": "VP PLANNING & DEVELOPMENT AUDIT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.9066666666666663,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9558823529411768,
			"nilai_komparatif_unit": 0.9620777954565499,
			"summary_team": 0.8666666666666667
		}]
	},
	"670132": {
		"nik": "670132",
		"nama": "SOENDOJOADI, R.",
		"band": "I",
		"posisi": "DEPUTY EGM ACCESS NW PLANNING&DEPLOYMEN",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8714285714285706,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000009,
			"nilai_komparatif_unit": 1.0064813860160835,
			"summary_team": 0.8714285714285713
		}]
	},
	"670136": {
		"nik": "670136",
		"nama": "ENDANG SUSILOWATI",
		"band": "I",
		"posisi": "SR PRINCIPAL EXPERT CONS SERV ASSURANCE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.865882352941179,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9816576086956491,
			"nilai_komparatif_unit": 0.9880201105932301,
			"summary_team": 0.8499999999999999
		}]
	},
	"670140": {
		"nik": "670140",
		"nama": "M. DJUFFRIANTO",
		"band": "I",
		"posisi": "DIREKTUR OPERATION",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8186046511627898,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0587121212121224,
			"nilai_komparatif_unit": 1.065574043149604,
			"summary_team": 0.8666666666666667
		}]
	},
	"670157": {
		"nik": "670157",
		"nama": "FIRMANSYAH, IR",
		"band": "I",
		"posisi": "DEPUTY EVP MARKETING",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8338797814207654,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9593709043250324,
			"nilai_komparatif_unit": 0.9655889574885611,
			"summary_team": 0.7999999999999999
		}]
	},
	"670159": {
		"nik": "670159",
		"nama": "ARIF PRABOWO",
		"band": "I",
		"posisi": "DIREKTUR UTAMA",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0105168269230718,
			"nilai_komparatif_unit": 1.017066376554107,
			"summary_team": 0.842857142857143
		}]
	},
	"670162": {
		"nik": "670162",
		"nama": "JAMES RYAN LATUPEIRISSA, IR.",
		"band": "II",
		"posisi": "OSM DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8519999999999984,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.981647460520702,
			"nilai_komparatif_unit": 0.9850067008041501,
			"summary_team": 0.8363636363636365
		}]
	},
	"670169": {
		"nik": "670169",
		"nama": "RIJANTO UTOMO, RADEN, IR",
		"band": "I",
		"posisi": "EVP TELKOM REGIONAL VI",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.851515151515151,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0462633451957302,
			"nilai_komparatif_unit": 1.0530445818104215,
			"summary_team": 0.8909090909090909
		}]
	},
	"670175": {
		"nik": "670175",
		"nama": "SUHARTONO",
		"band": "I",
		"posisi": "SENIOR STAFF I",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9034211713162776,
			"nilai_komparatif_unit": 0.9092765926626799,
			"summary_team": 0.7454545454545455
		}]
	},
	"670178": {
		"nik": "670178",
		"nama": "SRI AKHADAH",
		"band": "II",
		"posisi": "OSM CARRIER SERVICE DELIVERY & ASSURANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8318840579710131,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0257839721254374,
			"nilai_komparatif_unit": 1.029294249470271,
			"summary_team": 0.8533333333333334
		}]
	},
	"670195": {
		"nik": "670195",
		"nama": "IWAN ADJI PURDIANTO, IR",
		"band": "II",
		"posisi": "GM CONSTRUCTION 2",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.881481481481481,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9831932773109249,
			"nilai_komparatif_unit": 0.9865578074466279,
			"summary_team": 0.8666666666666666
		}]
	},
	"670196": {
		"nik": "670196",
		"nama": "GUNAWAN A",
		"band": "II",
		"posisi": "KAPRO SERVICE QUALITY DELIVERY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8694444444444462,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9354632587859406,
			"nilai_komparatif_unit": 0.9386644547233626,
			"summary_team": 0.8133333333333334
		}]
	},
	"670203": {
		"nik": "670203",
		"nama": "JOKO IMANANTO",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8519999999999984,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9650495565988544,
			"nilai_komparatif_unit": 0.9683519981335486,
			"summary_team": 0.8222222222222224
		}]
	},
	"670204": {
		"nik": "670204",
		"nama": "EDY SUSILO",
		"band": "II",
		"posisi": "DIREKTUR UTAMA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9266666666666666,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0575539568345325,
			"nilai_komparatif_unit": 1.0611729524481248,
			"summary_team": 0.98
		}]
	},
	"670208": {
		"nik": "670208",
		"nama": "FADJRUL FALAH, IR.",
		"band": "II",
		"posisi": "AVP PACKAGE & PRICING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8765957446808497,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9852162400706108,
			"nilai_komparatif_unit": 0.9885876928728184,
			"summary_team": 0.8636363636363639
		}]
	},
	"670216": {
		"nik": "670216",
		"nama": "AMBAR KUSPARDIANTO",
		"band": "I",
		"posisi": "VP ENTERPRISE BUSINESS GOVERNANCE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8558558558558567,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.973684210526315,
			"nilai_komparatif_unit": 0.9799950337525007,
			"summary_team": 0.8333333333333335
		}]
	},
	"670219": {
		"nik": "670219",
		"nama": "ARIS DWI TJAHJANTO",
		"band": "I",
		"posisi": "DIREKTUR CONSTRUCTION",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8186046511627898,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9772727272727284,
			"nilai_komparatif_unit": 0.9836068090611727,
			"summary_team": 0.8
		}]
	},
	"670227": {
		"nik": "670227",
		"nama": "MOHAMMAD FIRDAUS",
		"band": "I",
		"posisi": "SENIOR ADVISOR I",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9034211713162776,
			"nilai_komparatif_unit": 0.9092765926626799,
			"summary_team": 0.7454545454545455
		}]
	},
	"670229": {
		"nik": "670229",
		"nama": "PRAMUDI ARTANTO, IR. MT",
		"band": "II",
		"posisi": "SENIOR ADVISOR PROJECT SUPERVISION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8186046511627898,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8551136363636376,
			"nilai_komparatif_unit": 0.8580398724002234,
			"summary_team": 0.7000000000000002
		}]
	},
	"670230": {
		"nik": "670230",
		"nama": "ROEDI GOERNIDA R",
		"band": "II",
		"posisi": "GM TA CITAKA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8291666666666665,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.013065326633166,
			"nilai_komparatif_unit": 1.0165320802202282,
			"summary_team": 0.84
		}]
	},
	"670235": {
		"nik": "670235",
		"nama": "AGUNG SUTANTO ADI SUSETYO, R.",
		"band": "I",
		"posisi": "DIREKTUR HIGHER EDUCATION",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8901960784313723,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9735682819383263,
			"nilai_komparatif_unit": 0.979878353786583,
			"summary_team": 0.8666666666666667
		}]
	},
	"670236": {
		"nik": "670236",
		"nama": "MACHSUS KUSUMA APRIYONO",
		"band": "I",
		"posisi": "DIREKTUR HCM & STRATEGY",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8186046511627898,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9772727272727284,
			"nilai_komparatif_unit": 0.9836068090611727,
			"summary_team": 0.8
		}]
	},
	"670244": {
		"nik": "670244",
		"nama": "HARIS HASAN ASYARI, IR. M Komp",
		"band": "I",
		"posisi": "DEPUTY EGM IT OPERATION",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8733333333333327,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0877862595419856,
			"nilai_komparatif_unit": 1.0948366221930679,
			"summary_team": 0.9500000000000002
		}]
	},
	"670254": {
		"nik": "670254",
		"nama": "YUSRON HARIYADI",
		"band": "I",
		"posisi": "DIREKTUR MARKETING & SALES",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.9403508771929822,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0133889376646186,
			"nilai_komparatif_unit": 1.0199571025540508,
			"summary_team": 0.9529411764705884
		}]
	},
	"670255": {
		"nik": "670255",
		"nama": "T.HERCULES E.H",
		"band": "I",
		"posisi": "VP SUBSIDIARIES FINANCIAL CONTROL",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8715151515151515,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9944367176634215,
			"nilai_komparatif_unit": 1.0008820458991643,
			"summary_team": 0.8666666666666667
		}]
	},
	"670258": {
		"nik": "670258",
		"nama": "ROHMAT NUGROHO",
		"band": "II",
		"posisi": "AVP PERFORMANCE MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8306666666666651,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0112359550561816,
			"nilai_komparatif_unit": 1.014696448454182,
			"summary_team": 0.8399999999999999
		}]
	},
	"670261": {
		"nik": "670261",
		"nama": "HADI PRAKOSA",
		"band": "I",
		"posisi": "DEPUTY EVP MANAGED SERVICE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8424242424242417,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9928057553956845,
			"nilai_komparatif_unit": 0.9992405127353924,
			"summary_team": 0.8363636363636365
		}]
	},
	"670264": {
		"nik": "670264",
		"nama": "GATOT PRAMUHARTANTO, IR",
		"band": "II",
		"posisi": "OSM REG JAWA TENGAH",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8694444444444462,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9968051118210842,
			"nilai_komparatif_unit": 1.0002162222462059,
			"summary_team": 0.8666666666666667
		}]
	},
	"670265": {
		"nik": "670265",
		"nama": "DEVI ALZY, IR",
		"band": "I",
		"posisi": "TASK FORCE LEADER",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8757575757575752,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9134948096885818,
			"nilai_komparatif_unit": 0.9194155221738614,
			"summary_team": 0.7999999999999998
		}]
	},
	"670267": {
		"nik": "670267",
		"nama": "BAMBANG HARYASENA, IR",
		"band": "I",
		"posisi": "DEPUTY EVP INFRASTRUCTURE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.9333333333333333,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999999,
			"nilai_komparatif_unit": 1.0064813860160824,
			"summary_team": 0.9333333333333332
		}]
	},
	"670283": {
		"nik": "670283",
		"nama": "JAJAT SUTARJAT",
		"band": "I",
		"posisi": "VP RISK STRATEGY & GOVERNANCE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.877777777777777,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0329113924050641,
			"nilai_komparatif_unit": 1.0396060898596506,
			"summary_team": 0.9066666666666666
		}]
	},
	"670290": {
		"nik": "670290",
		"nama": "A. HARTONO",
		"band": "I",
		"posisi": "DIREKTUR FINANCE & BUSINESS SUPPORT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8851851851851847,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0328750747160793,
			"nilai_komparatif_unit": 1.0395695367817044,
			"summary_team": 0.9142857142857143
		}]
	},
	"670291": {
		"nik": "670291",
		"nama": "A. NUGROHO SUPRIHANTO",
		"band": "II",
		"posisi": "OVP SERVICE DELIVERY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8694444444444462,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9661341853035124,
			"nilai_komparatif_unit": 0.9694403384847842,
			"summary_team": 0.84
		}]
	},
	"670297": {
		"nik": "670297",
		"nama": "AKINTYASAKTI BAYU KATON",
		"band": "II",
		"posisi": "VP INTERNAL AUDIT & RISK MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8947368421052623,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0058823529411776,
			"nilai_komparatif_unit": 1.0093245260800123,
			"summary_team": 0.9000000000000001
		}]
	},
	"670302": {
		"nik": "670302",
		"nama": "HIKMATULLAH INSAN PURNAMA",
		"band": "I",
		"posisi": "HEAD OF DIG VER ECOSYSTEM AGRICULTURE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8496296296296325,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9415867480383578,
			"nilai_komparatif_unit": 0.9476895352200223,
			"summary_team": 0.8
		}]
	},
	"670307": {
		"nik": "670307",
		"nama": "IR.Pramasaleh Haryo Utomo, MT.",
		"band": "I",
		"posisi": "DIREKTUR UTAMA",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8558558558558567,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9347368421052623,
			"nilai_komparatif_unit": 0.9407952324024006,
			"summary_team": 0.8
		}]
	},
	"670453": {
		"nik": "670453",
		"nama": "WAHJUDAJANTO UTAMA",
		"band": "II",
		"posisi": "VP INTERNAL AUDIT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8379487179487183,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0442166462668294,
			"nilai_komparatif_unit": 1.047790001023868,
			"summary_team": 0.875
		}]
	},
	"670614": {
		"nik": "670614",
		"nama": "JUSTI ARIESTHIAWATI, Dra",
		"band": "I",
		"posisi": "SGM ASSESSMENT CENTER INDONESIA",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8542222222222235,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0340790842871992,
			"nilai_komparatif_unit": 1.0407813500036218,
			"summary_team": 0.8833333333333333
		}]
	},
	"670617": {
		"nik": "670617",
		"nama": "TOTO SUGIHARTO",
		"band": "I",
		"posisi": "DEPUTY EGM BACKBONE&CORE NW PLAN&DEP",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8714285714285706,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0491803278688534,
			"nilai_komparatif_unit": 1.0559804705742515,
			"summary_team": 0.9142857142857143
		}]
	},
	"670620": {
		"nik": "670620",
		"nama": "AGUS TATO, DRS.DIP.TEFL",
		"band": "I",
		"posisi": "SR PRINCIPAL EXPERT HC PROGRAM CONTROLLE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8279569892473108,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9236058059587483,
			"nilai_komparatif_unit": 0.929592051713862,
			"summary_team": 0.7647058823529411
		}]
	},
	"680020": {
		"nik": "680020",
		"nama": "AGUS YUDHA BASUKI",
		"band": "I",
		"posisi": "EVP TELKOM REGIONAL VII",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.851515151515151,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.973055414336554,
			"nilai_komparatif_unit": 0.9793621620919083,
			"summary_team": 0.8285714285714287
		}]
	},
	"680021": {
		"nik": "680021",
		"nama": "ARIF RUDIANA",
		"band": "I",
		"posisi": "DEPUTY SGM TELKOM CORPORATE UNIV CENTER",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8411764705882361,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0303030303030294,
			"nilai_komparatif_unit": 1.0369808219559629,
			"summary_team": 0.8666666666666667
		}]
	},
	"680023": {
		"nik": "680023",
		"nama": "AGUSTIAWAN SUMARNO",
		"band": "II",
		"posisi": "GM TA BANDUNG",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8291666666666665,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.013065326633166,
			"nilai_komparatif_unit": 1.0165320802202282,
			"summary_team": 0.8399999999999999
		}]
	},
	"680034": {
		"nik": "680034",
		"nama": "ACE",
		"band": "I",
		"posisi": "DIREKTUR FINANCE, HC & ADMINISTRATION",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8782608695652168,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9315931593159325,
			"nilai_komparatif_unit": 0.937631174191401,
			"summary_team": 0.8181818181818183
		}]
	},
	"680036": {
		"nik": "680036",
		"nama": "WIDI NUGROHO",
		"band": "I",
		"posisi": "VP DIGITAL BUSINESS PERFORMANCE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8571428571428563,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0266666666666677,
			"nilai_komparatif_unit": 1.0333208896431791,
			"summary_team": 0.88
		}]
	},
	"680037": {
		"nik": "680037",
		"nama": "DIDIK SUKASDI, IR. MT.",
		"band": "I",
		"posisi": "TASK FORCE LEADER",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8352941176470572,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9976525821596263,
			"nilai_komparatif_unit": 1.0041187536545444,
			"summary_team": 0.8333333333333333
		}]
	},
	"680058": {
		"nik": "680058",
		"nama": "NANANG HENDARNO",
		"band": "I",
		"posisi": "DIREKTUR TECHNOLOGY",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8711111111111103,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.00295381310419,
			"nilai_komparatif_unit": 1.0094543439232202,
			"summary_team": 0.8736842105263157
		}]
	},
	"680062": {
		"nik": "680062",
		"nama": "FIRMAN HIDAYAT",
		"band": "I",
		"posisi": "SENIOR ADVISOR I",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8947368421052623,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9686274509803934,
			"nilai_komparatif_unit": 0.9749054993959714,
			"summary_team": 0.8666666666666669
		}]
	},
	"680066": {
		"nik": "680066",
		"nama": "WIDJAJANTO BUDI SULISTIJO",
		"band": "I",
		"posisi": "EVP GLOBAL BUSINESS OPERATION",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8637037037037028,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9384309831181736,
			"nilai_komparatif_unit": 0.9445133165692144,
			"summary_team": 0.8105263157894735
		}]
	},
	"680070": {
		"nik": "680070",
		"nama": "YUSUF WIBISONO, IR. MSMOT",
		"band": "I",
		"posisi": "VP STRATEGIC INVESTMENT DIGITAL TELCO",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.849999999999999,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0117647058823545,
			"nilai_komparatif_unit": 1.0183223434986262,
			"summary_team": 0.8600000000000002
		}]
	},
	"680076": {
		"nik": "680076",
		"nama": "AGUS WIDJAJANTO",
		"band": "I",
		"posisi": "VP INTEGRATED & FINANCIAL AUDIT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.9066666666666663,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882356,
			"nilai_komparatif_unit": 0.9472765986033722,
			"summary_team": 0.8533333333333333
		}]
	},
	"680085": {
		"nik": "680085",
		"nama": "SINUNG WIBOWO",
		"band": "I",
		"posisi": "DEPUTY EVP INFRASTRUCTURE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8349726775956329,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9820680628272199,
			"nilai_komparatif_unit": 0.9884332250364696,
			"summary_team": 0.8200000000000001
		}]
	},
	"680086": {
		"nik": "680086",
		"nama": "FADJAR RACHMANTO",
		"band": "II",
		"posisi": "EGM CONSTRUCTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8185185185185184,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8726567550096964,
			"nilai_komparatif_unit": 0.8756430243609121,
			"summary_team": 0.7142857142857144
		}]
	},
	"680088": {
		"nik": "680088",
		"nama": "PERDANA YULIAN DARTA",
		"band": "II",
		"posisi": "VP SURVEY, DRAWING & INVENTORY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.7490196078431375,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9434554973821987,
			"nilai_komparatif_unit": 0.9466840431075305,
			"summary_team": 0.7066666666666667
		}]
	},
	"680140": {
		"nik": "680140",
		"nama": "ADDY KURNIA KOMARA",
		"band": "II",
		"posisi": "VP NETWORK & IT SOLUTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9313725490196086,
			"nilai_komparatif_unit": 0.9345597463703815,
			"summary_team": 0.7916666666666666
		}]
	},
	"680157": {
		"nik": "680157",
		"nama": "JODDY HERNADY,M.S.E.E,IR.",
		"band": "I",
		"posisi": "HEAD OF DIG VER ECOSYSTEM HEALTH",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8496296296296325,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.941586748038358,
			"nilai_komparatif_unit": 0.9476895352200224,
			"summary_team": 0.8000000000000002
		}]
	},
	"680160": {
		"nik": "680160",
		"nama": "YACOB TAMBUNAN, IR",
		"band": "II",
		"posisi": "GM OPERATIONS & MAINTENANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8407407407407405,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9356828193832601,
			"nilai_komparatif_unit": 0.9388847666665907,
			"summary_team": 0.7866666666666666
		}]
	},
	"680164": {
		"nik": "680164",
		"nama": "ARYA SATRIANANTA",
		"band": "II",
		"posisi": "AVP GO TO MARKET & DIGITAL CHANNEL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8765957446808497,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9229920564872038,
			"nilai_komparatif_unit": 0.9261505754282194,
			"summary_team": 0.8090909090909093
		}]
	},
	"680178": {
		"nik": "680178",
		"nama": "SUHERMAN",
		"band": "I",
		"posisi": "SENIOR ADVISOR I",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.865882352941179,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1356431159420255,
			"nilai_komparatif_unit": 1.1430036573529525,
			"summary_team": 0.9833333333333332
		}]
	},
	"680179": {
		"nik": "680179",
		"nama": "TORKIS ROPINDA SIHOMBING",
		"band": "I",
		"posisi": "VP CORPORATE STRATEGIC PLANNING",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.849999999999999,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9764705882352954,
			"nilai_komparatif_unit": 0.9827994710509996,
			"summary_team": 0.8300000000000001
		}]
	},
	"680352": {
		"nik": "680352",
		"nama": "IWAN RUSDARMONO",
		"band": "I",
		"posisi": "OVP CONSUMER ASSURANCE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9925925925925925,
			"nilai_komparatif_unit": 0.9990259683418893,
			"summary_team": 0.8933333333333333
		}]
	},
	"680355": {
		"nik": "680355",
		"nama": "SUJITO",
		"band": "I",
		"posisi": "EVP TELKOM REGIONAL III",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8769230769230771,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9578947368421051,
			"nilai_komparatif_unit": 0.9641032223943526,
			"summary_team": 0.8400000000000001
		}]
	},
	"680358": {
		"nik": "680358",
		"nama": "PRAYUDI UTOMO",
		"band": "II",
		"posisi": "SR PRINCIPAL EXPERT MKT&SALES PARTNRSHIP",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.865882352941179,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9659090909090883,
			"nilai_komparatif_unit": 0.9692144738075572,
			"summary_team": 0.8363636363636365
		}]
	},
	"680359": {
		"nik": "680359",
		"nama": "DJATMIKO",
		"band": "I",
		"posisi": "EVP TELKOM REGIONAL IV",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.851515151515151,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9730554143365535,
			"nilai_komparatif_unit": 0.9793621620919079,
			"summary_team": 0.8285714285714284
		}]
	},
	"680585": {
		"nik": "680585",
		"nama": "SIHMIRMO ADI, ST",
		"band": "I",
		"posisi": "EGM INFORMATION TECHNOLOGY",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8377192982456159,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9549738219895267,
			"nilai_komparatif_unit": 0.9611633759650946,
			"summary_team": 0.8
		}]
	},
	"680595": {
		"nik": "680595",
		"nama": "FITRIANSYAH NASUTION",
		"band": "II",
		"posisi": "AVP SECRETARIATE DIT CONSUMER SERVICE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8548148148148135,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.042224673073895,
			"nilai_komparatif_unit": 1.045791211212074,
			"summary_team": 0.8909090909090911
		}]
	},
	"690290": {
		"nik": "690290",
		"nama": "SUJARWO",
		"band": "II",
		"posisi": "GM TA JAKARTA SELATAN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8592592592592608,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.024137931034481,
			"nilai_komparatif_unit": 1.0276425755551457,
			"summary_team": 0.88
		}]
	},
	"690595": {
		"nik": "690595",
		"nama": "BUDI SUSILA",
		"band": "II",
		"posisi": "OSM REG JAKARTA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8694444444444462,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9661341853035124,
			"nilai_komparatif_unit": 0.9694403384847842,
			"summary_team": 0.84
		}]
	},
	"690602": {
		"nik": "690602",
		"nama": "Notje Rosanti",
		"band": "I",
		"posisi": "DIREKTUR KEUANGAN & INVESTASI",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8914285714285706,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9722222222222234,
			"nilai_komparatif_unit": 0.9785235697378593,
			"summary_team": 0.8666666666666669
		}]
	},
	"700002": {
		"nik": "700002",
		"nama": "SAIFUL HIDAJAT",
		"band": "I",
		"posisi": "EVP DIGITAL BUSINESS & TECHNOLOGY",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8571428571428563,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.011111111111112,
			"nilai_komparatif_unit": 1.0176645125273733,
			"summary_team": 0.8666666666666666
		}]
	},
	"700003": {
		"nik": "700003",
		"nama": "HESTI NUGRAHANI",
		"band": "I",
		"posisi": "SENIOR ADVISOR I",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9695251594613711,
			"nilai_komparatif_unit": 0.9758090262721443,
			"summary_team": 0.8
		}]
	},
	"700022": {
		"nik": "700022",
		"nama": "NOVITA ELFIANI, SE",
		"band": "II",
		"posisi": "VP FINANCE OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.942028985507246,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0190769230769232,
			"nilai_komparatif_unit": 1.0225642486083482,
			"summary_team": 0.9599999999999999
		}]
	},
	"700647": {
		"nik": "700647",
		"nama": "JULIADI NUGRAHA",
		"band": "II",
		"posisi": "VP HUMAN CAPITAL MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8592592592592588,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.008620689655173,
			"nilai_komparatif_unit": 1.0120722335012822,
			"summary_team": 0.8666666666666667
		}]
	},
	"700648": {
		"nik": "700648",
		"nama": "REINALDY CL. SJAMSUDDIN",
		"band": "II",
		"posisi": "VP DIGITAL & CX PERFORMANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8407407407407396,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0158352184783919,
			"nilai_komparatif_unit": 1.0193114507557595,
			"summary_team": 0.8540540540540542
		}]
	},
	"700668": {
		"nik": "700668",
		"nama": "MUHAMMAD AHSAN, M.ENG",
		"band": "II",
		"posisi": "VP SUBSIDIARY PARENTING & PERFORMANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8379487179487183,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9308445532435738,
			"nilai_komparatif_unit": 0.9340299437698484,
			"summary_team": 0.7800000000000001
		}]
	},
	"710366": {
		"nik": "710366",
		"nama": "Rizal Ahmad Fauzi",
		"band": "I",
		"posisi": "DIREKTUR KEUANGAN & ADMINISTRASI (CFO)",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8388888888888881,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0728476821192066,
			"nilai_komparatif_unit": 1.0798012220834805,
			"summary_team": 0.9000000000000002
		}]
	},
	"710367": {
		"nik": "710367",
		"nama": "SHALDI YUSUF",
		"band": "II",
		"posisi": "VP HC STRATEGY MGT & INDUSTRIAL RELATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.852777777777777,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9850162866449519,
			"nilai_komparatif_unit": 0.9883870551978446,
			"summary_team": 0.8399999999999999
		}]
	},
	"710370": {
		"nik": "710370",
		"nama": "WIBOWO PUDJIANTORO",
		"band": "II",
		"posisi": "GM TA SEMARANG",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9142857142857135,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0937500000000009,
			"nilai_komparatif_unit": 1.0974928600467966,
			"summary_team": 0.9999999999999999
		}]
	},
	"710373": {
		"nik": "710373",
		"nama": "KRISTIAN WAHJUONO ARI",
		"band": "II",
		"posisi": "VP CARRIER ENTERPRISE SALES ENABLER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8313725490196077,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9622641509433965,
			"nilai_komparatif_unit": 0.9655570606988523,
			"summary_team": 0.8
		}]
	},
	"710377": {
		"nik": "710377",
		"nama": "ABDUL HADI",
		"band": "I",
		"posisi": "DIREKTUR UTAMA",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8558558558558567,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9848120300751869,
			"nilai_komparatif_unit": 0.9911949769953862,
			"summary_team": 0.8428571428571427
		}]
	},
	"710382": {
		"nik": "710382",
		"nama": "RATIH PRATAMA KUSUMASTUTI",
		"band": "II",
		"posisi": "AVP CX CONSUMER FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8765957446808497,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9333627537511049,
			"nilai_komparatif_unit": 0.9365567616689858,
			"summary_team": 0.8181818181818183
		}]
	},
	"710383": {
		"nik": "710383",
		"nama": "Tri Priyo Anggoro",
		"band": "I",
		"posisi": "KETUA YAYASAN KESEHATAN PEGAWAI TELKOM",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8542222222222235,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9365244536940674,
			"nilai_komparatif_unit": 0.9425944301919595,
			"summary_team": 0.8000000000000002
		}]
	},
	"710385": {
		"nik": "710385",
		"nama": "I GUSTI NGURAH PUTRA BANUAJI",
		"band": "II",
		"posisi": "GM IT & DIGITALIZATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8407407407407405,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0149779735682822,
			"nilai_komparatif_unit": 1.0184512723163017,
			"summary_team": 0.8533333333333333
		}]
	},
	"710395": {
		"nik": "710395",
		"nama": "YANTITO SIMANJUNTAK",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8407407407407393,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9095620627105482,
			"nilai_komparatif_unit": 0.91267462362904,
			"summary_team": 0.7647058823529411
		}]
	},
	"710402": {
		"nik": "710402",
		"nama": "KARNO BUDIONO",
		"band": "II",
		"posisi": "DIREKTUR SHARED SERVICE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8535714285714283,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9958158995815903,
			"nilai_komparatif_unit": 0.9992236248794262,
			"summary_team": 0.8500000000000001
		}]
	},
	"710408": {
		"nik": "710408",
		"nama": "HERU ADRYANA",
		"band": "I",
		"posisi": "FINANCE & BUSINESS SUPPORT DIRECTOR",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8562770562770574,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.970210780119778,
			"nilai_komparatif_unit": 0.9764990907026989,
			"summary_team": 0.8307692307692309
		}]
	},
	"710411": {
		"nik": "710411",
		"nama": "SUDIN",
		"band": "II",
		"posisi": "VP NETWORK & IT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8333333333333329,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.002352941176471,
			"nilai_komparatif_unit": 1.0057830365148184,
			"summary_team": 0.8352941176470586
		}]
	},
	"710427": {
		"nik": "710427",
		"nama": "MUKHNI",
		"band": "I",
		"posisi": "DEPUTY EGM INFRASTRUCTURE O & M",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8555555555555552,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649354,
			"nilai_komparatif_unit": 0.9411254518591944,
			"summary_team": 0.7999999999999999
		}]
	},
	"710438": {
		"nik": "710438",
		"nama": "ADMIRAL DASRIN, ST",
		"band": "I",
		"posisi": "VP PERFORMANCE & GOVERNANCE MANAGEMENT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8377192982456159,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9815008726003469,
			"nilai_komparatif_unit": 0.9878623586307916,
			"summary_team": 0.8222222222222222
		}]
	},
	"710498": {
		"nik": "710498",
		"nama": "PRASABRI PESTI, ST",
		"band": "I",
		"posisi": "HEAD OF DIG VER ECOSYSTEM EDUCATION",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8496296296296325,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9778016229629101,
			"nilai_komparatif_unit": 0.9841391327284847,
			"summary_team": 0.8307692307692308
		}]
	},
	"710502": {
		"nik": "710502",
		"nama": "DEWA MADE WIDARTHA BONHA",
		"band": "I",
		"posisi": "SENIOR ADVISOR I",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8377192982456159,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9549738219895267,
			"nilai_komparatif_unit": 0.9611633759650946,
			"summary_team": 0.8
		}]
	},
	"710518": {
		"nik": "710518",
		"nama": "SHELTER MANGATAS LUMBAN TOBING",
		"band": "II",
		"posisi": "GM TA JAKARTA TIMUR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8592592592592608,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0551724137931016,
			"nilai_komparatif_unit": 1.0587832596628772,
			"summary_team": 0.9066666666666666
		}]
	},
	"710520": {
		"nik": "710520",
		"nama": "SIGIT SUBIANTORO",
		"band": "II",
		"posisi": "GM TA BOGOR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8592592592592608,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9620689655172396,
			"nilai_komparatif_unit": 0.9653612073396822,
			"summary_team": 0.8266666666666667
		}]
	},
	"710530": {
		"nik": "710530",
		"nama": "ADRIANSJAH NASUTION, BET.MSIS.",
		"band": "I",
		"posisi": "SENIOR PRINCIPAL EXPERT INFORMATION TECH",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8377192982456159,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0634935744883365,
			"nilai_komparatif_unit": 1.0703864868702189,
			"summary_team": 0.8909090909090909
		}]
	},
	"715478": {
		"nik": "715478",
		"nama": "ACHMAD ALIYADIN",
		"band": "I",
		"posisi": "VP FINANCIAL & PROCUREMENT POLICY",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8715151515151515,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0062051995292607,
			"nilai_komparatif_unit": 1.0127268038387993,
			"summary_team": 0.8769230769230769
		}]
	},
	"720065": {
		"nik": "720065",
		"nama": "NIZAR",
		"band": "I",
		"posisi": "VP HC DEVELOPMENT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8542222222222235,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9105098855358987,
			"nilai_komparatif_unit": 0.9164112515755161,
			"summary_team": 0.7777777777777778
		}]
	},
	"720075": {
		"nik": "720075",
		"nama": "AGUS WINARNO",
		"band": "I",
		"posisi": "DIREKTUR UTAMA",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8558558558558567,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0282105263157884,
			"nilai_komparatif_unit": 1.0348747556426403,
			"summary_team": 0.8799999999999999
		}]
	},
	"720082": {
		"nik": "720082",
		"nama": "SUHARYOTO",
		"band": "I",
		"posisi": "EVP GLOBAL NETWORK OPERATION",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.833333333333332,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9931034482758637,
			"nilai_komparatif_unit": 0.9995401350780423,
			"summary_team": 0.8275862068965518
		}]
	},
	"720087": {
		"nik": "720087",
		"nama": "OKTADIASIH MUNINGGAR",
		"band": "II",
		"posisi": "OSM MANAGED SERVICE PLANNING & DEVT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8724637681159415,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9780730897009972,
			"nilai_komparatif_unit": 0.9814200983321175,
			"summary_team": 0.8533333333333333
		}]
	},
	"720088": {
		"nik": "720088",
		"nama": "AHMED YASSER",
		"band": "I",
		"posisi": "VP INTEGRATED PORTFOLIO MANAGEMENT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.849999999999999,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000013,
			"nilai_komparatif_unit": 1.006481386016084,
			"summary_team": 0.8500000000000001
		}]
	},
	"720096": {
		"nik": "720096",
		"nama": "STANISLAUS SUSATYO",
		"band": "I",
		"posisi": "DIREKTUR SUPPLY & COMMERCE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8186046511627898,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0098484848484859,
			"nilai_komparatif_unit": 1.016393702696545,
			"summary_team": 0.8266666666666667
		}]
	},
	"720117": {
		"nik": "720117",
		"nama": "MUHAMAD SUHAENDRY",
		"band": "I",
		"posisi": "DIREKTUR FINANCE & BUSINESS SUPPORT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.9403508771929822,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0258999122036878,
			"nilai_komparatif_unit": 1.032549165548545,
			"summary_team": 0.9647058823529413
		}]
	},
	"720118": {
		"nik": "720118",
		"nama": "DEDY MARDHIANTO",
		"band": "I",
		"posisi": "EVP DIVISI GOVERNMENT SERVICE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8558558558558567,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9814736842105254,
			"nilai_komparatif_unit": 0.9878349940225205,
			"summary_team": 0.84
		}]
	},
	"720128": {
		"nik": "720128",
		"nama": "TEDDY HARTADI",
		"band": "I",
		"posisi": "DEPUTY EVP MARKETING",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8482758620689653,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0020325203252036,
			"nilai_komparatif_unit": 1.0085270798900994,
			"summary_team": 0.8500000000000001
		}]
	},
	"720132": {
		"nik": "720132",
		"nama": "SADIMAN EKO KRISWANTO",
		"band": "II",
		"posisi": "VP DIGITAL PLATFORM OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8407407407407396,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9258245029170156,
			"nilai_komparatif_unit": 0.9289927146128443,
			"summary_team": 0.7783783783783786
		}]
	},
	"720159": {
		"nik": "720159",
		"nama": "PUJO PRAMONO",
		"band": "I",
		"posisi": "VP CORPORATE COMMUNICATION",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8533333333333335,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9960937499999999,
			"nilai_komparatif_unit": 1.0025498181019572,
			"summary_team": 0.8500000000000001
		}]
	},
	"720162": {
		"nik": "720162",
		"nama": "MULYANTO",
		"band": "II",
		"posisi": "VP LEGAL & CORPORATE SECRETARY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8947368421052623,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1176470588235308,
			"nilai_komparatif_unit": 1.1214716956444581,
			"summary_team": 1.0000000000000002
		}]
	},
	"720189": {
		"nik": "720189",
		"nama": "MOHAMMAD FARIED ABIDIN",
		"band": "I",
		"posisi": "DEPUTY SGM BILLING & REV ASSURANCE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8740740740740739,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8825665859564168,
			"nilai_komparatif_unit": 0.8882868406848965,
			"summary_team": 0.7714285714285716
		}]
	},
	"720191": {
		"nik": "720191",
		"nama": "KURNIA MAREZA BACHRIL",
		"band": "II",
		"posisi": "CHIEF EXECUTIVE OFFICER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9934640522875827,
			"nilai_komparatif_unit": 0.9968637294617404,
			"summary_team": 0.8444444444444446
		}]
	},
	"720204": {
		"nik": "720204",
		"nama": "MAKHDOR ROSADI",
		"band": "II",
		"posisi": "EGM SERVICE OPERATION & IT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8185185185185184,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0297349709114418,
			"nilai_komparatif_unit": 1.0332587687458763,
			"summary_team": 0.8428571428571431
		}]
	},
	"720210": {
		"nik": "720210",
		"nama": "ALBERTUS HUGO",
		"band": "II",
		"posisi": "GM SALES EXTERNAL TG",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8515151515151511,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0479058308239808,
			"nilai_komparatif_unit": 1.051491810130948,
			"summary_team": 0.8923076923076924
		}]
	},
	"720222": {
		"nik": "720222",
		"nama": "DWI PRATOMO JUNIARTO, ST",
		"band": "I",
		"posisi": "DEPUTY EVP MARKETING",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8349726775956329,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9581151832460685,
			"nilai_komparatif_unit": 0.9643250975965559,
			"summary_team": 0.8000000000000003
		}]
	},
	"720224": {
		"nik": "720224",
		"nama": "MARIA GORETI TRI WIJAYANTI, ST",
		"band": "II",
		"posisi": "GM SALES (TG)",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8476190476190478,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9585674157303371,
			"nilai_komparatif_unit": 0.9618476750971918,
			"summary_team": 0.8125000000000002
		}]
	},
	"720225": {
		"nik": "720225",
		"nama": "MOHAMMAD SALSABIL",
		"band": "I",
		"posisi": "EVP DIVISI ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8558558558558567,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0256140350877183,
			"nilai_komparatif_unit": 1.0322614355526338,
			"summary_team": 0.8777777777777778
		}]
	},
	"720231": {
		"nik": "720231",
		"nama": "YULFI INDRA",
		"band": "II",
		"posisi": "GM TA BEKASI",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8592592592592608,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1172413793103428,
			"nilai_komparatif_unit": 1.1210646278783407,
			"summary_team": 0.96
		}]
	},
	"720235": {
		"nik": "720235",
		"nama": "PRI HANDOKO",
		"band": "II",
		"posisi": "VP CORPORATE SECRETARY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8379487179487183,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0314303199860113,
			"nilai_komparatif_unit": 1.0349599193786783,
			"summary_team": 0.8642857142857144
		}]
	},
	"720254": {
		"nik": "720254",
		"nama": "GUNADI DWI HANTORO",
		"band": "II",
		"posisi": "VP QUALITY & INFRASTRUCTURE DEVELOPMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8476190476190472,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9619706136560074,
			"nilai_komparatif_unit": 0.9652625189141054,
			"summary_team": 0.8153846153846154
		}]
	},
	"720267": {
		"nik": "720267",
		"nama": "RIZAL AKBAR",
		"band": "I",
		"posisi": "VP NETWORK/IT STRA, TECH & ARCHITECTURE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8377192982456159,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9430366492146579,
			"nilai_komparatif_unit": 0.9491488337655312,
			"summary_team": 0.7900000000000003
		}]
	},
	"720304": {
		"nik": "720304",
		"nama": "NANANG ASNADI",
		"band": "II",
		"posisi": "OSM MANAGED SERVICE QUALITY & OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8724637681159415,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0086378737541535,
			"nilai_komparatif_unit": 1.0120894764049964,
			"summary_team": 0.88
		}]
	},
	"720318": {
		"nik": "720318",
		"nama": "CHAIRUDIN MIRZA",
		"band": "I",
		"posisi": "VP REGULATORY MANAGEMENT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.877777777777777,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9569620253164567,
			"nilai_komparatif_unit": 0.9631644656052648,
			"summary_team": 0.8400000000000001
		}]
	},
	"720330": {
		"nik": "720330",
		"nama": "GAEANRI SITUMORANG",
		"band": "I",
		"posisi": "DEPUTY EVP INFRASTRUCTURE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8271929824561421,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0110864745011066,
			"nilai_komparatif_unit": 1.0176397162379882,
			"summary_team": 0.8363636363636365
		}]
	},
	"720348": {
		"nik": "720348",
		"nama": "AKHMAD LUDFY",
		"band": "I",
		"posisi": "EGM SERVICE OPERATION",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8377192982456159,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0218219895287937,
			"nilai_komparatif_unit": 1.0284448122826513,
			"summary_team": 0.856
		}]
	},
	"720373": {
		"nik": "720373",
		"nama": "ANDRI YUNIANTO",
		"band": "I",
		"posisi": "DEPUTY EVP DIVISI GOVERNMENT SERVICE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8634920634920623,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9264705882352955,
			"nilai_komparatif_unit": 0.9324754017501955,
			"summary_team": 0.8
		}]
	},
	"720382": {
		"nik": "720382",
		"nama": "EKO SANTOSO",
		"band": "II",
		"posisi": "DIREKTUR BISNIS & ADMINISTRASI",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8444444444444446,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857144,
			"nilai_komparatif_unit": 0.9675855419188077,
			"summary_team": 0.8142857142857145
		}]
	},
	"720395": {
		"nik": "720395",
		"nama": "JEFRIZAL LEONARD RAMBE",
		"band": "II",
		"posisi": "AVP SALES & PARTNERSHIP MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8407407407407393,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1101321585903103,
			"nilai_komparatif_unit": 1.1139310790959567,
			"summary_team": 0.9333333333333333
		}]
	},
	"720402": {
		"nik": "720402",
		"nama": "EFFY ABADI",
		"band": "II",
		"posisi": "VP HC SERVICES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8476190476190472,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9910112359550567,
			"nilai_komparatif_unit": 0.9944025194850973,
			"summary_team": 0.84
		}]
	},
	"720417": {
		"nik": "720417",
		"nama": "IVAN RINALDY",
		"band": "II",
		"posisi": "VP PRODUCT/SERVICE INNOVATION AND DEV",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8637037037037028,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8683533447684401,
			"nilai_komparatif_unit": 0.8713248876627351,
			"summary_team": 0.75
		}]
	},
	"720418": {
		"nik": "720418",
		"nama": "MIETA YUDIANDARI",
		"band": "II",
		"posisi": "OSM WHOLESALE & DIGITAL SOLUTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8318840579710131,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9937282229965175,
			"nilai_komparatif_unit": 0.997128804174325,
			"summary_team": 0.8266666666666668
		}]
	},
	"720434": {
		"nik": "720434",
		"nama": "FIRMAN NAAMSYAH",
		"band": "II",
		"posisi": "GM VAS, CONTENT, & SERVICE PROVIDER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8318840579710131,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9616724738675976,
			"nilai_komparatif_unit": 0.964963358878379,
			"summary_team": 0.8
		}]
	},
	"720438": {
		"nik": "720438",
		"nama": "FAUZAN NURUL FADLIL",
		"band": "II",
		"posisi": "GM ADVANCED MANAGED SERVICE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8724637681159415,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.005111167901866,
			"nilai_komparatif_unit": 1.0085507020119717,
			"summary_team": 0.8769230769230768
		}]
	},
	"720446": {
		"nik": "720446",
		"nama": "EDDY SOFRYANO,M.M",
		"band": "I",
		"posisi": "WAKIL DIREKTUR UTAMA",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.805555555555556,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1255172413793095,
			"nilai_komparatif_unit": 1.1328121530884452,
			"summary_team": 0.9066666666666666
		}]
	},
	"720465": {
		"nik": "720465",
		"nama": "HENRY CHRISTIADI",
		"band": "I",
		"posisi": "PRESIDENT DIRECTOR",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8558558558558567,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0905263157894727,
			"nilai_komparatif_unit": 1.0975944378028006,
			"summary_team": 0.9333333333333333
		}]
	},
	"720479": {
		"nik": "720479",
		"nama": "MUHAMMAD ROFIK, M.M",
		"band": "I",
		"posisi": "VP STRATEGY PLANNING & PERFORMANCE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8711111111111103,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0905612244897969,
			"nilai_komparatif_unit": 1.097629572759887,
			"summary_team": 0.95
		}]
	},
	"720480": {
		"nik": "720480",
		"nama": "RUDY BERLIANDY",
		"band": "I",
		"posisi": "AUDITOR UTAMA",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9693045858579591,
			"nilai_komparatif_unit": 0.9755870230460636,
			"summary_team": 0.8461538461538463
		}]
	},
	"720487": {
		"nik": "720487",
		"nama": "Irphan Wijaya",
		"band": "I",
		"posisi": "DIREKTUR STRATEGIC PLANNING & PORTFOLIO",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.811764705882353,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.932712215320911,
			"nilai_komparatif_unit": 0.9387574832303214,
			"summary_team": 0.7571428571428572
		}]
	},
	"720505": {
		"nik": "720505",
		"nama": "TIA INDRAWATI, M.ENG",
		"band": "II",
		"posisi": "AVP PRODUCT & SERVICE DIG CONNECTIVITY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8199999999999992,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9918699186991881,
			"nilai_komparatif_unit": 0.9952641406789071,
			"summary_team": 0.8133333333333334
		}]
	},
	"720506": {
		"nik": "720506",
		"nama": "KOMANG BUDI ARYASA",
		"band": "I",
		"posisi": "DEPUTY EVP DIGITAL BUSINESS BUILDER",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8496296296296325,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9102005231037458,
			"nilai_komparatif_unit": 0.9160998840460215,
			"summary_team": 0.7733333333333333
		}]
	},
	"720515": {
		"nik": "720515",
		"nama": "TRI GUNADI",
		"band": "I",
		"posisi": "SENIOR ADVISOR I",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0356291476064647,
			"nilai_komparatif_unit": 1.0423414598816088,
			"summary_team": 0.8545454545454546
		}]
	},
	"720557": {
		"nik": "720557",
		"nama": "MEYER SILABAN",
		"band": "I",
		"posisi": "SENIOR EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7271438695960282,
			"nilai_komparatif_unit": 0.7318567697041081,
			"summary_team": 0.5999999999999999
		}]
	},
	"720562": {
		"nik": "720562",
		"nama": "JUNIAN SIDHARTA",
		"band": "I",
		"posisi": "VP LEGAL & COMPLIANCE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.877777777777777,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0025316455696212,
			"nilai_komparatif_unit": 1.0090294401578963,
			"summary_team": 0.88
		}]
	},
	"720571": {
		"nik": "720571",
		"nama": "RETNONINGSIH, M.ENG.",
		"band": "II",
		"posisi": "AVP  CUSTOMER EXPERIENCE MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8407407407407393,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9991189427312791,
			"nilai_komparatif_unit": 1.002537971186361,
			"summary_team": 0.84
		}]
	},
	"720572": {
		"nik": "720572",
		"nama": "TEUKU MUDA NANTA, M.ENG.",
		"band": "I",
		"posisi": "EVP TELKOM REGIONAL II",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9859409495667748,
			"summary_team": 0.8
		}]
	},
	"720596": {
		"nik": "720596",
		"nama": "AMBARI, ST. MSCS.",
		"band": "I",
		"posisi": "DEPUTY EVP INFRASTRUCTURE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.865608465608466,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9242053789731046,
			"nilai_komparatif_unit": 0.9301955107923692,
			"summary_team": 0.7999999999999999
		}]
	},
	"720597": {
		"nik": "720597",
		"nama": "AAD AZHARI, ST. MSIS",
		"band": "II",
		"posisi": "VP DIGITAL & SERVICE READINESS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8407407407407396,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0398993077407193,
			"nilai_komparatif_unit": 1.043457888377641,
			"summary_team": 0.8742857142857147
		}]
	},
	"720604": {
		"nik": "720604",
		"nama": "M. ROSADI",
		"band": "II",
		"posisi": "AVP PARENTING & INVESTMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8306666666666651,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9630818619582682,
			"nilai_komparatif_unit": 0.9663775699563639,
			"summary_team": 0.7999999999999999
		}]
	},
	"730037": {
		"nik": "730037",
		"nama": "DWI SULISTIANI",
		"band": "I",
		"posisi": "PRESIDENT DIRECTOR",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8558558558558567,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0126315789473677,
			"nilai_komparatif_unit": 1.0191948351026008,
			"summary_team": 0.8666666666666668
		}]
	},
	"730038": {
		"nik": "730038",
		"nama": "Dr. RIZA A. N. RUKMANA",
		"band": "I",
		"posisi": "VP DIGITAL BUSINESS STRATEGY & GOV",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8571428571428563,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.94888888888889,
			"nilai_komparatif_unit": 0.9550390040641505,
			"summary_team": 0.8133333333333335
		}]
	},
	"730041": {
		"nik": "730041",
		"nama": "PONTJO SUHARWONO",
		"band": "I",
		"posisi": "EVP TELKOM REGIONAL V",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.851515151515151,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9608540925266911,
			"nilai_komparatif_unit": 0.9670817588054893,
			"summary_team": 0.8181818181818182
		}]
	},
	"730056": {
		"nik": "730056",
		"nama": "AZIZ SIDQI",
		"band": "I",
		"posisi": "DEPUTY EVP DIVISI ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0180014174344396,
			"nilai_komparatif_unit": 1.0245994775857514,
			"summary_team": 0.8400000000000001
		}]
	},
	"730057": {
		"nik": "730057",
		"nama": "IAN SIGIT KURNIAWAN",
		"band": "I",
		"posisi": "DIREKTUR KEUANGAN",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8947368421052623,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9090196078431383,
			"nilai_komparatif_unit": 0.9149113148177577,
			"summary_team": 0.8133333333333335
		}]
	},
	"730068": {
		"nik": "730068",
		"nama": "ARIEF SETIYAWAN,ST",
		"band": "II",
		"posisi": "OVP MANAGED SERVICE TERITORY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8694444444444462,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9507987220447265,
			"nilai_komparatif_unit": 0.9540523966040734,
			"summary_team": 0.8266666666666667
		}]
	},
	"730069": {
		"nik": "730069",
		"nama": "HENDRI BUSTAMAM, ST, MM",
		"band": "II",
		"posisi": "VP BILLING & COLLECTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8515151515151511,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0298384889132224,
			"nilai_komparatif_unit": 1.033362640990759,
			"summary_team": 0.8769230769230769
		}]
	},
	"730070": {
		"nik": "730070",
		"nama": "SITI RAKHMAWATI",
		"band": "I",
		"posisi": "DIREKTUR INVESTASI",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.826666666666666,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9392789373814051,
			"nilai_komparatif_unit": 0.9453667667513499,
			"summary_team": 0.7764705882352942
		}]
	},
	"730081": {
		"nik": "730081",
		"nama": "THEODORUS ARDI HARTOKO, ST.",
		"band": "I",
		"posisi": "DIREKTUR UTAMA",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8533333333333335,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9588068181818182,
			"nilai_komparatif_unit": 0.9650212152853065,
			"summary_team": 0.8181818181818183
		}]
	},
	"730082": {
		"nik": "730082",
		"nama": "NUR YUSUF",
		"band": "II",
		"posisi": "OSM QOS & DATA MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.865882352941179,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9344120553359658,
			"nilai_komparatif_unit": 0.9376096540094847,
			"summary_team": 0.8090909090909092
		}]
	},
	"730088": {
		"nik": "730088",
		"nama": "ELFRI JUFRI",
		"band": "II",
		"posisi": "GM CONSTRUCTION TG",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.881481481481481,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0285714285714291,
			"nilai_komparatif_unit": 1.0320912447133954,
			"summary_team": 0.9066666666666666
		}]
	},
	"730091": {
		"nik": "730091",
		"nama": "RISTIANTO, S.T, M.M",
		"band": "II",
		"posisi": "AVP GOVERNANCE & COMPLIANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8306666666666651,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.059390048154095,
			"nilai_komparatif_unit": 1.0630153269520004,
			"summary_team": 0.88
		}]
	},
	"730100": {
		"nik": "730100",
		"nama": "RADEN AGUS UTORO",
		"band": "II",
		"posisi": "OSM IPTV OPERATION & ASSURANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8270833333333325,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9892374627891013,
			"nilai_komparatif_unit": 0.9926226763902596,
			"summary_team": 0.8181818181818183
		}]
	},
	"730104": {
		"nik": "730104",
		"nama": "GATOT DWIWAHJUDI",
		"band": "II",
		"posisi": "VP CORPORATE STRATEGY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8186046511627898,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.997632575757577,
			"nilai_komparatif_unit": 1.0010465178002603,
			"summary_team": 0.8166666666666668
		}]
	},
	"730114": {
		"nik": "730114",
		"nama": "EDIE KURNIAWAN",
		"band": "I",
		"posisi": "VP MARKETING MANAGEMENT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8548148148148135,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9618717504332772,
			"nilai_komparatif_unit": 0.9681060125458003,
			"summary_team": 0.8222222222222223
		}]
	},
	"730135": {
		"nik": "730135",
		"nama": "DIDIK HARIANTO",
		"band": "II",
		"posisi": "VP SUBMARINE COMMERCIAL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.859649122807017,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9716686674669873,
			"nilai_komparatif_unit": 0.974993759886814,
			"summary_team": 0.8352941176470587
		}]
	},
	"730137": {
		"nik": "730137",
		"nama": "LINA ANDRINI",
		"band": "II",
		"posisi": "VP HUMAN CAPITAL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.898412698412698,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0091872791519438,
			"nilai_komparatif_unit": 1.0126407618919413,
			"summary_team": 0.9066666666666665
		}]
	},
	"730138": {
		"nik": "730138",
		"nama": "DADANG DARMAWAN, ST",
		"band": "II",
		"posisi": "VP STRATEGIC PLANNING & EVALUATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8535714285714283,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9684797768479778,
			"nilai_komparatif_unit": 0.9717939567454809,
			"summary_team": 0.8266666666666665
		}]
	},
	"730141": {
		"nik": "730141",
		"nama": "DEDDY IMAWAN PRIYAMBADA, ST",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8535714285714283,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9684797768479778,
			"nilai_komparatif_unit": 0.9717939567454809,
			"summary_team": 0.8266666666666665
		}]
	},
	"730155": {
		"nik": "730155",
		"nama": "MARKUS FERRIEK KHRISTIANTO",
		"band": "II",
		"posisi": "VP CORPORATE STRATEGY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8379487179487183,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9638922888616888,
			"nilai_komparatif_unit": 0.9671907701758784,
			"summary_team": 0.8076923076923077
		}]
	},
	"730171": {
		"nik": "730171",
		"nama": "Roby Roediyanto",
		"band": "I",
		"posisi": "DIREKTUR CORPORATE FINANCE & SHARED SERV",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.811764705882353,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8975155279503106,
			"nilai_komparatif_unit": 0.9033326725423848,
			"summary_team": 0.7285714285714288
		}]
	},
	"730180": {
		"nik": "730180",
		"nama": "SYAIFUDIN, MBA",
		"band": "I",
		"posisi": "EVP DIVISI BUSINESS SERVICE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8558558558558567,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0282105263157886,
			"nilai_komparatif_unit": 1.0348747556426405,
			"summary_team": 0.88
		}]
	},
	"730192": {
		"nik": "730192",
		"nama": "MOHAMAD SAHLAN SYAUQI",
		"band": "I",
		"posisi": "SENIOR ADVISOR I",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9695251594613711,
			"nilai_komparatif_unit": 0.9758090262721443,
			"summary_team": 0.8
		}]
	},
	"730193": {
		"nik": "730193",
		"nama": "INDRAWAN DITAPRADANA",
		"band": "I",
		"posisi": "SVP CORPORATE SECRETARY",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8757575757575752,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0200692041522499,
			"nilai_komparatif_unit": 1.0266806664274788,
			"summary_team": 0.8933333333333333
		}]
	},
	"730197": {
		"nik": "730197",
		"nama": "ERFIZAL FIKRI YUSMANSYAH,",
		"band": "I",
		"posisi": "TRANSFORMATION PROGRAM LEADER",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8352941176470572,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9577464788732414,
			"nilai_komparatif_unit": 0.9639540035083627,
			"summary_team": 0.8
		}]
	},
	"730198": {
		"nik": "730198",
		"nama": "BHIMO ARYANTO",
		"band": "I",
		"posisi": "PRESIDENT DIRECTOR",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8558558558558567,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9134928229665065,
			"nilai_komparatif_unit": 0.9194135225750734,
			"summary_team": 0.781818181818182
		}]
	},
	"730199": {
		"nik": "730199",
		"nama": "FADCHI NIZAR AMIZADE",
		"band": "I",
		"posisi": "DIREKTUR KEPESERTAAN",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.826666666666666,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000009,
			"nilai_komparatif_unit": 1.0064813860160835,
			"summary_team": 0.8266666666666667
		}]
	},
	"730215": {
		"nik": "730215",
		"nama": "HARRY RAMDAN",
		"band": "II",
		"posisi": "GM AREA OFFICE JABODETABEB & JABAR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8185185185185184,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.994828700711054,
			"nilai_komparatif_unit": 0.99823304777144,
			"summary_team": 0.8142857142857145
		}]
	},
	"730219": {
		"nik": "730219",
		"nama": "RAHMAD FITRIYANTO",
		"band": "II",
		"posisi": "VP DIG & SERVICE PLANNING & DEVELOPMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.833333333333332,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0338461538461559,
			"nilai_komparatif_unit": 1.0373840203273117,
			"summary_team": 0.8615384615384619
		}]
	},
	"730230": {
		"nik": "730230",
		"nama": "YOGO SANTOSO, ST",
		"band": "II",
		"posisi": "GM TA YOGYAKARTA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9142857142857135,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625000000000008,
			"nilai_komparatif_unit": 0.9657937168411811,
			"summary_team": 0.88
		}]
	},
	"730232": {
		"nik": "730232",
		"nama": "BENY SOLIFIN HUTASUHUT, ST, MM",
		"band": "II",
		"posisi": "OSM CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.865882352941179,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9554100790513809,
			"nilai_komparatif_unit": 0.9586795338748665,
			"summary_team": 0.8272727272727275
		}]
	},
	"730234": {
		"nik": "730234",
		"nama": "JOHANSYAH BENHAR PUTRA,ST, M.T",
		"band": "II",
		"posisi": "AVP PRDCT & SER DIG PLATFRM & DIG SRVC",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8199999999999992,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9756097560975621,
			"nilai_komparatif_unit": 0.978948335094007,
			"summary_team": 0.8
		}]
	},
	"730235": {
		"nik": "730235",
		"nama": "SATRIYO TEJO UTOMO, ST",
		"band": "II",
		"posisi": "OVP MANAGED SERVICE NON TERITORY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8694444444444462,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.042811501597442,
			"nilai_komparatif_unit": 1.0463800478883385,
			"summary_team": 0.9066666666666666
		}]
	},
	"730242": {
		"nik": "730242",
		"nama": "NOOR MAHMUDAH MOORCY, ST",
		"band": "II",
		"posisi": "GM CARRIER SERVICE OLO",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8318840579710131,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.785365853658538,
			"nilai_komparatif_unit": 0.7880534097506762,
			"summary_team": 0.6533333333333333
		}]
	},
	"730265": {
		"nik": "730265",
		"nama": "KHARISMA",
		"band": "II",
		"posisi": "DIREKTUR COMMERCIAL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8637037037037028,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.075104141141878,
			"nilai_komparatif_unit": 1.0787831942491004,
			"summary_team": 0.9285714285714284
		}]
	},
	"730277": {
		"nik": "730277",
		"nama": "MUHAMAD NI'MATUDDIN, ST",
		"band": "II",
		"posisi": "OSM OTT OPERATION & ASSURANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8270833333333325,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.011220517517748,
			"nilai_komparatif_unit": 1.0146809580878209,
			"summary_team": 0.8363636363636365
		}]
	},
	"730283": {
		"nik": "730283",
		"nama": "ERWIN JAYA DIWANGSA",
		"band": "II",
		"posisi": "VP PROCUREMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8592592592592588,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.008620689655173,
			"nilai_komparatif_unit": 1.0120722335012822,
			"summary_team": 0.8666666666666667
		}]
	},
	"730290": {
		"nik": "730290",
		"nama": "SENDY ADITYA KAMESVARA,M.M",
		"band": "I",
		"posisi": "SGM HC BUSINESS PARTNER CENTER",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8542222222222235,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0184703433922981,
			"nilai_komparatif_unit": 1.0250714428337557,
			"summary_team": 0.8700000000000001
		}]
	},
	"730292": {
		"nik": "730292",
		"nama": "ALEX ISKANDAR YAPIS, ST",
		"band": "II",
		"posisi": "GM MARKETING & PARNERSHIP",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8515151515151511,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0021352313167264,
			"nilai_komparatif_unit": 1.0055645816424699,
			"summary_team": 0.8533333333333333
		}]
	},
	"730306": {
		"nik": "730306",
		"nama": "ARIEF WIBISONO",
		"band": "II",
		"posisi": "OSM REG JATIM BALNUS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8694444444444462,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9968051118210842,
			"nilai_komparatif_unit": 1.0002162222462059,
			"summary_team": 0.8666666666666667
		}]
	},
	"730307": {
		"nik": "730307",
		"nama": "ANTON TIMUR",
		"band": "II",
		"posisi": "EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8424242424242417,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9636055861193406,
			"nilai_komparatif_unit": 0.9669030863242825,
			"summary_team": 0.8117647058823529
		}]
	},
	"730337": {
		"nik": "730337",
		"nama": "RAKHMAD TUNGGAL AFIFUDDIN",
		"band": "I",
		"posisi": "PRESIDENT DIRECTOR",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8715151515151515,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0369340132900633,
			"nilai_komparatif_unit": 1.043654782903402,
			"summary_team": 0.9037037037037037
		}]
	},
	"730344": {
		"nik": "730344",
		"nama": "BENTARTO BUDISETIAWAN",
		"band": "II",
		"posisi": "VP VOICE & MOBILITY SALES REGION II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8549019607843136,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9968089349820501,
			"nilai_komparatif_unit": 1.0002200584901948,
			"summary_team": 0.852173913043478
		}]
	},
	"730354": {
		"nik": "730354",
		"nama": "RENI YUSTIANI",
		"band": "II",
		"posisi": "OSM APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8519999999999984,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9709773794280856,
			"nilai_komparatif_unit": 0.9743001062301919,
			"summary_team": 0.8272727272727274
		}]
	},
	"730356": {
		"nik": "730356",
		"nama": "ERY PUNTA HENDRASWARA",
		"band": "I",
		"posisi": "DEPUTY EVP DIG TECH & PLATFORM BUSINESS",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8496296296296325,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8945074106364399,
			"nilai_komparatif_unit": 0.9003050584590212,
			"summary_team": 0.76
		}]
	},
	"730362": {
		"nik": "730362",
		"nama": "BENNY HENRICUS SAMOSIR",
		"band": "II",
		"posisi": "OSM REG SUMATERA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8694444444444462,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9968051118210842,
			"nilai_komparatif_unit": 1.0002162222462059,
			"summary_team": 0.8666666666666667
		}]
	},
	"730371": {
		"nik": "730371",
		"nama": "I GEDE WAHYU DANYNDRA",
		"band": "II",
		"posisi": "OSM REG KTI",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8694444444444462,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9201277955271547,
			"nilai_komparatif_unit": 0.9232765128426517,
			"summary_team": 0.8
		}]
	},
	"730372": {
		"nik": "730372",
		"nama": "WAHYUDI HANDRIYANTO",
		"band": "II",
		"posisi": "VP HUMAN CAPITAL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8279999999999987,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9732878658709877,
			"nilai_komparatif_unit": 0.9766184992581416,
			"summary_team": 0.8058823529411766
		}]
	},
	"730378": {
		"nik": "730378",
		"nama": "LILIK SUGIHARTO, ST",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8306666666666651,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.014675533134604,
			"nilai_komparatif_unit": 1.0181477969183121,
			"summary_team": 0.8428571428571427
		}]
	},
	"730379": {
		"nik": "730379",
		"nama": "I WAYAN MULIASTIKA",
		"band": "II",
		"posisi": "VP DIGITAL CONNECTIVITY OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8407407407407396,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9515418502202664,
			"nilai_komparatif_unit": 0.9547980677965348,
			"summary_team": 0.8000000000000006
		}]
	},
	"730380": {
		"nik": "730380",
		"nama": "GIGIN HERGIANTO",
		"band": "II",
		"posisi": "VP FINANCIAL OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.898412698412698,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9821242984826444,
			"nilai_komparatif_unit": 0.9854851705263271,
			"summary_team": 0.8823529411764706
		}]
	},
	"730398": {
		"nik": "730398",
		"nama": "HARIYO SANTOSO",
		"band": "II",
		"posisi": "SM INFRASTRUCTURE PLANNING & SERVICE DEV",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8270833333333325,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.978245935424778,
			"nilai_komparatif_unit": 0.981593535541479,
			"summary_team": 0.8090909090909093
		}]
	},
	"730418": {
		"nik": "730418",
		"nama": "EDI SISWANTO, M.ENG",
		"band": "II",
		"posisi": "VP CUSTOMER SERVICE OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.833333333333332,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0183783783783802,
			"nilai_komparatif_unit": 1.0218633134594801,
			"summary_team": 0.8486486486486489
		}]
	},
	"730420": {
		"nik": "730420",
		"nama": "ARIYANTO",
		"band": "I",
		"posisi": "SENIOR ADVISOR I",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8377192982456159,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0080279232111673,
			"nilai_komparatif_unit": 1.014561341296489,
			"summary_team": 0.8444444444444446
		}]
	},
	"730421": {
		"nik": "730421",
		"nama": "BUDI SATRIA DHARMA PURBA",
		"band": "I",
		"posisi": "DIREKTUR UTAMA",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8711111111111103,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0271213748657368,
			"nilai_komparatif_unit": 1.033778544981611,
			"summary_team": 0.8947368421052632
		}]
	},
	"730424": {
		"nik": "730424",
		"nama": "MUHAMMAD SUBHAN ISWAHYUDI",
		"band": "I",
		"posisi": "COORD GROUP OF LEARNING EXPT,INNOV,&RES",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8411764705882361,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9726636999364265,
			"nilai_komparatif_unit": 0.9789679088395457,
			"summary_team": 0.8181818181818183
		}]
	},
	"730427": {
		"nik": "730427",
		"nama": "SRI SAFITRI",
		"band": "I",
		"posisi": "DEPUTY EVP CX & DIGITIZATION",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8496296296296325,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9729729729729697,
			"nilai_komparatif_unit": 0.979279186394023,
			"summary_team": 0.8266666666666667
		}]
	},
	"730441": {
		"nik": "730441",
		"nama": "BUNGZIO LEONY, MBA.",
		"band": "II",
		"posisi": "AVP SOLUTION MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8407407407407393,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9515418502202659,
			"nilai_komparatif_unit": 0.9547980677965343,
			"summary_team": 0.8
		}]
	},
	"730446": {
		"nik": "730446",
		"nama": "AAN HELMY ANSORI",
		"band": "II",
		"posisi": "VP IT PLATFORM OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.833333333333332,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9257142857142875,
			"nilai_komparatif_unit": 0.9288821202420572,
			"summary_team": 0.7714285714285717
		}]
	},
	"730457": {
		"nik": "730457",
		"nama": "UMAR SYAHID",
		"band": "I",
		"posisi": "VP IT AUDIT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.9066666666666663,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0294117647058827,
			"nilai_komparatif_unit": 1.0360837797224383,
			"summary_team": 0.9333333333333332
		}]
	},
	"730466": {
		"nik": "730466",
		"nama": "TEGUH INDRAYATNO",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8379487179487183,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9308445532435737,
			"nilai_komparatif_unit": 0.9340299437698483,
			"summary_team": 0.78
		}]
	},
	"730477": {
		"nik": "730477",
		"nama": "KETUT DHARMA YOGA SANDI",
		"band": "II",
		"posisi": "VP SALES NON-TELKOM GROUP",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8535714285714283,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9913099452848408,
			"nilai_komparatif_unit": 0.9947022510111933,
			"summary_team": 0.846153846153846
		}]
	},
	"730491": {
		"nik": "730491",
		"nama": "ANIS GUNAEFI",
		"band": "II",
		"posisi": "AVP SALES & CHANNEL PARTNERSHIP",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8765957446808497,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9541041482789073,
			"nilai_komparatif_unit": 0.9573691341505189,
			"summary_team": 0.8363636363636365
		}]
	},
	"730500": {
		"nik": "730500",
		"nama": "ALI SYAFIQ",
		"band": "II",
		"posisi": "VP BILLING COLLECTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.942028985507246,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0332307692307696,
			"nilai_komparatif_unit": 1.03676652983902,
			"summary_team": 0.9733333333333333
		}]
	},
	"730513": {
		"nik": "730513",
		"nama": "MUHAMAT ARIF WIBOWO",
		"band": "I",
		"posisi": "DEPUTY EVP INFRASTRUCTURE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8338797814207654,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9593709043250324,
			"nilai_komparatif_unit": 0.9655889574885611,
			"summary_team": 0.7999999999999999
		}]
	},
	"730519": {
		"nik": "730519",
		"nama": "DEDDY AGUS PRAMUDIA",
		"band": "II",
		"posisi": "DIREKTUR OPERASI & PEMBANGUNAN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9266666666666666,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0020554984583763,
			"nilai_komparatif_unit": 1.0054845759348123,
			"summary_team": 0.9285714285714287
		}]
	},
	"730522": {
		"nik": "730522",
		"nama": "ABDUL RAHMAN ANSYORY, ST. M.M.",
		"band": "I",
		"posisi": "DEPUTY EVP INFRASTRUCTURE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8439215686274537,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818109399893756,
			"nilai_komparatif_unit": 0.9881744356862596,
			"summary_team": 0.8285714285714287
		}]
	},
	"730525": {
		"nik": "730525",
		"nama": "EDI SANTOSO",
		"band": "II",
		"posisi": "SENIOR STAFF II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.865882352941179,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0317028985507215,
			"nilai_komparatif_unit": 1.0352334307190862,
			"summary_team": 0.8933333333333333
		}]
	},
	"730560": {
		"nik": "730560",
		"nama": "MARINDRA BAWONO",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8256410256410257,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9689440993788819,
			"nilai_komparatif_unit": 0.9722598682082705,
			"summary_team": 0.8
		}]
	},
	"730563": {
		"nik": "730563",
		"nama": "TANTE SURATNO, MSC.",
		"band": "I",
		"posisi": "DIRECTOR OF BUSINESS AND SALES",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8799999999999996,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0064935064935072,
			"nilai_komparatif_unit": 1.013016979431772,
			"summary_team": 0.8857142857142859
		}]
	},
	"730566": {
		"nik": "730566",
		"nama": "Anggoro Kurnianto Widiawan,Msc",
		"band": "I",
		"posisi": "DIREKTUR OPERASI (CTO)",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8388888888888881,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9695364238410605,
			"nilai_komparatif_unit": 0.9758203636606267,
			"summary_team": 0.8133333333333334
		}]
	},
	"730601": {
		"nik": "730601",
		"nama": "DEVY PARLINDUNGAN SIREGAR",
		"band": "I",
		"posisi": "CHIEF EXECUTIVE OFFICER",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8637037037037028,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0188679245283032,
			"nilai_komparatif_unit": 1.025471600846576,
			"summary_team": 0.8800000000000001
		}]
	},
	"730610": {
		"nik": "730610",
		"nama": "BENNY ASRIANTO",
		"band": "II",
		"posisi": "GM PROJECT & ADJACENT BUSINESS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.881481481481481,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9529411764705888,
			"nilai_komparatif_unit": 0.9562021826021164,
			"summary_team": 0.8400000000000001
		}]
	},
	"735647": {
		"nik": "735647",
		"nama": "FX BUDI SETYO MULYONO",
		"band": "II",
		"posisi": "SM IN-HOUSE CHANNEL & PRODUCTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8173913043478244,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.000967117988397,
			"nilai_komparatif_unit": 1.0043924709795513,
			"summary_team": 0.8181818181818183
		}]
	},
	"740031": {
		"nik": "740031",
		"nama": "MOEHAMMAD DODY FEBRIARTO",
		"band": "II",
		"posisi": "VP SALES PLANNING & ADMINISTRATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8535714285714283,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.033718926901305,
			"nilai_komparatif_unit": 1.0372563580063248,
			"summary_team": 0.8823529411764708
		}]
	},
	"740034": {
		"nik": "740034",
		"nama": "JUWANSYAH",
		"band": "II",
		"posisi": "GM TA NUSA TENGGARA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.852777777777777,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9693811074918575,
			"nilai_komparatif_unit": 0.9726983717820059,
			"summary_team": 0.8266666666666665
		}]
	},
	"740037": {
		"nik": "740037",
		"nama": "BAMBANG WAHYU SAMODRA",
		"band": "II",
		"posisi": "VP VOICE & MOBILITY SALES REGION I",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8549019607843136,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9580602883355179,
			"nilai_komparatif_unit": 0.961338812290382,
			"summary_team": 0.8190476190476191
		}]
	},
	"740042": {
		"nik": "740042",
		"nama": "DEVINDRA KAMAL",
		"band": "I",
		"posisi": "VP TELKOM FINANCIAL CONTROL",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8715151515151515,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.902642559109875,
			"nilai_komparatif_unit": 0.9084929339700107,
			"summary_team": 0.7866666666666667
		}]
	},
	"740044": {
		"nik": "740044",
		"nama": "FIANDIS SUSANTO , S.Si",
		"band": "II",
		"posisi": "AVP PRODUCT & SERVICE WHOLESALE LEGACY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8199999999999992,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9756097560975621,
			"nilai_komparatif_unit": 0.978948335094007,
			"summary_team": 0.8
		}]
	},
	"740045": {
		"nik": "740045",
		"nama": "MADE TEJA BUANA",
		"band": "II",
		"posisi": "AVP MANAGED SERVICE SOLUTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8407407407407393,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9515418502202659,
			"nilai_komparatif_unit": 0.9547980677965343,
			"summary_team": 0.8
		}]
	},
	"740050": {
		"nik": "740050",
		"nama": "EKO MUJI SANTOSO",
		"band": "II",
		"posisi": "VP PROCUREMENT & PARTNERSHIP MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8740740740740738,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9915254237288139,
			"nilai_komparatif_unit": 0.9949184668317687,
			"summary_team": 0.8666666666666667
		}]
	},
	"740066": {
		"nik": "740066",
		"nama": "EKA SETIAWAN, ST, MBIS",
		"band": "I",
		"posisi": "SGM TELKOM SHARED SERVICE CENTER",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8352941176470572,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9776995305164338,
			"nilai_komparatif_unit": 0.9840363785814534,
			"summary_team": 0.8166666666666667
		}]
	},
	"740076": {
		"nik": "740076",
		"nama": "RACHMAD DWI HARTANTO",
		"band": "I",
		"posisi": "DEPUTY EVP MARKETING",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.865608465608466,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9627139364303172,
			"nilai_komparatif_unit": 0.9689536570753845,
			"summary_team": 0.8333333333333333
		}]
	},
	"740094": {
		"nik": "740094",
		"nama": "RADEN RIHARSO",
		"band": "II",
		"posisi": "OSM PERFORMANCE & SALES SUPPORT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8519999999999984,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9741784037558704,
			"nilai_komparatif_unit": 0.9775120846023793,
			"summary_team": 0.8300000000000001
		}]
	},
	"740098": {
		"nik": "740098",
		"nama": "TONI SATRIO ST",
		"band": "II",
		"posisi": "GM TA JAKARTA BARAT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8592592592592608,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9931034482758603,
			"nilai_komparatif_unit": 0.996501891447414,
			"summary_team": 0.8533333333333334
		}]
	},
	"740115": {
		"nik": "740115",
		"nama": "HERY SUSANTO",
		"band": "I",
		"posisi": "SGM COMMUNITY DEVELOPMENT CENTER",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8542222222222235,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9657908428720068,
			"nilai_komparatif_unit": 0.9720505061354581,
			"summary_team": 0.825
		}]
	},
	"740123": {
		"nik": "740123",
		"nama": "INDARTO",
		"band": "I",
		"posisi": "CHIEF EXECUTIVE OFFICER",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8637037037037028,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0420240137221282,
			"nilai_komparatif_unit": 1.048777773593089,
			"summary_team": 0.9000000000000001
		}]
	},
	"740127": {
		"nik": "740127",
		"nama": "WILSON NORMAL",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8306666666666651,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0662692043109399,
			"nilai_komparatif_unit": 1.0699180238802601,
			"summary_team": 0.8857142857142857
		}]
	},
	"740132": {
		"nik": "740132",
		"nama": "SYAIFUL RAHIM SOENARIA",
		"band": "I",
		"posisi": "TRANSFORMATION PROGRAM LEADER",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8352941176470572,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9748490945674062,
			"nilai_komparatif_unit": 0.9811674678567261,
			"summary_team": 0.8142857142857142
		}]
	},
	"740139": {
		"nik": "740139",
		"nama": "DODI IRAWAN",
		"band": "I",
		"posisi": "DIREKTUR GENERAL SUPPORT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8901960784313723,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0044052863436126,
			"nilai_komparatif_unit": 1.0109152247209996,
			"summary_team": 0.8941176470588235
		}]
	},
	"740142": {
		"nik": "740142",
		"nama": "ARIF SWASONO",
		"band": "II",
		"posisi": "SM PRODUCT MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8270833333333325,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9672544080604546,
			"nilai_komparatif_unit": 0.9705643946926983,
			"summary_team": 0.8000000000000002
		}]
	},
	"740150": {
		"nik": "740150",
		"nama": "HERU YULIANTO",
		"band": "II",
		"posisi": "VP MARKETING & SALES",
		"category": [{
			"code": "SP-4-01",
			"kriteria": 0.833333333333333,
			"kriteria_bp": 0.995228823802874,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.058823529411765,
			"nilai_komparatif_unit": 1.06389958177245,
			"summary_team": 0.8823529411764706
		}]
	},
	"740155": {
		"nik": "740155",
		"nama": "Didik Budi Santoso",
		"band": "I",
		"posisi": "DIREKTUR UTAMA",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8571428571428563,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9969696969696982,
			"nilai_komparatif_unit": 1.0034314424220956,
			"summary_team": 0.8545454545454547
		}]
	},
	"740156": {
		"nik": "740156",
		"nama": "DEDHY SUSAMTO",
		"band": "II",
		"posisi": "GM SALES TOWER RELATED BUSINESS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8515151515151511,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9551601423487548,
			"nilai_komparatif_unit": 0.9584287418779789,
			"summary_team": 0.8133333333333332
		}]
	},
	"740157": {
		"nik": "740157",
		"nama": "MICHAEL ADIGUNA, ST",
		"band": "I",
		"posisi": "SENIOR EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8424242424242417,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9496402877697848,
			"nilai_komparatif_unit": 0.9557952730512446,
			"summary_team": 0.7999999999999999
		}]
	},
	"740175": {
		"nik": "740175",
		"nama": "DANIEL SYAFRIL",
		"band": "II",
		"posisi": "AVP SEKRETARIAT DIREKTORAT WINS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8306666666666651,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9630818619582681,
			"nilai_komparatif_unit": 0.9663775699563638,
			"summary_team": 0.7999999999999998
		}]
	},
	"740183": {
		"nik": "740183",
		"nama": "ELYSABETH DAMAYANTI",
		"band": "I",
		"posisi": "OVP CYBER SECURITY",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8733333333333327,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9814612868047992,
			"nilai_komparatif_unit": 0.9878225162644222,
			"summary_team": 0.8571428571428573
		}]
	},
	"740208": {
		"nik": "740208",
		"nama": "FAJAR WIBAWA",
		"band": "I",
		"posisi": "DIREKTUR FINANCE",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8186046511627898,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.09128787878788,
			"nilai_komparatif_unit": 1.098360936784976,
			"summary_team": 0.8933333333333333
		}]
	},
	"740221": {
		"nik": "740221",
		"nama": "ANUNG ANENDITO",
		"band": "II",
		"posisi": "VP TREASURY & FUNDING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8592592592592588,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9310344827586212,
			"nilai_komparatif_unit": 0.9342205232319528,
			"summary_team": 0.8
		}]
	},
	"740249": {
		"nik": "740249",
		"nama": "BENNY YULIANTO ATMADJA, ST",
		"band": "II",
		"posisi": "OVP DIAGNOSTIC & SURVEILANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8694444444444462,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9968051118210842,
			"nilai_komparatif_unit": 1.0002162222462059,
			"summary_team": 0.8666666666666667
		}]
	},
	"740255": {
		"nik": "740255",
		"nama": "DENNI ARIEF MULYAWAN,RD.MENG",
		"band": "II",
		"posisi": "SM MARKETING & BUSINESS DEVELOPMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8424242424242417,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0129496402877705,
			"nilai_komparatif_unit": 1.0164159979911298,
			"summary_team": 0.8533333333333332
		}]
	},
	"740258": {
		"nik": "740258",
		"nama": "DIAN PRAMBINI",
		"band": "I",
		"posisi": "MARKETING & BUSINESS DIRECTOR",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8558558558558567,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9897213622291013,
			"nilai_komparatif_unit": 0.9961361284260712,
			"summary_team": 0.8470588235294119
		}]
	},
	"740260": {
		"nik": "740260",
		"nama": "UMAR HAPPY YANUAR PRIHARTONO",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8199999999999992,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8895265423242474,
			"nilai_komparatif_unit": 0.8925705408210061,
			"summary_team": 0.7294117647058822
		}]
	},
	"740274": {
		"nik": "740274",
		"nama": "FERDINAND SORITAN",
		"band": "II",
		"posisi": "SM PROGRAMMING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8173913043478244,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0232108317214723,
			"nilai_komparatif_unit": 1.0267123036679857,
			"summary_team": 0.8363636363636365
		}]
	},
	"740276": {
		"nik": "740276",
		"nama": "I WAYAN SUKERTA",
		"band": "I",
		"posisi": "DIRECTOR OF DELIVERY AND OPERATION",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8799999999999996,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.925324675324676,
			"nilai_komparatif_unit": 0.9313220617356615,
			"summary_team": 0.8142857142857145
		}]
	},
	"740277": {
		"nik": "740277",
		"nama": "LUKMAN HAKIM ABD. RAUF",
		"band": "I",
		"posisi": "EGM DIGITAL INFRASTRUCTURE DEVELOPMENT",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8377192982456159,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9072251308900504,
			"nilai_komparatif_unit": 0.9131052071668398,
			"summary_team": 0.76
		}]
	},
	"740279": {
		"nik": "740279",
		"nama": "IWAN SETIAWAN",
		"band": "I",
		"posisi": "VP HC ORGANIZATIONAL EFFECTIVENESS",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8542222222222235,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0145681581685728,
			"nilai_komparatif_unit": 1.0211439660412893,
			"summary_team": 0.8666666666666667
		}]
	},
	"750045": {
		"nik": "750045",
		"nama": "AGUNG NUGROHO",
		"band": "II",
		"posisi": "VP BUSINESS & OPERATION PREPARATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8947368421052623,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9984313725490206,
			"nilai_komparatif_unit": 1.001848048109049,
			"summary_team": 0.8933333333333333
		}]
	},
	"750055": {
		"nik": "750055",
		"nama": "JEMY VESTIUS CONFIDO",
		"band": "I",
		"posisi": "SGM TELKOM CORPORATE UNIVERSITY CENTER",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8542222222222235,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9365244536940672,
			"nilai_komparatif_unit": 0.9425944301919593,
			"summary_team": 0.8
		}]
	},
	"750067": {
		"nik": "750067",
		"nama": "LUKMAN HERI WAHYONO",
		"band": "II",
		"posisi": "VP FINANCE PLAN & REPORTING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.942028985507246,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9624615384615388,
			"nilai_komparatif_unit": 0.9657551236856625,
			"summary_team": 0.9066666666666666
		}]
	},
	"760051": {
		"nik": "760051",
		"nama": "IKA PRIASTUTI",
		"band": "II",
		"posisi": "OSM COLLECTION & DEBT MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8318840579710131,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9777003484320574,
			"nilai_komparatif_unit": 0.981046081526352,
			"summary_team": 0.8133333333333334
		}]
	},
	"760058": {
		"nik": "760058",
		"nama": "RAHMADI FAJAR AJI HANDAYA",
		"band": "II",
		"posisi": "PRINCIPAL EXPERT PREMIUM CLUSTER & APART",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8519999999999984,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0758998435054792,
			"nilai_komparatif_unit": 1.0795816195407801,
			"summary_team": 0.9166666666666665
		}]
	},
	"770036": {
		"nik": "770036",
		"nama": "HARRY CHRISMANARIA,ST",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.865882352941179,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9816576086956491,
			"nilai_komparatif_unit": 0.9850168837065931,
			"summary_team": 0.8499999999999999
		}]
	},
	"770058": {
		"nik": "770058",
		"nama": "ADI WAHYU NADIRI",
		"band": "II",
		"posisi": "GM TA PAPUA & AMBON",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8476190476190472,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9438202247191015,
			"nilai_komparatif_unit": 0.9470500185572354,
			"summary_team": 0.7999999999999999
		}]
	},
	"770074": {
		"nik": "770074",
		"nama": "AFIFUDIN, ST",
		"band": "II",
		"posisi": "AVP MARKETING COMMUNICATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8765957446808497,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.995586937334512,
			"nilai_komparatif_unit": 0.998993879113585,
			"summary_team": 0.8727272727272729
		}]
	},
	"770077": {
		"nik": "770077",
		"nama": "PRAYUDI NUGROHO",
		"band": "II",
		"posisi": "GM CARRIER SERVICE T-GROUP",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8318840579710131,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9616724738675976,
			"nilai_komparatif_unit": 0.964963358878379,
			"summary_team": 0.8
		}]
	},
	"770088": {
		"nik": "770088",
		"nama": "KENNY NAZAR",
		"band": "I",
		"posisi": "AUDITOR UTAMA",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8592592592592581,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9775862068965531,
			"nilai_komparatif_unit": 0.9839223204674477,
			"summary_team": 0.84
		}]
	},
	"775748": {
		"nik": "775748",
		"nama": "AHMAD REZA",
		"band": "I",
		"posisi": "SVP CORPORATE COMM & INVESTOR RELATION",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8757575757575752,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9705882352941184,
			"nilai_komparatif_unit": 0.976878992309728,
			"summary_team": 0.8500000000000001
		}]
	},
	"780050": {
		"nik": "780050",
		"nama": "ENDAH PUSPITASARI",
		"band": "II",
		"posisi": "OSM SALES OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8519999999999984,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9496372172428531,
			"nilai_komparatif_unit": 0.9528869170822758,
			"summary_team": 0.8090909090909093
		}]
	},
	"780051": {
		"nik": "780051",
		"nama": "GUNAWAN WASISTO CIPTANING ANDRI",
		"band": "I",
		"posisi": "DEPUTY EGM INFRASTRUCTURE  & OPERATION",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8173913043478244,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0276595744680872,
			"nilai_komparatif_unit": 1.034320232863338,
			"summary_team": 0.84
		}]
	},
	"780063": {
		"nik": "780063",
		"nama": "TEGUH PRIHATMONO",
		"band": "II",
		"posisi": "VP INFORMATION TECHNOLOGY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8476190476190472,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9438202247191015,
			"nilai_komparatif_unit": 0.9470500185572354,
			"summary_team": 0.7999999999999999
		}]
	},
	"790027": {
		"nik": "790027",
		"nama": "RULI SATYA DHARMA ",
		"band": "II",
		"posisi": "SVP CORPORATE STRATEGY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8947368421052623,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9579831932773121,
			"nilai_komparatif_unit": 0.9612614534095356,
			"summary_team": 0.8571428571428573
		}]
	},
	"790047": {
		"nik": "790047",
		"nama": "HANDY PRAMUDIANTO",
		"band": "II",
		"posisi": "VP INVENTORY & ASSET MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8740740740740738,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0372881355932206,
			"nilai_komparatif_unit": 1.0408377806855427,
			"summary_team": 0.9066666666666666
		}]
	},
	"790089": {
		"nik": "790089",
		"nama": "GANJAR DANISWARA",
		"band": "I",
		"posisi": "DEPUTY SGM HC BUSINESS PARTNER CENTER",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8460317460317445,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9849906191369625,
			"nilai_komparatif_unit": 0.9913747235618093,
			"summary_team": 0.8333333333333334
		}]
	},
	"800010": {
		"nik": "800010",
		"nama": "FERRY STUDIYONO PURBA",
		"band": "II",
		"posisi": "PRINCIPAL EXPERT DATA ANALYSIS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.865882352941179,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0009057971014461,
			"nilai_komparatif_unit": 1.0043309402498597,
			"summary_team": 0.8666666666666665
		}]
	},
	"800044": {
		"nik": "800044",
		"nama": "RANI SIESARIA",
		"band": "II",
		"posisi": "PROJECT HEAD LEGAL PREPARATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8947368421052623,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9800904977375575,
			"nilai_komparatif_unit": 0.9834444100266785,
			"summary_team": 0.8769230769230769
		}]
	},
	"800097": {
		"nik": "800097",
		"nama": "DIDI HARYADI",
		"band": "I",
		"posisi": "VP HC TECHOLOGY AND PEOPLE ANALYTICS",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8542222222222235,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0145681581685728,
			"nilai_komparatif_unit": 1.0211439660412893,
			"summary_team": 0.8666666666666667
		}]
	},
	"800116": {
		"nik": "800116",
		"nama": "IRFAN BUDIMAN",
		"band": "II",
		"posisi": "SM BUSINESS DEVELOPMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8173913043478244,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0232108317214723,
			"nilai_komparatif_unit": 1.0267123036679857,
			"summary_team": 0.8363636363636365
		}]
	},
	"810062": {
		"nik": "810062",
		"nama": "BUDI PRABOWO DARMAHADI",
		"band": "II",
		"posisi": "GM CLOSED USER GROUP",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8318840579710131,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0356472795497205,
			"nilai_komparatif_unit": 1.0391913095613312,
			"summary_team": 0.8615384615384616
		}]
	},
	"810111": {
		"nik": "810111",
		"nama": "JUNAINAH",
		"band": "I",
		"posisi": "VP FINANCIAL ACCOUNTING & CORPORATE FIN",
		"category": [{
			"code": "MD-5-03",
			"kriteria": 0.8715151515151515,
			"kriteria_bp": 0.9935603518295181,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9332406119610572,
			"nilai_komparatif_unit": 0.9392893046130619,
			"summary_team": 0.8133333333333335
		}]
	},
	"820036": {
		"nik": "820036",
		"nama": "DELTA QUARTAMA DIEN WARDI HASFAREZA",
		"band": "II",
		"posisi": "AVP TARIFF, PRODUCT & QUALITY ASSURANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.865882352941179,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9554100790513808,
			"nilai_komparatif_unit": 0.9586795338748664,
			"summary_team": 0.8272727272727274
		}]
	},
	"820073": {
		"nik": "820073",
		"nama": "SEALY EMMYLOW TAURAN",
		"band": "II",
		"posisi": "AVP BUSINESS STRATEGY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8306666666666651,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9791332263242393,
			"nilai_komparatif_unit": 0.9824838627889699,
			"summary_team": 0.8133333333333332
		}]
	},
	"830142": {
		"nik": "830142",
		"nama": "MUHAMAD ARIEF SETIAWAN",
		"band": "II",
		"posisi": "AVP PRODUCT & SERVICE SRTGY & MKTG",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8199999999999992,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9756097560975621,
			"nilai_komparatif_unit": 0.978948335094007,
			"summary_team": 0.8
		}]
	},
	"850067": {
		"nik": "850067",
		"nama": "GUNTUR PRAMONO",
		"band": "II",
		"posisi": "SM SALES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8173913043478244,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9787234042553214,
			"nilai_komparatif_unit": 0.9820726382911168,
			"summary_team": 0.8000000000000002
		}]
	}
};