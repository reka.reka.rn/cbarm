export const cbark19 = {
	"670477": {
		"nik": "670477",
		"nama": "AGUS SUHAIDI",
		"band": "V",
		"posisi": "OFF 2 ENTERPRISE & BUSINESS BILLING SUPP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272723,
			"nilai_komparatif_unit": 1.0233485696940576,
			"summary_team": 0.8999999999999998
		}]
	},
	"670483": {
		"nik": "670483",
		"nama": "FERINO",
		"band": "V",
		"posisi": "OFF 2 SERVICE PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8158730158730151,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9280155642023353,
			"nilai_komparatif_unit": 0.9285793247185001,
			"summary_team": 0.757142857142857
		}]
	},
	"690441": {
		"nik": "690441",
		"nama": "ARIES JAUHARI",
		"band": "V",
		"posisi": "OFF 2 DEBT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8400000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0034013605442174,
			"nilai_komparatif_unit": 1.004010917205508,
			"summary_team": 0.8428571428571426
		}]
	},
	"690584": {
		"nik": "690584",
		"nama": "TEGUH SULISTYO",
		"band": "V",
		"posisi": "OFF 2 ACCESS PROJECT ADMIN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8200000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9105691056910565,
			"nilai_komparatif_unit": 0.9111222676517283,
			"summary_team": 0.7466666666666667
		}]
	},
	"700088": {
		"nik": "700088",
		"nama": "IMAM SUBAGIO",
		"band": "V",
		"posisi": "OFF 2 INTERNAL CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8175438596491225,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9901900674432867,
			"nilai_komparatif_unit": 0.9907915983712761,
			"summary_team": 0.8095238095238095
		}]
	},
	"740321": {
		"nik": "740321",
		"nama": "DIYAH SANTI INDRAWATI",
		"band": "V",
		"posisi": "OFF 2 SEKRETARIAT BOC",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8368421052631614,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9981502034776132,
			"nilai_komparatif_unit": 0.9987565701115675,
			"summary_team": 0.8352941176470587
		}]
	},
	"826002": {
		"nik": "826002",
		"nama": "ENI EKOWATI",
		"band": "V",
		"posisi": "JUNIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7636363636363636,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0476190476190474,
			"nilai_komparatif_unit": 1.0482554660993102,
			"summary_team": 0.7999999999999998
		}]
	},
	"835859": {
		"nik": "835859",
		"nama": "FAISAL EKO WIBOWO",
		"band": "V",
		"posisi": "JUNIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.749999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8761904761904772,
			"nilai_komparatif_unit": 0.8767227534648787,
			"summary_team": 0.657142857142857
		}]
	},
	"880095": {
		"nik": "880095",
		"nama": "RENDI PRATIKTO",
		"band": "V",
		"posisi": "OFF 2 PDI PREPARATION DIGI TELCO 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888886,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.019117647058824,
			"nilai_komparatif_unit": 1.0197367512127853,
			"summary_team": 0.9058823529411766
		}]
	},
	"880096": {
		"nik": "880096",
		"nama": "SHELLA DEVIANY HAKIM",
		"band": "V",
		"posisi": "OFF 2 BUSINESS SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882353,
			"nilai_komparatif_unit": 0.9417482262282574,
			"summary_team": 0.8
		}]
	},
	"880099": {
		"nik": "880099",
		"nama": "LAURENTIA NINDITA ARIYANI",
		"band": "V",
		"posisi": "OFF 2 PPN OPERATION CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9466666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0563380281690138,
			"nilai_komparatif_unit": 1.0569797433459753,
			"summary_team": 0.9999999999999998
		}]
	},
	"885905": {
		"nik": "885905",
		"nama": "AYMAN HUSNI KAMAL",
		"band": "V",
		"posisi": "JUNIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.829743589743589,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7940085799461941,
			"nilai_komparatif_unit": 0.7944909325102425,
			"summary_team": 0.6588235294117646
		}]
	},
	"890096": {
		"nik": "890096",
		"nama": "FEBY MUSTIKAWATI",
		"band": "V",
		"posisi": "OFF 2 PREPARATION EXTERNAL ASSESSMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8200000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9930313588850169,
			"nilai_komparatif_unit": 0.9936346158701884,
			"summary_team": 0.8142857142857142
		}]
	},
	"890103": {
		"nik": "890103",
		"nama": "ENDANG TRISIA",
		"band": "V",
		"posisi": "OFF 2 FINANCIAL & PORTFOLIO DATA MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8390804597701141,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9534246575342475,
			"nilai_komparatif_unit": 0.954003853829859,
			"summary_team": 0.8
		}]
	},
	"890109": {
		"nik": "890109",
		"nama": "SAMUEL P. SIMANJUNTAK",
		"band": "V",
		"posisi": "AUDITOR PRATAMA II",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0800822528131542,
			"nilai_komparatif_unit": 1.0807383923778713,
			"summary_team": 0.9428571428571427
		}]
	},
	"900081": {
		"nik": "900081",
		"nama": "RIKA FEBITA",
		"band": "V",
		"posisi": "OFF 2 HC STRATEGY RESTRUCTURING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0135135135135132,
			"nilai_komparatif_unit": 1.0141292132103275,
			"summary_team": 0.9999999999999998
		}]
	},
	"900104": {
		"nik": "900104",
		"nama": "CARINE WULANDARI PRASETYOWATI",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8471014492753652,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0624465355004242,
			"nilai_komparatif_unit": 1.0630919615367496,
			"summary_team": 0.9000000000000001
		}]
	},
	"900109": {
		"nik": "900109",
		"nama": "ELSA HUTAMI FAZA",
		"band": "V",
		"posisi": "OFF 2 STRATEGIC PLANNING & EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8694444444444437,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9742529599699312,
			"nilai_komparatif_unit": 0.9748448092586443,
			"summary_team": 0.8470588235294116
		}]
	},
	"900112": {
		"nik": "900112",
		"nama": "RISKI PRATAMA",
		"band": "V",
		"posisi": "OFF 2 COMPLEX EVENT DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9874999999999998,
			"nilai_komparatif_unit": 0.9880998967379293,
			"summary_team": 0.8777777777777778
		}]
	},
	"900113": {
		"nik": "900113",
		"nama": "DIANTI MELLISA",
		"band": "V",
		"posisi": "OFF 2 FINANCE SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9719008264462807,
			"nilai_komparatif_unit": 0.972491246836535,
			"summary_team": 0.8909090909090909
		}]
	},
	"900116": {
		"nik": "900116",
		"nama": "RIZKITA RIA",
		"band": "V",
		"posisi": "OFF 2 SYNERGY COMMUNICATION & SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8296296296296293,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0635504201680677,
			"nilai_komparatif_unit": 1.0641965168036953,
			"summary_team": 0.8823529411764706
		}]
	},
	"900119": {
		"nik": "900119",
		"nama": "MUHAMMAD FADHILAH HANGGORO",
		"band": "V",
		"posisi": "OFF 2 PLANNING & STRATEGIC ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8027777777777776,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0844697740687974,
			"nilai_komparatif_unit": 1.0851285790104146,
			"summary_team": 0.8705882352941177
		}]
	},
	"900120": {
		"nik": "900120",
		"nama": "KOMANG YOGA WIDHARTAWAN",
		"band": "V",
		"posisi": "OFF 2 STRATEGIC INVEST EXEC DIGI TELCO 3",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9148148148148144,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.913074541557514,
			"nilai_komparatif_unit": 0.9136292255463412,
			"summary_team": 0.8352941176470587
		}]
	},
	"900121": {
		"nik": "900121",
		"nama": "REZA DWI PERMANA",
		"band": "V",
		"posisi": "JUNIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8422222222222243,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9638367220238993,
			"nilai_komparatif_unit": 0.9644222435483942,
			"summary_team": 0.8117647058823527
		}]
	},
	"900123": {
		"nik": "900123",
		"nama": "ALFA PUTRA KURNIA",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7851851851851854,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9493996569468265,
			"nilai_komparatif_unit": 0.9499764080933518,
			"summary_team": 0.7454545454545455
		}]
	},
	"900124": {
		"nik": "900124",
		"nama": "KARINA LUPITASARI",
		"band": "V",
		"posisi": "OFF 2 PROGRAM & PRODUCT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.877777777777778,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9918093819806398,
			"nilai_komparatif_unit": 0.9924118966266126,
			"summary_team": 0.8705882352941174
		}]
	},
	"900125": {
		"nik": "900125",
		"nama": "AQUILA YOMA ERADIPA",
		"band": "V",
		"posisi": "OFF 2 DATACOM CONNECTIVITY PROD&SERV STR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8222222222222211,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9729729729729744,
			"nilai_komparatif_unit": 0.9735640446819162,
			"summary_team": 0.8
		}]
	},
	"900126": {
		"nik": "900126",
		"nama": "RYAN ARIFIANTO NUGROHO",
		"band": "V",
		"posisi": "OFF 2 STRATEGIC INVEST EXEC DIGI TELCO 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888886,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9926470588235297,
			"nilai_komparatif_unit": 0.9932500823501155,
			"summary_team": 0.8823529411764706
		}]
	},
	"900128": {
		"nik": "900128",
		"nama": "ADITYO RAHMANTO",
		"band": "V",
		"posisi": "OFF 2 COMPETENCY DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8488888888888888,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0097232610321618,
			"nilai_komparatif_unit": 1.0103366581871034,
			"summary_team": 0.8571428571428572
		}]
	},
	"900130": {
		"nik": "900130",
		"nama": "AHMAD MUSHOFI HASAN",
		"band": "V",
		"posisi": "OFF 2 RETAIL DATA MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8527777777777769,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9932937344318844,
			"nilai_komparatif_unit": 0.9938971508076733,
			"summary_team": 0.8470588235294116
		}]
	},
	"900132": {
		"nik": "900132",
		"nama": "LYSABRINA RIZKY ANNISTYA",
		"band": "V",
		"posisi": "OFF 2 INTERNAL ASSESSMENT EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9030303030303026,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0943545203316232,
			"nilai_komparatif_unit": 1.0950193301613804,
			"summary_team": 0.9882352941176471
		}]
	},
	"900135": {
		"nik": "900135",
		"nama": "ARYAN SETIAWAN",
		"band": "V",
		"posisi": "OFF 2 SYNERGY INTEGRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9848484848484843,
			"nilai_komparatif_unit": 0.9854467708164999,
			"summary_team": 0.8666666666666665
		}]
	},
	"900137": {
		"nik": "900137",
		"nama": "MUHAMMAD HUDI WIBOWO",
		"band": "V",
		"posisi": "OFF 2 INDUSTRY SCANNING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8390804597701141,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0796132151490745,
			"nilai_komparatif_unit": 1.0802690697779287,
			"summary_team": 0.9058823529411766
		}]
	},
	"900139": {
		"nik": "900139",
		"nama": "HENDRA GUNAWAN",
		"band": "V",
		"posisi": "OFF 2 ASSET MANAGEMENT SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0129870129870129,
			"nilai_komparatif_unit": 1.013602392839829,
			"summary_team": 0.9454545454545454
		}]
	},
	"900140": {
		"nik": "900140",
		"nama": "MUHAMMAD RIZQI FAHMA",
		"band": "V",
		"posisi": "OFF 2 RETAIL PROJECT DESIGN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.911111111111111,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0701219512195121,
			"nilai_komparatif_unit": 1.0707720399969536,
			"summary_team": 0.9749999999999999
		}]
	},
	"900141": {
		"nik": "900141",
		"nama": "ANDI AULIA SABRI",
		"band": "V",
		"posisi": "OFF 2 DIGITAL MARKET ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8289855072463764,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0339660339660344,
			"nilai_komparatif_unit": 1.0345941583720153,
			"summary_team": 0.8571428571428571
		}]
	},
	"900142": {
		"nik": "900142",
		"nama": "RISKY GELAR MALIQ",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0120419301741261,
			"nilai_komparatif_unit": 1.0126567358982368,
			"summary_team": 0.8428571428571427
		}]
	},
	"906547": {
		"nik": "906547",
		"nama": "ASEP RAHMAT",
		"band": "V",
		"posisi": "JUNIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.829743589743589,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.895285184531168,
			"nilai_komparatif_unit": 0.8958290616569572,
			"summary_team": 0.7428571428571428
		}]
	},
	"910116": {
		"nik": "910116",
		"nama": "WAYAN ANDI MAHARDHIKA",
		"band": "V",
		"posisi": "OFF 2 MANAGED & NETWORK SERVICE BILLING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.989164086687306,
			"nilai_komparatif_unit": 0.9897649943418688,
			"summary_team": 0.8352941176470587
		}]
	},
	"910122": {
		"nik": "910122",
		"nama": "YUSTIA FAUZIAH",
		"band": "V",
		"posisi": "JUNIOR DESIGNER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.829743589743589,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9783320002908465,
			"nilai_komparatif_unit": 0.9789263275572633,
			"summary_team": 0.8117647058823529
		}]
	},
	"910123": {
		"nik": "910123",
		"nama": "TRISTIYANA RIZKI",
		"band": "V",
		"posisi": "JUNIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8685185185185175,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0294744763577084,
			"nilai_komparatif_unit": 1.030099872185707,
			"summary_team": 0.8941176470588235
		}]
	},
	"910128": {
		"nik": "910128",
		"nama": "KUKUH PRIMANANDA PUTRA",
		"band": "V",
		"posisi": "JUNIOR PRODUCT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8685185185185175,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9346544587984459,
			"nilai_komparatif_unit": 0.9352222523791288,
			"summary_team": 0.8117647058823529
		}]
	},
	"910129": {
		"nik": "910129",
		"nama": "GAMA BISMATAMA PRASETYO",
		"band": "V",
		"posisi": "JUNIOR PRODUCT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.829743589743589,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96415327564895,
			"nilai_komparatif_unit": 0.964738989476723,
			"summary_team": 0.7999999999999998
		}]
	},
	"910133": {
		"nik": "910133",
		"nama": "VONDA BRI VALDO ARY",
		"band": "V",
		"posisi": "JUNIOR PRODUCT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.829743589743589,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006689449574639,
			"nilai_komparatif_unit": 1.0073010037183432,
			"summary_team": 0.8352941176470587
		}]
	},
	"910151": {
		"nik": "910151",
		"nama": "MARISATUSHOLEKHA",
		"band": "V",
		"posisi": "AUDITOR PRATAMA II",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0830576860164964,
			"nilai_komparatif_unit": 1.0837156331282238,
			"summary_team": 0.9454545454545454
		}]
	},
	"910154": {
		"nik": "910154",
		"nama": "MARINI NURSHADRINA",
		"band": "V",
		"posisi": "AUDITOR PRATAMA II",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.096447135431535,
			"nilai_komparatif_unit": 1.0971132165048085,
			"summary_team": 0.9571428571428569
		}]
	},
	"910155": {
		"nik": "910155",
		"nama": "BAGUS FACSI AGINSA",
		"band": "V",
		"posisi": "ENG 2 IOT PLATFORM & SERVICES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8944444444444447,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0390938984289366,
			"nilai_komparatif_unit": 1.0397251379631847,
			"summary_team": 0.9294117647058824
		}]
	},
	"910176": {
		"nik": "910176",
		"nama": "AUDREY ADYUTA PUTRI",
		"band": "V",
		"posisi": "OFF 2 POST DEAL ANALYSIS & EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8027777777777776,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0551597801750459,
			"nilai_komparatif_unit": 1.0558007795777005,
			"summary_team": 0.8470588235294116
		}]
	},
	"910183": {
		"nik": "910183",
		"nama": "MADE UTARI DWIYANI",
		"band": "V",
		"posisi": "JUNIOR DESIGNER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.829743589743589,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0634043481422244,
			"nilai_komparatif_unit": 1.0640503560405035,
			"summary_team": 0.8823529411764706
		}]
	},
	"910186": {
		"nik": "910186",
		"nama": "ARIF FAJARUDDIN",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8604938271604932,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9961057593769221,
			"nilai_komparatif_unit": 0.9967108840307783,
			"summary_team": 0.857142857142857
		}]
	},
	"910189": {
		"nik": "910189",
		"nama": "WULANTIKA OKTARIANI",
		"band": "V",
		"posisi": "OFF 2 ON GOING QUALITY ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8592592592592581,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0268762677484802,
			"nilai_komparatif_unit": 1.027500085189776,
			"summary_team": 0.8823529411764706
		}]
	},
	"910194": {
		"nik": "910194",
		"nama": "DEA ANANDYA RAHARDJO",
		"band": "V",
		"posisi": "OFF 2 SINERGI & SHARING EVENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8374999999999996,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.039508340649693,
			"nilai_komparatif_unit": 1.0401398319535982,
			"summary_team": 0.8705882352941174
		}]
	},
	"910196": {
		"nik": "910196",
		"nama": "EMILIA NURHUDA",
		"band": "V",
		"posisi": "OFF 2 GROUP DEBT MANAGEMENT & REPORTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.857575757575757,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9602993140719192,
			"nilai_komparatif_unit": 0.9608826866551573,
			"summary_team": 0.8235294117647058
		}]
	},
	"910207": {
		"nik": "910207",
		"nama": "ISTI APRILLANI",
		"band": "V",
		"posisi": "OFF 2 CEM OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.92,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9855072463768114,
			"nilai_komparatif_unit": 0.98610593253611,
			"summary_team": 0.9066666666666665
		}]
	},
	"910208": {
		"nik": "910208",
		"nama": "MOHAMMAD FAISAL RIZKI",
		"band": "V",
		"posisi": "OFF 2 SYNERGY BIZ ANALYST CROSS CFU/FU",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.011428571428571,
			"nilai_komparatif_unit": 1.0120430045431519,
			"summary_team": 0.8428571428571427
		}]
	},
	"910211": {
		"nik": "910211",
		"nama": "ARIP ANDRIKA BUDI SETIO",
		"band": "V",
		"posisi": "OFF 2 DESAIN ACCESS NODE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555551,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0259740259740264,
			"nilai_komparatif_unit": 1.026597295312135,
			"summary_team": 0.8777777777777777
		}]
	},
	"910215": {
		"nik": "910215",
		"nama": "DINUR RAHMANI SADAT",
		"band": "V",
		"posisi": "OFF 2 IT INFRASTRUCTURE REQ ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9780564263322884,
			"nilai_komparatif_unit": 0.9786505861901797,
			"summary_team": 0.9454545454545454
		}]
	},
	"910216": {
		"nik": "910216",
		"nama": "JESSICA ADELAIDE GUSTI",
		"band": "V",
		"posisi": "JUNIOR DESIGNER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0519031141868522,
			"nilai_komparatif_unit": 1.0525421351962887,
			"summary_team": 0.8941176470588235
		}]
	},
	"910218": {
		"nik": "910218",
		"nama": "FIKRI INDRA MAULANA",
		"band": "V",
		"posisi": "OFF 2 HC DATA GOVERNANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8969696969696972,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9396718146718143,
			"nilai_komparatif_unit": 0.9402426562478607,
			"summary_team": 0.8428571428571427
		}]
	},
	"910221": {
		"nik": "910221",
		"nama": "PRADITA PUTI ARSTIANTI",
		"band": "V",
		"posisi": "OFF 2 REGULATORY COMPLIANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8743589743589737,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0456640134059496,
			"nilai_komparatif_unit": 1.0462992442217598,
			"summary_team": 0.9142857142857141
		}]
	},
	"910224": {
		"nik": "910224",
		"nama": "DARYANTO",
		"band": "V",
		"posisi": "OFF 2 SYNERGY SYSTEM ANALYST",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.88695652173913,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9663865546218491,
			"nilai_komparatif_unit": 0.9669736251450861,
			"summary_team": 0.857142857142857
		}]
	},
	"910225": {
		"nik": "910225",
		"nama": "NICKY YUDHA ANANDA",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8231884057971014,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0147058823529411,
			"nilai_komparatif_unit": 1.01532230640234,
			"summary_team": 0.8352941176470587
		}]
	},
	"910227": {
		"nik": "910227",
		"nama": "BAYU AULIA RAHMAN",
		"band": "V",
		"posisi": "OFF 2 WS&MOBILE BIZ PORT DATA MINING&SUP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8121212121212125,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0343283582089549,
			"nilai_komparatif_unit": 1.0349567027234232,
			"summary_team": 0.8400000000000001
		}]
	},
	"910228": {
		"nik": "910228",
		"nama": "ABDULLOH AL MUBAROK",
		"band": "V",
		"posisi": "OFF 2 BILLING MGT SYSTEM OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.925373134328358,
			"nilai_komparatif_unit": 0.9259352895938275,
			"summary_team": 0.8266666666666665
		}]
	},
	"910233": {
		"nik": "910233",
		"nama": "IRMA ULFAH ARDANISSA",
		"band": "V",
		"posisi": "OFF 2 PREPARATION INTERNAL ASSESSMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9030303030303026,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0682984603237273,
			"nilai_komparatif_unit": 1.0689474413480142,
			"summary_team": 0.9647058823529413
		}]
	},
	"910242": {
		"nik": "910242",
		"nama": "KISWANTO",
		"band": "V",
		"posisi": "ENG 2 DEVICE & ENERGY QUALITY ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7537037037037038,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0145974851857202,
			"nilai_komparatif_unit": 1.015213843384884,
			"summary_team": 0.764705882352941
		}]
	},
	"910243": {
		"nik": "910243",
		"nama": "MUHAMAD ARIFIN",
		"band": "V",
		"posisi": "OFF 2 SUPPORT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8027777777777776,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1137797679625485,
			"nilai_komparatif_unit": 1.1144563784431283,
			"summary_team": 0.8941176470588235
		}]
	},
	"910244": {
		"nik": "910244",
		"nama": "MUHAMMAD ELDI RADITYO",
		"band": "V",
		"posisi": "OFF 2 DATA & REPORTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8368421052631614,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9559748427672914,
			"nilai_komparatif_unit": 0.9565555882758674,
			"summary_team": 0.7999999999999998
		}]
	},
	"910247": {
		"nik": "910247",
		"nama": "MUHAMAD INSAN NASHER",
		"band": "V",
		"posisi": "OFF 2 RECRUITMENT & PLACEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8518518518518513,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9805626598465479,
			"nilai_komparatif_unit": 0.9811583422171579,
			"summary_team": 0.8352941176470587
		}]
	},
	"910249": {
		"nik": "910249",
		"nama": "FAZA FAIKAR CORDOVA",
		"band": "V",
		"posisi": "OFF 2 BUSINESS ECOSYSTEM SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9763177998472126,
			"nilai_komparatif_unit": 0.9769109035062615,
			"summary_team": 0.8352941176470587
		}]
	},
	"910253": {
		"nik": "910253",
		"nama": "RIZKY DAMIRI PUTRA",
		"band": "V",
		"posisi": "OFF 2 ENT COMM & DATACOMM INTERFACE MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0549450549450543,
			"nilai_komparatif_unit": 1.0555859239042,
			"summary_team": 0.914285714285714
		}]
	},
	"910256": {
		"nik": "910256",
		"nama": "PRETTY SISKA BUDHIYATI",
		"band": "V",
		"posisi": "OFF 2 ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0336134453781514,
			"nilai_komparatif_unit": 1.0342413555899614,
			"summary_team": 0.9647058823529411
		}]
	},
	"910258": {
		"nik": "910258",
		"nama": "SWASTIKA TIARA ARDHIANI",
		"band": "V",
		"posisi": "OFF 2 SYNERGY DATA MINING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8296296296296293,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0493697478991602,
			"nilai_komparatif_unit": 1.0500072299129795,
			"summary_team": 0.8705882352941177
		}]
	},
	"910259": {
		"nik": "910259",
		"nama": "FAHMY AMRILLAH",
		"band": "V",
		"posisi": "OFF 2 TRANSPORT ISP DEPLOYMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0149253731343282,
			"nilai_komparatif_unit": 1.0155419305222626,
			"summary_team": 0.9066666666666665
		}]
	},
	"910265": {
		"nik": "910265",
		"nama": "ISHAK FIRDAUZI RUSLAN",
		"band": "V",
		"posisi": "OFF 2 MIKRO WORKFORCE PLANNING & EVALUAT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8277777777777781,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9543624161073823,
			"nilai_komparatif_unit": 0.954942182082294,
			"summary_team": 0.79
		}]
	},
	"910269": {
		"nik": "910269",
		"nama": "AMANDA ARGADINATA GINTING",
		"band": "V",
		"posisi": "OFF 2 NMS  SERVICE NODE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9806451612903221,
			"nilai_komparatif_unit": 0.9812408937797645,
			"summary_team": 0.8444444444444443
		}]
	},
	"910279": {
		"nik": "910279",
		"nama": "DEA RAHMATIA",
		"band": "V",
		"posisi": "OFF 2 ENTERPRISE & BUSINESS BILLING SOL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8722222222222225,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0116148370176092,
			"nilai_komparatif_unit": 1.0122293832867413,
			"summary_team": 0.8823529411764706
		}]
	},
	"920007": {
		"nik": "920007",
		"nama": "KITARA HARUMI",
		"band": "V",
		"posisi": "OFF 2 EXTERNAL ASSESSMENT EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8200000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9233449477351913,
			"nilai_komparatif_unit": 0.923905870896842,
			"summary_team": 0.7571428571428571
		}]
	},
	"920081": {
		"nik": "920081",
		"nama": "HASYIM YUSUF ASJARI",
		"band": "V",
		"posisi": "OFF 2 TRIBE SYSTEM DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666664,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1131221719457018,
			"nilai_komparatif_unit": 1.1137983829430358,
			"summary_team": 0.9647058823529413
		}]
	},
	"920082": {
		"nik": "920082",
		"nama": "MAYA KANIA",
		"band": "V",
		"posisi": "OFF 2 FINANCIAL STATEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888888,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0406250000000001,
			"nilai_komparatif_unit": 1.0412571696637043,
			"summary_team": 0.925
		}]
	},
	"920097": {
		"nik": "920097",
		"nama": "BELLA DWI JAYANTI",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8604938271604932,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0459110473457682,
			"nilai_komparatif_unit": 1.0465464282323171,
			"summary_team": 0.8999999999999999
		}]
	},
	"920104": {
		"nik": "920104",
		"nama": "ARNI SUHARTI",
		"band": "V",
		"posisi": "OFF 2 PROCUREMENT CONTRACT DRAFTER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882353,
			"nilai_komparatif_unit": 0.9417482262282574,
			"summary_team": 0.8
		}]
	},
	"920105": {
		"nik": "920105",
		"nama": "NI MADE DWI MARTINA SWANDAYANI",
		"band": "V",
		"posisi": "AUDITOR PRATAMA II",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9997455563229196,
			"nilai_komparatif_unit": 1.0003528921183602,
			"summary_team": 0.8727272727272727
		}]
	},
	"920110": {
		"nik": "920110",
		"nama": "SASKIA DWI APRILIA PUTRI",
		"band": "V",
		"posisi": "AUDITOR PRATAMA II",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1051108968177372,
			"nilai_komparatif_unit": 1.1057822410425993,
			"summary_team": 0.9647058823529411
		}]
	},
	"920115": {
		"nik": "920115",
		"nama": "ARWIN TARUNA RIZQURRAHMAN",
		"band": "V",
		"posisi": "ENG 2 TRANSMISSION QUALITY ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8138888888888886,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.01184501104196,
			"nilai_komparatif_unit": 1.0124596971395947,
			"summary_team": 0.8235294117647058
		}]
	},
	"920124": {
		"nik": "920124",
		"nama": "ARI VIO SARJONO",
		"band": "V",
		"posisi": "OFF 2 MEDIA PARTNERSHIP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8799999999999992,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0026737967914447,
			"nilai_komparatif_unit": 1.003282911464764,
			"summary_team": 0.8823529411764706
		}]
	},
	"920140": {
		"nik": "920140",
		"nama": "ARI PRIYANTI",
		"band": "V",
		"posisi": "AUDITOR PRATAMA II",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0653538584566113,
			"nilai_komparatif_unit": 1.0660010506636277,
			"summary_team": 0.93
		}]
	},
	"920144": {
		"nik": "920144",
		"nama": "MOHAMAD IZZATULLAH FATIH",
		"band": "V",
		"posisi": "PENDIDIKAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999998,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090913,
			"nilai_komparatif_unit": 1.0915718076736625,
			"summary_team": 0.9272727272727272
		}]
	},
	"920150": {
		"nik": "920150",
		"nama": "FAUZI FEBRY SANI",
		"band": "V",
		"posisi": "OFF 2 BUSINESS & PRODUCT LINE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555552,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0313216195569141,
			"nilai_komparatif_unit": 1.0319481375066137,
			"summary_team": 0.8823529411764706
		}]
	},
	"920156": {
		"nik": "920156",
		"nama": "SEKAR MUTAQINA",
		"band": "V",
		"posisi": "OFF 2 CAPEX ALIGNMENT & SOLUTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777772,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9918093819806407,
			"nilai_komparatif_unit": 0.9924118966266134,
			"summary_team": 0.8705882352941174
		}]
	},
	"920161": {
		"nik": "920161",
		"nama": "AHMAD FITRIADI",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP VALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8799999999999992,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0427807486631027,
			"nilai_komparatif_unit": 1.0434142279233545,
			"summary_team": 0.9176470588235295
		}]
	},
	"920173": {
		"nik": "920173",
		"nama": "NIRMALA TIKA",
		"band": "V",
		"posisi": "AUDITOR PRATAMA II",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.047352487576392,
			"nilai_komparatif_unit": 1.0479887441239966,
			"summary_team": 0.9142857142857141
		}]
	},
	"920174": {
		"nik": "920174",
		"nama": "HADYAN ZAKI NUGRAHA",
		"band": "V",
		"posisi": "OFF 2 EMPLOYEE ACTUALIZATION PLAN & EVAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8212121212121206,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9741697416974174,
			"nilai_komparatif_unit": 0.9747615404318315,
			"summary_team": 0.7999999999999998
		}]
	},
	"920207": {
		"nik": "920207",
		"nama": "VALENTINO OKTAWIJAYA",
		"band": "V",
		"posisi": "OFF 2 IT SYSTEM DEVELOPMENT SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444444,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9473684210526314,
			"nilai_komparatif_unit": 0.9479439382429169,
			"summary_team": 0.7999999999999998
		}]
	},
	"920209": {
		"nik": "920209",
		"nama": "QOLIQINA ZOLLA SABRINA",
		"band": "V",
		"posisi": "OFF 2 LEGAL PARTNERSHIP SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333337,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999994,
			"nilai_komparatif_unit": 0.960583190752822,
			"summary_team": 0.7999999999999998
		}]
	},
	"920210": {
		"nik": "920210",
		"nama": "RIEN NISA",
		"band": "V",
		"posisi": "OFF 2 DECISION SUPPORT & EIS SYSTEM SUPP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.983606557377049,
			"nilai_komparatif_unit": 0.9842040888860886,
			"summary_team": 0.7999999999999999
		}]
	},
	"920214": {
		"nik": "920214",
		"nama": "ACHMAD NASHIRUDIN",
		"band": "V",
		"posisi": "JUNIOR PRODUCT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8527777777777763,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.076068212301209,
			"nilai_komparatif_unit": 1.0767219133749804,
			"summary_team": 0.9176470588235295
		}]
	},
	"920215": {
		"nik": "920215",
		"nama": "LEIDI YELSYA NOVANDA",
		"band": "V",
		"posisi": "JUNIOR DESIGNER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8588235294117645,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9980430528375736,
			"nilai_komparatif_unit": 0.9986493543785462,
			"summary_team": 0.8571428571428571
		}]
	},
	"920226": {
		"nik": "920226",
		"nama": "IMAM HERDIANSYAH",
		"band": "V",
		"posisi": "OFF 2 CULTURAL DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8277777777777777,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0232925384919067,
			"nilai_komparatif_unit": 1.0239141788521993,
			"summary_team": 0.8470588235294116
		}]
	},
	"920228": {
		"nik": "920228",
		"nama": "ARYA PRADANA NUGRAHANDITO",
		"band": "V",
		"posisi": "JUNIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8422222222222243,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.075585907186091,
			"nilai_komparatif_unit": 1.0762393152641505,
			"summary_team": 0.9058823529411766
		}]
	},
	"920234": {
		"nik": "920234",
		"nama": "ANDREW SETIAWAN TARIGAN",
		"band": "V",
		"posisi": "JUNIOR PRODUCT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8527777777777763,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9381107491856693,
			"nilai_komparatif_unit": 0.9386806424294699,
			"summary_team": 0.7999999999999999
		}]
	},
	"920235": {
		"nik": "920235",
		"nama": "RAMADHAN WIJAYANTO",
		"band": "V",
		"posisi": "OFF 2 NFV NETWORK & SECURITY DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0153846153846153,
			"nilai_komparatif_unit": 1.016001451757793,
			"summary_team": 0.88
		}]
	},
	"920236": {
		"nik": "920236",
		"nama": "GINANJAR ANDHANG RAMADHAN",
		"band": "V",
		"posisi": "OFF 2 DATA ACCESS MIGRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0307692307692304,
			"nilai_komparatif_unit": 1.0313954131480625,
			"summary_team": 0.8933333333333333
		}]
	},
	"920238": {
		"nik": "920238",
		"nama": "TEUKU RIFQI ADELU BARCHIA",
		"band": "V",
		"posisi": "OFF 2 EMPLOYEE TERMINATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666657,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9725274725274735,
			"nilai_komparatif_unit": 0.973118273599186,
			"summary_team": 0.8428571428571427
		}]
	},
	"920239": {
		"nik": "920239",
		"nama": "ANDHIKA APROMULO GAHARU",
		"band": "V",
		"posisi": "OFF 2 ACCOUNTING OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8980392156862742,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9544603867747974,
			"nilai_komparatif_unit": 0.9550402122659458,
			"summary_team": 0.857142857142857
		}]
	},
	"920244": {
		"nik": "920244",
		"nama": "ANSHAR ABDULLAH",
		"band": "V",
		"posisi": "OFF 2 IT MANAGEMENT SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444444,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0657894736842104,
			"nilai_komparatif_unit": 1.0664369305232815,
			"summary_team": 0.8999999999999999
		}]
	},
	"920248": {
		"nik": "920248",
		"nama": "AKHIRUL HAJRI",
		"band": "V",
		"posisi": "OFF 2 IP MULTIMEDIA SUB SYSTEM 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9934640522875825,
			"nilai_komparatif_unit": 0.9940675721298281,
			"summary_team": 0.8444444444444443
		}]
	},
	"920249": {
		"nik": "920249",
		"nama": "TRI YATMOKO YULIANDONO",
		"band": "V",
		"posisi": "OFF 2 INFRASTRUCTURE CATEGORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346935,
			"nilai_komparatif_unit": 0.9801869293396145,
			"summary_team": 0.8
		}]
	},
	"920250": {
		"nik": "920250",
		"nama": "ERFAN SOFHA",
		"band": "V",
		"posisi": "OFF 2 BIG DATA ANALYTICS EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.983606557377049,
			"nilai_komparatif_unit": 0.9842040888860886,
			"summary_team": 0.7999999999999999
		}]
	},
	"920255": {
		"nik": "920255",
		"nama": "HARTANTO PRABOWO",
		"band": "V",
		"posisi": "OFF 2 TRIBE PLANNING & PERFORMANCE II",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8238095238095239,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0593799264319494,
			"nilai_komparatif_unit": 1.0600234895328047,
			"summary_team": 0.8727272727272727
		}]
	},
	"920259": {
		"nik": "920259",
		"nama": "AZWAR TILAMEO",
		"band": "V",
		"posisi": "OFF 2 TRANSPORT SKSO & RMJ 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9182389937106916,
			"nilai_komparatif_unit": 0.9187968150544553,
			"summary_team": 0.8111111111111111
		}]
	},
	"920260": {
		"nik": "920260",
		"nama": "UTAMI NAZMI PUSPAHATI",
		"band": "V",
		"posisi": "OFF 2 OPEX SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8277777777777782,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0201342281879189,
			"nilai_komparatif_unit": 1.0207539499051241,
			"summary_team": 0.8444444444444443
		}]
	},
	"920267": {
		"nik": "920267",
		"nama": "BONDAN BHASKARA",
		"band": "V",
		"posisi": "OFF 2 BIG DATA PLTFRM&AI CAPABILITY DEVT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8685185185185175,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0023830427693479,
			"nilai_komparatif_unit": 1.0029919808123993,
			"summary_team": 0.8705882352941177
		}]
	},
	"920269": {
		"nik": "920269",
		"nama": "ARIEF FAIZIN",
		"band": "V",
		"posisi": "JUNIOR DATA SCIENTIST",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666665,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0180995475113124,
			"nilai_komparatif_unit": 1.0187180331796055,
			"summary_team": 0.8823529411764706
		}]
	},
	"920272": {
		"nik": "920272",
		"nama": "SOETJIPTO",
		"band": "V",
		"posisi": "OFF 2 GOVERNMENT COMM. CHANEL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8722222222222226,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0251030348445105,
			"nilai_komparatif_unit": 1.0257257750638977,
			"summary_team": 0.8941176470588235
		}]
	},
	"920273": {
		"nik": "920273",
		"nama": "FAHMI HIDAYATULLAH",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0806549423893212,
			"nilai_komparatif_unit": 1.0813114298574396,
			"summary_team": 0.8999999999999999
		}]
	},
	"920276": {
		"nik": "920276",
		"nama": "LARA SORAYA RIFANA",
		"band": "V",
		"posisi": "OFF 2 SUBSIDIARY ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.862745098039215,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.00909090909091,
			"nilai_komparatif_unit": 1.0097039220981383,
			"summary_team": 0.8705882352941177
		}]
	},
	"920279": {
		"nik": "920279",
		"nama": "MOHAMMAD FIRMAN FADILLAH",
		"band": "V",
		"posisi": "OFF 2 SCANNING, MONITORING &THREAT INTEL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8313725490196076,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9622641509433965,
			"nilai_komparatif_unit": 0.9628487171461078,
			"summary_team": 0.7999999999999999
		}]
	},
	"920282": {
		"nik": "920282",
		"nama": "ALDEN HARIYANTO PUTRA",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7851851851851854,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0552560646900264,
			"nilai_komparatif_unit": 1.0558971225845963,
			"summary_team": 0.8285714285714284
		}]
	},
	"920286": {
		"nik": "920286",
		"nama": "Novita Rohmaningtyas",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8604938271604932,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9508282248597894,
			"nilai_komparatif_unit": 0.9514058438475612,
			"summary_team": 0.8181818181818181
		}]
	},
	"920288": {
		"nik": "920288",
		"nama": "THEOFILUS PURBA",
		"band": "V",
		"posisi": "OFF 2 BB & CORE PROCUREMENT PROCESS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0499999999999998,
			"nilai_komparatif_unit": 1.0506378648858996,
			"summary_team": 0.84
		}]
	},
	"920292": {
		"nik": "920292",
		"nama": "JOVIANO RINALDI ELAN PATHIBANG",
		"band": "V",
		"posisi": "OFF 2 ENTERPRISE DATA SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7822222222222212,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.977606951871659,
			"nilai_komparatif_unit": 0.9782008386781451,
			"summary_team": 0.7647058823529411
		}]
	},
	"920294": {
		"nik": "920294",
		"nama": "INTAN DEA YUTAMI",
		"band": "V",
		"posisi": "OFF 2 PDI PREPARATION DIGI TELCO 3",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9148148148148144,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0030959752321984,
			"nilai_komparatif_unit": 1.0037053463748535,
			"summary_team": 0.9176470588235293
		}]
	},
	"920295": {
		"nik": "920295",
		"nama": "MAYA SUSANTI",
		"band": "V",
		"posisi": "OFF 2 HC COMMUNICATION PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8374999999999996,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0234541577825162,
			"nilai_komparatif_unit": 1.024075896324971,
			"summary_team": 0.857142857142857
		}]
	},
	"920298": {
		"nik": "920298",
		"nama": "MUTIARA IJMI RIWINDA",
		"band": "V",
		"posisi": "OFF 2 ACCOUNTING ASSESSMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.987723214285714,
			"nilai_komparatif_unit": 0.9883232466241719,
			"summary_team": 0.8428571428571427
		}]
	},
	"920300": {
		"nik": "920300",
		"nama": "ANGGITA SARI DEWI",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8231884057971014,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0004142502071252,
			"nilai_komparatif_unit": 1.0010219922276593,
			"summary_team": 0.8235294117647058
		}]
	},
	"920302": {
		"nik": "920302",
		"nama": "FAISAL KHALID",
		"band": "V",
		"posisi": "OFF 2 SERVICE QUALITY MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.961538461538461,
			"nilai_komparatif_unit": 0.962122586891849,
			"summary_team": 0.8333333333333333
		}]
	},
	"920304": {
		"nik": "920304",
		"nama": "REZA AKHMAD GANDARA",
		"band": "V",
		"posisi": "JUNIOR PRODUCT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666665,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0180995475113124,
			"nilai_komparatif_unit": 1.0187180331796055,
			"summary_team": 0.8823529411764706
		}]
	},
	"920305": {
		"nik": "920305",
		"nama": "MARANDIKA EKA SAPUTRA",
		"band": "V",
		"posisi": "OFF 2 LEGAL ADMINISTRATION & COMPLIANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8694444444444439,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.028378124412705,
			"nilai_komparatif_unit": 1.0290028542174576,
			"summary_team": 0.8941176470588235
		}]
	},
	"920306": {
		"nik": "920306",
		"nama": "SALLY FAHDARINA",
		"band": "V",
		"posisi": "OFF 2 PLATFORM PLANNING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9848484848484845,
			"nilai_komparatif_unit": 0.9854467708165001,
			"summary_team": 0.8666666666666665
		}]
	},
	"920308": {
		"nik": "920308",
		"nama": "RIDWAN YUSUF SURYO GUMILANG",
		"band": "V",
		"posisi": "OFF 2 TALENT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8222222222222221,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1017488076311606,
			"nilai_komparatif_unit": 1.1024181094192271,
			"summary_team": 0.9058823529411764
		}]
	},
	"920310": {
		"nik": "920310",
		"nama": "HENDITA KHAIRINA PUTRI",
		"band": "V",
		"posisi": "OFF 2 PENGELOLA PROGRAM KERJA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200685,
			"nilai_komparatif_unit": 1.0503804187048444,
			"summary_team": 0.9272727272727272
		}]
	},
	"920312": {
		"nik": "920312",
		"nama": "ANDYKA DIMAS AGASSI",
		"band": "V",
		"posisi": "OFF 2 BOD FACILITY POLICY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8206349206349192,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0178632381385841,
			"nilai_komparatif_unit": 1.0184815802512097,
			"summary_team": 0.8352941176470587
		}]
	},
	"920313": {
		"nik": "920313",
		"nama": "PUTU HARINI",
		"band": "V",
		"posisi": "OFF 2 LEGAL ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8694444444444439,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9471903777485441,
			"nilai_komparatif_unit": 0.9477657867792373,
			"summary_team": 0.8235294117647058
		}]
	},
	"920318": {
		"nik": "920318",
		"nama": "ANIES SAYYIDATUN NISA",
		"band": "V",
		"posisi": "OFF 2 EXECUTIVE DATA & REPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8368421052631614,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0543840177580421,
			"nilai_komparatif_unit": 1.055024545892501,
			"summary_team": 0.8823529411764706
		}]
	},
	"920319": {
		"nik": "920319",
		"nama": "MAYANG DIANTY PRAMESWARI KATIM",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8691358024691347,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0041322314049599,
			"nilai_komparatif_unit": 1.0047422320632584,
			"summary_team": 0.8727272727272727
		}]
	},
	"920320": {
		"nik": "920320",
		"nama": "DINDIN ZAENUDIN",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0635016893355225,
			"nilai_komparatif_unit": 1.0641477563676387,
			"summary_team": 0.8857142857142857
		}]
	},
	"920330": {
		"nik": "920330",
		"nama": "RIDHO MURTADHA BARASILA",
		"band": "V",
		"posisi": "OFF 2 RESOURCE & BUDGET CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828875,
			"nilai_komparatif_unit": 1.005958332562002,
			"summary_team": 0.8545454545454546
		}]
	},
	"920333": {
		"nik": "920333",
		"nama": "BADZLINA ANINDYKA",
		"band": "V",
		"posisi": "OFF 2 GOVERNANCE & RISK MGT SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9531249999999998,
			"nilai_komparatif_unit": 0.9537040142565456,
			"summary_team": 0.8133333333333332
		}]
	},
	"920334": {
		"nik": "920334",
		"nama": "MIRYAM OMPUSUNGGU",
		"band": "V",
		"posisi": "OFF 2 ASSET SETTLEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9599999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9821428571428571,
			"nilai_komparatif_unit": 0.9827394994681035,
			"summary_team": 0.9428571428571426
		}]
	},
	"926305": {
		"nik": "926305",
		"nama": "ANANG TRI PURNOMO AJI",
		"band": "V",
		"posisi": "JUNIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.749999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8470588235294129,
			"nilai_komparatif_unit": 0.8475734036054328,
			"summary_team": 0.6352941176470588
		}]
	},
	"930011": {
		"nik": "930011",
		"nama": "DHIKA FEBRIANOV",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9831932773109242,
			"nilai_komparatif_unit": 0.9837905577563045,
			"summary_team": 0.9176470588235295
		}]
	},
	"930014": {
		"nik": "930014",
		"nama": "EVAN NARATAMA",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.982413583990292,
			"nilai_komparatif_unit": 0.9830103907794904,
			"summary_team": 0.8181818181818181
		}]
	},
	"930024": {
		"nik": "930024",
		"nama": "DAVID CUH SIHOTANG",
		"band": "V",
		"posisi": "JUNIOR PRODUCT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666665,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.072398190045249,
			"nilai_komparatif_unit": 1.0730496616158511,
			"summary_team": 0.9294117647058824
		}]
	},
	"930025": {
		"nik": "930025",
		"nama": "IMAMUL AKHYAR",
		"band": "V",
		"posisi": "OFF 2 MARKET & PORTFOLIO",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8390804597701141,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.004500978473582,
			"nilai_komparatif_unit": 1.0051112031421727,
			"summary_team": 0.8428571428571427
		}]
	},
	"930031": {
		"nik": "930031",
		"nama": "DIMAS EGGI",
		"band": "V",
		"posisi": "OFF 2 INTERNAL ACTIVATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8977777777777771,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9828188701223071,
			"nilai_komparatif_unit": 0.9834159231189268,
			"summary_team": 0.8823529411764706
		}]
	},
	"930049": {
		"nik": "930049",
		"nama": "BAGAS SATYAPARAHATMA",
		"band": "V",
		"posisi": "OFF 2 PERFORMANCE EVALUATION & REPORTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0085763293310461,
			"nilai_komparatif_unit": 1.009189029736027,
			"summary_team": 0.8909090909090909
		}]
	},
	"930055": {
		"nik": "930055",
		"nama": "RINA FITRIANA",
		"band": "V",
		"posisi": "JUNIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.872222222222221,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.957662045710005,
			"nilai_komparatif_unit": 0.9582438161781167,
			"summary_team": 0.8352941176470587
		}]
	},
	"930056": {
		"nik": "930056",
		"nama": "BUTET IKA FITRINA SIREGAR",
		"band": "V",
		"posisi": "OFF 2 FINANCE COUNCIL ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.862745098039215,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0431818181818189,
			"nilai_komparatif_unit": 1.04381554108794,
			"summary_team": 0.8999999999999999
		}]
	},
	"930057": {
		"nik": "930057",
		"nama": "TAUFIK HENDRA LUKMANA",
		"band": "V",
		"posisi": "OFF 2 MEDIA PLACEMENT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8911111111111103,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.976969341352502,
			"nilai_komparatif_unit": 0.9775628408167395,
			"summary_team": 0.8705882352941177
		}]
	},
	"930063": {
		"nik": "930063",
		"nama": "CHANDRA WULANDARI",
		"band": "V",
		"posisi": "OFF 2 PROCUREMENT CONTRACT DRAFTER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0303030303030303,
			"nilai_komparatif_unit": 1.0309289294695696,
			"summary_team": 0.9272727272727272
		}]
	},
	"930066": {
		"nik": "930066",
		"nama": "ANNISA ARNIZANTHYA",
		"band": "V",
		"posisi": "OFF 2 CONTRACT COMPLIANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.856249999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0010427528675716,
			"nilai_komparatif_unit": 1.001650876697418,
			"summary_team": 0.8571428571428571
		}]
	},
	"930068": {
		"nik": "930068",
		"nama": "FATIMA ZAHRA KANNES",
		"band": "V",
		"posisi": "OFF 2 OPERATIONAL CATEGORY ANALYST",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9780564263322883,
			"nilai_komparatif_unit": 0.9786505861901796,
			"summary_team": 0.9454545454545454
		}]
	},
	"930073": {
		"nik": "930073",
		"nama": "ANIS EVIENTYA",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP VALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8799999999999992,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9491978609625676,
			"nilai_komparatif_unit": 0.9497744895199763,
			"summary_team": 0.8352941176470587
		}]
	},
	"930077": {
		"nik": "930077",
		"nama": "TIFFANY CHRISY MONIKA SITOMPUL",
		"band": "V",
		"posisi": "OFF 2 ENT BIZ PORTFOLIO DATA MINING&SUPP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9277777777777777,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0299401197604792,
			"nilai_komparatif_unit": 1.0305657984623597,
			"summary_team": 0.9555555555555555
		}]
	},
	"930078": {
		"nik": "930078",
		"nama": "BANINDA YURISULHIKMAH",
		"band": "V",
		"posisi": "OFF 2 JOB PROFILE REVIEW & ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8377777777777772,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0391636760805125,
			"nilai_komparatif_unit": 1.0397949580040118,
			"summary_team": 0.8705882352941177
		}]
	},
	"930081": {
		"nik": "930081",
		"nama": "ADHELA KURNIARTHA SEKAR ARUM",
		"band": "V",
		"posisi": "OFF 2 LOGISTIK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090908,
			"nilai_komparatif_unit": 1.091571807673662,
			"summary_team": 0.9272727272727272
		}]
	},
	"930083": {
		"nik": "930083",
		"nama": "MUHAMMAD FATONI",
		"band": "V",
		"posisi": "OFF 2 IT APPLICATION DESIGN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090908,
			"nilai_komparatif_unit": 1.091571807673662,
			"summary_team": 0.9818181818181818
		}]
	},
	"930088": {
		"nik": "930088",
		"nama": "MUHAMMAD FAZLURRAHMAN ARIEF",
		"band": "V",
		"posisi": "OFF 2 TRIBE SYSTEM DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666664,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0588235294117652,
			"nilai_komparatif_unit": 1.0594667545067902,
			"summary_team": 0.9176470588235295
		}]
	},
	"930095": {
		"nik": "930095",
		"nama": "MIA MULIANINGSIH",
		"band": "V",
		"posisi": "OFF 2 FINANCING ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.92,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9782608695652173,
			"nilai_komparatif_unit": 0.9788551536204033,
			"summary_team": 0.8999999999999999
		}]
	},
	"930114": {
		"nik": "930114",
		"nama": "STEFFI SUSYATI FRANSISKA HUTASOIT",
		"band": "V",
		"posisi": "OFF 2 LEGAL ADMINISTRATION & COMPLIANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8694444444444439,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.055440706634092,
			"nilai_komparatif_unit": 1.0560818766968645,
			"summary_team": 0.9176470588235295
		}]
	},
	"930115": {
		"nik": "930115",
		"nama": "ALDHIRA PRAMANTARI",
		"band": "V",
		"posisi": "OFF 2 LEGAL RESEARCH & ANALYST",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8743589743589737,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9149560117302058,
			"nilai_komparatif_unit": 0.9155118386940396,
			"summary_team": 0.7999999999999998
		}]
	},
	"930125": {
		"nik": "930125",
		"nama": "LUKE SAMANTHA TIYAS",
		"band": "V",
		"posisi": "OFF 2 AUDIT REPORTING & MONITORING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8592592592592581,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9858012170385408,
			"nilai_komparatif_unit": 0.9864000817821847,
			"summary_team": 0.8470588235294116
		}]
	},
	"930126": {
		"nik": "930126",
		"nama": "DESTY EKA PUTRI",
		"band": "V",
		"posisi": "OFF 2 CFU MANAGEMENT REPORTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.862745098039215,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9409090909090917,
			"nilai_komparatif_unit": 0.9414806841185343,
			"summary_team": 0.8117647058823529
		}]
	},
	"930130": {
		"nik": "930130",
		"nama": "FAZA RASYADAN ARFANDIKA",
		"band": "V",
		"posisi": "OFF 2 HC COMPLIANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666657,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0180995475113133,
			"nilai_komparatif_unit": 1.0187180331796064,
			"summary_team": 0.8823529411764706
		}]
	},
	"930134": {
		"nik": "930134",
		"nama": "DEAR AHMAD A'DHOMUL AMAL SYAFA'AT",
		"band": "V",
		"posisi": "OFF 2 PROCESS SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8071428571428563,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9474232170744413,
			"nilai_komparatif_unit": 0.9479987675527822,
			"summary_team": 0.7647058823529411
		}]
	},
	"930135": {
		"nik": "930135",
		"nama": "FADJAR RAMADHAN",
		"band": "V",
		"posisi": "OFF 2 DATA ARCHITECT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0303030303030303,
			"nilai_komparatif_unit": 1.0309289294695696,
			"summary_team": 0.9272727272727272
		}]
	},
	"930139": {
		"nik": "930139",
		"nama": "FIVTINA MARBELANTY",
		"band": "V",
		"posisi": "OFF 2 RISK REVIEWER & ASSESSMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.826666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9250474383301714,
			"nilai_komparatif_unit": 0.9256093957384594,
			"summary_team": 0.7647058823529411
		}]
	},
	"930140": {
		"nik": "930140",
		"nama": "GALIH DIAN HUTAMA",
		"band": "V",
		"posisi": "OFF 2 APP DEVT & MAINTENANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9910714285714286,
			"nilai_komparatif_unit": 0.9916734949178135,
			"summary_team": 0.925
		}]
	},
	"930141": {
		"nik": "930141",
		"nama": "KURNIAWAN ADINA KUSUMA",
		"band": "V",
		"posisi": "OFF 2 CAREER & TALENT DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8419753086419745,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0689149560117313,
			"nilai_komparatif_unit": 1.0695643115512101,
			"summary_team": 0.9
		}]
	},
	"930142": {
		"nik": "930142",
		"nama": "AHSANIA MURTI PADMARANI",
		"band": "V",
		"posisi": "OFF 2 TREASURY SERVICES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0180995475113124,
			"nilai_komparatif_unit": 1.0187180331796055,
			"summary_team": 0.8823529411764706
		}]
	},
	"930143": {
		"nik": "930143",
		"nama": "ANINDYA PUTRI ARFYANTARI",
		"band": "V",
		"posisi": "AUDITOR PRATAMA II",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1340863654538123,
			"nilai_komparatif_unit": 1.1347753119967652,
			"summary_team": 0.9900000000000001
		}]
	},
	"930145": {
		"nik": "930145",
		"nama": "KARINA NURUL AYUNINGTYAS",
		"band": "V",
		"posisi": "OFF 2 SOA PLATFORM DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9714285714285711,
			"nilai_komparatif_unit": 0.9720187049284511,
			"summary_team": 0.9066666666666665
		}]
	},
	"930147": {
		"nik": "930147",
		"nama": "DYAS NURRAHMAWATI",
		"band": "V",
		"posisi": "OFF 2 INDIGO INCUBATION PROGRAM MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8592592592592588,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0268762677484793,
			"nilai_komparatif_unit": 1.027500085189775,
			"summary_team": 0.8823529411764706
		}]
	},
	"930148": {
		"nik": "930148",
		"nama": "REZKI ADITIAN",
		"band": "V",
		"posisi": "AUDITOR PRATAMA II",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.024249123879854,
			"nilai_komparatif_unit": 1.0248713453565552,
			"summary_team": 0.8941176470588235
		}]
	},
	"930154": {
		"nik": "930154",
		"nama": "FIRDAUS NUTRIHADI",
		"band": "V",
		"posisi": "OFF 2 SUPPORT READINESS & ADM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0346540116984828,
			"nilai_komparatif_unit": 1.0352825540443091,
			"summary_team": 0.8823529411764706
		}]
	},
	"930156": {
		"nik": "930156",
		"nama": "ANTON SUHARTONO",
		"band": "V",
		"posisi": "OFF 2 BIG DATA ANALYTCS EXTRNL USE CASES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.94847775175644,
			"nilai_komparatif_unit": 0.9490539428544424,
			"summary_team": 0.7714285714285712
		}]
	},
	"930157": {
		"nik": "930157",
		"nama": "AHMAD REZA DWI PERMANA",
		"band": "V",
		"posisi": "OFF 2 CONSOLIDATION PROCESS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9910714285714286,
			"nilai_komparatif_unit": 0.9916734949178135,
			"summary_team": 0.925
		}]
	},
	"930162": {
		"nik": "930162",
		"nama": "MUHAMMAD FADHIL",
		"band": "V",
		"posisi": "JUNIOR PRODUCT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.872222222222221,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9711502435369064,
			"nilai_komparatif_unit": 0.9717402079552732,
			"summary_team": 0.8470588235294116
		}]
	},
	"930163": {
		"nik": "930163",
		"nama": "MUHAMMAD RAJA IHSAN",
		"band": "V",
		"posisi": "OFF 2 INDIGO STARTUP ASSESSMENT MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8740740740740738,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0094715852442675,
			"nilai_komparatif_unit": 1.010084829508592,
			"summary_team": 0.8823529411764706
		}]
	},
	"930170": {
		"nik": "930170",
		"nama": "DERI FADHILAH ARIFINSYAH",
		"band": "V",
		"posisi": "OFF 2 INTERNAL ACTIVATION & BRANDING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8977777777777771,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9435061153174149,
			"nilai_komparatif_unit": 0.9440792861941698,
			"summary_team": 0.8470588235294119
		}]
	},
	"930175": {
		"nik": "930175",
		"nama": "EDWIN SOFYAN LATHIFUDDIN",
		"band": "V",
		"posisi": "OFF 2 STRATEGIC PLANNING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8694444444444437,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0148468333020118,
			"nilai_komparatif_unit": 1.0154633429777544,
			"summary_team": 0.8823529411764706
		}]
	},
	"930184": {
		"nik": "930184",
		"nama": "PINGKAN PRISILIA ISTRA",
		"band": "V",
		"posisi": "OFF 2 USER JOURNEY ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8753623188405791,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.007985975847293,
			"nilai_komparatif_unit": 1.0085983176182192,
			"summary_team": 0.8823529411764706
		}]
	},
	"930185": {
		"nik": "930185",
		"nama": "ZETIL HIKMAH",
		"band": "V",
		"posisi": "JUNIOR PRODUCT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7333333333333337,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.2077922077922072,
			"nilai_komparatif_unit": 1.2085259299244109,
			"summary_team": 0.8857142857142857
		}]
	},
	"930190": {
		"nik": "930190",
		"nama": "MUHAMMAD MIZAN GHIFARI",
		"band": "V",
		"posisi": "OFF 2 BUDGET OPEX & CAPEX MONITORING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300696,
			"nilai_komparatif_unit": 1.070580042141476,
			"summary_team": 0.9272727272727272
		}]
	},
	"930193": {
		"nik": "930193",
		"nama": "MUHAMMAD RIZAL PRIHANDOKO",
		"band": "V",
		"posisi": "OFF 2 INDIGO INCUBATION PROGRAM MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8592592592592588,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9721095334685602,
			"nilai_komparatif_unit": 0.9727000806463202,
			"summary_team": 0.8352941176470587
		}]
	},
	"930196": {
		"nik": "930196",
		"nama": "LULUK LISTYANI AYUNINGTYAS",
		"band": "V",
		"posisi": "OFF 2 DATA INVENTORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777781,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9493670886075943,
			"nilai_komparatif_unit": 0.9499438199691673,
			"summary_team": 0.8333333333333331
		}]
	},
	"930213": {
		"nik": "930213",
		"nama": "ANINDITA PUTRI APRILIANI",
		"band": "V",
		"posisi": "OFF 2 DIGITAL BUSINESS PARENTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8289855072463764,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0076100370218022,
			"nilai_komparatif_unit": 1.008222150413513,
			"summary_team": 0.8352941176470587
		}]
	},
	"930214": {
		"nik": "930214",
		"nama": "MUHAMMAD AULIA FIRMANSYAH",
		"band": "V",
		"posisi": "OFF 2 WORKFORCE MANAGEMENT SYSTEM ADM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8746031746031734,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9299455535390212,
			"nilai_komparatif_unit": 0.9305104865051175,
			"summary_team": 0.8133333333333332
		}]
	},
	"930217": {
		"nik": "930217",
		"nama": "HARYO PRADIPTA BAYUWEGA",
		"band": "V",
		"posisi": "OFF 2 IT PROCUREMENT PROCESS & ADM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8148148148148151,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.047272727272727,
			"nilai_komparatif_unit": 1.0479089353667153,
			"summary_team": 0.8533333333333333
		}]
	},
	"930219": {
		"nik": "930219",
		"nama": "Muhammad Hari Diputera",
		"band": "V",
		"posisi": "OFF 2 API OPERATION & INTEGRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333337,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9007506255212673,
			"nilai_komparatif_unit": 0.9012978228498123,
			"summary_team": 0.8181818181818181
		}]
	},
	"930224": {
		"nik": "930224",
		"nama": "HERJUNA DONY ANGGARA PUTRA",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8691358024691347,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0250516528925633,
			"nilai_komparatif_unit": 1.0256743618979096,
			"summary_team": 0.8909090909090909
		}]
	},
	"930225": {
		"nik": "930225",
		"nama": "HERBANU MAHARINDRO",
		"band": "V",
		"posisi": "JUNIOR DESIGNER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9222222222222223,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.033309709425939,
			"nilai_komparatif_unit": 1.0339374351210837,
			"summary_team": 0.9529411764705882
		}]
	},
	"930230": {
		"nik": "930230",
		"nama": "NADYA AMALIA",
		"band": "V",
		"posisi": "AUDITOR PRATAMA II",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0473524875763918,
			"nilai_komparatif_unit": 1.0479887441239963,
			"summary_team": 0.914285714285714
		}]
	},
	"930232": {
		"nik": "930232",
		"nama": "NANDA SEPTIANA",
		"band": "V",
		"posisi": "JUNIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8724637681159412,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0113347664647263,
			"nilai_komparatif_unit": 1.0119491425936953,
			"summary_team": 0.8823529411764706
		}]
	},
	"930234": {
		"nik": "930234",
		"nama": "RIZA FAUZI RAHMAN HARAHAP",
		"band": "V",
		"posisi": "OFF 2 INTERNET PRODUCT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0149253731343282,
			"nilai_komparatif_unit": 1.0155419305222626,
			"summary_team": 0.9066666666666665
		}]
	},
	"930235": {
		"nik": "930235",
		"nama": "PUTI HARIFIA AMANAH",
		"band": "V",
		"posisi": "OFF 2 PARTNER & CHANNEL RELATION DEVT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9358974358974357,
			"nilai_komparatif_unit": 0.9364659845747334,
			"summary_team": 0.8111111111111111
		}]
	},
	"930237": {
		"nik": "930237",
		"nama": "KARTIKA MAHARANI",
		"band": "V",
		"posisi": "OFF 2 SUPPLY CHAIN MANAGEMENT PLATFORM D",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.969230769230769,
			"nilai_komparatif_unit": 0.9698195675869842,
			"summary_team": 0.84
		}]
	},
	"930238": {
		"nik": "930238",
		"nama": "RIAN WIDYOTOMO",
		"band": "V",
		"posisi": "OFF 2 ACCOUNTING ASESSMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9902597402597398,
			"nilai_komparatif_unit": 0.990861313513294,
			"summary_team": 0.8714285714285712
		}]
	},
	"930240": {
		"nik": "930240",
		"nama": "NUGROHO BUDI WICAKSONO",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.876190476190476,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0583003952569172,
			"nilai_komparatif_unit": 1.0589433025529822,
			"summary_team": 0.9272727272727272
		}]
	},
	"930241": {
		"nik": "930241",
		"nama": "MUHAMMAD FARIZKO NURDITAMA",
		"band": "V",
		"posisi": "OFF 2 LEARNING DEVT EXECUTION & REPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769228,
			"nilai_komparatif_unit": 0.9236376834161752,
			"summary_team": 0.7999999999999998
		}]
	},
	"930249": {
		"nik": "930249",
		"nama": "DWINDA ANDRIANSYAH",
		"band": "V",
		"posisi": "OFF 2 LEGAL SETTLEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8392156862745095,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0093457943925237,
			"nilai_komparatif_unit": 1.0099589622401175,
			"summary_team": 0.8470588235294116
		}]
	},
	"930252": {
		"nik": "930252",
		"nama": "DWITA KUSMAWATI",
		"band": "V",
		"posisi": "OFF 2 ACCOUNTING CONTROL SUBSIDIARY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9133333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0792492179353488,
			"nilai_komparatif_unit": 1.0799048514394018,
			"summary_team": 0.9857142857142855
		}]
	},
	"930260": {
		"nik": "930260",
		"nama": "RIZKY VINDI NURYAHYA",
		"band": "V",
		"posisi": "OFF 2 ANGGARAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7666666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9723320158102766,
			"nilai_komparatif_unit": 0.9729226981439161,
			"summary_team": 0.7454545454545454
		}]
	},
	"930263": {
		"nik": "930263",
		"nama": "BUANA INDRAPURA",
		"band": "V",
		"posisi": "OFF 2 TRANSACT&STRAT CONTRACT COMPLIANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.856249999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9343065693430667,
			"nilai_komparatif_unit": 0.9348741515842566,
			"summary_team": 0.7999999999999998
		}]
	},
	"930265": {
		"nik": "930265",
		"nama": "NUR FITRI APRILIA",
		"band": "V",
		"posisi": "OFF 2 DIGITAL BUSINESS MARKETING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0380622837370252,
			"nilai_komparatif_unit": 1.038692896575285,
			"summary_team": 0.8823529411764706
		}]
	},
	"930273": {
		"nik": "930273",
		"nama": "KHAIRUL ARIFIN",
		"band": "V",
		"posisi": "OFF 2 EXECUTIVE SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8368421052631614,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0122086570477205,
			"nilai_komparatif_unit": 1.0128235640568009,
			"summary_team": 0.8470588235294116
		}]
	},
	"930279": {
		"nik": "930279",
		"nama": "NOOR IRSALINA",
		"band": "V",
		"posisi": "OFF 2 TRIBE PLANNING & PERFORMANCE I",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8633333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.87699944842802,
			"nilai_komparatif_unit": 0.8775322171452635,
			"summary_team": 0.757142857142857
		}]
	},
	"930282": {
		"nik": "930282",
		"nama": "MUHAMMAD GUNAWAN HENDRO MARTOYO",
		"band": "V",
		"posisi": "OFF 2 ACCOUNTING CONTROL SUBSIDIARY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9133333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9854014598540143,
			"nilai_komparatif_unit": 0.9860000817490192,
			"summary_team": 0.8999999999999999
		}]
	},
	"930289": {
		"nik": "930289",
		"nama": "SYAHID YOGGA PRAHASTA",
		"band": "V",
		"posisi": "OFF 2 GCT REPORTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8790123456790117,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0238764044943827,
			"nilai_komparatif_unit": 1.0244983995476475,
			"summary_team": 0.8999999999999999
		}]
	},
	"930301": {
		"nik": "930301",
		"nama": "FAISHAL AZKA JELLYANTO",
		"band": "V",
		"posisi": "OFF 2 PROCESS ORCHETRATION SYSTEM ADM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9552238805970149,
			"nilai_komparatif_unit": 0.9558041699033061,
			"summary_team": 0.8533333333333333
		}]
	},
	"930302": {
		"nik": "930302",
		"nama": "DIMAS PRASSTYO",
		"band": "V",
		"posisi": "OFF 2 ENTERPRISE & SUBSIDIARY RISK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8259259259259255,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0540754418359277,
			"nilai_komparatif_unit": 1.054715782513486,
			"summary_team": 0.8705882352941177
		}]
	},
	"930304": {
		"nik": "930304",
		"nama": "JOLANG JATI PAMUNGKAS",
		"band": "V",
		"posisi": "OFF 2 CX ENTERPRISE DIGITIZATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8176100628930801,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0072398190045269,
			"nilai_komparatif_unit": 1.0078517074923583,
			"summary_team": 0.8235294117647058
		}]
	},
	"930305": {
		"nik": "930305",
		"nama": "MUHAMMAD ARIYA",
		"band": "V",
		"posisi": "OFF 2 STRATEGIC INVEST EXEC ICT & SERV 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9161290322580641,
			"nilai_komparatif_unit": 0.9166855718205695,
			"summary_team": 0.7888888888888888
		}]
	},
	"930306": {
		"nik": "930306",
		"nama": "BESTAMA ABHI PRIAMBADA",
		"band": "V",
		"posisi": "OFF 2 FIN APP PLATFORM DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8740740740740743,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0958965209634248,
			"nilai_komparatif_unit": 1.0965622675437126,
			"summary_team": 0.957894736842105
		}]
	},
	"930309": {
		"nik": "930309",
		"nama": "GALIH PUTRA KURNIAWAN",
		"band": "V",
		"posisi": "OFF 2 AUTHORITY MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8874999999999998,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0207125103562555,
			"nilai_komparatif_unit": 1.0213325833743077,
			"summary_team": 0.9058823529411766
		}]
	},
	"930314": {
		"nik": "930314",
		"nama": "SRI YULIYANTI",
		"band": "V",
		"posisi": "OFF 2 COMM & CONVERGENCE PRODUCT MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0151515151515147,
			"nilai_komparatif_unit": 1.015768209918546,
			"summary_team": 0.8933333333333332
		}]
	},
	"930315": {
		"nik": "930315",
		"nama": "OKKY PRATAMA PUTRA",
		"band": "V",
		"posisi": "OFF 2 LOGISTIC PLANNING & EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9177777777777774,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254949437402083,
			"nilai_komparatif_unit": 1.0261179220404746,
			"summary_team": 0.9411764705882353
		}]
	},
	"930328": {
		"nik": "930328",
		"nama": "GEDE CANDRAYANA GIRI",
		"band": "V",
		"posisi": "OFF 2 AMOEBA SYSTEM DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9801869293396148,
			"summary_team": 0.8
		}]
	},
	"930337": {
		"nik": "930337",
		"nama": "MUHAMMAD ARIEF MA'RUF NASUTION",
		"band": "V",
		"posisi": "OFF 2 PROJECT ADMIN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9037037037037039,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9295081967213112,
			"nilai_komparatif_unit": 0.9300728639973536,
			"summary_team": 0.84
		}]
	},
	"930340": {
		"nik": "930340",
		"nama": "VANIA RIZKY AMANDA",
		"band": "V",
		"posisi": "OFF 2 JOB PROFILE REVIEW & ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8377777777777772,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0391636760805125,
			"nilai_komparatif_unit": 1.0397949580040118,
			"summary_team": 0.8705882352941177
		}]
	},
	"930341": {
		"nik": "930341",
		"nama": "BAKTI SATRIA ADHITYATAMA",
		"band": "V",
		"posisi": "OFF 2 ASSET INSURANCE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9133333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0323253388946814,
			"nilai_komparatif_unit": 1.0329524665942103,
			"summary_team": 0.9428571428571426
		}]
	},
	"930343": {
		"nik": "930343",
		"nama": "BRIAN AJRIANTO",
		"band": "V",
		"posisi": "OFF 2 ANALYSIS & REPORTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9075268817204291,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9333705045999452,
			"nilai_komparatif_unit": 0.9339375181908203,
			"summary_team": 0.8470588235294116
		}]
	},
	"930359": {
		"nik": "930359",
		"nama": "BRAMANTYO KRISDITO ADI",
		"band": "V",
		"posisi": "OFF 2 OPEX",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.046153846153846,
			"nilai_komparatif_unit": 1.046789374538332,
			"summary_team": 0.9066666666666666
		}]
	},
	"930362": {
		"nik": "930362",
		"nama": "RIFQI ABDUL AZIZ",
		"band": "V",
		"posisi": "OFF 2 DCN-SN OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0123456790123453,
			"nilai_komparatif_unit": 1.0129606692609494,
			"summary_team": 0.911111111111111
		}]
	},
	"930368": {
		"nik": "930368",
		"nama": "RIFQI LUTHFIL HADI",
		"band": "V",
		"posisi": "OFF 2 SUBMARINE PLANNING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999996,
			"nilai_komparatif_unit": 1.000607490367523,
			"summary_team": 0.8533333333333332
		}]
	},
	"930370": {
		"nik": "930370",
		"nama": "NAFISHA TENISYAFESTI",
		"band": "V",
		"posisi": "OFF 2 LEARNING EXPERIENCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0059583325620023,
			"summary_team": 0.8545454545454546
		}]
	},
	"930373": {
		"nik": "930373",
		"nama": "RINALDI WIRANEGARA",
		"band": "V",
		"posisi": "OFF 2 REGULATORY AFFAIR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9177777777777774,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9101267625694349,
			"nilai_komparatif_unit": 0.9106796558109213,
			"summary_team": 0.8352941176470587
		}]
	},
	"930374": {
		"nik": "930374",
		"nama": "ACHMAD ZULFIKAR",
		"band": "V",
		"posisi": "AUDITOR PRATAMA II",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.096447135431535,
			"nilai_komparatif_unit": 1.0971132165048085,
			"summary_team": 0.9571428571428569
		}]
	},
	"930375": {
		"nik": "930375",
		"nama": "WIDA ZAHRA NOVALINA",
		"band": "V",
		"posisi": "OFF 2 REPORTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8790123456790117,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0238764044943827,
			"nilai_komparatif_unit": 1.0244983995476475,
			"summary_team": 0.8999999999999998
		}]
	},
	"930380": {
		"nik": "930380",
		"nama": "DEBY HELMA PUTRA HASYIM",
		"band": "V",
		"posisi": "OFF 2 PERSONAL ASSISTANT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9957983193277313,
			"nilai_komparatif_unit": 0.996403257214719,
			"summary_team": 0.9294117647058824
		}]
	},
	"930382": {
		"nik": "930382",
		"nama": "IKHWAN REZA",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0635016893355225,
			"nilai_komparatif_unit": 1.0641477563676387,
			"summary_team": 0.8857142857142857
		}]
	},
	"930394": {
		"nik": "930394",
		"nama": "FAJAR ADITYA IKHSAN",
		"band": "V",
		"posisi": "OFF 2 DIGITAL TELCO REGULATION ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8622222222222212,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9278350515463927,
			"nilai_komparatif_unit": 0.9283987024028578,
			"summary_team": 0.7999999999999998
		}]
	},
	"930396": {
		"nik": "930396",
		"nama": "DINAH KAMILAH ULFA",
		"band": "V",
		"posisi": "OFF 2 MAINTENANCE ADM & PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.882962962962962,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9941275167785245,
			"nilai_komparatif_unit": 0.9947314396690575,
			"summary_team": 0.8777777777777777
		}]
	},
	"930399": {
		"nik": "930399",
		"nama": "DZIKRI AULIA MUHAMMAD",
		"band": "V",
		"posisi": "OFF 2 MANAGED SERVICE CATEGORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0076047455449186,
			"summary_team": 0.8727272727272727
		}]
	},
	"930401": {
		"nik": "930401",
		"nama": "SOPHIA KHANA",
		"band": "V",
		"posisi": "OFF 2 QUALITY & GOVERNANCE SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.83030303030303,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0151199165797706,
			"nilai_komparatif_unit": 1.0157365921509742,
			"summary_team": 0.8428571428571426
		}]
	},
	"940004": {
		"nik": "940004",
		"nama": "KANIA PUTRI ISLAMIATI",
		"band": "V",
		"posisi": "OFF 2 INTERNAL MEDIA & DOCUMENTATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8977777777777771,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9304018637157839,
			"nilai_komparatif_unit": 0.9309670738859173,
			"summary_team": 0.8352941176470587
		}]
	},
	"940009": {
		"nik": "940009",
		"nama": "HANIF FAIDZ RAHADIAN",
		"band": "V",
		"posisi": "OFF 2 SYSTEM COMPLIANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0191176470588235,
			"nilai_komparatif_unit": 1.0197367512127848,
			"summary_team": 0.9058823529411766
		}]
	},
	"940013": {
		"nik": "940013",
		"nama": "DINARYATI AMINDA",
		"band": "V",
		"posisi": "OFF 2 DIGITAL SYNERGY MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649358,
			"nilai_komparatif_unit": 0.9356329780059968,
			"summary_team": 0.7999999999999998
		}]
	},
	"940014": {
		"nik": "940014",
		"nama": "YAFSHIL NAUFAL",
		"band": "V",
		"posisi": "OFF 2 DELIVERY REPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0045248868778276,
			"nilai_komparatif_unit": 1.0051351260705434,
			"summary_team": 0.8705882352941174
		}]
	},
	"940037": {
		"nik": "940037",
		"nama": "WULAN NURHIDAYAH",
		"band": "V",
		"posisi": "OFF 2 TRIBE PLANNING & PERFORMANCE I",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0326797385620914,
			"nilai_komparatif_unit": 1.0333070815560046,
			"summary_team": 0.9294117647058824
		}]
	},
	"940039": {
		"nik": "940039",
		"nama": "HIMAWAN ADI PRAKOSA",
		"band": "V",
		"posisi": "JUNIOR DATA SCIENTIST",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8155555555555545,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0386279852540483,
			"nilai_komparatif_unit": 1.0392589417505305,
			"summary_team": 0.8470588235294116
		}]
	},
	"940040": {
		"nik": "940040",
		"nama": "BRIGITA DIANZA ALVYNTIA",
		"band": "V",
		"posisi": "OFF 2 API MGT OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999995,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0849673202614383,
			"nilai_komparatif_unit": 1.0856264274575749,
			"summary_team": 0.922222222222222
		}]
	},
	"940041": {
		"nik": "940041",
		"nama": "ANGGITA RAHMA PUSPITA",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP ACCOUNT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.027361701339917,
			"summary_team": 0.8727272727272727
		}]
	},
	"940042": {
		"nik": "940042",
		"nama": "RIZTAMA PRAWITA",
		"band": "V",
		"posisi": "OFF 2 COMMUNICATION & HELP DESK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200687,
			"nilai_komparatif_unit": 1.0503804187048447,
			"summary_team": 0.9272727272727272
		}]
	},
	"940043": {
		"nik": "940043",
		"nama": "DEVITHA PERMATASARI",
		"band": "V",
		"posisi": "OFF 2 PROCUREMENT POLICY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8807017543859643,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.054354012521344,
			"nilai_komparatif_unit": 1.0549945224279105,
			"summary_team": 0.9285714285714287
		}]
	},
	"940060": {
		"nik": "940060",
		"nama": "NADHIRA DWI PUTRI",
		"band": "V",
		"posisi": "OFF 2 OUTSOURCE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8407407407407402,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0075149002332218,
			"nilai_komparatif_unit": 1.00812695583025,
			"summary_team": 0.8470588235294119
		}]
	},
	"940064": {
		"nik": "940064",
		"nama": "MUHAMAD ZAKI MUBAROK RAHMAN",
		"band": "V",
		"posisi": "OFF 2 TRANSPORT SKSO & RMJ 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8681481481481468,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9726962457337898,
			"nilai_komparatif_unit": 0.9732871493335994,
			"summary_team": 0.8444444444444443
		}]
	},
	"940070": {
		"nik": "940070",
		"nama": "KEVIN GERAN SIAHAAN",
		"band": "V",
		"posisi": "OFF 2 TRANSPORT ISP PLANNING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9846153846153842,
			"nilai_komparatif_unit": 0.9852135289772536,
			"summary_team": 0.8533333333333332
		}]
	},
	"940085": {
		"nik": "940085",
		"nama": "MUHAMMAD IQBAL",
		"band": "V",
		"posisi": "OFF 2 DIGITIZATION ALIGNMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9801869293396148,
			"summary_team": 0.8
		}]
	},
	"940088": {
		"nik": "940088",
		"nama": "IQBAL HANIF",
		"band": "V",
		"posisi": "OFF 2 BIG DATA ANALYTCS INTRNL USE CASES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9806295399515736,
			"nilai_komparatif_unit": 0.9812252629512033,
			"summary_team": 0.7714285714285714
		}]
	},
	"940098": {
		"nik": "940098",
		"nama": "NGAKAN GDE BAYU ADITYA",
		"band": "V",
		"posisi": "OFF 2 STRATEGIC INVEST EXEC DIGI TELCO 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8600000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9468438538205974,
			"nilai_komparatif_unit": 0.9474190523413423,
			"summary_team": 0.8142857142857142
		}]
	},
	"940104": {
		"nik": "940104",
		"nama": "UBAY MUHAMMAD NOOR",
		"band": "V",
		"posisi": "OFF 2 SUBMARINE DEPLOYMENT AREA-2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.969230769230769,
			"nilai_komparatif_unit": 0.9698195675869842,
			"summary_team": 0.84
		}]
	},
	"940115": {
		"nik": "940115",
		"nama": "IWAN VITRYAWAN",
		"band": "V",
		"posisi": "OFF 2 STRATEGIC INVEST EXEC ICT & SERV 3",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8629629629629626,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0088361524867462,
			"nilai_komparatif_unit": 1.0094490107317915,
			"summary_team": 0.8705882352941177
		}]
	},
	"940116": {
		"nik": "940116",
		"nama": "CORNELIA HEMA RETNANDA",
		"band": "V",
		"posisi": "JUNIOR PRODUCT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.829743589743589,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96415327564895,
			"nilai_komparatif_unit": 0.964738989476723,
			"summary_team": 0.7999999999999998
		}]
	},
	"940120": {
		"nik": "940120",
		"nama": "BASKARA WIDHI JAYANTA",
		"band": "V",
		"posisi": "JUNIOR PRODUCT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8724637681159412,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9304279851475483,
			"nilai_komparatif_unit": 0.9309932111861998,
			"summary_team": 0.8117647058823529
		}]
	},
	"940123": {
		"nik": "940123",
		"nama": "MEGA AULIA SILVIADARA",
		"band": "V",
		"posisi": "OFF 2 LEARNING MATERIAL ADM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9577922077922074,
			"nilai_komparatif_unit": 0.9583740573325303,
			"summary_team": 0.8428571428571427
		}]
	},
	"940124": {
		"nik": "940124",
		"nama": "REZA ADITYA PRATAMA",
		"band": "V",
		"posisi": "OFF 2 DIGITAL TALENT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8388888888888892,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0518114530580438,
			"nilai_komparatif_unit": 1.0524504183842276,
			"summary_team": 0.8823529411764705
		}]
	},
	"940128": {
		"nik": "940128",
		"nama": "NAUFAL ARIF KURNIAWAN",
		"band": "V",
		"posisi": "OFF 2 ENT & OLO PRODUCT DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9200000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0289855072463765,
			"nilai_komparatif_unit": 1.02961060603035,
			"summary_team": 0.9466666666666665
		}]
	},
	"940136": {
		"nik": "940136",
		"nama": "NUR MEI NUGRAHENI",
		"band": "V",
		"posisi": "OFF 2 ASSURANCE DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.0006074903675233,
			"summary_team": 0.8999999999999999
		}]
	},
	"940138": {
		"nik": "940138",
		"nama": "SABRINA ANDIYANI",
		"band": "V",
		"posisi": "OFF 2 DATA SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8070175438596496,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267080745341608,
			"nilai_komparatif_unit": 1.027331789799699,
			"summary_team": 0.8285714285714285
		}]
	},
	"940148": {
		"nik": "940148",
		"nama": "SENJA RIZKIYAH PUTRI",
		"band": "V",
		"posisi": "OFF 2 PENGELOLAAN SISTEM INFORMASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9801869293396148,
			"summary_team": 0.8
		}]
	},
	"940153": {
		"nik": "940153",
		"nama": "SHIFA MANARULIESYA ANGGRIANE",
		"band": "V",
		"posisi": "OFF 2 CFU SUPPORT IT SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0192307692307694,
			"nilai_komparatif_unit": 1.0198499421053606,
			"summary_team": 0.8833333333333333
		}]
	},
	"940156": {
		"nik": "940156",
		"nama": "DEISNA RAHMANINGTYAS",
		"band": "V",
		"posisi": "OFF 2 OLO CORE NETWORK FAULT HANDLING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.92156862745098,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9957446808510644,
			"nilai_komparatif_unit": 0.9963495861531942,
			"summary_team": 0.9176470588235295
		}]
	},
	"940161": {
		"nik": "940161",
		"nama": "TANTRI MAWARSIH",
		"band": "V",
		"posisi": "OFF 2 CONTRACT DRAFTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333337,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.908571428571428,
			"nilai_komparatif_unit": 0.9091233769624922,
			"summary_team": 0.757142857142857
		}]
	},
	"940174": {
		"nik": "940174",
		"nama": "AMELIA SHAFFIRA ARIFIN",
		"band": "V",
		"posisi": "JUNIOR INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8691358024691347,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0190746753246764,
			"nilai_komparatif_unit": 1.0196937533737234,
			"summary_team": 0.8857142857142856
		}]
	},
	"940175": {
		"nik": "940175",
		"nama": "AMIRSYAH RAYHAN MUBARAK",
		"band": "V",
		"posisi": "OFF 2 CORE ROUTER OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333327,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9663608562691137,
			"nilai_komparatif_unit": 0.966947911180849,
			"summary_team": 0.8777777777777777
		}]
	},
	"940182": {
		"nik": "940182",
		"nama": "LAKSMANA EKA SURYA",
		"band": "V",
		"posisi": "OFF 2 NFV NW & SECURITY DATABASE ADM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0285714285714285,
			"nilai_komparatif_unit": 1.0291962758065956,
			"summary_team": 0.96
		}]
	},
	"940191": {
		"nik": "940191",
		"nama": "LUTHFAN AUFAR AKBAR",
		"band": "V",
		"posisi": "OFF 2 TRANSPORT BACKBONE & RMJ 4",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9740259740259735,
			"nilai_komparatif_unit": 0.9746176854229119,
			"summary_team": 0.8333333333333331
		}]
	},
	"940199": {
		"nik": "940199",
		"nama": "ERSYA TAUFIQ HIDAYATULLAH",
		"band": "V",
		"posisi": "OFF 2 SOLUTION IPTV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.044025157232704,
			"nilai_komparatif_unit": 1.0446593924591752,
			"summary_team": 0.922222222222222
		}]
	},
	"940200": {
		"nik": "940200",
		"nama": "ALIFIYAH PRATIWI P. WEDDA",
		"band": "V",
		"posisi": "OFF 2 VPN ROUTER OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333327,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0152905198776765,
			"nilai_komparatif_unit": 1.0159072990887401,
			"summary_team": 0.9222222222222222
		}]
	},
	"940203": {
		"nik": "940203",
		"nama": "FARHANA IRMADELA ERDIA",
		"band": "V",
		"posisi": "OFF 2 QUALITY SYSTEM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8175438596491225,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0217116889674327,
			"nilai_komparatif_unit": 1.0223323689768666,
			"summary_team": 0.8352941176470587
		}]
	},
	"940207": {
		"nik": "940207",
		"nama": "INDRI ASTARI ARIEF",
		"band": "V",
		"posisi": "JUNIOR DESIGNER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545454,
			"nilai_komparatif_unit": 1.0260774992132422,
			"summary_team": 0.8545454545454546
		}]
	},
	"940225": {
		"nik": "940225",
		"nama": "ADRIAN FIRMANSYAH TAUFIK",
		"band": "V",
		"posisi": "OFF 2 ASSURANCE ACCESS SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8722222222222225,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9936305732484072,
			"nilai_komparatif_unit": 0.9942341942505325,
			"summary_team": 0.8666666666666665
		}]
	},
	"940238": {
		"nik": "940238",
		"nama": "DEYA NILAN AMYHORSEA",
		"band": "V",
		"posisi": "OFF 2 LOG PLAN,SYSTEM & PELIMPAHAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8277777777777782,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0201342281879189,
			"nilai_komparatif_unit": 1.0207539499051241,
			"summary_team": 0.8444444444444444
		}]
	},
	"940241": {
		"nik": "940241",
		"nama": "TALITHA FAUZIA RAHMA",
		"band": "V",
		"posisi": "OFF 2 WS&MOBILE BIZ PORT DATA ADM & SUP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.962566844919786,
			"nilai_komparatif_unit": 0.9631515950061722,
			"summary_team": 0.8181818181818181
		}]
	},
	"940246": {
		"nik": "940246",
		"nama": "TANIA PUTRI PERMATASARI",
		"band": "V",
		"posisi": "OFF 2 FINANCE SYSTEM OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0703259005145795,
			"nilai_komparatif_unit": 1.070976113189253,
			"summary_team": 0.9454545454545454
		}]
	},
	"940250": {
		"nik": "940250",
		"nama": "DWISYAH NABILA ANGGIA SILITONGA",
		"band": "V",
		"posisi": "JUNIOR DESIGNER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9841269841269839,
			"nilai_komparatif_unit": 0.9847248317902609,
			"summary_team": 0.8857142857142857
		}]
	},
	"940253": {
		"nik": "940253",
		"nama": "Eko Dwi Muttaqin",
		"band": "V",
		"posisi": "OFF 2 COMM& CONVERGENCE PROD DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9600000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0416666666666663,
			"nilai_komparatif_unit": 1.0422994691328367,
			"summary_team": 0.9999999999999999
		}]
	},
	"940257": {
		"nik": "940257",
		"nama": "KEVIN SINAGA",
		"band": "V",
		"posisi": "OFF 2 DATACOMM PROD INFRA INTEGRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0779220779220773,
			"nilai_komparatif_unit": 1.0785769052013559,
			"summary_team": 0.922222222222222
		}]
	},
	"940269": {
		"nik": "940269",
		"nama": "ANGGA KURNIA PUTRA",
		"band": "V",
		"posisi": "OFF 2 BILLING & REVENUE MGT SYSTEM DEVT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8571428571428564,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9222222222222229,
			"nilai_komparatif_unit": 0.9227824633389391,
			"summary_team": 0.7904761904761904
		}]
	},
	"940279": {
		"nik": "940279",
		"nama": "MUHAMMAD GALANG PERSADA",
		"band": "V",
		"posisi": "OFF 2 SOA PLATFORM SYSTEM ADM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0151515151515147,
			"nilai_komparatif_unit": 1.015768209918546,
			"summary_team": 0.8933333333333332
		}]
	},
	"940280": {
		"nik": "940280",
		"nama": "ISKANDAR SUHAIMI",
		"band": "V",
		"posisi": "JUNIOR PRODUCT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666665,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.909502262443439,
			"nilai_komparatif_unit": 0.9100547763071143,
			"summary_team": 0.7882352941176469
		}]
	},
	"940290": {
		"nik": "940290",
		"nama": "IRHAM RENALDI HERDIAN",
		"band": "V",
		"posisi": "OFF 2 PROCUREMENT 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8277777777777782,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9664429530201337,
			"nilai_komparatif_unit": 0.9670300578048544,
			"summary_team": 0.7999999999999999
		}]
	},
	"940298": {
		"nik": "940298",
		"nama": "RAHARDI GITA NAUTIKA",
		"band": "V",
		"posisi": "OFF 2 ACCOUNTING OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8980392156862742,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0742358078602625,
			"nilai_komparatif_unit": 1.0748883957659865,
			"summary_team": 0.9647058823529413
		}]
	},
	"940307": {
		"nik": "940307",
		"nama": "MASANGGA FERBIYANA MUKTI",
		"band": "V",
		"posisi": "OFF 2 DATA SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999989,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9965397923875446,
			"nilai_komparatif_unit": 0.997145180712274,
			"summary_team": 0.8470588235294119
		}]
	},
	"940314": {
		"nik": "940314",
		"nama": "ABDUL HARIS ASRI",
		"band": "V",
		"posisi": "OFF 2 INTERNAL STAKEHOLDER RELATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8212121212121206,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0959409594095948,
			"nilai_komparatif_unit": 1.0966067329858107,
			"summary_team": 0.8999999999999999
		}]
	},
	"940325": {
		"nik": "940325",
		"nama": "IMAM BHASKARA",
		"band": "V",
		"posisi": "OFF 2 CAREER & TALENT DEVELOPMENT",
		"category": [{
			"code": "OC-1-01",
			"kriteria": 0.8166666666666669,
			"kriteria_bp": 1.0005572897665742,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0686456400742113,
			"nilai_komparatif_unit": 1.0680504265013369,
			"summary_team": 0.8727272727272727
		}]
	},
	"940330": {
		"nik": "940330",
		"nama": "ARDHIAN BAYU FIRDAUZ",
		"band": "V",
		"posisi": "OFF 2 TRANSPORT OSP DEPLOYMENT AREA-1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8600000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9922480620155032,
			"nilai_komparatif_unit": 0.9928508431553715,
			"summary_team": 0.8533333333333332
		}]
	},
	"940342": {
		"nik": "940342",
		"nama": "MUHAMAD ALFAT FAUZIE",
		"band": "V",
		"posisi": "OFF 2 LOGISTIC LEGAL & COMPLIANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.92,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0289855072463767,
			"nilai_komparatif_unit": 1.0296106060303503,
			"summary_team": 0.9466666666666665
		}]
	},
	"940357": {
		"nik": "940357",
		"nama": "GHALI ADYO PUTRA",
		"band": "V",
		"posisi": "OFF 2 IT  PLANNING MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090908,
			"nilai_komparatif_unit": 1.091571807673662,
			"summary_team": 0.9272727272727272
		}]
	},
	"940359": {
		"nik": "940359",
		"nama": "MUHAMMAD AAFIUDDIN",
		"band": "V",
		"posisi": "OFF 2  INTERNET ROUTER OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333327,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9418960244648324,
			"nilai_komparatif_unit": 0.9424682172269035,
			"summary_team": 0.8555555555555555
		}]
	},
	"940370": {
		"nik": "940370",
		"nama": "ANITA DWINATA LUBIS",
		"band": "V",
		"posisi": "OFF 2 ASSESSMENT ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.980392156862745,
			"nilai_komparatif_unit": 0.9809877356544346,
			"summary_team": 0.8823529411764706
		}]
	},
	"950005": {
		"nik": "950005",
		"nama": "ELLYANA SANTI",
		"band": "V",
		"posisi": "OFF 2 REGULATORY COMPLIANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999994,
			"nilai_komparatif_unit": 1.000607490367523,
			"summary_team": 0.8444444444444443
		}]
	},
	"950014": {
		"nik": "950014",
		"nama": "NADIA SHAFIRA NOVARENA",
		"band": "V",
		"posisi": "OFF 2 SYNERGY PERFORMANCE & REPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.88695652173913,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1141868512110733,
			"nilai_komparatif_unit": 1.1148637089908053,
			"summary_team": 0.9882352941176471
		}]
	},
	"950026": {
		"nik": "950026",
		"nama": "MAUDI PRAMEDIA PUTRI",
		"band": "V",
		"posisi": "OFF 2 BIG DATA&AI SERVICE CREATN&INTGRTN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777779,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9650037230081905,
			"nilai_komparatif_unit": 0.9655899534745424,
			"summary_team": 0.8470588235294118
		}]
	},
	"950040": {
		"nik": "950040",
		"nama": "HANIF SUDIRA",
		"band": "V",
		"posisi": "OFF 2 CA & COHERENT IT ENABLEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9852941176470587,
			"nilai_komparatif_unit": 0.9858926743327069,
			"summary_team": 0.8933333333333332
		}]
	},
	"950044": {
		"nik": "950044",
		"nama": "FRISKI GATRA PAMUNGKAS",
		"band": "V",
		"posisi": "JUNIOR DEVELOPER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9801869293396148,
			"summary_team": 0.8
		}]
	},
	"950047": {
		"nik": "950047",
		"nama": "SYAMSUL HIDAYAT",
		"band": "V",
		"posisi": "OFF 2 OLO & GLOBAL DATACOMM PROD DEVELOP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9075268817204296,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0818612666953904,
			"nilai_komparatif_unit": 1.0825184869939046,
			"summary_team": 0.9818181818181818
		}]
	},
	"950050": {
		"nik": "950050",
		"nama": "GALIH PANGESTI",
		"band": "V",
		"posisi": "JUNIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8527777777777763,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9932937344318851,
			"nilai_komparatif_unit": 0.9938971508076739,
			"summary_team": 0.8470588235294116
		}]
	},
	"950070": {
		"nik": "950070",
		"nama": "ANINDITA HAPSARI",
		"band": "V",
		"posisi": "OFF 2 EXPOSURE & API FACTORY MGT DESIGN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999997,
			"nilai_komparatif_unit": 1.0006074903675233,
			"summary_team": 0.9066666666666665
		}]
	},
	"950074": {
		"nik": "950074",
		"nama": "IKA MONICA",
		"band": "V",
		"posisi": "OFF 2 IT PLATFORM REQ. ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9573283858998143,
			"nilai_komparatif_unit": 0.9579099536728053,
			"summary_team": 0.7818181818181817
		}]
	},
	"950108": {
		"nik": "950108",
		"nama": "BIMA BRAMANTIYA PUTRA",
		"band": "V",
		"posisi": "OFF 2 IH3P INTEGRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8791666666666659,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9984202211690371,
			"nilai_komparatif_unit": 0.999026751836138,
			"summary_team": 0.8777777777777777
		}]
	},
	"950119": {
		"nik": "950119",
		"nama": "TESAR AKRAM PRATAMA",
		"band": "V",
		"posisi": "OFF 2 ORDER MANAGEMENT ADM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0149253731343282,
			"nilai_komparatif_unit": 1.0155419305222626,
			"summary_team": 0.9066666666666665
		}]
	},
	"950137": {
		"nik": "950137",
		"nama": "LARAS ADHIANTI",
		"band": "V",
		"posisi": "OFF 2 CORE OPERATION MAINTENANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8944444444444447,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.993788819875776,
			"nilai_komparatif_unit": 0.9943925370112031,
			"summary_team": 0.8888888888888888
		}]
	},
	"950141": {
		"nik": "950141",
		"nama": "CHITRA UTAMI PUTRI",
		"band": "V",
		"posisi": "OFF 2 CA & COHERENT IT OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9600000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.0006074903675233,
			"summary_team": 0.96
		}]
	},
	"950147": {
		"nik": "950147",
		"nama": "ALDRIE VINANDITYO",
		"band": "V",
		"posisi": "OFF 2 DATA WAREHOUSE DESIGN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8740740740740743,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9152542372881356,
			"nilai_komparatif_unit": 0.9158102454211232,
			"summary_team": 0.8000000000000003
		}]
	},
	"950154": {
		"nik": "950154",
		"nama": "MUHAMMAD BAYHAQI IRWANSYAH",
		"band": "V",
		"posisi": "JUNIOR PRODUCT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8724637681159412,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0113347664647263,
			"nilai_komparatif_unit": 1.0119491425936953,
			"summary_team": 0.8823529411764706
		}]
	},
	"950176": {
		"nik": "950176",
		"nama": "DYLAN MAHESA ANGGASTA",
		"band": "V",
		"posisi": "OFF 2 NETWORK SURVEILLANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8242424242424243,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.037990196078431,
			"nilai_komparatif_unit": 1.0386207651241326,
			"summary_team": 0.8555555555555554
		}]
	},
	"950182": {
		"nik": "950182",
		"nama": "YUNITA CAPRIATI",
		"band": "V",
		"posisi": "OFF 2 TECHNICAL PERFORMANCE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.849382716049382,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9300872093023265,
			"nilai_komparatif_unit": 0.9306522283229345,
			"summary_team": 0.79
		}]
	},
	"950205": {
		"nik": "950205",
		"nama": "GILANG FATHIN CEGA",
		"band": "V",
		"posisi": "OFF 2 SUBMARINE DEPLOYMENT AREA-1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9344262295081968,
			"nilai_komparatif_unit": 0.9349938844417843,
			"summary_team": 0.76
		}]
	},
	"950207": {
		"nik": "950207",
		"nama": "IMAM SYAFI'I",
		"band": "V",
		"posisi": "OFF 2 BROADBAND NODE PLANNING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0294117647058822,
			"nilai_komparatif_unit": 1.0300371224371565,
			"summary_team": 0.9333333333333332
		}]
	},
	"950235": {
		"nik": "950235",
		"nama": "MOCH IRIANDA",
		"band": "V",
		"posisi": "OFF 2 PROCUREMENT ADMINISTRATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8222222222222214,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0424710424710435,
			"nilai_komparatif_unit": 1.0431043335877668,
			"summary_team": 0.8571428571428571
		}]
	},
	"950267": {
		"nik": "950267",
		"nama": "FEBI ARY RAMADHAN",
		"band": "V",
		"posisi": "OFF 2 TELKOMGROUP & OLO PROJECT ADMIN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8600000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.03875968992248,
			"nilai_komparatif_unit": 1.0393907264282796,
			"summary_team": 0.8933333333333332
		}]
	},
	"950273": {
		"nik": "950273",
		"nama": "MIFTAKHUL AKHYAR",
		"band": "V",
		"posisi": "OFF 2 OLO CORE NETWORK READINESS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.855555555555556,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9480519480519476,
			"nilai_komparatif_unit": 0.948627880478301,
			"summary_team": 0.8111111111111111
		}]
	},
	"950298": {
		"nik": "950298",
		"nama": "PUTRI DIANATA GITA",
		"band": "V",
		"posisi": "OFF 2 IP-METRO DEPLOYMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.983606557377049,
			"nilai_komparatif_unit": 0.9842040888860886,
			"summary_team": 0.7999999999999999
		}]
	},
	"950303": {
		"nik": "950303",
		"nama": "NYIMAS NABILAH OKTAVIANI",
		"band": "V",
		"posisi": "JUNIOR PRODUCT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0588235294117658,
			"nilai_komparatif_unit": 1.0594667545067908,
			"summary_team": 0.9058823529411764
		}]
	},
	"950309": {
		"nik": "950309",
		"nama": "FRANSISKA DYAH AYU OKTAVIANI",
		"band": "V",
		"posisi": "OFF 2 DATA MANAGEMENT SUPPORTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9200000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0289855072463765,
			"nilai_komparatif_unit": 1.02961060603035,
			"summary_team": 0.9466666666666667
		}]
	},
	"950311": {
		"nik": "950311",
		"nama": "NAILUL HANA",
		"band": "V",
		"posisi": "OFF 2 TRANSPORT SYNERGY PLANNING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8745098039215675,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9605381165919294,
			"nilai_komparatif_unit": 0.9611216342453982,
			"summary_team": 0.84
		}]
	},
	"950313": {
		"nik": "950313",
		"nama": "ENRICO WIRATAMA PURWANTO",
		"band": "V",
		"posisi": "OFF 2 TRANSPORT SKKL 3",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1038961038961033,
			"nilai_komparatif_unit": 1.1045667101459669,
			"summary_team": 0.9444444444444443
		}]
	},
	"960006": {
		"nik": "960006",
		"nama": "MUHAMMAD IHSAN MUSTIKA",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0076047455449186,
			"summary_team": 0.8727272727272727
		}]
	},
	"960010": {
		"nik": "960010",
		"nama": "NADYA CHANDRA ROSIANTI",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER MGT SYSTEM DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969693,
			"nilai_komparatif_unit": 0.970286051265477,
			"summary_team": 0.8533333333333332
		}]
	},
	"960012": {
		"nik": "960012",
		"nama": "RACHMAT SIMBARA SAPUTRA",
		"band": "V",
		"posisi": "OFF 2 SERVICE OPERATION PROCUREMENT  -3",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8222222222222214,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.972972972972974,
			"nilai_komparatif_unit": 0.9735640446819158,
			"summary_team": 0.8
		}]
	},
	"960027": {
		"nik": "960027",
		"nama": "JWALITA GALUH GARINI",
		"band": "V",
		"posisi": "OFF 2 PLM OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9200000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0289855072463767,
			"nilai_komparatif_unit": 1.0296106060303503,
			"summary_team": 0.9466666666666667
		}]
	},
	"960029": {
		"nik": "960029",
		"nama": "MUTIARA KAFFA",
		"band": "V",
		"posisi": "OFF 2 PRODUCT PARTNERSHIP MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999996,
			"nilai_komparatif_unit": 1.000607490367523,
			"summary_team": 0.8533333333333332
		}]
	},
	"960034": {
		"nik": "960034",
		"nama": "RATNA AUDIA NILASARI",
		"band": "V",
		"posisi": "OFF 2 ONE NETWORK OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0329670329670326,
			"nilai_komparatif_unit": 1.0335945504895294,
			"summary_team": 0.8952380952380953
		}]
	},
	"960037": {
		"nik": "960037",
		"nama": "ILATIFAH NUR HIDAYAT",
		"band": "V",
		"posisi": "OFF 2 PARTNER & CHANNEL RELATION DEVT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9743589743589741,
			"nilai_komparatif_unit": 0.9749508880504073,
			"summary_team": 0.8444444444444444
		}]
	},
	"960070": {
		"nik": "960070",
		"nama": "CINDY ALICIA SAHARA",
		"band": "V",
		"posisi": "OFF 2 PERFORMANCE REVIEW",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0377358490566035,
			"nilai_komparatif_unit": 1.0383662635889392,
			"summary_team": 0.9166666666666665
		}]
	},
	"960072": {
		"nik": "960072",
		"nama": "ARSANA YUDISTIRA",
		"band": "V",
		"posisi": "OFF 2 DCN-SN PERFORMANCE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9753086419753083,
			"nilai_komparatif_unit": 0.9759011325806708,
			"summary_team": 0.8777777777777777
		}]
	},
	"960078": {
		"nik": "960078",
		"nama": "CAHYA BUDI MUHAMMAD",
		"band": "V",
		"posisi": "OFF 2 SATELLITE SPECTRUM MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.0006074903675233,
			"summary_team": 0.9066666666666665
		}]
	},
	"960108": {
		"nik": "960108",
		"nama": "WULAN DWI ANGGRAINI",
		"band": "V",
		"posisi": "OFF 2 OPERATION AREA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9074074074074071,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.028571428571429,
			"nilai_komparatif_unit": 1.029196275806596,
			"summary_team": 0.9333333333333332
		}]
	},
	"960134": {
		"nik": "960134",
		"nama": "DAMAR SATRIO YUDANTO",
		"band": "V",
		"posisi": "OFF 2 PDI PREPARATION ICT & SERV 3",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8629629629629626,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.107296137339056,
			"nilai_komparatif_unit": 1.1079688090764854,
			"summary_team": 0.9555555555555554
		}]
	},
	"960162": {
		"nik": "960162",
		"nama": "RIFQI JULI INDRAYANTO",
		"band": "V",
		"posisi": "OFF 2 IT INTEGRATION DESIGN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999999,
			"nilai_komparatif_unit": 1.0006074903675235,
			"summary_team": 0.9066666666666665
		}]
	},
	"980001": {
		"nik": "980001",
		"nama": "YULIUS ADYAN MANDATAPUTRA",
		"band": "V",
		"posisi": "OFF 2 DATACOMM PROD PORTFOLIO & QUAL MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9075268817204296,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.05467163168585,
			"nilai_komparatif_unit": 1.0553123345429996,
			"summary_team": 0.9571428571428569
		}]
	}
};