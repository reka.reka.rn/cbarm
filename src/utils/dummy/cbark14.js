export const cbark14 = {
	"650194": {
		"nik": "650194",
		"nama": "ANDREAS NOVARI PRAMBUDI",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197862,
			"nilai_komparatif_unit": 0.9693124104100126,
			"summary_team": 0.8181818181818182
		}]
	},
	"650369": {
		"nik": "650369",
		"nama": "NURMAWATI",
		"band": "IV",
		"posisi": "OFF 1 SECRETARY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8138888888888879,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9684802248544482,
			"nilai_komparatif_unit": 0.9752672306787442,
			"summary_team": 0.7882352941176471
		}]
	},
	"650633": {
		"nik": "650633",
		"nama": "DWI JOEDO PRAMONO",
		"band": "IV",
		"posisi": "OFF 1 REPAIR CENTER MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8277777777777782,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9664429530201338,
			"nilai_komparatif_unit": 0.9732156818613276,
			"summary_team": 0.8
		}]
	},
	"651478": {
		"nik": "651478",
		"nama": "SUJIMAN, S.E",
		"band": "IV",
		"posisi": "ASMAN OM TRANSPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.956043956043956,
			"nilai_komparatif_unit": 0.9627438098266479,
			"summary_team": 0.8285714285714287
		}]
	},
	"660021": {
		"nik": "660021",
		"nama": "ENDANG MURBANI",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666671,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9551020408163258,
			"nilai_komparatif_unit": 0.9617952937578524,
			"summary_team": 0.7799999999999998
		}]
	},
	"660034": {
		"nik": "660034",
		"nama": "YETTY SURYANI",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0208507089241032,
			"nilai_komparatif_unit": 1.0280047214990589,
			"summary_team": 0.9272727272727274
		}]
	},
	"660054": {
		"nik": "660054",
		"nama": "RETNO HAPSARI",
		"band": "IV",
		"posisi": "ASMAN PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9466666666666664,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0563380281690142,
			"nilai_komparatif_unit": 1.0637407320814105,
			"summary_team": 0.9999999999999999
		}]
	},
	"660091": {
		"nik": "660091",
		"nama": "SULISTIYONO",
		"band": "IV",
		"posisi": "ASMAN DATA CENTER & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9940828402366858,
			"nilai_komparatif_unit": 1.0010492664510497,
			"summary_team": 0.8615384615384614
		}]
	},
	"660098": {
		"nik": "660098",
		"nama": "HERU SUJATMIKO",
		"band": "IV",
		"posisi": "ASMAN DEBT MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9876543209876544,
			"nilai_komparatif_unit": 0.9945756968267344,
			"summary_team": 0.8888888888888891
		}]
	},
	"660104": {
		"nik": "660104",
		"nama": "BAMBANG LUKITO AJI",
		"band": "IV",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666677,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769219,
			"nilai_komparatif_unit": 0.9295457474188313,
			"summary_team": 0.7999999999999999
		}]
	},
	"660142": {
		"nik": "660142",
		"nama": "CIPTADI WIDODO",
		"band": "IV",
		"posisi": "ASMAN HR DEVELOPMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8380952380952383,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0196280991735536,
			"nilai_komparatif_unit": 1.0267735438301513,
			"summary_team": 0.8545454545454547
		}]
	},
	"660218": {
		"nik": "660218",
		"nama": "B.JAKA SUPRIYANTO",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1,
			"nilai_komparatif_unit": 1.0070078930370685,
			"summary_team": 0.8
		}]
	},
	"660274": {
		"nik": "660274",
		"nama": "RATNA LELIANA",
		"band": "IV",
		"posisi": "OFF 1 EMPLOYEE & INDUSTRIAL RELATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8407407407407405,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9655351127235038,
			"nilai_komparatif_unit": 0.972301479517004,
			"summary_team": 0.8117647058823529
		}]
	},
	"660446": {
		"nik": "660446",
		"nama": "SUGIYANTO",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9876923076923073,
			"nilai_komparatif_unit": 0.9946139497381504,
			"summary_team": 0.823076923076923
		}]
	},
	"660447": {
		"nik": "660447",
		"nama": "KASIBAN, S.E. M.M.",
		"band": "IV",
		"posisi": "ASMAN WIFI FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857142,
			"nilai_komparatif_unit": 0.9710433254286017,
			"summary_team": 0.7714285714285714
		}]
	},
	"660452": {
		"nik": "660452",
		"nama": "MOKHAMMAD SOLEHUDDIN,IR",
		"band": "IV",
		"posisi": "OFF 1 PARTNERSHIP MARKETING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0686456400742113,
			"nilai_komparatif_unit": 1.076134594414381,
			"summary_team": 0.8727272727272727
		}]
	},
	"660459": {
		"nik": "660459",
		"nama": "ZULKIFLI",
		"band": "IV",
		"posisi": "ASMAN CCAN FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8000000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9821428571428568,
			"nilai_komparatif_unit": 0.9890256092328348,
			"summary_team": 0.7857142857142856
		}]
	},
	"660460": {
		"nik": "660460",
		"nama": "GUSHARYUNIS",
		"band": "IV",
		"posisi": "ASMAN SAS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9674099485420238,
			"nilai_komparatif_unit": 0.9741894539844023,
			"summary_team": 0.8545454545454546
		}]
	},
	"660474": {
		"nik": "660474",
		"nama": "RUDY TRISNA SUSANTO, S.Kom",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.060884070058382,
			"nilai_komparatif_unit": 1.068318632146081,
			"summary_team": 0.9636363636363637
		}]
	},
	"660501": {
		"nik": "660501",
		"nama": "WIZIA",
		"band": "IV",
		"posisi": "ASMAN GES SALES ENGINEER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.875362318840579,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0385310054184236,
			"nilai_komparatif_unit": 1.045808919620075,
			"summary_team": 0.9090909090909092
		}]
	},
	"660502": {
		"nik": "660502",
		"nama": "EDI ZULFAN",
		"band": "IV",
		"posisi": "ASMAN CDC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181817,
			"nilai_komparatif_unit": 0.9886986586182126,
			"summary_team": 0.8727272727272728
		}]
	},
	"660504": {
		"nik": "660504",
		"nama": "KURHANA",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8577777777777779,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9909326424870465,
			"nilai_komparatif_unit": 0.9978769924525354,
			"summary_team": 0.85
		}]
	},
	"660506": {
		"nik": "660506",
		"nama": "YUSNAN",
		"band": "IV",
		"posisi": "ASMAN BUSINESS OBL & DELIVERY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8476190476190467,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9101123595505626,
			"nilai_komparatif_unit": 0.9164903296180069,
			"summary_team": 0.7714285714285712
		}]
	},
	"660523": {
		"nik": "660523",
		"nama": "ANDI SISWANTO",
		"band": "IV",
		"posisi": "OFF 1 PKBL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0394456289978675,
			"nilai_komparatif_unit": 1.046729952783733,
			"summary_team": 0.9285714285714284
		}]
	},
	"660533": {
		"nik": "660533",
		"nama": "SUHERMAN",
		"band": "IV",
		"posisi": "ASMAN DATA CENTER & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8419047619047649,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9718222953517037,
			"nilai_komparatif_unit": 0.9786327220485669,
			"summary_team": 0.8181818181818182
		}]
	},
	"660548": {
		"nik": "660548",
		"nama": "HARDI PRAYETNO",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8379084967320288,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.976457240107783,
			"nilai_komparatif_unit": 0.9833001480017295,
			"summary_team": 0.8181818181818182
		}]
	},
	"660551": {
		"nik": "660551",
		"nama": "AFRIZAL",
		"band": "IV",
		"posisi": "ASMAN FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8419047619047649,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.950226244343888,
			"nilai_komparatif_unit": 0.9568853282252653,
			"summary_team": 0.8
		}]
	},
	"660554": {
		"nik": "660554",
		"nama": "ARUMNINGSIH",
		"band": "IV",
		"posisi": "ASMAN DEBT MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0291595197255574,
			"nilai_komparatif_unit": 1.036371759557875,
			"summary_team": 0.9090909090909092
		}]
	},
	"660585": {
		"nik": "660585",
		"nama": "SOEJADI, S.E",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7777777777777777,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1571428571428575,
			"nilai_komparatif_unit": 1.1652519905143224,
			"summary_team": 0.9000000000000001
		}]
	},
	"660587": {
		"nik": "660587",
		"nama": "HERI ISWANTO",
		"band": "IV",
		"posisi": "OFF 1 CD ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.031404958677686,
			"nilai_komparatif_unit": 1.0386329343060012,
			"summary_team": 0.9454545454545454
		}]
	},
	"660588": {
		"nik": "660588",
		"nama": "DIDIT NURHARJANTO, SE",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96,
			"nilai_komparatif_unit": 0.9667275773155858,
			"summary_team": 0.8
		}]
	},
	"660589": {
		"nik": "660589",
		"nama": "MARSUDIYONO",
		"band": "IV",
		"posisi": "ASMAN CCAN FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666665,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0549450549450552,
			"nilai_komparatif_unit": 1.0623379970500946,
			"summary_team": 0.8615384615384616
		}]
	},
	"660607": {
		"nik": "660607",
		"nama": "KHAIRUDDIN",
		"band": "IV",
		"posisi": "ASMAN CDC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200687,
			"nilai_komparatif_unit": 1.0570991947490325,
			"summary_team": 0.9272727272727274
		}]
	},
	"660608": {
		"nik": "660608",
		"nama": "HARBI",
		"band": "IV",
		"posisi": "ASMAN DATA CENTER & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8633333333333328,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0108810108810116,
			"nilai_komparatif_unit": 1.0179651568784693,
			"summary_team": 0.8727272727272728
		}]
	},
	"660613": {
		"nik": "660613",
		"nama": "ARIANTO",
		"band": "IV",
		"posisi": "ASMAN DEPLOYMENT & CAPEX MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.842857142857143,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0138674884437595,
			"nilai_komparatif_unit": 1.0209725633565347,
			"summary_team": 0.8545454545454546
		}]
	},
	"660634": {
		"nik": "660634",
		"nama": "FERRY ARDIATOS",
		"band": "IV",
		"posisi": "OFF 1 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.5666666666666665,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9304812834224601,
			"nilai_komparatif_unit": 0.9370019967296789,
			"summary_team": 0.5272727272727272
		}]
	},
	"660646": {
		"nik": "660646",
		"nama": "MOCH SOLICHIN",
		"band": "IV",
		"posisi": "OFF 1 PUBLIC RELATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8138888888888879,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.982935153583619,
			"nilai_komparatif_unit": 0.9898234580023075,
			"summary_team": 0.8
		}]
	},
	"670012": {
		"nik": "670012",
		"nama": "MARJOHAN",
		"band": "IV",
		"posisi": "ASMAN DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8523809523809527,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0025393600812593,
			"nilai_komparatif_unit": 1.00956504868216,
			"summary_team": 0.8545454545454546
		}]
	},
	"670047": {
		"nik": "670047",
		"nama": "AMALIANUR",
		"band": "IV",
		"posisi": "ASMAN CDC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8589743589743583,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9313432835820903,
			"nilai_komparatif_unit": 0.9378700376942257,
			"summary_team": 0.7999999999999999
		}]
	},
	"670056": {
		"nik": "670056",
		"nama": "FITRIN NUGRAHENI",
		"band": "IV",
		"posisi": "OFF 1 BUDGET CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8933333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0234541577825156,
			"nilai_komparatif_unit": 1.0306264150485984,
			"summary_team": 0.9142857142857141
		}]
	},
	"670059": {
		"nik": "670059",
		"nama": "HERNI SULMITA",
		"band": "IV",
		"posisi": "ASMAN HR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9890109890109888,
			"nilai_komparatif_unit": 0.9959418722344632,
			"summary_team": 0.857142857142857
		}]
	},
	"670063": {
		"nik": "670063",
		"nama": "JOHAR WARDANI",
		"band": "IV",
		"posisi": "SENIOR AM GOVERNMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8320512820512806,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.072419106317413,
			"nilai_komparatif_unit": 1.079934504705394,
			"summary_team": 0.8923076923076922
		}]
	},
	"670118": {
		"nik": "670118",
		"nama": "BAMBANG ROEDIYANTO",
		"band": "IV",
		"posisi": "OFF 1 PUSH CHANNEL INTEGRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000004,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090904,
			"nilai_komparatif_unit": 1.098554065131347,
			"summary_team": 0.9818181818181818
		}]
	},
	"670174": {
		"nik": "670174",
		"nama": "SUMARYONO",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9882352941176469,
			"nilai_komparatif_unit": 0.9951607413542793,
			"summary_team": 0.8400000000000001
		}]
	},
	"670177": {
		"nik": "670177",
		"nama": "PUTRO RIYANTO, SE",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9843749999999997,
			"nilai_komparatif_unit": 0.991273394708364,
			"summary_team": 0.84
		}]
	},
	"670179": {
		"nik": "670179",
		"nama": "SURYO ADI KUSHARTONO, S.E.",
		"band": "IV",
		"posisi": "ASMAN SURVEILLANCE & LOGIC NE ACCESS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.048951048951049,
			"nilai_komparatif_unit": 1.0563019857032188,
			"summary_team": 0.9090909090909092
		}]
	},
	"670249": {
		"nik": "670249",
		"nama": "DANI RACHMANDANI",
		"band": "IV",
		"posisi": "ASMAN FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8545454545454549,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0531914893617016,
			"nilai_komparatif_unit": 1.0605721426666992,
			"summary_team": 0.8999999999999999
		}]
	},
	"670296": {
		"nik": "670296",
		"nama": "SUNARDI",
		"band": "IV",
		"posisi": "ASMAN ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9777777777777773,
			"nilai_komparatif_unit": 0.9846299398584665,
			"summary_team": 0.8799999999999998
		}]
	},
	"670315": {
		"nik": "670315",
		"nama": "MAIMUNAH",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7487179487179492,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1301369863013693,
			"nilai_komparatif_unit": 1.1380568654186043,
			"summary_team": 0.8461538461538461
		}]
	},
	"670320": {
		"nik": "670320",
		"nama": "EDIESA",
		"band": "IV",
		"posisi": "ASMAN CONSTRUCTION SUPERVISION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8589743589743583,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9313432835820904,
			"nilai_komparatif_unit": 0.9378700376942258,
			"summary_team": 0.8
		}]
	},
	"670321": {
		"nik": "670321",
		"nama": "KMS. M. YUSUF",
		"band": "IV",
		"posisi": "ASMAN BUSINESS TERITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8622222222222221,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0272459499263624,
			"nilai_komparatif_unit": 1.0344447796662082,
			"summary_team": 0.8857142857142857
		}]
	},
	"670331": {
		"nik": "670331",
		"nama": "UMI FATIMAH",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8874999999999998,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0328638497652582,
			"nilai_komparatif_unit": 1.040102049146268,
			"summary_team": 0.9166666666666665
		}]
	},
	"670332": {
		"nik": "670332",
		"nama": "RASIKUN",
		"band": "IV",
		"posisi": "ASMAN CONSTRUCTION SUPERVISION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8363636363636368,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9714673913043473,
			"nilai_komparatif_unit": 0.9782753308716082,
			"summary_team": 0.8125
		}]
	},
	"670334": {
		"nik": "670334",
		"nama": "PUJI KASTIYANI",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333332,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9285714285714286,
			"nilai_komparatif_unit": 0.935078757820135,
			"summary_team": 0.8666666666666666
		}]
	},
	"670340": {
		"nik": "670340",
		"nama": "BURHANUDDIN",
		"band": "IV",
		"posisi": "ASMAN BUSINESS TERITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8133333333333331,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9836065573770492,
			"nilai_komparatif_unit": 0.9904995669217067,
			"summary_team": 0.7999999999999998
		}]
	},
	"670405": {
		"nik": "670405",
		"nama": "HERRY SIAGA PUTRA",
		"band": "IV",
		"posisi": "ASMAN HOME SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0018348623853208,
			"nilai_komparatif_unit": 1.0088556139417235,
			"summary_team": 0.91
		}]
	},
	"670406": {
		"nik": "670406",
		"nama": "ZAINAL",
		"band": "IV",
		"posisi": "ASMAN MAINTENANCE & QE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8140350877192981,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9827586206896552,
			"nilai_komparatif_unit": 0.9896456879847053,
			"summary_team": 0.7999999999999999
		}]
	},
	"670492": {
		"nik": "670492",
		"nama": "TASRIFAL",
		"band": "IV",
		"posisi": "ASMAN MAINTENANCE & QE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444436,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0150375939849632,
			"nilai_komparatif_unit": 1.0221508688722132,
			"summary_team": 0.857142857142857
		}]
	},
	"670498": {
		"nik": "670498",
		"nama": "NANANG ROBIT MUSTHOFA, ST",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.877777777777778,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0433044636908724,
			"nilai_komparatif_unit": 1.050615829777514,
			"summary_team": 0.9157894736842105
		}]
	},
	"670504": {
		"nik": "670504",
		"nama": "ABDUL HAMID",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8615384615384616,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9948979591836732,
			"nilai_komparatif_unit": 1.0018700976644301,
			"summary_team": 0.857142857142857
		}]
	},
	"670505": {
		"nik": "670505",
		"nama": "YUSWANTO BASUKI",
		"band": "IV",
		"posisi": "ASMAN OM TRANSPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8222222222222226,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0216216216216212,
			"nilai_komparatif_unit": 1.028781036670302,
			"summary_team": 0.8400000000000001
		}]
	},
	"670506": {
		"nik": "670506",
		"nama": "RIKY HARGIARTO",
		"band": "IV",
		"posisi": "ASMAN INTEGRATION SYSTEM SITE ADVALJAR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9595141700404854,
			"nilai_komparatif_unit": 0.9662383427116807,
			"summary_team": 0.8315789473684211
		}]
	},
	"670508": {
		"nik": "670508",
		"nama": "BUDI SANTOSA",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0123929619837908,
			"summary_team": 0.8545454545454545
		}]
	},
	"670609": {
		"nik": "670609",
		"nama": "ADJI GUSRIJANTO",
		"band": "IV",
		"posisi": "OFF 1 FACILITY OPTIMALIZATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9777777777777779,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.022727272727273,
			"nilai_komparatif_unit": 1.0298944360606386,
			"summary_team": 1.0000000000000002
		}]
	},
	"670612": {
		"nik": "670612",
		"nama": "MOCH SJAMSURI",
		"band": "IV",
		"posisi": "OFF 1 CD ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8157894736842092,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9806451612903243,
			"nilai_komparatif_unit": 0.9875174176879657,
			"summary_team": 0.8
		}]
	},
	"680009": {
		"nik": "680009",
		"nama": "AGUSTINA MURWANI T.",
		"band": "IV",
		"posisi": "OFF 1 PPH",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666662,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9072580645161297,
			"nilai_komparatif_unit": 0.9136160319892765,
			"summary_team": 0.7500000000000001
		}]
	},
	"680040": {
		"nik": "680040",
		"nama": "SITI RAHAYU, ST",
		"band": "IV",
		"posisi": "ASMAN COMMUNICATION & SECRETARIATE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8060606060606066,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9786967418546357,
			"nilai_komparatif_unit": 0.9855553439372805,
			"summary_team": 0.7888888888888888
		}]
	},
	"680041": {
		"nik": "680041",
		"nama": "FATKHIYAH",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8060606060606066,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9924812030075182,
			"nilai_komparatif_unit": 0.9994364051194959,
			"summary_team": 0.8
		}]
	},
	"680092": {
		"nik": "680092",
		"nama": "SJILFIA FISKA",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888888,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857143,
			"nilai_komparatif_unit": 0.9710433254286018,
			"summary_team": 0.8571428571428571
		}]
	},
	"680093": {
		"nik": "680093",
		"nama": "HENRY AGUSTRY SITUMEANG",
		"band": "IV",
		"posisi": "ASMAN DATA CENTER & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0471698113207548,
			"nilai_komparatif_unit": 1.0545082653501379,
			"summary_team": 0.9250000000000002
		}]
	},
	"680096": {
		"nik": "680096",
		"nama": "DAHLIA",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0612244897959184,
			"nilai_komparatif_unit": 1.0686614375087258,
			"summary_team": 0.8666666666666668
		}]
	},
	"680097": {
		"nik": "680097",
		"nama": "MATLIZAR",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8717948717948718,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9831932773109242,
			"nilai_komparatif_unit": 0.990083390633084,
			"summary_team": 0.857142857142857
		}]
	},
	"680098": {
		"nik": "680098",
		"nama": "ASTUTI",
		"band": "IV",
		"posisi": "OFF 1 VERIFICATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7684210526315789,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0039138943248531,
			"nilai_komparatif_unit": 1.0109492155147086,
			"summary_team": 0.7714285714285712
		}]
	},
	"680100": {
		"nik": "680100",
		"nama": "SARWAN",
		"band": "IV",
		"posisi": "ASMAN TGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8491228070175435,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9421487603305788,
			"nilai_komparatif_unit": 0.9487512380679822,
			"summary_team": 0.7999999999999999
		}]
	},
	"680121": {
		"nik": "680121",
		"nama": "ANDREAS SETYO PRAYOGO",
		"band": "IV",
		"posisi": "OFF 1 SECURITY & SAFETY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9423076923076923,
			"nilai_komparatif_unit": 0.9489112838233915,
			"summary_team": 0.8166666666666668
		}]
	},
	"680122": {
		"nik": "680122",
		"nama": "AGUSTINA BETI SETIANINGSIH",
		"band": "IV",
		"posisi": "OFF 1 FINANCE SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.111111111111111,
			"nilai_komparatif_unit": 1.1188976589300759,
			"summary_team": 1
		}]
	},
	"680124": {
		"nik": "680124",
		"nama": "EDI RATMAN",
		"band": "IV",
		"posisi": "OFF 1 TELKOMGROUP FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0494402985074627,
			"nilai_komparatif_unit": 1.0567946638681922,
			"summary_team": 0.9375
		}]
	},
	"680257": {
		"nik": "680257",
		"nama": "OMBEDI",
		"band": "IV",
		"posisi": "ASMAN ACCESS ASSET MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444436,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0150375939849632,
			"nilai_komparatif_unit": 1.0221508688722132,
			"summary_team": 0.857142857142857
		}]
	},
	"680258": {
		"nik": "680258",
		"nama": "DELFI NORA",
		"band": "IV",
		"posisi": "OFF 1 CASH BANK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.038461538461538,
			"nilai_komparatif_unit": 1.0457389658461862,
			"summary_team": 0.8999999999999999
		}]
	},
	"680259": {
		"nik": "680259",
		"nama": "YONGNASFI",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999993,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000009,
			"nilai_komparatif_unit": 1.0070078930370694,
			"summary_team": 0.8
		}]
	},
	"680327": {
		"nik": "680327",
		"nama": "RUDY IRAWAN",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666665,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0204081632653064,
			"nilai_komparatif_unit": 1.0275590745276213,
			"summary_team": 0.8333333333333334
		}]
	},
	"680334": {
		"nik": "680334",
		"nama": "KAMASE EPRIYANTI",
		"band": "IV",
		"posisi": "ASMAN DIGITAL SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7600000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.996240601503759,
			"nilai_komparatif_unit": 1.0032221490782822,
			"summary_team": 0.757142857142857
		}]
	},
	"680336": {
		"nik": "680336",
		"nama": "ISNAINI",
		"band": "IV",
		"posisi": "ASMAN PARTNERSHIP & AGENCY 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8461538461538456,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8715909090909096,
			"nilai_komparatif_unit": 0.8776989249539,
			"summary_team": 0.7374999999999999
		}]
	},
	"680338": {
		"nik": "680338",
		"nama": "ESTER SIHOMBING",
		"band": "IV",
		"posisi": "OFF 1 PRODUCT SOLUTION & DELIVERY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8035087719298245,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0408892417626043,
			"nilai_komparatif_unit": 1.048183682232312,
			"summary_team": 0.8363636363636364
		}]
	},
	"680339": {
		"nik": "680339",
		"nama": "ASMER SINAGA",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7487179487179492,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9106475716064751,
			"nilai_komparatif_unit": 0.9170292923827595,
			"summary_team": 0.6818181818181818
		}]
	},
	"680344": {
		"nik": "680344",
		"nama": "SAPTO HADI WARSONO",
		"band": "IV",
		"posisi": "ASMAN IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.994082840236686,
			"nilai_komparatif_unit": 1.00104926645105,
			"summary_team": 0.8615384615384616
		}]
	},
	"680435": {
		"nik": "680435",
		"nama": "LILIK SUHAIMI",
		"band": "IV",
		"posisi": "ASMAN SAS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9030303030303032,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9333652924256949,
			"nilai_komparatif_unit": 0.9399062165595263,
			"summary_team": 0.8428571428571427
		}]
	},
	"680436": {
		"nik": "680436",
		"nama": "SITI NURUL HASANAH",
		"band": "IV",
		"posisi": "ASMAN TGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9142857142857144,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9687499999999999,
			"nilai_komparatif_unit": 0.9755388963796601,
			"summary_team": 0.8857142857142857
		}]
	},
	"680437": {
		"nik": "680437",
		"nama": "EVY TRI ERNINY",
		"band": "IV",
		"posisi": "ASMAN PARTNERSHIP INFRASTRUCTURE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444436,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857152,
			"nilai_komparatif_unit": 0.9710433254286027,
			"summary_team": 0.8142857142857142
		}]
	},
	"680438": {
		"nik": "680438",
		"nama": "SUHARNI",
		"band": "IV",
		"posisi": "ASMAN HR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.964285714285714,
			"nilai_komparatif_unit": 0.9710433254286015,
			"summary_team": 0.857142857142857
		}]
	},
	"680451": {
		"nik": "680451",
		"nama": "DARKO, S.E.",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8000000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999999,
			"nilai_komparatif_unit": 1.0070078930370685,
			"summary_team": 0.8000000000000002
		}]
	},
	"680457": {
		"nik": "680457",
		"nama": "AMANUL IHWAN",
		"band": "IV",
		"posisi": "ASMAN OM TRANSPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8844444444444444,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0088906068805568,
			"nilai_komparatif_unit": 1.0159608043396788,
			"summary_team": 0.8923076923076924
		}]
	},
	"680495": {
		"nik": "680495",
		"nama": "SUKANTA",
		"band": "IV",
		"posisi": "ASMAN BUSINESS TERITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.81159420289855,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9857142857142868,
			"nilai_komparatif_unit": 0.9926220659936829,
			"summary_team": 0.8000000000000002
		}]
	},
	"680505": {
		"nik": "680505",
		"nama": "PUJIYONO",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7333333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272727,
			"nilai_komparatif_unit": 1.0298944360606384,
			"summary_team": 0.75
		}]
	},
	"690008": {
		"nik": "690008",
		"nama": "WAHYU LESTARI",
		"band": "IV",
		"posisi": "OFF 1 CASH BANK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.972527472527472,
			"nilai_komparatif_unit": 0.9793428410305551,
			"summary_team": 0.8428571428571427
		}]
	},
	"690021": {
		"nik": "690021",
		"nama": "BUDI",
		"band": "IV",
		"posisi": "JM CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.871485943775102,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9885856079404448,
			"nilai_komparatif_unit": 0.9955135101388768,
			"summary_team": 0.8615384615384616
		}]
	},
	"690026": {
		"nik": "690026",
		"nama": "RUDI DARMAWAN",
		"band": "IV",
		"posisi": "ASMAN OM TRANSPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8419047619047649,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0986990950226205,
			"nilai_komparatif_unit": 1.106398660760463,
			"summary_team": 0.925
		}]
	},
	"690172": {
		"nik": "690172",
		"nama": "MURACHNO",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8733333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9651035986913846,
			"nilai_komparatif_unit": 0.9718669414807037,
			"summary_team": 0.8428571428571427
		}]
	},
	"690186": {
		"nik": "690186",
		"nama": "MULIADIN R.M",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7684210526315789,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9464508094645082,
			"nilai_komparatif_unit": 0.9530834355020824,
			"summary_team": 0.7272727272727273
		}]
	},
	"690189": {
		"nik": "690189",
		"nama": "YANI SUKARTY",
		"band": "IV",
		"posisi": "ASMAN HR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8589743589743583,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0371777476255097,
			"nilai_komparatif_unit": 1.044446178341297,
			"summary_team": 0.8909090909090909
		}]
	},
	"690190": {
		"nik": "690190",
		"nama": "MUHAMMAD SUHDI",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.877777777777778,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0253164556962024,
			"nilai_komparatif_unit": 1.0325017637468676,
			"summary_team": 0.9000000000000001
		}]
	},
	"690194": {
		"nik": "690194",
		"nama": "RUSTIANI",
		"band": "IV",
		"posisi": "ASMAN CRM & LEVERAGING CUSTOMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9095238095238096,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9995240361732508,
			"nilai_komparatif_unit": 1.0065285937067319,
			"summary_team": 0.9090909090909092
		}]
	},
	"690195": {
		"nik": "690195",
		"nama": "MOHAMAD FADIL",
		"band": "IV",
		"posisi": "ASMAN DEPLOYMENT & CAPEX MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444436,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0657894736842115,
			"nilai_komparatif_unit": 1.073258412315824,
			"summary_team": 0.8999999999999999
		}]
	},
	"690196": {
		"nik": "690196",
		"nama": "IMAN SUSILO",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9095238095238096,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0170157068062826,
			"nilai_komparatif_unit": 1.0241428440965996,
			"summary_team": 0.9249999999999999
		}]
	},
	"690273": {
		"nik": "690273",
		"nama": "ELVI SILALAHI",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999995,
			"nilai_komparatif_unit": 0.9667275773155853,
			"summary_team": 0.7999999999999999
		}]
	},
	"690274": {
		"nik": "690274",
		"nama": "SUBANDIO",
		"band": "IV",
		"posisi": "ASMAN DATA CENTER & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545454,
			"nilai_komparatif_unit": 1.0326408212234666,
			"summary_team": 0.8545454545454546
		}]
	},
	"690275": {
		"nik": "690275",
		"nama": "MONANG JETRO PARLUHUTAN SILABAN",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8285714285714285,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9655172413793103,
			"nilai_komparatif_unit": 0.9722834829323419,
			"summary_team": 0.7999999999999999
		}]
	},
	"690383": {
		"nik": "690383",
		"nama": "YOHANES DARSONO",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8615384615384616,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9633928571428569,
			"nilai_komparatif_unit": 0.9701442112383899,
			"summary_team": 0.8299999999999998
		}]
	},
	"690388": {
		"nik": "690388",
		"nama": "NENY DESMIYARSI, S.E",
		"band": "IV",
		"posisi": "ASMAN ADVALJAR OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9735849056603768,
			"nilai_komparatif_unit": 0.9804076845417492,
			"summary_team": 0.8599999999999998
		}]
	},
	"690394": {
		"nik": "690394",
		"nama": "SARWONO",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9771428571428569,
			"nilai_komparatif_unit": 0.9839905697676495,
			"summary_team": 0.8142857142857141
		}]
	},
	"690395": {
		"nik": "690395",
		"nama": "YENTI ERNAWATI",
		"band": "IV",
		"posisi": "ASMAN BUSINESS OBL & DELIVERY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9238095238095237,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.062792877225867,
			"nilai_komparatif_unit": 1.0702408160300243,
			"summary_team": 0.9818181818181818
		}]
	},
	"690396": {
		"nik": "690396",
		"nama": "SUMARWANTO",
		"band": "IV",
		"posisi": "ASMAN DATA CENTER & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8844444444444444,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0014357501794686,
			"nilai_komparatif_unit": 1.008453704800223,
			"summary_team": 0.8857142857142856
		}]
	},
	"690400": {
		"nik": "690400",
		"nama": "SULISTYOWATI, S.E.",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0467289719626167,
			"nilai_komparatif_unit": 1.0540643366369313,
			"summary_team": 0.9333333333333335
		}]
	},
	"690401": {
		"nik": "690401",
		"nama": "TITIN HARDININGSIH, SH",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9898989898989898,
			"nilai_komparatif_unit": 0.9968360961377041,
			"summary_team": 0.8909090909090911
		}]
	},
	"690407": {
		"nik": "690407",
		"nama": "ADE NIRWANI",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9545454545454545,
			"nilai_komparatif_unit": 0.961234806989929,
			"summary_team": 0.8272727272727274
		}]
	},
	"700006": {
		"nik": "700006",
		"nama": "NIRWAN ARIF",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333332,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.012987012987013,
			"nilai_komparatif_unit": 1.0200859176219657,
			"summary_team": 0.9454545454545454
		}]
	},
	"700007": {
		"nik": "700007",
		"nama": "ERY SURYADI",
		"band": "IV",
		"posisi": "OFF 1 HR SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7833333333333332,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9516441005802709,
			"nilai_komparatif_unit": 0.9583131206464948,
			"summary_team": 0.7454545454545455
		}]
	},
	"700159": {
		"nik": "700159",
		"nama": "ERLINA",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8428571428571432,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0354391371340521,
			"nilai_komparatif_unit": 1.042695383853482,
			"summary_team": 0.8727272727272728
		}]
	},
	"700160": {
		"nik": "700160",
		"nama": "AZHAR",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8285714285714285,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.875,
			"nilai_komparatif_unit": 0.881131906407435,
			"summary_team": 0.725
		}]
	},
	"700161": {
		"nik": "700161",
		"nama": "ZULMAN EFFENDI",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK & LEGAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8379084967320288,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9547581903276101,
			"nilai_komparatif_unit": 0.9614490336016911,
			"summary_team": 0.8
		}]
	},
	"700162": {
		"nik": "700162",
		"nama": "AMIR MAHMUD",
		"band": "IV",
		"posisi": "MGR CCAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8000000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0535714285714282,
			"nilai_komparatif_unit": 1.0609547444497682,
			"summary_team": 0.8428571428571427
		}]
	},
	"700164": {
		"nik": "700164",
		"nama": "YUSI OCTIVIANI",
		"band": "IV",
		"posisi": "ASMAN GES TERITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.875362318840579,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.017760385310055,
			"nilai_komparatif_unit": 1.0248927412276734,
			"summary_team": 0.8909090909090909
		}]
	},
	"700166": {
		"nik": "700166",
		"nama": "ELVI",
		"band": "IV",
		"posisi": "ASMAN CRM & LEVERAGING CUSTOMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8577777777777779,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0492227979274609,
			"nilai_komparatif_unit": 1.0565756390673904,
			"summary_team": 0.8999999999999998
		}]
	},
	"700190": {
		"nik": "700190",
		"nama": "TEGUH TRIYONO",
		"band": "IV",
		"posisi": "OFF 1 SECURITY & SAFETY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.979591836734694,
			"nilai_komparatif_unit": 0.9864567115465162,
			"summary_team": 0.8000000000000002
		}]
	},
	"700256": {
		"nik": "700256",
		"nama": "EFALINDA BR MANIHURUK",
		"band": "IV",
		"posisi": "OFF 1 PROJECT MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.92,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.024844720496894,
			"nilai_komparatif_unit": 1.0320267226777406,
			"summary_team": 0.9428571428571426
		}]
	},
	"700258": {
		"nik": "700258",
		"nama": "RUMINDU SITUMORANG",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.941176470588236,
			"nilai_komparatif_unit": 0.9477721346231239,
			"summary_team": 0.7999999999999998
		}]
	},
	"700263": {
		"nik": "700263",
		"nama": "SYAFINATUNNAZAH",
		"band": "IV",
		"posisi": "OFF 1 PROGRAM & EVALUATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8000000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.0070078930370683,
			"summary_team": 0.7999999999999999
		}]
	},
	"700264": {
		"nik": "700264",
		"nama": "ESTER OKTAVERA",
		"band": "IV",
		"posisi": "OFF 1 ENTERPRISE ACCOUNT MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0255102040816326,
			"nilai_komparatif_unit": 1.032696869900259,
			"summary_team": 0.9571428571428571
		}]
	},
	"700269": {
		"nik": "700269",
		"nama": "ERNAWATI",
		"band": "IV",
		"posisi": "JM CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8631578947368416,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0592334494773523,
			"nilai_komparatif_unit": 1.0666564441925748,
			"summary_team": 0.9142857142857141
		}]
	},
	"700339": {
		"nik": "700339",
		"nama": "SUTARNO",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0666666666666667,
			"nilai_komparatif_unit": 1.0741417525728731,
			"summary_team": 0.9600000000000002
		}]
	},
	"700349": {
		"nik": "700349",
		"nama": "BUDIYANTO",
		"band": "IV",
		"posisi": "ASMAN SURVEILLANCE & LOGIC NE ACCESS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.011764705882353,
			"nilai_komparatif_unit": 1.0188550447198574,
			"summary_team": 0.8600000000000002
		}]
	},
	"700404": {
		"nik": "700404",
		"nama": "WIWIK SRI HARTATI",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9157894736842102,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8891625615763549,
			"nilai_komparatif_unit": 0.8953937177004478,
			"summary_team": 0.8142857142857142
		}]
	},
	"700405": {
		"nik": "700405",
		"nama": "EVY EMMA SIAHAAN",
		"band": "IV",
		"posisi": "ASMAN DEBT MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999995,
			"nilai_komparatif_unit": 0.9667275773155853,
			"summary_team": 0.7999999999999999
		}]
	},
	"700423": {
		"nik": "700423",
		"nama": "DESFITRI",
		"band": "IV",
		"posisi": "ASMAN ACCESS ASSET MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8419047619047649,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0690045248868738,
			"nilai_komparatif_unit": 1.0764959942534233,
			"summary_team": 0.8999999999999999
		}]
	},
	"700425": {
		"nik": "700425",
		"nama": "RENO LAILA",
		"band": "IV",
		"posisi": "ASMAN PARTNERSHIP INFRASTRUCTURE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8589743589743583,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1006784260515612,
			"nilai_komparatif_unit": 1.1083918627295395,
			"summary_team": 0.9454545454545454
		}]
	},
	"700430": {
		"nik": "700430",
		"nama": "ANIEK YULIANI, SE",
		"band": "IV",
		"posisi": "ASMAN BUSINESS TERITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666663,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0195852534562215,
			"nilai_komparatif_unit": 1.0267303978546152,
			"summary_team": 0.8428571428571427
		}]
	},
	"700431": {
		"nik": "700431",
		"nama": "MARGARETHA ETTY INDRIATI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8972222222222216,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9726991274978899,
			"nilai_komparatif_unit": 0.9795156989406449,
			"summary_team": 0.8727272727272728
		}]
	},
	"700433": {
		"nik": "700433",
		"nama": "WIWIK RATNA KUSUMAWATI, SE",
		"band": "IV",
		"posisi": "ASMAN QUALITY ENHANCEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666664,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9352226720647778,
			"nilai_komparatif_unit": 0.9417766125164492,
			"summary_team": 0.8105263157894739
		}]
	},
	"700438": {
		"nik": "700438",
		"nama": "DWI WAHYUNINGSIH",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666671,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346935,
			"nilai_komparatif_unit": 0.9864567115465157,
			"summary_team": 0.8000000000000002
		}]
	},
	"700440": {
		"nik": "700440",
		"nama": "SRI WAHYUNI, SE",
		"band": "IV",
		"posisi": "ASMAN CDC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8380952380952383,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9545454545454546,
			"nilai_komparatif_unit": 0.9612348069899291,
			"summary_team": 0.8000000000000003
		}]
	},
	"700441": {
		"nik": "700441",
		"nama": "DARWIYANTI",
		"band": "IV",
		"posisi": "JM CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8484848484848507,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857117,
			"nilai_komparatif_unit": 0.9710433254285993,
			"summary_team": 0.8181818181818182
		}]
	},
	"700442": {
		"nik": "700442",
		"nama": "TRI WIDYASTUTI",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9387755102040817,
			"nilai_komparatif_unit": 0.9453543485654113,
			"summary_team": 0.7666666666666668
		}]
	},
	"700443": {
		"nik": "700443",
		"nama": "UHDA RUBIANI, S.E.",
		"band": "IV",
		"posisi": "OFF 1 BUDGET OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8222222222222224,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9729729729729727,
			"nilai_komparatif_unit": 0.9797914634955258,
			"summary_team": 0.7999999999999999
		}]
	},
	"700471": {
		"nik": "700471",
		"nama": "IGET",
		"band": "IV",
		"posisi": "ASMAN OM TRANSPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8207407407407394,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0633409911388267,
			"nilai_komparatif_unit": 1.070792771066658,
			"summary_team": 0.8727272727272728
		}]
	},
	"700472": {
		"nik": "700472",
		"nama": "YUSISWANDI",
		"band": "IV",
		"posisi": "ASMAN DATA CENTER & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8207407407407394,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9747292418772582,
			"nilai_komparatif_unit": 0.9815600401444369,
			"summary_team": 0.8000000000000003
		}]
	},
	"700504": {
		"nik": "700504",
		"nama": "SEFRIANDI",
		"band": "IV",
		"posisi": "ASMAN WIFI ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8419047619047649,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.950226244343888,
			"nilai_komparatif_unit": 0.9568853282252653,
			"summary_team": 0.8
		}]
	},
	"700563": {
		"nik": "700563",
		"nama": "HAPOSAN TURNIP",
		"band": "IV",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.835294117647058,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0261569416499003,
			"nilai_komparatif_unit": 1.0333481397362283,
			"summary_team": 0.857142857142857
		}]
	},
	"700649": {
		"nik": "700649",
		"nama": "WAHYUDIN SOFYAN",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK & LEGAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8000000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0749999999999995,
			"nilai_komparatif_unit": 1.0825334850148483,
			"summary_team": 0.8599999999999998
		}]
	},
	"710067": {
		"nik": "710067",
		"nama": "SISWANTO",
		"band": "IV",
		"posisi": "ASMAN SURVEILLANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666664,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0070078930370687,
			"summary_team": 0.8666666666666666
		}]
	},
	"710072": {
		"nik": "710072",
		"nama": "YULITA",
		"band": "IV",
		"posisi": "ASMAN COMMUNICATION & SECRETARIATE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9890109890109888,
			"nilai_komparatif_unit": 0.9959418722344632,
			"summary_team": 0.857142857142857
		}]
	},
	"710073": {
		"nik": "710073",
		"nama": "ARFAN SUATAN",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.875362318840579,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0444654683065286,
			"nilai_komparatif_unit": 1.0517849705893325,
			"summary_team": 0.9142857142857141
		}]
	},
	"710074": {
		"nik": "710074",
		"nama": "RITA TRIANA",
		"band": "IV",
		"posisi": "ASMAN COMMUNICATION & SECRETARIATE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1114922813036017,
			"nilai_komparatif_unit": 1.1192815003225045,
			"summary_team": 0.9818181818181818
		}]
	},
	"710076": {
		"nik": "710076",
		"nama": "FITRI YENI",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8208333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9081679741578219,
			"nilai_komparatif_unit": 0.9145323181804111,
			"summary_team": 0.7454545454545455
		}]
	},
	"710116": {
		"nik": "710116",
		"nama": "SULASMIATI",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8631578947368416,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9900221729490029,
			"nilai_komparatif_unit": 0.9969601424413557,
			"summary_team": 0.8545454545454546
		}]
	},
	"710118": {
		"nik": "710118",
		"nama": "REFINA MAGDA NAINGGOLAN",
		"band": "IV",
		"posisi": "OFF 1 SMALL & MICRO CUSTOMER MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9422222222222221,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.924865229110512,
			"nilai_komparatif_unit": 0.9313465857098224,
			"summary_team": 0.8714285714285712
		}]
	},
	"710120": {
		"nik": "710120",
		"nama": "KALSUM ALSYAHRANI",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8285714285714285,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0948275862068968,
			"nilai_komparatif_unit": 1.1025000208250666,
			"summary_team": 0.9071428571428573
		}]
	},
	"710121": {
		"nik": "710121",
		"nama": "NURHAYATI",
		"band": "IV",
		"posisi": "ASMAN ACCESS PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7948717948717953,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.952534562211981,
			"nilai_komparatif_unit": 0.9592098225380734,
			"summary_team": 0.757142857142857
		}]
	},
	"710122": {
		"nik": "710122",
		"nama": "NURHAFLAWATI",
		"band": "IV",
		"posisi": "OFF 1 OSS APPLICATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0714285714285712,
			"nilai_komparatif_unit": 1.0789370282540018,
			"summary_team": 0.957142857142857
		}]
	},
	"710127": {
		"nik": "710127",
		"nama": "NARTI TRIYANI YAHYA",
		"band": "IV",
		"posisi": "ASMAN NETWORK PERFORMANCE, QM & ADMIN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8844444444444444,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0337401292175161,
			"nilai_komparatif_unit": 1.0409844694711978,
			"summary_team": 0.9142857142857141
		}]
	},
	"710130": {
		"nik": "710130",
		"nama": "YUL HIDAYATI",
		"band": "IV",
		"posisi": "OFF 1 VERIFICATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9190476190476191,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0414507772020725,
			"nilai_komparatif_unit": 1.0487491528520765,
			"summary_team": 0.9571428571428571
		}]
	},
	"710134": {
		"nik": "710134",
		"nama": "TRI MARTININGSIH",
		"band": "IV",
		"posisi": "OFF 1 BUDGET OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8874999999999998,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9628681177976953,
			"nilai_komparatif_unit": 0.9696157945760251,
			"summary_team": 0.8545454545454545
		}]
	},
	"710216": {
		"nik": "710216",
		"nama": "RATNA WITA",
		"band": "IV",
		"posisi": "OFF 1 CASH BANK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9190476190476191,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9948186528497407,
			"nilai_komparatif_unit": 1.0017902355601922,
			"summary_team": 0.9142857142857141
		}]
	},
	"710218": {
		"nik": "710218",
		"nama": "YAYUK INDRASWATI",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7833333333333332,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9284332688588011,
			"nilai_komparatif_unit": 0.9349396298990195,
			"summary_team": 0.7272727272727274
		}]
	},
	"710221": {
		"nik": "710221",
		"nama": "SRI YULIANA, SH",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8410256410256403,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.002177700348433,
			"nilai_komparatif_unit": 1.00920085447661,
			"summary_team": 0.8428571428571427
		}]
	},
	"710222": {
		"nik": "710222",
		"nama": "SRI HERAWATI,S.E.",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8733333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0305343511450378,
			"nilai_komparatif_unit": 1.037756225648887,
			"summary_team": 0.8999999999999999
		}]
	},
	"710223": {
		"nik": "710223",
		"nama": "CANTI NUGRAHENI",
		"band": "IV",
		"posisi": "ASMAN GES BIDDING MANAGEMENT & OBL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8385964912280698,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1057436287561815,
			"nilai_komparatif_unit": 1.1134925618329248,
			"summary_team": 0.9272727272727272
		}]
	},
	"710224": {
		"nik": "710224",
		"nama": "MAHARANI RATIH DWIASIH, S.E.",
		"band": "IV",
		"posisi": "ASMAN BUSINESS OBL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666663,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0104364326375717,
			"nilai_komparatif_unit": 1.0175174630782529,
			"summary_team": 0.835294117647059
		}]
	},
	"710225": {
		"nik": "710225",
		"nama": "LAILA DIYAH HASANAH",
		"band": "IV",
		"posisi": "OFF 1 LOGISTIC PLANNING & EVALUATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8222222222222224,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0015898251192366,
			"nilai_komparatif_unit": 1.0086088594806883,
			"summary_team": 0.8235294117647058
		}]
	},
	"710241": {
		"nik": "710241",
		"nama": "SITI SUDIYARTI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8972222222222216,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9726991274978899,
			"nilai_komparatif_unit": 0.9795156989406449,
			"summary_team": 0.8727272727272728
		}]
	},
	"710264": {
		"nik": "710264",
		"nama": "SISWANTO",
		"band": "IV",
		"posisi": "ASMAN CCAN FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769227,
			"nilai_komparatif_unit": 0.9295457474188321,
			"summary_team": 0.7999999999999999
		}]
	},
	"710340": {
		"nik": "710340",
		"nama": "RUFFIE",
		"band": "IV",
		"posisi": "OFF 1 MAINTENANCE ENGINEERING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8277777777777782,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9884075655887732,
			"nilai_komparatif_unit": 0.9953342200854486,
			"summary_team": 0.8181818181818182
		}]
	},
	"710514": {
		"nik": "710514",
		"nama": "DIANA SUSAN",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8866666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9183673469387751,
			"nilai_komparatif_unit": 0.9248031670748584,
			"summary_team": 0.8142857142857142
		}]
	},
	"720001": {
		"nik": "720001",
		"nama": "ERLINA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8133333333333331,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.088992974238876,
			"nilai_komparatif_unit": 1.0966245205204612,
			"summary_team": 0.8857142857142856
		}]
	},
	"720013": {
		"nik": "720013",
		"nama": "MASYITHOH LUBIS",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9095238095238096,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8995716325559258,
			"nilai_komparatif_unit": 0.9058757343360588,
			"summary_team": 0.8181818181818182
		}]
	},
	"720019": {
		"nik": "720019",
		"nama": "MARDIANA KARMILA",
		"band": "IV",
		"posisi": "ASMAN HR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9948979591836732,
			"nilai_komparatif_unit": 1.0018700976644301,
			"summary_team": 0.9285714285714284
		}]
	},
	"720032": {
		"nik": "720032",
		"nama": "KHADIJAH",
		"band": "IV",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8379084967320288,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9547581903276101,
			"nilai_komparatif_unit": 0.9614490336016911,
			"summary_team": 0.8
		}]
	},
	"720034": {
		"nik": "720034",
		"nama": "WISNEL",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8419047619047649,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9502262443438879,
			"nilai_komparatif_unit": 0.9568853282252652,
			"summary_team": 0.7999999999999999
		}]
	},
	"720035": {
		"nik": "720035",
		"nama": "JANUARINI HARTANTI. SE.",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8410256410256403,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.070121951219513,
			"nilai_komparatif_unit": 1.0776212513902785,
			"summary_team": 0.8999999999999999
		}]
	},
	"720036": {
		"nik": "720036",
		"nama": "RACHMANI SARWENDYAH, SE",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346933,
			"nilai_komparatif_unit": 0.9864567115465155,
			"summary_team": 0.7999999999999998
		}]
	},
	"720042": {
		"nik": "720042",
		"nama": "ELPI SAPUTRA",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999993,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267857142857155,
			"nilai_komparatif_unit": 1.03398131874342,
			"summary_team": 0.8214285714285716
		}]
	},
	"720058": {
		"nik": "720058",
		"nama": "WILHELMUS FANDA",
		"band": "IV",
		"posisi": "ASMAN ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8740740740740743,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.010593220338983,
			"nilai_komparatif_unit": 1.017675349531105,
			"summary_team": 0.8833333333333335
		}]
	},
	"720315": {
		"nik": "720315",
		"nama": "MADHE PIETERS AGUNG R BASTIAN NALLE",
		"band": "IV",
		"posisi": "OFF 1 LAN MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300696,
			"nilai_komparatif_unit": 1.0774280254172828,
			"summary_team": 0.9272727272727272
		}]
	},
	"720519": {
		"nik": "720519",
		"nama": "IMAM WAHYUDI",
		"band": "IV",
		"posisi": "OFF 1 INSTRUMENTATION & CALIBRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8277777777777782,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9664429530201338,
			"nilai_komparatif_unit": 0.9732156818613276,
			"summary_team": 0.8
		}]
	},
	"720522": {
		"nik": "720522",
		"nama": "HENRIE BRILLIANTORO",
		"band": "IV",
		"posisi": "ASMAN Wifi Fulfillment & Assurance",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7904761904761907,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1040525739320919,
			"nilai_komparatif_unit": 1.1117896562775083,
			"summary_team": 0.8727272727272728
		}]
	},
	"730016": {
		"nik": "730016",
		"nama": "NENI TRIANA",
		"band": "IV",
		"posisi": "ASMAN FINANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7904761904761907,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0120481927710843,
			"nilai_komparatif_unit": 1.0191405182543825,
			"summary_team": 0.8000000000000002
		}]
	},
	"730017": {
		"nik": "730017",
		"nama": "SRI HANDAYANI, S.E.",
		"band": "IV",
		"posisi": "ASMAN BUSINESS OBL & DELIVERY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8972222222222216,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9119054320292719,
			"nilai_komparatif_unit": 0.9182959677568547,
			"summary_team": 0.8181818181818183
		}]
	},
	"730031": {
		"nik": "730031",
		"nama": "RUSLI LEURIMA",
		"band": "IV",
		"posisi": "ASMAN SAS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8499999999999994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0084033613445382,
			"nilai_komparatif_unit": 1.015470144239061,
			"summary_team": 0.857142857142857
		}]
	},
	"730123": {
		"nik": "730123",
		"nama": "MAMAN DURACHMANSYAH",
		"band": "IV",
		"posisi": "OFF 1 BUSINESS APPLICATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139857,
			"nilai_komparatif_unit": 0.9929238665610253,
			"summary_team": 0.8545454545454546
		}]
	},
	"730467": {
		"nik": "730467",
		"nama": "DWILITA FAJARWATI, SE",
		"band": "IV",
		"posisi": "OFF 1 OPERATION CONTACT CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300698,
			"nilai_komparatif_unit": 1.077428025417283,
			"summary_team": 0.9272727272727272
		}]
	},
	"730544": {
		"nik": "730544",
		"nama": "R. HERJUNO KUSUMO HAPSORO,ST",
		"band": "IV",
		"posisi": "ASMAN DIGITAL SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9719626168224298,
			"nilai_komparatif_unit": 0.9787740268771506,
			"summary_team": 0.8666666666666668
		}]
	},
	"740041": {
		"nik": "740041",
		"nama": "ANIK FATIHATIN",
		"band": "IV",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0070078930370687,
			"summary_team": 0.8
		}]
	},
	"740047": {
		"nik": "740047",
		"nama": "CHRISCENTIANA DYAH I.",
		"band": "IV",
		"posisi": "OFF 1 BILLING SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9688581314878892,
			"nilai_komparatif_unit": 0.9756477856414505,
			"summary_team": 0.8235294117647058
		}]
	},
	"740102": {
		"nik": "740102",
		"nama": "YUSTINA ANEKE SARI",
		"band": "IV",
		"posisi": "OFF 1 CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.92,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.971014492753623,
			"nilai_komparatif_unit": 0.9778192584562837,
			"summary_team": 0.8933333333333332
		}]
	},
	"740295": {
		"nik": "740295",
		"nama": "SANTI WIDYASARI",
		"band": "IV",
		"posisi": "OFF 1 REVENUE ASSURANCE & FRAUD MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9444444444444446,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0030959752321977,
			"nilai_komparatif_unit": 1.0101255645325389,
			"summary_team": 0.9473684210526314
		}]
	},
	"740314": {
		"nik": "740314",
		"nama": "TRI RAHMI OFIANTI",
		"band": "IV",
		"posisi": "ASMAN GES BIDDING & PROJECT MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8499999999999994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0084033613445382,
			"nilai_komparatif_unit": 1.015470144239061,
			"summary_team": 0.857142857142857
		}]
	},
	"750065": {
		"nik": "750065",
		"nama": "ROSANA INTAN PERMATASARI",
		"band": "IV",
		"posisi": "MGR FINANCE & PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8222222222222224,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9729729729729728,
			"nilai_komparatif_unit": 0.9797914634955259,
			"summary_team": 0.8
		}]
	},
	"760010": {
		"nik": "760010",
		"nama": "NURSIA DANA",
		"band": "IV",
		"posisi": "ASMAN CCAN WIFI FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "CF-2-01",
			"kriteria": 0.8750000000000003,
			"kriteria_bp": 0.9951792317081541,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0285714285714282,
			"nilai_komparatif_unit": 1.0335539526944897,
			"summary_team": 0.9000000000000001
		}]
	},
	"760031": {
		"nik": "760031",
		"nama": "IWAN KUSSUGIARTO PUTRO",
		"band": "IV",
		"posisi": "OFF 1 INFRA FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0123929619837908,
			"summary_team": 0.8545454545454546
		}]
	},
	"770001": {
		"nik": "770001",
		"nama": "RASIDIN M.H. HUTAGAOL",
		"band": "IV",
		"posisi": "OFF 1 INSTRUMENTATION & CALIBRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7777777777777777,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9321428571428574,
			"nilai_komparatif_unit": 0.938675214580982,
			"summary_team": 0.7250000000000001
		}]
	},
	"780006": {
		"nik": "780006",
		"nama": "SAN VRISCA RUMONDANG",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0195852534562209,
			"nilai_komparatif_unit": 1.0267303978546145,
			"summary_team": 0.8428571428571427
		}]
	},
	"780023": {
		"nik": "780023",
		"nama": "EKA SAFRI",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK & LEGAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9030303030303032,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0161863403079352,
			"nilai_komparatif_unit": 1.0233076654865434,
			"summary_team": 0.9176470588235295
		}]
	},
	"780049": {
		"nik": "780049",
		"nama": "FITTY MUSTIKA",
		"band": "IV",
		"posisi": "ASMAN GES SALES ENGINEER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8923076923076925,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9780564263322883,
			"nilai_komparatif_unit": 0.9849105411522424,
			"summary_team": 0.8727272727272728
		}]
	},
	"790005": {
		"nik": "790005",
		"nama": "ANIES YUNITA SARI",
		"band": "IV",
		"posisi": "OFF 1 INFRA OPERATION PERFORMANCE MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8083333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9896907216494841,
			"nilai_komparatif_unit": 0.9966263683665829,
			"summary_team": 0.8
		}]
	},
	"790014": {
		"nik": "790014",
		"nama": "YUDI IRWANTO,S.Kom",
		"band": "IV",
		"posisi": "MGR NTE MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8385964912280698,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.95397489539749,
			"nilai_komparatif_unit": 0.9606602494244842,
			"summary_team": 0.8
		}]
	},
	"790025": {
		"nik": "790025",
		"nama": "ASNA MUNIATI",
		"band": "IV",
		"posisi": "OFF 1 IP OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999995,
			"nilai_komparatif_unit": 0.9667275773155853,
			"summary_team": 0.7999999999999999
		}]
	},
	"790068": {
		"nik": "790068",
		"nama": "BETTY KARNALIS",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK & LEGAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545454,
			"nilai_komparatif_unit": 1.0326408212234666,
			"summary_team": 0.8545454545454546
		}]
	},
	"800024": {
		"nik": "800024",
		"nama": "RISKA SURYANI, M.T",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.81159420289855,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.003316326530613,
			"nilai_komparatif_unit": 1.010347460029284,
			"summary_team": 0.8142857142857142
		}]
	},
	"800035": {
		"nik": "800035",
		"nama": "FAHMI ISKANDAR LUBIS",
		"band": "IV",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8745098039215687,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9355890746025275,
			"nilai_komparatif_unit": 0.942145582763992,
			"summary_team": 0.8181818181818182
		}]
	},
	"810015": {
		"nik": "810015",
		"nama": "AHMAD FURQONI AFI",
		"band": "IV",
		"posisi": "ASMAN TGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8592592592592596,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0396551724137926,
			"nilai_komparatif_unit": 1.0469409646575034,
			"summary_team": 0.8933333333333333
		}]
	},
	"810025": {
		"nik": "810025",
		"nama": "WAHYU ADI SYAHPUTRA",
		"band": "IV",
		"posisi": "OFF 1 GOVERNMENT ACCOUNT MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8724489795918366,
			"nilai_komparatif_unit": 0.8785630087211158,
			"summary_team": 0.8142857142857142
		}]
	},
	"810027": {
		"nik": "810027",
		"nama": "DINA KHAIRA BATUBARA",
		"band": "IV",
		"posisi": "ASMAN ADVALJAR OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8140350877192981,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.020557029177719,
			"nilai_komparatif_unit": 1.027708983676425,
			"summary_team": 0.8307692307692308
		}]
	},
	"810028": {
		"nik": "810028",
		"nama": "MERI NOFIANTI",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8256410256410259,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9689440993788818,
			"nilai_komparatif_unit": 0.9757343559862277,
			"summary_team": 0.8000000000000002
		}]
	},
	"820063": {
		"nik": "820063",
		"nama": "ANETTA NIMASAYU",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8972222222222216,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9119054320292718,
			"nilai_komparatif_unit": 0.9182959677568546,
			"summary_team": 0.8181818181818182
		}]
	},
	"830095": {
		"nik": "830095",
		"nama": "DIANTY ELISIANA",
		"band": "IV",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.871485943775102,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9638709677419337,
			"nilai_komparatif_unit": 0.9706256723854049,
			"summary_team": 0.84
		}]
	},
	"840008": {
		"nik": "840008",
		"nama": "ARIE SULISTYANINGRUM",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444444,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.048872180451128,
			"nilai_komparatif_unit": 1.0562225645012864,
			"summary_team": 0.8857142857142859
		}]
	},
	"840012": {
		"nik": "840012",
		"nama": "VERRY DIAN ROMANTO",
		"band": "IV",
		"posisi": "OFF 1 CAREER DEVELOPMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8733333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0305343511450378,
			"nilai_komparatif_unit": 1.037756225648887,
			"summary_team": 0.8999999999999999
		}]
	},
	"840035": {
		"nik": "840035",
		"nama": "HAPSARI INDAH PUJIATI",
		"band": "IV",
		"posisi": "ASMAN HOME SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0070078930370687,
			"summary_team": 0.8666666666666668
		}]
	},
	"840042": {
		"nik": "840042",
		"nama": "MADZARIO AKBAR SIREGAR",
		"band": "IV",
		"posisi": "ASMAN OM IP NETWORK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8633333333333328,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9898209898209904,
			"nilai_komparatif_unit": 0.9967575494435013,
			"summary_team": 0.8545454545454546
		}]
	},
	"840046": {
		"nik": "840046",
		"nama": "ARY SURACAHYADI",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8962962962962964,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9563164108618651,
			"nilai_komparatif_unit": 0.9630181739787783,
			"summary_team": 0.857142857142857
		}]
	},
	"840049": {
		"nik": "840049",
		"nama": "M. RIDWAN",
		"band": "IV",
		"posisi": "ASMAN SURVEILLANCE & LOGIC NE ACCESS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8140350877192981,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9827586206896554,
			"nilai_komparatif_unit": 0.9896456879847054,
			"summary_team": 0.8
		}]
	},
	"840078": {
		"nik": "840078",
		"nama": "ULFA FITRIYANI GAFFAR",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8596491228070169,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0998144712430435,
			"nilai_komparatif_unit": 1.1075218534181348,
			"summary_team": 0.9454545454545454
		}]
	},
	"840080": {
		"nik": "840080",
		"nama": "DWISARI WIGUNA",
		"band": "IV",
		"posisi": "ASMAN GES QUALITY & PROJECT MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9461538461538462,
			"nilai_komparatif_unit": 0.9527843911043034,
			"summary_team": 0.8200000000000001
		}]
	},
	"840082": {
		"nik": "840082",
		"nama": "SITI MUTHMAINNAH",
		"band": "IV",
		"posisi": "OFF 1 VERIFICATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8622222222222221,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.977540500736377,
			"nilai_komparatif_unit": 0.98439100000494,
			"summary_team": 0.8428571428571427
		}]
	},
	"840113": {
		"nik": "840113",
		"nama": "TATIT HIDAYATI",
		"band": "IV",
		"posisi": "OFF 1 COLLECTION PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666662,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9897360703812322,
			"nilai_komparatif_unit": 0.9966720348973924,
			"summary_team": 0.8181818181818182
		}]
	},
	"840122": {
		"nik": "840122",
		"nama": "DIANI MARDISAR",
		"band": "IV",
		"posisi": "OFF 1 FINANCE SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8874999999999998,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.065300896286812,
			"nilai_komparatif_unit": 1.0727664110202833,
			"summary_team": 0.9454545454545454
		}]
	},
	"840143": {
		"nik": "840143",
		"nama": "INDRIANA OKTAVIA SINAMBELA",
		"band": "IV",
		"posisi": "OFF 1 QUALITY & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.027972027972028,
			"nilai_komparatif_unit": 1.0351759459891543,
			"summary_team": 0.8909090909090909
		}]
	},
	"840166": {
		"nik": "840166",
		"nama": "MITA PERMANA SARI",
		"band": "IV",
		"posisi": "OFF 1 PPN & NON TAX OBLIGATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666662,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0584677419354847,
			"nilai_komparatif_unit": 1.065885370654156,
			"summary_team": 0.8750000000000002
		}]
	},
	"850002": {
		"nik": "850002",
		"nama": "NURHASANAH  SIREGAR",
		"band": "IV",
		"posisi": "OFF 1 BILLING SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8962962962962964,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.054845980465815,
			"nilai_komparatif_unit": 1.0622382282675011,
			"summary_team": 0.9454545454545454
		}]
	},
	"850011": {
		"nik": "850011",
		"nama": "SUCI LESTARI",
		"band": "IV",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8379084967320288,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8751950078003091,
			"nilai_komparatif_unit": 0.88132828080155,
			"summary_team": 0.7333333333333333
		}]
	},
	"850012": {
		"nik": "850012",
		"nama": "IMAM SUMANTRI HARAHAP",
		"band": "IV",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8633333333333328,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0740610740610745,
			"nilai_komparatif_unit": 1.0815879791833736,
			"summary_team": 0.9272727272727272
		}]
	},
	"850014": {
		"nik": "850014",
		"nama": "T. SURYA HASRI",
		"band": "IV",
		"posisi": "ASMAN ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.864102564102563,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9258160237388735,
			"nilai_komparatif_unit": 0.9323040434052396,
			"summary_team": 0.7999999999999999
		}]
	},
	"850023": {
		"nik": "850023",
		"nama": "RIZKY TRIYUT ARMANSYAH",
		"band": "IV",
		"posisi": "OFF 1 CRISIS SUPPORT MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8571428571428573,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.033333333333333,
			"nilai_komparatif_unit": 1.0405748228049705,
			"summary_team": 0.8857142857142857
		}]
	},
	"850024": {
		"nik": "850024",
		"nama": "ANDI SYAPUTRA",
		"band": "IV",
		"posisi": "ASMAN DEPLOYMENT & CAPEX MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.864102564102563,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9547477744807135,
			"nilai_komparatif_unit": 0.9614385447616535,
			"summary_team": 0.8250000000000001
		}]
	},
	"850030": {
		"nik": "850030",
		"nama": "SUNEDY ABDILA",
		"band": "IV",
		"posisi": "ASMAN INTEGRATION SYSTEM SITE ADVALJAR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7948717948717953,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0064516129032257,
			"nilai_komparatif_unit": 1.0135047181534367,
			"summary_team": 0.8000000000000003
		}]
	},
	"850031": {
		"nik": "850031",
		"nama": "AZLI SYAHPANDINATA",
		"band": "IV",
		"posisi": "MGR ACCESS DATA & INVENTORY MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9277777777777774,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9238665526090678,
			"nilai_komparatif_unit": 0.9303409105902773,
			"summary_team": 0.857142857142857
		}]
	},
	"850032": {
		"nik": "850032",
		"nama": "ARMIKA SYAHPUTRA S.E",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8761904761904761,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9130434782608696,
			"nilai_komparatif_unit": 0.9194419892947148,
			"summary_team": 0.7999999999999999
		}]
	},
	"850036": {
		"nik": "850036",
		"nama": "FADLAN AZMI",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.864102564102563,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9754133107248845,
			"nilai_komparatif_unit": 0.9822489028733773,
			"summary_team": 0.8428571428571427
		}]
	},
	"850040": {
		"nik": "850040",
		"nama": "RENAL FRANATA",
		"band": "IV",
		"posisi": "ASMAN DATA CENTER & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.842857142857143,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9491525423728812,
			"nilai_komparatif_unit": 0.955804101865692,
			"summary_team": 0.7999999999999999
		}]
	},
	"850070": {
		"nik": "850070",
		"nama": "WULAN EKA DALU",
		"band": "IV",
		"posisi": "OFF 1 BIDDING & OFFERING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200683,
			"nilai_komparatif_unit": 1.057099194749032,
			"summary_team": 0.9272727272727272
		}]
	},
	"850094": {
		"nik": "850094",
		"nama": "SANDRA FITRI ASTRINI, MM",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8923076923076925,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0160919540229882,
			"nilai_komparatif_unit": 1.0232126177526073,
			"summary_team": 0.9066666666666666
		}]
	},
	"850104": {
		"nik": "850104",
		"nama": "RUTH FHIDOLA SILALAHI",
		"band": "IV",
		"posisi": "OFF 1 OUTBOND LOGISTIC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9533333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0489510489510487,
			"nilai_komparatif_unit": 1.0563019857032185,
			"summary_team": 0.9999999999999998
		}]
	},
	"850178": {
		"nik": "850178",
		"nama": "WESTI NORIA FURI",
		"band": "IV",
		"posisi": "OFF 1 COMMAND CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8631578947368416,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8440766550522651,
			"nilai_komparatif_unit": 0.849991853965958,
			"summary_team": 0.7285714285714284
		}]
	},
	"860001": {
		"nik": "860001",
		"nama": "EVAN WAHYUDI",
		"band": "IV",
		"posisi": "MGR CCAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8304347826086982,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0149588631263993,
			"nilai_komparatif_unit": 1.0220715862762138,
			"summary_team": 0.8428571428571427
		}]
	},
	"860003": {
		"nik": "860003",
		"nama": "TIAS EKO PURWANTO",
		"band": "IV",
		"posisi": "ASMAN OM TRANSPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8600000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9302325581395345,
			"nilai_komparatif_unit": 0.936751528406575,
			"summary_team": 0.7999999999999999
		}]
	},
	"860004": {
		"nik": "860004",
		"nama": "ABDUL RAHMAN",
		"band": "IV",
		"posisi": "ASMAN CONSTRUCTION SUPERVISION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8140350877192981,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9476600985221676,
			"nilai_komparatif_unit": 0.9543011991281087,
			"summary_team": 0.7714285714285714
		}]
	},
	"860006": {
		"nik": "860006",
		"nama": "ARNOLD REINHARD SITOMPUL",
		"band": "IV",
		"posisi": "ASMAN OM IP NETWORK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8600000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9468438538205977,
			"nilai_komparatif_unit": 0.9534792342709781,
			"summary_team": 0.8142857142857142
		}]
	},
	"860013": {
		"nik": "860013",
		"nama": "PARLINDUNGAN NASUTION",
		"band": "IV",
		"posisi": "ASMAN LOGIC NE ACCESS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000004,
			"nilai_komparatif_unit": 1.007007893037069,
			"summary_team": 0.8000000000000003
		}]
	},
	"860019": {
		"nik": "860019",
		"nama": "MUHAMMAD REZA",
		"band": "IV",
		"posisi": "ASMAN SURVEILLANCE & LOGIC NE ACCESS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.864102564102563,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0250105977108956,
			"nilai_komparatif_unit": 1.0321937623415152,
			"summary_team": 0.8857142857142856
		}]
	},
	"860020": {
		"nik": "860020",
		"nama": "RUNSON BINSAR TARIDA MANALU",
		"band": "IV",
		"posisi": "ASMAN OM TRANSPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8518518518518521,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.955900621118012,
			"nilai_komparatif_unit": 0.9625994704248744,
			"summary_team": 0.8142857142857142
		}]
	},
	"860022": {
		"nik": "860022",
		"nama": "DENNY ANDHIKA",
		"band": "IV",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.842857142857143,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9491525423728813,
			"nilai_komparatif_unit": 0.9558041018656921,
			"summary_team": 0.8
		}]
	},
	"860023": {
		"nik": "860023",
		"nama": "HADI KRISTIANTA",
		"band": "IV",
		"posisi": "MGR WIRELESS OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8745098039215687,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9311338885329914,
			"nilai_komparatif_unit": 0.9376591752270202,
			"summary_team": 0.8142857142857142
		}]
	},
	"860025": {
		"nik": "860025",
		"nama": "KURNIA PRASETIA ZEGA",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8405797101449269,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8667487684729068,
			"nilai_komparatif_unit": 0.8728228511323758,
			"summary_team": 0.7285714285714284
		}]
	},
	"860027": {
		"nik": "860027",
		"nama": "SARWONO UTOMO",
		"band": "IV",
		"posisi": "ASMAN CCAN FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8631578947368416,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9731707317073177,
			"nilai_komparatif_unit": 0.9799906081019283,
			"summary_team": 0.8400000000000001
		}]
	},
	"860031": {
		"nik": "860031",
		"nama": "MUHAMMAD ZAKARIA",
		"band": "IV",
		"posisi": "OFF 1 SERVICE DELIVERY & PROJECT MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9600000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9821428571428568,
			"nilai_komparatif_unit": 0.9890256092328348,
			"summary_team": 0.9428571428571426
		}]
	},
	"860032": {
		"nik": "860032",
		"nama": "ANDA ARISTIA KURNIAWAN",
		"band": "IV",
		"posisi": "MGR ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0054945054945053,
			"nilai_komparatif_unit": 1.0125409034383708,
			"summary_team": 0.8714285714285712
		}]
	},
	"860053": {
		"nik": "860053",
		"nama": "SURYANA MAULIDA",
		"band": "IV",
		"posisi": "OFF 1 HR ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8407407407407405,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.049494687742939,
			"nilai_komparatif_unit": 1.0568494342576131,
			"summary_team": 0.8823529411764706
		}]
	},
	"860073": {
		"nik": "860073",
		"nama": "INTAN KURNIA IMASTUTI, S.T, M.M",
		"band": "IV",
		"posisi": "ASMAN BUSINESS OBL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.81159420289855,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9329081632653072,
			"nilai_komparatif_unit": 0.9394458838868786,
			"summary_team": 0.7571428571428573
		}]
	},
	"860076": {
		"nik": "860076",
		"nama": "WINONA ALMIRA",
		"band": "IV",
		"posisi": "MGR PLAN & BUDGET CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7777777777777777,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9000000000000004,
			"nilai_komparatif_unit": 0.906307103733362,
			"summary_team": 0.7000000000000002
		}]
	},
	"860114": {
		"nik": "860114",
		"nama": "DIAN FATRA ANGGITA",
		"band": "IV",
		"posisi": "MGR WHOLESALE ACCESS NETWORK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444436,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0119617224880395,
			"nilai_komparatif_unit": 1.0190534419968433,
			"summary_team": 0.8545454545454546
		}]
	},
	"860124": {
		"nik": "860124",
		"nama": "PARWATA",
		"band": "IV",
		"posisi": "ASMAN BUSINESS SALES ENGINEER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8972222222222216,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.094286518435126,
			"nilai_komparatif_unit": 1.1019551613082255,
			"summary_team": 0.9818181818181819
		}]
	},
	"860129": {
		"nik": "860129",
		"nama": "NATASIA BENITA KINARTO",
		"band": "IV",
		"posisi": "OFF 1 SMALL & MICRO CUSTOMER MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.019607843137255,
			"nilai_komparatif_unit": 1.0267531458417172,
			"summary_team": 0.8666666666666668
		}]
	},
	"860156": {
		"nik": "860156",
		"nama": "SITI WIMA ADITYA MAHADINI",
		"band": "IV",
		"posisi": "MGR GOVERNMENT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8476190476190467,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9943820224719111,
			"nilai_komparatif_unit": 1.0013505453233782,
			"summary_team": 0.8428571428571427
		}]
	},
	"880033": {
		"nik": "880033",
		"nama": "JUNI HEZI ROMANSYAH",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8761904761904761,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0790513833992097,
			"nilai_komparatif_unit": 1.0866132600755722,
			"summary_team": 0.9454545454545454
		}]
	},
	"880049": {
		"nik": "880049",
		"nama": "PUTERI ALFARISA",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888888,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9281250000000002,
			"nilai_komparatif_unit": 0.9346292007250294,
			"summary_team": 0.8250000000000002
		}]
	},
	"880055": {
		"nik": "880055",
		"nama": "NOVITA WAHYUNINGTYAS",
		"band": "IV",
		"posisi": "OFF 1 CASH BANK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8874999999999998,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9833546734955186,
			"nilai_komparatif_unit": 0.9902459178648767,
			"summary_team": 0.8727272727272727
		}]
	},
	"880076": {
		"nik": "880076",
		"nama": "ARDYANTO KURNIAWAN",
		"band": "IV",
		"posisi": "ASMAN QUALITY ENHANCEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0153846153846149,
			"nilai_komparatif_unit": 1.0225003221607152,
			"summary_team": 0.88
		}]
	},
	"890018": {
		"nik": "890018",
		"nama": "DYAN FEBRINA KUSUMADEWI",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365353,
			"nilai_komparatif_unit": 0.9949168891755601,
			"summary_team": 0.8727272727272728
		}]
	},
	"890019": {
		"nik": "890019",
		"nama": "INDAKA PRADNYA RITYATAMA",
		"band": "IV",
		"posisi": "OFF 1 WALK IN CHANNEL OPERATION & PARTNE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0140499062750898,
			"summary_team": 0.8727272727272727
		}]
	},
	"890035": {
		"nik": "890035",
		"nama": "ABIDA ZURIKA",
		"band": "IV",
		"posisi": "MGR WHOLESALE & INTERNATIONAL MARKETING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333332,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0085763293310464,
			"nilai_komparatif_unit": 1.0156443243667175,
			"summary_team": 0.8909090909090909
		}]
	},
	"890054": {
		"nik": "890054",
		"nama": "ALBENO SALEH AHMAD",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9555555555555555,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9680232558139538,
			"nilai_komparatif_unit": 0.9748070592480927,
			"summary_team": 0.9250000000000002
		}]
	},
	"890062": {
		"nik": "890062",
		"nama": "ADITYA YONA PRASETYO",
		"band": "IV",
		"posisi": "OFF 1 TELKOMGROUP FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.962566844919786,
			"nilai_komparatif_unit": 0.9693124104100124,
			"summary_team": 0.8181818181818182
		}]
	},
	"890071": {
		"nik": "890071",
		"nama": "SARTIKA",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8222222222222225,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0185810810810807,
			"nilai_komparatif_unit": 1.0257191883468786,
			"summary_team": 0.8375
		}]
	},
	"896266": {
		"nik": "896266",
		"nama": "A. DAMAS PRADIGDO",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER GOVERNMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8292397660818702,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8654276943499554,
			"nilai_komparatif_unit": 0.8714925190632767,
			"summary_team": 0.7176470588235293
		}]
	},
	"900020": {
		"nik": "900020",
		"nama": "RADEN MUHAMMAD RIZAL GHAZALI",
		"band": "IV",
		"posisi": "OFF 1 DIGITAL SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559442,
			"nilai_komparatif_unit": 0.950671787132897,
			"summary_team": 0.8181818181818182
		}]
	},
	"900022": {
		"nik": "900022",
		"nama": "RIZKA SARI MEUTIA",
		"band": "IV",
		"posisi": "ASMAN CRM & LEVERAGING CUSTOMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9075630252100847,
			"nilai_komparatif_unit": 0.9139231298151553,
			"summary_team": 0.7714285714285712
		}]
	},
	"900083": {
		"nik": "900083",
		"nama": "CATHARINA NARISWARI",
		"band": "IV",
		"posisi": "ASMAN SALES OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9466666666666664,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0075839653304444,
			"nilai_komparatif_unit": 1.0146450059853456,
			"summary_team": 0.9538461538461537
		}]
	},
	"900094": {
		"nik": "900094",
		"nama": "SOFIANA WANTI",
		"band": "IV",
		"posisi": "ASMAN ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769232,
			"nilai_komparatif_unit": 0.9295457474188327,
			"summary_team": 0.8000000000000003
		}]
	},
	"900095": {
		"nik": "900095",
		"nama": "GELAR GUNADI PUTRA",
		"band": "IV",
		"posisi": "OFF 1 SECRETARY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9841269841269839,
			"nilai_komparatif_unit": 0.9910236407666386,
			"summary_team": 0.8857142857142856
		}]
	},
	"900097": {
		"nik": "900097",
		"nama": "MITA PUSPASARI",
		"band": "IV",
		"posisi": "JM CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.835294117647058,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1050920910075852,
			"nilai_komparatif_unit": 1.1128364581774768,
			"summary_team": 0.9230769230769231
		}]
	},
	"910005": {
		"nik": "910005",
		"nama": "KAMILATUN NIKMAH",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.81159420289855,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9857142857142867,
			"nilai_komparatif_unit": 0.9926220659936827,
			"summary_team": 0.8
		}]
	},
	"910089": {
		"nik": "910089",
		"nama": "CHAIRUNNISYA DONEVA",
		"band": "IV",
		"posisi": "ASMAN DEPLOYMENT & CAPEX MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.028571428571428,
			"nilai_komparatif_unit": 1.0357795471238413,
			"summary_team": 0.857142857142857
		}]
	},
	"910097": {
		"nik": "910097",
		"nama": "FADILAH RAHMAN",
		"band": "IV",
		"posisi": "ASMAN HOME SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.814814814814815,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9081818181818181,
			"nilai_komparatif_unit": 0.9145462592218467,
			"summary_team": 0.7400000000000001
		}]
	},
	"910101": {
		"nik": "910101",
		"nama": "NAHDATIN HASANAH B. JUMALA",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.871794871794872,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0399999999999996,
			"nilai_komparatif_unit": 1.0472882087585509,
			"summary_team": 0.9066666666666665
		}]
	},
	"910106": {
		"nik": "910106",
		"nama": "FATIMAH PUTRI UJIANTI MUFAZAH AFRIYON",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8000000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.019230769230769,
			"nilai_komparatif_unit": 1.0263734294416273,
			"summary_team": 0.8153846153846154
		}]
	},
	"910117": {
		"nik": "910117",
		"nama": "APRI FAJAR KURNIAWAN",
		"band": "IV",
		"posisi": "OFF 1 TECHNICAL SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999995,
			"nilai_komparatif_unit": 0.9667275773155853,
			"summary_team": 0.7999999999999999
		}]
	},
	"910118": {
		"nik": "910118",
		"nama": "AZRIN AYUB",
		"band": "IV",
		"posisi": "OFF 1 PRICING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8600000000000004,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0132890365448497,
			"nilai_komparatif_unit": 1.0203900577285903,
			"summary_team": 0.8714285714285712
		}]
	},
	"910124": {
		"nik": "910124",
		"nama": "YUSUF ARIFANDI",
		"band": "IV",
		"posisi": "ASMAN DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200683,
			"nilai_komparatif_unit": 1.057099194749032,
			"summary_team": 0.9272727272727272
		}]
	},
	"910126": {
		"nik": "910126",
		"nama": "PRASETYAN NUR FIRDAUS",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444447,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9972299168975067,
			"nilai_komparatif_unit": 1.0042183974884893,
			"summary_team": 0.8421052631578948
		}]
	},
	"910130": {
		"nik": "910130",
		"nama": "RATRI MAYRA MAHERANI",
		"band": "IV",
		"posisi": "ASMAN HOME SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8545454545454549,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9946808510638294,
			"nilai_komparatif_unit": 1.001651468074105,
			"summary_team": 0.8500000000000001
		}]
	},
	"910136": {
		"nik": "910136",
		"nama": "RANDY NOARY",
		"band": "IV",
		"posisi": "ASMAN FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.864102564102563,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9423484527342104,
			"nilai_komparatif_unit": 0.9489523298946188,
			"summary_team": 0.8142857142857142
		}]
	},
	"910141": {
		"nik": "910141",
		"nama": "JULIAN FATHANI",
		"band": "IV",
		"posisi": "ASMAN DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8379084967320288,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.95475819032761,
			"nilai_komparatif_unit": 0.961449033601691,
			"summary_team": 0.7999999999999999
		}]
	},
	"910159": {
		"nik": "910159",
		"nama": "MUHAMMAD RIZKY PRATAMA",
		"band": "IV",
		"posisi": "ASMAN LOGIC NE ACCESS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666664,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139864,
			"nilai_komparatif_unit": 0.9929238665610259,
			"summary_team": 0.8545454545454546
		}]
	},
	"910170": {
		"nik": "910170",
		"nama": "RIYANDHANI",
		"band": "IV",
		"posisi": "ASMAN GES BIDDING MANAGEMENT & OBL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8318181818181802,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9960967993754896,
			"nilai_komparatif_unit": 1.0030773392000794,
			"summary_team": 0.8285714285714284
		}]
	},
	"910171": {
		"nik": "910171",
		"nama": "BELLA ANINDITA",
		"band": "IV",
		"posisi": "ASMAN ADVALJAR OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8133333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0538641686182664,
			"nilai_komparatif_unit": 1.0612495359875422,
			"summary_team": 0.857142857142857
		}]
	},
	"910172": {
		"nik": "910172",
		"nama": "MUHAMMAD ARI RIFKI",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8545454545454549,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.936170212765957,
			"nilai_komparatif_unit": 0.9427307934815106,
			"summary_team": 0.8
		}]
	},
	"910175": {
		"nik": "910175",
		"nama": "SENDRA DWI AGUSTINA",
		"band": "IV",
		"posisi": "OFF 1 VERIFIKASI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7684210526315789,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0410958904109588,
			"nilai_komparatif_unit": 1.0483917790522905,
			"summary_team": 0.7999999999999999
		}]
	},
	"910179": {
		"nik": "910179",
		"nama": "DENI PUTRA",
		"band": "IV",
		"posisi": "OFF 1 COMMAND CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8400000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9013605442176869,
			"nilai_komparatif_unit": 0.9076771824993983,
			"summary_team": 0.757142857142857
		}]
	},
	"910182": {
		"nik": "910182",
		"nama": "NASRAH NASRIFAH",
		"band": "IV",
		"posisi": "OFF 1 WORKFORCE & DEVELOPMENT PLANNING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333329,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9600000000000004,
			"nilai_komparatif_unit": 0.9667275773155862,
			"summary_team": 0.7999999999999999
		}]
	},
	"910198": {
		"nik": "910198",
		"nama": "ORCHID GOBENVY",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK & LEGAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9674099485420238,
			"nilai_komparatif_unit": 0.9741894539844023,
			"summary_team": 0.8545454545454546
		}]
	},
	"910202": {
		"nik": "910202",
		"nama": "LYNDA DASLAM",
		"band": "IV",
		"posisi": "OFF 1 CAPEX MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8400000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0204081632653055,
			"nilai_komparatif_unit": 1.0275590745276202,
			"summary_team": 0.857142857142857
		}]
	},
	"920049": {
		"nik": "920049",
		"nama": "HENI JAYANTI SUTADI",
		"band": "IV",
		"posisi": "ASMAN DEPLOYMENT & CAPEX MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8589743589743583,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9313432835820904,
			"nilai_komparatif_unit": 0.9378700376942258,
			"summary_team": 0.8
		}]
	},
	"920060": {
		"nik": "920060",
		"nama": "ALAN NUSRA",
		"band": "IV",
		"posisi": "ASMAN CAPEX MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7948717948717953,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0064516129032253,
			"nilai_komparatif_unit": 1.0135047181534362,
			"summary_team": 0.7999999999999999
		}]
	},
	"920063": {
		"nik": "920063",
		"nama": "RISKA AMALIA SETYARINI",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8740740740740743,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9457627118644065,
			"nilai_komparatif_unit": 0.9523905157876001,
			"summary_team": 0.8266666666666667
		}]
	},
	"920067": {
		"nik": "920067",
		"nama": "MUHAMMAD AZHARI ZAKIRI",
		"band": "IV",
		"posisi": "ASMAN CCAN ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9828947368421049,
			"nilai_komparatif_unit": 0.989782758024592,
			"summary_team": 0.8736842105263156
		}]
	},
	"920079": {
		"nik": "920079",
		"nama": "DEWI TRESSIA ROITO SARAGIH",
		"band": "IV",
		"posisi": "OFF 1 PRODUCT SOLUTION & DELIVERY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8035087719298245,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9600748596381783,
			"nilai_komparatif_unit": 0.9668029615621012,
			"summary_team": 0.7714285714285712
		}]
	},
	"920099": {
		"nik": "920099",
		"nama": "GRACE KARLINA PERMATASARI BR L TOBING",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.037593984962406,
			"nilai_komparatif_unit": 1.0448653326249282,
			"summary_team": 0.968421052631579
		}]
	},
	"920119": {
		"nik": "920119",
		"nama": "AINUN KHAIRIYAH FADLA",
		"band": "IV",
		"posisi": "JM CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8207407407407394,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9442689530685936,
			"nilai_komparatif_unit": 0.950886288889923,
			"summary_team": 0.775
		}]
	},
	"920121": {
		"nik": "920121",
		"nama": "MUHAMMAD AFNAAN SUBHI",
		"band": "IV",
		"posisi": "ASMAN FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9473684210526313,
			"nilai_komparatif_unit": 0.9540074776140647,
			"summary_team": 0.8
		}]
	},
	"920145": {
		"nik": "920145",
		"nama": "AULIA PUTRI UTAMI",
		"band": "IV",
		"posisi": "OFF 1 VERIFICATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8631578947368416,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1419860627177705,
			"nilai_komparatif_unit": 1.1499889788951196,
			"summary_team": 0.9857142857142855
		}]
	},
	"920159": {
		"nik": "920159",
		"nama": "HILDIA RAHMADANI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8622222222222221,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0438144329896908,
			"nilai_komparatif_unit": 1.0511293728866309,
			"summary_team": 0.8999999999999999
		}]
	},
	"920162": {
		"nik": "920162",
		"nama": "DIANA FITRI ANGGRAINI",
		"band": "IV",
		"posisi": "OFF 1 COMMAND CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7684210526315789,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8923679060665362,
			"nilai_komparatif_unit": 0.8986215249019632,
			"summary_team": 0.6857142857142856
		}]
	},
	"920163": {
		"nik": "920163",
		"nama": "NUR RAHMAWATI",
		"band": "IV",
		"posisi": "ASMAN CRM & LEVERAGING CUSTOMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346935,
			"nilai_komparatif_unit": 0.9864567115465157,
			"summary_team": 0.8
		}]
	},
	"920178": {
		"nik": "920178",
		"nama": "YULIA ARNA SARAGIH",
		"band": "IV",
		"posisi": "OFF 1 CASH BANK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8622222222222221,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.977540500736377,
			"nilai_komparatif_unit": 0.98439100000494,
			"summary_team": 0.8428571428571427
		}]
	},
	"920193": {
		"nik": "920193",
		"nama": "AYU ADINDA PUTRI",
		"band": "IV",
		"posisi": "ASMAN ACCESS PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.041509433962264,
			"nilai_komparatif_unit": 1.0488082206725693,
			"summary_team": 0.9200000000000002
		}]
	},
	"920212": {
		"nik": "920212",
		"nama": "FEMIENOV HAGIA RAHIMADINDA",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0129870129870129,
			"nilai_komparatif_unit": 1.0200859176219654,
			"summary_team": 0.9454545454545454
		}]
	},
	"920216": {
		"nik": "920216",
		"nama": "M. TAUFIK HIDAYAT",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8488888888888891,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.959237097980553,
			"nilai_komparatif_unit": 0.9659593289603887,
			"summary_team": 0.8142857142857142
		}]
	},
	"920275": {
		"nik": "920275",
		"nama": "MUHAMAD MULYA FUADI AGISNA",
		"band": "IV",
		"posisi": "OFF 1 ACCESS CAPEX QE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9677419354838709,
			"nilai_komparatif_unit": 0.9745237674552275,
			"summary_team": 0.8
		}]
	},
	"925919": {
		"nik": "925919",
		"nama": "MUHAMMAD RIZAL GHUROBI",
		"band": "IV",
		"posisi": "JUNIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8320512820512806,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7776669990029922,
			"nilai_komparatif_unit": 0.7831168061504632,
			"summary_team": 0.6470588235294117
		}]
	},
	"930005": {
		"nik": "930005",
		"nama": "HERBRIAWAN WIRATAMA DWI WARDHANA",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9466666666666664,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9263271939328279,
			"nilai_komparatif_unit": 0.932818795825237,
			"summary_team": 0.8769230769230769
		}]
	},
	"930043": {
		"nik": "930043",
		"nama": "FEBY KARUNIA",
		"band": "IV",
		"posisi": "ASMAN BGES SALES ENGINEER, BIDDING & PM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8410256410256403,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9512195121951229,
			"nilai_komparatif_unit": 0.9578855567913588,
			"summary_team": 0.8
		}]
	},
	"930044": {
		"nik": "930044",
		"nama": "FAJRIN REZA ANSHARI",
		"band": "IV",
		"posisi": "ASMAN GES BIDDING & PROJECT MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8923076923076925,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0391849529780561,
			"nilai_komparatif_unit": 1.0464674499742574,
			"summary_team": 0.9272727272727272
		}]
	},
	"930045": {
		"nik": "930045",
		"nama": "TERRY SAFIRIA RAMADHANI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8765432098765426,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267605633802823,
			"nilai_komparatif_unit": 1.0339559915831316,
			"summary_team": 0.8999999999999999
		}]
	},
	"930070": {
		"nik": "930070",
		"nama": "MEUTIA PUSPITASARI",
		"band": "IV",
		"posisi": "ASMAN DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8693333333333324,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.969544259421561,
			"nilai_komparatif_unit": 0.9763387218862911,
			"summary_team": 0.8428571428571427
		}]
	},
	"940023": {
		"nik": "940023",
		"nama": "BIMA PUTRA ARI  WIJAYA",
		"band": "IV",
		"posisi": "OFF 1 BUSINESS LEGAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0291595197255572,
			"nilai_komparatif_unit": 1.0363717595578747,
			"summary_team": 0.9090909090909092
		}]
	},
	"940025": {
		"nik": "940025",
		"nama": "MAYA PREVIANA SYAFITRI",
		"band": "IV",
		"posisi": "OFF 1 OLO FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0634328358208953,
			"nilai_komparatif_unit": 1.0708852593864346,
			"summary_team": 0.95
		}]
	},
	"940048": {
		"nik": "940048",
		"nama": "ADITYA KURNIAWAN, S.T, M.M",
		"band": "IV",
		"posisi": "ASMAN ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8370370370370374,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.003539823008849,
			"nilai_komparatif_unit": 1.0105725227469338,
			"summary_team": 0.84
		}]
	}
};