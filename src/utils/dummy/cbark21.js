export const cbark21 = {
	"651575": {
		"nik": "651575",
		"nama": "OKTOVIANUS MAKALPESSY",
		"band": "V",
		"posisi": "OFF 2 ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7111111111111111,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.94921875,
			"nilai_komparatif_unit": 0.9497953912472977,
			"summary_team": 0.675
		}]
	},
	"660157": {
		"nik": "660157",
		"nama": "FATHOR RAHMAN",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8460784313725501,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8568945538818067,
			"nilai_komparatif_unit": 0.8574151090692732,
			"summary_team": 0.7250000000000001
		}]
	},
	"660525": {
		"nik": "660525",
		"nama": "BLESY JULIANA",
		"band": "V",
		"posisi": "OFF 2 DATA CUSTOMER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333337,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999996,
			"nilai_komparatif_unit": 0.9605831907528222,
			"summary_team": 0.8
		}]
	},
	"660576": {
		"nik": "660576",
		"nama": "IDA BAGUS NYOMAN SUPARTA",
		"band": "V",
		"posisi": "OFF 2 FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7555555555555555,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9595588235294119,
			"nilai_komparatif_unit": 0.9601417462717783,
			"summary_team": 0.7250000000000001
		}]
	},
	"660595": {
		"nik": "660595",
		"nama": "RISNO JANI",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8863636363636361,
			"nilai_komparatif_unit": 0.8869020937348502,
			"summary_team": 0.7090909090909091
		}]
	},
	"670539": {
		"nik": "670539",
		"nama": "KASWONO",
		"band": "V",
		"posisi": "OFF 2 COLLECTION PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0085763293310464,
			"nilai_komparatif_unit": 1.0091890297360273,
			"summary_team": 0.8909090909090909
		}]
	},
	"690050": {
		"nik": "690050",
		"nama": "ARDIANSYAH",
		"band": "V",
		"posisi": "OFF 2 SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7500000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.751515151515151,
			"nilai_komparatif_unit": 0.7519716897307445,
			"summary_team": 0.5636363636363636
		}]
	},
	"730107": {
		"nik": "730107",
		"nama": "IMAM ROFI`I",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ - INNER AREA KD",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9318181818181815,
			"nilai_komparatif_unit": 0.9323842523879193,
			"summary_team": 0.7454545454545455
		}]
	},
	"750041": {
		"nik": "750041",
		"nama": "YOHANIS DOJA SAY",
		"band": "V",
		"posisi": "OFF 2 NETWORK PERF , QM & ADMIN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000004,
			"nilai_komparatif_unit": 1.000607490367524,
			"summary_team": 0.8000000000000003
		}]
	},
	"760025": {
		"nik": "760025",
		"nama": "MELKY SALMON AIBOY",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7575757575757579,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8139999999999995,
			"nilai_komparatif_unit": 0.8144944971591637,
			"summary_team": 0.6166666666666666
		}]
	},
	"780019": {
		"nik": "780019",
		"nama": "NELDA LASTRI EKA DEWI SIAHAYA",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8722222222222226,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.979733642154024,
			"nilai_komparatif_unit": 0.9803288209043713,
			"summary_team": 0.8545454545454546
		}]
	},
	"805934": {
		"nik": "805934",
		"nama": "ELDO HERMANSYAH",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666662,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.08288770053476,
			"nilai_komparatif_unit": 1.0835455443819444,
			"summary_team": 0.9818181818181818
		}]
	},
	"846065": {
		"nik": "846065",
		"nama": "M. RIDUAN",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8142857142857147,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0175438596491222,
			"nilai_komparatif_unit": 1.0181620077423919,
			"summary_team": 0.8285714285714285
		}]
	},
	"850058": {
		"nik": "850058",
		"nama": "MAYA SUSANA",
		"band": "V",
		"posisi": "OFF 2 SALES NUNUKAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8974358974358976,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.033246753246753,
			"nilai_komparatif_unit": 1.0338744406966254,
			"summary_team": 0.9272727272727272
		}]
	},
	"855982": {
		"nik": "855982",
		"nama": "SAHARTHICA MAYA INDAH",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9375000000000002,
			"nilai_komparatif_unit": 0.9380695222195535,
			"summary_team": 0.8
		}]
	},
	"860041": {
		"nik": "860041",
		"nama": "MUKHLIS",
		"band": "V",
		"posisi": "OFF 2 HR ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7866666666666663,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Down Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.6240369799691836,
			"nilai_komparatif_unit": 0.6244160764234934,
			"summary_team": 0.49090909090909085
		}]
	},
	"866053": {
		"nik": "866053",
		"nama": "HERLIA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8643678160919535,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0208072590738428,
			"nilai_komparatif_unit": 1.0214273896508284,
			"summary_team": 0.8823529411764706
		}]
	},
	"876936": {
		"nik": "876936",
		"nama": "ISMET MUHAMMAD RAHMAT",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8395833333333326,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0611324159711266,
			"nilai_komparatif_unit": 1.061777043692496,
			"summary_team": 0.8909090909090909
		}]
	},
	"877033": {
		"nik": "877033",
		"nama": "DENNY HERMAWAN",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999996,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9772727272727276,
			"nilai_komparatif_unit": 0.9778664110409893,
			"summary_team": 0.7818181818181817
		}]
	},
	"877034": {
		"nik": "877034",
		"nama": "IRA RAHMANIAH NUR'IZZATI",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8787878787878791,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0758620689655167,
			"nilai_komparatif_unit": 1.0765156448091973,
			"summary_team": 0.9454545454545454
		}]
	},
	"880093": {
		"nik": "880093",
		"nama": "AHMAD SYUKRI",
		"band": "V",
		"posisi": "OFF 2 WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0076047455449186,
			"summary_team": 0.8727272727272727
		}]
	},
	"896417": {
		"nik": "896417",
		"nama": "FAHD IBNU AKBAR",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8299999999999994,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0327022375215151,
			"nilai_komparatif_unit": 1.0333295941833294,
			"summary_team": 0.857142857142857
		}]
	},
	"900117": {
		"nik": "900117",
		"nama": "ASRIADI",
		"band": "V",
		"posisi": "OFF 2 PROMO PLAN & EXECUTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9500000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9824561403508768,
			"nilai_komparatif_unit": 0.9830529729926543,
			"summary_team": 0.9333333333333331
		}]
	},
	"900118": {
		"nik": "900118",
		"nama": "ARUM SHADEWI ITTAQA",
		"band": "V",
		"posisi": "OFF 2 IP OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9801869293396148,
			"summary_team": 0.8
		}]
	},
	"900122": {
		"nik": "900122",
		"nama": "YUNITA SARI NENDHYA SUSANTI",
		"band": "V",
		"posisi": "OFF 2 COMMAND CONTROL MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9072907290729071,
			"nilai_komparatif_unit": 0.9078418994513623,
			"summary_team": 0.7636363636363637
		}]
	},
	"900138": {
		"nik": "900138",
		"nama": "FICKY SUPIT SANTOSO",
		"band": "V",
		"posisi": "OFF 2 SMALL & MICRO CUSTOMER MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.88,
			"nilai_komparatif_unit": 0.8805345915234207,
			"summary_team": 0.7333333333333334
		}]
	},
	"906522": {
		"nik": "906522",
		"nama": "DEWA NYOMAN REDI SURYA ADITYA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8277777777777782,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9664429530201338,
			"nilai_komparatif_unit": 0.9670300578048545,
			"summary_team": 0.8
		}]
	},
	"906527": {
		"nik": "906527",
		"nama": "WULANDARI NAHAS",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8547008547008538,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8934545454545465,
			"nilai_komparatif_unit": 0.8939973104847303,
			"summary_team": 0.7636363636363637
		}]
	},
	"910138": {
		"nik": "910138",
		"nama": "ICNASIUS LOYOLA ARRUAN PATANDEAN",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8222222222222221,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9729729729729729,
			"nilai_komparatif_unit": 0.9735640446819147,
			"summary_team": 0.7999999999999998
		}]
	},
	"910165": {
		"nik": "910165",
		"nama": "SYAIFUL SADDAM",
		"band": "V",
		"posisi": "OFF 2 SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969697,
			"nilai_komparatif_unit": 0.9702860512654774,
			"summary_team": 0.8727272727272728
		}]
	},
	"910169": {
		"nik": "910169",
		"nama": "AFIAT UTAMA ASHAR",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8844444444444443,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9559159433531296,
			"nilai_komparatif_unit": 0.9564966530808787,
			"summary_team": 0.8454545454545456
		}]
	},
	"910177": {
		"nik": "910177",
		"nama": "TIURMA LAOSMA AGUSTINA PURBA",
		"band": "V",
		"posisi": "OFF 2 CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9499999999999998,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9952153110047849,
			"nilai_komparatif_unit": 0.9958198947198322,
			"summary_team": 0.9454545454545454
		}]
	},
	"910204": {
		"nik": "910204",
		"nama": "SUSFIKA KHAIRUSSYIFA NABILA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.81159420289855,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0033163265306129,
			"nilai_komparatif_unit": 1.0039258315345594,
			"summary_team": 0.8142857142857141
		}]
	},
	"916315": {
		"nik": "916315",
		"nama": "RIZKY ANDAWASATYA RACHMAN",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.869135802469135,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0190746753246762,
			"nilai_komparatif_unit": 1.0196937533737231,
			"summary_team": 0.8857142857142857
		}]
	},
	"916317": {
		"nik": "916317",
		"nama": "AGUS IRIANTO",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8294117647058812,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.021276595744682,
			"nilai_komparatif_unit": 1.0218970114391743,
			"summary_team": 0.8470588235294116
		}]
	},
	"916444": {
		"nik": "916444",
		"nama": "ANDRI FEBRIANSYAH",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333328,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1718750000000007,
			"nilai_komparatif_unit": 1.1725869027744422,
			"summary_team": 1
		}]
	},
	"916446": {
		"nik": "916446",
		"nama": "SURYA HANDOKO",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777779,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9113924050632913,
			"nilai_komparatif_unit": 0.9119460671704014,
			"summary_team": 0.8000000000000003
		}]
	},
	"920075": {
		"nik": "920075",
		"nama": "INES DWI ANDINI",
		"band": "V",
		"posisi": "OFF 2 APARTMENT OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090908,
			"nilai_komparatif_unit": 1.091571807673662,
			"summary_team": 0.9090909090909092
		}]
	},
	"920112": {
		"nik": "920112",
		"nama": "VIDHITA PARAMA ISVARI",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8545454545454549,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1702127659574466,
			"nilai_komparatif_unit": 1.1709236589407188,
			"summary_team": 1.0000000000000002
		}]
	},
	"920153": {
		"nik": "920153",
		"nama": "ADITYA AMIRULLAH",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.75,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1,
			"nilai_komparatif_unit": 1.0006074903675235,
			"summary_team": 0.75
		}]
	},
	"920165": {
		"nik": "920165",
		"nama": "TENRIPADA AULIA",
		"band": "V",
		"posisi": "OFF 2 SOLUTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882352,
			"nilai_komparatif_unit": 0.9417482262282573,
			"summary_team": 0.8
		}]
	},
	"920183": {
		"nik": "920183",
		"nama": "LENA SABRINA",
		"band": "V",
		"posisi": "OFF 2 PUBLIC RELATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8912280701754386,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.979241231209735,
			"nilai_komparatif_unit": 0.9798361108251769,
			"summary_team": 0.8727272727272727
		}]
	},
	"920185": {
		"nik": "920185",
		"nama": "YULIANTI MURPRAYANA",
		"band": "V",
		"posisi": "OFF 2 ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8361904761904755,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9132325533236703,
			"nilai_komparatif_unit": 0.9137873333031233,
			"summary_team": 0.7636363636363637
		}]
	},
	"920189": {
		"nik": "920189",
		"nama": "GHILMAN HAFIZHAN",
		"band": "V",
		"posisi": "OFF 2 INTEGRATION SYSTEM SITE ADVALJAR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999997,
			"nilai_komparatif_unit": 0.9605831907528224,
			"summary_team": 0.8
		}]
	},
	"920221": {
		"nik": "920221",
		"nama": "DYAH NORMA MAHARSI",
		"band": "V",
		"posisi": "OFF 2 ADVALJAR OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888888,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0250000000000001,
			"nilai_komparatif_unit": 1.0256226776267117,
			"summary_team": 0.9111111111111112
		}]
	},
	"920232": {
		"nik": "920232",
		"nama": "AMELIA PUSPASARI",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.848148148148148,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0932909884874953,
			"nilai_komparatif_unit": 1.0939551522319018,
			"summary_team": 0.9272727272727272
		}]
	},
	"920237": {
		"nik": "920237",
		"nama": "DIEFA AGUNG RIYADI",
		"band": "V",
		"posisi": "OFF 2 MICRO DEMAND",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8222222222222222,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.972972972972973,
			"nilai_komparatif_unit": 0.9735640446819148,
			"summary_team": 0.8
		}]
	},
	"920242": {
		"nik": "920242",
		"nama": "HAFIDZ MUSLIM",
		"band": "V",
		"posisi": "OFF 2 SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999996,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000004,
			"nilai_komparatif_unit": 1.000607490367524,
			"summary_team": 0.8
		}]
	},
	"920247": {
		"nik": "920247",
		"nama": "PRASTIYANTO",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139859,
			"nilai_komparatif_unit": 0.9866129800127329,
			"summary_team": 0.8545454545454546
		}]
	},
	"920251": {
		"nik": "920251",
		"nama": "LAURENTIUS ADITYA LASIKA RAHARJA",
		"band": "V",
		"posisi": "OFF 2 ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.01010101010101,
			"nilai_komparatif_unit": 1.010714636734872,
			"summary_team": 0.9090909090909092
		}]
	},
	"920264": {
		"nik": "920264",
		"nama": "RIYAN RISWANDI",
		"band": "V",
		"posisi": "OFF 2 PUBLIC RELATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8138888888888879,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.982935153583619,
			"nilai_komparatif_unit": 0.9835322772213214,
			"summary_team": 0.8
		}]
	},
	"920266": {
		"nik": "920266",
		"nama": "KHARISMA WIDI UTAMI",
		"band": "V",
		"posisi": "OFF 2 BATTLE MANAGEMENT EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0369036903690365,
			"nilai_komparatif_unit": 1.0375335993729853,
			"summary_team": 0.8727272727272727
		}]
	},
	"920268": {
		"nik": "920268",
		"nama": "NATANAEL PANDAPOTAN",
		"band": "V",
		"posisi": "OFF 2 OM TRANSPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8549019607843138,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9570475396163469,
			"nilai_komparatif_unit": 0.9576289367779259,
			"summary_team": 0.8181818181818182
		}]
	},
	"920277": {
		"nik": "920277",
		"nama": "ANITA RAHMAWATI",
		"band": "V",
		"posisi": "OFF 2 INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0059583325620023,
			"summary_team": 0.8545454545454546
		}]
	},
	"920281": {
		"nik": "920281",
		"nama": "RIZKY AKBAR",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333337,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9320388349514559,
			"nilai_komparatif_unit": 0.9326050395658467,
			"summary_team": 0.8
		}]
	},
	"920284": {
		"nik": "920284",
		"nama": "CHRISTOPHER WIMBA MANGGALA",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8539682539682542,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0432578573842513,
			"nilai_komparatif_unit": 1.0438916264834555,
			"summary_team": 0.8909090909090909
		}]
	},
	"920285": {
		"nik": "920285",
		"nama": "AHADYN ALIEF ARLINGGA",
		"band": "V",
		"posisi": "OFF 2 ACCESS PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9955869373345099,
			"nilai_komparatif_unit": 0.9961917468089728,
			"summary_team": 0.8545454545454545
		}]
	},
	"920299": {
		"nik": "920299",
		"nama": "KIRIE ELEISON BETTY",
		"band": "V",
		"posisi": "OFF 2 BIDDING MANAGEMENT & OBL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8323232323232315,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1140776699029138,
			"nilai_komparatif_unit": 1.114754461356053,
			"summary_team": 0.9272727272727272
		}]
	},
	"920301": {
		"nik": "920301",
		"nama": "DEFRY FARA DILA SANDY",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8791666666666665,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9926755708746232,
			"nilai_komparatif_unit": 0.9932786117220055,
			"summary_team": 0.8727272727272728
		}]
	},
	"920335": {
		"nik": "920335",
		"nama": "I PUTU ADI SAYOGA",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.027361701339917,
			"summary_team": 0.8727272727272727
		}]
	},
	"920336": {
		"nik": "920336",
		"nama": "KURNIAN FEBRIARTO",
		"band": "V",
		"posisi": "SPV AREA SANGGAU",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8592592592592596,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9746767241379305,
			"nilai_komparatif_unit": 0.9752688308592937,
			"summary_team": 0.8374999999999999
		}]
	},
	"920338": {
		"nik": "920338",
		"nama": "DONI WAHYU PRASETYO",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769228,
			"nilai_komparatif_unit": 0.9236376834161752,
			"summary_team": 0.8
		}]
	},
	"920347": {
		"nik": "920347",
		"nama": "FAMYTYAS",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9142857142857144,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.01171875,
			"nilai_komparatif_unit": 1.012333359395268,
			"summary_team": 0.9250000000000002
		}]
	},
	"926127": {
		"nik": "926127",
		"nama": "MEILISSA PRATIWI HARTONO",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999995,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625000000000007,
			"nilai_komparatif_unit": 0.9630847094787421,
			"summary_team": 0.77
		}]
	},
	"926128": {
		"nik": "926128",
		"nama": "ANDRI GUNAWAN",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9375000000000002,
			"nilai_komparatif_unit": 0.9380695222195535,
			"summary_team": 0.8
		}]
	},
	"928690": {
		"nik": "928690",
		"nama": "FARID AFYUDIN",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999996,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.910714285714286,
			"nilai_komparatif_unit": 0.9112675358704235,
			"summary_team": 0.7285714285714284
		}]
	},
	"930030": {
		"nik": "930030",
		"nama": "FAZRI SATRIA DHARMAWANPUTRA",
		"band": "V",
		"posisi": "OFF 2 CAPEX MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0196078431372546,
			"nilai_komparatif_unit": 1.020227245080612,
			"summary_team": 0.8666666666666665
		}]
	},
	"930037": {
		"nik": "930037",
		"nama": "BETHARY AJENG PRAMANANDI",
		"band": "V",
		"posisi": "OFF 2 DIGITAL SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8125,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9174825174825174,
			"nilai_komparatif_unit": 0.9180398792742592,
			"summary_team": 0.7454545454545454
		}]
	},
	"930039": {
		"nik": "930039",
		"nama": "RISANG ARONO",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0208507089241032,
			"nilai_komparatif_unit": 1.0214708658964542,
			"summary_team": 0.9272727272727274
		}]
	},
	"930050": {
		"nik": "930050",
		"nama": "TRI ANUGRAH RAHMADANI",
		"band": "V",
		"posisi": "OFF 2 INFRA & SERVICE SURVEILANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0044642857142854,
			"nilai_komparatif_unit": 1.005074488092378,
			"summary_team": 0.8571428571428571
		}]
	},
	"930058": {
		"nik": "930058",
		"nama": "FITRIA AYU NURDIANA",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8703703703703703,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0235976789168277,
			"nilai_komparatif_unit": 1.024219504646989,
			"summary_team": 0.8909090909090909
		}]
	},
	"930059": {
		"nik": "930059",
		"nama": "ERWIN GINANJAR KUSUMADIREDJA",
		"band": "V",
		"posisi": "OFF 2 QUALITY & PROJECT MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8791666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9926755708746228,
			"nilai_komparatif_unit": 0.993278611722005,
			"summary_team": 0.8727272727272727
		}]
	},
	"930061": {
		"nik": "930061",
		"nama": "RISTE ISABELLA",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8125,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006993006993007,
			"nilai_komparatif_unit": 1.0076047455449189,
			"summary_team": 0.8181818181818182
		}]
	},
	"930064": {
		"nik": "930064",
		"nama": "RATIH TRYAS PRODONINGRUM",
		"band": "V",
		"posisi": "OFF 2 OBL & BIDDING MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0192307692307692,
			"nilai_komparatif_unit": 1.0198499421053604,
			"summary_team": 0.8833333333333333
		}]
	},
	"930075": {
		"nik": "930075",
		"nama": "NUR ANNILA",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.0006074903675233,
			"summary_team": 0.8
		}]
	},
	"930104": {
		"nik": "930104",
		"nama": "SEPTIAN ADI NUGRAHA",
		"band": "V",
		"posisi": "OFF 2 SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8277777777777782,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0323367907260521,
			"nilai_komparatif_unit": 1.0329639253824583,
			"summary_team": 0.8545454545454546
		}]
	},
	"930109": {
		"nik": "930109",
		"nama": "ANDREW ISKANDAR ZULKARNAIN",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0059583325620023,
			"summary_team": 0.8545454545454546
		}]
	},
	"930122": {
		"nik": "930122",
		"nama": "NITA ANDRIYATI",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8909090909090912,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.020408163265306,
			"nilai_komparatif_unit": 1.021028051395432,
			"summary_team": 0.9090909090909092
		}]
	},
	"930149": {
		"nik": "930149",
		"nama": "ADHI ANDRIYANTO",
		"band": "V",
		"posisi": "OFF 2 REVAS & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0599999999999998,
			"nilai_komparatif_unit": 1.0606439397895748,
			"summary_team": 0.8833333333333335
		}]
	},
	"930151": {
		"nik": "930151",
		"nama": "WIDYASARI AMBAR UTAMI",
		"band": "V",
		"posisi": "OFF 2 DESIGN & ENGINEERING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882353,
			"nilai_komparatif_unit": 0.9417482262282574,
			"summary_team": 0.8
		}]
	},
	"930158": {
		"nik": "930158",
		"nama": "FAHRY ADNANTYA",
		"band": "V",
		"posisi": "OFF 2 ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.877777777777778,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9942462600690446,
			"nilai_komparatif_unit": 0.9948502550949828,
			"summary_team": 0.8727272727272727
		}]
	},
	"930160": {
		"nik": "930160",
		"nama": "TIMOTIUS FLOREAN",
		"band": "V",
		"posisi": "OFF 2 FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9374999999999999,
			"nilai_komparatif_unit": 0.9380695222195532,
			"summary_team": 0.8125
		}]
	},
	"930166": {
		"nik": "930166",
		"nama": "FADHILA ADZKIA Z",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8222222222222222,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.972972972972973,
			"nilai_komparatif_unit": 0.9735640446819148,
			"summary_team": 0.8
		}]
	},
	"930167": {
		"nik": "930167",
		"nama": "AMARULLAH MUHAMMAD",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8250000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969695,
			"nilai_komparatif_unit": 0.9702860512654772,
			"summary_team": 0.7999999999999999
		}]
	},
	"930169": {
		"nik": "930169",
		"nama": "IRYANI",
		"band": "V",
		"posisi": "OFF 2 SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8388888888888891,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.105358217940999,
			"nilai_komparatif_unit": 1.1060297124110612,
			"summary_team": 0.9272727272727274
		}]
	},
	"930181": {
		"nik": "930181",
		"nama": "REAULIA NADINE RACHMAT",
		"band": "V",
		"posisi": "OFF 2 ACCESS CAPEX QE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.974025974025974,
			"nilai_komparatif_unit": 0.9746176854229125,
			"summary_team": 0.9090909090909092
		}]
	},
	"930182": {
		"nik": "930182",
		"nama": "ANITA PRATIWI",
		"band": "V",
		"posisi": "OFF 2 HELPDESK OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181818,
			"nilai_komparatif_unit": 0.9824146269062958,
			"summary_team": 0.8181818181818182
		}]
	},
	"930183": {
		"nik": "930183",
		"nama": "WICAKSONO INDRA RADITO",
		"band": "V",
		"posisi": "OFF 2 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999994,
			"nilai_komparatif_unit": 1.000607490367523,
			"summary_team": 0.8666666666666665
		}]
	},
	"930209": {
		"nik": "930209",
		"nama": "ADI CIPTA ERLANGGA",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.005,
			"nilai_komparatif_unit": 1.005610527819361,
			"summary_team": 0.8375
		}]
	},
	"930218": {
		"nik": "930218",
		"nama": "RILLO SATRIA WAHYUDIN",
		"band": "V",
		"posisi": "OFF 2 OUTBOUND CHANNEL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9808173477898245,
			"nilai_komparatif_unit": 0.9814131848809069,
			"summary_team": 0.8909090909090909
		}]
	},
	"930222": {
		"nik": "930222",
		"nama": "RIDWAN PRAHADI HIDAYAT",
		"band": "V",
		"posisi": "OFF 2 DEBT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8380952380952383,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.954545454545454,
			"nilai_komparatif_unit": 0.9551253317144538,
			"summary_team": 0.7999999999999998
		}]
	},
	"930227": {
		"nik": "930227",
		"nama": "WHILDAN PAKARTIPANGI",
		"band": "V",
		"posisi": "OFF 2 CONSTRUCTION SUPERVISION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8458333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8598298253470668,
			"nilai_komparatif_unit": 0.8603521636836745,
			"summary_team": 0.7272727272727273
		}]
	},
	"930228": {
		"nik": "930228",
		"nama": "MUHARAM GINANJAR JATNIKA",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9504950495049503,
			"nilai_komparatif_unit": 0.9510724660919034,
			"summary_team": 0.8
		}]
	},
	"930231": {
		"nik": "930231",
		"nama": "RIZKY PRADITYA",
		"band": "V",
		"posisi": "OFF 2 ACCESS PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0076047455449186,
			"summary_team": 0.8727272727272727
		}]
	},
	"930245": {
		"nik": "930245",
		"nama": "MUHAMMAD NAIRONUL HAQ",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8905380333951761,
			"nilai_komparatif_unit": 0.891079026672377,
			"summary_team": 0.7272727272727273
		}]
	},
	"930248": {
		"nik": "930248",
		"nama": "JAYNER EVANGGELINI WENNYI",
		"band": "V",
		"posisi": "OFF 2 ACCESS PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8791666666666665,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0340370529943992,
			"nilai_komparatif_unit": 1.0346652205437556,
			"summary_team": 0.9090909090909092
		}]
	},
	"930250": {
		"nik": "930250",
		"nama": "PASCALINA RAKHMASARI PUTRI",
		"band": "V",
		"posisi": "OFF 2 DESIGN & ENGINEERING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8740740740740743,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9762711864406778,
			"nilai_komparatif_unit": 0.9768642617825313,
			"summary_team": 0.8533333333333334
		}]
	},
	"930255": {
		"nik": "930255",
		"nama": "YONATAN CHONDRO NUGROHO",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ - AREA PACITAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0181818181818179,
			"nilai_komparatif_unit": 1.018800353828751,
			"summary_team": 0.8909090909090909
		}]
	},
	"930258": {
		"nik": "930258",
		"nama": "ESRIT DORINA NENABU",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9592476489028214,
			"nilai_komparatif_unit": 0.9598303826095995,
			"summary_team": 0.9272727272727274
		}]
	},
	"930261": {
		"nik": "930261",
		"nama": "AKHMAD RAHMADI",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8250000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969694,
			"nilai_komparatif_unit": 0.9702860512654771,
			"summary_team": 0.8
		}]
	},
	"930262": {
		"nik": "930262",
		"nama": "FITRIYADI SYAFWAN",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300698,
			"nilai_komparatif_unit": 1.0705800421414762,
			"summary_team": 0.9272727272727274
		}]
	},
	"930269": {
		"nik": "930269",
		"nama": "SAWUNG AJI WICAKSONO",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8592592592592596,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0183189655172413,
			"nilai_komparatif_unit": 1.0189375844798596,
			"summary_team": 0.8750000000000002
		}]
	},
	"930270": {
		"nik": "930270",
		"nama": "MARINA BELMINCE MASOSENDIFU",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0006074903675237,
			"summary_team": 0.8
		}]
	},
	"930271": {
		"nik": "930271",
		"nama": "JORDY GERRY REZANDY",
		"band": "V",
		"posisi": "OFF 2 LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8404040404040394,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1219093406593417,
			"nilai_komparatif_unit": 1.122590889777027,
			"summary_team": 0.9428571428571426
		}]
	},
	"930277": {
		"nik": "930277",
		"nama": "ABDURRAHMAN",
		"band": "V",
		"posisi": "OFF 2 ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969696,
			"nilai_komparatif_unit": 0.9702860512654773,
			"summary_team": 0.8727272727272727
		}]
	},
	"930287": {
		"nik": "930287",
		"nama": "GIOVANI VERANINSKY",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666662,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0427807486631022,
			"nilai_komparatif_unit": 1.043414227923354,
			"summary_team": 0.9454545454545454
		}]
	},
	"930290": {
		"nik": "930290",
		"nama": "TRI NUGROHO GUMELAR",
		"band": "V",
		"posisi": "OFF 2 INFRA FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.038461538461538,
			"nilai_komparatif_unit": 1.039092393843197,
			"summary_team": 0.8999999999999998
		}]
	},
	"930294": {
		"nik": "930294",
		"nama": "YOGANDA RAGIL PRANATA",
		"band": "V",
		"posisi": "SPV PLASA WATES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999999,
			"nilai_komparatif_unit": 0.9605831907528225,
			"summary_team": 0.8000000000000002
		}]
	},
	"930296": {
		"nik": "930296",
		"nama": "RYAN RIDHA RAMADHAN",
		"band": "V",
		"posisi": "OFF 2 CAPABILITY DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8733333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.997818974918211,
			"nilai_komparatif_unit": 0.9984251403340061,
			"summary_team": 0.8714285714285712
		}]
	},
	"930297": {
		"nik": "930297",
		"nama": "ADI MUHAMMAD WICAKSONO",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER EXPERIENCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7833333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0212765957446808,
			"nilai_komparatif_unit": 1.021897011439173,
			"summary_team": 0.7999999999999998
		}]
	},
	"930298": {
		"nik": "930298",
		"nama": "IRWAN HADI",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ - AREA SUMBAWA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300698,
			"nilai_komparatif_unit": 1.0705800421414762,
			"summary_team": 0.9272727272727272
		}]
	},
	"930303": {
		"nik": "930303",
		"nama": "REXY SEPTIAN ARAFAT",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8643678160919535,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0208072590738428,
			"nilai_komparatif_unit": 1.0214273896508284,
			"summary_team": 0.8823529411764706
		}]
	},
	"930312": {
		"nik": "930312",
		"nama": "DICKIE ZULFICKAR HERVIANTO",
		"band": "V",
		"posisi": "OFF 2 TELKOMGROUP FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0059583325620023,
			"summary_team": 0.8545454545454546
		}]
	},
	"930313": {
		"nik": "930313",
		"nama": "NI MADE AYU KARINA WIRASWARI",
		"band": "V",
		"posisi": "OFF 2 ACCESS INTEGRATION & SINERGY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9545454545454543,
			"nilai_komparatif_unit": 0.955125331714454,
			"summary_team": 0.8909090909090909
		}]
	},
	"930320": {
		"nik": "930320",
		"nama": "FADILAH",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649353,
			"nilai_komparatif_unit": 0.9356329780059962,
			"summary_team": 0.8727272727272728
		}]
	},
	"930324": {
		"nik": "930324",
		"nama": "RONI VAYAYANG",
		"band": "V",
		"posisi": "OFF 2 TGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8453333333333325,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9759463722397488,
			"nilai_komparatif_unit": 0.976539250260104,
			"summary_team": 0.8250000000000002
		}]
	},
	"930325": {
		"nik": "930325",
		"nama": "NUR AZIZAH",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0389610389610389,
			"nilai_komparatif_unit": 1.03959219778444,
			"summary_team": 0.9090909090909092
		}]
	},
	"930331": {
		"nik": "930331",
		"nama": "CORRIE ANGEL NATALIA LEWERISSA",
		"band": "V",
		"posisi": "SPV PLASA PASSO",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8722222222222226,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.936305732484076,
			"nilai_komparatif_unit": 0.9368745291976172,
			"summary_team": 0.8166666666666667
		}]
	},
	"930332": {
		"nik": "930332",
		"nama": "HIMAWAN PARAMESWARA NUGROHO",
		"band": "V",
		"posisi": "OFF 2 ADVALJAR OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.962566844919786,
			"nilai_komparatif_unit": 0.9631515950061722,
			"summary_team": 0.8181818181818182
		}]
	},
	"930334": {
		"nik": "930334",
		"nama": "RIDWAN ANDRI ANWAR",
		"band": "V",
		"posisi": "OFF 2 LOGISTIC PLANNING & EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8733333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0305343511450378,
			"nilai_komparatif_unit": 1.0311603908367606,
			"summary_team": 0.8999999999999999
		}]
	},
	"930335": {
		"nik": "930335",
		"nama": "PRASTYO PRAKOSO",
		"band": "V",
		"posisi": "OFF 2 CCAN WIFI FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8716049382716041,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0079927154997987,
			"nilai_komparatif_unit": 1.0086050613649988,
			"summary_team": 0.8785714285714287
		}]
	},
	"930339": {
		"nik": "930339",
		"nama": "FIRMANSYAH KURNIA AURIN",
		"band": "V",
		"posisi": "OFF 2 ACCESS DESIGN & RAB",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0006074903675237,
			"summary_team": 0.8
		}]
	},
	"930345": {
		"nik": "930345",
		"nama": "YUNI ARTI DARMAWAN",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139862,
			"nilai_komparatif_unit": 0.9866129800127331,
			"summary_team": 0.8545454545454546
		}]
	},
	"930346": {
		"nik": "930346",
		"nama": "NAHDA RIZQI MURLAWA",
		"band": "V",
		"posisi": "OFF 2 PAYMENT SUPPORT & READINESS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8380952380952383,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9545454545454544,
			"nilai_komparatif_unit": 0.9551253317144541,
			"summary_team": 0.8
		}]
	},
	"930349": {
		"nik": "930349",
		"nama": "YULISNA GITA HAPSARI",
		"band": "V",
		"posisi": "OFF 2 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8277777777777783,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0543014032946911,
			"nilai_komparatif_unit": 1.054941881241659,
			"summary_team": 0.8727272727272727
		}]
	},
	"930353": {
		"nik": "930353",
		"nama": "I MADE DEVAN GANAPATHI",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8857142857142859,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0469208211143692,
			"nilai_komparatif_unit": 1.047556815428756,
			"summary_team": 0.9272727272727272
		}]
	},
	"930357": {
		"nik": "930357",
		"nama": "FAHRI HANDIKA",
		"band": "V",
		"posisi": "OFF 2 LEGAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8691358024691351,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9368912337662344,
			"nilai_komparatif_unit": 0.9374603861661647,
			"summary_team": 0.8142857142857142
		}]
	},
	"930364": {
		"nik": "930364",
		"nama": "FACHRIAN ZULHAR",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ - AREA KUPANG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8999999999999991,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969707,
			"nilai_komparatif_unit": 0.9702860512654784,
			"summary_team": 0.8727272727272728
		}]
	},
	"930367": {
		"nik": "930367",
		"nama": "JASMIN",
		"band": "V",
		"posisi": "OFF 2 ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0576923076923073,
			"nilai_komparatif_unit": 1.0583348455810342,
			"summary_team": 0.9166666666666665
		}]
	},
	"930371": {
		"nik": "930371",
		"nama": "ADITYA WISHNU PRADIPTA",
		"band": "V",
		"posisi": "OFF 2 CCAN FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8277777777777782,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0323367907260521,
			"nilai_komparatif_unit": 1.0329639253824583,
			"summary_team": 0.8545454545454546
		}]
	},
	"930376": {
		"nik": "930376",
		"nama": "HARIATI",
		"band": "V",
		"posisi": "OFF 2 SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969694,
			"nilai_komparatif_unit": 0.9702860512654771,
			"summary_team": 0.8727272727272727
		}]
	},
	"930378": {
		"nik": "930378",
		"nama": "ABUDZAR ALI",
		"band": "V",
		"posisi": "OFF 2 OLO FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9318181818181819,
			"nilai_komparatif_unit": 0.9323842523879197,
			"summary_team": 0.7454545454545455
		}]
	},
	"930388": {
		"nik": "930388",
		"nama": "ENDAH WULANSARI",
		"band": "V",
		"posisi": "OFF 2 PREMIUM CLUSTER OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1,
			"nilai_komparatif_unit": 1.0006074903675235,
			"summary_team": 0.7999999999999999
		}]
	},
	"930406": {
		"nik": "930406",
		"nama": "RIFQI KASYIDI",
		"band": "V",
		"posisi": "OFF 2 PAYMENT & COLLECTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9807692307692305,
			"nilai_komparatif_unit": 0.9813650386296863,
			"summary_team": 0.8499999999999999
		}]
	},
	"930410": {
		"nik": "930410",
		"nama": "SATRIA ARIEF WICAKSONO BAKRI",
		"band": "V",
		"posisi": "OFF 2 REVAS & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139859,
			"nilai_komparatif_unit": 0.9866129800127329,
			"summary_team": 0.8545454545454546
		}]
	},
	"930414": {
		"nik": "930414",
		"nama": "SEPTHIAN DWI PUTRA PRABOWO",
		"band": "V",
		"posisi": "OFF 2 OM TRANSPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999997,
			"nilai_komparatif_unit": 0.9605831907528224,
			"summary_team": 0.8
		}]
	},
	"930418": {
		"nik": "930418",
		"nama": "AULIA FAIRUZANNUR YURA RIZQULLAH",
		"band": "V",
		"posisi": "OFF 2 CRISIS SUPPORT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9055555555555556,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0239821528165085,
			"nilai_komparatif_unit": 1.0246042121108605,
			"summary_team": 0.9272727272727272
		}]
	},
	"930420": {
		"nik": "930420",
		"nama": "FARIS RASYADI",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666662,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9425133689839578,
			"nilai_komparatif_unit": 0.9430859367768777,
			"summary_team": 0.8545454545454546
		}]
	},
	"940010": {
		"nik": "940010",
		"nama": "AJENG RAHMANINGTYAS FIRNADYA",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.111492281303602,
			"nilai_komparatif_unit": 1.1121675021580706,
			"summary_team": 0.9818181818181818
		}]
	},
	"940019": {
		"nik": "940019",
		"nama": "RIO WIBISONO",
		"band": "V",
		"posisi": "OFF 2 SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8547008547008538,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.148727272727274,
			"nilai_komparatif_unit": 1.1494251134803672,
			"summary_team": 0.9818181818181818
		}]
	},
	"940033": {
		"nik": "940033",
		"nama": "BIMA TAUFAN PRASEDYA",
		"band": "V",
		"posisi": "OFF 2 BACKBONE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559442,
			"nilai_komparatif_unit": 0.9446294489483615,
			"summary_team": 0.8181818181818182
		}]
	},
	"940062": {
		"nik": "940062",
		"nama": "CAESAR FIRDAUS",
		"band": "V",
		"posisi": "OFF 2 SOLUTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.75,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.993939393939394,
			"nilai_komparatif_unit": 0.9945432025471144,
			"summary_team": 0.7454545454545455
		}]
	},
	"940068": {
		"nik": "940068",
		"nama": "ARI SATRIO",
		"band": "V",
		"posisi": "OFF 2 OLO FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545454,
			"nilai_komparatif_unit": 1.0260774992132422,
			"summary_team": 0.8545454545454546
		}]
	},
	"940072": {
		"nik": "940072",
		"nama": "FERIZKA PARAMITA FIRDAUSI",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8361904761904755,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0436943466556232,
			"nilai_komparatif_unit": 1.0443283809178552,
			"summary_team": 0.8727272727272727
		}]
	},
	"940074": {
		"nik": "940074",
		"nama": "FADLUR RAHMAN MULIA TRISNO",
		"band": "V",
		"posisi": "OFF 2 ACCESS ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.027361701339917,
			"summary_team": 0.8727272727272727
		}]
	},
	"940080": {
		"nik": "940080",
		"nama": "NURSHABRINA PRAMESWARI",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8299999999999994,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0295728368017534,
			"nilai_komparatif_unit": 1.0301982923827744,
			"summary_team": 0.8545454545454546
		}]
	},
	"940083": {
		"nik": "940083",
		"nama": "MOCHAMMAD ICHWANUL REIZA",
		"band": "V",
		"posisi": "OFF 2 LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8691358024691351,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9697646103896109,
			"nilai_komparatif_unit": 0.9703537330491878,
			"summary_team": 0.8428571428571426
		}]
	},
	"940086": {
		"nik": "940086",
		"nama": "MUHAMMAD ZAKKI GHUFRON",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882353,
			"nilai_komparatif_unit": 0.9417482262282574,
			"summary_team": 0.8
		}]
	},
	"940090": {
		"nik": "940090",
		"nama": "NISSA AMELIA PAHLEVY",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER PROFILING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0463821892393321,
			"nilai_komparatif_unit": 1.0470178563400432,
			"summary_team": 0.8545454545454546
		}]
	},
	"940094": {
		"nik": "940094",
		"nama": "KARENDIYA KINASIH",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.027361701339917,
			"summary_team": 0.8727272727272727
		}]
	},
	"940097": {
		"nik": "940097",
		"nama": "I MADE ARY SWANTIKA",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8596491228070172,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0363636363636368,
			"nilai_komparatif_unit": 1.0369932172899794,
			"summary_team": 0.8909090909090909
		}]
	},
	"940101": {
		"nik": "940101",
		"nama": "MUHAMMAD IRFAN NOOR ALIF",
		"band": "V",
		"posisi": "OFF 2 SURVEILLANCE & LOGIC NE ACCESS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8458333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9673085535154501,
			"nilai_komparatif_unit": 0.9678961841441338,
			"summary_team": 0.8181818181818182
		}]
	},
	"940108": {
		"nik": "940108",
		"nama": "FAUZI HIDAYAT",
		"band": "V",
		"posisi": "OFF 2 ACCESS INTEGRATION & SINERGY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769228,
			"nilai_komparatif_unit": 0.9236376834161752,
			"summary_team": 0.8
		}]
	},
	"940113": {
		"nik": "940113",
		"nama": "PUSPITA MAHARDHIKA RAMADHANIA",
		"band": "V",
		"posisi": "OFF 2 LEVERAGING CUSTOMER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333337,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999996,
			"nilai_komparatif_unit": 0.9605831907528222,
			"summary_team": 0.8
		}]
	},
	"940118": {
		"nik": "940118",
		"nama": "AHMAD ALI",
		"band": "V",
		"posisi": "OFF 2 QUALITY MANAGEMENT SYSTEM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545454,
			"nilai_komparatif_unit": 1.0260774992132422,
			"summary_team": 0.8545454545454546
		}]
	},
	"940126": {
		"nik": "940126",
		"nama": "IKROM AULIA FAHDI",
		"band": "V",
		"posisi": "OFF 2 EMPLOYEE & INDUSTRIAL RELATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8407407407407405,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0075149002332213,
			"nilai_komparatif_unit": 1.0081269558302495,
			"summary_team": 0.8470588235294116
		}]
	},
	"940127": {
		"nik": "940127",
		"nama": "FATAHUDIN",
		"band": "V",
		"posisi": "OFF 2 OLO FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999999,
			"nilai_komparatif_unit": 0.9605831907528225,
			"summary_team": 0.8
		}]
	},
	"940129": {
		"nik": "940129",
		"nama": "GOLDSTEYN MELKY OCTOVIANUS MASELA",
		"band": "V",
		"posisi": "OFF 2 ADVALJAR OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.08051948051948,
			"nilai_komparatif_unit": 1.0811758856958171,
			"summary_team": 0.9454545454545454
		}]
	},
	"940130": {
		"nik": "940130",
		"nama": "NI PUTU EKA APSARI YUNIARI",
		"band": "V",
		"posisi": "SPV PLASA JEMBER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8733333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9680777238029149,
			"nilai_komparatif_unit": 0.9686658216951393,
			"summary_team": 0.8454545454545455
		}]
	},
	"940132": {
		"nik": "940132",
		"nama": "ALAMSYAH SYAMSUDDIN",
		"band": "V",
		"posisi": "OFF 2 PROCUREMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8733333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9160305343511447,
			"nilai_komparatif_unit": 0.9165870140771204,
			"summary_team": 0.7999999999999998
		}]
	},
	"940135": {
		"nik": "940135",
		"nama": "SUKMA WICAKSANA ABDI",
		"band": "V",
		"posisi": "SPV PLASA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0195412064570941,
			"nilai_komparatif_unit": 1.0201605679193102,
			"summary_team": 0.9090909090909091
		}]
	},
	"940137": {
		"nik": "940137",
		"nama": "ISTIHAMIN FITRIYAH",
		"band": "V",
		"posisi": "OFF 2 QoS & SLG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444443,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.947368421052632,
			"nilai_komparatif_unit": 0.9479439382429174,
			"summary_team": 0.8000000000000003
		}]
	},
	"940140": {
		"nik": "940140",
		"nama": "LUTHFAN IBNU ASHARI",
		"band": "V",
		"posisi": "OFF 2 LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8912280701754386,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0404438081603435,
			"nilai_komparatif_unit": 1.0410758677517504,
			"summary_team": 0.9272727272727272
		}]
	},
	"940143": {
		"nik": "940143",
		"nama": "RIO WICAKSONO",
		"band": "V",
		"posisi": "SPV AREA KOTA BARU",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8848484848484849,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9349315068493149,
			"nilai_komparatif_unit": 0.9354994687340201,
			"summary_team": 0.8272727272727272
		}]
	},
	"940144": {
		"nik": "940144",
		"nama": "AHMAD WIRA INDRAWAN",
		"band": "V",
		"posisi": "OFF 2 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.021276595744681,
			"nilai_komparatif_unit": 1.0218970114391732,
			"summary_team": 0.8
		}]
	},
	"940152": {
		"nik": "940152",
		"nama": "AIFATUL VIPTA FITRIA EL INDRI",
		"band": "V",
		"posisi": "OFF 2 PRODUCT SOLUTION & DELIVERY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0576923076923075,
			"nilai_komparatif_unit": 1.0583348455810344,
			"summary_team": 0.9166666666666665
		}]
	},
	"940159": {
		"nik": "940159",
		"nama": "RADITYA NARENDRA PUTRA",
		"band": "V",
		"posisi": "OFF 2 ACCESS OM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.090909090909091,
			"nilai_komparatif_unit": 1.0915718076736622,
			"summary_team": 0.8545454545454546
		}]
	},
	"940162": {
		"nik": "940162",
		"nama": "MARLINTIKA",
		"band": "V",
		"posisi": "OFF 2 ACCESS PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090906,
			"nilai_komparatif_unit": 1.0915718076736618,
			"summary_team": 0.9454545454545454
		}]
	},
	"940164": {
		"nik": "940164",
		"nama": "PRAYUDI ADITYA NUGRAHA",
		"band": "V",
		"posisi": "OFF 2 DIGITAL SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0408673894912424,
			"nilai_komparatif_unit": 1.0414997064042277,
			"summary_team": 0.9454545454545454
		}]
	},
	"940165": {
		"nik": "940165",
		"nama": "AISHA ALFIANI MAHARDHIKA",
		"band": "V",
		"posisi": "OFF 2 BUSINESS APPLICATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9592476489028213,
			"nilai_komparatif_unit": 0.9598303826095994,
			"summary_team": 0.9272727272727272
		}]
	},
	"940166": {
		"nik": "940166",
		"nama": "AMALIA SHARFINA SARNYOTO",
		"band": "V",
		"posisi": "OFF 2 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7333333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0165289256198347,
			"nilai_komparatif_unit": 1.0171464571504578,
			"summary_team": 0.7454545454545455
		}]
	},
	"940170": {
		"nik": "940170",
		"nama": "TOMY PARANDANGI",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0980392156862742,
			"nilai_komparatif_unit": 1.0987062639329668,
			"summary_team": 0.9333333333333331
		}]
	},
	"940171": {
		"nik": "940171",
		"nama": "FATHUR AHMADI",
		"band": "V",
		"posisi": "OFF 2 OM CME",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8708333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1274467159634627,
			"nilai_komparatif_unit": 1.1281316289833065,
			"summary_team": 0.9818181818181818
		}]
	},
	"940180": {
		"nik": "940180",
		"nama": "NUR RAHMAN ALI MUHTI",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333337,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545452,
			"nilai_komparatif_unit": 1.026077499213242,
			"summary_team": 0.8545454545454546
		}]
	},
	"940185": {
		"nik": "940185",
		"nama": "TAUFIK NUR FAUZI",
		"band": "V",
		"posisi": "OFF 2 NETWORK PERF, QM & ADMIN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8952380952380956,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0357833655705992,
			"nilai_komparatif_unit": 1.0364125939880244,
			"summary_team": 0.9272727272727274
		}]
	},
	"940190": {
		"nik": "940190",
		"nama": "FIANDRA FATHARANY",
		"band": "V",
		"posisi": "OFF 2 FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272725,
			"nilai_komparatif_unit": 1.0233485696940579,
			"summary_team": 0.8181818181818182
		}]
	},
	"940192": {
		"nik": "940192",
		"nama": "DENI KURNIAWAN LAMSI",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090904,
			"nilai_komparatif_unit": 1.0915718076736616,
			"summary_team": 0.9272727272727272
		}]
	},
	"940194": {
		"nik": "940194",
		"nama": "DHELVINA DWI NEDTRI",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8277777777777782,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9664429530201338,
			"nilai_komparatif_unit": 0.9670300578048545,
			"summary_team": 0.8
		}]
	},
	"940196": {
		"nik": "940196",
		"nama": "RIZKA NURHASANAH",
		"band": "V",
		"posisi": "OFF 2 DATA CUSTOMER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.087378640776699,
			"nilai_komparatif_unit": 1.0880392128268217,
			"summary_team": 0.9333333333333335
		}]
	},
	"940197": {
		"nik": "940197",
		"nama": "MARDIANTY LAMUSU",
		"band": "V",
		"posisi": "OFF 2 VERIFICATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8983957219251332,
			"nilai_komparatif_unit": 0.8989414886724271,
			"summary_team": 0.7636363636363636
		}]
	},
	"940198": {
		"nik": "940198",
		"nama": "YULPEN WENDA",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ ABEPURA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8296296296296299,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0519480519480515,
			"nilai_komparatif_unit": 1.052587100256745,
			"summary_team": 0.8727272727272727
		}]
	},
	"940201": {
		"nik": "940201",
		"nama": "RADITYA SUKMA YUDHA",
		"band": "V",
		"posisi": "OFF 2 DATA CUSTOMER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0059583325620023,
			"summary_team": 0.8545454545454546
		}]
	},
	"940204": {
		"nik": "940204",
		"nama": "MERI ANGGRAINI",
		"band": "V",
		"posisi": "OFF 2 FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.877777777777778,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9735327963176061,
			"nilai_komparatif_unit": 0.9741242081138374,
			"summary_team": 0.8545454545454545
		}]
	},
	"940206": {
		"nik": "940206",
		"nama": "HAVID GHILMAN SHALEH",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0805194805194804,
			"nilai_komparatif_unit": 1.0811758856958176,
			"summary_team": 0.9454545454545454
		}]
	},
	"940208": {
		"nik": "940208",
		"nama": "FAISAL HELMI AMARULLAH",
		"band": "V",
		"posisi": "OFF 2 HC PLANNING",
		"category": [{
			"code": "SP-1-01",
			"kriteria": 0.8333333333333337,
			"kriteria_bp": 0.9986889999534173,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181815,
			"nilai_komparatif_unit": 0.9831070351871075,
			"summary_team": 0.8181818181818182
		}]
	},
	"940220": {
		"nik": "940220",
		"nama": "RYAN ACHMAD ARIANTO EKASAPUTRA",
		"band": "V",
		"posisi": "OFF 2 MARKET PLAN & ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96,
			"nilai_komparatif_unit": 0.9605831907528226,
			"summary_team": 0.8
		}]
	},
	"940221": {
		"nik": "940221",
		"nama": "SUHENDAR",
		"band": "V",
		"posisi": "OFF 2 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.944055944055944,
			"nilai_komparatif_unit": 0.9446294489483613,
			"summary_team": 0.8181818181818181
		}]
	},
	"940223": {
		"nik": "940223",
		"nama": "MUHAMMAD QOMARUZ ZAMAN",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.027361701339917,
			"summary_team": 0.8727272727272728
		}]
	},
	"940226": {
		"nik": "940226",
		"nama": "MOH. ARIEF RAHMAN MANSUR",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8606060606060609,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0264084507042253,
			"nilai_komparatif_unit": 1.0270319839511728,
			"summary_team": 0.8833333333333335
		}]
	},
	"940227": {
		"nik": "940227",
		"nama": "ILHAM EFENDI",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ/ SPV AREA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9128014842300555,
			"nilai_komparatif_unit": 0.9133560023391865,
			"summary_team": 0.7454545454545455
		}]
	},
	"940229": {
		"nik": "940229",
		"nama": "SIGIT WAHYU PRATAMA",
		"band": "V",
		"posisi": "OFF 2 CONSTRUCTION SUPERVISION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8361111111111103,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9568106312292369,
			"nilai_komparatif_unit": 0.9573918844712528,
			"summary_team": 0.8
		}]
	},
	"940231": {
		"nik": "940231",
		"nama": "TAUFIQ DWI APRIYANTO",
		"band": "V",
		"posisi": "OFF 2 HC SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8266666666666661,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0557184750733144,
			"nilai_komparatif_unit": 1.0563598138777381,
			"summary_team": 0.8727272727272727
		}]
	},
	"940232": {
		"nik": "940232",
		"nama": "APRI ROKHYADI",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9520661157024791,
			"nilai_komparatif_unit": 0.952644486697014,
			"summary_team": 0.8727272727272727
		}]
	},
	"940236": {
		"nik": "940236",
		"nama": "KALEB MARTINUS AWOM",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.018181818181818,
			"nilai_komparatif_unit": 1.0188003538287511,
			"summary_team": 0.9333333333333331
		}]
	},
	"940239": {
		"nik": "940239",
		"nama": "ANDRE LARRY MARAMIS",
		"band": "V",
		"posisi": "OFF 2 WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9629629629629627,
			"nilai_komparatif_unit": 0.9635479536872447,
			"summary_team": 0.8666666666666665
		}]
	},
	"940243": {
		"nik": "940243",
		"nama": "INDRA BARI YULIO",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365353,
			"nilai_komparatif_unit": 0.9885933352516185,
			"summary_team": 0.8727272727272728
		}]
	},
	"940247": {
		"nik": "940247",
		"nama": "SILA PRAYOGININGSIH",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER PAYMENT COLLECTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0380622837370241,
			"nilai_komparatif_unit": 1.038692896575284,
			"summary_team": 0.8823529411764706
		}]
	},
	"940249": {
		"nik": "940249",
		"nama": "YAFET YULIO",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9766233766233763,
			"nilai_komparatif_unit": 0.9772166659173733,
			"summary_team": 0.8545454545454546
		}]
	},
	"940252": {
		"nik": "940252",
		"nama": "MUHAMAD BODY PRAHALA",
		"band": "V",
		"posisi": "OFF 2 SALES (AMUNTAI)",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999997,
			"nilai_komparatif_unit": 0.9605831907528224,
			"summary_team": 0.8
		}]
	},
	"940254": {
		"nik": "940254",
		"nama": "ESRA HANDITO",
		"band": "V",
		"posisi": "OFF 2 OM TRANSPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0059583325620023,
			"summary_team": 0.8545454545454546
		}]
	},
	"940255": {
		"nik": "940255",
		"nama": "DHENY ASHARI HS",
		"band": "V",
		"posisi": "OFF 2 SECRETARY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8138888888888879,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0696647259586443,
			"nilai_komparatif_unit": 1.0703145369761438,
			"summary_team": 0.8705882352941177
		}]
	},
	"940263": {
		"nik": "940263",
		"nama": "TEGUH SEPTIANTO",
		"band": "V",
		"posisi": "OFF 2 SALES MALINAU",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333337,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0167696381288611,
			"nilai_komparatif_unit": 1.0173873158900149,
			"summary_team": 0.8727272727272728
		}]
	},
	"940265": {
		"nik": "940265",
		"nama": "RACHMANIA ILAVI",
		"band": "V",
		"posisi": "OFF 2 WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333337,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0167696381288611,
			"nilai_komparatif_unit": 1.0173873158900149,
			"summary_team": 0.8727272727272728
		}]
	},
	"940266": {
		"nik": "940266",
		"nama": "FAISAL DHIO SAPUTRA",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ GUBENG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8453333333333325,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0108976197304285,
			"nilai_komparatif_unit": 1.0115117302969672,
			"summary_team": 0.8545454545454546
		}]
	},
	"940273": {
		"nik": "940273",
		"nama": "ADIKA EKA PRAWIRA",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9090909090909093,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0199999999999998,
			"nilai_komparatif_unit": 1.0206196401748737,
			"summary_team": 0.9272727272727272
		}]
	},
	"940274": {
		"nik": "940274",
		"nama": "JAN FANDRO SIAHAAN",
		"band": "V",
		"posisi": "OFF 2 SALES PROMOTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9163636363636363,
			"nilai_komparatif_unit": 0.9169203184458761,
			"summary_team": 0.7636363636363637
		}]
	},
	"940275": {
		"nik": "940275",
		"nama": "SOFYAN ABDURAHMAN",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7692307692307695,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0399999999999994,
			"nilai_komparatif_unit": 1.040631789982224,
			"summary_team": 0.7999999999999998
		}]
	},
	"940282": {
		"nik": "940282",
		"nama": "RIO PARANGRENGI",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9927431059506531,
			"nilai_komparatif_unit": 0.9933461878249434,
			"summary_team": 0.8769230769230769
		}]
	},
	"940291": {
		"nik": "940291",
		"nama": "EKO TRI PRASTYO",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ - INNER AREA PB",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8083333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9222118088097468,
			"nilai_komparatif_unit": 0.9227720436004152,
			"summary_team": 0.7454545454545456
		}]
	},
	"940295": {
		"nik": "940295",
		"nama": "AGUNG NUR WICAKSONO",
		"band": "V",
		"posisi": "OFF 2 ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300696,
			"nilai_komparatif_unit": 1.070580042141476,
			"summary_team": 0.9272727272727272
		}]
	},
	"940296": {
		"nik": "940296",
		"nama": "ANDI PARAMATA IMADUDIN PABABBARI",
		"band": "V",
		"posisi": "OFF 2 OM TRANSPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8708333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0021748586341892,
			"nilai_komparatif_unit": 1.0027836702073838,
			"summary_team": 0.8727272727272728
		}]
	},
	"940299": {
		"nik": "940299",
		"nama": "MUHAMMAD WISNU FIRMAN",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9777777777777779,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272727,
			"nilai_komparatif_unit": 1.023348569694058,
			"summary_team": 1
		}]
	},
	"940308": {
		"nik": "940308",
		"nama": "RACHMAT FAHMI AZIZ",
		"band": "V",
		"posisi": "OFF 2 FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559442,
			"nilai_komparatif_unit": 0.9446294489483615,
			"summary_team": 0.8181818181818182
		}]
	},
	"940320": {
		"nik": "940320",
		"nama": "M. ILHAM ESMAN",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7916666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9684210526315786,
			"nilai_komparatif_unit": 0.9690093590927593,
			"summary_team": 0.7666666666666666
		}]
	},
	"940323": {
		"nik": "940323",
		"nama": "HANIF KUNCAHYO ADI",
		"band": "V",
		"posisi": "OFF 2 OSS APPLICATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559437,
			"nilai_komparatif_unit": 0.9446294489483611,
			"summary_team": 0.8181818181818182
		}]
	},
	"940328": {
		"nik": "940328",
		"nama": "MUHAMMAD SYAMSUDIN AL MUHAROM",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ - INNER AREA JG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8512820512820516,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9397590361445783,
			"nilai_komparatif_unit": 0.9403299307068294,
			"summary_team": 0.8000000000000003
		}]
	},
	"940337": {
		"nik": "940337",
		"nama": "EKA CITRA AGUSTINI",
		"band": "V",
		"posisi": "OFF 2 FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8460784313725501,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0959654482250065,
			"nilai_komparatif_unit": 1.0966312366779418,
			"summary_team": 0.9272727272727272
		}]
	},
	"940338": {
		"nik": "940338",
		"nama": "EDO PROBOLAKSANA AGANINGGAR",
		"band": "V",
		"posisi": "OFF 2 CCAN FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8999999999999991,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9494949494949505,
			"nilai_komparatif_unit": 0.950071758530781,
			"summary_team": 0.8545454545454546
		}]
	},
	"940341": {
		"nik": "940341",
		"nama": "FAISAL WISNU PRADHANA",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8358974358974361,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9570552147239262,
			"nilai_komparatif_unit": 0.9576366165480591,
			"summary_team": 0.8
		}]
	},
	"940351": {
		"nik": "940351",
		"nama": "AZKA ADITYA",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9814453124999997,
			"nilai_komparatif_unit": 0.9820415310735945,
			"summary_team": 0.8374999999999999
		}]
	},
	"940354": {
		"nik": "940354",
		"nama": "LESLIE PANJAITAN",
		"band": "V",
		"posisi": "OFF 2 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.901960784313725,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0280632411067199,
			"nilai_komparatif_unit": 1.0286877796228973,
			"summary_team": 0.9272727272727272
		}]
	},
	"940360": {
		"nik": "940360",
		"nama": "A. REZKY PANCA PUTRA",
		"band": "V",
		"posisi": "OFF 2 CAPEX MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365353,
			"nilai_komparatif_unit": 0.9885933352516185,
			"summary_team": 0.8727272727272728
		}]
	},
	"940361": {
		"nik": "940361",
		"nama": "ALIN WAHYU SETIAWAN",
		"band": "V",
		"posisi": "OFF 2 CCAN FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8703703703703703,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0340425531914892,
			"nilai_komparatif_unit": 1.0346707240821624,
			"summary_team": 0.8999999999999999
		}]
	},
	"940372": {
		"nik": "940372",
		"nama": "PITKAHISMI WIMADATU",
		"band": "V",
		"posisi": "SPV PLASA UBUD",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0076047455449186,
			"summary_team": 0.8727272727272728
		}]
	},
	"940380": {
		"nik": "940380",
		"nama": "RITA AMANDA ANDRIANA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8643678160919535,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0727756286266932,
			"nilai_komparatif_unit": 1.073427329487598,
			"summary_team": 0.9272727272727274
		}]
	},
	"940385": {
		"nik": "940385",
		"nama": "DELINA RISA KINASIH",
		"band": "V",
		"posisi": "OFF 2 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444443,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.947368421052632,
			"nilai_komparatif_unit": 0.9479439382429174,
			"summary_team": 0.8000000000000003
		}]
	},
	"940395": {
		"nik": "940395",
		"nama": "LUTHFIYAH AZZAHRA",
		"band": "V",
		"posisi": "OFF 2 TAX OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0526315789473684,
			"nilai_komparatif_unit": 1.0532710424921299,
			"summary_team": 1
		}]
	},
	"940396": {
		"nik": "940396",
		"nama": "FAISAL FATHONI",
		"band": "V",
		"posisi": "OFF 2 WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090908,
			"nilai_komparatif_unit": 1.091571807673662,
			"summary_team": 0.9454545454545454
		}]
	},
	"940400": {
		"nik": "940400",
		"nama": "MARINA INDRIANI LESTARI",
		"band": "V",
		"posisi": "OFF 2 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197862,
			"nilai_komparatif_unit": 0.9631515950061724,
			"summary_team": 0.8181818181818182
		}]
	},
	"950016": {
		"nik": "950016",
		"nama": "KADEK HARI BASKARA",
		"band": "V",
		"posisi": "OFF 2 TGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7047619047619048,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.170608108108108,
			"nilai_komparatif_unit": 1.1713192412579285,
			"summary_team": 0.825
		}]
	},
	"950025": {
		"nik": "950025",
		"nama": "FADHILAH HAMZAH",
		"band": "V",
		"posisi": "OFF 2 PRICING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8410256410256411,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0304878048780484,
			"nilai_komparatif_unit": 1.0311138162933622,
			"summary_team": 0.8666666666666665
		}]
	},
	"950028": {
		"nik": "950028",
		"nama": "AYUNDHA PUSPADINI",
		"band": "V",
		"posisi": "OFF 2 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9935064935064937,
			"nilai_komparatif_unit": 0.994110039131371,
			"summary_team": 0.9272727272727274
		}]
	},
	"950029": {
		"nik": "950029",
		"nama": "ASTRI ELMADHANIA",
		"band": "V",
		"posisi": "SPV PLASA PASURUAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8277777777777783,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9444783404514943,
			"nilai_komparatif_unit": 0.9450521019456531,
			"summary_team": 0.7818181818181819
		}]
	},
	"950033": {
		"nik": "950033",
		"nama": "RAHMAT FAUZAN TANGGAHMA",
		"band": "V",
		"posisi": "OFF 2 CCAN FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8857142857142858,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0725806451612903,
			"nilai_komparatif_unit": 1.073232227571618,
			"summary_team": 0.95
		}]
	},
	"950039": {
		"nik": "950039",
		"nama": "YOGA BAYU AJI PRANAWA",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0059583325620023,
			"summary_team": 0.8545454545454546
		}]
	},
	"950046": {
		"nik": "950046",
		"nama": "TARA DAMAYANTI",
		"band": "V",
		"posisi": "OFF 2 PROVISIONING & PIC MITRA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9514563106796116,
			"nilai_komparatif_unit": 0.9520343112234689,
			"summary_team": 0.8166666666666668
		}]
	},
	"950051": {
		"nik": "950051",
		"nama": "RUBEN ALLPIO SIREGAR",
		"band": "V",
		"posisi": "OFF 2 ADVALJAR OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9801869293396148,
			"summary_team": 0.8
		}]
	},
	"950054": {
		"nik": "950054",
		"nama": "ADRIANUS ARYO BIMO NICOLAS ROMUTY",
		"band": "V",
		"posisi": "OFF 2 ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888893,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.961363636363636,
			"nilai_komparatif_unit": 0.9619476555124142,
			"summary_team": 0.8545454545454546
		}]
	},
	"950055": {
		"nik": "950055",
		"nama": "TYASSARI KUSUMANINGSIH",
		"band": "V",
		"posisi": "OFF 2 INFRA OPERATION PERFORMANCE MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9090909090909092,
			"nilai_komparatif_unit": 0.9096431730613851,
			"summary_team": 0.8181818181818182
		}]
	},
	"950058": {
		"nik": "950058",
		"nama": "DANA INTAN SATARI",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999995,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000007,
			"nilai_komparatif_unit": 1.0006074903675242,
			"summary_team": 0.8
		}]
	},
	"950059": {
		"nik": "950059",
		"nama": "INSOS LINCE MIRINO",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.891666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9906542056074762,
			"nilai_komparatif_unit": 0.9912560184949294,
			"summary_team": 0.8833333333333333
		}]
	},
	"950065": {
		"nik": "950065",
		"nama": "REZA HERMANSYAH",
		"band": "V",
		"posisi": "OFF 2 FAULT HANDLING & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8250000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.057851239669421,
			"nilai_komparatif_unit": 1.0584938741077932,
			"summary_team": 0.8727272727272727
		}]
	},
	"950066": {
		"nik": "950066",
		"nama": "CLARA THERESIA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8547008547008538,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.949764705882354,
			"nilai_komparatif_unit": 0.9503416787925913,
			"summary_team": 0.8117647058823529
		}]
	},
	"950078": {
		"nik": "950078",
		"nama": "FATHURAHMAN BURHANI",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8857142857142858,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0263929618768328,
			"nilai_komparatif_unit": 1.027016485714467,
			"summary_team": 0.9090909090909092
		}]
	},
	"950079": {
		"nik": "950079",
		"nama": "AMALIA ARDIASTI",
		"band": "V",
		"posisi": "OFF 2 REVAS & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8095238095238096,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.055614973262032,
			"nilai_komparatif_unit": 1.0562562491901022,
			"summary_team": 0.8545454545454546
		}]
	},
	"950085": {
		"nik": "950085",
		"nama": "IRFAN RAMADHAN PRAMUDITA",
		"band": "V",
		"posisi": "OFF 2 CCAN FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9882352941176468,
			"nilai_komparatif_unit": 0.9888356375396701,
			"summary_team": 0.84
		}]
	},
	"950087": {
		"nik": "950087",
		"nama": "FAKHRUL ARIFIN FEBRIYANTO",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8547008547008538,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0210909090909104,
			"nilai_komparatif_unit": 1.0217112119825489,
			"summary_team": 0.8727272727272728
		}]
	},
	"950096": {
		"nik": "950096",
		"nama": "SWIZYA SATIRA NOLIKA",
		"band": "V",
		"posisi": "OFF 2 HR CAREER & TASKFORCE MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559442,
			"nilai_komparatif_unit": 0.9446294489483615,
			"summary_team": 0.8181818181818182
		}]
	},
	"950098": {
		"nik": "950098",
		"nama": "SELINA DWI SUSANTI",
		"band": "V",
		"posisi": "OFF 2 QUALITY ENHANCEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0076047455449186,
			"summary_team": 0.8727272727272727
		}]
	},
	"950101": {
		"nik": "950101",
		"nama": "DEVLINA PRILIANTI",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9504950495049503,
			"nilai_komparatif_unit": 0.9510724660919034,
			"summary_team": 0.8
		}]
	},
	"950102": {
		"nik": "950102",
		"nama": "ESMID MUHAMAD RASYID RIDHO",
		"band": "V",
		"posisi": "OFF 2 HR SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8266666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.019585253456221,
			"nilai_komparatif_unit": 1.0202046416765649,
			"summary_team": 0.8428571428571427
		}]
	},
	"950103": {
		"nik": "950103",
		"nama": "FIJRAH APRILLA S. ASRI, S.T.",
		"band": "V",
		"posisi": "OFF 2 ACCESS DESIGN & RAB",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8460784313725501,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1174549668176537,
			"nilai_komparatif_unit": 1.1181338099461366,
			"summary_team": 0.9454545454545454
		}]
	},
	"950106": {
		"nik": "950106",
		"nama": "DZULHARMAN BARUWADI",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8361111111111103,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0764119601328914,
			"nilai_komparatif_unit": 1.0770658700301592,
			"summary_team": 0.8999999999999999
		}]
	},
	"950109": {
		"nik": "950109",
		"nama": "DIDIT PRASETYA",
		"band": "V",
		"posisi": "OFF 2 REVAS & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0291595197255574,
			"nilai_komparatif_unit": 1.0297847242204359,
			"summary_team": 0.9090909090909092
		}]
	},
	"950110": {
		"nik": "950110",
		"nama": "HAMIDA ALFATHI SYIFAUNA",
		"band": "V",
		"posisi": "SPV PLASA SIDOARJO",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666661,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300707,
			"nilai_komparatif_unit": 1.070580042141477,
			"summary_team": 0.9272727272727274
		}]
	},
	"950117": {
		"nik": "950117",
		"nama": "MUHAMMAD AJRUL MAHBUB",
		"band": "V",
		"posisi": "SPV PLASA BANYUWANGI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.02020202020202,
			"nilai_komparatif_unit": 1.0208217831022208,
			"summary_team": 0.9181818181818182
		}]
	},
	"950118": {
		"nik": "950118",
		"nama": "ACHSANUL KAMAL",
		"band": "V",
		"posisi": "OFF 2 ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.031404958677686,
			"nilai_komparatif_unit": 1.0320315272550986,
			"summary_team": 0.9454545454545454
		}]
	},
	"950120": {
		"nik": "950120",
		"nama": "FAIRUZ FAISAL THALIB",
		"band": "V",
		"posisi": "OFF 2 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96,
			"nilai_komparatif_unit": 0.9605831907528226,
			"summary_team": 0.8
		}]
	},
	"950121": {
		"nik": "950121",
		"nama": "SADDEK SYAFRULLAH",
		"band": "V",
		"posisi": "OFF 2 CAPEX EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888888,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9000000000000002,
			"nilai_komparatif_unit": 0.9005467413307714,
			"summary_team": 0.8000000000000002
		}]
	},
	"950126": {
		"nik": "950126",
		"nama": "IRFAN KURNIAWAN",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ-INNER AREA LMG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8708333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.91866028708134,
			"nilai_komparatif_unit": 0.9192183643567683,
			"summary_team": 0.8
		}]
	},
	"950131": {
		"nik": "950131",
		"nama": "ANDRI IRAWAN",
		"band": "V",
		"posisi": "OFF 2 DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.891666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9532710280373826,
			"nilai_komparatif_unit": 0.9538501310045546,
			"summary_team": 0.8499999999999999
		}]
	},
	"950132": {
		"nik": "950132",
		"nama": "AHMAD FAUZI",
		"band": "V",
		"posisi": "OFF 2 QUALITY ENHANCEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9322314049586778,
			"nilai_komparatif_unit": 0.9327977265574932,
			"summary_team": 0.8545454545454546
		}]
	},
	"950134": {
		"nik": "950134",
		"nama": "BILLY REINHART COLUMBUS MARPAUNG",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER EXPERIENCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0101010101010102,
			"nilai_komparatif_unit": 1.0107146367348723,
			"summary_team": 0.9090909090909092
		}]
	},
	"950135": {
		"nik": "950135",
		"nama": "MUHAMMAD DWI RIFQI",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8111111111111113,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0787671232876712,
			"nilai_komparatif_unit": 1.0794224639238696,
			"summary_team": 0.8750000000000002
		}]
	},
	"950140": {
		"nik": "950140",
		"nama": "RACHEL EVALYN",
		"band": "V",
		"posisi": "OFF 2 BIDDING MANAGEMENT & OBL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8749999999999994,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0597402597402603,
			"nilai_komparatif_unit": 1.0603840417401293,
			"summary_team": 0.9272727272727272
		}]
	},
	"950148": {
		"nik": "950148",
		"nama": "NOVI PERMATA SARI",
		"band": "V",
		"posisi": "OFF 2 LOGISTIK & LEGAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139859,
			"nilai_komparatif_unit": 0.9866129800127329,
			"summary_team": 0.8545454545454546
		}]
	},
	"950159": {
		"nik": "950159",
		"nama": "I WAYAN ANGGA KUSUMA YOGA",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649348,
			"nilai_komparatif_unit": 0.9356329780059958,
			"summary_team": 0.8
		}]
	},
	"950161": {
		"nik": "950161",
		"nama": "TIARA IMA KHOTA",
		"band": "V",
		"posisi": "OFF 2 BUDGET CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999999,
			"nilai_komparatif_unit": 1.0006074903675235,
			"summary_team": 0.7999999999999998
		}]
	},
	"950163": {
		"nik": "950163",
		"nama": "MOCH.KURNIAWAN PURNAMA SAPUTRA",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0505050505050502,
			"nilai_komparatif_unit": 1.0511432222042667,
			"summary_team": 0.9454545454545454
		}]
	},
	"950165": {
		"nik": "950165",
		"nama": "MUHAMMAD TARIS ANSHARI",
		"band": "V",
		"posisi": "OFF 2 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0463821892393321,
			"nilai_komparatif_unit": 1.0470178563400432,
			"summary_team": 0.8545454545454546
		}]
	},
	"950166": {
		"nik": "950166",
		"nama": "FAREZA RIZKY RAMADHAN",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8539682539682542,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9580939506590063,
			"nilai_komparatif_unit": 0.9586759835052142,
			"summary_team": 0.8181818181818182
		}]
	},
	"950173": {
		"nik": "950173",
		"nama": "PUTRI NUR CAHYANTI",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.877777777777778,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9735327963176063,
			"nilai_komparatif_unit": 0.9741242081138375,
			"summary_team": 0.8545454545454546
		}]
	},
	"950179": {
		"nik": "950179",
		"nama": "ROKHAYATI MAYA RENGGANI",
		"band": "V",
		"posisi": "OFF 2 CCAN FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000004,
			"nilai_komparatif_unit": 1.000607490367524,
			"summary_team": 0.8000000000000003
		}]
	},
	"950190": {
		"nik": "950190",
		"nama": "RAHMAH SHABRINA",
		"band": "V",
		"posisi": "OFF 2 PUSH CHANNEL INTEGRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9090909090909088,
			"nilai_komparatif_unit": 0.9096431730613848,
			"summary_team": 0.8181818181818182
		}]
	},
	"950197": {
		"nik": "950197",
		"nama": "M. ALFIAN FADLI JAUHARI",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8323232323232315,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9830097087378652,
			"nilai_komparatif_unit": 0.9836068776671055,
			"summary_team": 0.8181818181818182
		}]
	},
	"950210": {
		"nik": "950210",
		"nama": "NAUFAL RAHMAD NUR",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777781,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9735327963176061,
			"nilai_komparatif_unit": 0.9741242081138374,
			"summary_team": 0.8545454545454546
		}]
	},
	"950213": {
		"nik": "950213",
		"nama": "RAHMA YULIA PRASTIWI",
		"band": "V",
		"posisi": "OFF 2 PUSH CHANNEL INTEGRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8250000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9917355371900823,
			"nilai_komparatif_unit": 0.9923380069760561,
			"summary_team": 0.8181818181818182
		}]
	},
	"950215": {
		"nik": "950215",
		"nama": "NADIA MAULIZA",
		"band": "V",
		"posisi": "OFF 2 CONSTRUCTION SUPERVISION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.962566844919786,
			"nilai_komparatif_unit": 0.9631515950061722,
			"summary_team": 0.8181818181818182
		}]
	},
	"950216": {
		"nik": "950216",
		"nama": "AGUSTIN NUR MALITA",
		"band": "V",
		"posisi": "SPV PLASA MADIUN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0686456400742113,
			"nilai_komparatif_unit": 1.0692948320068523,
			"summary_team": 0.8727272727272727
		}]
	},
	"950222": {
		"nik": "950222",
		"nama": "RANA AHMAD PRIMANUGRAHA",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ -INNER AREA LMJ",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.971900826446281,
			"nilai_komparatif_unit": 0.9724912468365352,
			"summary_team": 0.8909090909090909
		}]
	},
	"950224": {
		"nik": "950224",
		"nama": "AGREDO MESAKH F SIHOMBING",
		"band": "V",
		"posisi": "OFF 2 APPLICATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666665,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0018552875695734,
			"nilai_komparatif_unit": 1.0024639050064246,
			"summary_team": 0.8181818181818182
		}]
	},
	"950231": {
		"nik": "950231",
		"nama": "LIWA NOOR GAJAYANA",
		"band": "V",
		"posisi": "OFF 2 SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0303030303030303,
			"nilai_komparatif_unit": 1.0309289294695696,
			"summary_team": 0.9272727272727272
		}]
	},
	"950244": {
		"nik": "950244",
		"nama": "ERLIN NABILA OCTAVIANI",
		"band": "V",
		"posisi": "OFF 2 TAX OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9952153110047846,
			"nilai_komparatif_unit": 0.9958198947198319,
			"summary_team": 0.9454545454545454
		}]
	},
	"950245": {
		"nik": "950245",
		"nama": "SITTI ALVINA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8545454545454549,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1063829787234036,
			"nilai_komparatif_unit": 1.10705509572577,
			"summary_team": 0.9454545454545454
		}]
	},
	"950248": {
		"nik": "950248",
		"nama": "MEUTIA QOONITA NOVIYANI",
		"band": "V",
		"posisi": "OFF 2 SURVEILLANCE & LOGIC NE ACCESS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.962566844919786,
			"nilai_komparatif_unit": 0.9631515950061722,
			"summary_team": 0.8181818181818182
		}]
	},
	"950258": {
		"nik": "950258",
		"nama": "DESY PUTRI UTAMI",
		"band": "V",
		"posisi": "OFF 2 SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.967409948542024,
			"nilai_komparatif_unit": 0.9679976407672097,
			"summary_team": 0.8545454545454546
		}]
	},
	"950265": {
		"nik": "950265",
		"nama": "FURRY RACHMAWATI",
		"band": "V",
		"posisi": "OFF 2 ACCESS DEPLOYMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.914285714285714,
			"nilai_komparatif_unit": 0.914841134050307,
			"summary_team": 0.8
		}]
	},
	"950279": {
		"nik": "950279",
		"nama": "AFIF FATRI PRATAMA",
		"band": "V",
		"posisi": "OFF 2 DEBT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888888,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0968749999999998,
			"nilai_komparatif_unit": 1.0975413409968773,
			"summary_team": 0.9749999999999999
		}]
	},
	"950280": {
		"nik": "950280",
		"nama": "RIZALDY MARTADIPUTRA",
		"band": "V",
		"posisi": "OFF 2 OM CME",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8632478632478621,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9899189918991913,
			"nilai_komparatif_unit": 0.9905203581513987,
			"summary_team": 0.8545454545454546
		}]
	},
	"950284": {
		"nik": "950284",
		"nama": "MAULANA PRAGNYA GHITA",
		"band": "V",
		"posisi": "OFF 2 OM TRANSPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.986013986013986,
			"nilai_komparatif_unit": 0.986612980012733,
			"summary_team": 0.8545454545454546
		}]
	},
	"950289": {
		"nik": "950289",
		"nama": "RIZKA MAULINA FIRDAUSI",
		"band": "V",
		"posisi": "OFF 2 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9814814814814815,
			"nilai_komparatif_unit": 0.9820777220273842,
			"summary_team": 0.8833333333333333
		}]
	},
	"950300": {
		"nik": "950300",
		"nama": "RISKI WAHYU HUTABRI",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ - INNER AREA MR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8512820512820516,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.939759036144578,
			"nilai_komparatif_unit": 0.940329930706829,
			"summary_team": 0.8
		}]
	},
	"950336": {
		"nik": "950336",
		"nama": "RAFIF TARUNA RAMADHAN",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.944055944055944,
			"nilai_komparatif_unit": 0.9446294489483613,
			"summary_team": 0.8181818181818182
		}]
	},
	"950389": {
		"nik": "950389",
		"nama": "ANDRIANI NASIR",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER DATA MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.017857142857143,
			"nilai_komparatif_unit": 1.0184754812669436,
			"summary_team": 0.95
		}]
	},
	"960005": {
		"nik": "960005",
		"nama": "MUSLIM KHADAVI",
		"band": "V",
		"posisi": "OFF 2 SECRETARY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8912280701754386,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0608446671438798,
			"nilai_komparatif_unit": 1.0614891200606085,
			"summary_team": 0.9454545454545454
		}]
	},
	"960007": {
		"nik": "960007",
		"nama": "BAGUS KURNIAWAN SUSANTO",
		"band": "V",
		"posisi": "OFF 2 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090908,
			"nilai_komparatif_unit": 1.091571807673662,
			"summary_team": 0.9272727272727272
		}]
	},
	"960015": {
		"nik": "960015",
		"nama": "TIKA PURI ARDIANTI",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300698,
			"nilai_komparatif_unit": 1.0705800421414762,
			"summary_team": 0.9272727272727272
		}]
	},
	"960016": {
		"nik": "960016",
		"nama": "MUHAMMAD YOSAN AL AZIM",
		"band": "V",
		"posisi": "OFF 2 ACCESS PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.888888888888889,
			"nilai_komparatif_unit": 0.8894288803266877,
			"summary_team": 0.8
		}]
	},
	"960021": {
		"nik": "960021",
		"nama": "WIRATAMA PUTRA PRATIKTA",
		"band": "V",
		"posisi": "OFF 2 BATTLE MANAGEMENT EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9055555555555556,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0239821528165085,
			"nilai_komparatif_unit": 1.0246042121108605,
			"summary_team": 0.9272727272727272
		}]
	},
	"960022": {
		"nik": "960022",
		"nama": "HILMAN RAMADHAN",
		"band": "V",
		"posisi": "OFF 2 ACCESS PROVISIONING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545454,
			"nilai_komparatif_unit": 1.0260774992132422,
			"summary_team": 0.8545454545454546
		}]
	},
	"960025": {
		"nik": "960025",
		"nama": "ANAK AGUNG NGURAH SURYA LAKSAMANA",
		"band": "V",
		"posisi": "OFF 2 COMMAND CONTROL MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0291595197255574,
			"nilai_komparatif_unit": 1.0297847242204359,
			"summary_team": 0.9090909090909092
		}]
	},
	"960026": {
		"nik": "960026",
		"nama": "FEBRIANA DWI LESTARI",
		"band": "V",
		"posisi": "OFF 2 DIGITAL SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8733333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9368494101318532,
			"nilai_komparatif_unit": 0.9374185371243284,
			"summary_team": 0.8181818181818182
		}]
	},
	"960030": {
		"nik": "960030",
		"nama": "RIO PRAKOSO WIBOWO",
		"band": "V",
		"posisi": "OFF 2 OM LOKASI TANJUNG SELOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8716049382716041,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9178470254957516,
			"nilai_komparatif_unit": 0.9184046087226004,
			"summary_team": 0.8
		}]
	},
	"960031": {
		"nik": "960031",
		"nama": "DARIANT DEO WIJAYA",
		"band": "V",
		"posisi": "OFF 2 CCAN FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0076047455449186,
			"summary_team": 0.8727272727272727
		}]
	},
	"960032": {
		"nik": "960032",
		"nama": "SYAIF MUHAMI SHIDIQ",
		"band": "V",
		"posisi": "OFF 2 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9801869293396148,
			"summary_team": 0.8
		}]
	},
	"960045": {
		"nik": "960045",
		"nama": "NARULITA FUBIAN",
		"band": "V",
		"posisi": "OFF 2 ACCESS DATA & INVENTORY MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0449927431059507,
			"nilai_komparatif_unit": 1.0456275661315195,
			"summary_team": 0.9230769230769231
		}]
	},
	"960047": {
		"nik": "960047",
		"nama": "FRISKARINE GITY CLADELLA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8294117647058812,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.117988394584141,
			"nilai_komparatif_unit": 1.1186675617648538,
			"summary_team": 0.9272727272727272
		}]
	},
	"960050": {
		"nik": "960050",
		"nama": "ANDRE RIZAL SINAGA",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ-OUTER AREA JR 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9111111111111114,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8780487804878047,
			"nilai_komparatif_unit": 0.8785821866641668,
			"summary_team": 0.8
		}]
	},
	"960051": {
		"nik": "960051",
		"nama": "REYNALDO JOHANES",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8294117647058812,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0303030303030318,
			"nilai_komparatif_unit": 1.0309289294695712,
			"summary_team": 0.8545454545454546
		}]
	},
	"960056": {
		"nik": "960056",
		"nama": "RULLY ZULFA ASTUTI",
		"band": "V",
		"posisi": "OFF 2 HR SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8299999999999994,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9857612267250829,
			"nilai_komparatif_unit": 0.9863600671749966,
			"summary_team": 0.8181818181818182
		}]
	},
	"960065": {
		"nik": "960065",
		"nama": "AMRIZAL KARIM AMRULLOH",
		"band": "V",
		"posisi": "OFF 2 ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8571428571428571,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9333333333333335,
			"nilai_komparatif_unit": 0.9339003243430221,
			"summary_team": 0.8
		}]
	},
	"960075": {
		"nik": "960075",
		"nama": "M. IVAN FADILLAH",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9843750000000001,
			"nilai_komparatif_unit": 0.9849729983305311,
			"summary_team": 0.8750000000000002
		}]
	},
	"960079": {
		"nik": "960079",
		"nama": "HAFIZH",
		"band": "V",
		"posisi": "OFF 2 CONSTRUCTION SUPERVISION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9519230769230771,
			"nilai_komparatif_unit": 0.9525013610229313,
			"summary_team": 0.8250000000000002
		}]
	},
	"960103": {
		"nik": "960103",
		"nama": "NIKEN SALMA NABILA",
		"band": "V",
		"posisi": "OFF 2 LOGISTIK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0384615384615383,
			"nilai_komparatif_unit": 1.0390923938431973,
			"summary_team": 0.9000000000000001
		}]
	},
	"960112": {
		"nik": "960112",
		"nama": "ULFAH ANDARYANI",
		"band": "V",
		"posisi": "OFF 2 PROCUREMENT & PROCESS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8912280701754386,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0608446671438798,
			"nilai_komparatif_unit": 1.0614891200606085,
			"summary_team": 0.9454545454545454
		}]
	},
	"960116": {
		"nik": "960116",
		"nama": "GUNTUR WAHYU SEJATI",
		"band": "V",
		"posisi": "OFF 2 SURVEILLANCE & LOGIC NE ACCESS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9899999999999997,
			"nilai_komparatif_unit": 0.990601415463848,
			"summary_team": 0.825
		}]
	},
	"960146": {
		"nik": "960146",
		"nama": "WILLIAM ARIEL YOSIA BUNGA",
		"band": "V",
		"posisi": "OFF 2 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0085763293310461,
			"nilai_komparatif_unit": 1.009189029736027,
			"summary_team": 0.8909090909090909
		}]
	},
	"960177": {
		"nik": "960177",
		"nama": "MUHAMMAD BAGUS SAMUDRA",
		"band": "V",
		"posisi": "SPV SITE OPERATION & POJ SINGKAWANG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8250000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0358126721763083,
			"nilai_komparatif_unit": 1.0364419183972142,
			"summary_team": 0.8545454545454546
		}]
	},
	"965251": {
		"nik": "965251",
		"nama": "JEKY CENIAGO",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8547008547008538,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9998181818181829,
			"nilai_komparatif_unit": 1.0004255617329123,
			"summary_team": 0.8545454545454546
		}]
	},
	"970003": {
		"nik": "970003",
		"nama": "MOHAMAD DIKA ARZALDI",
		"band": "V",
		"posisi": "OFF 2 ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8277777777777782,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9664429530201338,
			"nilai_komparatif_unit": 0.9670300578048545,
			"summary_team": 0.8
		}]
	},
	"970005": {
		"nik": "970005",
		"nama": "MUHAMMAD IKHSAN ALRIZAL",
		"band": "V",
		"posisi": "OFF 2 CASH BANK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0472727272727271,
			"nilai_komparatif_unit": 1.0479089353667155,
			"summary_team": 0.8727272727272727
		}]
	},
	"970006": {
		"nik": "970006",
		"nama": "MADE KEVIN BRATAWISNU",
		"band": "V",
		"posisi": "OFF 2 ACCESS PARTNERSHIP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365347,
			"nilai_komparatif_unit": 0.988593335251618,
			"summary_team": 0.8727272727272727
		}]
	},
	"980002": {
		"nik": "980002",
		"nama": "YOGI MAHENDRA WENDY WIRATAMA",
		"band": "V",
		"posisi": "OFF 2 QOS, SLG & DATA CUSTOMER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8844444444444443,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.986751941525811,
			"nilai_komparatif_unit": 0.9873513838254231,
			"summary_team": 0.8727272727272727
		}]
	},
	"980003": {
		"nik": "980003",
		"nama": "AZARIA ZADA NOORDIKA",
		"band": "V",
		"posisi": "OFF 2 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300696,
			"nilai_komparatif_unit": 1.070580042141476,
			"summary_team": 0.9272727272727272
		}]
	}
};