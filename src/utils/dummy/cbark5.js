export const cbark5 = {
	"650547": {
		"nik": "650547",
		"nama": "SYAIFUL",
		"band": "III",
		"posisi": "MGR COMMUNITY & TERRITORY MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.880392156862744,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9621380846325178,
			"nilai_komparatif_unit": 0.9672365858405991,
			"summary_team": 0.8470588235294116
		}]
	},
	"651256": {
		"nik": "651256",
		"nama": "IMAM ZAHIR",
		"band": "III",
		"posisi": "SENIOR EXPERT PROJECT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8141843971631193,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8932530883750409,
			"nilai_komparatif_unit": 0.8979865585732838,
			"summary_team": 0.7272727272727272
		}]
	},
	"660152": {
		"nik": "660152",
		"nama": "BAMBANG ACHIRANTO",
		"band": "III",
		"posisi": "SO SYNERGY INTEGRTN & IMPLEMENTATION SOE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.88695652173913,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9771241830065364,
			"nilai_komparatif_unit": 0.982302097598086,
			"summary_team": 0.8666666666666666
		}]
	},
	"660190": {
		"nik": "660190",
		"nama": "HERI IKHWAN DIANA",
		"band": "III",
		"posisi": "MGR ENT PROJECT COMPLIANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666663,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.976331360946746,
			"nilai_komparatif_unit": 0.981505074265844,
			"summary_team": 0.8461538461538461
		}]
	},
	"660282": {
		"nik": "660282",
		"nama": "TASMIN",
		"band": "III",
		"posisi": "AVP TAX MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8307692307692298,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9771241830065374,
			"nilai_komparatif_unit": 0.982302097598087,
			"summary_team": 0.8117647058823532
		}]
	},
	"660318": {
		"nik": "660318",
		"nama": "NAZIRWAN, ST. M.KOM",
		"band": "III",
		"posisi": "SENIOR EXPERT PROJECT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8141843971631193,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9155844155844169,
			"nilai_komparatif_unit": 0.9204362225376159,
			"summary_team": 0.7454545454545454
		}]
	},
	"660362": {
		"nik": "660362",
		"nama": "HAMBALI",
		"band": "III",
		"posisi": "MGR REVENUE ASSURANCE & COMPLIANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8644444444444436,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0796915167095125,
			"nilai_komparatif_unit": 1.0854129496204663,
			"summary_team": 0.9333333333333332
		}]
	},
	"660379": {
		"nik": "660379",
		"nama": "Hadi Waluyo",
		"band": "III",
		"posisi": "GM SALES DOMESTIC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769231,
			"nilai_komparatif_unit": 0.9279684338513431,
			"summary_team": 0.8000000000000002
		}]
	},
	"660413": {
		"nik": "660413",
		"nama": "I GDE WIRAYASA",
		"band": "III",
		"posisi": "HEAD OF SBY ENTERPRISE DATA CENTER MGMT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8142857142857147,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9969040247678014,
			"nilai_komparatif_unit": 1.0021867554442,
			"summary_team": 0.8117647058823529
		}]
	},
	"660451": {
		"nik": "660451",
		"nama": "EKO PRIHARTONO",
		"band": "III",
		"posisi": "SENIOR EXPERT PROJECT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8141843971631193,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8039277795375369,
			"nilai_komparatif_unit": 0.8081879027159555,
			"summary_team": 0.6545454545454545
		}]
	},
	"660499": {
		"nik": "660499",
		"nama": "AGUS SOLEH",
		"band": "III",
		"posisi": "MGR PROJECT SERVICE DELIVERY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8862745098039212,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1109598366235538,
			"nilai_komparatif_unit": 1.116846964635245,
			"summary_team": 0.9846153846153846
		}]
	},
	"670046": {
		"nik": "670046",
		"nama": "AHMAD SUPRIYONO",
		"band": "III",
		"posisi": "MGR PROCUREMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564102564102558,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0119760479041924,
			"nilai_komparatif_unit": 1.0173386472911188,
			"summary_team": 0.8666666666666667
		}]
	},
	"670076": {
		"nik": "670076",
		"nama": "SUMARNO",
		"band": "III",
		"posisi": "AVP BILLING,COLLECTION & DEBT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8307692307692298,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.038194444444446,
			"nilai_komparatif_unit": 1.0436959786979674,
			"summary_team": 0.8625000000000003
		}]
	},
	"670104": {
		"nik": "670104",
		"nama": "MUKHLIS",
		"band": "III",
		"posisi": "MGR GOVERNMENT SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9208333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0280542986425347,
			"nilai_komparatif_unit": 1.033502098877575,
			"summary_team": 0.9466666666666665
		}]
	},
	"670176": {
		"nik": "670176",
		"nama": "WAHYUDI JATMIKO",
		"band": "III",
		"posisi": "MGR INCIDENT MGT TOP 20 & PUBLIC SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666658,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.029585798816569,
			"nilai_komparatif_unit": 1.0350417146803452,
			"summary_team": 0.8923076923076924
		}]
	},
	"670191": {
		"nik": "670191",
		"nama": "Titok Rulianto",
		"band": "III",
		"posisi": "AVP PROCUREMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0036363636363639,
			"nilai_komparatif_unit": 1.0089547698965513,
			"summary_team": 0.9200000000000002
		}]
	},
	"670257": {
		"nik": "670257",
		"nama": "SLAMET SUDIYONO, IR, M.Si",
		"band": "III",
		"posisi": "MGR SERVICE MIGRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8571428571428565,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.933333333333334,
			"nilai_komparatif_unit": 0.9382791942274697,
			"summary_team": 0.8
		}]
	},
	"670272": {
		"nik": "670272",
		"nama": "Sudirman",
		"band": "III",
		"posisi": "AVP BILLING & REVENUE ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9222222222222223,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0120481927710843,
			"nilai_komparatif_unit": 1.0174111744635206,
			"summary_team": 0.9333333333333333
		}]
	},
	"670274": {
		"nik": "670274",
		"nama": "SURYENI,S.KOMP",
		"band": "III",
		"posisi": "MGR IT SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8358974358974345,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0208588957055231,
			"nilai_komparatif_unit": 1.026268566516988,
			"summary_team": 0.8533333333333333
		}]
	},
	"670300": {
		"nik": "670300",
		"nama": "DEDY SUYANTO BUDIARTO",
		"band": "III",
		"posisi": "SO ACCESS NETWORK STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8688888888888879,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9718670076726355,
			"nilai_komparatif_unit": 0.9770170637735806,
			"summary_team": 0.8444444444444446
		}]
	},
	"670319": {
		"nik": "670319",
		"nama": "AMRIZAL M",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666665,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559442,
			"nilai_komparatif_unit": 0.9490586255297827,
			"summary_team": 0.8181818181818181
		}]
	},
	"670397": {
		"nik": "670397",
		"nama": "MUCHLIS RIYONO",
		"band": "III",
		"posisi": "MGR LEGAL & COMPLIANCE 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8864197530864188,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9025069637883018,
			"nilai_komparatif_unit": 0.9072894715371078,
			"summary_team": 0.8
		}]
	},
	"670435": {
		"nik": "670435",
		"nama": "ADRID",
		"band": "III",
		"posisi": "SENIOR EXPERT PROJECT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8141843971631193,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8039277795375369,
			"nilai_komparatif_unit": 0.8081879027159555,
			"summary_team": 0.6545454545454545
		}]
	},
	"670454": {
		"nik": "670454",
		"nama": "MARTALENA SEBAYANG",
		"band": "III",
		"posisi": "GM BUMN SALES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.84,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9920634920634923,
			"nilai_komparatif_unit": 0.9973205720955243,
			"summary_team": 0.8333333333333335
		}]
	},
	"670542": {
		"nik": "670542",
		"nama": "IRWANDI",
		"band": "III",
		"posisi": "MGR PROJECT RESOURCE & MONITORING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8141843971631193,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9989547038327543,
			"nilai_komparatif_unit": 1.0042483013377894,
			"summary_team": 0.8133333333333335
		}]
	},
	"670615": {
		"nik": "670615",
		"nama": "MUHAMMAD MASYHUDI",
		"band": "III",
		"posisi": "MGR MANAGED SERVICE COMPLAINT HANDLING-2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8444444444444446,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.781578947368421,
			"nilai_komparatif_unit": 0.7857206410307095,
			"summary_team": 0.66
		}]
	},
	"680013": {
		"nik": "680013",
		"nama": "LILIS HARTINI",
		"band": "III",
		"posisi": "MGR TERRITORY SALES AREA TIMUR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8747474747474735,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0376621069461731,
			"nilai_komparatif_unit": 1.0431608202705356,
			"summary_team": 0.9076923076923077
		}]
	},
	"680029": {
		"nik": "680029",
		"nama": "Hairul",
		"band": "III",
		"posisi": "GM AREA KBI (SMTRA,JAWA,BALI&KALIMANTAN)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555556,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.018552875695733,
			"nilai_komparatif_unit": 1.023950326591997,
			"summary_team": 0.8714285714285716
		}]
	},
	"680054": {
		"nik": "680054",
		"nama": "MUHAMAD FEBRIANTO",
		"band": "III",
		"posisi": "MGR SOLUTION REQUIREMENT & ADM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8699999999999994,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9902740937223703,
			"nilai_komparatif_unit": 0.9955216914880315,
			"summary_team": 0.8615384615384616
		}]
	},
	"680082": {
		"nik": "680082",
		"nama": "BERILIANTO, IR",
		"band": "III",
		"posisi": "SO SYNERGY INTEG&IMPLEMENT CROSS CFU/FU",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8121212121212125,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0179104477611935,
			"nilai_komparatif_unit": 1.0233044943440301,
			"summary_team": 0.8266666666666667
		}]
	},
	"680180": {
		"nik": "680180",
		"nama": "HARYADI HAMONANGAN L.TOBING, IR.MT.",
		"band": "III",
		"posisi": "SO CONSUMER STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8527777777777769,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9519064954972227,
			"nilai_komparatif_unit": 0.9569507781161014,
			"summary_team": 0.8117647058823529
		}]
	},
	"680273": {
		"nik": "680273",
		"nama": "HIDAYAT",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666665,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300702,
			"nilai_komparatif_unit": 1.0755997756004205,
			"summary_team": 0.9272727272727272
		}]
	},
	"680400": {
		"nik": "680400",
		"nama": "WASIYAR",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9208333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9872480460715765,
			"nilai_komparatif_unit": 0.9924796083971593,
			"summary_team": 0.9090909090909092
		}]
	},
	"680588": {
		"nik": "680588",
		"nama": "DODI NUGROHO",
		"band": "III",
		"posisi": "MGR PROJECT HANDOVER MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666663,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930075,
			"nilai_komparatif_unit": 1.012329200565102,
			"summary_team": 0.8727272727272727
		}]
	},
	"690036": {
		"nik": "690036",
		"nama": "RITA GUNAWATI",
		"band": "III",
		"posisi": "SO RESOURCE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.859259259259259,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9721095334685603,
			"nilai_komparatif_unit": 0.9772608747468446,
			"summary_team": 0.835294117647059
		}]
	},
	"690087": {
		"nik": "690087",
		"nama": "CHITRA NOVARINA",
		"band": "III",
		"posisi": "MGR GOVERNMENT SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666665,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0307692307692309,
			"nilai_komparatif_unit": 1.0362314178006664,
			"summary_team": 0.8933333333333333
		}]
	},
	"690318": {
		"nik": "690318",
		"nama": "BUDIYONO",
		"band": "III",
		"posisi": "SENIOR STAFF III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.880392156862744,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9621380846325178,
			"nilai_komparatif_unit": 0.9672365858405991,
			"summary_team": 0.8470588235294116
		}]
	},
	"690340": {
		"nik": "690340",
		"nama": "DEDEN RAHMAWAN",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8612244897959169,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0555794915984509,
			"nilai_komparatif_unit": 1.0611731515928957,
			"summary_team": 0.9090909090909092
		}]
	},
	"690614": {
		"nik": "690614",
		"nama": "AHMAD FIRDAUS THAHIR",
		"band": "III",
		"posisi": "SO SATELIT & WIRELESS STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8688888888888879,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9207161125319704,
			"nilai_komparatif_unit": 0.9255951130486553,
			"summary_team": 0.8
		}]
	},
	"700052": {
		"nik": "700052",
		"nama": "DESSY SAHARINI",
		"band": "III",
		"posisi": "MGR SECRETARIATE & RESOURCE OPTIMATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8972972972972965,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9833451452870315,
			"nilai_komparatif_unit": 0.9885560256079386,
			"summary_team": 0.8823529411764707
		}]
	},
	"700168": {
		"nik": "700168",
		"nama": "ACHMAD IRFAN",
		"band": "III",
		"posisi": "MGR OUTBOUND LOGISTIC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9662337662337673,
			"nilai_komparatif_unit": 0.9713539710184199,
			"summary_team": 0.8266666666666667
		}]
	},
	"700224": {
		"nik": "700224",
		"nama": "DJAWOTO MULYODIONO, ST, MM",
		"band": "III",
		"posisi": "SO INTEGRATED MGT SYSTEM & RISK MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7866666666666671,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9661016949152537,
			"nilai_komparatif_unit": 0.9712211998359389,
			"summary_team": 0.7599999999999999
		}]
	},
	"700240": {
		"nik": "700240",
		"nama": "IWAN PURNAMA",
		"band": "III",
		"posisi": "MGR PROPOSAL EDITOR & SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.836363636363636,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9884057971014497,
			"nilai_komparatif_unit": 0.9936434945079723,
			"summary_team": 0.8266666666666667
		}]
	},
	"700371": {
		"nik": "700371",
		"nama": "SYARIFFUDIN",
		"band": "III",
		"posisi": "MGR BUSINESS MODEL ENGINEERING & CUST EN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8399999999999989,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9943977591036427,
			"nilai_komparatif_unit": 0.9996672087357501,
			"summary_team": 0.8352941176470587
		}]
	},
	"700392": {
		"nik": "700392",
		"nama": "LIES DIARAWATI",
		"band": "III",
		"posisi": "SO SYNERGY GENERAL ADMINISTRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.88695652173913,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9771241830065364,
			"nilai_komparatif_unit": 0.982302097598086,
			"summary_team": 0.8666666666666666
		}]
	},
	"700659": {
		"nik": "700659",
		"nama": "ADY PRANATA E.S. PANJAITAN, ST",
		"band": "III",
		"posisi": "MGR PLANNING & PERFORMANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8758620689655164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9286089238845154,
			"nilai_komparatif_unit": 0.933529749487286,
			"summary_team": 0.8133333333333335
		}]
	},
	"710002": {
		"nik": "710002",
		"nama": "KUSRIYATI PURWANINGSIH",
		"band": "III",
		"posisi": "MGR TRANSACTION VALIDATION 3",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740751,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9754683318465643,
			"nilai_komparatif_unit": 0.9806374718565083,
			"summary_team": 0.8526315789473684
		}]
	},
	"710008": {
		"nik": "710008",
		"nama": "EKA RAHARDJA ZAIBI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666665,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300702,
			"nilai_komparatif_unit": 1.0755997756004205,
			"summary_team": 0.9272727272727272
		}]
	},
	"710011": {
		"nik": "710011",
		"nama": "PURWANTO",
		"band": "III",
		"posisi": "MGR SALES PLAN & OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8266666666666684,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0967741935483848,
			"nilai_komparatif_unit": 1.1025861498986365,
			"summary_team": 0.9066666666666666
		}]
	},
	"710098": {
		"nik": "710098",
		"nama": "MOHAMAD DARMAWI",
		"band": "III",
		"posisi": "MGR PROGRAM & OPERATIONS SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7863636363636347,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9977767896843062,
			"nilai_komparatif_unit": 1.0030641452612803,
			"summary_team": 0.7846153846153846
		}]
	},
	"710106": {
		"nik": "710106",
		"nama": "AGUS NAWAN",
		"band": "III",
		"posisi": "MGR ACCOUNT DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8972972972972965,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9510040160642579,
			"nilai_komparatif_unit": 0.9560435163212774,
			"summary_team": 0.8533333333333333
		}]
	},
	"710170": {
		"nik": "710170",
		"nama": "HARI MULYA",
		"band": "III",
		"posisi": "SENIOR EXPERT PROJECT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8141843971631193,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.960247070003169,
			"nilai_komparatif_unit": 0.9653355504662802,
			"summary_team": 0.7818181818181817
		}]
	},
	"710256": {
		"nik": "710256",
		"nama": "DENI HANDHOKO",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8266666666666684,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.024667931688802,
			"nilai_komparatif_unit": 1.030097787102532,
			"summary_team": 0.8470588235294116
		}]
	},
	"710352": {
		"nik": "710352",
		"nama": "HASUNA SAIDA",
		"band": "III",
		"posisi": "SO GENERAL AFFAIR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8699999999999994,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0180623973727427,
			"nilai_komparatif_unit": 1.0234572491573384,
			"summary_team": 0.8857142857142856
		}]
	},
	"710392": {
		"nik": "710392",
		"nama": "Ni Ketut Sukantini",
		"band": "III",
		"posisi": "GM SATELLITE PERFORMNCE REPORT&SUPP MGMT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740733,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9295550847457635,
			"nilai_komparatif_unit": 0.9344809241842519,
			"summary_team": 0.8125
		}]
	},
	"710415": {
		"nik": "710415",
		"nama": "ERWIN PIETER TARIGAN",
		"band": "III",
		"posisi": "SO COMM & WIFI CONNECTIVITY TARIFF STRA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8222222222222211,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8871224165341826,
			"nilai_komparatif_unit": 0.8918233994644478,
			"summary_team": 0.7294117647058824
		}]
	},
	"710416": {
		"nik": "710416",
		"nama": "Agus Susanto",
		"band": "III",
		"posisi": "GM SATELLITE DATA ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8800000000000003,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9917355371900822,
			"nilai_komparatif_unit": 0.9969908793444177,
			"summary_team": 0.8727272727272727
		}]
	},
	"710423": {
		"nik": "710423",
		"nama": "RIZAL FAISAL",
		"band": "III",
		"posisi": "SO OUTBOUND LOGISTIC MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7888888888888888,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0140845070422537,
			"nilai_komparatif_unit": 1.0194582794423206,
			"summary_team": 0.7999999999999999
		}]
	},
	"710441": {
		"nik": "710441",
		"nama": "HENDRY ZULFIKAR, ST",
		"band": "III",
		"posisi": "MANAGER SEGMENT 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8562770562770574,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9809908998988865,
			"nilai_komparatif_unit": 0.9861893047517218,
			"summary_team": 0.8400000000000001
		}]
	},
	"710443": {
		"nik": "710443",
		"nama": "EDY TRISUWITO",
		"band": "III",
		"posisi": "MGR MARKETING STRATEGY & OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8266666666666684,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999979,
			"nilai_komparatif_unit": 1.005299136672286,
			"summary_team": 0.8266666666666667
		}]
	},
	"710466": {
		"nik": "710466",
		"nama": "NURUL HIDAYATI",
		"band": "III",
		"posisi": "MGR PROJECT SLA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666663,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0473372781065093,
			"nilai_komparatif_unit": 1.052887261485178,
			"summary_team": 0.9076923076923077
		}]
	},
	"710468": {
		"nik": "710468",
		"nama": "AGUNG LAKSMANA ADI",
		"band": "III",
		"posisi": "MGR DELIVERY & INTEGRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8571428571428565,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9916666666666675,
			"nilai_komparatif_unit": 0.9969216438666867,
			"summary_team": 0.8500000000000001
		}]
	},
	"710481": {
		"nik": "710481",
		"nama": "KURNIAWAN",
		"band": "III",
		"posisi": "MGR QUALITY & ASSET MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564102564102558,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1137724550898211,
			"nilai_komparatif_unit": 1.1196744875511722,
			"summary_team": 0.9538461538461538
		}]
	},
	"710497": {
		"nik": "710497",
		"nama": "SIGIT TRINARTO",
		"band": "III",
		"posisi": "SO SYNERGY PLANNING CROSS CFU & FU",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9771428571428566,
			"nilai_komparatif_unit": 0.9823208706912069,
			"summary_team": 0.8142857142857142
		}]
	},
	"710507": {
		"nik": "710507",
		"nama": "DAVID YUDHA SAPUTRA, S.IP",
		"band": "III",
		"posisi": "MGR TERRITORY SALES AREA BARAT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8747474747474735,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0728370936223146,
			"nilai_komparatif_unit": 1.0785222040085198,
			"summary_team": 0.9384615384615385
		}]
	},
	"710526": {
		"nik": "710526",
		"nama": "JONI ANSAH",
		"band": "III",
		"posisi": "SO PORTFOLIO EVALUATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8777777777777771,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.025316455696203,
			"nilai_komparatif_unit": 1.0307497477272836,
			"summary_team": 0.8999999999999999
		}]
	},
	"720115": {
		"nik": "720115",
		"nama": "JATI SETIAWAN D",
		"band": "III",
		"posisi": "SO INTERNET CONNECTIVITY TARIFF STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8222222222222211,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9729729729729744,
			"nilai_komparatif_unit": 0.9781288897352007,
			"summary_team": 0.8
		}]
	},
	"720130": {
		"nik": "720130",
		"nama": "GORDON PAULUS HUTAURUK",
		"band": "III",
		"posisi": "SENIOR ADVISOR III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8266666666666684,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.209677419354836,
			"nilai_komparatif_unit": 1.2160876653293784,
			"summary_team": 1
		}]
	},
	"720161": {
		"nik": "720161",
		"nama": "Philipus Nanang Harjendra S.",
		"band": "III",
		"posisi": "AVP PROJECT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8600000000000002,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999999,
			"nilai_komparatif_unit": 1.005299136672288,
			"summary_team": 0.8600000000000001
		}]
	},
	"720166": {
		"nik": "720166",
		"nama": "MANGAPUL PANGARIBUAN",
		"band": "III",
		"posisi": "MGR CUSTOMER ANALYSIS & SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8399999999999989,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9206349206349219,
			"nilai_komparatif_unit": 0.9255134909046476,
			"summary_team": 0.7733333333333333
		}]
	},
	"720182": {
		"nik": "720182",
		"nama": "BUDI SUTRISNO",
		"band": "III",
		"posisi": "MGR GOVERNMENT SALES 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9208333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9477581242287133,
			"nilai_komparatif_unit": 0.9527804240612728,
			"summary_team": 0.8727272727272727
		}]
	},
	"720190": {
		"nik": "720190",
		"nama": "YULIA YUSMAL",
		"band": "III",
		"posisi": "MGR OUTBOUND LOGISTIC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8699999999999994,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9372236958443861,
			"nilai_komparatif_unit": 0.9421901723011726,
			"summary_team": 0.8153846153846154
		}]
	},
	"720193": {
		"nik": "720193",
		"nama": "Ricky Kusnandar",
		"band": "III",
		"posisi": "AVP PRODUCT DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8600000000000002,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0232558139534882,
			"nilai_komparatif_unit": 1.0286781863623413,
			"summary_team": 0.88
		}]
	},
	"720195": {
		"nik": "720195",
		"nama": "TRIBHUWANA PRIHANDONO LINGGARTO, ST",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666665,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769234,
			"nilai_komparatif_unit": 0.9279684338513433,
			"summary_team": 0.8
		}]
	},
	"720198": {
		"nik": "720198",
		"nama": "AGUS HARYO WINARDI",
		"band": "III",
		"posisi": "MGR COMMERCIAL MANAGEMENT - 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.836363636363636,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0117056856187296,
			"nilai_komparatif_unit": 1.0170668523189543,
			"summary_team": 0.8461538461538461
		}]
	},
	"720226": {
		"nik": "720226",
		"nama": "RIANA WIHARDEWI",
		"band": "III",
		"posisi": "MGR CUSTOMER ANALYSIS & SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.880392156862744,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.075278396436527,
			"nilai_komparatif_unit": 1.080976443620003,
			"summary_team": 0.9466666666666665
		}]
	},
	"720234": {
		"nik": "720234",
		"nama": "Selamet Joelianto Mollijono",
		"band": "III",
		"posisi": "AVP CAPACITY PLANNING & MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7962962962962964,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0046511627906975,
			"nilai_komparatif_unit": 1.0099749466102987,
			"summary_team": 0.7999999999999999
		}]
	},
	"720243": {
		"nik": "720243",
		"nama": "GEGET IWAN ERYANTO",
		"band": "III",
		"posisi": "GHO BUSS EFFECTIVENESS & QUALITY MGMT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0114285714285713,
			"nilai_komparatif_unit": 1.0167882696628285,
			"summary_team": 0.8428571428571429
		}]
	},
	"720258": {
		"nik": "720258",
		"nama": "AAS HARISTMAYA, ST",
		"band": "III",
		"posisi": "MGR PROJECT RESOURCE & MONITORING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8571428571428565,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0266666666666673,
			"nilai_komparatif_unit": 1.0321071136502165,
			"summary_team": 0.8799999999999999
		}]
	},
	"720277": {
		"nik": "720277",
		"nama": "DODDY HADI RUKMANA, ST",
		"band": "III",
		"posisi": "MGR IT PLANNING & PORTFOLIO MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8542635658914716,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0242740471869345,
			"nilai_komparatif_unit": 1.0297018153528559,
			"summary_team": 0.8750000000000001
		}]
	},
	"720293": {
		"nik": "720293",
		"nama": "LESTARI DAMAYANTI",
		"band": "III",
		"posisi": "SO SUBSIDIARIES REVENUE ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8571428571428567,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000004,
			"nilai_komparatif_unit": 1.0052991366722888,
			"summary_team": 0.857142857142857
		}]
	},
	"720296": {
		"nik": "720296",
		"nama": "SUMARDIONO",
		"band": "III",
		"posisi": "SENIOR GOVERNMENT RELATION OFFICER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7863636363636347,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.01734104046243,
			"nilai_komparatif_unit": 1.0227320696781683,
			"summary_team": 0.8
		}]
	},
	"720319": {
		"nik": "720319",
		"nama": "DICKY SOPHIANTO",
		"band": "III",
		"posisi": "SO CONS PORTFOLIO EVALUATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8086253369272236,
			"nilai_komparatif_unit": 0.8129103531042761,
			"summary_team": 0.7142857142857142
		}]
	},
	"720320": {
		"nik": "720320",
		"nama": "Safaruddin",
		"band": "III",
		"posisi": "AVP BUSINESS PORTFOLIO STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346935,
			"nilai_komparatif_unit": 0.9847828277606085,
			"summary_team": 0.8
		}]
	},
	"720321": {
		"nik": "720321",
		"nama": "IRFAN NUR",
		"band": "III",
		"posisi": "HEAD OF IT SECURITY OPERATION MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8142857142857147,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0257997936016507,
			"nilai_komparatif_unit": 1.031235646906351,
			"summary_team": 0.835294117647059
		}]
	},
	"720326": {
		"nik": "720326",
		"nama": "HAFIZD",
		"band": "III",
		"posisi": "MGR SALES - 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8399999999999989,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0952380952380967,
			"nilai_komparatif_unit": 1.10104191159346,
			"summary_team": 0.9199999999999999
		}]
	},
	"720333": {
		"nik": "720333",
		"nama": "ERIC MAURITZ H.L. TOBING",
		"band": "III",
		"posisi": "MGR BUSINESS CUSTOMER RM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8266666666666684,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9516129032258044,
			"nilai_komparatif_unit": 0.9566556300591109,
			"summary_team": 0.7866666666666666
		}]
	},
	"720343": {
		"nik": "720343",
		"nama": "HADI TRIYONO, ST",
		"band": "III",
		"posisi": "SENIOR EXPERT PROJECT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8141843971631193,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8932530883750409,
			"nilai_komparatif_unit": 0.8979865585732838,
			"summary_team": 0.7272727272727272
		}]
	},
	"720350": {
		"nik": "720350",
		"nama": "BUDI PRASETYO",
		"band": "III",
		"posisi": "MGR SALES - 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8266666666666684,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0483870967741913,
			"nilai_komparatif_unit": 1.0539426432854613,
			"summary_team": 0.8666666666666667
		}]
	},
	"720381": {
		"nik": "720381",
		"nama": "BUDI TEGUH PRAKOSO",
		"band": "III",
		"posisi": "SO REVENUE ASSURANCE&DELIVERY MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7888888888888888,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0865191146881288,
			"nilai_komparatif_unit": 1.0922767279739147,
			"summary_team": 0.857142857142857
		}]
	},
	"720387": {
		"nik": "720387",
		"nama": "EDY RAHADI",
		"band": "III",
		"posisi": "MGR SOLUTION DESIGN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.981818181818183,
			"nilai_komparatif_unit": 0.9870209705509752,
			"summary_team": 0.8400000000000001
		}]
	},
	"720394": {
		"nik": "720394",
		"nama": "PAULUS WIBOWO SURJO LAKSONO",
		"band": "III",
		"posisi": "MANAGER SEGMENT 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8562770562770574,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0203288808472128,
			"nilai_komparatif_unit": 1.0257357430375051,
			"summary_team": 0.8736842105263158
		}]
	},
	"720409": {
		"nik": "720409",
		"nama": "I KETUT SUWETJA DARSANA, MBA",
		"band": "III",
		"posisi": "SO CAPEX POLICY & ALIGNMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.846666666666667,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.070866141732283,
			"nilai_komparatif_unit": 1.0765408077750482,
			"summary_team": 0.9066666666666666
		}]
	},
	"720433": {
		"nik": "720433",
		"nama": "TEGUH BUDI KARYONO",
		"band": "III",
		"posisi": "MGR GOVERNMENT SALES 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8612244897959169,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9289099526066367,
			"nilai_komparatif_unit": 0.9338323734017481,
			"summary_team": 0.8
		}]
	},
	"720451": {
		"nik": "720451",
		"nama": "GITO SUKMONO",
		"band": "III",
		"posisi": "SO CORE NETWORK & TRANSPORT SERVICE OPT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.866666666666666,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9502262443438919,
			"nilai_komparatif_unit": 0.9552616230822654,
			"summary_team": 0.8235294117647057
		}]
	},
	"720467": {
		"nik": "720467",
		"nama": "EKO BUDI SUSIANTO",
		"band": "III",
		"posisi": "MGR GOVERNMENT SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8747474747474735,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9755196304849899,
			"nilai_komparatif_unit": 0.98068904233343,
			"summary_team": 0.8533333333333334
		}]
	},
	"720484": {
		"nik": "720484",
		"nama": "AGUNG CAHYONO RAHARJO",
		"band": "III",
		"posisi": "MGR  IT COMPETENCY MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564102564102558,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.000855431993157,
			"nilai_komparatif_unit": 1.0061591017164908,
			"summary_team": 0.857142857142857
		}]
	},
	"720493": {
		"nik": "720493",
		"nama": "CHRISTIANUS KARTIKO P.",
		"band": "III",
		"posisi": "VP CORPORATE SECRETARY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.805555555555556,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9221674876847287,
			"nilai_komparatif_unit": 0.9270541792367107,
			"summary_team": 0.742857142857143
		}]
	},
	"720497": {
		"nik": "720497",
		"nama": "PURWANTO",
		"band": "III",
		"posisi": "MGR DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8758620689655164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0199475065616808,
			"nilai_komparatif_unit": 1.0253523477975108,
			"summary_team": 0.8933333333333333
		}]
	},
	"720520": {
		"nik": "720520",
		"nama": "LELY MELIA KANIAWATI",
		"band": "III",
		"posisi": "MGR PROGRAM MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8266666666666668,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.005299136672288,
			"summary_team": 0.8266666666666665
		}]
	},
	"720548": {
		"nik": "720548",
		"nama": "ARDIAN GAGULA",
		"band": "III",
		"posisi": "VP PROJECT SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9403508771929822,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9216417910447762,
			"nilai_komparatif_unit": 0.926525696858415,
			"summary_team": 0.8666666666666665
		}]
	},
	"720566": {
		"nik": "720566",
		"nama": "KAREL PURBA",
		"band": "III",
		"posisi": "MGR SERVICE LEVEL GUARANTEE - 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7766666666666662,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9508088478045567,
			"nilai_komparatif_unit": 0.9558473138382939,
			"summary_team": 0.7384615384615385
		}]
	},
	"720577": {
		"nik": "720577",
		"nama": "GIRI AGUNG PRASTAWA",
		"band": "III",
		"posisi": "MGR LEGAL & COMPLIANCE - 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8282051282051274,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9287925696594437,
			"nilai_komparatif_unit": 0.9337143684262749,
			"summary_team": 0.7692307692307693
		}]
	},
	"720584": {
		"nik": "720584",
		"nama": "MUHAMMAD ASRIF PUTRA",
		"band": "III",
		"posisi": "MGR GOVERNMENT OFFERING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8747474747474735,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0517321016166297,
			"nilai_komparatif_unit": 1.0573053737657292,
			"summary_team": 0.92
		}]
	},
	"720590": {
		"nik": "720590",
		"nama": "ABDULLAH",
		"band": "III",
		"posisi": "MGR VVIP/VIP SEVICE & EVENT MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8571428571428565,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9871794871794879,
			"nilai_komparatif_unit": 0.9924106862021315,
			"summary_team": 0.8461538461538461
		}]
	},
	"730010": {
		"nik": "730010",
		"nama": "PRILLI HANDAJANI NUGRAHAWATI",
		"band": "III",
		"posisi": "SO BUDGET PLANNING OPT EVAL & REPORTING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8222222222222221,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0135135135135136,
			"nilai_komparatif_unit": 1.0188842601408328,
			"summary_team": 0.8333333333333333
		}]
	},
	"730044": {
		"nik": "730044",
		"nama": "FERRA SYUKRI",
		"band": "III",
		"posisi": "MGR GOVERNMENT PERFORM & ORDER ALIGNMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8747474747474735,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9907621247113179,
			"nilai_komparatif_unit": 0.9960123086198899,
			"summary_team": 0.8666666666666667
		}]
	},
	"730094": {
		"nik": "730094",
		"nama": "Fatmi Dewi Kandi Astuti",
		"band": "III",
		"posisi": "GM ORBITAL OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740733,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9152542372881364,
			"nilai_komparatif_unit": 0.9201042945814172,
			"summary_team": 0.8
		}]
	},
	"730096": {
		"nik": "730096",
		"nama": "ANTON DANISMAYA",
		"band": "III",
		"posisi": "OSM BUSINESS MARKETING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8361111111111105,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9568106312292367,
			"nilai_komparatif_unit": 0.9618809015336188,
			"summary_team": 0.8
		}]
	},
	"730097": {
		"nik": "730097",
		"nama": "SAIFULLANI",
		"band": "III",
		"posisi": "SO IP NETWORK STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.866666666666666,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9366515837104079,
			"nilai_komparatif_unit": 0.9416150284668046,
			"summary_team": 0.8117647058823529
		}]
	},
	"730099": {
		"nik": "730099",
		"nama": "Erik Zulkarnaen",
		"band": "III",
		"posisi": "GM SATELLITE CONTROL OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8800000000000003,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9659090909090906,
			"nilai_komparatif_unit": 0.9710275751948235,
			"summary_team": 0.8500000000000001
		}]
	},
	"730103": {
		"nik": "730103",
		"nama": "YESI JAMAL, S.Kom",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8597701149425278,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0125825731362075,
			"nilai_komparatif_unit": 1.0179483865832335,
			"summary_team": 0.8705882352941177
		}]
	},
	"730127": {
		"nik": "730127",
		"nama": "FIRDAUS EFFENDI",
		"band": "III",
		"posisi": "MGR GOVERNMENT DEVELOPMENT PROGRAM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7863636363636347,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9586482881280589,
			"nilai_komparatif_unit": 0.9637282964275047,
			"summary_team": 0.7538461538461538
		}]
	},
	"730151": {
		"nik": "730151",
		"nama": "Erma Susilowati",
		"band": "III",
		"posisi": "AVP BILLING & COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8444444444444442,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0319548872180457,
			"nilai_komparatif_unit": 1.03742335720505,
			"summary_team": 0.8714285714285717
		}]
	},
	"730152": {
		"nik": "730152",
		"nama": "ARI WIDIYANTO, ST",
		"band": "III",
		"posisi": "SENIOR EXPERT PROJECT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8358974358974345,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9353039598438386,
			"nilai_komparatif_unit": 0.9402602633571835,
			"summary_team": 0.7818181818181817
		}]
	},
	"730157": {
		"nik": "730157",
		"nama": "BETTI HERMASTUTI",
		"band": "III",
		"posisi": "MGR CUSTOMER ANALYSIS & SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8597701149425278,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0080213903743327,
			"nilai_komparatif_unit": 1.0133630334905164,
			"summary_team": 0.8666666666666666
		}]
	},
	"730173": {
		"nik": "730173",
		"nama": "FERY INDRAWAN",
		"band": "III",
		"posisi": "SENIOR ADVISOR III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9695251594613711,
			"nilai_komparatif_unit": 0.9746628057885789,
			"summary_team": 0.8
		}]
	},
	"730174": {
		"nik": "730174",
		"nama": "CIPUT GATOT AGUS SAKSOMO",
		"band": "III",
		"posisi": "MGR SOLUTION DEPLOYMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8699999999999994,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0433244916003543,
			"nilai_komparatif_unit": 1.0488532106748902,
			"summary_team": 0.9076923076923077
		}]
	},
	"730194": {
		"nik": "730194",
		"nama": "ROSIANA YULIASTRI, ST",
		"band": "III",
		"posisi": "MGR IT SPENDING & BUDGETING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8542635658914716,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0145190562613446,
			"nilai_komparatif_unit": 1.0198951313971143,
			"summary_team": 0.8666666666666667
		}]
	},
	"730233": {
		"nik": "730233",
		"nama": "SUPRIYONO",
		"band": "III",
		"posisi": "SO TRANSPORT STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8242424242424247,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0110294117647052,
			"nilai_komparatif_unit": 1.0163869947973496,
			"summary_team": 0.8333333333333333
		}]
	},
	"730258": {
		"nik": "730258",
		"nama": "BAMBANG FEBRIANSYAH",
		"band": "III",
		"posisi": "OSM RUMAH BUMN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8399999999999989,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.063492063492065,
			"nilai_komparatif_unit": 1.0691276532864034,
			"summary_team": 0.8933333333333334
		}]
	},
	"730266": {
		"nik": "730266",
		"nama": "JOKO SANTOSA",
		"band": "III",
		"posisi": "MGR SOLUTION DEPLOYMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8574712643678148,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9868014023510017,
			"nilai_komparatif_unit": 0.9920305978504653,
			"summary_team": 0.8461538461538461
		}]
	},
	"730278": {
		"nik": "730278",
		"nama": "AHMAD ZAKI FAIRUZI",
		"band": "III",
		"posisi": "SO SERVICE & BROADBAND NODE STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.866666666666666,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9366515837104079,
			"nilai_komparatif_unit": 0.9416150284668046,
			"summary_team": 0.8117647058823529
		}]
	},
	"730286": {
		"nik": "730286",
		"nama": "DUDI DAHNIAR DAHLAN",
		"band": "III",
		"posisi": "MGR GOVERNMENT SALES 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8612244897959169,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9753554502369685,
			"nilai_komparatif_unit": 0.9805239920718354,
			"summary_team": 0.84
		}]
	},
	"730297": {
		"nik": "730297",
		"nama": "FELISA HERU PRATOMO EDIATMOKO, ST",
		"band": "III",
		"posisi": "SO SUBSIDIARIES GOVERNANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8571428571428567,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.03939393939394,
			"nilai_komparatif_unit": 1.0449018299351365,
			"summary_team": 0.8909090909090909
		}]
	},
	"730303": {
		"nik": "730303",
		"nama": "LANDUNG",
		"band": "III",
		"posisi": "MGR DIGITALISASI & MONETISASI UKM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740743,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.998459167950693,
			"nilai_komparatif_unit": 1.003750139543363,
			"summary_team": 0.8727272727272727
		}]
	},
	"730311": {
		"nik": "730311",
		"nama": "RAHMAT MAHODUM HASIBUAN",
		"band": "III",
		"posisi": "GM BILLING AND COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7809523809523812,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9791965566714489,
			"nilai_komparatif_unit": 0.9843854530542849,
			"summary_team": 0.7647058823529412
		}]
	},
	"730316": {
		"nik": "730316",
		"nama": "KASRIYADI NUR",
		"band": "III",
		"posisi": "SO SALES & PIPELINE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8350877192982454,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9797555385790682,
			"nilai_komparatif_unit": 0.98494739708343,
			"summary_team": 0.8181818181818181
		}]
	},
	"730338": {
		"nik": "730338",
		"nama": "PAULUS PANDU PARLAUNGAN H.",
		"band": "III",
		"posisi": "SO ENTERPRISE STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7822222222222212,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8422459893048141,
			"nilai_komparatif_unit": 0.8467091659138269,
			"summary_team": 0.6588235294117648
		}]
	},
	"730377": {
		"nik": "730377",
		"nama": "DANI SETIAWAN BASUKI, ST",
		"band": "III",
		"posisi": "MGR GOVERNMENT SALES 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666665,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0153846153846156,
			"nilai_komparatif_unit": 1.0207652772364775,
			"summary_team": 0.88
		}]
	},
	"730402": {
		"nik": "730402",
		"nama": "FRANCISCUS XAVERIUS ARI  WIBOWO",
		"band": "III",
		"posisi": "MGR BIZ CONT, STRA ALIGN & ADAPTATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111105,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9700189753320689,
			"nilai_komparatif_unit": 0.9751592384570665,
			"summary_team": 0.8352941176470587
		}]
	},
	"730403": {
		"nik": "730403",
		"nama": "FERIE CAHYADIE",
		"band": "III",
		"posisi": "MGR CUSTOMER ANALYSIS & SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9138888888888883,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9191489361702134,
			"nilai_komparatif_unit": 0.9240196320051678,
			"summary_team": 0.8400000000000001
		}]
	},
	"730406": {
		"nik": "730406",
		"nama": "KRISDIASTORO FX,ST",
		"band": "III",
		"posisi": "SO CAPEX EVALUATION & CONTROL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.846666666666667,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9448818897637791,
			"nilai_komparatif_unit": 0.9498889480368073,
			"summary_team": 0.7999999999999999
		}]
	},
	"730413": {
		"nik": "730413",
		"nama": "I PUTU PUTRA KENCANA",
		"band": "III",
		"posisi": "SO ENTEPRISE MARKETING & SYNERGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8350877192982454,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8926661573720399,
			"nilai_komparatif_unit": 0.8973965173426808,
			"summary_team": 0.7454545454545455
		}]
	},
	"730434": {
		"nik": "730434",
		"nama": "DJEMI PERMATA INTAN",
		"band": "III",
		"posisi": "MGR BUSINESS MODEL ENGINEERING & CUST EN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9138888888888883,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006686930091186,
			"nilai_komparatif_unit": 1.0120215017199454,
			"summary_team": 0.9199999999999999
		}]
	},
	"730436": {
		"nik": "730436",
		"nama": "IRFAN AFFIANTO",
		"band": "III",
		"posisi": "MGR SALES - 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8758620689655164,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.989501312335959,
			"nilai_komparatif_unit": 0.9947448150274358,
			"summary_team": 0.8666666666666667
		}]
	},
	"730438": {
		"nik": "730438",
		"nama": "JESTAR SAHAT HAMONANGAN S.",
		"band": "III",
		"posisi": "MGR SOLUTION DESIGN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8699999999999994,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9902740937223703,
			"nilai_komparatif_unit": 0.9955216914880315,
			"summary_team": 0.8615384615384616
		}]
	},
	"730503": {
		"nik": "730503",
		"nama": "Mokhammad Salim Santoso",
		"band": "III",
		"posisi": "EXPERT SATELLITE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740733,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9152542372881364,
			"nilai_komparatif_unit": 0.9201042945814172,
			"summary_team": 0.8
		}]
	},
	"730504": {
		"nik": "730504",
		"nama": "GDE IVAN SYAHPUTRA",
		"band": "III",
		"posisi": "SO PERFORMANCE PLANNING & ANALYSIS-1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8499999999999989,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.008403361344539,
			"nilai_komparatif_unit": 1.0137470285770984,
			"summary_team": 0.857142857142857
		}]
	},
	"730517": {
		"nik": "730517",
		"nama": "GATI CAHYO HANDOYO",
		"band": "III",
		"posisi": "AVP CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8844444444444433,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9893216080402023,
			"nilai_komparatif_unit": 0.9945641584540553,
			"summary_team": 0.875
		}]
	},
	"730558": {
		"nik": "730558",
		"nama": "JANPIETER ROBERT HASOLOAN H.",
		"band": "III",
		"posisi": "MGR PRODUCT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8205128205128197,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0725000000000011,
			"nilai_komparatif_unit": 1.0781833240810303,
			"summary_team": 0.88
		}]
	},
	"730559": {
		"nik": "730559",
		"nama": "DEDY RISTANTO",
		"band": "III",
		"posisi": "SO IT DEVELOPMENT STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111105,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9290322580645168,
			"nilai_komparatif_unit": 0.9339553269729652,
			"summary_team": 0.8
		}]
	},
	"730581": {
		"nik": "730581",
		"nama": "AKHMAD SHOPIAN",
		"band": "III",
		"posisi": "MGR VVIP & GENERAL SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564102564102558,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0239520958083839,
			"nilai_komparatif_unit": 1.0293781579099486,
			"summary_team": 0.8769230769230769
		}]
	},
	"730609": {
		"nik": "730609",
		"nama": "MONICA PRIMADIYANTI",
		"band": "III",
		"posisi": "SO BIDDING MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8350877192982454,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9579831932773112,
			"nilai_komparatif_unit": 0.9630596771482428,
			"summary_team": 0.8
		}]
	},
	"740049": {
		"nik": "740049",
		"nama": "ADHI PRASETIO, S.KOMP",
		"band": "III",
		"posisi": "MGR  DECISION SUPPORT & EIS DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8748299319727879,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1002138413685862,
			"nilai_komparatif_unit": 1.1060440248827417,
			"summary_team": 0.9625
		}]
	},
	"740052": {
		"nik": "740052",
		"nama": "IRNI YULITA SARI",
		"band": "III",
		"posisi": "SO KPI MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559448,
			"nilai_komparatif_unit": 0.9490586255297834,
			"summary_team": 0.8181818181818181
		}]
	},
	"740054": {
		"nik": "740054",
		"nama": "DENI SETIAWAN",
		"band": "III",
		"posisi": "MGR VAR & PARTNERSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.880392156862744,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0146993318485535,
			"nilai_komparatif_unit": 1.0200763622892985,
			"summary_team": 0.8933333333333333
		}]
	},
	"740057": {
		"nik": "740057",
		"nama": "ARDHIRA KUSUMANINGRUM, ST",
		"band": "III",
		"posisi": "MGR DIGITAL LIFE & COLLABORATION DEVT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8748299319727879,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.085925349922241,
			"nilai_komparatif_unit": 1.0916798167673814,
			"summary_team": 0.9500000000000001
		}]
	},
	"740109": {
		"nik": "740109",
		"nama": "AMASA ESAF HENDRIKUS NDOLOE",
		"band": "III",
		"posisi": "SENIOR ADVISOR III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8518518518518522,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.963844393592677,
			"nilai_komparatif_unit": 0.9689519367651435,
			"summary_team": 0.8210526315789475
		}]
	},
	"740116": {
		"nik": "740116",
		"nama": "BRAHMANDI ANDRIA SASTRA",
		"band": "III",
		"posisi": "MGR FINANCIAL REPORTING PLATFORM DEVELOP",
		"category": [{
			"code": "DE-3-01",
			"kriteria": 0.8721088435374137,
			"kriteria_bp": 0.996889486449693,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9746489859594398,
			"nilai_komparatif_unit": 0.9776901042767938,
			"summary_team": 0.85
		}]
	},
	"740122": {
		"nik": "740122",
		"nama": "EKO BUDI SANTOSO HADI PRAYITNO",
		"band": "III",
		"posisi": "SO COLLECTION & DEBT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7888888888888888,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9449423815621001,
			"nilai_komparatif_unit": 0.9499497603894352,
			"summary_team": 0.7454545454545455
		}]
	},
	"740137": {
		"nik": "740137",
		"nama": "SUMARYADI, ST",
		"band": "III",
		"posisi": "MGR SUPPLY CHAIN MANAGEMENT PLATFORM DEV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8748299319727879,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9573289269051335,
			"nilai_komparatif_unit": 0.9624019437291389,
			"summary_team": 0.8375
		}]
	},
	"740181": {
		"nik": "740181",
		"nama": "REGINA LENGGOGENI",
		"band": "III",
		"posisi": "MANAGER AREA JAWA BARAT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857144,
			"nilai_komparatif_unit": 0.9693955960768496,
			"summary_team": 0.7714285714285716
		}]
	},
	"740195": {
		"nik": "740195",
		"nama": "MOHAMAD NOER FAJAR",
		"band": "III",
		"posisi": "MGR COMMERCIAL MANAGEMENT - 3",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.836363636363636,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9381270903010037,
			"nilai_komparatif_unit": 0.9430983539684848,
			"summary_team": 0.7846153846153846
		}]
	},
	"740200": {
		"nik": "740200",
		"nama": "LAMHOT TAGOR LINDUNG SIMANUNGKALIT, ST",
		"band": "III",
		"posisi": "MANAGER PRE SALES 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8844444444444433,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.010521356783921,
			"nilai_komparatif_unit": 1.0158762475637853,
			"summary_team": 0.89375
		}]
	},
	"740226": {
		"nik": "740226",
		"nama": "LUKAS PRIMADI RESPATIA",
		"band": "III",
		"posisi": "MGR ENTERPRISE ASSURANCE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666658,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9940828402366875,
			"nilai_komparatif_unit": 0.9993506210706782,
			"summary_team": 0.8615384615384616
		}]
	},
	"740247": {
		"nik": "740247",
		"nama": "HERU IRMAN",
		"band": "III",
		"posisi": "SENIOR STAFF III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8205128205128197,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7755681818181825,
			"nilai_komparatif_unit": 0.7796780236123152,
			"summary_team": 0.6363636363636364
		}]
	},
	"740250": {
		"nik": "740250",
		"nama": "DWI AGUSMIRANTONO SOETRISNO",
		"band": "III",
		"posisi": "MGR OUTBOUND LOGISTIC - 3",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8574712643678148,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9329758713136743,
			"nilai_komparatif_unit": 0.9379198379677127,
			"summary_team": 0.8
		}]
	},
	"740270": {
		"nik": "740270",
		"nama": "ZULFRI SANDRA SAKTI",
		"band": "III",
		"posisi": "MGR MANAGED SOLUTION DELIVERY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8862745098039212,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0530973451327437,
			"nilai_komparatif_unit": 1.058677851893826,
			"summary_team": 0.9333333333333332
		}]
	},
	"740273": {
		"nik": "740273",
		"nama": "WIBI GUNAWAN",
		"band": "III",
		"posisi": "MGR GOVERNMENT BIDDING MANAGEMENT 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8864197530864188,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.989286479537177,
			"nilai_komparatif_unit": 0.9945288438002914,
			"summary_team": 0.8769230769230769
		}]
	},
	"740286": {
		"nik": "740286",
		"nama": "DWI WIDYA SAKTI",
		"band": "III",
		"posisi": "MGR GOVT & BUSINESS PROJECT COMPLIANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666663,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0118343195266277,
			"nilai_komparatif_unit": 1.017196167875511,
			"summary_team": 0.8769230769230769
		}]
	},
	"750032": {
		"nik": "750032",
		"nama": "BUDI HAKIKI",
		"band": "III",
		"posisi": "SENIOR ADVISOR III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8266666666666684,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8374689826302713,
			"nilai_komparatif_unit": 0.8419068452280313,
			"summary_team": 0.6923076923076924
		}]
	},
	"750033": {
		"nik": "750033",
		"nama": "NASHRI",
		"band": "III",
		"posisi": "MGR SOLUTION DESIGN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8574712643678148,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0406269333883291,
			"nilai_komparatif_unit": 1.0461413577332181,
			"summary_team": 0.8923076923076924
		}]
	},
	"750035": {
		"nik": "750035",
		"nama": "ANDRI FABRIANSYAH",
		"band": "III",
		"posisi": "AVP ENTERPRISE CUSTOMER EXPERIENCE MGMT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8407407407407402,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9731678013616345,
			"nilai_komparatif_unit": 0.9783247505461201,
			"summary_team": 0.8181818181818181
		}]
	},
	"750058": {
		"nik": "750058",
		"nama": "DHINA MARLUPI RATNANINGTYAS",
		"band": "III",
		"posisi": "MGR SALES - 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9138888888888883,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9337386018237088,
			"nilai_komparatif_unit": 0.938686610290964,
			"summary_team": 0.8533333333333334
		}]
	},
	"750063": {
		"nik": "750063",
		"nama": "DANANG TRIYANTO",
		"band": "III",
		"posisi": "SO PROJECT EXECUTION DIGITAL TELCO 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8600000000000004,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9800664451827237,
			"nilai_komparatif_unit": 0.9852599512236706,
			"summary_team": 0.8428571428571427
		}]
	},
	"750077": {
		"nik": "750077",
		"nama": "TATI NOVIANI",
		"band": "III",
		"posisi": "SO GENERAL AFFAIR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8027777777777776,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7620598412375333,
			"nilai_komparatif_unit": 0.7660981004887133,
			"summary_team": 0.611764705882353
		}]
	},
	"760020": {
		"nik": "760020",
		"nama": "MARIETJE MANDUAPESSY",
		"band": "III",
		"posisi": "SO INFRA & SERV SLA PLANNING & CONTROL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.905555555555555,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.065319379285457,
			"nilai_komparatif_unit": 1.070964652275928,
			"summary_team": 0.9647058823529411
		}]
	},
	"760044": {
		"nik": "760044",
		"nama": "OGA AULIA GAUTAMA",
		"band": "III",
		"posisi": "MGR BILLING, COMPLIANCE, & INVOICING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8972972972972965,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.930900070871723,
			"nilai_komparatif_unit": 0.9358330375755151,
			"summary_team": 0.8352941176470587
		}]
	},
	"760057": {
		"nik": "760057",
		"nama": "CRISH MARSHAL",
		"band": "III",
		"posisi": "SO STRATEGY REGULATION & RISK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.859259259259259,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9310344827586211,
			"nilai_komparatif_unit": 0.9359681617293722,
			"summary_team": 0.8
		}]
	},
	"760060": {
		"nik": "760060",
		"nama": "THARIK A. SOFJAN",
		"band": "III",
		"posisi": "MGR PAYMENT & INVOICING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8282051282051274,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0142414860681124,
			"nilai_komparatif_unit": 1.0196160903214921,
			"summary_team": 0.84
		}]
	},
	"770011": {
		"nik": "770011",
		"nama": "IMRAN UMAR",
		"band": "III",
		"posisi": "SO FACILITY & SUPPORT SYSTEM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8699999999999994,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9359605911330054,
			"nilai_komparatif_unit": 0.940920374225295,
			"summary_team": 0.8142857142857142
		}]
	},
	"770020": {
		"nik": "770020",
		"nama": "VINSENSIUS PAYONG DOSI",
		"band": "III",
		"posisi": "MGR DATA & INFORMATION ARCHITECTURE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8542635658914716,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0242740471869345,
			"nilai_komparatif_unit": 1.0297018153528559,
			"summary_team": 0.875
		}]
	},
	"770081": {
		"nik": "770081",
		"nama": "MELANI ANUGRAHANI",
		"band": "III",
		"posisi": "VP SWITCHING COMMERCIAL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0054945054945055,
			"nilai_komparatif_unit": 1.0108227583023557,
			"summary_team": 0.8714285714285714
		}]
	},
	"780030": {
		"nik": "780030",
		"nama": "EKA PURA HARTATI",
		"band": "III",
		"posisi": "SO CAPEX PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7849462365591385,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9728518057285195,
			"nilai_komparatif_unit": 0.9780070804089573,
			"summary_team": 0.7636363636363636
		}]
	},
	"780032": {
		"nik": "780032",
		"nama": "ARIEF LUKMAN MULYANA",
		"band": "III",
		"posisi": "MGR IT APPLICATION ARCHITECTURE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8542635658914716,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0242740471869345,
			"nilai_komparatif_unit": 1.0297018153528559,
			"summary_team": 0.875
		}]
	},
	"780033": {
		"nik": "780033",
		"nama": "MUHAMMAD SYAHREZA RITONGA",
		"band": "III",
		"posisi": "SO ENTERPRISE IT TOOLS & DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0356291476064647,
			"nilai_komparatif_unit": 1.0411170880014369,
			"summary_team": 0.8545454545454546
		}]
	},
	"780041": {
		"nik": "780041",
		"nama": "LASMISANTI",
		"band": "III",
		"posisi": "MGR PARTNERSHIP & SYNERGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8205128205128197,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.007500000000001,
			"nilai_komparatif_unit": 1.0128388801973314,
			"summary_team": 0.8266666666666667
		}]
	},
	"780060": {
		"nik": "780060",
		"nama": "MAS MOCHAMAD SHOHIFUDDIN",
		"band": "III",
		"posisi": "MGR DIGITAL CHANNEL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8747474747474735,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0200746136081025,
			"nilai_komparatif_unit": 1.0254801284015433,
			"summary_team": 0.8923076923076924
		}]
	},
	"780064": {
		"nik": "780064",
		"nama": "KRSHNA SULANJANA",
		"band": "III",
		"posisi": "SENIOR EXPERT PROJECT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8358974358974345,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9788064696040172,
			"nilai_komparatif_unit": 0.9839932988621688,
			"summary_team": 0.8181818181818181
		}]
	},
	"780065": {
		"nik": "780065",
		"nama": "TANTAN SANTIKA",
		"band": "III",
		"posisi": "SO DIGITAL STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8527777777777769,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9932937344318844,
			"nilai_komparatif_unit": 0.9985573336863666,
			"summary_team": 0.8470588235294116
		}]
	},
	"790024": {
		"nik": "790024",
		"nama": "YULI SARIFAH",
		"band": "III",
		"posisi": "SO TELKOM GROUP DIGITIZATION STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7866666666666671,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0169491525423722,
			"nilai_komparatif_unit": 1.0223381050904619,
			"summary_team": 0.7999999999999999
		}]
	},
	"790039": {
		"nik": "790039",
		"nama": "NILA HERDIYANTI, ST.",
		"band": "III",
		"posisi": "SENIOR GOVERNMENT RELATION OFFICER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7863636363636347,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9479768786127186,
			"nilai_komparatif_unit": 0.9530003376546566,
			"summary_team": 0.7454545454545454
		}]
	},
	"790058": {
		"nik": "790058",
		"nama": "WILA ABADI",
		"band": "III",
		"posisi": "MGR ACCOUNT TEAM MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7863636363636347,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.13603082851638,
			"nilai_komparatif_unit": 1.1420508111406211,
			"summary_team": 0.8933333333333333
		}]
	},
	"790075": {
		"nik": "790075",
		"nama": "ERNY KURNIAWATI",
		"band": "III",
		"posisi": "SO CUSTOMER RELATIONSHIP MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8407407407407402,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9515418502202649,
			"nilai_komparatif_unit": 0.9565842005339842,
			"summary_team": 0.8
		}]
	},
	"790081": {
		"nik": "790081",
		"nama": "EDVYA NOER",
		"band": "III",
		"posisi": "MGR OFFERING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8617283950617275,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9573782234957031,
			"nilai_komparatif_unit": 0.9624515015490793,
			"summary_team": 0.8250000000000001
		}]
	},
	"790086": {
		"nik": "790086",
		"nama": "ARI EKA REZA",
		"band": "III",
		"posisi": "SO ENTEPRISE TRANSFORMATION MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8499999999999998,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197863,
			"nilai_komparatif_unit": 0.9676676181872296,
			"summary_team": 0.8181818181818181
		}]
	},
	"790087": {
		"nik": "790087",
		"nama": "FAUZAN AZHARY",
		"band": "III",
		"posisi": "MGR IT INFRASTRUCTURE ARCHITECTURE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8542635658914716,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9511116152450105,
			"nilai_komparatif_unit": 0.9561516856847947,
			"summary_team": 0.8125
		}]
	},
	"790109": {
		"nik": "790109",
		"nama": "RETNO KURNIAWATI",
		"band": "III",
		"posisi": "MGR DELIVERY & INTEGRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8141843971631193,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9825783972125451,
			"nilai_komparatif_unit": 0.9877852144306124,
			"summary_team": 0.8
		}]
	},
	"790113": {
		"nik": "790113",
		"nama": "YUSRON A. RAHMAN, M.T",
		"band": "III",
		"posisi": "MGR SEGMENT SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9753086419753083,
			"nilai_komparatif_unit": 0.9804769357667993,
			"summary_team": 0.8777777777777777
		}]
	},
	"790115": {
		"nik": "790115",
		"nama": "EKO YUDHAWAN",
		"band": "III",
		"posisi": "MGR OFFERING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8619883040935664,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9459346623525737,
			"nilai_komparatif_unit": 0.9509472994114349,
			"summary_team": 0.8153846153846154
		}]
	},
	"790117": {
		"nik": "790117",
		"nama": "SRI NURFALAH",
		"band": "III",
		"posisi": "MGR SERVICE MIGRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8141843971631193,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0644599303135907,
			"nilai_komparatif_unit": 1.0701006489664968,
			"summary_team": 0.8666666666666667
		}]
	},
	"790119": {
		"nik": "790119",
		"nama": "DAVY ELDY",
		"band": "III",
		"posisi": "MGR SOLUTION REQUIREMENT & ADM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8205128205128197,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9912500000000012,
			"nilai_komparatif_unit": 0.9965027692264069,
			"summary_team": 0.8133333333333335
		}]
	},
	"790127": {
		"nik": "790127",
		"nama": "DANA MAHARANI",
		"band": "III",
		"posisi": "MGR OUTBOUND LOGISTIC-1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8574712643678148,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9329758713136743,
			"nilai_komparatif_unit": 0.9379198379677127,
			"summary_team": 0.8
		}]
	},
	"795511": {
		"nik": "795511",
		"nama": "LUQMAN HADI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666665,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559442,
			"nilai_komparatif_unit": 0.9490586255297827,
			"summary_team": 0.8181818181818181
		}]
	},
	"800009": {
		"nik": "800009",
		"nama": "IRMIA DIESTYANA NILAM SARI",
		"band": "III",
		"posisi": "SO PORTFOLIO EVALUATION ALIGNMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8434782608695645,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0321406913280786,
			"nilai_komparatif_unit": 1.0376101459164562,
			"summary_team": 0.8705882352941177
		}]
	},
	"800015": {
		"nik": "800015",
		"nama": "RIVIERA RENDRA NURCAHYA",
		"band": "III",
		"posisi": "MGR GOVERNMENT OFFERING 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8612244897959169,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9753554502369686,
			"nilai_komparatif_unit": 0.9805239920718355,
			"summary_team": 0.8400000000000001
		}]
	},
	"800019": {
		"nik": "800019",
		"nama": "MUHAMAD IKBAL GODI",
		"band": "III",
		"posisi": "MGR BUSINESS MODEL ENGINEERING & CUST EN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8597701149425278,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9459893048128355,
			"nilai_komparatif_unit": 0.9510022314295616,
			"summary_team": 0.8133333333333335
		}]
	},
	"800039": {
		"nik": "800039",
		"nama": "CATUR KURNIA MUSTIKA RAMDHANI",
		"band": "III",
		"posisi": "SENIOR GOVERNMENT RELATION OFFICER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7863636363636347,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.01734104046243,
			"nilai_komparatif_unit": 1.0227320696781683,
			"summary_team": 0.8
		}]
	},
	"800050": {
		"nik": "800050",
		"nama": "PURBO ASIH SAYEKTI",
		"band": "III",
		"posisi": "MGR SOLUTION REQUIREMENT & ADM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8574712643678148,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9688595586718926,
			"nilai_komparatif_unit": 0.9739936778895478,
			"summary_team": 0.8307692307692308
		}]
	},
	"800055": {
		"nik": "800055",
		"nama": "YAYUK SETIYANINGSIH",
		"band": "III",
		"posisi": "MGR INDUSTRIAL ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8156249999999994,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9585510275165454,
			"nilai_komparatif_unit": 0.9636305204187179,
			"summary_team": 0.7818181818181817
		}]
	},
	"800063": {
		"nik": "800063",
		"nama": "RISA ARTANTI RIANI ARTI",
		"band": "III",
		"posisi": "MGR BIDDING MANAGEMENT - 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.836363636363636,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9086956521739135,
			"nilai_komparatif_unit": 0.9135109546282971,
			"summary_team": 0.76
		}]
	},
	"800067": {
		"nik": "800067",
		"nama": "DINA KUSUMA WAHYUNI, M.T",
		"band": "III",
		"posisi": "SO INET CONNECTIVITY PROD & SERVICE STRA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8222222222222211,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0158982511923702,
			"nilai_komparatif_unit": 1.021281634870577,
			"summary_team": 0.8352941176470587
		}]
	},
	"800068": {
		"nik": "800068",
		"nama": "FIDYA SAVITRI",
		"band": "III",
		"posisi": "MGR GOVERNMENT PERFORM & ORDER ALIGNMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8612244897959169,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0372827804107443,
			"nilai_komparatif_unit": 1.042779483631952,
			"summary_team": 0.8933333333333333
		}]
	},
	"800083": {
		"nik": "800083",
		"nama": "JOKO DWINANTO",
		"band": "III",
		"posisi": "MGR INDUSTRIAL ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8293040293040309,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0098829505300333,
			"nilai_komparatif_unit": 1.0152344583079056,
			"summary_team": 0.8375
		}]
	},
	"800084": {
		"nik": "800084",
		"nama": "DAUD ACHMAD",
		"band": "III",
		"posisi": "MGR INCIDENT MGT TOP200",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666658,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.04733727810651,
			"nilai_komparatif_unit": 1.0528872614851787,
			"summary_team": 0.9076923076923077
		}]
	},
	"800090": {
		"nik": "800090",
		"nama": "ADITYO  TON  SETIAWAN",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8324074074074084,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.026595206795428,
			"nilai_komparatif_unit": 1.032035275103353,
			"summary_team": 0.8545454545454546
		}]
	},
	"800092": {
		"nik": "800092",
		"nama": "DIMAS ARDIYANTO",
		"band": "III",
		"posisi": "MGR COLLECTION & DEBT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8644444444444436,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0025706940874046,
			"nilai_komparatif_unit": 1.0078834532190046,
			"summary_team": 0.8666666666666666
		}]
	},
	"800093": {
		"nik": "800093",
		"nama": "FACHRUL INDRACAHYO",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8617283950617275,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9781006958657397,
			"nilai_komparatif_unit": 0.9832837851323925,
			"summary_team": 0.8428571428571426
		}]
	},
	"800095": {
		"nik": "800095",
		"nama": "RONALDO NAIBORHU",
		"band": "III",
		"posisi": "VP CORPORATE AFFAIR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8562770562770574,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9702107801197778,
			"nilai_komparatif_unit": 0.97535205964456,
			"summary_team": 0.8307692307692307
		}]
	},
	"800100": {
		"nik": "800100",
		"nama": "ANDRIA PERMATA VEITHZAL",
		"band": "III",
		"posisi": "SO WS & MOBILE PORTFOLIO EVALUATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8121212121212125,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440298507462682,
			"nilai_komparatif_unit": 0.9490323939480926,
			"summary_team": 0.7666666666666666
		}]
	},
	"800102": {
		"nik": "800102",
		"nama": "SANDY ADISUTIYONO",
		"band": "III",
		"posisi": "MGR SOLUTION DESIGN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8205128205128197,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.975000000000001,
			"nilai_komparatif_unit": 0.980166658255482,
			"summary_team": 0.8
		}]
	},
	"800105": {
		"nik": "800105",
		"nama": "FADLI LUKMAN",
		"band": "III",
		"posisi": "MGR INBOUND LOGISTIC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8282051282051274,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.021671826625388,
			"nilai_komparatif_unit": 1.0270858052689023,
			"summary_team": 0.8461538461538461
		}]
	},
	"800107": {
		"nik": "800107",
		"nama": "YUDI KUSWANTO",
		"band": "III",
		"posisi": "SO LEGAL & LITIGATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0356291476064647,
			"nilai_komparatif_unit": 1.0411170880014369,
			"summary_team": 0.8545454545454546
		}]
	},
	"800109": {
		"nik": "800109",
		"nama": "ANAK AGUNG GDE BAGUS DHARMA SUASTIKA",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8612244897959169,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9500215424386056,
			"nilai_komparatif_unit": 0.9550558364336058,
			"summary_team": 0.8181818181818181
		}]
	},
	"800111": {
		"nik": "800111",
		"nama": "AJI TIRTOYO PUTRO",
		"band": "III",
		"posisi": "SO PERFORMANCE PLANNING & ANALYSIS-2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8499999999999989,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.008403361344539,
			"nilai_komparatif_unit": 1.0137470285770984,
			"summary_team": 0.857142857142857
		}]
	},
	"800113": {
		"nik": "800113",
		"nama": "RAHMAT ARYADI ARSYAD",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8949771689497721,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9577259475218652,
			"nilai_komparatif_unit": 0.9628010682123803,
			"summary_team": 0.857142857142857
		}]
	},
	"800114": {
		"nik": "800114",
		"nama": "ELITA RACHMAWATI",
		"band": "III",
		"posisi": "MGR INDUSTRIAL ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8394557823129278,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.967888978930305,
			"nilai_komparatif_unit": 0.9730179549132583,
			"summary_team": 0.8125000000000001
		}]
	},
	"810030": {
		"nik": "810030",
		"nama": "AHMAD NAZHIR",
		"band": "III",
		"posisi": "MGR BIDDING MANAGEMENT - 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9111111111111112,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9658536585365852,
			"nilai_komparatif_unit": 0.9709718490786002,
			"summary_team": 0.88
		}]
	},
	"810044": {
		"nik": "810044",
		"nama": "MUHAMAD ISNAINI SAPUTRO",
		"band": "III",
		"posisi": "SENIOR EXPERT PROJECT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8358974358974345,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.022308979364196,
			"nilai_komparatif_unit": 1.0277263343671543,
			"summary_team": 0.8545454545454546
		}]
	},
	"810059": {
		"nik": "810059",
		"nama": "LINA MAULANI TOHIR",
		"band": "III",
		"posisi": "MGR LOGISTIC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8999999999999992,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0493827160493836,
			"nilai_komparatif_unit": 1.0549435384832664,
			"summary_team": 0.9444444444444444
		}]
	},
	"810063": {
		"nik": "810063",
		"nama": "RINI AMELIA BAAY",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8539007092198568,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9870669197911737,
			"nilai_komparatif_unit": 0.9922975223038417,
			"summary_team": 0.8428571428571427
		}]
	},
	"810091": {
		"nik": "810091",
		"nama": "FAHMI MUHAMMAD HUSIN",
		"band": "III",
		"posisi": "MGR ACCOUNT TEAM MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8362318840579697,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9965337954939358,
			"nilai_komparatif_unit": 1.0018145642748124,
			"summary_team": 0.8333333333333334
		}]
	},
	"810096": {
		"nik": "810096",
		"nama": "HERI PRASETYA",
		"band": "III",
		"posisi": "MGR GOVERNMENT OFFERING 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8612244897959169,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9289099526066367,
			"nilai_komparatif_unit": 0.9338323734017481,
			"summary_team": 0.8
		}]
	},
	"810100": {
		"nik": "810100",
		"nama": "DITA PUSPITASARI",
		"band": "III",
		"posisi": "MGR SOLUTION REQUIREMENT & ADM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9662337662337673,
			"nilai_komparatif_unit": 0.9713539710184199,
			"summary_team": 0.8266666666666667
		}]
	},
	"810114": {
		"nik": "810114",
		"nama": "DIAN ENDAH PRATIWI",
		"band": "III",
		"posisi": "MGR INDUSTRIAL ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8324074074074084,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0640394088669938,
			"nilai_komparatif_unit": 1.0696778991192808,
			"summary_team": 0.8857142857142857
		}]
	},
	"810118": {
		"nik": "810118",
		"nama": "DESI KARTIKASARI",
		"band": "III",
		"posisi": "MGR GOVERNMENT OFFERING 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666665,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0118343195266275,
			"nilai_komparatif_unit": 1.0171961678755108,
			"summary_team": 0.8769230769230769
		}]
	},
	"820001": {
		"nik": "820001",
		"nama": "LENI TRIWANTI, S.E, M.S.M.",
		"band": "III",
		"posisi": "SO PORTFOLIO EXPANSION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8777777777777771,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9276672694394219,
			"nilai_komparatif_unit": 0.9325831050865898,
			"summary_team": 0.8142857142857142
		}]
	},
	"820015": {
		"nik": "820015",
		"nama": "NADIA PRIMASARI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.85528455284553,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9353612167300361,
			"nilai_komparatif_unit": 0.9403178236554465,
			"summary_team": 0.7999999999999998
		}]
	},
	"820019": {
		"nik": "820019",
		"nama": "DARU LUGAS PAMUNGKAS",
		"band": "III",
		"posisi": "MGR INDUSTRIAL ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8583333333333338,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9611650485436887,
			"nilai_komparatif_unit": 0.9662583935005482,
			"summary_team": 0.825
		}]
	},
	"820020": {
		"nik": "820020",
		"nama": "RISTYAWAN FAUZI MUBAROK",
		"band": "III",
		"posisi": "MGR BIG & MEGA PROJECT DELIVERY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8141843971631193,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0770570892522129,
			"nilai_komparatif_unit": 1.0827645619720174,
			"summary_team": 0.8769230769230769
		}]
	},
	"820024": {
		"nik": "820024",
		"nama": "ADHI FIRMANSYAH",
		"band": "III",
		"posisi": "MGR GOVERNMENT SALES 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666665,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0473372781065091,
			"nilai_komparatif_unit": 1.0528872614851779,
			"summary_team": 0.9076923076923077
		}]
	},
	"820025": {
		"nik": "820025",
		"nama": "RURUH PUTRO SUSETYO",
		"band": "III",
		"posisi": "MGR SEGMENT SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8617283950617275,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9283667621776512,
			"nilai_komparatif_unit": 0.9332863045324403,
			"summary_team": 0.7999999999999998
		}]
	},
	"820030": {
		"nik": "820030",
		"nama": "SANDHI PRIMAYUDI",
		"band": "III",
		"posisi": "MGR PROGRAM MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8542635658914716,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0096415607985496,
			"nilai_komparatif_unit": 1.0149917894192435,
			"summary_team": 0.8624999999999999
		}]
	},
	"820035": {
		"nik": "820035",
		"nama": "FARIDATUL MUFLIHA",
		"band": "III",
		"posisi": "MGR RESOURCE & BUDGET",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8358974358974345,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9754601226993881,
			"nilai_komparatif_unit": 0.9806292192079392,
			"summary_team": 0.8153846153846154
		}]
	},
	"820041": {
		"nik": "820041",
		"nama": "HARSONO SASONGKO SIPASULTA",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666665,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0489510489510492,
			"nilai_komparatif_unit": 1.054509583921981,
			"summary_team": 0.9090909090909092
		}]
	},
	"820044": {
		"nik": "820044",
		"nama": "BUDI ARIF MULYADI",
		"band": "III",
		"posisi": "MGR BIG DATA & ANALYTICS DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8748299319727879,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.979782270606533,
			"nilai_komparatif_unit": 0.9849742707675619,
			"summary_team": 0.857142857142857
		}]
	},
	"820046": {
		"nik": "820046",
		"nama": "HARDIANTO",
		"band": "III",
		"posisi": "MGR BUSINESS LEGAL & COMPLIANCE 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740751,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9533898305084734,
			"nilai_komparatif_unit": 0.9584419735223075,
			"summary_team": 0.8333333333333333
		}]
	},
	"820054": {
		"nik": "820054",
		"nama": "LASTTRI PALUPI DANGAN RIANTO",
		"band": "III",
		"posisi": "MGR MANAGED OPERATION SUPPORT-1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8444444444444446,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006578947368421,
			"nilai_komparatif_unit": 1.0119129467819743,
			"summary_team": 0.8500000000000001
		}]
	},
	"820056": {
		"nik": "820056",
		"nama": "HARVARD ADITYA GREGORY",
		"band": "III",
		"posisi": "MGR SALES - 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9138888888888883,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9629179331306997,
			"nilai_komparatif_unit": 0.9680205668625567,
			"summary_team": 0.88
		}]
	},
	"820062": {
		"nik": "820062",
		"nama": "ARIES PRIYONO",
		"band": "III",
		"posisi": "MGR BUSINESS MODEL ENGINEERING & CUST EN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8266666666666684,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0322580645161268,
			"nilai_komparatif_unit": 1.0377281410810695,
			"summary_team": 0.8533333333333333
		}]
	},
	"820064": {
		"nik": "820064",
		"nama": "DONES YHOHANES",
		"band": "III",
		"posisi": "MGR BIDDING ADMINISTRATION & LOG SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8864197530864188,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9545746732376269,
			"nilai_komparatif_unit": 0.959633094895018,
			"summary_team": 0.8461538461538461
		}]
	},
	"820065": {
		"nik": "820065",
		"nama": "ASTRI PRAWESTI",
		"band": "III",
		"posisi": "MGR GOVERNMENT PERFORM & ORDER ALIGNMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666665,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0295857988165682,
			"nilai_komparatif_unit": 1.0350417146803443,
			"summary_team": 0.8923076923076922
		}]
	},
	"820069": {
		"nik": "820069",
		"nama": "ARIE DWI UTOMO",
		"band": "III",
		"posisi": "MGR SECRETARIAT DIVISION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8999999999999992,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0694444444444453,
			"nilai_komparatif_unit": 1.0751115767189758,
			"summary_team": 0.9625
		}]
	},
	"820080": {
		"nik": "820080",
		"nama": "IBNU IBRAHIM ADJI",
		"band": "III",
		"posisi": "MGR ENTERPRISE PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8999999999999992,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.986111111111112,
			"nilai_komparatif_unit": 0.9913366486629519,
			"summary_team": 0.8875000000000001
		}]
	},
	"820087": {
		"nik": "820087",
		"nama": "DINNI DESTIANI",
		"band": "III",
		"posisi": "MGR ASSURANCE PERFORMANCE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7766666666666662,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9904258831297464,
			"nilai_komparatif_unit": 0.9956742852482228,
			"summary_team": 0.7692307692307693
		}]
	},
	"820089": {
		"nik": "820089",
		"nama": "GRATANIA GUNAWAN",
		"band": "III",
		"posisi": "MGR PERFORMANCE & PREDICTIVE ANALYTIC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8972972972972965,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.99558232931727,
			"nilai_komparatif_unit": 1.0008580561488374,
			"summary_team": 0.8933333333333333
		}]
	},
	"820090": {
		"nik": "820090",
		"nama": "RAHMALIA DINI PUTRANTI",
		"band": "III",
		"posisi": "SM PLANNING, PERFORMANCE & SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8972972972972965,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9361445783132539,
			"nilai_komparatif_unit": 0.9411053363787575,
			"summary_team": 0.8400000000000001
		}]
	},
	"820091": {
		"nik": "820091",
		"nama": "IDO LAKSONO SRI HARYOTO",
		"band": "III",
		"posisi": "MGR SALES - 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8266666666666684,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0806451612903203,
			"nilai_komparatif_unit": 1.0863716476942447,
			"summary_team": 0.8933333333333333
		}]
	},
	"820094": {
		"nik": "820094",
		"nama": "HAPPY CHRISTMANTO",
		"band": "III",
		"posisi": "SO CUSTOMER EXPERIENCE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8407407407407402,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0025173064820647,
			"nilai_komparatif_unit": 1.0078297827054474,
			"summary_team": 0.8428571428571427
		}]
	},
	"830031": {
		"nik": "830031",
		"nama": "RUDY SETYO NUGROHO",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8293040293040309,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9208159331834226,
			"nilai_komparatif_unit": 0.9256954626633822,
			"summary_team": 0.7636363636363637
		}]
	},
	"830032": {
		"nik": "830032",
		"nama": "YUSFI ARDIANSYAH",
		"band": "III",
		"posisi": "SO INDUSTRY SCANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8390804597701141,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9954875100725232,
			"nilai_komparatif_unit": 1.0007627344439534,
			"summary_team": 0.835294117647059
		}]
	},
	"830050": {
		"nik": "830050",
		"nama": "ANIE KURNIAWATI",
		"band": "III",
		"posisi": "MGR GOVERNMENT PERFORM & ORDER ALIGNMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9208333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0191437521754272,
			"nilai_komparatif_unit": 1.0245443342069134,
			"summary_team": 0.9384615384615385
		}]
	},
	"830060": {
		"nik": "830060",
		"nama": "SITI AISYAH RANGKUTI",
		"band": "III",
		"posisi": "MGR ORCHESTRATION OPERATION 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8156249999999994,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0574712643678168,
			"nilai_komparatif_unit": 1.0630749491247193,
			"summary_team": 0.8624999999999999
		}]
	},
	"830066": {
		"nik": "830066",
		"nama": "BHAKTI DHARMASTUTI INSANITAQWA",
		"band": "III",
		"posisi": "MGR SOLUTION DEPLOYMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.981818181818183,
			"nilai_komparatif_unit": 0.9870209705509752,
			"summary_team": 0.8400000000000001
		}]
	},
	"830068": {
		"nik": "830068",
		"nama": "FETHRIA CHRISTANTY",
		"band": "III",
		"posisi": "MGR LEGAL & COMPLIANCE 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8864197530864188,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0750450598066534,
			"nilai_komparatif_unit": 1.0807418705074372,
			"summary_team": 0.9529411764705882
		}]
	},
	"830070": {
		"nik": "830070",
		"nama": "MAHARDI PRABOWO",
		"band": "III",
		"posisi": "MGR SERVICE LEVEL GUARANTEE - 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7766666666666662,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9111918124793668,
			"nilai_komparatif_unit": 0.9160203424283649,
			"summary_team": 0.7076923076923077
		}]
	},
	"830079": {
		"nik": "830079",
		"nama": "CHRISTIAN WAHYU ADHI PUTRA",
		"band": "III",
		"posisi": "MGR OFFERING 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8293040293040309,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1003202296819767,
			"nilai_komparatif_unit": 1.1061509769623452,
			"summary_team": 0.9125
		}]
	},
	"830080": {
		"nik": "830080",
		"nama": "ARISTYA MEGA PUTRANTO",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.85528455284553,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9566194262011735,
			"nilai_komparatif_unit": 0.9616886832839795,
			"summary_team": 0.8181818181818181
		}]
	},
	"830081": {
		"nik": "830081",
		"nama": "ERVINA PRIYANTI",
		"band": "III",
		"posisi": "SO SYNERGY PERFORM & EVALUTION CROSS SOE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8629629629629625,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9406715475889932,
			"nilai_komparatif_unit": 0.9456562946834002,
			"summary_team": 0.8117647058823529
		}]
	},
	"830084": {
		"nik": "830084",
		"nama": "ELIZABETH MEILINDA",
		"band": "III",
		"posisi": "MGR SEGMENT SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8771929824561402,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9690000000000002,
			"nilai_komparatif_unit": 0.9741348634354475,
			"summary_team": 0.85
		}]
	},
	"830087": {
		"nik": "830087",
		"nama": "MAYRISA",
		"band": "III",
		"posisi": "MGR GOVERNMENT SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8612244897959169,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9753554502369686,
			"nilai_komparatif_unit": 0.9805239920718355,
			"summary_team": 0.8400000000000001
		}]
	},
	"830096": {
		"nik": "830096",
		"nama": "YOPI SOPIAWAN",
		"band": "III",
		"posisi": "VP BUSINESS STRATEGY & INNOVATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8612244897959169,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.010872595483693,
			"nilai_komparatif_unit": 1.0162293475254318,
			"summary_team": 0.8705882352941178
		}]
	},
	"830102": {
		"nik": "830102",
		"nama": "Risdianto Yuli Hermansyah, S.Si, Mt",
		"band": "III",
		"posisi": "AVP SPECTRUM PLANNING & MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7962962962962964,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.937674418604651,
			"nilai_komparatif_unit": 0.9426432835029455,
			"summary_team": 0.7466666666666666
		}]
	},
	"830106": {
		"nik": "830106",
		"nama": "DIAN AGUSTINA PUSPITASARI SRI HADI",
		"band": "III",
		"posisi": "MGR INDUSTRIAL ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.85528455284553,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.079262942380811,
			"nilai_komparatif_unit": 1.084982104217823,
			"summary_team": 0.923076923076923
		}]
	},
	"830120": {
		"nik": "830120",
		"nama": "RIYUSNO LEBANG",
		"band": "III",
		"posisi": "MGR GOVERNMENT OFFERING 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666665,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.958579881656805,
			"nilai_komparatif_unit": 0.9636595274610102,
			"summary_team": 0.8307692307692308
		}]
	},
	"830122": {
		"nik": "830122",
		"nama": "FENNY NOVITA",
		"band": "III",
		"posisi": "MGR INDUSTRIAL ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8771929824561402,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9282857142857143,
			"nilai_komparatif_unit": 0.933204827156647,
			"summary_team": 0.8142857142857142
		}]
	},
	"830125": {
		"nik": "830125",
		"nama": "DESSY",
		"band": "III",
		"posisi": "SO GENERAL RESOURCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8499999999999998,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0481283422459895,
			"nilai_komparatif_unit": 1.05368251758165,
			"summary_team": 0.8909090909090909
		}]
	},
	"830134": {
		"nik": "830134",
		"nama": "DINA FARHANI",
		"band": "III",
		"posisi": "SO PROJECT EXECUTION DIGITAL TELCO 3",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9148148148148144,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.964515360800191,
			"nilai_komparatif_unit": 0.9696264595195926,
			"summary_team": 0.8823529411764706
		}]
	},
	"830143": {
		"nik": "830143",
		"nama": "HARTOYO",
		"band": "III",
		"posisi": "MGR ASSURANCE RESOURCE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7766666666666662,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.089468471442721,
			"nilai_komparatif_unit": 1.095241713773045,
			"summary_team": 0.8461538461538461
		}]
	},
	"830146": {
		"nik": "830146",
		"nama": "GITHA KHARISMAWATI",
		"band": "III",
		"posisi": "SO INFRA & SERV PERF PLANNING & CONTROL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.905555555555555,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9224106820642375,
			"nilai_komparatif_unit": 0.9272986623364745,
			"summary_team": 0.835294117647059
		}]
	},
	"830148": {
		"nik": "830148",
		"nama": "DHENDY HAMDANI",
		"band": "III",
		"posisi": "MGR DIVISION SECRETARIAT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8282051282051274,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0030959752321993,
			"nilai_komparatif_unit": 1.008411517900377,
			"summary_team": 0.8307692307692308
		}]
	},
	"830154": {
		"nik": "830154",
		"nama": "ANISSA ISTIVANI",
		"band": "III",
		"posisi": "SO BUSINESS RISK & AUDIT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0576638103214957,
			"nilai_komparatif_unit": 1.0632685154057224,
			"summary_team": 0.8727272727272727
		}]
	},
	"840036": {
		"nik": "840036",
		"nama": "ACHMAD RETNO SYAFARI'E",
		"band": "III",
		"posisi": "CEO MUDA RUMAH BUMN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740743,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857139,
			"nilai_komparatif_unit": 0.969395596076849,
			"summary_team": 0.8428571428571427
		}]
	},
	"840057": {
		"nik": "840057",
		"nama": "MISA JANUAR SENI",
		"band": "III",
		"posisi": "MGR PERFORMANCE & REPORTING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8358974358974345,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9938650306748483,
			"nilai_komparatif_unit": 0.9991316573062023,
			"summary_team": 0.8307692307692308
		}]
	},
	"840081": {
		"nik": "840081",
		"nama": "ASTI HAPSARI",
		"band": "III",
		"posisi": "SO PROJECT EXECUTION ICT & SERVICES 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0322580645161286,
			"nilai_komparatif_unit": 1.0377281410810713,
			"summary_team": 0.8888888888888888
		}]
	},
	"840087": {
		"nik": "840087",
		"nama": "CHAIDIR ABADI",
		"band": "III",
		"posisi": "MGR IT PLATFORM ARCHITECTURE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8542635658914716,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9803765880217801,
			"nilai_komparatif_unit": 0.9855717375520191,
			"summary_team": 0.8375
		}]
	},
	"840093": {
		"nik": "840093",
		"nama": "LILA FITRIA",
		"band": "III",
		"posisi": "SO PROJECT EXECUTION ICT & SERVICES 3",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8629629629629626,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0633678364049488,
			"nilai_komparatif_unit": 1.069002767902974,
			"summary_team": 0.9176470588235295
		}]
	},
	"840105": {
		"nik": "840105",
		"nama": "IKA LOEKITA NINGDYAH",
		"band": "III",
		"posisi": "MGR PROJECT RESOURCE  & MONITORING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8862745098039212,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9778761061946906,
			"nilai_komparatif_unit": 0.9830580053299813,
			"summary_team": 0.8666666666666666
		}]
	},
	"840109": {
		"nik": "840109",
		"nama": "YUSTINUS JUNIAWAN NUGROHO",
		"band": "III",
		"posisi": "MGR ENTERPRISE PERFORMANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8999999999999992,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.074074074074075,
			"nilai_komparatif_unit": 1.079765739388755,
			"summary_team": 0.9666666666666667
		}]
	},
	"840117": {
		"nik": "840117",
		"nama": "LAGUSTY DAMAWATY MARS",
		"band": "III",
		"posisi": "MGR BUSINESS LEGAL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8644444444444436,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0025706940874046,
			"nilai_komparatif_unit": 1.0078834532190046,
			"summary_team": 0.8666666666666667
		}]
	},
	"840141": {
		"nik": "840141",
		"nama": "DWI INDRI LESTARI",
		"band": "III",
		"posisi": "MGR LEGAL & COMPLIANCE - 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8282051282051274,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9659442724458215,
			"nilai_komparatif_unit": 0.971062943163326,
			"summary_team": 0.8
		}]
	},
	"840165": {
		"nik": "840165",
		"nama": "AHMAD FATONI SUBIYANTO",
		"band": "III",
		"posisi": "MGR QUALITY & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8358974358974345,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9730061349693269,
			"nilai_komparatif_unit": 0.9781622274615043,
			"summary_team": 0.8133333333333334
		}]
	},
	"840169": {
		"nik": "840169",
		"nama": "HAFIIZH KUSUMANINGTYAS",
		"band": "III",
		"posisi": "MGR CUSTOMER RELATIONSHIP MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8362318840579697,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9965337954939358,
			"nilai_komparatif_unit": 1.0018145642748124,
			"summary_team": 0.8333333333333334
		}]
	},
	"840176": {
		"nik": "840176",
		"nama": "Erni Aji Prastiwi",
		"band": "III",
		"posisi": "AVP HC PLANNING & DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0384615384615385,
			"nilai_komparatif_unit": 1.043964488082761,
			"summary_team": 0.9000000000000001
		}]
	},
	"840178": {
		"nik": "840178",
		"nama": "RAGA OCTASSERA HARYONO",
		"band": "III",
		"posisi": "MGR OFFERING 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.85528455284553,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9938212927756637,
			"nilai_komparatif_unit": 0.9990876876339121,
			"summary_team": 0.85
		}]
	},
	"840180": {
		"nik": "840180",
		"nama": "BONAVENTURA ERICH FAJAR KURNIAWAN",
		"band": "III",
		"posisi": "MGR OFFERING 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8394557823129278,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0274513776337082,
			"nilai_komparatif_unit": 1.03289598290792,
			"summary_team": 0.8625
		}]
	},
	"840186": {
		"nik": "840186",
		"nama": "OKI MULYADES",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.85528455284553,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9353612167300361,
			"nilai_komparatif_unit": 0.9403178236554465,
			"summary_team": 0.7999999999999998
		}]
	},
	"840188": {
		"nik": "840188",
		"nama": "PRATIWI SUMIAR",
		"band": "III",
		"posisi": "MGR BUSINESS PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8358974358974345,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.067484662576689,
			"nilai_komparatif_unit": 1.0731414096992544,
			"summary_team": 0.8923076923076924
		}]
	},
	"840193": {
		"nik": "840193",
		"nama": "LUTCE RASIANA",
		"band": "III",
		"posisi": "SO NW PERFORMANCE ANALYST",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9296296296296293,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0278884462151399,
			"nilai_komparatif_unit": 1.0333353675755,
			"summary_team": 0.9555555555555555
		}]
	},
	"850065": {
		"nik": "850065",
		"nama": "MUHAMMAD ABDUH ASWIN",
		"band": "III",
		"posisi": "MGR HCM & RISK MANAGEMENT DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8748299319727879,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9652669777086587,
			"nilai_komparatif_unit": 0.9703820593487834,
			"summary_team": 0.8444444444444444
		}]
	},
	"850069": {
		"nik": "850069",
		"nama": "DEWI RATNASARI",
		"band": "III",
		"posisi": "MGR SALES TAM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.880392156862744,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.031009080006854,
			"nilai_komparatif_unit": 1.0364725380321806,
			"summary_team": 0.9076923076923077
		}]
	},
	"850089": {
		"nik": "850089",
		"nama": "R. WIDHA ANDI PARMOKO",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8293040293040309,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9129858657243797,
			"nilai_komparatif_unit": 0.9178239026067205,
			"summary_team": 0.757142857142857
		}]
	},
	"850097": {
		"nik": "850097",
		"nama": "NOSKI PANDAPOTAN SAMOSIR",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8619883040935664,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9778057763132398,
			"nilai_komparatif_unit": 0.9829873027608766,
			"summary_team": 0.8428571428571427
		}]
	},
	"850101": {
		"nik": "850101",
		"nama": "YULISTIKA RAHAYU",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8949771689497721,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9548237476808902,
			"nilai_komparatif_unit": 0.9598834892177976,
			"summary_team": 0.8545454545454546
		}]
	},
	"850115": {
		"nik": "850115",
		"nama": "QUVIN NOLA PRIHANDANI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8583333333333338,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9320388349514556,
			"nilai_komparatif_unit": 0.9369778361217437,
			"summary_team": 0.7999999999999998
		}]
	},
	"850122": {
		"nik": "850122",
		"nama": "FATIMAH MARJUWAH",
		"band": "III",
		"posisi": "AVP FINANCIAL ACCOUNTING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9047619047619049,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0085526315789473,
			"nilai_komparatif_unit": 1.01389708981488,
			"summary_team": 0.9125
		}]
	},
	"850124": {
		"nik": "850124",
		"nama": "DUTO PRATOMO",
		"band": "III",
		"posisi": "MGR INDUSTRIAL ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8617283950617275,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9718839541547288,
			"nilai_komparatif_unit": 0.9770341000573987,
			"summary_team": 0.8375
		}]
	},
	"850130": {
		"nik": "850130",
		"nama": "Diah Eni Puspitawati",
		"band": "III",
		"posisi": "AVP CORPORATE STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346935,
			"nilai_komparatif_unit": 0.9847828277606085,
			"summary_team": 0.8
		}]
	},
	"850132": {
		"nik": "850132",
		"nama": "ANDIKA PRATAMA PUTRA",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8156249999999994,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.033388067870827,
			"nilai_komparatif_unit": 1.0388641324779864,
			"summary_team": 0.8428571428571426
		}]
	},
	"850140": {
		"nik": "850140",
		"nama": "WIDYA WARDANI",
		"band": "III",
		"posisi": "VP HUMAN CAPITAL & BUSINESS EFFECTIVENES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9579831932773103,
			"nilai_komparatif_unit": 0.9630596771482419,
			"summary_team": 0.8142857142857142
		}]
	},
	"850141": {
		"nik": "850141",
		"nama": "Mohamad Saiful Hidayat",
		"band": "III",
		"posisi": "AVP FLEET PLANNING & MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7962962962962964,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0517441860465115,
			"nilai_komparatif_unit": 1.0573175222326565,
			"summary_team": 0.8375
		}]
	},
	"850147": {
		"nik": "850147",
		"nama": "MARIES SWENDY",
		"band": "III",
		"posisi": "MGR TRANSACTION VALIDATION 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740751,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9788135593220327,
			"nilai_komparatif_unit": 0.9840004261495691,
			"summary_team": 0.8555555555555555
		}]
	},
	"850161": {
		"nik": "850161",
		"nama": "FANNY SUKMA MAHENDRA",
		"band": "III",
		"posisi": "MGR SECRETARIATE & FACILITY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8266666666666684,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0161290322580625,
			"nilai_komparatif_unit": 1.021513638876678,
			"summary_team": 0.8400000000000001
		}]
	},
	"850169": {
		"nik": "850169",
		"nama": "ALDILA HARISANTO",
		"band": "III",
		"posisi": "SO BUSINESS PERFORMANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0384615384615392,
			"nilai_komparatif_unit": 1.0439644880827617,
			"summary_team": 0.8999999999999998
		}]
	},
	"850171": {
		"nik": "850171",
		"nama": "MUKHAMAD ARIF HIDAYAT",
		"band": "III",
		"posisi": "SO FUNCTIONAL STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7822222222222212,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0377673796791458,
			"nilai_komparatif_unit": 1.043266650858108,
			"summary_team": 0.8117647058823529
		}]
	},
	"860099": {
		"nik": "860099",
		"nama": "GERRY ROY MANAEK SIAHAAN",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8553191489361723,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9353233830845749,
			"nilai_komparatif_unit": 0.940279789524327,
			"summary_team": 0.8
		}]
	},
	"860101": {
		"nik": "860101",
		"nama": "SARAH RASYIDAH",
		"band": "III",
		"posisi": "MGR OFFERING 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.85528455284553,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9353612167300365,
			"nilai_komparatif_unit": 0.9403178236554468,
			"summary_team": 0.8
		}]
	},
	"860109": {
		"nik": "860109",
		"nama": "YESSI KUSTIANTI DEWI",
		"band": "III",
		"posisi": "MGR  ENT BIDDING MANAGEMENT 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740751,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0192604006163315,
			"nilai_komparatif_unit": 1.0246616007838487,
			"summary_team": 0.8909090909090909
		}]
	},
	"860119": {
		"nik": "860119",
		"nama": "AGNES THERESIA SITANGGANG",
		"band": "III",
		"posisi": "MGR OUTBOUND LOGISTIC-2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8574712643678148,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.004743246030111,
			"nilai_komparatif_unit": 1.010067517811383,
			"summary_team": 0.8615384615384616
		}]
	},
	"860121": {
		"nik": "860121",
		"nama": "Zigroseno Aswin Wardhana",
		"band": "III",
		"posisi": "GM SATELLITE TRNSPNDR&CARRIER SRVCE CNTR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740733,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9610169491525431,
			"nilai_komparatif_unit": 0.9661095093104879,
			"summary_team": 0.84
		}]
	},
	"860122": {
		"nik": "860122",
		"nama": "DEVI SAPMAWATI",
		"band": "III",
		"posisi": "MGR TRANSACTION VALIDATION 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740751,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9724576271186429,
			"nilai_komparatif_unit": 0.9776108129927538,
			"summary_team": 0.85
		}]
	},
	"860127": {
		"nik": "860127",
		"nama": "STEFANUS ENGGAR PRADIPTA, ST. MT",
		"band": "III",
		"posisi": "MGR OFFERING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8539007092198568,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9759136212624601,
			"nilai_komparatif_unit": 0.9810851209218776,
			"summary_team": 0.8333333333333334
		}]
	},
	"860135": {
		"nik": "860135",
		"nama": "PRADIPTA WISMAYA ALBY",
		"band": "III",
		"posisi": "SO ORCHESTRATION POLICY & STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8434782608695645,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0162002945508108,
			"nilai_komparatif_unit": 1.0215852787980553,
			"summary_team": 0.857142857142857
		}]
	},
	"860155": {
		"nik": "860155",
		"nama": "DENNA GARTHINDA",
		"band": "III",
		"posisi": "MGR SALES - 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8597701149425278,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9614973262032096,
			"nilai_komparatif_unit": 0.9665924319448002,
			"summary_team": 0.8266666666666667
		}]
	},
	"860160": {
		"nik": "860160",
		"nama": "MOHAMMAD TAUFAN",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8619883040935664,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9778057763132398,
			"nilai_komparatif_unit": 0.9829873027608766,
			"summary_team": 0.8428571428571427
		}]
	},
	"865445": {
		"nik": "865445",
		"nama": "L DIMAS SRI ADHIDARMA",
		"band": "III",
		"posisi": "SO INDUSTRIAL ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7849462365591385,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9099804305283771,
			"nilai_komparatif_unit": 0.9148025411988546,
			"summary_team": 0.7142857142857142
		}]
	},
	"870004": {
		"nik": "870004",
		"nama": "YADI RUSLANNURZAMAN",
		"band": "III",
		"posisi": "SO BUSINESS EVALUATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0384615384615392,
			"nilai_komparatif_unit": 1.0439644880827617,
			"summary_team": 0.8999999999999998
		}]
	},
	"870009": {
		"nik": "870009",
		"nama": "YULIATI ATHIAH",
		"band": "III",
		"posisi": "MGR BUSINESS COMPLAINT HANDLING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666658,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0118343195266282,
			"nilai_komparatif_unit": 1.0171961678755115,
			"summary_team": 0.8769230769230769
		}]
	},
	"870013": {
		"nik": "870013",
		"nama": "RIZAL JEFRISANI",
		"band": "III",
		"posisi": "MGR MARKETING OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8362318840579697,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9300982091276734,
			"nilai_komparatif_unit": 0.9350269266564915,
			"summary_team": 0.7777777777777778
		}]
	},
	"870017": {
		"nik": "870017",
		"nama": "ADITTA MARISKA",
		"band": "III",
		"posisi": "MGR GOVERNMENT OFFERING 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9208333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9730316742081456,
			"nilai_komparatif_unit": 0.9781879020362401,
			"summary_team": 0.8959999999999999
		}]
	},
	"870026": {
		"nik": "870026",
		"nama": "ARIEF ADHIYANTO KUSUMO PUTRO",
		"band": "III",
		"posisi": "SO PORTFOLIO ORCHESTRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8777777777777771,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9276672694394219,
			"nilai_komparatif_unit": 0.9325831050865898,
			"summary_team": 0.8142857142857142
		}]
	},
	"870065": {
		"nik": "870065",
		"nama": "BOBBY ISKANDAR",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8597701149425278,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9470970206264333,
			"nilai_komparatif_unit": 0.9521158171806497,
			"summary_team": 0.8142857142857142
		}]
	},
	"870071": {
		"nik": "870071",
		"nama": "FELISIA NOVIANA MUKTI",
		"band": "III",
		"posisi": "SO EXECUTIVE REPORT & REVIEW MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8500000000000003,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.941176470588235,
			"nilai_komparatif_unit": 0.9461638933386239,
			"summary_team": 0.8
		}]
	},
	"880002": {
		"nik": "880002",
		"nama": "EGA LESTARIA SUKMA",
		"band": "III",
		"posisi": "MGR OFFERING 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8583333333333338,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9499626587005222,
			"nilai_komparatif_unit": 0.9549966406625466,
			"summary_team": 0.8153846153846154
		}]
	},
	"880003": {
		"nik": "880003",
		"nama": "GDE ARI PRIMA SATRIA",
		"band": "III",
		"posisi": "MGR GOVERNMENT BIDDING MANAGEMENT 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8864197530864188,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.041354188986502,
			"nilai_komparatif_unit": 1.0468724671582013,
			"summary_team": 0.923076923076923
		}]
	},
	"880018": {
		"nik": "880018",
		"nama": "IRVAN SUPRADANA",
		"band": "III",
		"posisi": "SO EXECUTIVE SUPPORT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8222222222222221,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0135135135135136,
			"nilai_komparatif_unit": 1.0188842601408328,
			"summary_team": 0.8333333333333333
		}]
	},
	"880030": {
		"nik": "880030",
		"nama": "AFIF ROMADHON",
		"band": "III",
		"posisi": "SO STRATEGIC PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7849462365591385,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0555772994129173,
			"nilai_komparatif_unit": 1.0611709477906712,
			"summary_team": 0.8285714285714284
		}]
	},
	"880073": {
		"nik": "880073",
		"nama": "IMAM MASHARI",
		"band": "III",
		"posisi": "SO EXECUTIVE SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8266666666666662,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0887096774193554,
			"nilai_komparatif_unit": 1.0944788987964436,
			"summary_team": 0.8999999999999999
		}]
	},
	"890003": {
		"nik": "890003",
		"nama": "AGUNG DERMAWAN",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9208333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006993006993008,
			"nilai_komparatif_unit": 1.0123292005651023,
			"summary_team": 0.9272727272727272
		}]
	},
	"890007": {
		"nik": "890007",
		"nama": "FAIZAL ADI WARDANA",
		"band": "III",
		"posisi": "SO SYNERGY PERFOR&EVALUTION CROSS CFU/FU",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8296296296296293,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857147,
			"nilai_komparatif_unit": 0.9693955960768499,
			"summary_team": 0.8
		}]
	},
	"890009": {
		"nik": "890009",
		"nama": "HAFIZ REZA NOORZAMAN",
		"band": "III",
		"posisi": "MGR ORCHESTRATION OPERATION 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8362318840579697,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0015164644714054,
			"nilai_komparatif_unit": 1.0068236370961863,
			"summary_team": 0.8375
		}]
	},
	"890091": {
		"nik": "890091",
		"nama": "NIKE YOSEPHINE",
		"band": "III",
		"posisi": "SENIOR GOVERNMENT RELATION OFFICER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7863636363636347,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0404624277456669,
			"nilai_komparatif_unit": 1.045975980352672,
			"summary_team": 0.8181818181818181
		}]
	},
	"900006": {
		"nik": "900006",
		"nama": "INDRIANI REZKI PRATIWI",
		"band": "III",
		"posisi": "MGR REVENUE ASSURANCE & QUALITY MANAGEME",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8972972972972965,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9658634538152618,
			"nilai_komparatif_unit": 0.9709816962637973,
			"summary_team": 0.8666666666666666
		}]
	}
};