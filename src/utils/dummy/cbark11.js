export const cbark11 = {
	"635664": {
		"nik": "635664",
		"nama": "TASRONI",
		"band": "IV",
		"posisi": "OFF 1 CAPEX & NTE INVENTORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7948717948717953,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8177419354838708,
			"nilai_komparatif_unit": 0.8234725834996671,
			"summary_team": 0.6500000000000001
		}]
	},
	"650512": {
		"nik": "650512",
		"nama": "SUPRATMAN HAMID",
		"band": "III",
		"posisi": "KAKANDATEL MARISA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8316666666666652,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0174194542932036,
			"nilai_komparatif_unit": 1.0228108990345481,
			"summary_team": 0.8461538461538461
		}]
	},
	"651325": {
		"nik": "651325",
		"nama": "SUDARNO",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8395480225988695,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0164199192462995,
			"nilai_komparatif_unit": 1.0218060673148217,
			"summary_team": 0.8533333333333334
		}]
	},
	"651345": {
		"nik": "651345",
		"nama": "TONI PURWANTO",
		"band": "III",
		"posisi": "MGR SECURITY & SAFETY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8338461538461546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9594095940959402,
			"nilai_komparatif_unit": 0.9644936366597592,
			"summary_team": 0.8
		}]
	},
	"660008": {
		"nik": "660008",
		"nama": "AHMAD PURNAMA, RD.",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8431372549019593,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9883720930232577,
			"nilai_komparatif_unit": 0.9936096118272635,
			"summary_team": 0.8333333333333335
		}]
	},
	"660039": {
		"nik": "660039",
		"nama": "SYAMSU PRIHATINI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8394557823129278,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.004051863857371,
			"nilai_komparatif_unit": 1.0110881519229526,
			"summary_team": 0.8428571428571427
		}]
	},
	"660090": {
		"nik": "660090",
		"nama": "TULUS HAWARI SISWANDONO",
		"band": "III",
		"posisi": "MGR SECURITY & SAFETY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8551282051282039,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9715142428785623,
			"nilai_komparatif_unit": 0.9766624296306504,
			"summary_team": 0.8307692307692308
		}]
	},
	"660117": {
		"nik": "660117",
		"nama": "YUHERIZAL",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8935672514619886,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0445026178010468,
			"nilai_komparatif_unit": 1.0500375799273374,
			"summary_team": 0.9333333333333333
		}]
	},
	"660128": {
		"nik": "660128",
		"nama": "DINDIN SYAHRONI",
		"band": "III",
		"posisi": "MGR SHARED SERVICE PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8525641025641011,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696240601503776,
			"nilai_komparatif_unit": 0.9747622305658535,
			"summary_team": 0.8266666666666667
		}]
	},
	"660129": {
		"nik": "660129",
		"nama": "EMMA JEPSINA JACOBA T.",
		"band": "IV",
		"posisi": "MGR COMM & PERF REG JATIM BALNUS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.852777777777777,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9771986970684049,
			"nilai_komparatif_unit": 0.984046801013423,
			"summary_team": 0.8333333333333334
		}]
	},
	"660196": {
		"nik": "660196",
		"nama": "JOKO KUNCAHYO",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0153846153846162,
			"nilai_komparatif_unit": 1.0207652772364781,
			"summary_team": 0.88
		}]
	},
	"660249": {
		"nik": "660249",
		"nama": "RESTI MEDYANA",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8553191489361688,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9353233830845787,
			"nilai_komparatif_unit": 0.940279789524331,
			"summary_team": 0.8
		}]
	},
	"660363": {
		"nik": "660363",
		"nama": "MURSIYANA MULATSIH",
		"band": "III",
		"posisi": "KAKANDATEL CILEGON",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8569105691056935,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8869070208728628,
			"nilai_komparatif_unit": 0.8916068623920801,
			"summary_team": 0.76
		}]
	},
	"660374": {
		"nik": "660374",
		"nama": "AGUS HARYONO",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8431372549019593,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9488372093023273,
			"nilai_komparatif_unit": 0.9538652273541729,
			"summary_team": 0.8
		}]
	},
	"660388": {
		"nik": "660388",
		"nama": "NANDI MULYADI",
		"band": "III",
		"posisi": "MGR SHARED SERVICE HC & FINANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8624999999999985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9120772946859919,
			"nilai_komparatif_unit": 0.9169105169262238,
			"summary_team": 0.7866666666666666
		}]
	},
	"660406": {
		"nik": "660406",
		"nama": "ASEP SURATMAN",
		"band": "III",
		"posisi": "KAKANDATEL CIPUTAT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8935672514619886,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9997382198952878,
			"nilai_komparatif_unit": 1.0050359693590232,
			"summary_team": 0.8933333333333334
		}]
	},
	"660469": {
		"nik": "660469",
		"nama": "EDI SUNARYADI",
		"band": "III",
		"posisi": "MGR ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8577777777777781,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0414507772020722,
			"nilai_komparatif_unit": 1.0469695672079269,
			"summary_team": 0.8933333333333334
		}]
	},
	"660494": {
		"nik": "660494",
		"nama": "HERRY BUDIARTO",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8427860696517424,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0441558441558427,
			"nilai_komparatif_unit": 1.049688968681193,
			"summary_team": 0.88
		}]
	},
	"660546": {
		"nik": "660546",
		"nama": "NASIR PANJAITAN",
		"band": "III",
		"posisi": "MGR ACCESS NEW FTTH & MODERNIZATION DEPL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8454545454545446,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.025089605734768,
			"nilai_komparatif_unit": 1.0305216956568986,
			"summary_team": 0.8666666666666667
		}]
	},
	"660570": {
		"nik": "660570",
		"nama": "EMAN SULAEMAN",
		"band": "III",
		"posisi": "MGR SECURITY AND SAFETY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8888888888888893,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0124999999999997,
			"nilai_komparatif_unit": 1.0178653758806915,
			"summary_team": 0.9000000000000001
		}]
	},
	"660622": {
		"nik": "660622",
		"nama": "PURWADI",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & CONSTRUCTION SPV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8656084656084662,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0782396088019552,
			"nilai_komparatif_unit": 1.0839533478544714,
			"summary_team": 0.9333333333333333
		}]
	},
	"660640": {
		"nik": "660640",
		"nama": "DIDI WALUYO",
		"band": "III",
		"posisi": "MGR ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8427860696517424,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0125147579693021,
			"nilai_komparatif_unit": 1.0178802120544903,
			"summary_team": 0.8533333333333334
		}]
	},
	"670035": {
		"nik": "670035",
		"nama": "TUBAGUS HAERUDIN",
		"band": "III",
		"posisi": "KAKANDATEL RANGKAS-PANDEGLANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8569105691056935,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9335863377609082,
			"nilai_komparatif_unit": 0.9385335393600843,
			"summary_team": 0.8
		}]
	},
	"670068": {
		"nik": "670068",
		"nama": "MARTA MARLINA SINAGA",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8656084656084662,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9396088019559896,
			"nilai_komparatif_unit": 0.9445879174160394,
			"summary_team": 0.8133333333333334
		}]
	},
	"670130": {
		"nik": "670130",
		"nama": "NURHAYATI, IR.MT.",
		"band": "III",
		"posisi": "SM GENERAL AFFAIR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.92,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9683794466403163,
			"nilai_komparatif_unit": 0.9735110216786982,
			"summary_team": 0.890909090909091
		}]
	},
	"670135": {
		"nik": "670135",
		"nama": "VERA RIANIATI SITORUS",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8624999999999985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0666666666666687,
			"nilai_komparatif_unit": 1.0723190791171096,
			"summary_team": 0.92
		}]
	},
	"670170": {
		"nik": "670170",
		"nama": "YULIA SARI NASUTION",
		"band": "III",
		"posisi": "CONSUMER EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8499999999999989,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0695187165775417,
			"nilai_komparatif_unit": 1.0751862424302565,
			"summary_team": 0.9090909090909092
		}]
	},
	"670198": {
		"nik": "670198",
		"nama": "AGUS ROHMAN",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8431372549019593,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9962790697674436,
			"nilai_komparatif_unit": 1.0015584887218816,
			"summary_team": 0.8400000000000001
		}]
	},
	"670224": {
		"nik": "670224",
		"nama": "I MADE ARTA",
		"band": "III",
		"posisi": "MGR BILLING & PAYMENT COLLECTION CONS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8525641025641011,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9852631578947386,
			"nilai_komparatif_unit": 0.9904842020265932,
			"summary_team": 0.8400000000000001
		}]
	},
	"670225": {
		"nik": "670225",
		"nama": "MIRAWATY, IR.",
		"band": "IV",
		"posisi": "MGR SEG SUPP BASIC MANAGED SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9111111111111114,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.939024390243902,
			"nilai_komparatif_unit": 0.9456049727299298,
			"summary_team": 0.8555555555555555
		}]
	},
	"670263": {
		"nik": "670263",
		"nama": "DIANA FORTUNATA",
		"band": "III",
		"posisi": "MGR SHARED SERVICE PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8551282051282039,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.93553223388306,
			"nilai_komparatif_unit": 0.9404897470517374,
			"summary_team": 0.8
		}]
	},
	"670279": {
		"nik": "670279",
		"nama": "KUN SULISTYO",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8569105691056935,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.011385199240984,
			"nilai_komparatif_unit": 1.0167446676400913,
			"summary_team": 0.8666666666666667
		}]
	},
	"670345": {
		"nik": "670345",
		"nama": "ROHANANDA WIDYA",
		"band": "III",
		"posisi": "MGR PLANNING, ENGINEERING & CAPEX MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8454545454545446,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0372208436724577,
			"nilai_komparatif_unit": 1.0427172186824243,
			"summary_team": 0.8769230769230769
		}]
	},
	"670355": {
		"nik": "670355",
		"nama": "RIDWAN",
		"band": "III",
		"posisi": "MGR HOME SERVICE 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.046153846153847,
			"nilai_komparatif_unit": 1.0516975583648565,
			"summary_team": 0.9066666666666667
		}]
	},
	"670356": {
		"nik": "670356",
		"nama": "BUDI LEKSONO",
		"band": "III",
		"posisi": "KAKANDATEL PASAR BARU",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8935672514619886,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9848167539267013,
			"nilai_komparatif_unit": 0.9900354325029181,
			"summary_team": 0.88
		}]
	},
	"670358": {
		"nik": "670358",
		"nama": "HERMAN MAULANA",
		"band": "IV",
		"posisi": "OFF 1 SEGMENT PERF & INDUSTRIAL ANALYSIS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346936,
			"nilai_komparatif_unit": 0.9864567115465158,
			"summary_team": 0.7999999999999999
		}]
	},
	"670361": {
		"nik": "670361",
		"nama": "ACHMAD FIRDAUS",
		"band": "III",
		"posisi": "MGR DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8624999999999985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9275362318840596,
			"nilai_komparatif_unit": 0.9324513731453126,
			"summary_team": 0.8
		}]
	},
	"670400": {
		"nik": "670400",
		"nama": "RUSLI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8324074074074084,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9610678531701881,
			"nilai_komparatif_unit": 0.9678029138865698,
			"summary_team": 0.8
		}]
	},
	"670419": {
		"nik": "670419",
		"nama": "EDY SARWONO",
		"band": "IV",
		"posisi": "OFF 1 OFFERING 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8952380952380954,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1170212765957446,
			"nilai_komparatif_unit": 1.1248492422222574,
			"summary_team": 1
		}]
	},
	"670449": {
		"nik": "670449",
		"nama": "S.K. KRISTONADI",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & CONSTRUCTION SPV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8935672514619886,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9641562625855817,
			"nilai_komparatif_unit": 0.9692654583944653,
			"summary_team": 0.8615384615384616
		}]
	},
	"670451": {
		"nik": "670451",
		"nama": "SENO HURY PRABOWO",
		"band": "III",
		"posisi": "MGR DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8551282051282039,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0602698650674678,
			"nilai_komparatif_unit": 1.065888379991969,
			"summary_team": 0.9066666666666666
		}]
	},
	"670583": {
		"nik": "670583",
		"nama": "JOKO SARWONO",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8656084656084662,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9242053789731046,
			"nilai_komparatif_unit": 0.929102869589547,
			"summary_team": 0.8
		}]
	},
	"680005": {
		"nik": "680005",
		"nama": "ACHMAD NURDIN",
		"band": "III",
		"posisi": "KAKANDATEL CIKARANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8551282051282039,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.93553223388306,
			"nilai_komparatif_unit": 0.9404897470517374,
			"summary_team": 0.8
		}]
	},
	"680012": {
		"nik": "680012",
		"nama": "HERLIN WAHYUNINGSIH",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8395480225988695,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9370121130551823,
			"nilai_komparatif_unit": 0.9419774683058513,
			"summary_team": 0.7866666666666666
		}]
	},
	"680107": {
		"nik": "680107",
		"nama": "HALIMATUS SAKDIAH",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8624999999999985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9739130434782626,
			"nilai_komparatif_unit": 0.9790739418025781,
			"summary_team": 0.84
		}]
	},
	"680111": {
		"nik": "680111",
		"nama": "SRI RAHAYU",
		"band": "IV",
		"posisi": "OFF 1 SEGMENT ADMINISTRATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346936,
			"nilai_komparatif_unit": 0.9864567115465158,
			"summary_team": 0.7999999999999999
		}]
	},
	"680112": {
		"nik": "680112",
		"nama": "ACHMAD LUTHFI",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8553191489361688,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9509121061359882,
			"nilai_komparatif_unit": 0.9559511193497363,
			"summary_team": 0.8133333333333332
		}]
	},
	"680116": {
		"nik": "680116",
		"nama": "RETNO ISRO WARDHANI",
		"band": "III",
		"posisi": "MGR COMMUNITY DEVELOPMENT CENTER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.794444444444444,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8559440559440564,
			"nilai_komparatif_unit": 0.8604798204803368,
			"summary_team": 0.6799999999999999
		}]
	},
	"680201": {
		"nik": "680201",
		"nama": "JUNANDA",
		"band": "III",
		"posisi": "MGR HC PLANNING, DEVELOPMENT & SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.794444444444444,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0741258741258748,
			"nilai_komparatif_unit": 1.079817813936109,
			"summary_team": 0.8533333333333333
		}]
	},
	"680228": {
		"nik": "680228",
		"nama": "FATKHUL AJI",
		"band": "IV",
		"posisi": "OFF 1 ENTERPRISE SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333338,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181811,
			"nilai_komparatif_unit": 0.988698658618212,
			"summary_team": 0.8181818181818181
		}]
	},
	"680235": {
		"nik": "680235",
		"nama": "NUR CHOLIL",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8439393939393924,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0341113105924615,
			"nilai_komparatif_unit": 1.03959120776165,
			"summary_team": 0.8727272727272728
		}]
	},
	"680236": {
		"nik": "680236",
		"nama": "SUDARMADI",
		"band": "III",
		"posisi": "MGR BGES OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8676190476190493,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9989023051591638,
			"nilai_komparatif_unit": 1.004195624996466,
			"summary_team": 0.8666666666666666
		}]
	},
	"680239": {
		"nik": "680239",
		"nama": "YUDI SYAHRUNSYAH",
		"band": "IV",
		"posisi": "OFF 1 RESOURCE & BUDGET CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8711111111111108,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0331632653061227,
			"nilai_komparatif_unit": 1.0404035629592165,
			"summary_team": 0.8999999999999999
		}]
	},
	"680246": {
		"nik": "680246",
		"nama": "MARTUNUS",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8935672514619886,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9549738219895286,
			"nilai_komparatif_unit": 0.9600343587907086,
			"summary_team": 0.8533333333333334
		}]
	},
	"680279": {
		"nik": "680279",
		"nama": "ASWAR MUNAZAR",
		"band": "IV",
		"posisi": "MGR ASSURANCE & MAINTENANCE JAKBAR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777773,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0177215189873423,
			"nilai_komparatif_unit": 1.0248536025339285,
			"summary_team": 0.8933333333333333
		}]
	},
	"680311": {
		"nik": "680311",
		"nama": "DIDIK SURYADI",
		"band": "IV",
		"posisi": "OFF 1 MANAGED SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8839506172839499,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0019952114924189,
			"nilai_komparatif_unit": 1.0090170867582127,
			"summary_team": 0.8857142857142856
		}]
	},
	"680388": {
		"nik": "680388",
		"nama": "MISRODI",
		"band": "IV",
		"posisi": "MGR KONSTRUKSI JAKARTA SELATAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7333333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9454545454545451,
			"nilai_komparatif_unit": 0.9520801897805008,
			"summary_team": 0.6933333333333332
		}]
	},
	"680392": {
		"nik": "680392",
		"nama": "SLAMET SUPRIYADI",
		"band": "IV",
		"posisi": "SM CORRECTIVE MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8814814814814812,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0009886307464164,
			"nilai_komparatif_unit": 1.008003452002009,
			"summary_team": 0.8823529411764706
		}]
	},
	"680422": {
		"nik": "680422",
		"nama": "ILYA WINDAYANTI",
		"band": "IV",
		"posisi": "OFF 1 ENTERPRISE SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9991503823279521,
			"nilai_komparatif_unit": 1.0061523213352526,
			"summary_team": 0.8909090909090909
		}]
	},
	"680425": {
		"nik": "680425",
		"nama": "SUKOPRASETYO",
		"band": "IV",
		"posisi": "OFF 1 NETWORK BILLING VALIDATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7750000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9384164222873898,
			"nilai_komparatif_unit": 0.9449927441990082,
			"summary_team": 0.7272727272727273
		}]
	},
	"680426": {
		"nik": "680426",
		"nama": "SUTASIR",
		"band": "III",
		"posisi": "MGR PERFORMANCE MGT & SLA MONITORING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8505747126436772,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9191646191646201,
			"nilai_komparatif_unit": 0.9240353981059052,
			"summary_team": 0.7818181818181817
		}]
	},
	"680467": {
		"nik": "680467",
		"nama": "SUJARWO",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8427860696517424,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0757969303423833,
			"nilai_komparatif_unit": 1.0814977253078957,
			"summary_team": 0.9066666666666666
		}]
	},
	"680480": {
		"nik": "680480",
		"nama": "SAWALUDDIN HARAHAP",
		"band": "III",
		"posisi": "SENIOR EXPERT TDM SWITCHING EWSD",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.858333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9955869373345106,
			"nilai_komparatif_unit": 1.000862688584591,
			"summary_team": 0.8545454545454546
		}]
	},
	"680492": {
		"nik": "680492",
		"nama": "BUDI KURNIA",
		"band": "III",
		"posisi": "SENIOR STAFF III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.794444444444444,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0985378258105538,
			"nilai_komparatif_unit": 1.1043591278892024,
			"summary_team": 0.8727272727272727
		}]
	},
	"680500": {
		"nik": "680500",
		"nama": "SUGIYANTO",
		"band": "III",
		"posisi": "MGR SECURITY & SAFETY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9408284023668647,
			"nilai_komparatif_unit": 0.9458139806561773,
			"summary_team": 0.8153846153846154
		}]
	},
	"680506": {
		"nik": "680506",
		"nama": "SUYADI",
		"band": "IV",
		"posisi": "OFF 1 CAPACITY PLANNING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9142857142857144,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.06640625,
			"nilai_komparatif_unit": 1.0738795109340613,
			"summary_team": 0.9750000000000001
		}]
	},
	"680524": {
		"nik": "680524",
		"nama": "SUTRISNO",
		"band": "IV",
		"posisi": "SM PROCUREMENT & INVENTORY ASSET",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.911111111111111,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0071736011477763,
			"nilai_komparatif_unit": 1.0142317660143791,
			"summary_team": 0.9176470588235295
		}]
	},
	"680583": {
		"nik": "680583",
		"nama": "SUPRIYADI",
		"band": "III",
		"posisi": "KAKANDATEL SLEMAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.877777777777778,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0253164556962024,
			"nilai_komparatif_unit": 1.030749747727283,
			"summary_team": 0.9000000000000001
		}]
	},
	"680589": {
		"nik": "680589",
		"nama": "DENISA NURBUAT",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8739583333333335,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9153754469606672,
			"nilai_komparatif_unit": 0.9202261465605687,
			"summary_team": 0.7999999999999999
		}]
	},
	"680594": {
		"nik": "680594",
		"nama": "SRI ENDAH SULISTYANINGSIH",
		"band": "III",
		"posisi": "MGR SHARED SERVICE HC & FINANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8656084656084662,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9704156479217598,
			"nilai_komparatif_unit": 0.9755580130690243,
			"summary_team": 0.8400000000000001
		}]
	},
	"690001": {
		"nik": "690001",
		"nama": "MUH. HERRY FERIAL",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8553191489361723,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9520255863539421,
			"nilai_komparatif_unit": 0.958697279831663,
			"summary_team": 0.8142857142857142
		}]
	},
	"690030": {
		"nik": "690030",
		"nama": "FREDERYK TUHUMENA",
		"band": "III",
		"posisi": "MGR SECURITY & SAFETY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8395480225988695,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0261931877005908,
			"nilai_komparatif_unit": 1.0316311256543873,
			"summary_team": 0.8615384615384616
		}]
	},
	"690032": {
		"nik": "690032",
		"nama": "HILMA",
		"band": "III",
		"posisi": "MGR FINANCIAL SERVICE & TAX",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8525641025641011,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9852631578947385,
			"nilai_komparatif_unit": 0.990484202026593,
			"summary_team": 0.84
		}]
	},
	"690034": {
		"nik": "690034",
		"nama": "NURHIDAYATI",
		"band": "III",
		"posisi": "MGR SHARED SERVICE HC & FINANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8338461538461546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9753997539975392,
			"nilai_komparatif_unit": 0.9805685306040886,
			"summary_team": 0.8133333333333334
		}]
	},
	"690038": {
		"nik": "690038",
		"nama": "POPPY SEPTIANITA NURAHSID",
		"band": "IV",
		"posisi": "OFF 1 ENTERPRISE BIDDING MANAGEMENT 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346936,
			"nilai_komparatif_unit": 0.9864567115465158,
			"summary_team": 0.7999999999999999
		}]
	},
	"690072": {
		"nik": "690072",
		"nama": "SUNARYO SLAMET RIYADI",
		"band": "III",
		"posisi": "MGR ACCESS NE OM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111105,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0374193548387105,
			"nilai_komparatif_unit": 1.042916781786478,
			"summary_team": 0.8933333333333333
		}]
	},
	"690079": {
		"nik": "690079",
		"nama": "KENAN",
		"band": "IV",
		"posisi": "OFF 1 IN, ITKP, VAS&DIGI BILL VERIF&SETT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9197860962566842,
			"nilai_komparatif_unit": 0.9262318588362338,
			"summary_team": 0.7818181818181817
		}]
	},
	"690081": {
		"nik": "690081",
		"nama": "AHYAN",
		"band": "III",
		"posisi": "MGR ENTERPRISE & OLO DEPLOYMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8739583333333335,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9306317044100118,
			"nilai_komparatif_unit": 0.935563249003245,
			"summary_team": 0.8133333333333334
		}]
	},
	"690086": {
		"nik": "690086",
		"nama": "AHMAD BIDIN",
		"band": "III",
		"posisi": "MGR HOME SERVICE 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8338461538461546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.007380073800737,
			"nilai_komparatif_unit": 1.0127183184927468,
			"summary_team": 0.84
		}]
	},
	"690088": {
		"nik": "690088",
		"nama": "DADANG SUDIRMAN",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8551282051282039,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.013493253373315,
			"nilai_komparatif_unit": 1.0188638926393823,
			"summary_team": 0.8666666666666667
		}]
	},
	"690118": {
		"nik": "690118",
		"nama": "KARTONO",
		"band": "IV",
		"posisi": "MGR MANAGED OPT SPBU REG JAKARTA 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8592592592592608,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0205570291777168,
			"nilai_komparatif_unit": 1.0277089836764226,
			"summary_team": 0.8769230769230769
		}]
	},
	"690121": {
		"nik": "690121",
		"nama": "MAULANA",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8338461538461546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8954489544895442,
			"nilai_komparatif_unit": 0.9001940608824419,
			"summary_team": 0.7466666666666667
		}]
	},
	"690138": {
		"nik": "690138",
		"nama": "SUGIARTO",
		"band": "III",
		"posisi": "MGR ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8676190476190493,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0142700329308432,
			"nilai_komparatif_unit": 1.01964478845795,
			"summary_team": 0.88
		}]
	},
	"690145": {
		"nik": "690145",
		"nama": "SYAMSUL BAHRI",
		"band": "III",
		"posisi": "MGR SHARED SERVICE HC & FINANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000009,
			"nilai_komparatif_unit": 1.0052991366722892,
			"summary_team": 0.8666666666666667
		}]
	},
	"690153": {
		"nik": "690153",
		"nama": "AGUS HAERUDIN",
		"band": "IV",
		"posisi": "MGR KONSTRUKSI SERANG",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8722222222222225,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9936305732484073,
			"nilai_komparatif_unit": 1.0005938300240933,
			"summary_team": 0.8666666666666666
		}]
	},
	"690206": {
		"nik": "690206",
		"nama": "TEGUH DALIL HARDJONO",
		"band": "IV",
		"posisi": "PROJECT SUPERVISOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8814814814814812,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0009886307464164,
			"nilai_komparatif_unit": 1.008003452002009,
			"summary_team": 0.8823529411764706
		}]
	},
	"690233": {
		"nik": "690233",
		"nama": "JUNAR",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8569105691056935,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.011385199240984,
			"nilai_komparatif_unit": 1.0167446676400913,
			"summary_team": 0.8666666666666667
		}]
	},
	"690236": {
		"nik": "690236",
		"nama": "DANIL APRIANTO",
		"band": "IV",
		"posisi": "OFF 1 SEGMENT ADMINISTRATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333332,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.051948051948052,
			"nilai_komparatif_unit": 1.0593199913766564,
			"summary_team": 0.9818181818181818
		}]
	},
	"690237": {
		"nik": "690237",
		"nama": "ELVIERA WIJAYANTI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8324074074074084,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9610678531701881,
			"nilai_komparatif_unit": 0.9678029138865698,
			"summary_team": 0.8
		}]
	},
	"690242": {
		"nik": "690242",
		"nama": "SUHARNO",
		"band": "IV",
		"posisi": "OFF 1 BROADCAST INFRA & FACILITY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7916666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8982456140350878,
			"nilai_komparatif_unit": 0.9045404232192616,
			"summary_team": 0.7111111111111111
		}]
	},
	"690258": {
		"nik": "690258",
		"nama": "MOH. HOLILUDDIN",
		"band": "IV",
		"posisi": "MGR SALES AND MARKETING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8148148148148152,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181814,
			"nilai_komparatif_unit": 0.9886986586182123,
			"summary_team": 0.8
		}]
	},
	"690263": {
		"nik": "690263",
		"nama": "DUDI WAHYUDI",
		"band": "IV",
		"posisi": "OFF 1 MS BIZ PERFORMANCE & EVALUATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9750000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0256410256410255,
			"nilai_komparatif_unit": 1.032828608243147,
			"summary_team": 1
		}]
	},
	"690277": {
		"nik": "690277",
		"nama": "HESTY WIJAYANTI",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8571428571428559,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9955555555555571,
			"nilai_komparatif_unit": 1.0008311405093018,
			"summary_team": 0.8533333333333334
		}]
	},
	"690298": {
		"nik": "690298",
		"nama": "MAS ENAR KRESNARMAN",
		"band": "IV",
		"posisi": "MGR KONSTRUKSI JAKARTA PUSAT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9890909090909091,
			"nilai_komparatif_unit": 0.9960223523857551,
			"summary_team": 0.9066666666666666
		}]
	},
	"690313": {
		"nik": "690313",
		"nama": "TRIYANTO",
		"band": "IV",
		"posisi": "OFF 1 ENTERPRISE SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8399999999999997,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.969387755102041,
			"nilai_komparatif_unit": 0.9761811208012401,
			"summary_team": 0.8142857142857142
		}]
	},
	"690316": {
		"nik": "690316",
		"nama": "AHMAD MUSTAPID",
		"band": "IV",
		"posisi": "SM CORRECTIVE MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0045248868778278,
			"nilai_komparatif_unit": 1.011564489838141,
			"summary_team": 0.8705882352941177
		}]
	},
	"690320": {
		"nik": "690320",
		"nama": "BUHARI",
		"band": "IV",
		"posisi": "MGR SHARED SERVICE JAKARTA PUSAT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9309090909090909,
			"nilai_komparatif_unit": 0.9374328022454165,
			"summary_team": 0.8533333333333333
		}]
	},
	"690336": {
		"nik": "690336",
		"nama": "ABDUL DJABBAR HAKIM",
		"band": "IV",
		"posisi": "MGR BGES JAKARTA BARAT 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8592592592592608,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9620689655172396,
			"nilai_komparatif_unit": 0.9688110419218676,
			"summary_team": 0.8266666666666667
		}]
	},
	"690368": {
		"nik": "690368",
		"nama": "MACHMUDIN",
		"band": "III",
		"posisi": "MGR BILLING & PAYMENT COLLECTION EBIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8525641025641011,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0009022556390996,
			"nilai_komparatif_unit": 1.0062061734873329,
			"summary_team": 0.8533333333333334
		}]
	},
	"690376": {
		"nik": "690376",
		"nama": "SULISTYO",
		"band": "III",
		"posisi": "MGR ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8935672514619886,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9997382198952877,
			"nilai_komparatif_unit": 1.005035969359023,
			"summary_team": 0.8933333333333333
		}]
	},
	"690414": {
		"nik": "690414",
		"nama": "DEDE ABDUL RAHMAN",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8553191489361688,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9820895522388076,
			"nilai_komparatif_unit": 0.9872937790005474,
			"summary_team": 0.84
		}]
	},
	"690437": {
		"nik": "690437",
		"nama": "RUSDIONO",
		"band": "III",
		"posisi": "MGR BGES OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8935672514619886,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9549738219895286,
			"nilai_komparatif_unit": 0.9600343587907086,
			"summary_team": 0.8533333333333334
		}]
	},
	"690459": {
		"nik": "690459",
		"nama": "BOY SUBRATA",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8338461538461546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9913899138991381,
			"nilai_komparatif_unit": 0.9966434245484177,
			"summary_team": 0.8266666666666667
		}]
	},
	"690488": {
		"nik": "690488",
		"nama": "SRI WURYANTI",
		"band": "IV",
		"posisi": "OFF 1 SEGMENT ADMINISTRATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.967409948542024,
			"nilai_komparatif_unit": 0.9741894539844025,
			"summary_team": 0.8545454545454546
		}]
	},
	"690500": {
		"nik": "690500",
		"nama": "ANTING PRASTIWI",
		"band": "IV",
		"posisi": "OFF 1 ES COLLECTION & DEBT MGT SEGMENT B",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0505050505050504,
			"nilai_komparatif_unit": 1.05786687753389,
			"summary_team": 0.9454545454545454
		}]
	},
	"690506": {
		"nik": "690506",
		"nama": "TUKIYO",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8577777777777781,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0880829015544036,
			"nilai_komparatif_unit": 1.0938488015605203,
			"summary_team": 0.9333333333333332
		}]
	},
	"690516": {
		"nik": "690516",
		"nama": "DEWI ROSPITA",
		"band": "IV",
		"posisi": "OFF 1 SEGMENT ADMINISTRATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8394557823129278,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.104611757772208,
			"nilai_komparatif_unit": 1.112352758818164,
			"summary_team": 0.9272727272727272
		}]
	},
	"690544": {
		"nik": "690544",
		"nama": "ACHMAD FARIZAL",
		"band": "IV",
		"posisi": "OFF 1 DOM INTERCON&DIGI SETT ACC RECORD",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0167696381288611,
			"nilai_komparatif_unit": 1.023895050996207,
			"summary_team": 0.8727272727272727
		}]
	},
	"690572": {
		"nik": "690572",
		"nama": "SUTARNO",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8569105691056935,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9647058823529384,
			"nilai_komparatif_unit": 0.9698179906720871,
			"summary_team": 0.8266666666666667
		}]
	},
	"690577": {
		"nik": "690577",
		"nama": "SUNARTO",
		"band": "III",
		"posisi": "MGR SEKDIV & PUBLIC RELATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8561403508771928,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9344262295081973,
			"nilai_komparatif_unit": 0.9393778818085322,
			"summary_team": 0.8000000000000003
		}]
	},
	"690578": {
		"nik": "690578",
		"nama": "SRI DEWIANA",
		"band": "IV",
		"posisi": "OFF 1 COLLECTION & DEBT MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0703259005145795,
			"nilai_komparatif_unit": 1.0778266299401897,
			"summary_team": 0.9454545454545454
		}]
	},
	"690579": {
		"nik": "690579",
		"nama": "TARTO UTOMO",
		"band": "IV",
		"posisi": "MGR SPBU & PROJECT SOLUTION JAKBAR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777773,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9417721518987346,
			"nilai_komparatif_unit": 0.9483719904045308,
			"summary_team": 0.8266666666666667
		}]
	},
	"690601": {
		"nik": "690601",
		"nama": "TEGUH PRANANTO",
		"band": "III",
		"posisi": "MGR IP & SERVICE NODE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.858333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0252427184466024,
			"nilai_komparatif_unit": 1.0306756197339193,
			"summary_team": 0.88
		}]
	},
	"700010": {
		"nik": "700010",
		"nama": "SUNARTI",
		"band": "III",
		"posisi": "MGR SHARED SERVICE PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8656084656084662,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9858190709046449,
			"nilai_komparatif_unit": 0.9910430608955169,
			"summary_team": 0.8533333333333334
		}]
	},
	"700016": {
		"nik": "700016",
		"nama": "RIKA YUDHANTI",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8338461538461546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9594095940959401,
			"nilai_komparatif_unit": 0.9644936366597591,
			"summary_team": 0.7999999999999999
		}]
	},
	"700017": {
		"nik": "700017",
		"nama": "ANNIE FATMAWATI",
		"band": "III",
		"posisi": "MGR SHARED SERVICE PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8338461538461546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9753997539975392,
			"nilai_komparatif_unit": 0.9805685306040886,
			"summary_team": 0.8133333333333334
		}]
	},
	"700019": {
		"nik": "700019",
		"nama": "ASTININGSIH",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8569105691056935,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.042504743833014,
			"nilai_komparatif_unit": 1.0480291189520938,
			"summary_team": 0.8933333333333333
		}]
	},
	"700045": {
		"nik": "700045",
		"nama": "AGUS WIJAYA",
		"band": "III",
		"posisi": "MGR SHARED SERVICE PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8395480225988695,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9528936742934058,
			"nilai_komparatif_unit": 0.9579431881076454,
			"summary_team": 0.8
		}]
	},
	"700054": {
		"nik": "700054",
		"nama": "ARIS SRI HARTANTO",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8551282051282039,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0602698650674678,
			"nilai_komparatif_unit": 1.065888379991969,
			"summary_team": 0.9066666666666666
		}]
	},
	"700058": {
		"nik": "700058",
		"nama": "ARDIANSYAH",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333338,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9320388349514558,
			"nilai_komparatif_unit": 0.9385704634131895,
			"summary_team": 0.8
		}]
	},
	"700098": {
		"nik": "700098",
		"nama": "MUHAMMAD TOHIR",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8394557823129278,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9529983792544541,
			"nilai_komparatif_unit": 0.9596768899607689,
			"summary_team": 0.8
		}]
	},
	"700140": {
		"nik": "700140",
		"nama": "PARTONO",
		"band": "III",
		"posisi": "MGR ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8739583333333335,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0221692491060785,
			"nilai_komparatif_unit": 1.0275858636593018,
			"summary_team": 0.8933333333333334
		}]
	},
	"700148": {
		"nik": "700148",
		"nama": "SUPARNO",
		"band": "IV",
		"posisi": "OFF 1 OFFERING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8750000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9766233766233766,
			"nilai_komparatif_unit": 0.983467448784254,
			"summary_team": 0.8545454545454546
		}]
	},
	"700167": {
		"nik": "700167",
		"nama": "AMIRRUDIN",
		"band": "IV",
		"posisi": "OFF 1 ENTERPRISE SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0140499062750898,
			"summary_team": 0.8727272727272727
		}]
	},
	"700197": {
		"nik": "700197",
		"nama": "WIWIT LUHARDI",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8676190476190493,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9989023051591639,
			"nilai_komparatif_unit": 1.004195624996466,
			"summary_team": 0.8666666666666667
		}]
	},
	"700205": {
		"nik": "700205",
		"nama": "WIBANDOKO",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8569105691056935,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9647058823529384,
			"nilai_komparatif_unit": 0.9698179906720871,
			"summary_team": 0.8266666666666667
		}]
	},
	"700207": {
		"nik": "700207",
		"nama": "DEDEN SUPRIYATNA",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8739583333333335,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9306317044100118,
			"nilai_komparatif_unit": 0.935563249003245,
			"summary_team": 0.8133333333333334
		}]
	},
	"700212": {
		"nik": "700212",
		"nama": "HARSONO",
		"band": "III",
		"posisi": "MGR BGES OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8427860696517424,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9492325855962207,
			"nilai_komparatif_unit": 0.9542626988010846,
			"summary_team": 0.8
		}]
	},
	"700214": {
		"nik": "700214",
		"nama": "BADRIAH",
		"band": "III",
		"posisi": "MGR QUALITY, CHANGE & RISK MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8525641025641011,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9383458646616557,
			"nilai_komparatif_unit": 0.9433182876443743,
			"summary_team": 0.7999999999999999
		}]
	},
	"700303": {
		"nik": "700303",
		"nama": "NOERDIN",
		"band": "III",
		"posisi": "MGR SECURITY & SAFETY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8553191489361688,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9353233830845787,
			"nilai_komparatif_unit": 0.940279789524331,
			"summary_team": 0.8
		}]
	},
	"700328": {
		"nik": "700328",
		"nama": "KOMARUDIN",
		"band": "IV",
		"posisi": "OFF 1 PROCUREMENT & ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8345679012345671,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0457235072619697,
			"nilai_komparatif_unit": 1.0530518257472097,
			"summary_team": 0.8727272727272728
		}]
	},
	"700343": {
		"nik": "700343",
		"nama": "YUSRIZAL",
		"band": "IV",
		"posisi": "OFF 1 NETWORK BUSINESS SOLUTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9787595581988102,
			"nilai_komparatif_unit": 0.9856186004916759,
			"summary_team": 0.8727272727272727
		}]
	},
	"700357": {
		"nik": "700357",
		"nama": "HERI SURACHMAN",
		"band": "IV",
		"posisi": "OFF 1 OFFERING 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8795454545454543,
			"nilai_komparatif_unit": 0.8857092150121486,
			"summary_team": 0.7818181818181817
		}]
	},
	"700366": {
		"nik": "700366",
		"nama": "MUHAMAD TAUFIK",
		"band": "III",
		"posisi": "MGR APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8444444444444447,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9473684210526311,
			"nilai_komparatif_unit": 0.9523886557947989,
			"summary_team": 0.7999999999999998
		}]
	},
	"700388": {
		"nik": "700388",
		"nama": "ENCIN KURNAESIN",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & CONSTRUCTION SPV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8792792792792785,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0132265275707908,
			"nilai_komparatif_unit": 1.0185957534203764,
			"summary_team": 0.8909090909090909
		}]
	},
	"700389": {
		"nik": "700389",
		"nama": "HARI MULYONO",
		"band": "IV",
		"posisi": "OFF 1 ENTERPRISE SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0140499062750898,
			"summary_team": 0.8727272727272727
		}]
	},
	"700402": {
		"nik": "700402",
		"nama": "YUNI HARYANI",
		"band": "IV",
		"posisi": "OFF 1 BUDGET CONTROL & ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8711111111111108,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9675655976676386,
			"nilai_komparatif_unit": 0.9743461938824407,
			"summary_team": 0.8428571428571427
		}]
	},
	"700461": {
		"nik": "700461",
		"nama": "SANTY INDRIATI UTARI",
		"band": "IV",
		"posisi": "OFF 1 BUDGET CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8862745098039216,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9993678887484194,
			"nilai_komparatif_unit": 1.0063713520174493,
			"summary_team": 0.8857142857142856
		}]
	},
	"700475": {
		"nik": "700475",
		"nama": "PURWADI",
		"band": "IV",
		"posisi": "OFF 1 NETWORK DELIVERY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0519480519480517,
			"nilai_komparatif_unit": 1.0593199913766562,
			"summary_team": 0.9818181818181818
		}]
	},
	"700494": {
		"nik": "700494",
		"nama": "RUDY",
		"band": "IV",
		"posisi": "OFF 1 WALK IN CHANNEL DEVT & PARTNERSHIP",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9466666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999999,
			"nilai_komparatif_unit": 1.0070078930370685,
			"summary_team": 0.9466666666666665
		}]
	},
	"700497": {
		"nik": "700497",
		"nama": "MUHAMAD",
		"band": "III",
		"posisi": "KAKANDATEL SENTUL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9692307692307701,
			"nilai_komparatif_unit": 0.974366855543911,
			"summary_team": 0.8400000000000001
		}]
	},
	"700522": {
		"nik": "700522",
		"nama": "MUCHAMMAD ROSYAD",
		"band": "III",
		"posisi": "MGR OLO FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8959999999999992,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0130494505494516,
			"nilai_komparatif_unit": 1.0184177380436996,
			"summary_team": 0.9076923076923078
		}]
	},
	"700530": {
		"nik": "700530",
		"nama": "SUBAGIO",
		"band": "III",
		"posisi": "MGR SHARED SERVICE HC & FINANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8395480225988695,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9528936742934058,
			"nilai_komparatif_unit": 0.9579431881076454,
			"summary_team": 0.8
		}]
	},
	"700537": {
		"nik": "700537",
		"nama": "RIZAL IDHAM",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8617283950617275,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.012763740557438,
			"nilai_komparatif_unit": 1.0198610805230859,
			"summary_team": 0.8727272727272727
		}]
	},
	"700547": {
		"nik": "700547",
		"nama": "ERNA SULASTRI",
		"band": "IV",
		"posisi": "OFF 1 LOGISTIC PROCUREMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8799999999999993,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0389610389610395,
			"nilai_komparatif_unit": 1.0462419667917602,
			"summary_team": 0.9142857142857141
		}]
	},
	"700548": {
		"nik": "700548",
		"nama": "SUBEKTI",
		"band": "III",
		"posisi": "KAKANDATEL CIBINONG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8571428571428559,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1083333333333352,
			"nilai_komparatif_unit": 1.1142065431451214,
			"summary_team": 0.9500000000000002
		}]
	},
	"700551": {
		"nik": "700551",
		"nama": "YUSNAENI",
		"band": "IV",
		"posisi": "OFF 1 ES RECONCIL & ACCOUNT FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139859,
			"nilai_komparatif_unit": 0.9929238665610255,
			"summary_team": 0.8545454545454546
		}]
	},
	"700553": {
		"nik": "700553",
		"nama": "TUTI IRIANTI",
		"band": "IV",
		"posisi": "OFF 1 TRANSACTION VALIDATION 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.789473684210526,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0133333333333334,
			"nilai_komparatif_unit": 1.0204346649442295,
			"summary_team": 0.7999999999999998
		}]
	},
	"700555": {
		"nik": "700555",
		"nama": "WAWAN HADIAWAN",
		"band": "III",
		"posisi": "MGR ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8935672514619886,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0295811518324605,
			"nilai_komparatif_unit": 1.0350370430712326,
			"summary_team": 0.9199999999999999
		}]
	},
	"700571": {
		"nik": "700571",
		"nama": "SITI ROHAYATI",
		"band": "IV",
		"posisi": "OFF 1 COMPLIANCE & ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9787595581988102,
			"nilai_komparatif_unit": 0.9856186004916759,
			"summary_team": 0.8727272727272727
		}]
	},
	"700594": {
		"nik": "700594",
		"nama": "MURSIDI SAPUTRA",
		"band": "IV",
		"posisi": "OFF 1 ITKP &SMS, DIGI BILLING VALIDATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7750000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.079178885630498,
			"nilai_komparatif_unit": 1.0867416558288594,
			"summary_team": 0.8363636363636363
		}]
	},
	"700604": {
		"nik": "700604",
		"nama": "NURHAMSJAH",
		"band": "III",
		"posisi": "MGR BGES OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8577777777777781,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0103626943005177,
			"nilai_komparatif_unit": 1.0157167443061976,
			"summary_team": 0.8666666666666667
		}]
	},
	"700610": {
		"nik": "700610",
		"nama": "ELIZA AMBARSARI",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER RETENTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888892,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9803571428571423,
			"nilai_komparatif_unit": 0.9872273808524112,
			"summary_team": 0.8714285714285712
		}]
	},
	"700613": {
		"nik": "700613",
		"nama": "ZAINAL ARIEFIN",
		"band": "III",
		"posisi": "MGR BACKBONE, DC & CME OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.879365079365079,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0544798162126687,
			"nilai_komparatif_unit": 1.0600676488769492,
			"summary_team": 0.9272727272727274
		}]
	},
	"700626": {
		"nik": "700626",
		"nama": "SUDIKNO",
		"band": "IV",
		"posisi": "OFF 1 SEGMENT ADMINISTRATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9772727272727273,
			"nilai_komparatif_unit": 0.9841213500134988,
			"summary_team": 0.7818181818181817
		}]
	},
	"700644": {
		"nik": "700644",
		"nama": "AGUS RIYONO, S.T., M.M.",
		"band": "III",
		"posisi": "OSM REG ENTERPRISE,GOVERNMENT & BIZ SERV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9925925925925925,
			"nilai_komparatif_unit": 0.9978524764006416,
			"summary_team": 0.8933333333333333
		}]
	},
	"705041": {
		"nik": "705041",
		"nama": "HARIS NUGROHO ADI",
		"band": "III",
		"posisi": "MGR PARTNERSHIP MARKETING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.791666666666666,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8589473684210533,
			"nilai_komparatif_unit": 0.8634990479206188,
			"summary_team": 0.6799999999999999
		}]
	},
	"710003": {
		"nik": "710003",
		"nama": "EKO SULISTYOWATI",
		"band": "IV",
		"posisi": "MGR SEGMENT SUPP CARRIER SERVICE T-GROUP",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545454,
			"nilai_komparatif_unit": 1.0326408212234666,
			"summary_team": 0.8545454545454546
		}]
	},
	"710020": {
		"nik": "710020",
		"nama": "BUANG SANTOSO",
		"band": "III",
		"posisi": "MGR HOME SERVICE 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8395480225988695,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0481830417227462,
			"nilai_komparatif_unit": 1.0537375069184098,
			"summary_team": 0.88
		}]
	},
	"710027": {
		"nik": "710027",
		"nama": "BUDI YANTO",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8656084656084662,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.016625916870415,
			"nilai_komparatif_unit": 1.0220131565485018,
			"summary_team": 0.88
		}]
	},
	"710032": {
		"nik": "710032",
		"nama": "HAYATULLAH",
		"band": "IV",
		"posisi": "OFF 1 ENTERPRISE SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365352,
			"nilai_komparatif_unit": 0.99491688917556,
			"summary_team": 0.8727272727272727
		}]
	},
	"710045": {
		"nik": "710045",
		"nama": "RAHMAT SABARUDIN",
		"band": "IV",
		"posisi": "OFF 1 COLLECTION PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.06951871657754,
			"nilai_komparatif_unit": 1.0770137893444582,
			"summary_team": 0.9090909090909092
		}]
	},
	"710051": {
		"nik": "710051",
		"nama": "SUKAMTO",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8156249999999994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9808429118773951,
			"nilai_komparatif_unit": 0.9877165540899987,
			"summary_team": 0.7999999999999998
		}]
	},
	"710055": {
		"nik": "710055",
		"nama": "YANI ERDIYANTO",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8551282051282039,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8887556221889069,
			"nilai_komparatif_unit": 0.8934652596991505,
			"summary_team": 0.76
		}]
	},
	"710065": {
		"nik": "710065",
		"nama": "PUTUT PAMBUDI",
		"band": "III",
		"posisi": "MGR ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8553191489361688,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9820895522388077,
			"nilai_komparatif_unit": 0.9872937790005475,
			"summary_team": 0.8400000000000001
		}]
	},
	"710078": {
		"nik": "710078",
		"nama": "TAUFIK NURACHMAT",
		"band": "IV",
		"posisi": "MGR PROV & MIG JAKARTA UTARA",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8518518518518514,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9078260869565222,
			"nilai_komparatif_unit": 0.914188035070174,
			"summary_team": 0.7733333333333333
		}]
	},
	"710095": {
		"nik": "710095",
		"nama": "DEDY APRILRIADI",
		"band": "III",
		"posisi": "MGR HOME SERVICE 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8395480225988695,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.032301480484523,
			"nilai_komparatif_unit": 1.0377717871166159,
			"summary_team": 0.8666666666666667
		}]
	},
	"710163": {
		"nik": "710163",
		"nama": "AHMAD MULYONO",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8577777777777781,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.948186528497409,
			"nilai_komparatif_unit": 0.9532110985027393,
			"summary_team": 0.8133333333333334
		}]
	},
	"710186": {
		"nik": "710186",
		"nama": "MADINAH",
		"band": "III",
		"posisi": "MGR HOME SERVICE 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8656084656084662,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9396088019559896,
			"nilai_komparatif_unit": 0.9445879174160394,
			"summary_team": 0.8133333333333334
		}]
	},
	"710192": {
		"nik": "710192",
		"nama": "SUPRIYATNA",
		"band": "IV",
		"posisi": "SM CORRECTIVE MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8814814814814812,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0276816608996542,
			"nilai_komparatif_unit": 1.0348835440553958,
			"summary_team": 0.9058823529411764
		}]
	},
	"710195": {
		"nik": "710195",
		"nama": "AHMAD SYUKRI",
		"band": "III",
		"posisi": "MGR ACCESS CAPEX QE & PERFORMANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111105,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9909677419354846,
			"nilai_komparatif_unit": 0.9962190154378296,
			"summary_team": 0.8533333333333334
		}]
	},
	"710202": {
		"nik": "710202",
		"nama": "YAYAN HERYANA",
		"band": "III",
		"posisi": "MGR BGES OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8553191489361688,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0132669983416267,
			"nilai_komparatif_unit": 1.0186364386513582,
			"summary_team": 0.8666666666666666
		}]
	},
	"710250": {
		"nik": "710250",
		"nama": "ELLYSSA WARDIANI",
		"band": "IV",
		"posisi": "OFF 1 DATA REPORTING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8345679012345671,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9585798816568056,
			"nilai_komparatif_unit": 0.9652975069349423,
			"summary_team": 0.8
		}]
	},
	"710269": {
		"nik": "710269",
		"nama": "HARYASIN",
		"band": "IV",
		"posisi": "MGR KONSTRUKSI JAKARTA BARAT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8518518518518514,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9704347826086961,
			"nilai_komparatif_unit": 0.9772354857646687,
			"summary_team": 0.8266666666666667
		}]
	},
	"710273": {
		"nik": "710273",
		"nama": "BAIHAQI",
		"band": "III",
		"posisi": "MGR BGES OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8739583333333335,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006912991656734,
			"nilai_komparatif_unit": 1.0122487612166258,
			"summary_team": 0.88
		}]
	},
	"710294": {
		"nik": "710294",
		"nama": "SRI ANDRYANTI",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8569105691056935,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9958254269449688,
			"nilai_komparatif_unit": 1.00110244198409,
			"summary_team": 0.8533333333333334
		}]
	},
	"710295": {
		"nik": "710295",
		"nama": "NINA RUSTINA",
		"band": "IV",
		"posisi": "OFF 1 SALES PERFORMANCE & OFFERING MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7944444444444447,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0985378258105527,
			"nilai_komparatif_unit": 1.1062362613910068,
			"summary_team": 0.8727272727272728
		}]
	},
	"710302": {
		"nik": "710302",
		"nama": "LESTARI WIDIASTUTI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8324074074074084,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9610678531701881,
			"nilai_komparatif_unit": 0.9678029138865698,
			"summary_team": 0.8
		}]
	},
	"710304": {
		"nik": "710304",
		"nama": "SLAMET BUDI SUTRISNO",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8427860696517424,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0125147579693021,
			"nilai_komparatif_unit": 1.0178802120544903,
			"summary_team": 0.8533333333333334
		}]
	},
	"710305": {
		"nik": "710305",
		"nama": "SITI CHUSNUL CHOTIMAH",
		"band": "IV",
		"posisi": "OFF 1 ES COLLECTION & DEBT MGT SEGMENT C",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7833333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9516441005802707,
			"nilai_komparatif_unit": 0.9583131206464945,
			"summary_team": 0.7454545454545454
		}]
	},
	"710309": {
		"nik": "710309",
		"nama": "NAKHDUDIN",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "SP-2-01",
			"kriteria": 0.8886178861788635,
			"kriteria_bp": 0.9921219154269079,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0128087831655972,
			"nilai_komparatif_unit": 1.020851134741629,
			"summary_team": 0.8999999999999999
		}]
	},
	"710322": {
		"nik": "710322",
		"nama": "MOCHAMMAD SOLEH",
		"band": "IV",
		"posisi": "SM OPERATION & CONSTRUCTION REPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000004,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0588235294117643,
			"nilai_komparatif_unit": 1.0662436514510132,
			"summary_team": 0.9529411764705882
		}]
	},
	"710353": {
		"nik": "710353",
		"nama": "SANIAH",
		"band": "IV",
		"posisi": "OFF 1 QUALITY ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8774774774774767,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0180698151950727,
			"nilai_komparatif_unit": 1.0252043395642279,
			"summary_team": 0.8933333333333333
		}]
	},
	"710419": {
		"nik": "710419",
		"nama": "FENTI SITI ROHIMAH",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000009,
			"nilai_komparatif_unit": 1.0052991366722892,
			"summary_team": 0.8666666666666667
		}]
	},
	"710436": {
		"nik": "710436",
		"nama": "DESRUL, ST",
		"band": "III",
		"posisi": "MGR ACCESS OM & PARTNERSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111105,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0374193548387105,
			"nilai_komparatif_unit": 1.042916781786478,
			"summary_team": 0.8933333333333333
		}]
	},
	"710471": {
		"nik": "710471",
		"nama": "DEWI KAVITA APRIANA, SE,AK, CA.",
		"band": "III",
		"posisi": "MGR SHARED SERVICE HC & FINANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8571428571428559,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625000000000017,
			"nilai_komparatif_unit": 0.9676004190470792,
			"summary_team": 0.8250000000000002
		}]
	},
	"710474": {
		"nik": "710474",
		"nama": "MEILIA SAVITRI",
		"band": "III",
		"posisi": "MGR PLAN & BUDGET CONTROL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8525641025641011,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9852631578947385,
			"nilai_komparatif_unit": 0.990484202026593,
			"summary_team": 0.84
		}]
	},
	"720046": {
		"nik": "720046",
		"nama": "EDYAWATI",
		"band": "III",
		"posisi": "MGR CUSTOMER RELATIONSHIP MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666661,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000007,
			"nilai_komparatif_unit": 1.005299136672289,
			"summary_team": 0.8666666666666667
		}]
	},
	"720252": {
		"nik": "720252",
		"nama": "TONY ADI WIBOWO",
		"band": "III",
		"posisi": "MGR DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.791666666666666,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0442105263157904,
			"nilai_komparatif_unit": 1.0497439406093798,
			"summary_team": 0.8266666666666667
		}]
	},
	"720301": {
		"nik": "720301",
		"nama": "INNE GHINA AVIANTHIE",
		"band": "III",
		"posisi": "MGR CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666661,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.015384615384616,
			"nilai_komparatif_unit": 1.020765277236478,
			"summary_team": 0.88
		}]
	},
	"720323": {
		"nik": "720323",
		"nama": "SHAFIYUDDIN",
		"band": "III",
		"posisi": "MGR SHARED SERVICE PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8569105691056935,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9491461100569233,
			"nilai_komparatif_unit": 0.9541757650160857,
			"summary_team": 0.8133333333333334
		}]
	},
	"720374": {
		"nik": "720374",
		"nama": "HILAL MUTTAQIN",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8569105691056935,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.042504743833014,
			"nilai_komparatif_unit": 1.0480291189520938,
			"summary_team": 0.8933333333333333
		}]
	},
	"720404": {
		"nik": "720404",
		"nama": "AGUS BASUNI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272738,
			"nilai_komparatif_unit": 1.0281468443239323,
			"summary_team": 0.8727272727272728
		}]
	},
	"720408": {
		"nik": "720408",
		"nama": "WIDYA GUNAWAN",
		"band": "III",
		"posisi": "MGR PROJECT MGT & DELIVERY SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9687500000000009,
			"nilai_komparatif_unit": 0.9738835386512802,
			"summary_team": 0.8266666666666667
		}]
	},
	"720472": {
		"nik": "720472",
		"nama": "ERRY SUHERWAN",
		"band": "III",
		"posisi": "MGR WIRELESS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.858333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9941747572815539,
			"nilai_komparatif_unit": 0.9994430251965278,
			"summary_team": 0.8533333333333334
		}]
	},
	"720510": {
		"nik": "720510",
		"nama": "NGENDRANTO",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & CONSTRUCTION SPV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8676190476190493,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0603732162458814,
			"nilai_komparatif_unit": 1.0659922788424023,
			"summary_team": 0.9199999999999999
		}]
	},
	"720547": {
		"nik": "720547",
		"nama": "BULAN PURNAMASARI NOEGROHO PUTRI",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8571428571428559,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9644444444444459,
			"nilai_komparatif_unit": 0.9695551673683861,
			"summary_team": 0.8266666666666667
		}]
	},
	"720556": {
		"nik": "720556",
		"nama": "DANANG ARIS DRIANTO",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8395480225988695,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9846567967698526,
			"nilai_komparatif_unit": 0.9898746277112336,
			"summary_team": 0.8266666666666667
		}]
	},
	"720578": {
		"nik": "720578",
		"nama": "MOCHTAR ARIEF",
		"band": "III",
		"posisi": "MGR ACCESS DATA & INVENTORY MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8611111111111105,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0219354838709684,
			"nilai_komparatif_unit": 1.0273508596702616,
			"summary_team": 0.88
		}]
	},
	"720579": {
		"nik": "720579",
		"nama": "SUKMA INDRA",
		"band": "III",
		"posisi": "MGR ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8431372549019593,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0081395348837228,
			"nilai_komparatif_unit": 1.0134818040638087,
			"summary_team": 0.8500000000000001
		}]
	},
	"720599": {
		"nik": "720599",
		"nama": "IKA AYU KARTIKA",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8624999999999985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0048309178743977,
			"nilai_komparatif_unit": 1.010155654240755,
			"summary_team": 0.8666666666666666
		}]
	},
	"730001": {
		"nik": "730001",
		"nama": "YANTI EKA SAPTA TRIANI",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8569105691056935,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9491461100569233,
			"nilai_komparatif_unit": 0.9541757650160857,
			"summary_team": 0.8133333333333334
		}]
	},
	"730019": {
		"nik": "730019",
		"nama": "DWI IRIANI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.937500000000001,
			"nilai_komparatif_unit": 0.9424679406302713,
			"summary_team": 0.8
		}]
	},
	"730075": {
		"nik": "730075",
		"nama": "MUHAMAD PRIAMBONO",
		"band": "III",
		"posisi": "MGR BGES OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8715447154471535,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.063941655359567,
			"nilai_komparatif_unit": 1.0695796276026581,
			"summary_team": 0.9272727272727274
		}]
	},
	"730086": {
		"nik": "730086",
		"nama": "MARTARINI",
		"band": "III",
		"posisi": "SENIOR ADVISOR III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9725274725274723,
			"nilai_komparatif_unit": 0.9776810285219504,
			"summary_team": 0.8428571428571427
		}]
	},
	"730105": {
		"nik": "730105",
		"nama": "ZUHARMEN MUZAR",
		"band": "III",
		"posisi": "MGR TOP PRIORITY SOLUTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8933333333333336,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9328358208955222,
			"nilai_komparatif_unit": 0.9377790454032537,
			"summary_team": 0.8333333333333335
		}]
	},
	"730238": {
		"nik": "730238",
		"nama": "BEA JUBAEDAH SUGENG",
		"band": "III",
		"posisi": "MGR SHARED SERVICE PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8571428571428559,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8866666666666679,
			"nilai_komparatif_unit": 0.8913652345160968,
			"summary_team": 0.76
		}]
	},
	"730250": {
		"nik": "730250",
		"nama": "ZEIN AKHMAD SIDIK GIRSANG",
		"band": "III",
		"posisi": "MGR PERF MGT, SLA MONITORING & KPI REPOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8959999999999992,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9970238095238104,
			"nilai_komparatif_unit": 1.0023071749560026,
			"summary_team": 0.8933333333333334
		}]
	},
	"730298": {
		"nik": "730298",
		"nama": "MOHAMMAD KHUDORI",
		"band": "III",
		"posisi": "MGR ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8577777777777781,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0725388601036265,
			"nilai_komparatif_unit": 1.078222390109656,
			"summary_team": 0.92
		}]
	},
	"730323": {
		"nik": "730323",
		"nama": "RUSTAM EFENDI MARPAUNG",
		"band": "III",
		"posisi": "KAKANDATEL KUJANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8571428571428559,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9916666666666683,
			"nilai_komparatif_unit": 0.9969216438666875,
			"summary_team": 0.8500000000000001
		}]
	},
	"730346": {
		"nik": "730346",
		"nama": "RICHARDUS HER BENU BUDI P.",
		"band": "III",
		"posisi": "MGR HOME SERVICE 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0153846153846162,
			"nilai_komparatif_unit": 1.0207652772364781,
			"summary_team": 0.88
		}]
	},
	"730391": {
		"nik": "730391",
		"nama": "RIZAL FAHMI",
		"band": "III",
		"posisi": "GM WITEL KALTARA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555556,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9935064935064934,
			"nilai_komparatif_unit": 0.9987712202003902,
			"summary_team": 0.85
		}]
	},
	"730501": {
		"nik": "730501",
		"nama": "SUNAR WIDODO, ST",
		"band": "III",
		"posisi": "MGR ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8338461538461546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9114391143911431,
			"nilai_komparatif_unit": 0.9162689548267712,
			"summary_team": 0.76
		}]
	},
	"730524": {
		"nik": "730524",
		"nama": "RIZKA MUCHTAR",
		"band": "III",
		"posisi": "MGR SHARED SERVICE HC & FINANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8551282051282039,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1070464767616206,
			"nilai_komparatif_unit": 1.1129128673445556,
			"summary_team": 0.9466666666666665
		}]
	},
	"730541": {
		"nik": "730541",
		"nama": "DWI PRAYITNO",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8338461538461546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9913899138991381,
			"nilai_komparatif_unit": 0.9966434245484177,
			"summary_team": 0.8266666666666667
		}]
	},
	"730554": {
		"nik": "730554",
		"nama": "ILHAM FADLI, ST",
		"band": "III",
		"posisi": "MGR PERFORMANCE & WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8525641025641011,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0321804511278214,
			"nilai_komparatif_unit": 1.0376501164088119,
			"summary_team": 0.88
		}]
	},
	"730564": {
		"nik": "730564",
		"nama": "ROMDANI EDY PURWONO",
		"band": "III",
		"posisi": "MGR REVENUE ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8525641025641011,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9744360902255657,
			"nilai_komparatif_unit": 0.9795997602460811,
			"summary_team": 0.8307692307692308
		}]
	},
	"730574": {
		"nik": "730574",
		"nama": "SYAIFUL ISLAM",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & CONSTRUCTION SPV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8427860696517424,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9492325855962207,
			"nilai_komparatif_unit": 0.9542626988010846,
			"summary_team": 0.8
		}]
	},
	"730582": {
		"nik": "730582",
		"nama": "DWI ENDAR BAHAYANTO",
		"band": "III",
		"posisi": "KAKANDATEL CIKUPA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8569105691056935,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9958254269449688,
			"nilai_komparatif_unit": 1.00110244198409,
			"summary_team": 0.8533333333333334
		}]
	},
	"730592": {
		"nik": "730592",
		"nama": "NUR HAENI NUGROHO",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8676190476190493,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0296377607025229,
			"nilai_komparatif_unit": 1.0350939519194344,
			"summary_team": 0.8933333333333334
		}]
	},
	"740048": {
		"nik": "740048",
		"nama": "ARIS MEI NURROFIQ",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & CONSTRUCTION SPV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8577777777777781,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9326424870466319,
			"nilai_komparatif_unit": 0.9375846870518748,
			"summary_team": 0.8
		}]
	},
	"740072": {
		"nik": "740072",
		"nama": "SILVY INDRIANI",
		"band": "IV",
		"posisi": "OFF 1 BUDGET OPERATION & SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8641025641025636,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.02868447082097,
			"nilai_komparatif_unit": 1.0358933815613767,
			"summary_team": 0.8888888888888888
		}]
	},
	"740202": {
		"nik": "740202",
		"nama": "ROBERTUS PRASETYO WIBOWO",
		"band": "III",
		"posisi": "KAKANDATEL LENGKONG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8935672514619886,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9549738219895286,
			"nilai_komparatif_unit": 0.9600343587907086,
			"summary_team": 0.8533333333333334
		}]
	},
	"740213": {
		"nik": "740213",
		"nama": "HAMID SURYA LOKAYANTO",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0307692307692315,
			"nilai_komparatif_unit": 1.036231417800667,
			"summary_team": 0.8933333333333333
		}]
	},
	"740310": {
		"nik": "740310",
		"nama": "ADHI WIBOWO",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8676190476190493,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0181119648737633,
			"nilai_komparatif_unit": 1.0235070793233214,
			"summary_team": 0.8833333333333334
		}]
	},
	"750019": {
		"nik": "750019",
		"nama": "GRESY NIARIZA ABIDIN",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9161931818181828,
			"nilai_komparatif_unit": 0.9210482147068559,
			"summary_team": 0.7818181818181819
		}]
	},
	"755618": {
		"nik": "755618",
		"nama": "YOSH RAMADHANUL",
		"band": "IV",
		"posisi": "OFF 1 ADS PLACEMENT & PRESENTATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8041666666666654,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8455958549222812,
			"nilai_komparatif_unit": 0.851521700226165,
			"summary_team": 0.68
		}]
	},
	"770043": {
		"nik": "770043",
		"nama": "DIARTO RAHARDJO",
		"band": "III",
		"posisi": "MGR WIRELESS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8296296296296298,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857142,
			"nilai_komparatif_unit": 0.9693955960768493,
			"summary_team": 0.8000000000000002
		}]
	},
	"770050": {
		"nik": "770050",
		"nama": "KHAIDAR SUBHAN RAHMAN, M.T.",
		"band": "III",
		"posisi": "MGR CUSTOMER DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666661,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769237,
			"nilai_komparatif_unit": 0.9279684338513436,
			"summary_team": 0.8
		}]
	},
	"780022": {
		"nik": "780022",
		"nama": "JOHN CHRISTIAN",
		"band": "III",
		"posisi": "MGR ENTERPRISE & OLO DEPLOYMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8454545454545446,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9935483870967753,
			"nilai_komparatif_unit": 0.9988133357905327,
			"summary_team": 0.8400000000000001
		}]
	},
	"780025": {
		"nik": "780025",
		"nama": "DEVY INDRIASARI, ST.",
		"band": "III",
		"posisi": "SM SHARED SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8525641025641011,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0009022556390996,
			"nilai_komparatif_unit": 1.0062061734873329,
			"summary_team": 0.8533333333333334
		}]
	},
	"780039": {
		"nik": "780039",
		"nama": "SUSAN PRASETYA",
		"band": "III",
		"posisi": "MGR IP & SERVICE NODE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8352941176470584,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.95774647887324,
			"nilai_komparatif_unit": 0.9628217083621922,
			"summary_team": 0.8000000000000002
		}]
	},
	"790052": {
		"nik": "790052",
		"nama": "YANI MARIA CHRISTIE",
		"band": "III",
		"posisi": "MGR IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8363636363636368,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9565217391304344,
			"nilai_komparatif_unit": 0.9615904785561014,
			"summary_team": 0.8
		}]
	},
	"790054": {
		"nik": "790054",
		"nama": "INDAH SRI SWASTI",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8624999999999985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0975845410628038,
			"nilai_komparatif_unit": 1.1034007915552864,
			"summary_team": 0.9466666666666665
		}]
	},
	"795523": {
		"nik": "795523",
		"nama": "OKY GUSTIAWAN",
		"band": "IV",
		"posisi": "OFF 1 PRODUCT POLICY FORMULATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8644444444444435,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.007107213065176,
			"nilai_komparatif_unit": 1.014164912691197,
			"summary_team": 0.8705882352941177
		}]
	},
	"800005": {
		"nik": "800005",
		"nama": "RUDOLF TURNIP, M.KOM.",
		"band": "III",
		"posisi": "MGR HELPDESK OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.032,
			"nilai_komparatif_unit": 1.0374687090458015,
			"summary_team": 0.8600000000000001
		}]
	},
	"800049": {
		"nik": "800049",
		"nama": "FIERNAD NAPITU",
		"band": "III",
		"posisi": "MGR REGIONAL SOLUTION & SALES SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.000000000000001,
			"nilai_komparatif_unit": 1.0052991366722894,
			"summary_team": 0.8533333333333334
		}]
	},
	"810006": {
		"nik": "810006",
		"nama": "HENDRA SURYANA",
		"band": "III",
		"posisi": "MGR HOME SERVICE 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8553191489361688,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9820895522388077,
			"nilai_komparatif_unit": 0.9872937790005475,
			"summary_team": 0.8400000000000001
		}]
	},
	"810029": {
		"nik": "810029",
		"nama": "ALFON DEBI MARWAN LIMBONG",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8395480225988695,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9370121130551823,
			"nilai_komparatif_unit": 0.9419774683058513,
			"summary_team": 0.7866666666666666
		}]
	},
	"810046": {
		"nik": "810046",
		"nama": "ALFERDIN",
		"band": "IV",
		"posisi": "OFF 1 PARTNERSHIP MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8844444444444439,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9843334318652089,
			"nilai_komparatif_unit": 0.9912315352685309,
			"summary_team": 0.8705882352941177
		}]
	},
	"810083": {
		"nik": "810083",
		"nama": "RAHMI MULIANA",
		"band": "III",
		"posisi": "MGR WHOLESALE & INTERNATIONAL MARKETING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8452380952380943,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0095774647887334,
			"nilai_komparatif_unit": 1.0149273537559111,
			"summary_team": 0.8533333333333333
		}]
	},
	"820061": {
		"nik": "820061",
		"nama": "HENNY JUNIATY SINAGA",
		"band": "IV",
		"posisi": "OFF 1 TRACKING INTERFERENCE & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9384615384615382,
			"nilai_komparatif_unit": 0.9450381765424795,
			"summary_team": 0.8133333333333334
		}]
	},
	"820099": {
		"nik": "820099",
		"nama": "DANANG ISTIAJI",
		"band": "IV",
		"posisi": "OFF 1 OTT & CDN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7111111111111115,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9062499999999996,
			"nilai_komparatif_unit": 0.9126009030648429,
			"summary_team": 0.6444444444444445
		}]
	},
	"830043": {
		"nik": "830043",
		"nama": "HANA NURMADANIAH",
		"band": "III",
		"posisi": "MGR CHANNEL ACTIVATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.791666666666666,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0105263157894746,
			"nilai_komparatif_unit": 1.0158812328477869,
			"summary_team": 0.8
		}]
	},
	"830059": {
		"nik": "830059",
		"nama": "EKO AGUS BRAHMANTYO",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9788961038961036,
			"nilai_komparatif_unit": 0.9840834081386195,
			"summary_team": 0.8375
		}]
	},
	"830064": {
		"nik": "830064",
		"nama": "NOVITASARI KUSUMADEWI",
		"band": "III",
		"posisi": "MGR SALES PROMOTION & PRICING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.791666666666666,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0610526315789484,
			"nilai_komparatif_unit": 1.0666752944901763,
			"summary_team": 0.84
		}]
	},
	"830072": {
		"nik": "830072",
		"nama": "MAYASARI UTAMI",
		"band": "III",
		"posisi": "MGR HELPDESK OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8505747126436772,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9767151767151778,
			"nilai_komparatif_unit": 0.9818909239264897,
			"summary_team": 0.8307692307692308
		}]
	},
	"830091": {
		"nik": "830091",
		"nama": "IMELDA WIDYANTI",
		"band": "III",
		"posisi": "MGR WIFI FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8800000000000002,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0606060606060603,
			"nilai_komparatif_unit": 1.066226357076669,
			"summary_team": 0.9333333333333333
		}]
	},
	"830118": {
		"nik": "830118",
		"nama": "RINI SASMITA",
		"band": "IV",
		"posisi": "OFF 1 QUALITY MANAGEMENT SYSTEM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.873333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9814612868047985,
			"nilai_komparatif_unit": 0.9883392625227502,
			"summary_team": 0.857142857142857
		}]
	},
	"830131": {
		"nik": "830131",
		"nama": "DESY YUSIANOR, M.T",
		"band": "III",
		"posisi": "MGR PLANNING, ENGINEERING & CAPEX MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8388888888888882,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9695364238410604,
			"nilai_komparatif_unit": 0.9746741298597558,
			"summary_team": 0.8133333333333334
		}]
	},
	"835947": {
		"nik": "835947",
		"nama": "ZAKI MUBARROK",
		"band": "IV",
		"posisi": "OFF 1 DATA MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7151515151515154,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8389830508474573,
			"nilai_komparatif_unit": 0.8448625543277097,
			"summary_team": 0.6
		}]
	},
	"840034": {
		"nik": "840034",
		"nama": "RULLY HARDIAN",
		"band": "IV",
		"posisi": "MGR ASSURANCE & MAINTENANCE BALIKPAPAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7833333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.021276595744681,
			"nilai_komparatif_unit": 1.0284335928889212,
			"summary_team": 0.8
		}]
	},
	"840085": {
		"nik": "840085",
		"nama": "RIO NURJATI SUDIYONO",
		"band": "III",
		"posisi": "MGR ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8505747126436772,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.971891891891893,
			"nilai_komparatif_unit": 0.9770420798577168,
			"summary_team": 0.8266666666666667
		}]
	},
	"840128": {
		"nik": "840128",
		"nama": "SARTIKA SARI",
		"band": "III",
		"posisi": "MGR SHARED SERVICE HC & FINANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8569105691056935,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9491461100569233,
			"nilai_komparatif_unit": 0.9541757650160857,
			"summary_team": 0.8133333333333334
		}]
	},
	"840152": {
		"nik": "840152",
		"nama": "ERICK SONJAYA, M.T",
		"band": "III",
		"posisi": "MGR PERFORMANCE MGT & SLA MONITORING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9629629629629629,
			"nilai_komparatif_unit": 0.9680658353140553,
			"summary_team": 0.8666666666666668
		}]
	},
	"845957": {
		"nik": "845957",
		"nama": "SITI HAMIDAH",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9060109289617485,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1037394451145959,
			"nilai_komparatif_unit": 1.1114743330867523,
			"summary_team": 0.9999999999999998
		}]
	},
	"846018": {
		"nik": "846018",
		"nama": "PRAJNA RAKADITYA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.897435897435897,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0225210084033618,
			"nilai_komparatif_unit": 1.029686726258408,
			"summary_team": 0.9176470588235295
		}]
	},
	"850007": {
		"nik": "850007",
		"nama": "MUHAMMAD ARFI",
		"band": "III",
		"posisi": "MGR FULFILLMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8549999999999988,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9356725146198844,
			"nilai_komparatif_unit": 0.9406307711553589,
			"summary_team": 0.8
		}]
	},
	"850018": {
		"nik": "850018",
		"nama": "FARID KURNIAWAN",
		"band": "IV",
		"posisi": "MGR CORPORATE SECRETARY & COMMUNICATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7600000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.991902834008097,
			"nilai_komparatif_unit": 0.9988539829719909,
			"summary_team": 0.7538461538461538
		}]
	},
	"850021": {
		"nik": "850021",
		"nama": "TAUFAN HARHARA",
		"band": "IV",
		"posisi": "MGR KONSTRUKSI BANDA ACEH",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.035,
			"nilai_komparatif_unit": 1.0422531692933659,
			"summary_team": 0.92
		}]
	},
	"850073": {
		"nik": "850073",
		"nama": "TITAH KUSUMASTITI",
		"band": "IV",
		"posisi": "MGR SECRETARY & PROCUREMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828873,
			"nilai_komparatif_unit": 1.0123929619837904,
			"summary_team": 0.8545454545454546
		}]
	},
	"850076": {
		"nik": "850076",
		"nama": "TITIN",
		"band": "III",
		"posisi": "MGR HOME SERVICE 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8338461538461546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9913899138991381,
			"nilai_komparatif_unit": 0.9966434245484177,
			"summary_team": 0.8266666666666667
		}]
	},
	"850151": {
		"nik": "850151",
		"nama": "RINA REFLINA",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8551282051282039,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9667166416791618,
			"nilai_komparatif_unit": 0.9718394052867952,
			"summary_team": 0.8266666666666667
		}]
	},
	"850183": {
		"nik": "850183",
		"nama": "PUTHUT SETYO WIBOWO",
		"band": "IV",
		"posisi": "OFF 1 LIBRARY & QUALITY CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8041666666666654,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8455958549222812,
			"nilai_komparatif_unit": 0.851521700226165,
			"summary_team": 0.68
		}]
	},
	"855878": {
		"nik": "855878",
		"nama": "DAVID WAHYU GURETNO",
		"band": "IV",
		"posisi": "OFF 1 IPTV HEADEND O&M",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8260869565217385,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9280701754385973,
			"nilai_komparatif_unit": 0.9345739919589644,
			"summary_team": 0.7666666666666667
		}]
	},
	"860009": {
		"nik": "860009",
		"nama": "M. RIZHAM",
		"band": "IV",
		"posisi": "MGR HCM REG JAKARTA",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8592592592592608,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.024137931034481,
			"nilai_komparatif_unit": 1.0313149801103751,
			"summary_team": 0.88
		}]
	},
	"860018": {
		"nik": "860018",
		"nama": "ANDI PARASI TARIHORAN",
		"band": "IV",
		"posisi": "MGR KONSTRUKSI PEMATANG SIANTAR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8111111111111113,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0191780821917806,
			"nilai_komparatif_unit": 1.0263203731775052,
			"summary_team": 0.8266666666666667
		}]
	},
	"860021": {
		"nik": "860021",
		"nama": "WILIANSON PANJAITAN, S.KOM",
		"band": "IV",
		"posisi": "MGR PROG SDI & PRO ADM REG SUMATERA 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8411764705882342,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0778554778554792,
			"nilai_komparatif_unit": 1.0854089737537087,
			"summary_team": 0.9066666666666666
		}]
	},
	"860024": {
		"nik": "860024",
		"nama": "M. RIVAI HASIBUAN",
		"band": "III",
		"posisi": "MGR ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8866666666666662,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9774436090225571,
			"nilai_komparatif_unit": 0.9826232162962223,
			"summary_team": 0.8666666666666668
		}]
	},
	"860100": {
		"nik": "860100",
		"nama": "FINA GINTING",
		"band": "III",
		"posisi": "MGR SERVICE DELIVERY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.846666666666667,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9448818897637792,
			"nilai_komparatif_unit": 0.9498889480368075,
			"summary_team": 0.8
		}]
	},
	"860112": {
		"nik": "860112",
		"nama": "FARID BASKORO",
		"band": "III",
		"posisi": "MGR BACKBONE & CME OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8518518518518521,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0565217391304345,
			"nilai_komparatif_unit": 1.0621203922233302,
			"summary_team": 0.9
		}]
	},
	"860116": {
		"nik": "860116",
		"nama": "MUHAMMAD FAQIH MUTTAQIN",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8553191489361688,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9976782752902174,
			"nilai_komparatif_unit": 1.002965108825953,
			"summary_team": 0.8533333333333334
		}]
	},
	"860158": {
		"nik": "860158",
		"nama": "PACIOLO DAVID",
		"band": "IV",
		"posisi": "SR OFFICER FINANCIAL AUDIT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9222222222222223,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9930247305009509,
			"nilai_komparatif_unit": 0.9999837415954653,
			"summary_team": 0.9157894736842103
		}]
	},
	"860163": {
		"nik": "860163",
		"nama": "MUHAMMAD RIZAL SYAHIIRUL 'ALIM",
		"band": "III",
		"posisi": "MGR ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8581196581196568,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0382107931908744,
			"nilai_komparatif_unit": 1.0437124140786376,
			"summary_team": 0.8909090909090909
		}]
	},
	"865844": {
		"nik": "865844",
		"nama": "INNE ALFIAH PUTRI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7716049382716039,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8928000000000011,
			"nilai_komparatif_unit": 0.899056646903496,
			"summary_team": 0.6888888888888889
		}]
	},
	"870028": {
		"nik": "870028",
		"nama": "NOVIANA PUTRI KUSUMASARI",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8571428571428559,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0500000000000018,
			"nilai_komparatif_unit": 1.0555640935059045,
			"summary_team": 0.9000000000000002
		}]
	},
	"870043": {
		"nik": "870043",
		"nama": "SANNTY DEVIYANTI",
		"band": "III",
		"posisi": "MGR CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666661,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0615384615384622,
			"nilai_komparatif_unit": 1.0671636989290452,
			"summary_team": 0.9199999999999999
		}]
	},
	"870060": {
		"nik": "870060",
		"nama": "HIMAWAN YOGIESWARA",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8452380952380943,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9464788732394377,
			"nilai_komparatif_unit": 0.9514943941461669,
			"summary_team": 0.8
		}]
	},
	"870068": {
		"nik": "870068",
		"nama": "BILFIAN SUGIANA",
		"band": "III",
		"posisi": "MGR REGIONAL BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.031250000000001,
			"nilai_komparatif_unit": 1.0367147346932983,
			"summary_team": 0.88
		}]
	},
	"870077": {
		"nik": "870077",
		"nama": "ARIO AGISNA PRASETYO",
		"band": "IV",
		"posisi": "SO BUSINESS TOOLS MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.0070078930370683,
			"summary_team": 0.9333333333333332
		}]
	},
	"876786": {
		"nik": "876786",
		"nama": "FAJAR FEBRIAN",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7716049382716039,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8496000000000011,
			"nilai_komparatif_unit": 0.8555539059242946,
			"summary_team": 0.6555555555555556
		}]
	},
	"876845": {
		"nik": "876845",
		"nama": "ANDITO DWI PRATOMO",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333326,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200696,
			"nilai_komparatif_unit": 1.0570991947490334,
			"summary_team": 0.9272727272727274
		}]
	},
	"876996": {
		"nik": "876996",
		"nama": "MUHAMMAD ZAINULLAH",
		"band": "IV",
		"posisi": "OFF 1 OTT PLATFORM OPERATION & MAINTENAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.797101449275362,
			"nilai_komparatif_unit": 0.802687450971576,
			"summary_team": 0.611111111111111
		}]
	},
	"880004": {
		"nik": "880004",
		"nama": "INTAN YUSANTINA CALVIANTY",
		"band": "IV",
		"posisi": "MANAGER HC SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0399999999999998,
			"nilai_komparatif_unit": 1.047288208758551,
			"summary_team": 0.8666666666666666
		}]
	},
	"880031": {
		"nik": "880031",
		"nama": "SIGIT KUSDWIANTO",
		"band": "III",
		"posisi": "MGR HOME SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8624999999999985,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9739130434782626,
			"nilai_komparatif_unit": 0.9790739418025781,
			"summary_team": 0.84
		}]
	},
	"880064": {
		"nik": "880064",
		"nama": "RAKHMAWATI FITRI PUTRANTI",
		"band": "IV",
		"posisi": "OFF 1 TRAFFIC PARTNERSHIP",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0505050505050504,
			"nilai_komparatif_unit": 1.05786687753389,
			"summary_team": 0.9454545454545454
		}]
	},
	"880071": {
		"nik": "880071",
		"nama": "TRIHARYO PUTRA",
		"band": "III",
		"posisi": "MGR FULFILLMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8505747126436772,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.01891891891892,
			"nilai_komparatif_unit": 1.0243183095282515,
			"summary_team": 0.8666666666666667
		}]
	},
	"880083": {
		"nik": "880083",
		"nama": "HARRY SUSATYO",
		"band": "IV",
		"posisi": "SO STRATEGIC INVESTMENT PLAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9780564263322884,
			"nilai_komparatif_unit": 0.9849105411522425,
			"summary_team": 0.9454545454545454
		}]
	},
	"880084": {
		"nik": "880084",
		"nama": "ANDRE SAUT FAJAR HAMONANGAN HUTABARAT",
		"band": "IV",
		"posisi": "SR OFFICER BUSINESS DEVELOPMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7966666666666664,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9414225941422597,
			"nilai_komparatif_unit": 0.9480199829846883,
			"summary_team": 0.75
		}]
	},
	"880086": {
		"nik": "880086",
		"nama": "RADITHYA INDRA PRAYOGA",
		"band": "III",
		"posisi": "MGR WHOLESALE SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8452380952380943,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0095774647887334,
			"nilai_komparatif_unit": 1.0149273537559111,
			"summary_team": 0.8533333333333334
		}]
	},
	"885980": {
		"nik": "885980",
		"nama": "SILVY YUSWANTI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7716049382716039,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9216000000000011,
			"nilai_komparatif_unit": 0.9280584742229634,
			"summary_team": 0.711111111111111
		}]
	},
	"886101": {
		"nik": "886101",
		"nama": "FATHUL AMIN AS'AD",
		"band": "IV",
		"posisi": "OFF 1 OTT SERVICE FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7833333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8794326241134749,
			"nilai_komparatif_unit": 0.8855955938765706,
			"summary_team": 0.6888888888888889
		}]
	},
	"890011": {
		"nik": "890011",
		"nama": "BHRENDA PITALOKA ASTANINGTYAS MARHAEN",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8452380952380943,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9464788732394377,
			"nilai_komparatif_unit": 0.9514943941461669,
			"summary_team": 0.8
		}]
	},
	"890031": {
		"nik": "890031",
		"nama": "ZAKI MUJAHID",
		"band": "III",
		"posisi": "MGR ACCESS NEW FTTH & MODERNIZATION DEPL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8388888888888882,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.049006622516557,
			"nilai_komparatif_unit": 1.0545654519794079,
			"summary_team": 0.88
		}]
	},
	"890039": {
		"nik": "890039",
		"nama": "RIFFA RUFAIDA",
		"band": "IV",
		"posisi": "OFF 1 CRO EVALUATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8565656565656555,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.01179245283019,
			"nilai_komparatif_unit": 1.0188829861153372,
			"summary_team": 0.8666666666666667
		}]
	},
	"890040": {
		"nik": "890040",
		"nama": "AGUNG SANGGABUANA",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER SERVICE PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8000000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0312499999999996,
			"nilai_komparatif_unit": 1.0384768896944765,
			"summary_team": 0.825
		}]
	},
	"890041": {
		"nik": "890041",
		"nama": "HAFIDH AL AFIF",
		"band": "IV",
		"posisi": "OFF 1 OTT & CDN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8260869565217385,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.022222222222223,
			"nilai_komparatif_unit": 1.0293858462156709,
			"summary_team": 0.8444444444444444
		}]
	},
	"890056": {
		"nik": "890056",
		"nama": "ALYNDA NOVITA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8894308943089421,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9636980934969975,
			"nilai_komparatif_unit": 0.9704515866562513,
			"summary_team": 0.857142857142857
		}]
	},
	"890059": {
		"nik": "890059",
		"nama": "YUDHA BESTARI",
		"band": "IV",
		"posisi": "OFFICER CORPORATE LEGAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9948979591836733,
			"nilai_komparatif_unit": 1.0018700976644301,
			"summary_team": 0.9285714285714284
		}]
	},
	"890088": {
		"nik": "890088",
		"nama": "NICKY SATRIO SUGIHARTO",
		"band": "IV",
		"posisi": "SR OFFICER PARTNERSHIP & SOURCING III",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9166666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9636363636363634,
			"nilai_komparatif_unit": 0.9703894241993567,
			"summary_team": 0.8833333333333333
		}]
	},
	"890089": {
		"nik": "890089",
		"nama": "DICKY HIDAYAT",
		"band": "IV",
		"posisi": "SR OFFICER PRODUCT PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7333333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9545454545454543,
			"nilai_komparatif_unit": 0.9612348069899288,
			"summary_team": 0.7
		}]
	},
	"896229": {
		"nik": "896229",
		"nama": "ANINDITO WISNU SAMPURNO",
		"band": "IV",
		"posisi": "OFF 1 BROADCAST INFRA & FACILITY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7916666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0827067669172932,
			"nilai_komparatif_unit": 1.0902942601303598,
			"summary_team": 0.857142857142857
		}]
	},
	"896255": {
		"nik": "896255",
		"nama": "YUDHI SUKMA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9060109289617485,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.040668619679476,
			"nilai_komparatif_unit": 1.0479615140532237,
			"summary_team": 0.9428571428571426
		}]
	},
	"900014": {
		"nik": "900014",
		"nama": "RATIH AIDA MAYANGSARI",
		"band": "IV",
		"posisi": "OFF 1 TRAFFIC BUSINESS SOLUTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0344827586206897,
			"nilai_komparatif_unit": 1.041732303141795,
			"summary_team": 1
		}]
	},
	"900015": {
		"nik": "900015",
		"nama": "FATARDHI RIZKY ANDHIKA",
		"band": "IV",
		"posisi": "OFF 1 DIGITAL SERVICE OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9200000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9420289855072461,
			"nilai_komparatif_unit": 0.9486306238754991,
			"summary_team": 0.8666666666666667
		}]
	},
	"900096": {
		"nik": "900096",
		"nama": "AGA CHRISTIE NUR VANTOKO",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9060109289617485,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9460623815267964,
			"nilai_komparatif_unit": 0.9526922855029305,
			"summary_team": 0.857142857142857
		}]
	},
	"900105": {
		"nik": "900105",
		"nama": "FIKRI AKHMAD BADRIAN",
		"band": "IV",
		"posisi": "OFF 1 INFRASTRUCTURE & PARTNERSHIP SUPP",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8600000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9767441860465114,
			"nilai_komparatif_unit": 0.9835891048269039,
			"summary_team": 0.84
		}]
	},
	"906371": {
		"nik": "906371",
		"nama": "BUNGA POETRI DEWI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.897435897435897,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1142857142857145,
			"nilai_komparatif_unit": 1.1220945093841623,
			"summary_team": 0.9999999999999998
		}]
	},
	"910002": {
		"nik": "910002",
		"nama": "DANAR ADIWENA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8894308943089421,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9476364586053809,
			"nilai_komparatif_unit": 0.9542773935453138,
			"summary_team": 0.8428571428571427
		}]
	},
	"910068": {
		"nik": "910068",
		"nama": "NADESIA GEOGEMA",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER PROFILE DATA ANALYTIC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999999,
			"nilai_komparatif_unit": 1.0070078930370685,
			"summary_team": 0.8533333333333334
		}]
	},
	"910082": {
		"nik": "910082",
		"nama": "HERU PRASETYO",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8894308943089421,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0600679028466975,
			"nilai_komparatif_unit": 1.0674967453218767,
			"summary_team": 0.9428571428571428
		}]
	},
	"910083": {
		"nik": "910083",
		"nama": "ROMY WILLY CAHYO SOFANTRI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9060109289617485,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0128432555169236,
			"nilai_komparatif_unit": 1.0199411527149025,
			"summary_team": 0.9176470588235295
		}]
	},
	"910100": {
		"nik": "910100",
		"nama": "DEBBIE",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144382,
			"nilai_komparatif_unit": 1.0339332377706796,
			"summary_team": 0.8727272727272727
		}]
	},
	"910103": {
		"nik": "910103",
		"nama": "PANDE PUTU AGUS HENDRAWAN",
		"band": "IV",
		"posisi": "OFF 1 BUSINESS SOLUTION ANALYSIS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8844444444444437,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0375406443984636,
			"nilai_komparatif_unit": 1.0448116182560192,
			"summary_team": 0.9176470588235293
		}]
	},
	"910120": {
		"nik": "910120",
		"nama": "TEUKU IBNU SINA",
		"band": "IV",
		"posisi": "OFF 1 SALES PERFORMANCE & OFFERING MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333326,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9056603773584914,
			"nilai_komparatif_unit": 0.9120071484109308,
			"summary_team": 0.8
		}]
	},
	"910135": {
		"nik": "910135",
		"nama": "DZIKRILLA QALBI, ST, M.Sc.",
		"band": "IV",
		"posisi": "OFF 1 DIGITAL SOLUTION & PARTNERSHIP",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8727272727272727,
			"nilai_komparatif_unit": 0.878843252105078,
			"summary_team": 0.7999999999999999
		}]
	},
	"910139": {
		"nik": "910139",
		"nama": "FIRAS ZULKARNAIN",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.897435897435897,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.035630252100841,
			"nilai_komparatif_unit": 1.0428878381335158,
			"summary_team": 0.9294117647058824
		}]
	},
	"910146": {
		"nik": "910146",
		"nama": "YUSFITA DYAH FANANI",
		"band": "IV",
		"posisi": "OFF 1 RESOURCES & INTERFACE MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139859,
			"nilai_komparatif_unit": 0.9929238665610255,
			"summary_team": 0.8545454545454546
		}]
	},
	"910197": {
		"nik": "910197",
		"nama": "NUGROHO ADI SAPUTRO",
		"band": "IV",
		"posisi": "SR OFFICER SERVICE PLATFORM READINESS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.88,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9659090909090907,
			"nilai_komparatif_unit": 0.9726780785017137,
			"summary_team": 0.8499999999999999
		}]
	},
	"910209": {
		"nik": "910209",
		"nama": "TAUFIQ HARTANTYO",
		"band": "IV",
		"posisi": "OFF 1 REGULATORY MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8869565217391299,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.02723311546841,
			"nilai_komparatif_unit": 1.0344318552657472,
			"summary_team": 0.911111111111111
		}]
	},
	"916462": {
		"nik": "916462",
		"nama": "PUJI SUHARMANTO",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9500000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9760765550239234,
			"nilai_komparatif_unit": 0.9829167951175214,
			"summary_team": 0.9272727272727274
		}]
	},
	"920059": {
		"nik": "920059",
		"nama": "FITRI IDAYANA",
		"band": "IV",
		"posisi": "OFF 1 SLA & SLG",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.048951048951049,
			"nilai_komparatif_unit": 1.0563019857032188,
			"summary_team": 0.9090909090909092
		}]
	},
	"920071": {
		"nik": "920071",
		"nama": "AHMAD WILDAN SYARWANI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9060109289617485,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9349322358617753,
			"nilai_komparatif_unit": 0.9414841409676019,
			"summary_team": 0.8470588235294116
		}]
	},
	"920095": {
		"nik": "920095",
		"nama": "FRESSILYA CAROLINE TAMPUBOLON",
		"band": "IV",
		"posisi": "OFF 1 CX PLANNING & PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8407407407407405,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0044052863436126,
			"nilai_komparatif_unit": 1.0114440511561749,
			"summary_team": 0.8444444444444444
		}]
	},
	"920138": {
		"nik": "920138",
		"nama": "NINDYA KUSUMA WARDHANI",
		"band": "IV",
		"posisi": "OFF 1 SYNERGY EXECUTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333328,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9877232142857147,
			"nilai_komparatif_unit": 0.9946450729216586,
			"summary_team": 0.8428571428571427
		}]
	},
	"920149": {
		"nik": "920149",
		"nama": "RIZKI BINTA PUTRI",
		"band": "IV",
		"posisi": "OFF 1 QUALITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9066666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9852941176470589,
			"nilai_komparatif_unit": 0.9921989534335822,
			"summary_team": 0.8933333333333333
		}]
	},
	"920197": {
		"nik": "920197",
		"nama": "WAHYU NARENDRA JATI",
		"band": "IV",
		"posisi": "SR OFFICER CONNECTIVITY READINESS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999996,
			"nilai_komparatif_unit": 0.9667275773155855,
			"summary_team": 0.7999999999999997
		}]
	},
	"920208": {
		"nik": "920208",
		"nama": "ABDULQADIR MUHTADI",
		"band": "IV",
		"posisi": "OFF 1 BUSINESS SOLUTION ANALYSIS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8044444444444433,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0383490412739695,
			"nilai_komparatif_unit": 1.04562568029036,
			"summary_team": 0.8352941176470587
		}]
	},
	"920223": {
		"nik": "920223",
		"nama": "ROSIKA KUSHARFINNA",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER EXPERIENCE ANALYSIS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8916666666666659,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9499725123694344,
			"nilai_komparatif_unit": 0.9566298181242746,
			"summary_team": 0.8470588235294116
		}]
	},
	"930091": {
		"nik": "930091",
		"nama": "RANDY PRADANA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9294117647058822,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9976985040276182,
			"nilai_komparatif_unit": 1.004690268427087,
			"summary_team": 0.9272727272727272
		}]
	},
	"930096": {
		"nik": "930096",
		"nama": "MUHAMMAD RIFQI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.845714285714285,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7524570024570032,
			"nilai_komparatif_unit": 0.757730140645215,
			"summary_team": 0.6363636363636365
		}]
	},
	"930099": {
		"nik": "930099",
		"nama": "PUTU AMBARISHA KARISMA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8916666666666659,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0093457943925244,
			"nilai_komparatif_unit": 1.016419181757042,
			"summary_team": 0.9000000000000001
		}]
	},
	"940005": {
		"nik": "940005",
		"nama": "OVITA ELSA WANDANI",
		"band": "IV",
		"posisi": "OFF 1 COMMERCIALIZATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9807692307692306,
			"nilai_komparatif_unit": 0.9876423566325093,
			"summary_team": 0.8499999999999999
		}]
	},
	"940029": {
		"nik": "940029",
		"nama": "ILMAN ALFAFA NOVARA",
		"band": "IV",
		"posisi": "OFF 1 MANAGED SERVICE CAPACITY PLANNING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9250000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.081081081081081,
			"nilai_komparatif_unit": 1.0886571816616955,
			"summary_team": 1
		}]
	}
};