export const cbark26 = {
	"680231": {
		"nik": "680231",
		"nama": "MAHMUD",
		"band": "VI",
		"posisi": "OFF 3 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8060606060606063,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8345864661654133,
			"nilai_komparatif_unit": 0.8373001821772713,
			"summary_team": 0.6727272727272727
		}]
	},
	"690267": {
		"nik": "690267",
		"nama": "HALIMI",
		"band": "VI",
		"posisi": "OFF 3 HUMAN CAPITAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0115702479338842,
			"nilai_komparatif_unit": 1.0148594390365748,
			"summary_team": 0.9272727272727272
		}]
	},
	"690474": {
		"nik": "690474",
		"nama": "ASEP PRIYANTO",
		"band": "VI",
		"posisi": "OFF 3 HUMAN CAPITAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0384615384615388,
			"nilai_komparatif_unit": 1.0418381684679776,
			"summary_team": 0.9000000000000001
		}]
	},
	"700351": {
		"nik": "700351",
		"nama": "DJOKO TRIJANTO",
		"band": "VI",
		"posisi": "OFF 3 COMMUNICATION & SECRETARIATE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0472727272727271,
			"nilai_komparatif_unit": 1.0506780074731599,
			"summary_team": 0.8727272727272727
		}]
	},
	"700359": {
		"nik": "700359",
		"nama": "IMAM BUDIANTO",
		"band": "VI",
		"posisi": "OFF 3 HC SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8266666666666661,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.857771260997068,
			"nilai_komparatif_unit": 0.8605603639838136,
			"summary_team": 0.7090909090909091
		}]
	},
	"700587": {
		"nik": "700587",
		"nama": "BUDIANTORO",
		"band": "VI",
		"posisi": "OFF 3 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649348,
			"nilai_komparatif_unit": 0.9381053638153213,
			"summary_team": 0.7636363636363637
		}]
	},
	"880105": {
		"nik": "880105",
		"nama": "ANTONIO ERENST MIRINO",
		"band": "VI",
		"posisi": "OFF 3 OM TRANSPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666665,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0816326530612246,
			"nilai_komparatif_unit": 1.0851496569530406,
			"summary_team": 0.8833333333333333
		}]
	},
	"888326": {
		"nik": "888326",
		"nama": "ZAINAL ABUBAKAR",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.026737967914438,
			"nilai_komparatif_unit": 1.0300764779148623,
			"summary_team": 0.8727272727272727
		}]
	},
	"888337": {
		"nik": "888337",
		"nama": "IKA JULIN YONINGSI ORAY",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8564102564102566,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9841745081266035,
			"nilai_komparatif_unit": 0.9873746200735858,
			"summary_team": 0.8428571428571426
		}]
	},
	"900146": {
		"nik": "900146",
		"nama": "MUHAMMAD REZA PRIANSYAH",
		"band": "VI",
		"posisi": "OFF 3 CASH BANK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.008576329331046,
			"nilai_komparatif_unit": 1.0118557854989154,
			"summary_team": 0.8909090909090909
		}]
	},
	"910262": {
		"nik": "910262",
		"nama": "RINA ANRIANI TACOK",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER RELATIONSHIP MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0256410256410258,
			"nilai_komparatif_unit": 1.0289759688572615,
			"summary_team": 0.8888888888888891
		}]
	},
	"910272": {
		"nik": "910272",
		"nama": "ALEXANDER DIAS EKASMARA PUTRA",
		"band": "VI",
		"posisi": "OFF 3 BIDDING MANAGEMENT & OBL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8805555555555546,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9085173501577297,
			"nilai_komparatif_unit": 0.9114714575871272,
			"summary_team": 0.8
		}]
	},
	"910276": {
		"nik": "910276",
		"nama": "Dumoli Budiman Simanjuntak",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.06578947368421,
			"nilai_komparatif_unit": 1.0692549623750287,
			"summary_team": 0.8999999999999999
		}]
	},
	"910278": {
		"nik": "910278",
		"nama": "AYREN TANTRI",
		"band": "VI",
		"posisi": "OFF 3 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8083333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0571696344892219,
			"nilai_komparatif_unit": 1.0606070951726485,
			"summary_team": 0.8545454545454546
		}]
	},
	"910285": {
		"nik": "910285",
		"nama": "RATIH NURMALASARI",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER PAYMENT COLLECTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7722222222222226,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0359712230215823,
			"nilai_komparatif_unit": 1.0393397555939528,
			"summary_team": 0.8
		}]
	},
	"910286": {
		"nik": "910286",
		"nama": "VIRJEAN PRICILLIA",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333328,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0014204545454553,
			"nilai_komparatif_unit": 1.0046766428881542,
			"summary_team": 0.8545454545454546
		}]
	},
	"916695": {
		"nik": "916695",
		"nama": "VARAMITA KIRRIK",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7619047619047619,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.91875,
			"nilai_komparatif_unit": 0.9217373796029187,
			"summary_team": 0.7
		}]
	},
	"920337": {
		"nik": "920337",
		"nama": "HARISH ALFARIZI RAMADHAN",
		"band": "VI",
		"posisi": "OFF 3 APARTMENT OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8083333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0796626054358012,
			"nilai_komparatif_unit": 1.0831732035805772,
			"summary_team": 0.8727272727272728
		}]
	},
	"920343": {
		"nik": "920343",
		"nama": "WINDY MEGAYANTI",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.901960784313725,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0885375494071152,
			"nilai_komparatif_unit": 1.0920770050502282,
			"summary_team": 0.9818181818181818
		}]
	},
	"920357": {
		"nik": "920357",
		"nama": "JONATHAN PRATAMA SIMANJUNTAK",
		"band": "VI",
		"posisi": "OFF 3 TGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.848148148148148,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0075426756649466,
			"nilai_komparatif_unit": 1.0108187708359415,
			"summary_team": 0.8545454545454546
		}]
	},
	"920360": {
		"nik": "920360",
		"nama": "ZUANSAH RACHMAT MUNGGARAN",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE PL GEBANG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8507246376811579,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9403747870528126,
			"nilai_komparatif_unit": 0.9434324811566935,
			"summary_team": 0.8
		}]
	},
	"920361": {
		"nik": "920361",
		"nama": "HANIF PRAMUDYA",
		"band": "VI",
		"posisi": "OFF 3 BS SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0115702479338844,
			"nilai_komparatif_unit": 1.014859439036575,
			"summary_team": 0.9272727272727272
		}]
	},
	"920362": {
		"nik": "920362",
		"nama": "SCIVO PATRIANTO PAYUNG",
		"band": "VI",
		"posisi": "SENIOR ACCESS NETWORK SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0032515696358302,
			"summary_team": 0.8833333333333335
		}]
	},
	"920363": {
		"nik": "920363",
		"nama": "RENY RENATA YASIN",
		"band": "VI",
		"posisi": "OFF 3 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.044642857142857,
			"nilai_komparatif_unit": 1.0480395861374292,
			"summary_team": 0.9749999999999999
		}]
	},
	"920368": {
		"nik": "920368",
		"nama": "JULIANO TRISMOYOSENO",
		"band": "VI",
		"posisi": "OFF 3 BGES SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9320388349514562,
			"nilai_komparatif_unit": 0.9350694241265987,
			"summary_team": 0.8
		}]
	},
	"920370": {
		"nik": "920370",
		"nama": "IRWAN RISMAWAN",
		"band": "VI",
		"posisi": "OFF 3 COMMAND CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0505050505050504,
			"nilai_komparatif_unit": 1.0539208408295586,
			"summary_team": 0.9454545454545454
		}]
	},
	"920371": {
		"nik": "920371",
		"nama": "KADEK YOGI SETIAWAN",
		"band": "VI",
		"posisi": "OFF 3 CONSTRUCTION SUPERVISION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000003,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9766233766233763,
			"nilai_komparatif_unit": 0.9797989355404465,
			"summary_team": 0.8545454545454546
		}]
	},
	"920372": {
		"nik": "920372",
		"nama": "DIANI DESI NUGRAHENI",
		"band": "VI",
		"posisi": "OFF 3 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200683,
			"nilai_komparatif_unit": 1.0531560216417284,
			"summary_team": 0.9272727272727274
		}]
	},
	"920373": {
		"nik": "920373",
		"nama": "ASRY FAIDHUL ASHAARI PINEM",
		"band": "VI",
		"posisi": "OFF 3 HEALTH & SAFETY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8724637681159415,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0774086378737548,
			"nilai_komparatif_unit": 1.080911907086046,
			"summary_team": 0.9400000000000001
		}]
	},
	"920375": {
		"nik": "920375",
		"nama": "DEXKA HADI ALFANSYAH",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE PIK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8427860696517424,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9492325855962207,
			"nilai_komparatif_unit": 0.9523190814488857,
			"summary_team": 0.8
		}]
	},
	"920376": {
		"nik": "920376",
		"nama": "NOVIYANTI K.",
		"band": "VI",
		"posisi": "OFF 3 CCAN FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000003,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.047619047619047,
			"nilai_komparatif_unit": 1.051025453904202,
			"summary_team": 0.9166666666666665
		}]
	},
	"920377": {
		"nik": "920377",
		"nama": "AZARIA NATASHA",
		"band": "VI",
		"posisi": "OFF 3 LOGISTIK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559437,
			"nilai_komparatif_unit": 0.9471256076981608,
			"summary_team": 0.8181818181818182
		}]
	},
	"920380": {
		"nik": "920380",
		"nama": "ARGA ADIPUTRA TANATI TAHIR",
		"band": "VI",
		"posisi": "OFF 3 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888893,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.920454545454545,
			"nilai_komparatif_unit": 0.9234474675057066,
			"summary_team": 0.8181818181818181
		}]
	},
	"920383": {
		"nik": "920383",
		"nama": "EWIS ELSITA AKHAHO TAIME",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0816326530612241,
			"nilai_komparatif_unit": 1.08514965695304,
			"summary_team": 0.8833333333333333
		}]
	},
	"920384": {
		"nik": "920384",
		"nama": "GLEN HASLLER SAJORI",
		"band": "VI",
		"posisi": "OFF 3 OM LOKASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999999,
			"nilai_komparatif_unit": 1.0032515696358297,
			"summary_team": 0.7999999999999998
		}]
	},
	"920385": {
		"nik": "920385",
		"nama": "MAUREN PATRICK MANSNEMBRA",
		"band": "VI",
		"posisi": "OFF 3 OM LOKASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8606060606060609,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0457746478873235,
			"nilai_komparatif_unit": 1.0491750569783147,
			"summary_team": 0.8999999999999999
		}]
	},
	"928692": {
		"nik": "928692",
		"nama": "AHMAD NAUVAL MAULANA",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8547008547008538,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8083636363636373,
			"nilai_komparatif_unit": 0.8109920870183464,
			"summary_team": 0.6909090909090909
		}]
	},
	"928721": {
		"nik": "928721",
		"nama": "POULEN REVALANI KAUNANG",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0334928229665068,
			"nilai_komparatif_unit": 1.036853296848513,
			"summary_team": 0.8727272727272727
		}]
	},
	"928726": {
		"nik": "928726",
		"nama": "ANDY REZEKI KARUNIA VIDY",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8323232323232315,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8082524271844669,
			"nilai_komparatif_unit": 0.8108805162347857,
			"summary_team": 0.6727272727272727
		}]
	},
	"930405": {
		"nik": "930405",
		"nama": "RAYA AMBARA",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8250000000000003,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.12396694214876,
			"nilai_komparatif_unit": 1.1276215989295275,
			"summary_team": 0.9272727272727272
		}]
	},
	"930412": {
		"nik": "930412",
		"nama": "RINA SULISTYA PUSPASARI",
		"band": "VI",
		"posisi": "OFF 3 DATA CUSTOMER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8857142857142859,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0161290322580645,
			"nilai_komparatif_unit": 1.01943304656544,
			"summary_team": 0.9000000000000001
		}]
	},
	"930429": {
		"nik": "930429",
		"nama": "FADHIL ARIQ SAPUTRA",
		"band": "VI",
		"posisi": "OFF 3 BS TERRITORY SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7416666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0051072522982636,
			"nilai_komparatif_unit": 1.008375428520589,
			"summary_team": 0.7454545454545455
		}]
	},
	"930440": {
		"nik": "930440",
		"nama": "BAGAS WARA RACHMAT RAMADHAN",
		"band": "VI",
		"posisi": "OFF 3 CCAN ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0102673148780383,
			"summary_team": 0.8727272727272727
		}]
	},
	"930441": {
		"nik": "930441",
		"nama": "IQBAL RIFQI",
		"band": "VI",
		"posisi": "OFF 3 WIFI FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181817,
			"nilai_komparatif_unit": 0.9850106320060874,
			"summary_team": 0.8181818181818181
		}]
	},
	"930444": {
		"nik": "930444",
		"nama": "HEPSY LYDIA SINA",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9807692307692303,
			"nilai_komparatif_unit": 0.9839582702197558,
			"summary_team": 0.8499999999999999
		}]
	},
	"930454": {
		"nik": "930454",
		"nama": "RISA HASHIMOTO",
		"band": "VI",
		"posisi": "OFF 3 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0032515696358302,
			"summary_team": 0.8
		}]
	},
	"930455": {
		"nik": "930455",
		"nama": "CLAUDIA CHASTOLIA",
		"band": "VI",
		"posisi": "OFF 3 ACCESS PARTNERSHIP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8222222222222224,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.094594594594594,
			"nilai_komparatif_unit": 1.0981537451419214,
			"summary_team": 0.8999999999999998
		}]
	},
	"935811": {
		"nik": "935811",
		"nama": "FATAHILLAH",
		"band": "VI",
		"posisi": "OFF 3 DAVAL & INVENTORY MGT PLAN & RVW",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8799999999999994,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9297520661157032,
			"nilai_komparatif_unit": 0.9327752197027351,
			"summary_team": 0.8181818181818182
		}]
	},
	"936128": {
		"nik": "936128",
		"nama": "MUHAMMAD ARIF SYAHBANA",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8361904761904755,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8914889211016781,
			"nilai_komparatif_unit": 0.8943876594082111,
			"summary_team": 0.7454545454545455
		}]
	},
	"936156": {
		"nik": "936156",
		"nama": "HANIF WICAKSONO",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8361904761904755,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9349761855456624,
			"nilai_komparatif_unit": 0.9380163257208067,
			"summary_team": 0.7818181818181819
		}]
	},
	"940345": {
		"nik": "940345",
		"nama": "ALPHONSO YANSEN P",
		"band": "VI",
		"posisi": "OFF 3 PUSH CHANNEL INTEGRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.792592592592593,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.914719626168224,
			"nilai_komparatif_unit": 0.9176939007299703,
			"summary_team": 0.7250000000000001
		}]
	},
	"940355": {
		"nik": "940355",
		"nama": "INDRAWATI",
		"band": "VI",
		"posisi": "OFF 3 SALES PROMOTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7583333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0309690309690305,
			"nilai_komparatif_unit": 1.0343212985656103,
			"summary_team": 0.7818181818181817
		}]
	},
	"940375": {
		"nik": "940375",
		"nama": "FACHRI RAMADHAN",
		"band": "VI",
		"posisi": "OFF 3 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8999999999999991,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0505050505050515,
			"nilai_komparatif_unit": 1.0539208408295597,
			"summary_team": 0.9454545454545454
		}]
	},
	"940376": {
		"nik": "940376",
		"nama": "AGIL RAMADHAN PRIMASTO",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE JABABEKA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8629629629629626,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1008583690987128,
			"nilai_komparatif_unit": 1.1044378867450235,
			"summary_team": 0.95
		}]
	},
	"940401": {
		"nik": "940401",
		"nama": "MUHAMMAD TAUFIQ AL KAUTSAR",
		"band": "VI",
		"posisi": "OFF 3 ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0467289719626167,
			"nilai_komparatif_unit": 1.0501324841047939,
			"summary_team": 0.9333333333333335
		}]
	},
	"940409": {
		"nik": "940409",
		"nama": "INES VISYERI YULIANI",
		"band": "VI",
		"posisi": "OFF 3 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8484848484848488,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.028571428571428,
			"nilai_komparatif_unit": 1.031915900196853,
			"summary_team": 0.8727272727272727
		}]
	},
	"940411": {
		"nik": "940411",
		"nama": "ANGGAINA ELFANDORA CAHYANTARI",
		"band": "VI",
		"posisi": "OFF 3 BS TERRITORY SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9532215357458075,
			"nilai_komparatif_unit": 0.9563210019476577,
			"summary_team": 0.8181818181818182
		}]
	},
	"940417": {
		"nik": "940417",
		"nama": "YESSI VELINI POYK",
		"band": "VI",
		"posisi": "OFF 3 PLASA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.901960784313725,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0482213438735184,
			"nilai_komparatif_unit": 1.0516297085668864,
			"summary_team": 0.9454545454545454
		}]
	},
	"940422": {
		"nik": "940422",
		"nama": "MUHAMMAD RIFQI SURYAMIHARDJA",
		"band": "VI",
		"posisi": "OFF 3 SALES AREA BUNTOK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999997,
			"nilai_komparatif_unit": 0.9631215068503964,
			"summary_team": 0.8
		}]
	},
	"940423": {
		"nik": "940423",
		"nama": "MICHAEL SAPUTRA JUNIANTO SOEDARGO",
		"band": "VI",
		"posisi": "OFF 3 OSS APPLICATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9827770478065272,
			"summary_team": 0.8
		}]
	},
	"940424": {
		"nik": "940424",
		"nama": "SAUT PANGERAN DYBO PASARIBU",
		"band": "VI",
		"posisi": "OFF 3 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8250000000000003,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.057851239669421,
			"nilai_komparatif_unit": 1.0612909166395552,
			"summary_team": 0.8727272727272727
		}]
	},
	"940426": {
		"nik": "940426",
		"nama": "DWI FAJAR YULIANTO",
		"band": "VI",
		"posisi": "OFF 3 BS SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8766666666666659,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9747666781887323,
			"nilai_komparatif_unit": 0.9779361999215496,
			"summary_team": 0.8545454545454546
		}]
	},
	"940431": {
		"nik": "940431",
		"nama": "RYANDHONI FIRGIAWAN",
		"band": "VI",
		"posisi": "OFF 3 LOGISTIK & LEGAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8285714285714288,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9923371647509575,
			"nilai_komparatif_unit": 0.9955638181443672,
			"summary_team": 0.8222222222222222
		}]
	},
	"940436": {
		"nik": "940436",
		"nama": "MUJAHID SATRIO NEGORO",
		"band": "VI",
		"posisi": "OFF 3 WIRELESS OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.849999999999999,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197873,
			"nilai_komparatif_unit": 0.9656966980451851,
			"summary_team": 0.8181818181818182
		}]
	},
	"940437": {
		"nik": "940437",
		"nama": "BAIS AZI RAMADHAN NUR",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545452,
			"nilai_komparatif_unit": 1.028788882317469,
			"summary_team": 0.8545454545454546
		}]
	},
	"940438": {
		"nik": "940438",
		"nama": "HERVIN HIDAYAT PUTRA",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE RKSBITUNG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8928571428571421,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9774545454545462,
			"nilai_komparatif_unit": 0.9806328069749501,
			"summary_team": 0.8727272727272727
		}]
	},
	"940439": {
		"nik": "940439",
		"nama": "RAMADHANI ULFITA IZANTARI NAMPIRA",
		"band": "VI",
		"posisi": "OFF 3 ENTERPRISE & OLO DESIGN & ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0505050505050504,
			"nilai_komparatif_unit": 1.0539208408295586,
			"summary_team": 0.9454545454545454
		}]
	},
	"940441": {
		"nik": "940441",
		"nama": "CUT MIRANDA AYU FITRIA",
		"band": "VI",
		"posisi": "OFF 3 PUSH CHANNEL INTEGRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111115,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9290322580645157,
			"nilai_komparatif_unit": 0.9320530711455448,
			"summary_team": 0.8
		}]
	},
	"940442": {
		"nik": "940442",
		"nama": "NOFINDA GHAISANI",
		"band": "VI",
		"posisi": "OFF 3 OBL SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333329,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.895522388059702,
			"nilai_komparatif_unit": 0.8984342414649228,
			"summary_team": 0.8
		}]
	},
	"940444": {
		"nik": "940444",
		"nama": "RADITYA SHINTA HANIFATI",
		"band": "VI",
		"posisi": "OFF 3 COLLECTION PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444446,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0119617224880382,
			"nilai_komparatif_unit": 1.0152521864975024,
			"summary_team": 0.8545454545454546
		}]
	},
	"940445": {
		"nik": "940445",
		"nama": "MUHAMMAD WILDAN FATHURRIZKI",
		"band": "VI",
		"posisi": "OFF 3 CONS FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882353,
			"nilai_komparatif_unit": 0.9442367714219575,
			"summary_team": 0.8
		}]
	},
	"940446": {
		"nik": "940446",
		"nama": "ANDRIAN GANDHI",
		"band": "VI",
		"posisi": "OFF 3 BGES PROJECT MGT & QUALITY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8722222222222226,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9380428488708741,
			"nilai_komparatif_unit": 0.94109296051537,
			"summary_team": 0.8181818181818182
		}]
	},
	"940448": {
		"nik": "940448",
		"nama": "ELSYA SAKTIA NINGTIAS",
		"band": "VI",
		"posisi": "OFF 3 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96,
			"nilai_komparatif_unit": 0.9631215068503967,
			"summary_team": 0.8
		}]
	},
	"940451": {
		"nik": "940451",
		"nama": "FRENDY WILTOCS SIRAS BERHITU",
		"band": "VI",
		"posisi": "OFF 3 OM LOKASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7575757575757579,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1439999999999992,
			"nilai_komparatif_unit": 1.1477197956633887,
			"summary_team": 0.8666666666666665
		}]
	},
	"940452": {
		"nik": "940452",
		"nama": "NOVELLA ORPA RUMERE,SH",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7958333333333337,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.101892871526379,
			"nilai_komparatif_unit": 1.1054757529293715,
			"summary_team": 0.8769230769230769
		}]
	},
	"946030": {
		"nik": "946030",
		"nama": "RIVALDY NIADI PUTRA",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0256410256410253,
			"nilai_komparatif_unit": 1.028975968857261,
			"summary_team": 0.8888888888888891
		}]
	},
	"946031": {
		"nik": "946031",
		"nama": "FATIAH WISMI WATI",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8388888888888891,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0186634557495482,
			"nilai_komparatif_unit": 1.021975710911393,
			"summary_team": 0.8545454545454546
		}]
	},
	"946051": {
		"nik": "946051",
		"nama": "ARIEF REKA MAULANA",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER-2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8805555555555546,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9704617149412113,
			"nilai_komparatif_unit": 0.9736172387862495,
			"summary_team": 0.8545454545454546
		}]
	},
	"950186": {
		"nik": "950186",
		"nama": "ASSYIFA REYHANNISA",
		"band": "VI",
		"posisi": "OFF 3 PLASA BSD",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333332,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.012987012987013,
			"nilai_komparatif_unit": 1.0162808107999317,
			"summary_team": 0.9454545454545454
		}]
	},
	"950201": {
		"nik": "950201",
		"nama": "AYYUB SETIYOSO",
		"band": "VI",
		"posisi": "OFF 3 FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888893,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272723,
			"nilai_komparatif_unit": 1.0260527416730074,
			"summary_team": 0.9090909090909092
		}]
	},
	"950240": {
		"nik": "950240",
		"nama": "RIZAL HAERUL AKBAR",
		"band": "VI",
		"posisi": "OFF 3 OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888893,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9409090909090905,
			"nilai_komparatif_unit": 0.9439685223391668,
			"summary_team": 0.8363636363636363
		}]
	},
	"950283": {
		"nik": "950283",
		"nama": "BOBY BAGUS SETIAWAN",
		"band": "VI",
		"posisi": "OFF 3 TGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8453333333333325,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0108976197304285,
			"nilai_komparatif_unit": 1.0141846237356766,
			"summary_team": 0.8545454545454546
		}]
	},
	"950285": {
		"nik": "950285",
		"nama": "SATRIO WIDIANTO",
		"band": "VI",
		"posisi": "OFF 3 SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8512820512820554,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9397590361445741,
			"nilai_komparatif_unit": 0.9428147280914986,
			"summary_team": 0.8000000000000003
		}]
	},
	"950286": {
		"nik": "950286",
		"nama": "ERSY BELLAKANO SINGKALI",
		"band": "VI",
		"posisi": "OFF 3 MICRO DEMAND",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9674099485420241,
			"nilai_komparatif_unit": 0.9705555493561031,
			"summary_team": 0.8545454545454546
		}]
	},
	"950304": {
		"nik": "950304",
		"nama": "PRADIPTA PRAYOGA NUGRAHA",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CIPONDOH",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666656,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930082,
			"nilai_komparatif_unit": 1.0102673148780397,
			"summary_team": 0.8727272727272727
		}]
	},
	"950319": {
		"nik": "950319",
		"nama": "RONNIE TJANDRA",
		"band": "VI",
		"posisi": "OFF 3 WHOLESALE & WIFI ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0167696381288611,
			"nilai_komparatif_unit": 1.0200757354108347,
			"summary_team": 0.8727272727272727
		}]
	},
	"950322": {
		"nik": "950322",
		"nama": "FERNANDA PANDU SAPUTRA",
		"band": "VI",
		"posisi": "OFF 3 DAVAL & INVENTORY MGT PLAN & REV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.986013986013986,
			"nilai_komparatif_unit": 0.9892200791514127,
			"summary_team": 0.8545454545454546
		}]
	},
	"950324": {
		"nik": "950324",
		"nama": "RISA MUTIARA",
		"band": "VI",
		"posisi": "OFF 3 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8200000000000003,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.086474501108647,
			"nilai_komparatif_unit": 1.0900072486065553,
			"summary_team": 0.8909090909090909
		}]
	},
	"950325": {
		"nik": "950325",
		"nama": "AGNIA INDRA WARDHANA",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7692307692307695,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9454545454545451,
			"nilai_komparatif_unit": 0.9485287567466024,
			"summary_team": 0.7272727272727273
		}]
	},
	"950326": {
		"nik": "950326",
		"nama": "MOHAMMAD IMAM KHARIZ",
		"band": "VI",
		"posisi": "OFF 3 OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0115702479338844,
			"nilai_komparatif_unit": 1.014859439036575,
			"summary_team": 0.9272727272727272
		}]
	},
	"950329": {
		"nik": "950329",
		"nama": "FEBRIAN ULFA AYU ROMADHANI",
		"band": "VI",
		"posisi": "OFF 3 APARTEMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559442,
			"nilai_komparatif_unit": 0.9471256076981612,
			"summary_team": 0.8181818181818182
		}]
	},
	"950335": {
		"nik": "950335",
		"nama": "DENNY ANGGA SETYAWAN",
		"band": "VI",
		"posisi": "OFF 3 ACCESS PROVISIONING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828873,
			"nilai_komparatif_unit": 1.008616551291636,
			"summary_team": 0.8545454545454546
		}]
	},
	"950341": {
		"nik": "950341",
		"nama": "MUHAMMAD RAYHAN HASIBUAN",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE KEMAYORAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8577777777777781,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9326424870466319,
			"nilai_komparatif_unit": 0.9356750390385976,
			"summary_team": 0.8
		}]
	},
	"950344": {
		"nik": "950344",
		"nama": "FRISTHA PRATIWI",
		"band": "VI",
		"posisi": "OFF 3 QUALITY & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8529914529914517,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0231371834578262,
			"nilai_komparatif_unit": 1.0264639852568462,
			"summary_team": 0.8727272727272728
		}]
	},
	"950358": {
		"nik": "950358",
		"nama": "TITIS MUTIARA",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER-2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8805555555555546,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0737023229136804,
			"nilai_komparatif_unit": 1.0771935407847866,
			"summary_team": 0.9454545454545454
		}]
	},
	"950360": {
		"nik": "950360",
		"nama": "BAHADURI AZKA ZUHAIR",
		"band": "VI",
		"posisi": "OFF 3 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9056603773584903,
			"nilai_komparatif_unit": 0.9086051951418834,
			"summary_team": 0.8
		}]
	},
	"950363": {
		"nik": "950363",
		"nama": "DINY HAFIZHA AMELIA",
		"band": "VI",
		"posisi": "OFF 3 NTE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333331,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9320388349514567,
			"nilai_komparatif_unit": 0.9350694241265991,
			"summary_team": 0.8
		}]
	},
	"950368": {
		"nik": "950368",
		"nama": "GELLA ARADEA PUTRI",
		"band": "VI",
		"posisi": "OFF 3 WIRELESS OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.849999999999999,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197873,
			"nilai_komparatif_unit": 0.9656966980451851,
			"summary_team": 0.8181818181818182
		}]
	},
	"950370": {
		"nik": "950370",
		"nama": "I WAYAN WIRASA",
		"band": "VI",
		"posisi": "OFF 3 PLASA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9333333333333333,
			"nilai_komparatif_unit": 0.9363681316601079,
			"summary_team": 0.8400000000000001
		}]
	},
	"950375": {
		"nik": "950375",
		"nama": "MUHAMMAD HUSAINI",
		"band": "VI",
		"posisi": "OFF 3 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9407839866555462,
			"nilai_komparatif_unit": 0.9438430113004304,
			"summary_team": 0.8545454545454546
		}]
	},
	"950379": {
		"nik": "950379",
		"nama": "MUHAMMAD AKMAL AL HAKIM",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7692307692307695,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9454545454545451,
			"nilai_komparatif_unit": 0.9485287567466024,
			"summary_team": 0.7272727272727273
		}]
	},
	"950381": {
		"nik": "950381",
		"nama": "VIRGIAWAN LISTANTO RAHAGUNG",
		"band": "VI",
		"posisi": "SR ACCESS NET SUPERVISORY TANJUNG REDEB",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8716049382716041,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0012876641771835,
			"nilai_komparatif_unit": 1.004543420742753,
			"summary_team": 0.8727272727272728
		}]
	},
	"950384": {
		"nik": "950384",
		"nama": "RAMADITYA PUTRA",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE KOTA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8739583333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.040199371546213,
			"nilai_komparatif_unit": 1.043581652237942,
			"summary_team": 0.9090909090909092
		}]
	},
	"950390": {
		"nik": "950390",
		"nama": "DANIA",
		"band": "VI",
		"posisi": "OFF 3 PAYMENT & COLLECTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190465,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.022471910112361,
			"nilai_komparatif_unit": 1.0257965487287712,
			"summary_team": 0.8666666666666667
		}]
	},
	"950391": {
		"nik": "950391",
		"nama": "MUHAMMAD IQBAL RABBANI",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CIBITUNG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8629629629629626,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0594727161250772,
			"nilai_komparatif_unit": 1.0629176654388197,
			"summary_team": 0.9142857142857145
		}]
	},
	"950396": {
		"nik": "950396",
		"nama": "TANTOWI JAUHARI",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE PANDEGLG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8928571428571421,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.916363636363637,
			"nilai_komparatif_unit": 0.9193432565390157,
			"summary_team": 0.8181818181818181
		}]
	},
	"950398": {
		"nik": "950398",
		"nama": "DITA NUR SYARAFINA",
		"band": "VI",
		"posisi": "OFF 3 SALES MUARA JAWA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9473684210526313,
			"nilai_komparatif_unit": 0.9504488554444702,
			"summary_team": 0.8
		}]
	},
	"950400": {
		"nik": "950400",
		"nama": "RIDWAN FIRDAUS",
		"band": "VI",
		"posisi": "OFF 3 CCAN FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144382,
			"nilai_komparatif_unit": 1.0300764779148626,
			"summary_team": 0.8727272727272727
		}]
	},
	"950410": {
		"nik": "950410",
		"nama": "ALFIN FAHMI ILMA MAFA'ID",
		"band": "VI",
		"posisi": "OFF 3 WIFI FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8250000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9476584022038566,
			"nilai_komparatif_unit": 0.9507397794896018,
			"summary_team": 0.7818181818181817
		}]
	},
	"950414": {
		"nik": "950414",
		"nama": "EKA ADITYA CHANDRA KUSUMA",
		"band": "VI",
		"posisi": "OFF 3 BUSINESS SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9555555555555555,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.046511627906977,
			"nilai_komparatif_unit": 1.0499144333398223,
			"summary_team": 1.0000000000000002
		}]
	},
	"950415": {
		"nik": "950415",
		"nama": "MOCHAMAD RAFLI ANDRIANSYAH",
		"band": "VI",
		"posisi": "OFF 3 DESIGN & ENGINEERING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930064,
			"nilai_komparatif_unit": 1.010267314878038,
			"summary_team": 0.8727272727272727
		}]
	},
	"950416": {
		"nik": "950416",
		"nama": "TRAFIKO BAYU WICAKSONO",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER-2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.847222222222222,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9657228017883759,
			"nilai_komparatif_unit": 0.9688629167272995,
			"summary_team": 0.8181818181818182
		}]
	},
	"950424": {
		"nik": "950424",
		"nama": "RIFQI AMIR HAMZAH",
		"band": "VI",
		"posisi": "OFF 3 DAVAL & INVENTORY MGT PLAN & RVW",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139859,
			"nilai_komparatif_unit": 0.9892200791514126,
			"summary_team": 0.8545454545454546
		}]
	},
	"950430": {
		"nik": "950430",
		"nama": "SYARIF ZAKY DARMAWAN",
		"band": "VI",
		"posisi": "OFF 3 AREA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9766233766233766,
			"nilai_komparatif_unit": 0.9797989355404468,
			"summary_team": 0.8545454545454546
		}]
	},
	"950431": {
		"nik": "950431",
		"nama": "NURUL FAWZIA",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8708333333333332,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0230535015224012,
			"nilai_komparatif_unit": 1.026380031223781,
			"summary_team": 0.8909090909090909
		}]
	},
	"950432": {
		"nik": "950432",
		"nama": "WULAN SETYA NINGSIH",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197858,
			"nilai_komparatif_unit": 0.9656966980451837,
			"summary_team": 0.8181818181818181
		}]
	},
	"950434": {
		"nik": "950434",
		"nama": "SODIPTA BADIA BANUREA",
		"band": "VI",
		"posisi": "OFF 3 OM TRANSPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7916666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0947368421052626,
			"nilai_komparatif_unit": 1.0982964551802763,
			"summary_team": 0.8666666666666665
		}]
	},
	"950435": {
		"nik": "950435",
		"nama": "ANDRE YANA WIJAYA",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8732673267326762,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0687830687830653,
			"nilai_komparatif_unit": 1.0722582913568095,
			"summary_team": 0.9333333333333333
		}]
	},
	"950438": {
		"nik": "950438",
		"nama": "KARYAS LEO SWOM",
		"band": "VI",
		"posisi": "OFF 3 ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8296296296296299,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.964285714285714,
			"nilai_komparatif_unit": 0.96742115643455,
			"summary_team": 0.8
		}]
	},
	"950441": {
		"nik": "950441",
		"nama": "ALIEF IMRON JULIODINATA",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200685,
			"nilai_komparatif_unit": 1.0531560216417286,
			"summary_team": 0.9272727272727274
		}]
	},
	"950442": {
		"nik": "950442",
		"nama": "VRISKA KEMALA THAYEB",
		"band": "VI",
		"posisi": "OFF 3 EMPLOYEE & INDUSTRIAL RELATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7866666666666663,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9887005649717517,
			"nilai_komparatif_unit": 0.9919153937077417,
			"summary_team": 0.7777777777777777
		}]
	},
	"950446": {
		"nik": "950446",
		"nama": "ALFONSUS SASANDO NARWASTU KUSUMA",
		"band": "VI",
		"posisi": "OFF 3 SALES MUARATEWEH",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545452,
			"nilai_komparatif_unit": 1.028788882317469,
			"summary_team": 0.8545454545454546
		}]
	},
	"950454": {
		"nik": "950454",
		"nama": "SAFITRI PUTRI ANJASARI",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8222222222222221,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.127764127764128,
			"nilai_komparatif_unit": 1.131431131358344,
			"summary_team": 0.9272727272727272
		}]
	},
	"950458": {
		"nik": "950458",
		"nama": "ZULVI SABILA OCTAVIANI",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL & WIFI SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006993006993007,
			"nilai_komparatif_unit": 1.0102673148780386,
			"summary_team": 0.8727272727272727
		}]
	},
	"950462": {
		"nik": "950462",
		"nama": "NAOMI HURAYAH ADEN",
		"band": "VI",
		"posisi": "OFF 3 QUALITY & PROJECT MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8791666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.054717794054287,
			"nilai_komparatif_unit": 1.0581472824078033,
			"summary_team": 0.9272727272727274
		}]
	},
	"950463": {
		"nik": "950463",
		"nama": "REYMON AGRA MEDIKA",
		"band": "VI",
		"posisi": "OFF 3 GOVERNMENT ACCOUNT MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7703703703703708,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0856643356643352,
			"nilai_komparatif_unit": 1.0891944488528849,
			"summary_team": 0.8363636363636364
		}]
	},
	"950464": {
		"nik": "950464",
		"nama": "ANDRE YOHANES ANTONI",
		"band": "VI",
		"posisi": "OFF 3 OM TRANSPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8761904761904764,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9752964426877468,
			"nilai_komparatif_unit": 0.9784676869867233,
			"summary_team": 0.8545454545454546
		}]
	},
	"950475": {
		"nik": "950475",
		"nama": "ANDALIYANTO WIBOWO",
		"band": "VI",
		"posisi": "OFF 3 BACKBONE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9128014842300556,
			"nilai_komparatif_unit": 0.9157695218197185,
			"summary_team": 0.7454545454545455
		}]
	},
	"950481": {
		"nik": "950481",
		"nama": "AVICENNA KEVAL HASSANI",
		"band": "VI",
		"posisi": "OFF 3 PLASA NUNUKAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333337,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9320388349514559,
			"nilai_komparatif_unit": 0.9350694241265983,
			"summary_team": 0.8
		}]
	},
	"950483": {
		"nik": "950483",
		"nama": "CHANDRA FITRAH SIRAIT",
		"band": "VI",
		"posisi": "OFF 3 ACCESS NE O&M",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888892,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857137,
			"nilai_komparatif_unit": 0.9674211564345497,
			"summary_team": 0.857142857142857
		}]
	},
	"950486": {
		"nik": "950486",
		"nama": "DAMARWULAN EKA AGUSTINA",
		"band": "VI",
		"posisi": "OFF 3 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8476190476190469,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0081716036772226,
			"nilai_komparatif_unit": 1.0114497438514454,
			"summary_team": 0.8545454545454546
		}]
	},
	"950488": {
		"nik": "950488",
		"nama": "DEWI RAHMAYANTI",
		"band": "VI",
		"posisi": "OFF 3 FACILITY OPTIMALIZATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0597402597402596,
			"nilai_komparatif_unit": 1.0631860789906975,
			"summary_team": 0.9272727272727274
		}]
	},
	"950498": {
		"nik": "950498",
		"nama": "INTAN PERMATA SARI",
		"band": "VI",
		"posisi": "OFF 3 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1009174311926606,
			"nilai_komparatif_unit": 1.1044971408834825,
			"summary_team": 1
		}]
	},
	"950499": {
		"nik": "950499",
		"nama": "IQBAL NASHRULLAH",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8703703703703703,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0012158054711244,
			"nilai_komparatif_unit": 1.0044713283831073,
			"summary_team": 0.8714285714285713
		}]
	},
	"950517": {
		"nik": "950517",
		"nama": "PRASETIYO LUMADI",
		"band": "VI",
		"posisi": "OFF 3 PLASA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9042386185243326,
			"nilai_komparatif_unit": 0.9071788133598712,
			"summary_team": 0.7384615384615385
		}]
	},
	"950520": {
		"nik": "950520",
		"nama": "RAHMAT IRIANTO",
		"band": "VI",
		"posisi": "OFF 3 OM LOKASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9607843137254903,
			"nilai_komparatif_unit": 0.9639083708265819,
			"summary_team": 0.8166666666666669
		}]
	},
	"955454": {
		"nik": "955454",
		"nama": "FITRAH MALIK",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8222222222222222,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9729729729729734,
			"nilai_komparatif_unit": 0.9761366623483755,
			"summary_team": 0.8000000000000003
		}]
	},
	"955909": {
		"nik": "955909",
		"nama": "MUHAMMAD MUJAHID AL ANSHORY",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8643678160919535,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7767857142857147,
			"nilai_komparatif_unit": 0.7793114871278326,
			"summary_team": 0.6714285714285714
		}]
	},
	"955912": {
		"nik": "955912",
		"nama": "YOGI MAULANA NUGROHO",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER-2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8829268292682917,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0296333500753403,
			"nilai_komparatif_unit": 1.0329812746124831,
			"summary_team": 0.9090909090909092
		}]
	},
	"960042": {
		"nik": "960042",
		"nama": "INDAH DWIYANA",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0096153846153848,
			"nilai_komparatif_unit": 1.012898219343867,
			"summary_team": 0.8750000000000002
		}]
	},
	"960080": {
		"nik": "960080",
		"nama": "ASRI NURFATHI ZAHIR",
		"band": "VI",
		"posisi": "OFF 3 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969695,
			"nilai_komparatif_unit": 0.9728500069195924,
			"summary_team": 0.8727272727272728
		}]
	},
	"960110": {
		"nik": "960110",
		"nama": "ARY KURNIAWAN",
		"band": "VI",
		"posisi": "OFF 3 REVAS & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969696,
			"nilai_komparatif_unit": 0.9728500069195926,
			"summary_team": 0.8727272727272727
		}]
	},
	"960111": {
		"nik": "960111",
		"nama": "RAIHAN HAFIDH DIRGANTARA WIDODO",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8848484848484849,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9863013698630138,
			"nilai_komparatif_unit": 0.9895083974490377,
			"summary_team": 0.8727272727272728
		}]
	},
	"960113": {
		"nik": "960113",
		"nama": "REZDI LUTHFAN PRAMANANDA",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8547008547008538,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0362857142857154,
			"nilai_komparatif_unit": 1.0396552694483312,
			"summary_team": 0.8857142857142857
		}]
	},
	"960115": {
		"nik": "960115",
		"nama": "HANNUM PRABAWATI SISWAYA",
		"band": "VI",
		"posisi": "OFF 3 QUALITY & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8684684684684674,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9211618257261422,
			"nilai_komparatif_unit": 0.924157047548359,
			"summary_team": 0.8
		}]
	},
	"960119": {
		"nik": "960119",
		"nama": "RAHMAN PARENTIO",
		"band": "VI",
		"posisi": "OFF 3 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0481283422459888,
			"nilai_komparatif_unit": 1.0515364045380886,
			"summary_team": 0.8909090909090909
		}]
	},
	"960121": {
		"nik": "960121",
		"nama": "AHMADA IRFAN SHIDQAN",
		"band": "VI",
		"posisi": "OFF 3 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8361111111111103,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0553058432675406,
			"nilai_komparatif_unit": 1.0587372437040232,
			"summary_team": 0.8823529411764706
		}]
	},
	"960126": {
		"nik": "960126",
		"nama": "AKMAL FAZA",
		"band": "VI",
		"posisi": "OFF 3 BGES ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090906,
			"nilai_komparatif_unit": 1.0944562577845414,
			"summary_team": 0.9818181818181818
		}]
	},
	"960127": {
		"nik": "960127",
		"nama": "FARRAS MUHAMMAD ISKANDAR",
		"band": "VI",
		"posisi": "OFF 3 SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.916666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0314049586776857,
			"nilai_komparatif_unit": 1.0347586437235665,
			"summary_team": 0.9454545454545454
		}]
	},
	"960136": {
		"nik": "960136",
		"nama": "WILDA MUDAWAMATUN NISA",
		"band": "VI",
		"posisi": "OFF 3 VALUE ADDED RESELLER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7619047619047619,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.05,
			"nilai_komparatif_unit": 1.0534141481176214,
			"summary_team": 0.8
		}]
	},
	"960137": {
		"nik": "960137",
		"nama": "FAISHAL AKMAL HERMAWAN",
		"band": "VI",
		"posisi": "OFF 3 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8439814814814864,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9817412428493008,
			"nilai_komparatif_unit": 0.9849334428647915,
			"summary_team": 0.8285714285714286
		}]
	},
	"960147": {
		"nik": "960147",
		"nama": "ARIF FIRDANA",
		"band": "VI",
		"posisi": "SENIOR ACCESS NETWORK SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365351,
			"nilai_komparatif_unit": 0.9912056674275094,
			"summary_team": 0.8727272727272728
		}]
	},
	"960149": {
		"nik": "960149",
		"nama": "MUHAMMAD ILHAM PRATOMO",
		"band": "VI",
		"posisi": "OFF 3 WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9827770478065272,
			"summary_team": 0.8
		}]
	},
	"960151": {
		"nik": "960151",
		"nama": "NAVENTA AUDINATA",
		"band": "VI",
		"posisi": "OFF 3 PAYMENT COLLECTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8460784313725501,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9670283366691235,
			"nilai_komparatif_unit": 0.9701726966456239,
			"summary_team": 0.8181818181818182
		}]
	},
	"960155": {
		"nik": "960155",
		"nama": "FAISAL KAMIL PERDANA",
		"band": "VI",
		"posisi": "OFF 3 CCAN FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.944055944055944,
			"nilai_komparatif_unit": 0.947125607698161,
			"summary_team": 0.8181818181818181
		}]
	},
	"960160": {
		"nik": "960160",
		"nama": "ARVIEDA NADYA ASTIN SANTOSA",
		"band": "VI",
		"posisi": "OFF 3 BIDDING MANAGEMENT & OBL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0129870129870129,
			"nilai_komparatif_unit": 1.0162808107999315,
			"summary_team": 0.9454545454545454
		}]
	},
	"960167": {
		"nik": "960167",
		"nama": "MERISA AULIA",
		"band": "VI",
		"posisi": "OFF 3 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9720972097209719,
			"nilai_komparatif_unit": 0.9752580514911756,
			"summary_team": 0.8181818181818182
		}]
	},
	"960171": {
		"nik": "960171",
		"nama": "REZA ANHARIO",
		"band": "VI",
		"posisi": "OFF 3 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8156862745098041,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8756868131868129,
			"nilai_komparatif_unit": 0.8785341698390677,
			"summary_team": 0.7142857142857142
		}]
	},
	"960173": {
		"nik": "960173",
		"nama": "DINIYAH ULYA",
		"band": "VI",
		"posisi": "OFF 3 PAYMENT & COLLECTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7666666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1014492753623188,
			"nilai_komparatif_unit": 1.105030714381494,
			"summary_team": 0.8444444444444446
		}]
	},
	"960174": {
		"nik": "960174",
		"nama": "FAZA RA'IS AGUSTIAN PUTRA",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE TEGAL ALUR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8427860696517424,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9492325855962207,
			"nilai_komparatif_unit": 0.9523190814488857,
			"summary_team": 0.8
		}]
	},
	"960175": {
		"nik": "960175",
		"nama": "DWI SETIANTO",
		"band": "VI",
		"posisi": "OFF 3 APPLICATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8549019607843138,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0846538782318598,
			"nilai_komparatif_unit": 1.0881807058477035,
			"summary_team": 0.9272727272727272
		}]
	},
	"960182": {
		"nik": "960182",
		"nama": "WIRADANA KUSUMA",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7500000000000004,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090904,
			"nilai_komparatif_unit": 1.0944562577845411,
			"summary_team": 0.8181818181818182
		}]
	},
	"960184": {
		"nik": "960184",
		"nama": "DYAH FAIRUZ",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER-2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8616666666666656,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0128362933005113,
			"nilai_komparatif_unit": 1.0161296010378738,
			"summary_team": 0.8727272727272728
		}]
	},
	"960185": {
		"nik": "960185",
		"nama": "RIFQI ADITYA RIADHI",
		"band": "VI",
		"posisi": "OFF 3 COMMAND CONTROL MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0369036903690365,
			"nilai_komparatif_unit": 1.0402752549239205,
			"summary_team": 0.8727272727272727
		}]
	},
	"960189": {
		"nik": "960189",
		"nama": "MAGHFIRA RIFKI HARIADI",
		"band": "VI",
		"posisi": "SENIOR ACCESS NETWORK SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139857,
			"nilai_komparatif_unit": 0.9892200791514124,
			"summary_team": 0.8545454545454546
		}]
	},
	"960191": {
		"nik": "960191",
		"nama": "INDAH BEATRY FEYSILYA LUSI",
		"band": "VI",
		"posisi": "OFF 3 CRM & LEVERAGING CUSTOMER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777782,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.987341772151898,
			"nilai_komparatif_unit": 0.9905521826784136,
			"summary_team": 0.8666666666666665
		}]
	},
	"960197": {
		"nik": "960197",
		"nama": "YOHANES FENDRYAN DIASTOMO",
		"band": "VI",
		"posisi": "OFF 3 COMMAND CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8200000000000003,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.997782705099778,
			"nilai_komparatif_unit": 1.0010270650468367,
			"summary_team": 0.8181818181818182
		}]
	},
	"960204": {
		"nik": "960204",
		"nama": "IKA RAHMAH MANIK",
		"band": "VI",
		"posisi": "SENIOR ACCESS NETWORK SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9125,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8966376089663761,
			"nilai_komparatif_unit": 0.8995530885900344,
			"summary_team": 0.8181818181818182
		}]
	},
	"960206": {
		"nik": "960206",
		"nama": "YOSHUA",
		"band": "VI",
		"posisi": "OFF 3 LOGISTIK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8909090909090911,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0612244897959182,
			"nilai_komparatif_unit": 1.0646751351237378,
			"summary_team": 0.9454545454545454
		}]
	},
	"960211": {
		"nik": "960211",
		"nama": "ISMAIL AHSAN MAS'UD",
		"band": "VI",
		"posisi": "OFF 3 OM LOKASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.962566844919786,
			"nilai_komparatif_unit": 0.9656966980451838,
			"summary_team": 0.8181818181818181
		}]
	},
	"960213": {
		"nik": "960213",
		"nama": "BENHARD SAUT TAHI",
		"band": "VI",
		"posisi": "OFF 3 DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8799999999999994,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9090909090909097,
			"nilai_komparatif_unit": 0.9120468814871188,
			"summary_team": 0.8
		}]
	},
	"960214": {
		"nik": "960214",
		"nama": "SITTA SUKMA MAHALIA",
		"band": "VI",
		"posisi": "OFF 3 LEVERAGING CUSTOMER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.048951048951049,
			"nilai_komparatif_unit": 1.0523617863312902,
			"summary_team": 0.9090909090909092
		}]
	},
	"960216": {
		"nik": "960216",
		"nama": "AYU LESTARI DWI PUTRI",
		"band": "VI",
		"posisi": "OFF 3 BS SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8766666666666659,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9955063947459394,
			"nilai_komparatif_unit": 0.9987433531113697,
			"summary_team": 0.8727272727272728
		}]
	},
	"960217": {
		"nik": "960217",
		"nama": "DWI NURUL CHOIRUNNISA",
		"band": "VI",
		"posisi": "OFF 3 INFRA & SERVICE SURVEILANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8470588235294104,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.944444444444446,
			"nilai_komparatif_unit": 0.9475153713227298,
			"summary_team": 0.8
		}]
	},
	"960226": {
		"nik": "960226",
		"nama": "RYAN DHARMA IMANSYAH",
		"band": "VI",
		"posisi": "OFF 3 SALES TANAHGROGOT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0765550239234447,
			"nilai_komparatif_unit": 1.0800555175505344,
			"summary_team": 0.9090909090909092
		}]
	},
	"960228": {
		"nik": "960228",
		"nama": "ANNAS FATKHURROHMAN",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL & WIFI SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333337,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0803177405119146,
			"nilai_komparatif_unit": 1.0838304688740115,
			"summary_team": 0.9272727272727272
		}]
	},
	"960229": {
		"nik": "960229",
		"nama": "ARIANTI SUHARTINI",
		"band": "VI",
		"posisi": "OFF 3 ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8805555555555548,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9842271293375403,
			"nilai_komparatif_unit": 0.9874274123860542,
			"summary_team": 0.8666666666666667
		}]
	},
	"960231": {
		"nik": "960231",
		"nama": "ARIF RAHMAN JUANDA",
		"band": "VI",
		"posisi": "OFF 3 OM LOKASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333332,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9740259740259742,
			"nilai_komparatif_unit": 0.9771930873076268,
			"summary_team": 0.9090909090909092
		}]
	},
	"960233": {
		"nik": "960233",
		"nama": "ARINDYA FANANIAR",
		"band": "VI",
		"posisi": "OFF 3 SALES PENAJAM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.011961722488038,
			"nilai_komparatif_unit": 1.0152521864975022,
			"summary_team": 0.8545454545454546
		}]
	},
	"960235": {
		"nik": "960235",
		"nama": "SEPTIAN ZAKARIA",
		"band": "VI",
		"posisi": "OFF 3 BIDDING & OFFERING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.935064935064935,
			"nilai_komparatif_unit": 0.9381053638153215,
			"summary_team": 0.7636363636363637
		}]
	},
	"960237": {
		"nik": "960237",
		"nama": "AINI FAKHRIYAH MAS`UDIN",
		"band": "VI",
		"posisi": "OFF 3 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1320754716981127,
			"nilai_komparatif_unit": 1.1357564939273541,
			"summary_team": 1
		}]
	},
	"960239": {
		"nik": "960239",
		"nama": "MUHAMMAD RAIHAN DZAKY",
		"band": "VI",
		"posisi": "OFF 3 SOLUTION INTEGRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333329,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9769335142469476,
			"nilai_komparatif_unit": 0.9801100815980976,
			"summary_team": 0.8727272727272728
		}]
	},
	"960240": {
		"nik": "960240",
		"nama": "FIKA FAJARINI",
		"band": "VI",
		"posisi": "OFF 3 VERIFICATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0115702479338844,
			"nilai_komparatif_unit": 1.014859439036575,
			"summary_team": 0.9272727272727272
		}]
	},
	"960242": {
		"nik": "960242",
		"nama": "FILLA MULYAWATI KHADIFAH",
		"band": "VI",
		"posisi": "OFF 3 LOGISTIK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8222222222222224,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9729729729729728,
			"nilai_komparatif_unit": 0.9761366623483749,
			"summary_team": 0.8
		}]
	},
	"960243": {
		"nik": "960243",
		"nama": "SRI HANDAYANI",
		"band": "VI",
		"posisi": "OFF 3 SURVEILLANCE & LOGIC NE ACCESS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0377358490566035,
			"nilai_komparatif_unit": 1.0411101194334083,
			"summary_team": 0.9166666666666667
		}]
	},
	"960249": {
		"nik": "960249",
		"nama": "RIZQI FADLILAH",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9090909090909093,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0999999999999999,
			"nilai_komparatif_unit": 1.1035767265994127,
			"summary_team": 1
		}]
	},
	"960254": {
		"nik": "960254",
		"nama": "ACHMAD ZACKY FATHONI HAQUE",
		"band": "VI",
		"posisi": "OFF 3 PUSH CHANNEL INTEGRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8250000000000003,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969694,
			"nilai_komparatif_unit": 0.9728500069195923,
			"summary_team": 0.8
		}]
	},
	"960259": {
		"nik": "960259",
		"nama": "ALMIRA PUTRI OLDIA",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9650349650349651,
			"nilai_komparatif_unit": 0.9681728434247869,
			"summary_team": 0.8363636363636363
		}]
	},
	"960262": {
		"nik": "960262",
		"nama": "ANDITA APRILINA NUGRAHENI",
		"band": "VI",
		"posisi": "OFF 3 ASSET MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7416666666666666,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9805924412665987,
			"nilai_komparatif_unit": 0.9837809058737456,
			"summary_team": 0.7272727272727273
		}]
	},
	"960263": {
		"nik": "960263",
		"nama": "ANDREW MARCHEL TAUDUFU",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0032515696358302,
			"summary_team": 0.8
		}]
	},
	"960264": {
		"nik": "960264",
		"nama": "ANGGA BAYU HARYANTO",
		"band": "VI",
		"posisi": "OFF 3 HR SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8315789473684199,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0994575045207975,
			"nilai_komparatif_unit": 1.1030324671583827,
			"summary_team": 0.9142857142857145
		}]
	},
	"960266": {
		"nik": "960266",
		"nama": "ANNISA RIFQIANA",
		"band": "VI",
		"posisi": "OFF-3 FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0102673148780383,
			"summary_team": 0.8727272727272728
		}]
	},
	"960268": {
		"nik": "960268",
		"nama": "ASTRID YUPITASARY",
		"band": "VI",
		"posisi": "OFF 3 DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0032515696358302,
			"summary_team": 0.8
		}]
	},
	"960270": {
		"nik": "960270",
		"nama": "BAGAS YUFA ARDANA",
		"band": "VI",
		"posisi": "SR ACCESS NETWORK SUPERVISORY SANGATA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.967409948542024,
			"nilai_komparatif_unit": 0.970555549356103,
			"summary_team": 0.8545454545454546
		}]
	},
	"960275": {
		"nik": "960275",
		"nama": "DANIEL DAVID HARTAMA SITOMPUL",
		"band": "VI",
		"posisi": "OFF 3 ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8460784313725501,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1174549668176537,
			"nilai_komparatif_unit": 1.1210884494571653,
			"summary_team": 0.9454545454545454
		}]
	},
	"960276": {
		"nik": "960276",
		"nama": "DIMAS IHSAN RASHIDI",
		"band": "VI",
		"posisi": "OFF 3 PLASA MALINAU",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8974358974358976,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9116883116883115,
			"nilai_komparatif_unit": 0.9146527297199383,
			"summary_team": 0.8181818181818182
		}]
	},
	"960277": {
		"nik": "960277",
		"nama": "DIMITRI ERLANGGA",
		"band": "VI",
		"posisi": "OFF 3 OM LOKASI (BJB)",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666665,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0018552875695734,
			"nilai_komparatif_unit": 1.0051128898021302,
			"summary_team": 0.8181818181818182
		}]
	},
	"960289": {
		"nik": "960289",
		"nama": "HUMAM BASKARA",
		"band": "VI",
		"posisi": "OFF 3 SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666662,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9024064171123,
			"nilai_komparatif_unit": 0.9053406544173603,
			"summary_team": 0.8181818181818182
		}]
	},
	"960292": {
		"nik": "960292",
		"nama": "IRHAM BIMANTORO",
		"band": "VI",
		"posisi": "OFF 3 PROJECT DELIVERY & SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8541666666666665,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0004434589800446,
			"nilai_komparatif_unit": 1.0036964705536289,
			"summary_team": 0.8545454545454546
		}]
	},
	"960298": {
		"nik": "960298",
		"nama": "MUCHAMMAD EKO PUJIYANTO",
		"band": "VI",
		"posisi": "OFF 3 LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.916666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9719008264462806,
			"nilai_komparatif_unit": 0.9750610296625913,
			"summary_team": 0.8909090909090909
		}]
	},
	"960302": {
		"nik": "960302",
		"nama": "MUHAMMAD RAFII' NAUFAL",
		"band": "VI",
		"posisi": "OFF 3 NE FTM ACCESS MNTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7555555555555554,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0257352941176474,
			"nilai_komparatif_unit": 1.0290705438543994,
			"summary_team": 0.7750000000000001
		}]
	},
	"960303": {
		"nik": "960303",
		"nama": "MUHAMMAD RIFKI",
		"band": "VI",
		"posisi": "SENIOR ACCESS NETWORK SUPERVISORY MELAK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7500000000000004,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.066666666666666,
			"nilai_komparatif_unit": 1.0701350076115512,
			"summary_team": 0.8
		}]
	},
	"960321": {
		"nik": "960321",
		"nama": "REYNALD OLOAN CAESAR",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9175870858113847,
			"nilai_komparatif_unit": 0.9205706841178387,
			"summary_team": 0.8181818181818182
		}]
	},
	"960326": {
		"nik": "960326",
		"nama": "ROY AJI ILHAM BURNAMA WAROI",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8592592592592595,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9698275862068962,
			"nilai_komparatif_unit": 0.9729810481381967,
			"summary_team": 0.8333333333333333
		}]
	},
	"960329": {
		"nik": "960329",
		"nama": "SYADZA HAIFA AMI DWINANDA",
		"band": "VI",
		"posisi": "OFF 3 CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0809007506255213,
			"nilai_komparatif_unit": 1.084415374685601,
			"summary_team": 0.9818181818181818
		}]
	},
	"960334": {
		"nik": "960334",
		"nama": "WINDA RIZKITA PUSPITASARI",
		"band": "VI",
		"posisi": "OFF 3 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0086165512916365,
			"summary_team": 0.8545454545454546
		}]
	},
	"965234": {
		"nik": "965234",
		"nama": "JEHAN PRATAMA HERDANING",
		"band": "VI",
		"posisi": "OFF 3 DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0195412064570941,
			"nilai_komparatif_unit": 1.0228563156864874,
			"summary_team": 0.9090909090909092
		}]
	},
	"965244": {
		"nik": "965244",
		"nama": "GAVRILA NATHANIA CALOSA BASKARA",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666664,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300702,
			"nilai_komparatif_unit": 1.0734090220579162,
			"summary_team": 0.9272727272727272
		}]
	},
	"970018": {
		"nik": "970018",
		"nama": "ANASTITI PUTRI RAMADHANI SUDARYONO",
		"band": "VI",
		"posisi": "OFF 3 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7899999999999998,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0356731875719218,
			"nilai_komparatif_unit": 1.0390407510612738,
			"summary_team": 0.8181818181818181
		}]
	},
	"970019": {
		"nik": "970019",
		"nama": "SAFIRA NUR DEUTY",
		"band": "VI",
		"posisi": "OFF 3 DEBT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.791666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0105263157894733,
			"nilai_komparatif_unit": 1.0138121124741013,
			"summary_team": 0.8
		}]
	},
	"970023": {
		"nik": "970023",
		"nama": "MEIKA ANDIANI",
		"band": "VI",
		"posisi": "OFF 3 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9501466275659821,
			"nilai_komparatif_unit": 0.9532360954897618,
			"summary_team": 0.8181818181818182
		}]
	},
	"970025": {
		"nik": "970025",
		"nama": "MUHAMMAD KEVIN ADITYO",
		"band": "VI",
		"posisi": "OFF 3 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139859,
			"nilai_komparatif_unit": 0.9892200791514126,
			"summary_team": 0.8545454545454546
		}]
	},
	"970026": {
		"nik": "970026",
		"nama": "ANTIKA ADZARY SEKAR FADLILAH",
		"band": "VI",
		"posisi": "OFF 3 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1131725417439702,
			"nilai_komparatif_unit": 1.1167920997801444,
			"summary_team": 0.9090909090909092
		}]
	},
	"970027": {
		"nik": "970027",
		"nama": "AGUSTINA WINDARYANTI",
		"band": "VI",
		"posisi": "OFF 3 COMMAND CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8431372549019593,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9488372093023273,
			"nilai_komparatif_unit": 0.9519224195614403,
			"summary_team": 0.8
		}]
	},
	"970032": {
		"nik": "970032",
		"nama": "ARNESIUS VICTOR LELEULY",
		"band": "VI",
		"posisi": "OFF 3 PLASA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200685,
			"nilai_komparatif_unit": 1.0531560216417286,
			"summary_team": 0.9272727272727272
		}]
	},
	"970033": {
		"nik": "970033",
		"nama": "ANGGA NIRWANA",
		"band": "VI",
		"posisi": "OFF 3 SALES TANJUNG REDEB",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8716049382716041,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0325779036827203,
			"nilai_komparatif_unit": 1.035935402640964,
			"summary_team": 0.8999999999999998
		}]
	},
	"970034": {
		"nik": "970034",
		"nama": "KEVIN ARI NUGRAHA",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8564102564102566,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9341317365269459,
			"nilai_komparatif_unit": 0.937169130917302,
			"summary_team": 0.8
		}]
	},
	"970038": {
		"nik": "970038",
		"nama": "AYA ANISA DWINIDASARI",
		"band": "VI",
		"posisi": "OFF 3 PAYMENT COLLECTION CONSUMER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9991503823279523,
			"nilai_komparatif_unit": 1.0023991893727577,
			"summary_team": 0.8909090909090909
		}]
	},
	"970039": {
		"nik": "970039",
		"nama": "ADRYAN RIZKY ALVIANO",
		"band": "VI",
		"posisi": "OFF 3 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8138888888888882,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9159168476574627,
			"nilai_komparatif_unit": 0.9188950150682508,
			"summary_team": 0.7454545454545455
		}]
	},
	"970043": {
		"nik": "970043",
		"nama": "SAMUELA VIZIA KERUBIM",
		"band": "VI",
		"posisi": "OFF 3 ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0666666666666667,
			"nilai_komparatif_unit": 1.070135007611552,
			"summary_team": 0.9600000000000001
		}]
	},
	"970044": {
		"nik": "970044",
		"nama": "RAYVALDO STEFAN MA'DIKA",
		"band": "VI",
		"posisi": "OFF 3 OM LOKASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0449927431059507,
			"nilai_komparatif_unit": 1.0483906097790967,
			"summary_team": 0.9230769230769231
		}]
	},
	"970047": {
		"nik": "970047",
		"nama": "SHAFIYAH SALSABILA",
		"band": "VI",
		"posisi": "OFF 3 LOGISTIK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197862,
			"nilai_komparatif_unit": 0.965696698045184,
			"summary_team": 0.8181818181818182
		}]
	},
	"970049": {
		"nik": "970049",
		"nama": "FAHDUL BAAR WIBOWO PUTRA",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9674099485420239,
			"nilai_komparatif_unit": 0.9705555493561029,
			"summary_team": 0.8545454545454545
		}]
	},
	"970051": {
		"nik": "970051",
		"nama": "MUHAMMAD YUSFIAN FEBRIANTO",
		"band": "VI",
		"posisi": "OFF 3 PROJECT DELIVERY & SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8761904761904756,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9337944664031627,
			"nilai_komparatif_unit": 0.9368307641362251,
			"summary_team": 0.8181818181818182
		}]
	},
	"970053": {
		"nik": "970053",
		"nama": "IVANA GRACE KAPITARAUW",
		"band": "VI",
		"posisi": "OFF 3 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8190476190476191,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0377906976744182,
			"nilai_komparatif_unit": 1.041165146395323,
			"summary_team": 0.8499999999999999
		}]
	},
	"970055": {
		"nik": "970055",
		"nama": "RESTU MUHAMMAD AFDHIL",
		"band": "VI",
		"posisi": "OFF 3 OM LOKASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888888,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0250000000000001,
			"nilai_komparatif_unit": 1.0283328588767258,
			"summary_team": 0.9111111111111112
		}]
	},
	"970056": {
		"nik": "970056",
		"nama": "MUHAMMAD URFAN NAFIS",
		"band": "VI",
		"posisi": "OFF 3 PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9212121212121214,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.026315789473684,
			"nilai_komparatif_unit": 1.0296529267315095,
			"summary_team": 0.9454545454545454
		}]
	},
	"970057": {
		"nik": "970057",
		"nama": "ANDIKA SETYA KUSUMAWARDANI",
		"band": "VI",
		"posisi": "OFF 3 PREMIUM CLUSTER OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8564102564102566,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9978225367446923,
			"nilai_komparatif_unit": 1.0010670262071182,
			"summary_team": 0.8545454545454546
		}]
	},
	"970059": {
		"nik": "970059",
		"nama": "FRANSISKA MARTADIS MOA",
		"band": "VI",
		"posisi": "OFF 3 PAYMENT & COLLECTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7666666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9723320158102764,
			"nilai_komparatif_unit": 0.9754936210688304,
			"summary_team": 0.7454545454545455
		}]
	},
	"970060": {
		"nik": "970060",
		"nama": "ABDI AFIFUDDIN ZUHRI",
		"band": "VI",
		"posisi": "OFF 3 OM LOKASI WAINGAPU",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9555555555555555,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9680232558139538,
			"nilai_komparatif_unit": 0.9711708508393356,
			"summary_team": 0.9250000000000002
		}]
	},
	"970061": {
		"nik": "970061",
		"nama": "MUHAMMAD FATHAN HIZBUDDIN",
		"band": "VI",
		"posisi": "OFF 3 OM TRANSPORT, RADIO & MUX",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9504950495049503,
			"nilai_komparatif_unit": 0.9535856503469273,
			"summary_team": 0.8
		}]
	},
	"970062": {
		"nik": "970062",
		"nama": "BRIAN BARELLA AUDRIE",
		"band": "VI",
		"posisi": "OFF 3 CCAN FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0032515696358302,
			"summary_team": 0.8
		}]
	},
	"970066": {
		"nik": "970066",
		"nama": "FAUZAN GHIFARI PRAKOSO",
		"band": "VI",
		"posisi": "OFF 3 VALUE ADDED RESELLER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8277777777777782,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9664429530201338,
			"nilai_komparatif_unit": 0.9695854095809359,
			"summary_team": 0.8
		}]
	},
	"970068": {
		"nik": "970068",
		"nama": "DANIEL STEVEN DOXAZO SITOMPUL",
		"band": "VI",
		"posisi": "OFF 3 DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8266666666666661,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.033724340175954,
			"nilai_komparatif_unit": 1.0370855668522885,
			"summary_team": 0.8545454545454546
		}]
	},
	"970069": {
		"nik": "970069",
		"nama": "ANANTA MUHAMMAD",
		"band": "VI",
		"posisi": "OFF 3 OM LOKASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545454,
			"nilai_komparatif_unit": 1.0287888823174691,
			"summary_team": 0.8545454545454546
		}]
	},
	"970073": {
		"nik": "970073",
		"nama": "AYASHA TAMARA",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0291595197255576,
			"nilai_komparatif_unit": 1.0325059035703226,
			"summary_team": 0.9090909090909092
		}]
	},
	"970074": {
		"nik": "970074",
		"nama": "AMMAR SHIDQI ROBBANI",
		"band": "VI",
		"posisi": "OFF 3 BGES ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.877777777777778,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9321058688147295,
			"nilai_komparatif_unit": 0.9351366759551463,
			"summary_team": 0.8181818181818182
		}]
	},
	"970076": {
		"nik": "970076",
		"nama": "FADHI MUHAMMAD",
		"band": "VI",
		"posisi": "OFF 3 WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8599999999999993,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0359408033826647,
			"nilai_komparatif_unit": 1.0393092370434611,
			"summary_team": 0.8909090909090909
		}]
	},
	"970079": {
		"nik": "970079",
		"nama": "AMELIA PUTRI OKTAVIANI",
		"band": "VI",
		"posisi": "OFF 3 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7611111111111114,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9077637690776373,
			"nilai_komparatif_unit": 0.9107154261856767,
			"summary_team": 0.6909090909090909
		}]
	},
	"970080": {
		"nik": "970080",
		"nama": "TAMARA NILASARI",
		"band": "VI",
		"posisi": "OFF 3 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9090909090909092,
			"nilai_komparatif_unit": 0.9120468814871182,
			"summary_team": 0.8181818181818182
		}]
	},
	"970082": {
		"nik": "970082",
		"nama": "NABILA ALYA ZAHRA",
		"band": "VI",
		"posisi": "OFF 3 ACCESS ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.935064935064935,
			"nilai_komparatif_unit": 0.9381053638153215,
			"summary_team": 0.8181818181818182
		}]
	},
	"970094": {
		"nik": "970094",
		"nama": "ARIF RAHMANDINOF",
		"band": "VI",
		"posisi": "OFF 3 SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8512820512820554,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9632530120481884,
			"nilai_komparatif_unit": 0.966385096293786,
			"summary_team": 0.8200000000000002
		}]
	},
	"970097": {
		"nik": "970097",
		"nama": "BONDAN AJI WINMOKO",
		"band": "VI",
		"posisi": "OFF 3 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0032515696358302,
			"summary_team": 0.8
		}]
	},
	"970105": {
		"nik": "970105",
		"nama": "DHANAWAN PRASIDYA SOEGONDO",
		"band": "VI",
		"posisi": "OFF 3 WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300698,
			"nilai_komparatif_unit": 1.0734090220579158,
			"summary_team": 0.9272727272727272
		}]
	},
	"970107": {
		"nik": "970107",
		"nama": "DRIANHAR RAFFY HARYADI",
		"band": "VI",
		"posisi": "OFF 3 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96,
			"nilai_komparatif_unit": 0.9631215068503967,
			"summary_team": 0.8
		}]
	},
	"970111": {
		"nik": "970111",
		"nama": "FAUZAN MUNGGARA",
		"band": "VI",
		"posisi": "OFF 3 ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9674099485420238,
			"nilai_komparatif_unit": 0.9705555493561028,
			"summary_team": 0.8545454545454546
		}]
	},
	"970117": {
		"nik": "970117",
		"nama": "INTAN NUR ANNISA",
		"band": "VI",
		"posisi": "OFF 3 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200685,
			"nilai_komparatif_unit": 1.0531560216417286,
			"summary_team": 0.9272727272727272
		}]
	},
	"970120": {
		"nik": "970120",
		"nama": "JESHU PUTRA",
		"band": "VI",
		"posisi": "OFF 3 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8601626016260151,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9533081285444249,
			"nilai_komparatif_unit": 0.9564078763087898,
			"summary_team": 0.8200000000000002
		}]
	},
	"970132": {
		"nik": "970132",
		"nama": "NABA AL RASYID",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8703703703703703,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0027079303675048,
			"nilai_komparatif_unit": 1.0059683050274937,
			"summary_team": 0.8727272727272727
		}]
	},
	"970147": {
		"nik": "970147",
		"nama": "RAYNANDA CHANDRA WIBISONO",
		"band": "VI",
		"posisi": "OFF 3 DESIGN & ENGINEERING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8509803921568626,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9400921658986177,
			"nilai_komparatif_unit": 0.9431489410401351,
			"summary_team": 0.8
		}]
	},
	"970157": {
		"nik": "970157",
		"nama": "SHAUMA HIBATUL WAFI",
		"band": "VI",
		"posisi": "OFF 3 SERVICE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197862,
			"nilai_komparatif_unit": 0.965696698045184,
			"summary_team": 0.8181818181818182
		}]
	},
	"970159": {
		"nik": "970159",
		"nama": "SISIS NOER ANINDITA",
		"band": "VI",
		"posisi": "OFF 3 GES OBL & BIDDING MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8294117647058812,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9864603481624773,
			"nilai_komparatif_unit": 0.9896678926775127,
			"summary_team": 0.8181818181818182
		}]
	},
	"970162": {
		"nik": "970162",
		"nama": "THAREQ AKBAR ARYA YUDHISTIRA",
		"band": "VI",
		"posisi": "OFF 3 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8361111111111103,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9785563273935376,
			"nilai_komparatif_unit": 0.9817381714346397,
			"summary_team": 0.8181818181818181
		}]
	},
	"970167": {
		"nik": "970167",
		"nama": "WILHELMI APRILLA IDRIS",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8791666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0753985351141746,
			"nilai_komparatif_unit": 1.0788952683373678,
			"summary_team": 0.9454545454545454
		}]
	},
	"970168": {
		"nik": "970168",
		"nama": "YABES HANDI NUGROHO",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346936,
			"nilai_komparatif_unit": 0.982777047806527,
			"summary_team": 0.8
		}]
	},
	"975111": {
		"nik": "975111",
		"nama": "AIDATUL FAUZIAH",
		"band": "VI",
		"posisi": "OFF 3 OM LOKASI & SITE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365353,
			"nilai_komparatif_unit": 0.9912056674275096,
			"summary_team": 0.8727272727272728
		}]
	},
	"980004": {
		"nik": "980004",
		"nama": "RIDHO MA'RUF QULUQ",
		"band": "VI",
		"posisi": "OFF 3 VALUE ADDED RESELLER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8564102564102566,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.019052803483941,
			"nilai_komparatif_unit": 1.0223663246370567,
			"summary_team": 0.8727272727272727
		}]
	},
	"980007": {
		"nik": "980007",
		"nama": "DIRA ALIFA",
		"band": "VI",
		"posisi": "OFF 3 WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8460905349794258,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0806003335186192,
			"nilai_komparatif_unit": 1.084113980751556,
			"summary_team": 0.9142857142857145
		}]
	},
	"980008": {
		"nik": "980008",
		"nama": "LAILATUL FITRI",
		"band": "VI",
		"posisi": "OFF 3 FINANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.791666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0105263157894733,
			"nilai_komparatif_unit": 1.0138121124741013,
			"summary_team": 0.8
		}]
	},
	"980010": {
		"nik": "980010",
		"nama": "MUHAMMAD FARKHAN ROKHMATULLAH",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8285714285714288,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96551724137931,
			"nilai_komparatif_unit": 0.9686566879242493,
			"summary_team": 0.8
		}]
	},
	"980013": {
		"nik": "980013",
		"nama": "RININTA NAFI SALSABIL",
		"band": "VI",
		"posisi": "OFF 3 COMMAND CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777773,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0771001150747992,
			"nilai_komparatif_unit": 1.0806023811037253,
			"summary_team": 0.9454545454545454
		}]
	},
	"980014": {
		"nik": "980014",
		"nama": "NADYA NOVITA CHANDRA",
		"band": "VI",
		"posisi": "SENIOR ACCESS NETWORK SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9494949494949493,
			"nilai_komparatif_unit": 0.952582298442101,
			"summary_team": 0.8545454545454546
		}]
	},
	"980017": {
		"nik": "980017",
		"nama": "AULYA ULFAH RAHMADHANI",
		"band": "VI",
		"posisi": "OFF 3 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559442,
			"nilai_komparatif_unit": 0.9471256076981612,
			"summary_team": 0.8181818181818182
		}]
	},
	"980024": {
		"nik": "980024",
		"nama": "KHUSNUL LAYLI PUTRI MARSAL",
		"band": "VI",
		"posisi": "OFF 3 COMMAND CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8684684684684674,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0258393059222948,
			"nilai_komparatif_unit": 1.0291748938606726,
			"summary_team": 0.8909090909090909
		}]
	},
	"980025": {
		"nik": "980025",
		"nama": "KUMARA PANJI ATMAJA",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0585058505850582,
			"nilai_komparatif_unit": 1.0619476560681689,
			"summary_team": 0.8909090909090909
		}]
	},
	"980031": {
		"nik": "980031",
		"nama": "RAIHAN ANSHARI",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9142857142857143,
			"nilai_komparatif_unit": 0.9172585779527588,
			"summary_team": 0.8
		}]
	}
};