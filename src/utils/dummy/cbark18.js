export const cbark18 = {
	"651423": {
		"nik": "651423",
		"nama": "ANDELMAN TANJUNG",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.833333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9920000000000003,
			"nilai_komparatif_unit": 0.9989518298927723,
			"summary_team": 0.8266666666666667
		}]
	},
	"660058": {
		"nik": "660058",
		"nama": "TEDI SUPRIYADI",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7966666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9357170026626093,
			"nilai_komparatif_unit": 0.9422744073302352,
			"summary_team": 0.7454545454545454
		}]
	},
	"660161": {
		"nik": "660161",
		"nama": "I MADE WARSA",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444446,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.026315789473684,
			"nilai_komparatif_unit": 1.0335081007485702,
			"summary_team": 0.8666666666666667
		}]
	},
	"660479": {
		"nik": "660479",
		"nama": "HALIM KADIR",
		"band": "IV",
		"posisi": "OFF 1 SECURITY & SAFETY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8800000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9090909090909086,
			"nilai_komparatif_unit": 0.9154617209427891,
			"summary_team": 0.7999999999999999
		}]
	},
	"660515": {
		"nik": "660515",
		"nama": "AYUN ENDIONO",
		"band": "V",
		"posisi": "OFF 2 BIDDING MANAGEMENT - 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.941176470588235,
			"nilai_komparatif_unit": 0.941748226228257,
			"summary_team": 0.8
		}]
	},
	"660540": {
		"nik": "660540",
		"nama": "BINSAR EDWARD LUMBAN GAOL",
		"band": "IV",
		"posisi": "ASMAN DIGITAL SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8944444444444439,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9192546583850938,
			"nilai_komparatif_unit": 0.9256966967048835,
			"summary_team": 0.8222222222222223
		}]
	},
	"660606": {
		"nik": "660606",
		"nama": "SOLAHUDDIN DALIMUNTHE",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8141414141414145,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9826302729528535,
			"nilai_komparatif_unit": 0.9895164408006926,
			"summary_team": 0.8000000000000003
		}]
	},
	"660644": {
		"nik": "660644",
		"nama": "Soma Noviyanti",
		"band": "V",
		"posisi": "OFFICER 1 FINANCE AND SUPPORT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545454,
			"nilai_komparatif_unit": 1.0260774992132422,
			"summary_team": 0.8545454545454547
		}]
	},
	"670190": {
		"nik": "670190",
		"nama": "SUDARYONO, ST.",
		"band": "IV",
		"posisi": "OFF 1 NON-PHYSICAL SECURITY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.845454545454545,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.040860215053764,
			"nilai_komparatif_unit": 1.0481544521074009,
			"summary_team": 0.88
		}]
	},
	"670501": {
		"nik": "670501",
		"nama": "PARTONO SUWITO",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8722222222222213,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9936305732484086,
			"nilai_komparatif_unit": 1.0005938300240946,
			"summary_team": 0.8666666666666667
		}]
	},
	"670577": {
		"nik": "670577",
		"nama": "YUSUF SUPRIATNA",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8303030303030289,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9635036496350382,
			"nilai_komparatif_unit": 0.9702557801525057,
			"summary_team": 0.8
		}]
	},
	"680115": {
		"nik": "680115",
		"nama": "SUDARMINAH",
		"band": "IV",
		"posisi": "OFF 1 PKBL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9926470588235292,
			"nilai_komparatif_unit": 0.9996034232353251,
			"summary_team": 0.8823529411764706
		}]
	},
	"680191": {
		"nik": "680191",
		"nama": "SLAMET SUPRIYATNO",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER ANALYSIS & SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8722222222222225,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9981266391907077,
			"nilai_komparatif_unit": 0.9987329915095846,
			"summary_team": 0.8705882352941174
		}]
	},
	"680387": {
		"nik": "680387",
		"nama": "MANONGGOR PARULIAN SIADARI",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE BJO,CWN,MER,",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8055555555555558,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9310344827586206,
			"nilai_komparatif_unit": 0.9375590728276154,
			"summary_team": 0.7500000000000001
		}]
	},
	"680411": {
		"nik": "680411",
		"nama": "MUHAMMAD ASIP SAID",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION EBIS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8461538461538448,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.979220779220781,
			"nilai_komparatif_unit": 0.9860830537012352,
			"summary_team": 0.8285714285714287
		}]
	},
	"680452": {
		"nik": "680452",
		"nama": "SULAEMAN",
		"band": "IV",
		"posisi": "ASMAN WHOLESALE & WIFI ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555557,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0140499062750898,
			"summary_team": 0.8615384615384616
		}]
	},
	"680470": {
		"nik": "680470",
		"nama": "HENDRA GUNAWAN",
		"band": "V",
		"posisi": "OFF 2 PROMOTION & COLLATERAL MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9494949494949495,
			"nilai_komparatif_unit": 0.95007175853078,
			"summary_team": 0.8545454545454546
		}]
	},
	"680554": {
		"nik": "680554",
		"nama": "AGUSTIAR RIDWAN",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8303921568627456,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9232585596221956,
			"nilai_komparatif_unit": 0.9297286568535859,
			"summary_team": 0.7666666666666668
		}]
	},
	"690068": {
		"nik": "690068",
		"nama": "LEO HENDRA ADITRIONO",
		"band": "IV",
		"posisi": "ASMAN FINANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8593939393939388,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9308885754583929,
			"nilai_komparatif_unit": 0.9374121430246344,
			"summary_team": 0.8000000000000002
		}]
	},
	"690108": {
		"nik": "690108",
		"nama": "DADANG SUDARSONO",
		"band": "IV",
		"posisi": "MGR ACCESS OPTIMA & CONSTRUCTION SPV",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8792792792792785,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.971870342771983,
			"nilai_komparatif_unit": 0.9786811061800282,
			"summary_team": 0.8545454545454546
		}]
	},
	"690143": {
		"nik": "690143",
		"nama": "SUWARDI",
		"band": "IV",
		"posisi": "ASMAN SALES SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.890666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9730538922155696,
			"nilai_komparatif_unit": 0.9798729498115195,
			"summary_team": 0.8666666666666667
		}]
	},
	"690155": {
		"nik": "690155",
		"nama": "ASNAL HARDIANTO",
		"band": "IV",
		"posisi": "ASMAN DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8326436781609244,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.934106386894896,
			"nilai_komparatif_unit": 0.940652504539498,
			"summary_team": 0.7777777777777778
		}]
	},
	"690164": {
		"nik": "690164",
		"nama": "HARIS HERMAWAN",
		"band": "V",
		"posisi": "OFF 2 BUSINESS MODEL ENGINEERING & CUST",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8722222222222227,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.011614837017609,
			"nilai_komparatif_unit": 1.012229383286741,
			"summary_team": 0.8823529411764706
		}]
	},
	"690170": {
		"nik": "690170",
		"nama": "MUHAMMAD TAKDIR",
		"band": "V",
		"posisi": "MGR TSEL REGIONAL JAKARTA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8592592592592608,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0205570291777168,
			"nilai_komparatif_unit": 1.0211770077424507,
			"summary_team": 0.8769230769230769
		}]
	},
	"690299": {
		"nik": "690299",
		"nama": "SUHADI",
		"band": "IV",
		"posisi": "OFF 1 SECURITY & SAFETY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8438596491228086,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1257796257796235,
			"nilai_komparatif_unit": 1.1336689689803983,
			"summary_team": 0.95
		}]
	},
	"690345": {
		"nik": "690345",
		"nama": "HAMDANI",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE CLS, GPI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8095238095238096,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0588235294117647,
			"nilai_komparatif_unit": 1.0662436514510139,
			"summary_team": 0.8571428571428573
		}]
	},
	"690346": {
		"nik": "690346",
		"nama": "IMAN GUSNANTO",
		"band": "V",
		"posisi": "OFF 2 SALES PERFORMANCE & OFFERING MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8894308943089421,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9198936347016797,
			"nilai_komparatif_unit": 0.9204524612239072,
			"summary_team": 0.8181818181818182
		}]
	},
	"690491": {
		"nik": "690491",
		"nama": "AMIRUDIN",
		"band": "V",
		"posisi": "PROJECT SUPERVISOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9222222222222223,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9950389794472005,
			"nilai_komparatif_unit": 0.9956434560425251,
			"summary_team": 0.9176470588235295
		}]
	},
	"690552": {
		"nik": "690552",
		"nama": "SUNARSO",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7966666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9813617344998098,
			"nilai_komparatif_unit": 0.9882390125658564,
			"summary_team": 0.7818181818181817
		}]
	},
	"690568": {
		"nik": "690568",
		"nama": "SULISTYO",
		"band": "IV",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8422222222222211,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0686015831134577,
			"nilai_komparatif_unit": 1.0760902287071588,
			"summary_team": 0.8999999999999998
		}]
	},
	"700112": {
		"nik": "700112",
		"nama": "SUKIRMAN",
		"band": "V",
		"posisi": "OFF 2 BUSINESS MODEL & CUST ENGAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8363636363636368,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0127877237851657,
			"nilai_komparatif_unit": 1.0134029825717112,
			"summary_team": 0.8470588235294116
		}]
	},
	"700141": {
		"nik": "700141",
		"nama": "PRIYANA DWIPA",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE BOO",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.877777777777778,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0090415913200723,
			"nilai_komparatif_unit": 1.0161128468619969,
			"summary_team": 0.8857142857142859
		}]
	},
	"700170": {
		"nik": "700170",
		"nama": "WIDODO WIDYANTORO",
		"band": "IV",
		"posisi": "ASMAN WIFI SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8920634920634916,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0213523131672604,
			"nilai_komparatif_unit": 1.028509840931099,
			"summary_team": 0.9111111111111111
		}]
	},
	"700253": {
		"nik": "700253",
		"nama": "SUKARNA",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.833333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0080000000000005,
			"nilai_komparatif_unit": 1.0150639561813655,
			"summary_team": 0.8400000000000001
		}]
	},
	"700309": {
		"nik": "700309",
		"nama": "SUGENG SANTOSO",
		"band": "V",
		"posisi": "OFF 2 ENTERPRISE SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333338,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999995,
			"nilai_komparatif_unit": 0.9605831907528221,
			"summary_team": 0.8
		}]
	},
	"700329": {
		"nik": "700329",
		"nama": "KUSUMA WAHYU MARGONO",
		"band": "IV",
		"posisi": "ASMAN BGES ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8619047619047658,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0055248618784485,
			"nilai_komparatif_unit": 1.0125714725566057,
			"summary_team": 0.8666666666666667
		}]
	},
	"700417": {
		"nik": "700417",
		"nama": "NYOMAN BAGIASIH",
		"band": "IV",
		"posisi": "ASMAN LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8974358974358975,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9657142857142857,
			"nilai_komparatif_unit": 0.9724819081329404,
			"summary_team": 0.8666666666666667
		}]
	},
	"700505": {
		"nik": "700505",
		"nama": "MASRUR",
		"band": "V",
		"posisi": "OFF 2 OFFERING 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666664,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9725274725274726,
			"nilai_komparatif_unit": 0.9731182735991851,
			"summary_team": 0.8428571428571426
		}]
	},
	"700528": {
		"nik": "700528",
		"nama": "UNTUNG YULIANTO",
		"band": "V",
		"posisi": "OFF 2 SEGMENT PERFORMANCE SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365351,
			"nilai_komparatif_unit": 0.9885933352516183,
			"summary_team": 0.8727272727272727
		}]
	},
	"700550": {
		"nik": "700550",
		"nama": "FERY SUJANA",
		"band": "V",
		"posisi": "OFF 2 OFFERING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139859,
			"nilai_komparatif_unit": 0.9866129800127329,
			"summary_team": 0.8545454545454546
		}]
	},
	"700599": {
		"nik": "700599",
		"nama": "SOLEH",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.794202898550724,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9480463718334057,
			"nilai_komparatif_unit": 0.9486223008722601,
			"summary_team": 0.7529411764705882
		}]
	},
	"710016": {
		"nik": "710016",
		"nama": "ARIESCA FRANCESCA",
		"band": "IV",
		"posisi": "ASMAN DIGITAL SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8944444444444439,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.993788819875777,
			"nilai_komparatif_unit": 1.000753185626901,
			"summary_team": 0.888888888888889
		}]
	},
	"710050": {
		"nik": "710050",
		"nama": "SUGIRI TRIADI",
		"band": "IV",
		"posisi": "ASMAN WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8460905349794258,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0637159533073908,
			"nilai_komparatif_unit": 1.0711703609299923,
			"summary_team": 0.9000000000000001
		}]
	},
	"710057": {
		"nik": "710057",
		"nama": "ACHMAD RIFAI",
		"band": "IV",
		"posisi": "OFF 1 PROCUREMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.940740740740741,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0629921259842516,
			"nilai_komparatif_unit": 1.0704414611023954,
			"summary_team": 1
		}]
	},
	"710066": {
		"nik": "710066",
		"nama": "SAPRUDIN",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8383966244725785,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.845770233792373,
			"nilai_komparatif_unit": 0.8516973011247264,
			"summary_team": 0.7090909090909091
		}]
	},
	"710069": {
		"nik": "710069",
		"nama": "SUGENG WIDIATNO",
		"band": "V",
		"posisi": "OFF 2 BIDDING MANAGEMENT - 3",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828875,
			"nilai_komparatif_unit": 1.005958332562002,
			"summary_team": 0.8545454545454546
		}]
	},
	"710101": {
		"nik": "710101",
		"nama": "RUDI WIJAYA",
		"band": "IV",
		"posisi": "ASMAN PROJECT DELIVERY & SUPERVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8549019607843145,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0360419397116638,
			"nilai_komparatif_unit": 1.04330241080708,
			"summary_team": 0.8857142857142859
		}]
	},
	"710143": {
		"nik": "710143",
		"nama": "HERLINA",
		"band": "IV",
		"posisi": "OFF 1 VERIFICATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8421052631578945,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1083333333333336,
			"nilai_komparatif_unit": 1.1161004147827511,
			"summary_team": 0.9333333333333333
		}]
	},
	"710153": {
		"nik": "710153",
		"nama": "YETTY FUDJAYA",
		"band": "IV",
		"posisi": "OFF 1 ASSET ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1,
			"nilai_komparatif_unit": 1.0070078930370685,
			"summary_team": 0.8666666666666667
		}]
	},
	"710155": {
		"nik": "710155",
		"nama": "SUWOTO",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CIBINONG",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999997,
			"nilai_komparatif_unit": 1.0070078930370683,
			"summary_team": 0.8666666666666667
		}]
	},
	"710235": {
		"nik": "710235",
		"nama": "YUPI SOPIATIN",
		"band": "IV",
		"posisi": "ASMAN COMMUNICATION & SECRETARIATE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8543209876543202,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9364161849710992,
			"nilai_komparatif_unit": 0.9429784894335564,
			"summary_team": 0.8
		}]
	},
	"710271": {
		"nik": "710271",
		"nama": "ANWARDIN",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8383966244725785,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0192615638010651,
			"nilai_komparatif_unit": 1.0264044398169783,
			"summary_team": 0.8545454545454546
		}]
	},
	"710281": {
		"nik": "710281",
		"nama": "BAMBANG IRWANTO",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8055555555555558,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9931034482758618,
			"nilai_komparatif_unit": 1.000063011016123,
			"summary_team": 0.8
		}]
	},
	"710298": {
		"nik": "710298",
		"nama": "TAUFIK HIDAYAT",
		"band": "IV",
		"posisi": "ASMAN WHOLESALE & WIFI ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8473118279569898,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9778825235678024,
			"nilai_komparatif_unit": 0.9847354196957842,
			"summary_team": 0.8285714285714287
		}]
	},
	"710328": {
		"nik": "710328",
		"nama": "HEKSA PRASONGKO",
		"band": "V",
		"posisi": "OFF 2 COLLECTION & DEBT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0085763293310461,
			"nilai_komparatif_unit": 1.009189029736027,
			"summary_team": 0.8909090909090909
		}]
	},
	"710343": {
		"nik": "710343",
		"nama": "SRI YUNIARTI",
		"band": "IV",
		"posisi": "OFF 1 LOGISTIC PLANNING & EVALUATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.940740740740741,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9415073115860517,
			"nilai_komparatif_unit": 0.9481052941192647,
			"summary_team": 0.8857142857142859
		}]
	},
	"710350": {
		"nik": "710350",
		"nama": "ROFIUDIN",
		"band": "V",
		"posisi": "OFF 2 PROCUREMENT & ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8345679012345671,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8932221624529325,
			"nilai_komparatif_unit": 0.8937647863126812,
			"summary_team": 0.7454545454545455
		}]
	},
	"710494": {
		"nik": "710494",
		"nama": "NOVIYANTI HAFIZAH",
		"band": "IV",
		"posisi": "MGR PARTNERSHIP MARKETING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8952380952380954,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0276595744680852,
			"nilai_komparatif_unit": 1.0348613028444769,
			"summary_team": 0.9200000000000002
		}]
	},
	"720020": {
		"nik": "720020",
		"nama": "TATI NURHAYATI, SE",
		"band": "IV",
		"posisi": "OFF 1 CASH BANK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8567901234567892,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.011527377521615,
			"nilai_komparatif_unit": 1.018616053187353,
			"summary_team": 0.8666666666666667
		}]
	},
	"785846": {
		"nik": "785846",
		"nama": "AGUSTINUS INDARTO",
		"band": "V",
		"posisi": "OFF 2 OFFICE SUPPORT & SECRETARY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7066666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9811320754716981,
			"nilai_komparatif_unit": 0.9817281037568155,
			"summary_team": 0.6933333333333334
		}]
	},
	"835861": {
		"nik": "835861",
		"nama": "M. RIOFALDI RUM",
		"band": "IV",
		"posisi": "AUDITOR PRATAMA I",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8682242990654238,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9214208826695338,
			"nilai_komparatif_unit": 0.9278781016574031,
			"summary_team": 0.8
		}]
	},
	"835948": {
		"nik": "835948",
		"nama": "ANAK AGUNG ARDHA CHANDRA",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333321,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272743,
			"nilai_komparatif_unit": 1.0233485696940596,
			"summary_team": 0.8727272727272727
		}]
	},
	"855920": {
		"nik": "855920",
		"nama": "INDRA PERMANA HUTOMO",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8949771689497721,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1173469387755097,
			"nilai_komparatif_unit": 1.1180257162779978,
			"summary_team": 1
		}]
	},
	"855981": {
		"nik": "855981",
		"nama": "YULI PURNAMASARI, SE",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9760765550239233,
			"nilai_komparatif_unit": 0.9766695121290659,
			"summary_team": 0.9272727272727272
		}]
	},
	"865924": {
		"nik": "865924",
		"nama": "ANIK FARIDA",
		"band": "IV",
		"posisi": "AUDITOR PRATAMA I",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8682242990654238,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7568814393356885,
			"nilai_komparatif_unit": 0.7621855835042955,
			"summary_team": 0.6571428571428573
		}]
	},
	"866055": {
		"nik": "866055",
		"nama": "NAQSYABANDI",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333338,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9320388349514557,
			"nilai_komparatif_unit": 0.9326050395658465,
			"summary_team": 0.7999999999999999
		}]
	},
	"877135": {
		"nik": "877135",
		"nama": "IKHWAN INDRA PUTRA",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8904761904761901,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1229946524064176,
			"nilai_komparatif_unit": 1.123676860840535,
			"summary_team": 1
		}]
	},
	"886056": {
		"nik": "886056",
		"nama": "DIAN KHAERUNNISA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8293040293040309,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9208159331834226,
			"nilai_komparatif_unit": 0.9213753199930937,
			"summary_team": 0.7636363636363637
		}]
	},
	"886060": {
		"nik": "886060",
		"nama": "ADITYA WISNU PRASETYA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8539007092198568,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0220477197221398,
			"nilai_komparatif_unit": 1.0226686038670203,
			"summary_team": 0.8727272727272727
		}]
	},
	"890013": {
		"nik": "890013",
		"nama": "A.A. BAGUS PRAMANANUGRAHA HARI PUTRA",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666659,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9677419354838719,
			"nilai_komparatif_unit": 0.9745237674552285,
			"summary_team": 0.8
		}]
	},
	"890069": {
		"nik": "890069",
		"nama": "ANITA SIAGIAN",
		"band": "V",
		"posisi": "OFF 2 ORDER MANAGEMENT 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8857142857142856,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0058651026392964,
			"nilai_komparatif_unit": 1.0064761560001778,
			"summary_team": 0.8909090909090909
		}]
	},
	"890101": {
		"nik": "890101",
		"nama": "BRIANTONO MUHAMMAD R",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER EXPERIENCE ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8250000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0358126721763083,
			"nilai_komparatif_unit": 1.0364419183972142,
			"summary_team": 0.8545454545454545
		}]
	},
	"896309": {
		"nik": "896309",
		"nama": "NIENDY YUSFITASARI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8878048780487822,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0649350649350628,
			"nilai_komparatif_unit": 1.065582002729049,
			"summary_team": 0.9454545454545454
		}]
	},
	"896565": {
		"nik": "896565",
		"nama": "ENGGAL PUTRANTO WIBOWO",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8156249999999994,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9585510275165454,
			"nilai_komparatif_unit": 0.9591333380325415,
			"summary_team": 0.7818181818181817
		}]
	},
	"896578": {
		"nik": "896578",
		"nama": "LUTHFI ZULFIKRI",
		"band": "V",
		"posisi": "OFF 2 SUPPORT READINESS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7515151515151519,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9935483870967737,
			"nilai_komparatif_unit": 0.9941519581716036,
			"summary_team": 0.7466666666666667
		}]
	},
	"900044": {
		"nik": "900044",
		"nama": "RIO ZULKARNAEN",
		"band": "V",
		"posisi": "OFF 2 OFFERING 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666664,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9395604395604398,
			"nilai_komparatif_unit": 0.9401312134771789,
			"summary_team": 0.8142857142857142
		}]
	},
	"900070": {
		"nik": "900070",
		"nama": "Aldiano Febrian Dwi Eska Mutasianto",
		"band": "V",
		"posisi": "ENGINEER SATELLITE PLANNING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8740740740740733,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0608628659476127,
			"nilai_komparatif_unit": 1.0615073299199393,
			"summary_team": 0.9272727272727274
		}]
	},
	"900099": {
		"nik": "900099",
		"nama": "WIDI ANDIKA HIDAYAT NUGRAHADI",
		"band": "V",
		"posisi": "OFF 2 GTM & DIGITAL CHANNEL DATA SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0370370370370368,
			"nilai_komparatif_unit": 1.037667027047802,
			"summary_team": 0.9333333333333332
		}]
	},
	"900100": {
		"nik": "900100",
		"nama": "GOGATAMA DESENWA PRAYOGA",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8293040293040309,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.05236106649534,
			"nilai_komparatif_unit": 1.0530003657063927,
			"summary_team": 0.8727272727272727
		}]
	},
	"900101": {
		"nik": "900101",
		"nama": "Taufik Hidayat Prihandono",
		"band": "V",
		"posisi": "ENGINEER SATELLITE EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8740740740740733,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0400616332819732,
			"nilai_komparatif_unit": 1.0406934607058227,
			"summary_team": 0.9090909090909092
		}]
	},
	"900108": {
		"nik": "900108",
		"nama": "CHRISTIANINGTYAS ARI PRAMONO PUTRI",
		"band": "V",
		"posisi": "OFF 2 ENTERPRISE MARKETING&COMMUNICATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8350877192982454,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0450725744843394,
			"nilai_komparatif_unit": 1.0457074460067017,
			"summary_team": 0.8727272727272727
		}]
	},
	"900114": {
		"nik": "900114",
		"nama": "BHANU PRABOWO",
		"band": "V",
		"posisi": "OFF 2 SECRETARY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.031404958677686,
			"nilai_komparatif_unit": 1.0320315272550986,
			"summary_team": 0.9454545454545454
		}]
	},
	"900115": {
		"nik": "900115",
		"nama": "MUHAMMAD RIZAL HABIBI",
		"band": "V",
		"posisi": "OFF 2 PRODUCT READINESS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8644444444444435,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.020716769998489,
			"nilai_komparatif_unit": 1.0213368456042327,
			"summary_team": 0.8823529411764706
		}]
	},
	"900125": {
		"nik": "900125",
		"nama": "AQUILA YOMA ERADIPA",
		"band": "V",
		"posisi": "OFF 2 DATACOM CONNECTIVITY PROD&SERV STR",
		"category": [{
			"code": "SO-1-01",
			"kriteria": 0.8555555555555547,
			"kriteria_bp": 0.9982806108437089,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1550802139037444,
			"nilai_komparatif_unit": 1.157069666942158,
			"summary_team": 0.9882352941176471
		}]
	},
	"900133": {
		"nik": "900133",
		"nama": "ALVIN SUMMA YOGASWARA",
		"band": "V",
		"posisi": "OFF 2 BUSINESS LEGAL & COMPLIANCE 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139859,
			"nilai_komparatif_unit": 0.9866129800127329,
			"summary_team": 0.8545454545454546
		}]
	},
	"900134": {
		"nik": "900134",
		"nama": "SUPRADI SITEPU",
		"band": "V",
		"posisi": "OFF 2 NETWORK/IT PERFORMANCE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111105,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9700189753320689,
			"nilai_komparatif_unit": 0.9706082525158982,
			"summary_team": 0.8352941176470587
		}]
	},
	"906410": {
		"nik": "906410",
		"nama": "EVA SEVIANA SITOMPUL",
		"band": "V",
		"posisi": "OFF 2 COMMERCIAL MANAGEMENT - 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.6166666666666665,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090913,
			"nilai_komparatif_unit": 1.0915718076736625,
			"summary_team": 0.6727272727272727
		}]
	},
	"906421": {
		"nik": "906421",
		"nama": "SIGIT CANDRA KUSUMA",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8553191489361723,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8715513342378991,
			"nilai_komparatif_unit": 0.8720807932782509,
			"summary_team": 0.7454545454545454
		}]
	},
	"906530": {
		"nik": "906530",
		"nama": "ERI SATRIO HERMAWAN",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8949771689497721,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0564007421150274,
			"nilai_komparatif_unit": 1.057042495390107,
			"summary_team": 0.9454545454545454
		}]
	},
	"906685": {
		"nik": "906685",
		"nama": "EKO SETIADI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9199999999999998,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0869565217391306,
			"nilai_komparatif_unit": 1.087616837356004,
			"summary_team": 1
		}]
	},
	"910084": {
		"nik": "910084",
		"nama": "Irma Nurlita Dewi",
		"band": "V",
		"posisi": "ENGINEER TRANSPONDER ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9935064935064937,
			"nilai_komparatif_unit": 0.994110039131371,
			"summary_team": 0.9272727272727274
		}]
	},
	"910137": {
		"nik": "910137",
		"nama": "ARIFUDDIN TIRO PARAWANGSA",
		"band": "IV",
		"posisi": "ASMAN DEPLOYMENT & CAPEX MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9599999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0416666666666667,
			"nilai_komparatif_unit": 1.0489665552469465,
			"summary_team": 1
		}]
	},
	"910142": {
		"nik": "910142",
		"nama": "ARGA GEMA RIDHALLA",
		"band": "V",
		"posisi": "OFF 2 GENERAL AFFAIR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365347,
			"nilai_komparatif_unit": 0.988593335251618,
			"summary_team": 0.8727272727272727
		}]
	},
	"910144": {
		"nik": "910144",
		"nama": "TITIS WENING",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8878048780487822,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625374625374608,
			"nilai_komparatif_unit": 0.9631221947743328,
			"summary_team": 0.8545454545454546
		}]
	},
	"910147": {
		"nik": "910147",
		"nama": "Andri Pranata Kusuma",
		"band": "V",
		"posisi": "ENGINEER SPACECRAFT ANALYST",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.031404958677686,
			"nilai_komparatif_unit": 1.0320315272550986,
			"summary_team": 0.9454545454545454
		}]
	},
	"910150": {
		"nik": "910150",
		"nama": "ANGGITA LESTARI",
		"band": "V",
		"posisi": "OFF 2 DATA MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9464285714285712,
			"nilai_komparatif_unit": 0.947003517669263,
			"summary_team": 0.8833333333333333
		}]
	},
	"910157": {
		"nik": "910157",
		"nama": "SHEINI CHRISTY",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8553191489361723,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0203527815468088,
			"nilai_komparatif_unit": 1.0209726360330742,
			"summary_team": 0.8727272727272727
		}]
	},
	"910161": {
		"nik": "910161",
		"nama": "VALENTINA NAINGGOLAN",
		"band": "V",
		"posisi": "OFF 2 TRANSACTION VALIDATION 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.789473684210526,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0133333333333334,
			"nilai_komparatif_unit": 1.013948923572424,
			"summary_team": 0.7999999999999998
		}]
	},
	"910192": {
		"nik": "910192",
		"nama": "ANGGA VIESTO ALVINDO",
		"band": "V",
		"posisi": "OFF 2 SALES - 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8361111111111105,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0553058432675404,
			"nilai_komparatif_unit": 1.0559469314021168,
			"summary_team": 0.8823529411764706
		}]
	},
	"910219": {
		"nik": "910219",
		"nama": "ARIEL GORBY PUTRA",
		"band": "V",
		"posisi": "OFF 2 INTL INTERCON&MS SETT ACC RECORD",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1438658428949688,
			"nilai_komparatif_unit": 1.1445607303762668,
			"summary_team": 0.9818181818181818
		}]
	},
	"910220": {
		"nik": "910220",
		"nama": "HEFRIAN NOOR KHALIQ SATYANTONO",
		"band": "V",
		"posisi": "OFF 2 PRICING MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8644444444444435,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.007107213065176,
			"nilai_komparatif_unit": 1.0077190209961764,
			"summary_team": 0.8705882352941177
		}]
	},
	"910222": {
		"nik": "910222",
		"nama": "DWIYANUAR PUTRI",
		"band": "V",
		"posisi": "OFF 2 MS BUSINESS DEVT & MODELLING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.031404958677686,
			"nilai_komparatif_unit": 1.0320315272550986,
			"summary_team": 0.9454545454545454
		}]
	},
	"910223": {
		"nik": "910223",
		"nama": "KURNIAWAN WIDI PRAMANA",
		"band": "V",
		"posisi": "OFF 2 ACCESS NETWORK STRATEGY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8688888888888879,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0230179028133004,
			"nilai_komparatif_unit": 1.0236393763350635,
			"summary_team": 0.888888888888889
		}]
	},
	"910226": {
		"nik": "910226",
		"nama": "DARMAWAN RASYID HADI SAPUTRA",
		"band": "V",
		"posisi": "OFF 2 IT TOOLS SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.916666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.952066115702479,
			"nilai_komparatif_unit": 0.9526444866970138,
			"summary_team": 0.8727272727272727
		}]
	},
	"910230": {
		"nik": "910230",
		"nama": "WAHDAN KUKUH ISMUDIAR",
		"band": "V",
		"posisi": "OFF 2 BUSINESS TRACKING ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7849462365591385,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0423412204234137,
			"nilai_komparatif_unit": 1.0429744326744936,
			"summary_team": 0.8181818181818181
		}]
	},
	"910231": {
		"nik": "910231",
		"nama": "MARIA ANINDITA DEITAS",
		"band": "V",
		"posisi": "OFF 2 PROCUREMENT & PROCESS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8545454545454548,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9751773049645387,
			"nilai_komparatif_unit": 0.9757697157839322,
			"summary_team": 0.8333333333333334
		}]
	},
	"910235": {
		"nik": "910235",
		"nama": "HENDRY PEBRIANSYAH",
		"band": "V",
		"posisi": "OFF 2 PRODUCT & SERV QUALITY & READINESS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8545454545454548,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0791962174940894,
			"nilai_komparatif_unit": 1.079851818800885,
			"summary_team": 0.9222222222222222
		}]
	},
	"910237": {
		"nik": "910237",
		"nama": "VICKY JASMINE PRECIOUSA",
		"band": "V",
		"posisi": "OFF 2 GENERAL RESOURCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999998,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090913,
			"nilai_komparatif_unit": 1.0915718076736625,
			"summary_team": 0.9272727272727272
		}]
	},
	"910241": {
		"nik": "910241",
		"nama": "ARYA VIDYA UTAMA",
		"band": "V",
		"posisi": "OFF 2 ENTERPRISE SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969693,
			"nilai_komparatif_unit": 0.970286051265477,
			"summary_team": 0.8727272727272727
		}]
	},
	"910248": {
		"nik": "910248",
		"nama": "AHMAD SIDIK",
		"band": "V",
		"posisi": "OFF 2  NW REPORTING & EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.905555555555555,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9873691808011553,
			"nilai_komparatif_unit": 0.9879689980676817,
			"summary_team": 0.8941176470588235
		}]
	},
	"910251": {
		"nik": "910251",
		"nama": "WILY LANGIT",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333321,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0014204545454561,
			"nilai_komparatif_unit": 1.0020288078254336,
			"summary_team": 0.8545454545454546
		}]
	},
	"910267": {
		"nik": "910267",
		"nama": "TEGUH NURUL FAUZI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8904761904761901,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9596499756927569,
			"nilai_komparatif_unit": 0.9602329538091844,
			"summary_team": 0.8545454545454546
		}]
	},
	"916440": {
		"nik": "916440",
		"nama": "YANUAR WIBISONO SIDDIQ",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9760765550239233,
			"nilai_komparatif_unit": 0.9766695121290659,
			"summary_team": 0.9272727272727272
		}]
	},
	"916445": {
		"nik": "916445",
		"nama": "WAHYU ARYANDA HAFIZ",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8904761904761901,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1229946524064176,
			"nilai_komparatif_unit": 1.123676860840535,
			"summary_team": 1
		}]
	},
	"916648": {
		"nik": "916648",
		"nama": "ANINDYA AYUNINGTYAS",
		"band": "V",
		"posisi": "OFF 2 CONTENT SOURCE PROVISIONING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7777777777777779,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8285714285714284,
			"nilai_komparatif_unit": 0.8290747777330908,
			"summary_team": 0.6444444444444444
		}]
	},
	"916692": {
		"nik": "916692",
		"nama": "CLARA SILVIANA",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8617283950617275,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1604584527220643,
			"nilai_komparatif_unit": 1.1611634200540042,
			"summary_team": 1
		}]
	},
	"916693": {
		"nik": "916693",
		"nama": "TRI ADE WIGUNA",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8293040293040309,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.05236106649534,
			"nilai_komparatif_unit": 1.0530003657063927,
			"summary_team": 0.8727272727272727
		}]
	},
	"916694": {
		"nik": "916694",
		"nama": "METDA GUMELAR SEKTIYOSO",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8111111111111103,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8557614826752626,
			"nilai_komparatif_unit": 0.8562813495328855,
			"summary_team": 0.6941176470588235
		}]
	},
	"916724": {
		"nik": "916724",
		"nama": "ZULVI ROSLI PUTRA",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9060109289617485,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.040668619679476,
			"nilai_komparatif_unit": 1.0413008158417154,
			"summary_team": 0.9428571428571426
		}]
	},
	"920009": {
		"nik": "920009",
		"nama": "Dimas Budiarto Kusuma",
		"band": "V",
		"posisi": "OFFICER 1 SPECTRUM ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769228,
			"nilai_komparatif_unit": 0.9236376834161752,
			"summary_team": 0.8
		}]
	},
	"920052": {
		"nik": "920052",
		"nama": "NINDI NUANSA MAHARANI",
		"band": "V",
		"posisi": "OFF 2 SALES & PIPELINE SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8350877192982454,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1675420168067228,
			"nilai_komparatif_unit": 1.168251287335612,
			"summary_team": 0.9749999999999999
		}]
	},
	"920072": {
		"nik": "920072",
		"nama": "NABILA PUTRI KENCANA",
		"band": "V",
		"posisi": "OFF 2 PRICING ANALYSIS & REPORTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0074626865671636,
			"nilai_komparatif_unit": 1.0080747104448926,
			"summary_team": 0.8999999999999998
		}]
	},
	"920090": {
		"nik": "920090",
		"nama": "ZAHRA BABETH",
		"band": "IV",
		"posisi": "ASMAN CONSUMER ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8536585365853686,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9797402597402567,
			"nilai_komparatif_unit": 0.9866061746846262,
			"summary_team": 0.8363636363636364
		}]
	},
	"920096": {
		"nik": "920096",
		"nama": "NURUL RIZKI UTAMI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8553191489361723,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9353233830845749,
			"nilai_komparatif_unit": 0.9358915830303183,
			"summary_team": 0.8
		}]
	},
	"920102": {
		"nik": "920102",
		"nama": "FEBRINA HANIFAH",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8553191489361723,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.062867480777926,
			"nilai_komparatif_unit": 1.0635131625344525,
			"summary_team": 0.9090909090909092
		}]
	},
	"920106": {
		"nik": "920106",
		"nama": "ANNISA ARUMSIWI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333321,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9375000000000014,
			"nilai_komparatif_unit": 0.9380695222195548,
			"summary_team": 0.8
		}]
	},
	"920107": {
		"nik": "920107",
		"nama": "SUHENDI ABDULLAH",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8949771689497721,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8938775510204078,
			"nilai_komparatif_unit": 0.8944205730223982,
			"summary_team": 0.8
		}]
	},
	"920109": {
		"nik": "920109",
		"nama": "MUHAMMAD ARIFIN",
		"band": "V",
		"posisi": "OFF 2 OFFERING 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.002272727272727,
			"nilai_komparatif_unit": 1.0028815983001766,
			"summary_team": 0.8909090909090909
		}]
	},
	"920123": {
		"nik": "920123",
		"nama": "ADE MURIA MUKARROMAH",
		"band": "V",
		"posisi": "OFF 2 ACCOUNT TEAM PERFORMANCE AND SUPPO",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8266666666666684,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0887096774193523,
			"nilai_komparatif_unit": 1.0893710580614142,
			"summary_team": 0.8999999999999999
		}]
	},
	"920126": {
		"nik": "920126",
		"nama": "CYNTHIA DEWI IMELIA",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8156249999999994,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9585510275165454,
			"nilai_komparatif_unit": 0.9591333380325415,
			"summary_team": 0.7818181818181817
		}]
	},
	"920127": {
		"nik": "920127",
		"nama": "M RIZKY EKA PERMANA",
		"band": "V",
		"posisi": "OFF 2 OFFERING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0472727272727271,
			"nilai_komparatif_unit": 1.0479089353667155,
			"summary_team": 0.8727272727272727
		}]
	},
	"920135": {
		"nik": "920135",
		"nama": "UTARI RATNA DEWI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8553191489361723,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0841248303934843,
			"nilai_komparatif_unit": 1.0847834257851414,
			"summary_team": 0.9272727272727272
		}]
	},
	"920136": {
		"nik": "920136",
		"nama": "M.I. ATIKA PRIHASTUTI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8156249999999994,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8916753744339957,
			"nilai_komparatif_unit": 0.8922170586349223,
			"summary_team": 0.7272727272727272
		}]
	},
	"920141": {
		"nik": "920141",
		"nama": "ARI RAHMADIANSYAH",
		"band": "V",
		"posisi": "OFF 2 VAR & PARTNERSHIP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9890109890109885,
			"nilai_komparatif_unit": 0.9896118036601876,
			"summary_team": 0.857142857142857
		}]
	},
	"920143": {
		"nik": "920143",
		"nama": "ICA MARISA HANIFAH",
		"band": "V",
		"posisi": "OFF 2 NETWORK DELIVERY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9545454545454543,
			"nilai_komparatif_unit": 0.955125331714454,
			"summary_team": 0.8909090909090909
		}]
	},
	"920148": {
		"nik": "920148",
		"nama": "ASTARI FEBRIANI RASIDI",
		"band": "V",
		"posisi": "OFF 2 CHANNEL & MARKETING PROGRAM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9197860962566843,
			"nilai_komparatif_unit": 0.9203448574503423,
			"summary_team": 0.7818181818181817
		}]
	},
	"920152": {
		"nik": "920152",
		"nama": "MULYANI",
		"band": "V",
		"posisi": "OFF 2 SALES - 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9965397923875441,
			"nilai_komparatif_unit": 0.9971451807122734,
			"summary_team": 0.8470588235294116
		}]
	},
	"920155": {
		"nik": "920155",
		"nama": "SUCI AMALIA",
		"band": "V",
		"posisi": "OFF 2 BUSINESS EVALUATION AND REPORTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0234541577825156,
			"nilai_komparatif_unit": 1.0240758963249703,
			"summary_team": 0.9142857142857141
		}]
	},
	"920166": {
		"nik": "920166",
		"nama": "Deni Ade Munanda",
		"band": "V",
		"posisi": "ENGINEER SATELLITE EXECUTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9831932773109244,
			"nilai_komparatif_unit": 0.9837905577563048,
			"summary_team": 0.9176470588235295
		}]
	},
	"920167": {
		"nik": "920167",
		"nama": "ANINDITO YUSUF WIRAWAN",
		"band": "IV",
		"posisi": "ASMAN ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8805555555555548,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9842271293375403,
			"nilai_komparatif_unit": 0.9911244877841188,
			"summary_team": 0.8666666666666667
		}]
	},
	"920171": {
		"nik": "920171",
		"nama": "MONA VINDYTIA",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8553191489361723,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9565807327001332,
			"nilai_komparatif_unit": 0.9571618462810072,
			"summary_team": 0.8181818181818181
		}]
	},
	"920172": {
		"nik": "920172",
		"nama": "FITRI KURNIASARI",
		"band": "V",
		"posisi": "OFF 2 SEGMENT PERFORMANCE SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300698,
			"nilai_komparatif_unit": 1.0705800421414762,
			"summary_team": 0.9272727272727272
		}]
	},
	"920175": {
		"nik": "920175",
		"nama": "RHIZO RASIKH BAKRI",
		"band": "V",
		"posisi": "OFF 2 ENTERPRISE SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365347,
			"nilai_komparatif_unit": 0.988593335251618,
			"summary_team": 0.8727272727272727
		}]
	},
	"920177": {
		"nik": "920177",
		"nama": "RINO SUPRIADI PUTRA",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333321,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0014204545454561,
			"nilai_komparatif_unit": 1.0020288078254336,
			"summary_team": 0.8545454545454546
		}]
	},
	"920180": {
		"nik": "920180",
		"nama": "PRIDANA DICKY SANTOSO",
		"band": "V",
		"posisi": "OFF 2 BUSINESS LEGAL OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272725,
			"nilai_komparatif_unit": 1.0233485696940579,
			"summary_team": 0.8999999999999999
		}]
	},
	"920181": {
		"nik": "920181",
		"nama": "VINA SHABRINA",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8958333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9539112050739958,
			"nilai_komparatif_unit": 0.9544906969425511,
			"summary_team": 0.8545454545454546
		}]
	},
	"920184": {
		"nik": "920184",
		"nama": "DHEA AYU PRATIWI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85528455284553,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0629104735568595,
			"nilai_komparatif_unit": 1.0635561814310852,
			"summary_team": 0.9090909090909092
		}]
	},
	"920186": {
		"nik": "920186",
		"nama": "Halim Futu Wijaya",
		"band": "V",
		"posisi": "ENGINEER RF & BASEBAND",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7888888888888892,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9910371318822019,
			"nilai_komparatif_unit": 0.9916391773936785,
			"summary_team": 0.7818181818181817
		}]
	},
	"920187": {
		"nik": "920187",
		"nama": "ERICK RAHMAN",
		"band": "V",
		"posisi": "OFF 2 GTM & DIGITAL CHANNEL DATA SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9629629629629627,
			"nilai_komparatif_unit": 0.9635479536872447,
			"summary_team": 0.8666666666666667
		}]
	},
	"920188": {
		"nik": "920188",
		"nama": "FARIZ FADHILLAH",
		"band": "V",
		"posisi": "OFF 2 ACCOUNT TEAM PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9222222222222223,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0054764512595837,
			"nilai_komparatif_unit": 1.0060872685184956,
			"summary_team": 0.9272727272727272
		}]
	},
	"920190": {
		"nik": "920190",
		"nama": "MUHAMMAD ZAID PAMUNGKAS",
		"band": "V",
		"posisi": "OFF 2 PROJECT REPORTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8999999999999999,
			"nilai_komparatif_unit": 0.9005467413307711,
			"summary_team": 0.8
		}]
	},
	"920192": {
		"nik": "920192",
		"nama": "SITI AISYAH NURSYAMSIYAH",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8894308943089421,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0846327562103466,
			"nilai_komparatif_unit": 1.0852916601620448,
			"summary_team": 0.9647058823529413
		}]
	},
	"920198": {
		"nik": "920198",
		"nama": "FIRDA SEPTA FLORENTINA",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8324074074074084,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0702801092122547,
			"nilai_komparatif_unit": 1.0709302940691532,
			"summary_team": 0.8909090909090909
		}]
	},
	"920202": {
		"nik": "920202",
		"nama": "ANGGARI DEKA PUTRI",
		"band": "V",
		"posisi": "OFF 2 OFFERING 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8952380952380954,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0560928433268857,
			"nilai_komparatif_unit": 1.0567344095564173,
			"summary_team": 0.9454545454545454
		}]
	},
	"920205": {
		"nik": "920205",
		"nama": "DIAN SULISTYANINGSIH",
		"band": "V",
		"posisi": "OFF 2 BUSINESS MODEL ENGINEERING & CUST",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0196078431372548,
			"nilai_komparatif_unit": 1.0202272450806122,
			"summary_team": 0.9176470588235295
		}]
	},
	"920206": {
		"nik": "920206",
		"nama": "RIZQA FADILAH",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8949771689497721,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9345083487940626,
			"nilai_komparatif_unit": 0.9350760536143253,
			"summary_team": 0.8363636363636363
		}]
	},
	"920211": {
		"nik": "920211",
		"nama": "RYAN IKHSAN SETIADI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8619883040935664,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.05464413469841,
			"nilai_komparatif_unit": 1.0552848208514043,
			"summary_team": 0.9090909090909092
		}]
	},
	"920217": {
		"nik": "920217",
		"nama": "DHARAF SIYADIL ALAM",
		"band": "IV",
		"posisi": "OFF 1 LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8694444444444431,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9201277955271581,
			"nilai_komparatif_unit": 0.926575952698646,
			"summary_team": 0.8000000000000002
		}]
	},
	"920218": {
		"nik": "920218",
		"nama": "NEVANDRA NATALIA SARASWATI",
		"band": "V",
		"posisi": "OFF 2 CONTRACT DRAFTING & REVIEW",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649346,
			"nilai_komparatif_unit": 0.9356329780059955,
			"summary_team": 0.8181818181818181
		}]
	},
	"920219": {
		"nik": "920219",
		"nama": "FERRY ARIESKA",
		"band": "V",
		"posisi": "OFF 2 PERFORMANCE & ORDER ALIGNMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0085763293310461,
			"nilai_komparatif_unit": 1.009189029736027,
			"summary_team": 0.8909090909090909
		}]
	},
	"920222": {
		"nik": "920222",
		"nama": "NADIA MAHARANI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8904761904761901,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9188138065143416,
			"nilai_komparatif_unit": 0.9193719770513468,
			"summary_team": 0.8181818181818181
		}]
	},
	"920241": {
		"nik": "920241",
		"nama": "WIDI PRAHATIKNO",
		"band": "V",
		"posisi": "OFF 2 CUSTOMIZE & ECOSYSTEM SOLUTION MKT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0074626865671636,
			"nilai_komparatif_unit": 1.0080747104448926,
			"summary_team": 0.8999999999999999
		}]
	},
	"920246": {
		"nik": "920246",
		"nama": "MONICA DWINITA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.845714285714285,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.031941031941033,
			"nilai_komparatif_unit": 1.0325679261777894,
			"summary_team": 0.8727272727272728
		}]
	},
	"920252": {
		"nik": "920252",
		"nama": "GALIH ABDUL RAUF",
		"band": "V",
		"posisi": "OFF 2 FRAUD & REVENUE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346936,
			"nilai_komparatif_unit": 0.9801869293396146,
			"summary_team": 0.8
		}]
	},
	"920258": {
		"nik": "920258",
		"nama": "GANJAR ADI SAPUTRA",
		"band": "V",
		"posisi": "OFF 2 BUSINESS LEGAL & COMPLIANCE 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0303030303030298,
			"nilai_komparatif_unit": 1.0309289294695692,
			"summary_team": 0.9272727272727272
		}]
	},
	"920270": {
		"nik": "920270",
		"nama": "ANNA SORAYA PUTRI",
		"band": "V",
		"posisi": "OFF 2 FAB TOOLS DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9111111111111112,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9756097560975608,
			"nilai_komparatif_unit": 0.9762024296268521,
			"summary_team": 0.8888888888888888
		}]
	},
	"920278": {
		"nik": "920278",
		"nama": "ADITYA DARDYA PURWANA",
		"band": "V",
		"posisi": "OFF 2 COLLECTION MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.806666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0625737898465166,
			"nilai_komparatif_unit": 1.0632192931886313,
			"summary_team": 0.857142857142857
		}]
	},
	"920291": {
		"nik": "920291",
		"nama": "MUHAMMAD NAUFAL ASHSHIDDIQ WANGSAATMADJA",
		"band": "V",
		"posisi": "OFF 2 CYBER SECURITY PROGRAM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8611111111111105,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9836812144212532,
			"nilai_komparatif_unit": 0.984278791283728,
			"summary_team": 0.8470588235294119
		}]
	},
	"920316": {
		"nik": "920316",
		"nama": "YUDHA RYANDIEKA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7716049382716039,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1577600000000017,
			"nilai_komparatif_unit": 1.1584633280479057,
			"summary_team": 0.8933333333333334
		}]
	},
	"920323": {
		"nik": "920323",
		"nama": "YANDA MUSTIKA RAMADHITA",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8949771689497721,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9954545454545449,
			"nilai_komparatif_unit": 0.9960592745022161,
			"summary_team": 0.8909090909090909
		}]
	},
	"920327": {
		"nik": "920327",
		"nama": "AHMAD SAHL",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8156249999999994,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9139672587948456,
			"nilai_komparatif_unit": 0.9145224851007954,
			"summary_team": 0.7454545454545454
		}]
	},
	"920332": {
		"nik": "920332",
		"nama": "I GUSTI BAGUS RAKA DHARMA YUDHA",
		"band": "V",
		"posisi": "OFF 2 LEGAL & LITIGATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1237677984665893,
			"nilai_komparatif_unit": 1.124450476579491,
			"summary_team": 0.9272727272727272
		}]
	},
	"920342": {
		"nik": "920342",
		"nama": "MICHAEL NORMAN HAPSORO",
		"band": "V",
		"posisi": "OFF 2 INVOICE HANDLING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9674099485420238,
			"nilai_komparatif_unit": 0.9679976407672095,
			"summary_team": 0.8545454545454546
		}]
	},
	"925880": {
		"nik": "925880",
		"nama": "IRSALINA RIZKA NURFADHILAH",
		"band": "V",
		"posisi": "OFF 2 CFU & CRO DATA ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8565656565656555,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9962264150943407,
			"nilai_komparatif_unit": 0.996831613045383,
			"summary_team": 0.8533333333333333
		}]
	},
	"925983": {
		"nik": "925983",
		"nama": "YUSNANI LUBIS",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8949771689497721,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1173469387755097,
			"nilai_komparatif_unit": 1.1180257162779978,
			"summary_team": 1
		}]
	},
	"925984": {
		"nik": "925984",
		"nama": "YUSFIE ARLANTO",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8553191489361723,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0203527815468088,
			"nilai_komparatif_unit": 1.0209726360330742,
			"summary_team": 0.8727272727272727
		}]
	},
	"926120": {
		"nik": "926120",
		"nama": "BERLIAN SIREGAR",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8878048780487822,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0444555444555426,
			"nilai_komparatif_unit": 1.0450900411381059,
			"summary_team": 0.9272727272727272
		}]
	},
	"926126": {
		"nik": "926126",
		"nama": "REVINA FEBRIANTY PUTRITAMA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9444444444444442,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.981818181818182,
			"nilai_komparatif_unit": 0.982414626906296,
			"summary_team": 0.9272727272727272
		}]
	},
	"926130": {
		"nik": "926130",
		"nama": "RIVA ADITYA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8617283950617275,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.012763740557438,
			"nilai_komparatif_unit": 1.0133789847744037,
			"summary_team": 0.8727272727272727
		}]
	},
	"928691": {
		"nik": "928691",
		"nama": "OKITA AMANDA OKTORA",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9444444444444442,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.981818181818182,
			"nilai_komparatif_unit": 0.982414626906296,
			"summary_team": 0.9272727272727272
		}]
	},
	"928693": {
		"nik": "928693",
		"nama": "ULFATUL MASRUROH",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8771929824561402,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0363636363636366,
			"nilai_komparatif_unit": 1.0369932172899792,
			"summary_team": 0.9090909090909092
		}]
	},
	"930006": {
		"nik": "930006",
		"nama": "ANGGI GHONI SATUN FITRI",
		"band": "V",
		"posisi": "OFF 2 SALES - 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8111111111111103,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.029814665592265,
			"nilai_komparatif_unit": 1.0304402680819469,
			"summary_team": 0.8352941176470587
		}]
	},
	"930008": {
		"nik": "930008",
		"nama": "AYU SHABRINA PUTERI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8293040293040309,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9646643109540618,
			"nilai_komparatif_unit": 0.9652503352308601,
			"summary_team": 0.8
		}]
	},
	"930009": {
		"nik": "930009",
		"nama": "ZAFARA ZANNAH",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8617283950617275,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.949466006772598,
			"nilai_komparatif_unit": 0.9500427982260033,
			"summary_team": 0.8181818181818181
		}]
	},
	"930013": {
		"nik": "930013",
		"nama": "SAPHIRA DIELLA VARIANTY",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333338,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0591350397175634,
			"nilai_komparatif_unit": 1.0597784540520985,
			"summary_team": 0.9090909090909092
		}]
	},
	"930016": {
		"nik": "930016",
		"nama": "NYIMAS TITIEK INDRIANI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8324074074074084,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8736980483365344,
			"nilai_komparatif_unit": 0.874228811485023,
			"summary_team": 0.7272727272727272
		}]
	},
	"930023": {
		"nik": "930023",
		"nama": "WIDHANI PUTRI",
		"band": "V",
		"posisi": "OFF 2 ENTERPRISE PLANNING & PROGRAM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9545454545454544,
			"nilai_komparatif_unit": 0.9551253317144541,
			"summary_team": 0.8909090909090909
		}]
	},
	"930028": {
		"nik": "930028",
		"nama": "CINDY ARINI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8156249999999994,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9585510275165454,
			"nilai_komparatif_unit": 0.9591333380325415,
			"summary_team": 0.7818181818181817
		}]
	},
	"930029": {
		"nik": "930029",
		"nama": "BERTHA DESVIANI PURBA",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85528455284553,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9353612167300367,
			"nilai_komparatif_unit": 0.9359294396593553,
			"summary_team": 0.8000000000000003
		}]
	},
	"930033": {
		"nik": "930033",
		"nama": "USWATUN HASANAH",
		"band": "V",
		"posisi": "OFF 2 WIFI SERVICE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9850746268656716,
			"nilai_komparatif_unit": 0.9856730502127843,
			"summary_team": 0.88
		}]
	},
	"930036": {
		"nik": "930036",
		"nama": "ERLYN DYAH FITRIYANTI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8394557823129278,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9746574333284188,
			"nilai_komparatif_unit": 0.975249528330801,
			"summary_team": 0.8181818181818181
		}]
	},
	"930041": {
		"nik": "930041",
		"nama": "AHMAD KHOIRUDIN ANWAR",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8433333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0132950053898673,
			"nilai_komparatif_unit": 1.0139105723451014,
			"summary_team": 0.8545454545454546
		}]
	},
	"930042": {
		"nik": "930042",
		"nama": "MUHAMAD FAHRUROZI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8878048780487822,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9830169830169811,
			"nilai_komparatif_unit": 0.983614156365276,
			"summary_team": 0.8727272727272727
		}]
	},
	"930046": {
		"nik": "930046",
		"nama": "MARIA YOSEFA FEBRIYANTI",
		"band": "V",
		"posisi": "OFF 2 PERFORMANCE & RISK MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8370370370370374,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0353982300884952,
			"nilai_komparatif_unit": 1.0360272245398248,
			"summary_team": 0.8666666666666666
		}]
	},
	"930052": {
		"nik": "930052",
		"nama": "INDRI AYU WULANSARI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8771929824561402,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9120000000000003,
			"nilai_komparatif_unit": 0.9125540312151817,
			"summary_team": 0.8
		}]
	},
	"930067": {
		"nik": "930067",
		"nama": "SUNARTO AMSALOM SAIRO",
		"band": "V",
		"posisi": "OFF 2 ENTERPRISE SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365347,
			"nilai_komparatif_unit": 0.988593335251618,
			"summary_team": 0.8727272727272727
		}]
	},
	"930072": {
		"nik": "930072",
		"nama": "REGITA PUTRI HADIJAH SOMANTRI",
		"band": "V",
		"posisi": "OFF 2 ACCOUNT TEAM DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9222222222222223,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9660460021905805,
			"nilai_komparatif_unit": 0.9666328658314959,
			"summary_team": 0.8909090909090909
		}]
	},
	"930074": {
		"nik": "930074",
		"nama": "SITI FADILAH RISTEKAWATI",
		"band": "V",
		"posisi": "OFF 2 DIGITAL CHANNEL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0703259005145795,
			"nilai_komparatif_unit": 1.070976113189253,
			"summary_team": 0.9454545454545454
		}]
	},
	"930076": {
		"nik": "930076",
		"nama": "IMELIA RIZKI LESTARI",
		"band": "V",
		"posisi": "OFF 2 SEGMENT PERFORMANCE SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0129870129870129,
			"nilai_komparatif_unit": 1.013602392839829,
			"summary_team": 0.9454545454545454
		}]
	},
	"930085": {
		"nik": "930085",
		"nama": "RIZAL EKO CAHYONO",
		"band": "V",
		"posisi": "OFF 2 PROGRAM MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0399320305862358,
			"nilai_komparatif_unit": 1.0405637792776963,
			"summary_team": 0.9272727272727272
		}]
	},
	"930090": {
		"nik": "930090",
		"nama": "ERSYHAD PRABAWA ADIGUNA",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8619883040935664,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0124583693104734,
			"nilai_komparatif_unit": 1.0130734280173481,
			"summary_team": 0.8727272727272727
		}]
	},
	"930092": {
		"nik": "930092",
		"nama": "REIKA ERMANSA",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8878048780487822,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9215784215784197,
			"nilai_komparatif_unit": 0.9221382715924462,
			"summary_team": 0.8181818181818181
		}]
	},
	"930093": {
		"nik": "930093",
		"nama": "AZARIA GEASHINTA",
		"band": "V",
		"posisi": "OFF 2 MARKETING EVALUATION AND CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197856,
			"nilai_komparatif_unit": 0.9631515950061719,
			"summary_team": 0.8470588235294116
		}]
	},
	"930103": {
		"nik": "930103",
		"nama": "DESTI VIRGANI",
		"band": "V",
		"posisi": "OFF 2 DEBT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7888888888888888,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9679897567221512,
			"nilai_komparatif_unit": 0.9685778011752213,
			"summary_team": 0.7636363636363636
		}]
	},
	"930110": {
		"nik": "930110",
		"nama": "DEVINA PUTRI AMADEA",
		"band": "V",
		"posisi": "OFF 2 COMPLIANCE & ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9787595581988102,
			"nilai_komparatif_unit": 0.9793541452025376,
			"summary_team": 0.8727272727272727
		}]
	},
	"930112": {
		"nik": "930112",
		"nama": "PUSPA HARUMI NUSANTARI",
		"band": "V",
		"posisi": "OFF 2 FRAUD MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9262435677530014,
			"nilai_komparatif_unit": 0.926806251798392,
			"summary_team": 0.8181818181818182
		}]
	},
	"930117": {
		"nik": "930117",
		"nama": "FIKRAN MUFID",
		"band": "V",
		"posisi": "OFF 2 INDUSTRY & MARKET ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8971962616822428,
			"nilai_komparatif_unit": 0.8977412997689929,
			"summary_team": 0.8
		}]
	},
	"930121": {
		"nik": "930121",
		"nama": "EMIRI ALMIRA",
		"band": "V",
		"posisi": "OFF 2 OFFERING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0076047455449186,
			"summary_team": 0.8727272727272727
		}]
	},
	"930123": {
		"nik": "930123",
		"nama": "INESIA DWI QURAINI",
		"band": "V",
		"posisi": "OFF 2 ORDER MANAGEMENT 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300698,
			"nilai_komparatif_unit": 1.0705800421414762,
			"summary_team": 0.9272727272727272
		}]
	},
	"930127": {
		"nik": "930127",
		"nama": "LINGGAR ASA BARANTI",
		"band": "V",
		"posisi": "OFF 2 SALES - 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8925925925925923,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0544300707835006,
			"nilai_komparatif_unit": 1.0550706268947287,
			"summary_team": 0.9411764705882353
		}]
	},
	"930128": {
		"nik": "930128",
		"nama": "DESKA RACHMALA",
		"band": "V",
		"posisi": "OFF 2 BUSINESS MODEL ENGINEERING & CUST",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8363636363636368,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9565217391304341,
			"nilai_komparatif_unit": 0.9571028168732827,
			"summary_team": 0.7999999999999998
		}]
	},
	"930131": {
		"nik": "930131",
		"nama": "KINNANTI ALRIAN RELLAUTRI",
		"band": "V",
		"posisi": "OFF 2 BIDDING SUPPORT & REPORTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0085763293310461,
			"nilai_komparatif_unit": 1.009189029736027,
			"summary_team": 0.8909090909090909
		}]
	},
	"930144": {
		"nik": "930144",
		"nama": "DEWI CARIESTA PUSPASARI",
		"band": "V",
		"posisi": "OFF 2 LEGAL COMPLIANCE & COUNSEL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8600000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9800664451827239,
			"nilai_komparatif_unit": 0.9806618261077055,
			"summary_team": 0.8428571428571427
		}]
	},
	"930146": {
		"nik": "930146",
		"nama": "MENTARI NUR FAJRIN",
		"band": "V",
		"posisi": "OFF 2 IPTV UI/UX DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9870129870129867,
			"nilai_komparatif_unit": 0.9876125878952177,
			"summary_team": 0.8444444444444444
		}]
	},
	"930153": {
		"nik": "930153",
		"nama": "ASPRIANTAMA YUDHA",
		"band": "V",
		"posisi": "OFF 2 CONTENT MARKETING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8083333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1216494845360823,
			"nilai_komparatif_unit": 1.1223308757936756,
			"summary_team": 0.9066666666666667
		}]
	},
	"930159": {
		"nik": "930159",
		"nama": "RATNA WULAN SARI",
		"band": "V",
		"posisi": "OFF 2 SOLUTION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9607843137254901,
			"nilai_komparatif_unit": 0.961367980941346,
			"summary_team": 0.8166666666666667
		}]
	},
	"930161": {
		"nik": "930161",
		"nama": "MUHAMMAD ROIS FADHIL",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8156249999999994,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8916753744339957,
			"nilai_komparatif_unit": 0.8922170586349223,
			"summary_team": 0.7272727272727272
		}]
	},
	"930164": {
		"nik": "930164",
		"nama": "MOH IMAM RAHMAT FAHMI",
		"band": "V",
		"posisi": "OFF 2 SOLUTION DEPLOYMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96,
			"nilai_komparatif_unit": 0.9605831907528226,
			"summary_team": 0.8
		}]
	},
	"930171": {
		"nik": "930171",
		"nama": "Bagas Akhmad Adi Nugroho",
		"band": "V",
		"posisi": "OFFICER 1 LEGAL COMPLIANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9772727272727273,
			"nilai_komparatif_unit": 0.9778664110409889,
			"summary_team": 0.7818181818181817
		}]
	},
	"930172": {
		"nik": "930172",
		"nama": "RIFAN SATRIA ADIATMA",
		"band": "V",
		"posisi": "OFF 2 ASSURANCE PERFORMANCE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272727,
			"nilai_komparatif_unit": 1.023348569694058,
			"summary_team": 0.8181818181818181
		}]
	},
	"930176": {
		"nik": "930176",
		"nama": "TSANA HASTI NABILA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.897435897435897,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9831932773109248,
			"nilai_komparatif_unit": 0.9837905577563051,
			"summary_team": 0.8823529411764706
		}]
	},
	"930177": {
		"nik": "930177",
		"nama": "ALMAS PRADANA",
		"band": "V",
		"posisi": "OFF 2 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8250000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969694,
			"nilai_komparatif_unit": 0.9702860512654771,
			"summary_team": 0.8
		}]
	},
	"930194": {
		"nik": "930194",
		"nama": "ARDIANA RACHMI NAFIDAH",
		"band": "V",
		"posisi": "OFF 2 SALES OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9151515151515153,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9366130558183535,
			"nilai_komparatif_unit": 0.9371820392278599,
			"summary_team": 0.857142857142857
		}]
	},
	"930195": {
		"nik": "930195",
		"nama": "MALINDA ROSYITA ANJANI",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER RETENTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888892,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0446428571428565,
			"nilai_komparatif_unit": 1.045277467616073,
			"summary_team": 0.9285714285714284
		}]
	},
	"930202": {
		"nik": "930202",
		"nama": "AKMAL FATAH FAINUSA",
		"band": "V",
		"posisi": "OFF 2 INBOUND LOGISTIC",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828875,
			"nilai_komparatif_unit": 1.005958332562002,
			"summary_team": 0.8545454545454546
		}]
	},
	"930204": {
		"nik": "930204",
		"nama": "RIZKI RAMADHAN",
		"band": "V",
		"posisi": "OFF 2 PACKAGE & PRICING EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666663,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9844236760124615,
			"nilai_komparatif_unit": 0.9850217039132012,
			"summary_team": 0.8777777777777778
		}]
	},
	"930205": {
		"nik": "930205",
		"nama": "PRISCILLA MARIA KRISNA VIOLETTA",
		"band": "V",
		"posisi": "OFF 2 OTT PRODUCT PLANNING & DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9714285714285715,
			"nilai_komparatif_unit": 0.9720187049284515,
			"summary_team": 0.9066666666666667
		}]
	},
	"930212": {
		"nik": "930212",
		"nama": "AHYAD ROSYADA JORDIAWAN",
		"band": "V",
		"posisi": "JUNIOR EXPERT-1 CREATIVE & PRODUCTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8296296296296292,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0982142857142863,
			"nilai_komparatif_unit": 1.0988814403143343,
			"summary_team": 0.9111111111111111
		}]
	},
	"930221": {
		"nik": "930221",
		"nama": "Rahyanditya Ilham",
		"band": "V",
		"posisi": "OFFICER 1 CAPACITY MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545454,
			"nilai_komparatif_unit": 1.0260774992132422,
			"summary_team": 0.8545454545454546
		}]
	},
	"930243": {
		"nik": "930243",
		"nama": "MUHAMAD IKBAL TAWAKAL",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8617283950617275,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0760614743422778,
			"nilai_komparatif_unit": 1.076715171322804,
			"summary_team": 0.9272727272727272
		}]
	},
	"930247": {
		"nik": "930247",
		"nama": "NURMAN WIBISANA",
		"band": "V",
		"posisi": "OFF 2 INVOICE MGT & COLLECTION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8740740740740743,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9776579352850537,
			"nilai_komparatif_unit": 0.9782518530634724,
			"summary_team": 0.8545454545454546
		}]
	},
	"930256": {
		"nik": "930256",
		"nama": "IMAN MUHAMAD RAMADHAN",
		"band": "V",
		"posisi": "OFF 2 IPTV INFRASTRUCTURE DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0769230769230769,
			"nilai_komparatif_unit": 1.0775772973188715,
			"summary_team": 0.9333333333333333
		}]
	},
	"930257": {
		"nik": "930257",
		"nama": "ADIPRAJA AL RASYID",
		"band": "V",
		"posisi": "OFF 2 OFFERING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9318181818181818,
			"nilai_komparatif_unit": 0.9323842523879196,
			"summary_team": 0.7454545454545454
		}]
	},
	"930266": {
		"nik": "930266",
		"nama": "NINDA FRISKY RAHMAWATI",
		"band": "V",
		"posisi": "OFF 2 MANAGED SERVICE QUALITY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8839506172839499,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9873031995937033,
			"nilai_komparatif_unit": 0.9879029767772816,
			"summary_team": 0.8727272727272728
		}]
	},
	"930272": {
		"nik": "930272",
		"nama": "LULU LUTHFIA",
		"band": "V",
		"posisi": "OFF 2 TECHNICAL SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333332,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9545454545454546,
			"nilai_komparatif_unit": 0.9551253317144544,
			"summary_team": 0.8909090909090909
		}]
	},
	"930274": {
		"nik": "930274",
		"nama": "TOMMY ANDRIAWAN SUNANDAR",
		"band": "V",
		"posisi": "OFF 2 BUSINESS MODEL ENGINEERING & CUST",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.963800904977375,
			"nilai_komparatif_unit": 0.9643864047433592,
			"summary_team": 0.8352941176470587
		}]
	},
	"930278": {
		"nik": "930278",
		"nama": "RADIAN MUHAMMAD",
		"band": "V",
		"posisi": "OFF 2 SALES - 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8595238095238087,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9307479224376738,
			"nilai_komparatif_unit": 0.9313133428351472,
			"summary_team": 0.7999999999999998
		}]
	},
	"930283": {
		"nik": "930283",
		"nama": "BINTANG GEMILAU FAKHRI",
		"band": "V",
		"posisi": "OFF 2 PERFORMANCE MEASUREMENT & EVAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333331,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857144,
			"nilai_komparatif_unit": 0.9648715085686835,
			"summary_team": 0.8999999999999999
		}]
	},
	"930284": {
		"nik": "930284",
		"nama": "QATHRUNNADA",
		"band": "V",
		"posisi": "OFF 2 BUDGET CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9388888888888888,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9682625067240453,
			"nilai_komparatif_unit": 0.9688507168701144,
			"summary_team": 0.9090909090909092
		}]
	},
	"930285": {
		"nik": "930285",
		"nama": "BENRAEN PIROGO",
		"band": "V",
		"posisi": "OFF 2 BIDDING MANAGEMENT - 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144382,
			"nilai_komparatif_unit": 1.0273617013399168,
			"summary_team": 0.8727272727272727
		}]
	},
	"930316": {
		"nik": "930316",
		"nama": "AMANDA JULIA",
		"band": "V",
		"posisi": "OFF 2 DIGITAL SERVICES PRODUCT READINESS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0379464285714284,
			"nilai_komparatif_unit": 1.0385769710287909,
			"summary_team": 0.8857142857142856
		}]
	},
	"930317": {
		"nik": "930317",
		"nama": "ADHELA INDAH PERMATASARI",
		"band": "V",
		"posisi": "OFF 2 IT & SERVICE REPORTING & EVAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9296296296296293,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9800796812749009,
			"nilai_komparatif_unit": 0.9806750702406809,
			"summary_team": 0.9111111111111112
		}]
	},
	"930323": {
		"nik": "930323",
		"nama": "VINCYA TUNGGADEVI",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666659,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.913217623497998,
			"nilai_komparatif_unit": 0.9137723944077257,
			"summary_team": 0.8142857142857142
		}]
	},
	"930330": {
		"nik": "930330",
		"nama": "VENTY ANDISTYA MELLAWATI",
		"band": "V",
		"posisi": "OFF 2 SALES PLAN & EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0115702479338844,
			"nilai_komparatif_unit": 1.0121847671155777,
			"summary_team": 0.9272727272727272
		}]
	},
	"930354": {
		"nik": "930354",
		"nama": "DYAH RACHMAWATI RASYIDA",
		"band": "V",
		"posisi": "OFF 2 MEDIA PLACEMENT & SPONSORSHIP EVAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8704761904761896,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0262582056892788,
			"nilai_komparatif_unit": 1.0268816476638272,
			"summary_team": 0.8933333333333333
		}]
	},
	"930356": {
		"nik": "930356",
		"nama": "RANI AULIYA SYAFRUDIN",
		"band": "V",
		"posisi": "OFF 2 PROVISIONING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7777777777777779,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1714285714285713,
			"nilai_komparatif_unit": 1.1721402030019559,
			"summary_team": 0.9111111111111111
		}]
	},
	"930358": {
		"nik": "930358",
		"nama": "AHZA RASYID RIYANTO",
		"band": "V",
		"posisi": "OFF 2 SALES SUPPORT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8844444444444439,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9843334318652089,
			"nilai_komparatif_unit": 0.9849314049434984,
			"summary_team": 0.8705882352941177
		}]
	},
	"930365": {
		"nik": "930365",
		"nama": "GUSTAF GEYSBERT LONTOH",
		"band": "V",
		"posisi": "OFF 2 FRAUD & REVENUE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.046382189239332,
			"nilai_komparatif_unit": 1.047017856340043,
			"summary_team": 0.8545454545454546
		}]
	},
	"930379": {
		"nik": "930379",
		"nama": "MARIO DESKARLIE",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER ANALYSIS & SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8722222222222225,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.971150243536905,
			"nilai_komparatif_unit": 0.9717402079552718,
			"summary_team": 0.8470588235294119
		}]
	},
	"930381": {
		"nik": "930381",
		"nama": "DESREA ZAHRINA ALIANI",
		"band": "V",
		"posisi": "OFF 2 BUDGET CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9388888888888888,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006993006993007,
			"nilai_komparatif_unit": 1.0076047455449189,
			"summary_team": 0.9454545454545454
		}]
	},
	"930383": {
		"nik": "930383",
		"nama": "ARDINI PARAMITHA",
		"band": "V",
		"posisi": "OFF 2 NETWORK BUSINESS SOLUTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9787595581988102,
			"nilai_komparatif_unit": 0.9793541452025376,
			"summary_team": 0.8727272727272727
		}]
	},
	"930385": {
		"nik": "930385",
		"nama": "INDAH PRATIWI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9444444444444442,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0010695187165777,
			"nilai_komparatif_unit": 1.0016776588064193,
			"summary_team": 0.9454545454545454
		}]
	},
	"930389": {
		"nik": "930389",
		"nama": "MADE INDRA PURNATA",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8433333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0995328781890048,
			"nilai_komparatif_unit": 1.10020083382128,
			"summary_team": 0.9272727272727272
		}]
	},
	"930416": {
		"nik": "930416",
		"nama": "AININA SHADRINA",
		"band": "V",
		"posisi": "OFF 2 PERFORMANCE & ORDER ALIGNMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9055555555555558,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.983825989960959,
			"nilai_komparatif_unit": 0.9844236547731796,
			"summary_team": 0.8909090909090909
		}]
	},
	"935669": {
		"nik": "935669",
		"nama": "NORA ELIGIA CHRISTIAN",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8619883040935664,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.075737017392378,
			"nilai_komparatif_unit": 1.0763905172684323,
			"summary_team": 0.9272727272727272
		}]
	},
	"936130": {
		"nik": "936130",
		"nama": "MARTHA PURNAMA SARI PANGGABEAN",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85528455284553,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0841686830279966,
			"nilai_komparatif_unit": 1.0848273050597068,
			"summary_team": 0.9272727272727272
		}]
	},
	"936132": {
		"nik": "936132",
		"nama": "ZICO PARENTI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8878048780487822,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9830169830169811,
			"nilai_komparatif_unit": 0.983614156365276,
			"summary_team": 0.8727272727272727
		}]
	},
	"936133": {
		"nik": "936133",
		"nama": "CECELIA PUSPITA ARIYANI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333321,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0014204545454561,
			"nilai_komparatif_unit": 1.0020288078254336,
			"summary_team": 0.8545454545454546
		}]
	},
	"936172": {
		"nik": "936172",
		"nama": "ACHFID JANAHA",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9060109289617485,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.040668619679476,
			"nilai_komparatif_unit": 1.0413008158417154,
			"summary_team": 0.9428571428571426
		}]
	},
	"940006": {
		"nik": "940006",
		"nama": "RULLY AMAL PUTRA",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8394557823129278,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0179755414763487,
			"nilai_komparatif_unit": 1.01859395181217,
			"summary_team": 0.8545454545454546
		}]
	},
	"940018": {
		"nik": "940018",
		"nama": "ROSALINA ANWAR PUTRI",
		"band": "V",
		"posisi": "OFF 2 OFFERING 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0291595197255576,
			"nilai_komparatif_unit": 1.029784724220436,
			"summary_team": 0.9090909090909092
		}]
	},
	"940020": {
		"nik": "940020",
		"nama": "EVI KURNIA FEBRIANI",
		"band": "V",
		"posisi": "OFF 2 BUSINESS LEGAL & COMPLIANCE 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000004,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9898989898989895,
			"nilai_komparatif_unit": 0.9905003440001744,
			"summary_team": 0.8909090909090909
		}]
	},
	"940026": {
		"nik": "940026",
		"nama": "ADNAN FARID IMMADUDDIN",
		"band": "V",
		"posisi": "OFF 2 BIDDING FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181816,
			"nilai_komparatif_unit": 0.9824146269062956,
			"summary_team": 0.8181818181818181
		}]
	},
	"940030": {
		"nik": "940030",
		"nama": "Lutfi Jamil Setiawan",
		"band": "V",
		"posisi": "OFFICER 1 SPECTRUM PLANNING & MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930066,
			"nilai_komparatif_unit": 1.0076047455449184,
			"summary_team": 0.8727272727272728
		}]
	},
	"940044": {
		"nik": "940044",
		"nama": "BIMA RIZKI NURRAHMAN",
		"band": "V",
		"posisi": "OFF 2 TRAFFIC & DIGITAL AGREEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.873333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9814612868047985,
			"nilai_komparatif_unit": 0.9820575150826296,
			"summary_team": 0.857142857142857
		}]
	},
	"940046": {
		"nik": "940046",
		"nama": "RATRIAPRASTI SALSABIELA",
		"band": "V",
		"posisi": "OFF 2 ON-AIR PROMOTION MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0441176470588234,
			"nilai_komparatif_unit": 1.044751938471973,
			"summary_team": 0.9466666666666665
		}]
	},
	"940047": {
		"nik": "940047",
		"nama": "DINI APRILIANI",
		"band": "V",
		"posisi": "OFF 2  QUALITY & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9250000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.081081081081081,
			"nilai_komparatif_unit": 1.0817378274243497,
			"summary_team": 1
		}]
	},
	"940051": {
		"nik": "940051",
		"nama": "IRHAM FAUZAN ARDANTYO",
		"band": "V",
		"posisi": "OFF 2 BUSINESS PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.031404958677686,
			"nilai_komparatif_unit": 1.0320315272550986,
			"summary_team": 0.9454545454545454
		}]
	},
	"940052": {
		"nik": "940052",
		"nama": "FILIA STEFANY SIHOTANG",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8553191489361723,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8928086838534577,
			"nilai_komparatif_unit": 0.89335105652894,
			"summary_team": 0.7636363636363636
		}]
	},
	"940054": {
		"nik": "940054",
		"nama": "INDRI AGUSTINI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8324074074074084,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8736980483365344,
			"nilai_komparatif_unit": 0.874228811485023,
			"summary_team": 0.7272727272727272
		}]
	},
	"940056": {
		"nik": "940056",
		"nama": "ULFA SHAFIRA FATHARANI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8394557823129278,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.104611757772208,
			"nilai_komparatif_unit": 1.1052827987749079,
			"summary_team": 0.9272727272727272
		}]
	},
	"940065": {
		"nik": "940065",
		"nama": "MUHAMMAD ARIF WICAKSONO",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9294117647058822,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9585730724971233,
			"nilai_komparatif_unit": 0.9591553964052327,
			"summary_team": 0.8909090909090909
		}]
	},
	"940117": {
		"nik": "940117",
		"nama": "FAIZ IRFAN HAJID",
		"band": "V",
		"posisi": "OFF 2 OFFERING 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769228,
			"nilai_komparatif_unit": 0.9236376834161752,
			"summary_team": 0.7999999999999999
		}]
	},
	"940125": {
		"nik": "940125",
		"nama": "FADHILLA RAHMA FRISKA",
		"band": "V",
		"posisi": "OFF 2 COLLECTION & DEBT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0085763293310461,
			"nilai_komparatif_unit": 1.009189029736027,
			"summary_team": 0.8909090909090909
		}]
	},
	"940133": {
		"nik": "940133",
		"nama": "ANISA LUTFIANA",
		"band": "V",
		"posisi": "OFF 2 MARKET RESEARCH",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8787878787878791,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.062068965517241,
			"nilai_komparatif_unit": 1.0627141621834384,
			"summary_team": 0.9333333333333333
		}]
	},
	"940147": {
		"nik": "940147",
		"nama": "I GUSTI BAGUS KRISNANJAYA GHEARTHA",
		"band": "V",
		"posisi": "OFF 2 PERFORMANCE SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.893333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.023454157782516,
			"nilai_komparatif_unit": 1.0240758963249708,
			"summary_team": 0.914285714285714
		}]
	},
	"940151": {
		"nik": "940151",
		"nama": "MAHENDRA KURNIAWAN",
		"band": "V",
		"posisi": "OFF 2 PROJECT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000003,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.997402597402597,
			"nilai_komparatif_unit": 0.998008509873062,
			"summary_team": 0.8727272727272727
		}]
	},
	"940154": {
		"nik": "940154",
		"nama": "LAILA MUNZIAH",
		"band": "V",
		"posisi": "OFF 2 IPTV HEADEND O&M",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7111111111111115,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1406249999999993,
			"nilai_komparatif_unit": 1.1413179187004558,
			"summary_team": 0.8111111111111111
		}]
	},
	"940155": {
		"nik": "940155",
		"nama": "JULIKA GIVARY",
		"band": "V",
		"posisi": "OFF 2 OBL SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545452,
			"nilai_komparatif_unit": 1.026077499213242,
			"summary_team": 0.8545454545454546
		}]
	},
	"940173": {
		"nik": "940173",
		"nama": "RATIH AYU INDRASWARI",
		"band": "V",
		"posisi": "OFF 2 FAULT HANDLING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7515151515151519,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497311827956983,
			"nilai_komparatif_unit": 1.050368884377736,
			"summary_team": 0.7888888888888889
		}]
	},
	"940178": {
		"nik": "940178",
		"nama": "SANDRA PUTERI YOHANI",
		"band": "V",
		"posisi": "OFF 2 SALES SUPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.92,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9855072463768116,
			"nilai_komparatif_unit": 0.9861059325361102,
			"summary_team": 0.9066666666666667
		}]
	},
	"940181": {
		"nik": "940181",
		"nama": "MUHAMMAD FATHAN KUSUMARTONO",
		"band": "V",
		"posisi": "OFF 2 BUSINESS MODEL ENGINEERING & CUST",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8722222222222227,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9711502435369048,
			"nilai_komparatif_unit": 0.9717402079552716,
			"summary_team": 0.8470588235294119
		}]
	},
	"940189": {
		"nik": "940189",
		"nama": "NOVANGGA ILMAWAN",
		"band": "V",
		"posisi": "OFF 2 DATA MINING & ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.069930069930071,
			"nilai_komparatif_unit": 1.0705800421414773,
			"summary_team": 0.9272727272727272
		}]
	},
	"940193": {
		"nik": "940193",
		"nama": "AGUNG PRASETIYO",
		"band": "V",
		"posisi": "OFF 2 OTT SERVICE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8400000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9523809523809522,
			"nilai_komparatif_unit": 0.9529595146357366,
			"summary_team": 0.8
		}]
	},
	"940195": {
		"nik": "940195",
		"nama": "DIAN ARYANTI HAPSARI",
		"band": "V",
		"posisi": "OFF 2 SEGMENT PERFORMANCE SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0059583325620023,
			"summary_team": 0.8545454545454546
		}]
	},
	"940212": {
		"nik": "940212",
		"nama": "AGVI RAMADHAN KURNIAWAN",
		"band": "V",
		"posisi": "OFF 2 QUALITY ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8774774774774767,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0636550308008224,
			"nilai_komparatif_unit": 1.0643011909864017,
			"summary_team": 0.9333333333333333
		}]
	},
	"940234": {
		"nik": "940234",
		"nama": "DICKY MAHENDRA",
		"band": "V",
		"posisi": "OFF 2 TRANSACTION VALIDATION 3",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181818,
			"nilai_komparatif_unit": 0.9824146269062958,
			"summary_team": 0.8181818181818182
		}]
	},
	"940260": {
		"nik": "940260",
		"nama": "SYAUQINA HADYANI SEMBIRING MELIALA",
		"band": "V",
		"posisi": "OFF 2 OFFERING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888888,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0125,
			"nilai_komparatif_unit": 1.0131150839971175,
			"summary_team": 0.8999999999999999
		}]
	},
	"940262": {
		"nik": "940262",
		"nama": "DANANG FIRMANSYAH",
		"band": "V",
		"posisi": "OFF 2 TRANSACTION VALIDATION 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.789473684210526,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0133333333333334,
			"nilai_komparatif_unit": 1.013948923572424,
			"summary_team": 0.7999999999999998
		}]
	},
	"940270": {
		"nik": "940270",
		"nama": "ADIPUTRA NUSANTARA",
		"band": "V",
		"posisi": "OFF 2 ENTERPRISE SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8592592592592595,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9521943573667708,
			"nilai_komparatif_unit": 0.9527728062668814,
			"summary_team": 0.8181818181818181
		}]
	},
	"940278": {
		"nik": "940278",
		"nama": "FANY AGRIANSYAH ROSYADA",
		"band": "V",
		"posisi": "OFF 2 OFFERING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.111492281303602,
			"nilai_komparatif_unit": 1.1121675021580706,
			"summary_team": 0.9818181818181818
		}]
	},
	"940287": {
		"nik": "940287",
		"nama": "TIO HORAS SOADUON SIANTURI",
		"band": "V",
		"posisi": "OFF 2 RISK & FRAUD MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8600000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9457364341085268,
			"nilai_komparatif_unit": 0.9463109598824638,
			"summary_team": 0.8133333333333332
		}]
	},
	"940289": {
		"nik": "940289",
		"nama": "SETIYA WAHYU NUGRAHA",
		"band": "V",
		"posisi": "OFF 2 SALES OPERATION SOLUTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9500000000000002,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9824561403508768,
			"nilai_komparatif_unit": 0.9830529729926543,
			"summary_team": 0.9333333333333331
		}]
	},
	"940377": {
		"nik": "940377",
		"nama": "NUR KAMILA",
		"band": "V",
		"posisi": "OFF 2 INTERNET PRODUCT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8966666666666664,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0355815188528945,
			"nilai_komparatif_unit": 1.036210624650383,
			"summary_team": 0.9285714285714284
		}]
	},
	"945486": {
		"nik": "945486",
		"nama": "MUTIARA RIZKY OCTAVIANI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85528455284553,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.020394054614585,
			"nilai_komparatif_unit": 1.0210139341738418,
			"summary_team": 0.8727272727272727
		}]
	},
	"945657": {
		"nik": "945657",
		"nama": "HANE KARTIKA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8433333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0995328781890048,
			"nilai_komparatif_unit": 1.10020083382128,
			"summary_team": 0.9272727272727272
		}]
	},
	"945658": {
		"nik": "945658",
		"nama": "ARKE ILLA YUNDIA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8958333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0553911205073996,
			"nilai_komparatif_unit": 1.0560322604470778,
			"summary_team": 0.9454545454545454
		}]
	},
	"945660": {
		"nik": "945660",
		"nama": "AJENG ASTRIYANI",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8958333333333334,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0350951374207187,
			"nilai_komparatif_unit": 1.0357239477461722,
			"summary_team": 0.9272727272727272
		}]
	},
	"946028": {
		"nik": "946028",
		"nama": "SARAH JESSICA SIPAYUNG",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8324074074074084,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0047527555870146,
			"nilai_komparatif_unit": 1.0053631332077764,
			"summary_team": 0.8363636363636363
		}]
	},
	"946029": {
		"nik": "946029",
		"nama": "SHEILA CLAUDY RIADY",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8394557823129278,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0829527036982434,
			"nilai_komparatif_unit": 1.0836105870342236,
			"summary_team": 0.9090909090909092
		}]
	},
	"950001": {
		"nik": "950001",
		"nama": "QISTHY NABILAH BUSNIA",
		"band": "V",
		"posisi": "OFF 2 CUSTOMER ANALYSIS & SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333335,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.078801331853496,
			"nilai_komparatif_unit": 1.0794566932710685,
			"summary_team": 0.9529411764705882
		}]
	},
	"950008": {
		"nik": "950008",
		"nama": "DIVA TERRY ANONA",
		"band": "V",
		"posisi": "OFF 2 NETWORK PARTNERSHIP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666669,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0314049586776857,
			"nilai_komparatif_unit": 1.0320315272550984,
			"summary_team": 0.9454545454545454
		}]
	},
	"950019": {
		"nik": "950019",
		"nama": "MUHAMAD FADHIL ABDULLAH",
		"band": "V",
		"posisi": "OFF 2 OFFERING 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1249999999999998,
			"nilai_komparatif_unit": 1.1256834266634637,
			"summary_team": 1
		}]
	},
	"950024": {
		"nik": "950024",
		"nama": "FAHIMAH RAHMADIAN",
		"band": "V",
		"posisi": "OFF 2 INFRA & SERVICE CONTRACT MGT EVAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.905555555555555,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9743774810537718,
			"nilai_komparatif_unit": 0.9749694059878438,
			"summary_team": 0.8823529411764706
		}]
	},
	"950032": {
		"nik": "950032",
		"nama": "CITA DIPLOMAWATI",
		"band": "V",
		"posisi": "OFF 2 LEGAL, REGULATION & COMPLIANCES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.873333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9651035986913853,
			"nilai_komparatif_unit": 0.9656898898312526,
			"summary_team": 0.8428571428571427
		}]
	},
	"950043": {
		"nik": "950043",
		"nama": "MARIYATUL KIBTIYAH",
		"band": "V",
		"posisi": "OFF 2 SEGMENT INDUSTRIAL ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0115702479338844,
			"nilai_komparatif_unit": 1.0121847671155777,
			"summary_team": 0.9272727272727272
		}]
	},
	"950045": {
		"nik": "950045",
		"nama": "NUR FAJRI MUTHOHARI",
		"band": "V",
		"posisi": "OFF 2 DATA SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8862745098039216,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0907079646017697,
			"nilai_komparatif_unit": 1.0913705591840464,
			"summary_team": 0.9666666666666666
		}]
	},
	"950057": {
		"nik": "950057",
		"nama": "M. IKROMIL AMRILLAH",
		"band": "V",
		"posisi": "OFF 2 TRAFFIC & DIGITAL BIZ PLANNING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197862,
			"nilai_komparatif_unit": 0.9631515950061724,
			"summary_team": 0.8181818181818182
		}]
	},
	"950068": {
		"nik": "950068",
		"nama": "ICHA KAUTSARI EKA PUTRI",
		"band": "V",
		"posisi": "OFF 2 CONTRACT DRAFTING & REVIEW",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333337,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0379523389232121,
			"nilai_komparatif_unit": 1.0385828849710566,
			"summary_team": 0.8909090909090909
		}]
	},
	"950072": {
		"nik": "950072",
		"nama": "NOVIA AYU SUNDARI",
		"band": "V",
		"posisi": "OFF 2 BASIC DELIVERY OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930064,
			"nilai_komparatif_unit": 1.0076047455449182,
			"summary_team": 0.8727272727272727
		}]
	},
	"950077": {
		"nik": "950077",
		"nama": "FAUZI RACHMAN WIJAYA",
		"band": "V",
		"posisi": "OFF 2 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1114922813036017,
			"nilai_komparatif_unit": 1.1121675021580704,
			"summary_team": 0.9818181818181818
		}]
	},
	"950083": {
		"nik": "950083",
		"nama": "JERI ISLAMI",
		"band": "V",
		"posisi": "OFF 2 ES COLLECTION SEGMENT A",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444444,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9473684210526316,
			"nilai_komparatif_unit": 0.9479439382429171,
			"summary_team": 0.8
		}]
	},
	"950089": {
		"nik": "950089",
		"nama": "FAIZ BURHANUDDIN RAMDHANI",
		"band": "V",
		"posisi": "OFF 2 LOGISTIC & PROCESS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8545454545454548,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9985815602836875,
			"nilai_komparatif_unit": 0.9991881889627464,
			"summary_team": 0.8533333333333333
		}]
	},
	"950092": {
		"nik": "950092",
		"nama": "LARAS QATRUNNADA",
		"band": "V",
		"posisi": "OFF 2 ACQUISITION READINESS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0980861244019133,
			"nilai_komparatif_unit": 1.0987532011451988,
			"summary_team": 0.9272727272727272
		}]
	},
	"950097": {
		"nik": "950097",
		"nama": "MIFTAKHUL ARIF",
		"band": "V",
		"posisi": "OFF 2 DIGITAL PLATFORM PRODUCT READINESS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0044642857142856,
			"nilai_komparatif_unit": 1.0050744880923783,
			"summary_team": 0.857142857142857
		}]
	},
	"950116": {
		"nik": "950116",
		"nama": "FATHIMAH RAIHANAH SALIM MARTAK",
		"band": "V",
		"posisi": "OFF 2 SUBSIDIARY EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333328,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.037946428571429,
			"nilai_komparatif_unit": 1.0385769710287915,
			"summary_team": 0.8857142857142857
		}]
	},
	"950130": {
		"nik": "950130",
		"nama": "FANDHI AHMAD",
		"band": "V",
		"posisi": "OFF 2 SALES - 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8595238095238087,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9972299168975077,
			"nilai_komparatif_unit": 0.9978357244662293,
			"summary_team": 0.857142857142857
		}]
	},
	"950142": {
		"nik": "950142",
		"nama": "ANINDYA VARADITA",
		"band": "V",
		"posisi": "OFF 2 RESOURCE & BUDGET CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8711111111111108,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9675655976676385,
			"nilai_komparatif_unit": 0.9681533844481688,
			"summary_team": 0.8428571428571426
		}]
	},
	"950144": {
		"nik": "950144",
		"nama": "EGA FAJAR YUSUP",
		"band": "V",
		"posisi": "OFF 2 DATA MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7151515151515154,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0564971751412424,
			"nilai_komparatif_unit": 1.0571389869984567,
			"summary_team": 0.7555555555555555
		}]
	},
	"950162": {
		"nik": "950162",
		"nama": "BINTANG RAMADHAN",
		"band": "V",
		"posisi": "OFF 2 SALES PERFORMANCE & OFFERING MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8894308943089421,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9198936347016797,
			"nilai_komparatif_unit": 0.9204524612239072,
			"summary_team": 0.8181818181818182
		}]
	},
	"950174": {
		"nik": "950174",
		"nama": "MOHAMMAD REZA KEMAL FIKRY",
		"band": "V",
		"posisi": "OFF 2 DATA SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8844444444444439,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.944428022465268,
			"nilai_komparatif_unit": 0.945001753391735,
			"summary_team": 0.8352941176470587
		}]
	},
	"950199": {
		"nik": "950199",
		"nama": "HYANGNAWANG DHUMARANANG",
		"band": "V",
		"posisi": "OFF 2 RISK MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.873333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0141766630316251,
			"nilai_komparatif_unit": 1.014792765585384,
			"summary_team": 0.8857142857142856
		}]
	},
	"955476": {
		"nik": "955476",
		"nama": "HARDIKA ANUGRAH RAMADHAN",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8771929824561402,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.077818181818182,
			"nilai_komparatif_unit": 1.0784729459815783,
			"summary_team": 0.9454545454545454
		}]
	},
	"955477": {
		"nik": "955477",
		"nama": "RATNA JUWITA",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8390804597701145,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7831702544031314,
			"nilai_komparatif_unit": 0.7836460227888122,
			"summary_team": 0.657142857142857
		}]
	},
	"955511": {
		"nik": "955511",
		"nama": "RAHMADHANI PUTRI YUWANDI",
		"band": "V",
		"posisi": "JUNIOR ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8293040293040309,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1400578220366184,
			"nilai_komparatif_unit": 1.1407503961819256,
			"summary_team": 0.9454545454545454
		}]
	},
	"955911": {
		"nik": "955911",
		"nama": "RIZKA LENGGOGINI",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333338,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8684907325684018,
			"nilai_komparatif_unit": 0.8690183323227205,
			"summary_team": 0.7454545454545454
		}]
	},
	"955947": {
		"nik": "955947",
		"nama": "NURUL FADHILA IZHAR",
		"band": "V",
		"posisi": "ACCOUNT MANAGER 1",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8619883040935664,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.075737017392378,
			"nilai_komparatif_unit": 1.0763905172684323,
			"summary_team": 0.9272727272727272
		}]
	},
	"960003": {
		"nik": "960003",
		"nama": "LILIN",
		"band": "V",
		"posisi": "OFF 2 APPLICATION MGT & IT INTERFACE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969697,
			"nilai_komparatif_unit": 0.9702860512654774,
			"summary_team": 0.8727272727272728
		}]
	},
	"960004": {
		"nik": "960004",
		"nama": "SVADEV PRASHANT MAHASAGARA",
		"band": "V",
		"posisi": "OFF 2 BUSINESS PERFORMANCE & SYNERGY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888882,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0050000000000008,
			"nilai_komparatif_unit": 1.0056105278193619,
			"summary_team": 0.8933333333333333
		}]
	},
	"960038": {
		"nik": "960038",
		"nama": "SAVITRI SEKAR PRASETYA",
		"band": "V",
		"posisi": "OFF 2 RESOURCES PLANNING & DELIV SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8844444444444437,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0109370381318363,
			"nilai_komparatif_unit": 1.0115511726446742,
			"summary_team": 0.8941176470588235
		}]
	},
	"960066": {
		"nik": "960066",
		"nama": "STELLA NADYA ARVITA",
		"band": "V",
		"posisi": "OFF 2 SOLUTION PLANNING & INNOVATION MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9993928784529682,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365352,
			"nilai_komparatif_unit": 0.9885933352516184,
			"summary_team": 0.8727272727272727
		}]
	}
};