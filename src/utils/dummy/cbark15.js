export const cbark15 = {
	"650106": {
		"nik": "650106",
		"nama": "ELLY NOVARI",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8400000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9920634920634919,
			"nilai_komparatif_unit": 0.9990157669018535,
			"summary_team": 0.8333333333333335
		}]
	},
	"650170": {
		"nik": "650170",
		"nama": "I GUSTI NYOMAN ALEKS",
		"band": "IV",
		"posisi": "ASMAN CCAN FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8250000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.018181818181818,
			"nilai_komparatif_unit": 1.0253171274559243,
			"summary_team": 0.8400000000000001
		}]
	},
	"650376": {
		"nik": "650376",
		"nama": "WAHYU ANGGRAINI, RR.",
		"band": "IV",
		"posisi": "ASMAN HR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9720972097209719,
			"nilai_komparatif_unit": 0.9789095629883292,
			"summary_team": 0.8181818181818182
		}]
	},
	"650378": {
		"nik": "650378",
		"nama": "FAYAKUNAH",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9519230769230771,
			"nilai_komparatif_unit": 0.9585940520256712,
			"summary_team": 0.8250000000000002
		}]
	},
	"650389": {
		"nik": "650389",
		"nama": "HERU BIANTORO",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8294117647058812,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9645390070922,
			"nilai_komparatif_unit": 0.9712983932839824,
			"summary_team": 0.8
		}]
	},
	"650426": {
		"nik": "650426",
		"nama": "HARFI",
		"band": "IV",
		"posisi": "OFF 1 PRICING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7583333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8791208791208788,
			"nilai_komparatif_unit": 0.8852816642084116,
			"summary_team": 0.6666666666666666
		}]
	},
	"650585": {
		"nik": "650585",
		"nama": "SOEDARJANTO",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8000000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.038461538461538,
			"nilai_komparatif_unit": 1.0457389658461862,
			"summary_team": 0.8307692307692307
		}]
	},
	"650798": {
		"nik": "650798",
		"nama": "LENNY DWIYANTI",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857144,
			"nilai_komparatif_unit": 0.9710433254286019,
			"summary_team": 0.9000000000000001
		}]
	},
	"650921": {
		"nik": "650921",
		"nama": "RAJJA",
		"band": "IV",
		"posisi": "ASMAN SURVEILLANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777781,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0563866513233597,
			"nilai_komparatif_unit": 1.0637896959816209,
			"summary_team": 0.9272727272727272
		}]
	},
	"651347": {
		"nik": "651347",
		"nama": "I KETUT UTAMA YASA",
		"band": "IV",
		"posisi": "ASMAN OM SWITCHING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0463821892393321,
			"nilai_komparatif_unit": 1.053715123697415,
			"summary_team": 0.8545454545454546
		}]
	},
	"651450": {
		"nik": "651450",
		"nama": "IDA KETUT PURWANTA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8277777777777782,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9884075655887732,
			"nilai_komparatif_unit": 0.9953342200854486,
			"summary_team": 0.8181818181818182
		}]
	},
	"651594": {
		"nik": "651594",
		"nama": "NASRUM BAHAR",
		"band": "IV",
		"posisi": "ASMAN SALES SUPPORT 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8410256410256411,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8597560975609755,
			"nilai_komparatif_unit": 0.8657811763306502,
			"summary_team": 0.7230769230769231
		}]
	},
	"651596": {
		"nik": "651596",
		"nama": "FRANSISKUS EGUS",
		"band": "IV",
		"posisi": "ASMAN HUBUNGAN ANTAR INSTANSI/PEMDA",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0714285714285712,
			"nilai_komparatif_unit": 1.0789370282540018,
			"summary_team": 1
		}]
	},
	"651624": {
		"nik": "651624",
		"nama": "MOCHAMAD SYAHUDI",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9238095238095239,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0037488284910965,
			"nilai_komparatif_unit": 1.010782992917245,
			"summary_team": 0.9272727272727272
		}]
	},
	"660012": {
		"nik": "660012",
		"nama": "MOHAMAD KOMARI",
		"band": "IV",
		"posisi": "ASMAN OM TRANSPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666665,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9072580645161293,
			"nilai_komparatif_unit": 0.9136160319892762,
			"summary_team": 0.7500000000000001
		}]
	},
	"660013": {
		"nik": "660013",
		"nama": "YUNI AKHIRIYAH",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8512820512820516,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0068846815834764,
			"nilai_komparatif_unit": 1.0139408217326762,
			"summary_team": 0.8571428571428572
		}]
	},
	"660016": {
		"nik": "660016",
		"nama": "HARI SETYO IRAWAN",
		"band": "IV",
		"posisi": "OFF 1 SECURITY & SAFETY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8549019607843138,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9357798165137614,
			"nilai_komparatif_unit": 0.9423376613741374,
			"summary_team": 0.8
		}]
	},
	"660032": {
		"nik": "660032",
		"nama": "NIRMAWATI",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8380952380952383,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9843749999999997,
			"nilai_komparatif_unit": 0.991273394708364,
			"summary_team": 0.825
		}]
	},
	"660044": {
		"nik": "660044",
		"nama": "PUGUH TAWANTO",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9843750000000001,
			"nilai_komparatif_unit": 0.9912733947083644,
			"summary_team": 0.8750000000000002
		}]
	},
	"660051": {
		"nik": "660051",
		"nama": "ENNY SRI SUDJIJATININGSIH",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8388888888888892,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0186634557495482,
			"nilai_komparatif_unit": 1.0258021402882116,
			"summary_team": 0.8545454545454546
		}]
	},
	"660070": {
		"nik": "660070",
		"nama": "GUSTI RAHMADI",
		"band": "IV",
		"posisi": "ASMAN SATELIT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8133333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Down Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.6147540983606559,
			"nilai_komparatif_unit": 0.6190622293260668,
			"summary_team": 0.5000000000000001
		}]
	},
	"660071": {
		"nik": "660071",
		"nama": "ARTATI YUNIAR",
		"band": "IV",
		"posisi": "ASMAN HR DEVELOPMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346935,
			"nilai_komparatif_unit": 0.9864567115465157,
			"summary_team": 0.8
		}]
	},
	"660096": {
		"nik": "660096",
		"nama": "MASLIKAN",
		"band": "IV",
		"posisi": "ASMAN INTEGRATION SYSTEM SITE ADVALJAR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0019417475728152,
			"nilai_komparatif_unit": 1.008963248169179,
			"summary_team": 0.8600000000000001
		}]
	},
	"660137": {
		"nik": "660137",
		"nama": "NURDIN SYABAN",
		"band": "IV",
		"posisi": "ASMAN OM RADIO & MUX",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8600000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9513742071881605,
			"nilai_komparatif_unit": 0.958041335870361,
			"summary_team": 0.8181818181818182
		}]
	},
	"660156": {
		"nik": "660156",
		"nama": "ABD HARIS",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8083333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9154639175257732,
			"nilai_komparatif_unit": 0.9218793907390895,
			"summary_team": 0.7400000000000002
		}]
	},
	"660164": {
		"nik": "660164",
		"nama": "BAKHTIAR S.",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "DE-2-01",
			"kriteria": 0.8222222222222222,
			"kriteria_bp": 0.9938808491689765,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.972972972972973,
			"nilai_komparatif_unit": 0.9789633976613139,
			"summary_team": 0.8
		}]
	},
	"660173": {
		"nik": "660173",
		"nama": "KUSWANDI",
		"band": "IV",
		"posisi": "ASMAN CRM & LEVERAGING CUSTOMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666661,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769237,
			"nilai_komparatif_unit": 0.9295457474188331,
			"summary_team": 0.8
		}]
	},
	"660181": {
		"nik": "660181",
		"nama": "RACHMAT BASUKI",
		"band": "IV",
		"posisi": "OFF 1 PROCUREMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559436,
			"nilai_komparatif_unit": 0.9506717871328965,
			"summary_team": 0.8181818181818181
		}]
	},
	"660209": {
		"nik": "660209",
		"nama": "IGNATIUS LIUNG TUKAN",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8222222222222222,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8817567567567569,
			"nilai_komparatif_unit": 0.8879360137928207,
			"summary_team": 0.7250000000000001
		}]
	},
	"660211": {
		"nik": "660211",
		"nama": "ABDUS SAMAD",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.986013986013986,
			"nilai_komparatif_unit": 0.9929238665610256,
			"summary_team": 0.8545454545454546
		}]
	},
	"660215": {
		"nik": "660215",
		"nama": "ASRIYATI",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8596491228070172,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9887755102040819,
			"nilai_komparatif_unit": 0.995704743217265,
			"summary_team": 0.85
		}]
	},
	"660223": {
		"nik": "660223",
		"nama": "GUNAWAN",
		"band": "IV",
		"posisi": "ASMAN FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1015004413062661,
			"nilai_komparatif_unit": 1.1092196385792241,
			"summary_team": 0.9454545454545454
		}]
	},
	"660264": {
		"nik": "660264",
		"nama": "DIAH SAPTAPUTRI DECIANTI",
		"band": "IV",
		"posisi": "OFF 1 RUMAH KREATIF BUMN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8361904761904755,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8697452888796859,
			"nilai_komparatif_unit": 0.8758403708336491,
			"summary_team": 0.7272727272727273
		}]
	},
	"660309": {
		"nik": "660309",
		"nama": "SIMON MUSA MONTONGLAYUK",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK & LEGAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7833333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0212765957446805,
			"nilai_komparatif_unit": 1.0284335928889208,
			"summary_team": 0.8
		}]
	},
	"660384": {
		"nik": "660384",
		"nama": "WIDODO BASUKI, ST",
		"band": "IV",
		"posisi": "ASMAN OM IP NETWORK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8708333333333331,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0207336523126,
			"nilai_komparatif_unit": 1.027886844567343,
			"summary_team": 0.888888888888889
		}]
	},
	"660475": {
		"nik": "660475",
		"nama": "SUSANTI ANDAYANI",
		"band": "IV",
		"posisi": "ASMAN HR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144382,
			"nilai_komparatif_unit": 1.0339332377706796,
			"summary_team": 0.8727272727272727
		}]
	},
	"660476": {
		"nik": "660476",
		"nama": "SUPADMI",
		"band": "IV",
		"posisi": "ASMAN CDC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8909090909090911,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.010204081632653,
			"nilai_komparatif_unit": 1.0172834837823446,
			"summary_team": 0.9000000000000001
		}]
	},
	"660477": {
		"nik": "660477",
		"nama": "SYAMSIR ALAM",
		"band": "IV",
		"posisi": "ASMAN CDC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7904761904761907,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.012048192771084,
			"nilai_komparatif_unit": 1.0191405182543822,
			"summary_team": 0.8
		}]
	},
	"660524": {
		"nik": "660524",
		"nama": "TITI HERLY",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7904761904761906,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8970427163198247,
			"nilai_komparatif_unit": 0.9033290957254754,
			"summary_team": 0.7090909090909091
		}]
	},
	"660531": {
		"nik": "660531",
		"nama": "AGUSTINUS",
		"band": "IV",
		"posisi": "ASMAN DATA CENTER & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8133333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9836065573770495,
			"nilai_komparatif_unit": 0.990499566921707,
			"summary_team": 0.8000000000000003
		}]
	},
	"660572": {
		"nik": "660572",
		"nama": "FELICIANO VALENTE",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.901960784313725,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.886956521739131,
			"nilai_komparatif_unit": 0.8931722181720092,
			"summary_team": 0.8
		}]
	},
	"660574": {
		"nik": "660574",
		"nama": "NYOMAN GUNAMA",
		"band": "IV",
		"posisi": "ASMAN GES QUALITY & PROJECT MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8749999999999994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.997402597402598,
			"nilai_komparatif_unit": 1.0043922881200897,
			"summary_team": 0.8727272727272727
		}]
	},
	"660577": {
		"nik": "660577",
		"nama": "I KETUT TARKA",
		"band": "IV",
		"posisi": "ASMAN NETWORK PERF, QM & ADMIN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8632478632478621,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.926732673267328,
			"nilai_komparatif_unit": 0.933227116715542,
			"summary_team": 0.8
		}]
	},
	"660592": {
		"nik": "660592",
		"nama": "MAHMILAN",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444446,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.994736842105263,
			"nilai_komparatif_unit": 1.001707851494768,
			"summary_team": 0.84
		}]
	},
	"660593": {
		"nik": "660593",
		"nama": "SUPARDI",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.132867132867133,
			"nilai_komparatif_unit": 1.1408061445594764,
			"summary_team": 0.9818181818181818
		}]
	},
	"660594": {
		"nik": "660594",
		"nama": "MIN MUKHTAR",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0303030303030303,
			"nilai_komparatif_unit": 1.0375232837351616,
			"summary_team": 0.9272727272727272
		}]
	},
	"660647": {
		"nik": "660647",
		"nama": "SELA MARTONO",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9142857142857144,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.91875,
			"nilai_komparatif_unit": 0.9251885017278066,
			"summary_team": 0.8400000000000001
		}]
	},
	"660650": {
		"nik": "660650",
		"nama": "HARI MURTONO",
		"band": "IV",
		"posisi": "ASMAN CCAN WIFI FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.911111111111111,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9054878048780491,
			"nilai_komparatif_unit": 0.9118333665610044,
			"summary_team": 0.8250000000000002
		}]
	},
	"670002": {
		"nik": "670002",
		"nama": "PRIYONO",
		"band": "IV",
		"posisi": "ASMAN OM IP NETWORK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8857142857142859,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9314516129032259,
			"nilai_komparatif_unit": 0.9379791261756566,
			"summary_team": 0.8250000000000002
		}]
	},
	"670004": {
		"nik": "670004",
		"nama": "MARSELUS SAMSON",
		"band": "IV",
		"posisi": "ASMAN DEPLOYMENT & CAPEX MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8791666666666665,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9781990521327016,
			"nilai_komparatif_unit": 0.9850541664590095,
			"summary_team": 0.8600000000000001
		}]
	},
	"670050": {
		"nik": "670050",
		"nama": "DEWA AYU ADI TRISNA DEWI",
		"band": "IV",
		"posisi": "ASMAN PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0542056074766353,
			"nilai_komparatif_unit": 1.0615933676129095,
			"summary_team": 0.9400000000000002
		}]
	},
	"670107": {
		"nik": "670107",
		"nama": "RADIANT MUDJAHIDIN",
		"band": "IV",
		"posisi": "ASMAN OM SWITCHING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7888888888888893,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.014084507042253,
			"nilai_komparatif_unit": 1.0211911027981535,
			"summary_team": 0.8
		}]
	},
	"670188": {
		"nik": "670188",
		"nama": "HERI PRASETYO",
		"band": "IV",
		"posisi": "ASMAN IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7888888888888893,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.014084507042253,
			"nilai_komparatif_unit": 1.0211911027981535,
			"summary_team": 0.8
		}]
	},
	"670316": {
		"nik": "670316",
		"nama": "NI LUH PUSPAWATI KARANG",
		"band": "IV",
		"posisi": "ASMAN CDC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969696,
			"nilai_komparatif_unit": 0.9764925023389754,
			"summary_team": 0.8727272727272728
		}]
	},
	"670336": {
		"nik": "670336",
		"nama": "FRANSISCUS XAVERIUS HARGONO",
		"band": "IV",
		"posisi": "ASMAN GES SALES ENGINEER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8749999999999994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0181818181818187,
			"nilai_komparatif_unit": 1.025317127455925,
			"summary_team": 0.8909090909090909
		}]
	},
	"670337": {
		"nik": "670337",
		"nama": "KEMIS SUGIRAN",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888893,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0431818181818178,
			"nilai_komparatif_unit": 1.0504923247818505,
			"summary_team": 0.9272727272727272
		}]
	},
	"670338": {
		"nik": "670338",
		"nama": "EFRIN",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7333333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8545454545454542,
			"nilai_komparatif_unit": 0.8605340176862218,
			"summary_team": 0.6266666666666666
		}]
	},
	"670339": {
		"nik": "670339",
		"nama": "MAKMUR HAFIEDZ",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888888,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.984375,
			"nilai_komparatif_unit": 0.9912733947083643,
			"summary_team": 0.875
		}]
	},
	"670362": {
		"nik": "670362",
		"nama": "AGUS RUSANTO",
		"band": "IV",
		"posisi": "ASMAN HOME SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.763636363636364,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9428571428571422,
			"nilai_komparatif_unit": 0.949464584863521,
			"summary_team": 0.7199999999999999
		}]
	},
	"670386": {
		"nik": "670386",
		"nama": "SUTIKNO",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9142857142857144,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9545454545454546,
			"nilai_komparatif_unit": 0.9612348069899291,
			"summary_team": 0.8727272727272728
		}]
	},
	"670459": {
		"nik": "670459",
		"nama": "SURISTIONO",
		"band": "IV",
		"posisi": "ASMAN CDC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0096153846153848,
			"nilai_komparatif_unit": 1.0166906612393483,
			"summary_team": 0.8750000000000002
		}]
	},
	"670490": {
		"nik": "670490",
		"nama": "SAHRIR",
		"band": "IV",
		"posisi": "ASMAN CDC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1,
			"nilai_komparatif_unit": 1.0070078930370685,
			"summary_team": 0.7999999999999999
		}]
	},
	"670509": {
		"nik": "670509",
		"nama": "SARIYONO",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.6750000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1043771043771042,
			"nilai_komparatif_unit": 1.1121164609971665,
			"summary_team": 0.7454545454545455
		}]
	},
	"670510": {
		"nik": "670510",
		"nama": "FARKAN EFFENDI IDIPASI",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.6750000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9158249158249155,
			"nilai_komparatif_unit": 0.9222429188756989,
			"summary_team": 0.6181818181818182
		}]
	},
	"670604": {
		"nik": "670604",
		"nama": "AKHMAD BADARUDIN HARIANTO",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8299999999999994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.963855421686748,
			"nilai_komparatif_unit": 0.9706100173851273,
			"summary_team": 0.8000000000000003
		}]
	},
	"670605": {
		"nik": "670605",
		"nama": "BUDI HARTADI",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0530612244897957,
			"nilai_komparatif_unit": 1.0604409649125046,
			"summary_team": 0.8600000000000001
		}]
	},
	"670607": {
		"nik": "670607",
		"nama": "SOEGI HARIYANTO",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7777777777777777,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9321428571428574,
			"nilai_komparatif_unit": 0.938675214580982,
			"summary_team": 0.7250000000000001
		}]
	},
	"670608": {
		"nik": "670608",
		"nama": "ARI HARUN WONGKAR",
		"band": "IV",
		"posisi": "ASMAN CONSTRUCTION SUPERVISION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8370370370370372,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0108917631041523,
			"nilai_komparatif_unit": 1.0179759844520397,
			"summary_team": 0.8461538461538461
		}]
	},
	"670613": {
		"nik": "670613",
		"nama": "KUKUH WAHYU SETIJONO",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888888,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0125000000000002,
			"nilai_komparatif_unit": 1.019595491700032,
			"summary_team": 0.9000000000000001
		}]
	},
	"680001": {
		"nik": "680001",
		"nama": "SELAMAT",
		"band": "IV",
		"posisi": "ASMAN OLO FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7583333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.959040959040959,
			"nilai_komparatif_unit": 0.9657618155000857,
			"summary_team": 0.7272727272727273
		}]
	},
	"680046": {
		"nik": "680046",
		"nama": "IKE RATNAWATI",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.848148148148148,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0414847161572054,
			"nilai_komparatif_unit": 1.0487833296477767,
			"summary_team": 0.8833333333333333
		}]
	},
	"680078": {
		"nik": "680078",
		"nama": "ENIE WAHYU LISTIJANINGSIH, IR",
		"band": "IV",
		"posisi": "ASMAN DEPLOYMENT & CAPEX MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0118343195266268,
			"nilai_komparatif_unit": 1.0189251462091045,
			"summary_team": 0.8769230769230769
		}]
	},
	"680129": {
		"nik": "680129",
		"nama": "LILY AMARAL DA SILVA ALVES",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK & LEGAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9318181818181819,
			"nilai_komparatif_unit": 0.9383482639663594,
			"summary_team": 0.7454545454545455
		}]
	},
	"680130": {
		"nik": "680130",
		"nama": "I MADE SUBAGIA",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8458333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9458128078817735,
			"nilai_komparatif_unit": 0.9524409628724984,
			"summary_team": 0.8
		}]
	},
	"680134": {
		"nik": "680134",
		"nama": "KETUT SUABAWA",
		"band": "IV",
		"posisi": "ASMAN NTE MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.048543689320388,
			"nilai_komparatif_unit": 1.0558917713398386,
			"summary_team": 0.9000000000000001
		}]
	},
	"680136": {
		"nik": "680136",
		"nama": "DEKY STEFANUS ROHI DJAWA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8323232323232315,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0485436893203894,
			"nilai_komparatif_unit": 1.05589177133984,
			"summary_team": 0.8727272727272727
		}]
	},
	"680137": {
		"nik": "680137",
		"nama": "IMAM TARMEDI",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7611111111111115,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9555408095554077,
			"nilai_komparatif_unit": 0.9622371373413258,
			"summary_team": 0.7272727272727273
		}]
	},
	"680138": {
		"nik": "680138",
		"nama": "SAFARIN",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444444,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9473684210526319,
			"nilai_komparatif_unit": 0.9540074776140652,
			"summary_team": 0.8000000000000003
		}]
	},
	"680222": {
		"nik": "680222",
		"nama": "CAHYO SUBIANTO",
		"band": "IV",
		"posisi": "ASMAN CCAN FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7666666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9486166007905141,
			"nilai_komparatif_unit": 0.9552644044620416,
			"summary_team": 0.7272727272727274
		}]
	},
	"680342": {
		"nik": "680342",
		"nama": "DJAMIATUL RACHMAH",
		"band": "IV",
		"posisi": "ASMAN NTE MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8909090909090911,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346936,
			"nilai_komparatif_unit": 0.9864567115465158,
			"summary_team": 0.8727272727272727
		}]
	},
	"680345": {
		"nik": "680345",
		"nama": "KUSNANTO",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9380952380952381,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0331901600937132,
			"nilai_komparatif_unit": 1.0404306462226016,
			"summary_team": 0.969230769230769
		}]
	},
	"680346": {
		"nik": "680346",
		"nama": "ZAINAL ARIFIN",
		"band": "IV",
		"posisi": "ASMAN BUSINESS OBL & DELIVERY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333328,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9375000000000009,
			"nilai_komparatif_unit": 0.9440698997222526,
			"summary_team": 0.8000000000000003
		}]
	},
	"680489": {
		"nik": "680489",
		"nama": "AGUS SUYITNO",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7111111111111109,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000004,
			"nilai_komparatif_unit": 1.007007893037069,
			"summary_team": 0.7111111111111112
		}]
	},
	"680582": {
		"nik": "680582",
		"nama": "SYAIFUL HAYAT",
		"band": "IV",
		"posisi": "ASMAN ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8460784313725501,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9455388180764762,
			"nilai_komparatif_unit": 0.9521650529759523,
			"summary_team": 0.8
		}]
	},
	"690043": {
		"nik": "690043",
		"nama": "I MADE PANDE WINDIA",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8749999999999994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649356,
			"nilai_komparatif_unit": 0.9416177701125841,
			"summary_team": 0.8181818181818181
		}]
	},
	"690045": {
		"nik": "690045",
		"nama": "NI WAYAN MURDANI",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7833333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.021276595744681,
			"nilai_komparatif_unit": 1.0284335928889212,
			"summary_team": 0.8
		}]
	},
	"690049": {
		"nik": "690049",
		"nama": "MULYADI NOOR",
		"band": "IV",
		"posisi": "ASMAN DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8848484848484849,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.008429926238145,
			"nilai_komparatif_unit": 1.0154968952966008,
			"summary_team": 0.8923076923076921
		}]
	},
	"690272": {
		"nik": "690272",
		"nama": "JULIARNIS",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8388888888888892,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0186634557495482,
			"nilai_komparatif_unit": 1.0258021402882116,
			"summary_team": 0.8545454545454546
		}]
	},
	"690278": {
		"nik": "690278",
		"nama": "ZURAIDAH NOOR HAJATY",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9380952380952381,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0659898477157361,
			"nilai_komparatif_unit": 1.073460190547129,
			"summary_team": 1
		}]
	},
	"690279": {
		"nik": "690279",
		"nama": "KEN MARYANI",
		"band": "IV",
		"posisi": "OFF 1 HR SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.06951871657754,
			"nilai_komparatif_unit": 1.0770137893444582,
			"summary_team": 0.9090909090909092
		}]
	},
	"690282": {
		"nik": "690282",
		"nama": "RATMI SINTA PARMIYATI",
		"band": "IV",
		"posisi": "ASMAN DEBT MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8857142857142859,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9648093841642228,
			"nilai_komparatif_unit": 0.9715706651296057,
			"summary_team": 0.8545454545454546
		}]
	},
	"690284": {
		"nik": "690284",
		"nama": "SOEKARDJI",
		"band": "IV",
		"posisi": "ASMAN DATA CENTER & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8250000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969694,
			"nilai_komparatif_unit": 0.9764925023389752,
			"summary_team": 0.8
		}]
	},
	"690384": {
		"nik": "690384",
		"nama": "TRIMURTIANTO",
		"band": "IV",
		"posisi": "ASMAN OM IP NETWORK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.011764705882353,
			"nilai_komparatif_unit": 1.0188550447198574,
			"summary_team": 0.8600000000000001
		}]
	},
	"690385": {
		"nik": "690385",
		"nama": "I GUSTI AYU LIANASARI",
		"band": "IV",
		"posisi": "ASMAN BUSINESS TERITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8416666666666658,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0369036903690378,
			"nilai_komparatif_unit": 1.0441702005208857,
			"summary_team": 0.8727272727272727
		}]
	},
	"690392": {
		"nik": "690392",
		"nama": "AGUS SETIAWAN",
		"band": "IV",
		"posisi": "ASMAN NETWORK PERF, QM & ADMIN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882352,
			"nilai_komparatif_unit": 0.9477721346231232,
			"summary_team": 0.8
		}]
	},
	"690393": {
		"nik": "690393",
		"nama": "I DEWA GDE AGUNG MAHA PUTRA",
		"band": "IV",
		"posisi": "ASMAN CCAN WIFI FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8416666666666658,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0369036903690378,
			"nilai_komparatif_unit": 1.0441702005208857,
			"summary_team": 0.8727272727272727
		}]
	},
	"690421": {
		"nik": "690421",
		"nama": "HENDRO MUDAKIR",
		"band": "IV",
		"posisi": "ASMAN SURVEILLANCE & LOGIC NE ACCESS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0140499062750898,
			"summary_team": 0.8727272727272728
		}]
	},
	"690567": {
		"nik": "690567",
		"nama": "WALUYO",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8285714285714288,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1163793103448274,
			"nilai_komparatif_unit": 1.1242027771405203,
			"summary_team": 0.9250000000000002
		}]
	},
	"690590": {
		"nik": "690590",
		"nama": "ANAK AGUNG RAKA WAHYUNI",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8205128205128207,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8999999999999998,
			"nilai_komparatif_unit": 0.9063071037333614,
			"summary_team": 0.7384615384615385
		}]
	},
	"690597": {
		"nik": "690597",
		"nama": "SEPTINING DYAH PURWANDARI",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666661,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0384615384615392,
			"nilai_komparatif_unit": 1.0457389658461873,
			"summary_team": 0.9000000000000001
		}]
	},
	"690611": {
		"nik": "690611",
		"nama": "MAHFUZAH",
		"band": "IV",
		"posisi": "ASMAN COMMUNICATION & SECRETARIATE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9615384615384615,
			"nilai_komparatif_unit": 0.9682768202279504,
			"summary_team": 0.7692307692307692
		}]
	},
	"690612": {
		"nik": "690612",
		"nama": "NI LUH PUTU BALIK SUWARINI",
		"band": "IV",
		"posisi": "ASMAN SALES OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8105263157894733,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9490509490509496,
			"nilai_komparatif_unit": 0.955701796588627,
			"summary_team": 0.7692307692307693
		}]
	},
	"690613": {
		"nik": "690613",
		"nama": "NAMROTUT TIS'AH",
		"band": "IV",
		"posisi": "ASMAN FINANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365351,
			"nilai_komparatif_unit": 0.9949168891755599,
			"summary_team": 0.8727272727272727
		}]
	},
	"700028": {
		"nik": "700028",
		"nama": "SJAMSIDAR MEILANI SITUMEANG",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8095238095238096,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8955882352941176,
			"nilai_komparatif_unit": 0.9018644218523157,
			"summary_team": 0.7250000000000001
		}]
	},
	"700122": {
		"nik": "700122",
		"nama": "AGUS GUNAWAN",
		"band": "IV",
		"posisi": "ASMAN OLO FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8493827160493849,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9418604651162767,
			"nilai_komparatif_unit": 0.9484609225116553,
			"summary_team": 0.8
		}]
	},
	"700225": {
		"nik": "700225",
		"nama": "HADI",
		"band": "IV",
		"posisi": "ASMAN DEBT MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8000000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999999,
			"nilai_komparatif_unit": 1.0070078930370685,
			"summary_team": 0.8
		}]
	},
	"700257": {
		"nik": "700257",
		"nama": "MARDIANI GUMALA",
		"band": "IV",
		"posisi": "ASMAN SALES OPERATION 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8523809523809525,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0107434464976364,
			"nilai_komparatif_unit": 1.0178266284586097,
			"summary_team": 0.8615384615384616
		}]
	},
	"700272": {
		"nik": "700272",
		"nama": "ERIN LISTIANA DEWI",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8380952380952381,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9247159090909093,
			"nilai_komparatif_unit": 0.931196219271494,
			"summary_team": 0.7750000000000001
		}]
	},
	"700274": {
		"nik": "700274",
		"nama": "JOKO SURONO",
		"band": "IV",
		"posisi": "OFF 1 CDC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882352,
			"nilai_komparatif_unit": 0.9477721346231232,
			"summary_team": 0.8
		}]
	},
	"700278": {
		"nik": "700278",
		"nama": "NURLAILA HAYATI",
		"band": "IV",
		"posisi": "ASMAN HR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7904761904761907,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0120481927710845,
			"nilai_komparatif_unit": 1.0191405182543827,
			"summary_team": 0.8000000000000003
		}]
	},
	"700280": {
		"nik": "700280",
		"nama": "FACHRUDIN",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7555555555555555,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0588235294117647,
			"nilai_komparatif_unit": 1.0662436514510139,
			"summary_team": 0.8
		}]
	},
	"700281": {
		"nik": "700281",
		"nama": "RAHMANOR FUADY",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8547008547008538,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1061818181818193,
			"nilai_komparatif_unit": 1.1139338220431874,
			"summary_team": 0.9454545454545454
		}]
	},
	"700284": {
		"nik": "700284",
		"nama": "RUSMINI",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8222222222222222,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9729729729729734,
			"nilai_komparatif_unit": 0.9797914634955265,
			"summary_team": 0.8000000000000003
		}]
	},
	"700286": {
		"nik": "700286",
		"nama": "MUHAMAD TARMIN",
		"band": "IV",
		"posisi": "ASMAN CCAN ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9166666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0115702479338842,
			"nilai_komparatif_unit": 1.0186592240308858,
			"summary_team": 0.9272727272727272
		}]
	},
	"700287": {
		"nik": "700287",
		"nama": "MASLIYANA",
		"band": "IV",
		"posisi": "OFF 1 PPN & NON TAX OBLIGATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.944055944055944,
			"nilai_komparatif_unit": 0.9506717871328968,
			"summary_team": 0.8181818181818182
		}]
	},
	"700292": {
		"nik": "700292",
		"nama": "TYAS TRI WIDYAWATI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999995,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.954545454545455,
			"nilai_komparatif_unit": 0.9612348069899296,
			"summary_team": 0.7636363636363636
		}]
	},
	"700304": {
		"nik": "700304",
		"nama": "LEONARD KITU RIWU",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8323232323232315,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0485436893203894,
			"nilai_komparatif_unit": 1.05589177133984,
			"summary_team": 0.8727272727272728
		}]
	},
	"700360": {
		"nik": "700360",
		"nama": "IMAN WAHYUDI",
		"band": "IV",
		"posisi": "ASMAN ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7833333333333332,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.090909090909091,
			"nilai_komparatif_unit": 1.0985540651313477,
			"summary_team": 0.8545454545454545
		}]
	},
	"700406": {
		"nik": "700406",
		"nama": "WAYAN SUKASANI",
		"band": "IV",
		"posisi": "ASMAN HR SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9090909090909091,
			"nilai_komparatif_unit": 0.9154617209427895,
			"summary_team": 0.8181818181818182
		}]
	},
	"700407": {
		"nik": "700407",
		"nama": "JULIASTRI",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8952380952380956,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9773936170212765,
			"nilai_komparatif_unit": 0.984243086944475,
			"summary_team": 0.8750000000000002
		}]
	},
	"700408": {
		"nik": "700408",
		"nama": "MARGARETHA MARIA ALACOQUE",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8323232323232315,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.895631067961166,
			"nilai_komparatif_unit": 0.9019075546861133,
			"summary_team": 0.7454545454545455
		}]
	},
	"700409": {
		"nik": "700409",
		"nama": "ERNI IDAYANTI",
		"band": "IV",
		"posisi": "OFF 1 HR SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300698,
			"nilai_komparatif_unit": 1.077428025417283,
			"summary_team": 0.9272727272727272
		}]
	},
	"700410": {
		"nik": "700410",
		"nama": "MUHAMMAD EFFENDI",
		"band": "IV",
		"posisi": "ASMAN CCAN WIFI FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8250000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0666666666666667,
			"nilai_komparatif_unit": 1.0741417525728731,
			"summary_team": 0.8800000000000002
		}]
	},
	"700412": {
		"nik": "700412",
		"nama": "I NYOMAN SUGIHARSANA",
		"band": "IV",
		"posisi": "JM CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8684210526315793,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.059393939393939,
			"nilai_komparatif_unit": 1.0668180588053304,
			"summary_team": 0.9200000000000002
		}]
	},
	"700413": {
		"nik": "700413",
		"nama": "ALBERTHA MARIETA",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.038461538461538,
			"nilai_komparatif_unit": 1.0457389658461862,
			"summary_team": 0.9000000000000001
		}]
	},
	"700415": {
		"nik": "700415",
		"nama": "I MADE SUARDHANA",
		"band": "IV",
		"posisi": "ASMAN ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8458333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9694581280788178,
			"nilai_komparatif_unit": 0.9762519869443108,
			"summary_team": 0.8200000000000001
		}]
	},
	"700419": {
		"nik": "700419",
		"nama": "NI MADE ASTITI PURBA SARI",
		"band": "IV",
		"posisi": "ASMAN BUSINESS QUALITY & DELIVERY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8416666666666658,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.972097209720973,
			"nilai_komparatif_unit": 0.9789095629883303,
			"summary_team": 0.8181818181818181
		}]
	},
	"700420": {
		"nik": "700420",
		"nama": "ERANITA",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.848148148148148,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8842794759825331,
			"nilai_komparatif_unit": 0.8904764119650936,
			"summary_team": 0.7500000000000001
		}]
	},
	"700421": {
		"nik": "700421",
		"nama": "NI KADEK SUDIANI",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8453333333333325,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0555690366415929,
			"nilai_komparatif_unit": 1.0629663515436185,
			"summary_team": 0.8923076923076922
		}]
	},
	"700436": {
		"nik": "700436",
		"nama": "KUSHARDIRIN LARASATI",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8539682539682542,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.007063197026022,
			"nilai_komparatif_unit": 1.0141205881923485,
			"summary_team": 0.86
		}]
	},
	"700576": {
		"nik": "700576",
		"nama": "KUSTIYO",
		"band": "IV",
		"posisi": "OFF 1 ASSET ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8912280701754386,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.925688976377953,
			"nilai_komparatif_unit": 0.9321761057100031,
			"summary_team": 0.8250000000000002
		}]
	},
	"700638": {
		"nik": "700638",
		"nama": "I GUSTI AYU PUTU PRIMARTINI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8416666666666658,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.972097209720973,
			"nilai_komparatif_unit": 0.9789095629883303,
			"summary_team": 0.8181818181818181
		}]
	},
	"700655": {
		"nik": "700655",
		"nama": "JUNCE KUSUMAWATI",
		"band": "IV",
		"posisi": "ASMAN COMMUNICATION, SECRETARIATE & SAS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666665,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1129032258064517,
			"nilai_komparatif_unit": 1.120702332573512,
			"summary_team": 0.92
		}]
	},
	"710004": {
		"nik": "710004",
		"nama": "DEWI AISYAH",
		"band": "IV",
		"posisi": "ASMAN SALES SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0542056074766353,
			"nilai_komparatif_unit": 1.0615933676129095,
			"summary_team": 0.9400000000000002
		}]
	},
	"710132": {
		"nik": "710132",
		"nama": "AGUNG RIYANTO , S.Si.",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK & LEGAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.03030303030303,
			"nilai_komparatif_unit": 1.0375232837351613,
			"summary_team": 0.9272727272727272
		}]
	},
	"710135": {
		"nik": "710135",
		"nama": "MAHDALENA",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0117647058823525,
			"nilai_komparatif_unit": 1.018855044719857,
			"summary_team": 0.8599999999999998
		}]
	},
	"710136": {
		"nik": "710136",
		"nama": "FATIMAH",
		"band": "IV",
		"posisi": "ASMAN CRM & LEVERAGING CUSTOMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7611111111111115,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0510948905109485,
			"nilai_komparatif_unit": 1.0584608510754585,
			"summary_team": 0.8
		}]
	},
	"710138": {
		"nik": "710138",
		"nama": "RUSNAWATI",
		"band": "IV",
		"posisi": "ASMAN GES SALES ENGINEER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8722222222222226,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9380428488708741,
			"nilai_komparatif_unit": 0.9446165528199482,
			"summary_team": 0.8181818181818182
		}]
	},
	"710139": {
		"nik": "710139",
		"nama": "RINA NOOR MAULIDA",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9185185185185188,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.06891495601173,
			"nilai_komparatif_unit": 1.076405797689183,
			"summary_team": 0.9818181818181818
		}]
	},
	"710141": {
		"nik": "710141",
		"nama": "DEWI ANA PARWATI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333328,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9375000000000004,
			"nilai_komparatif_unit": 0.9440698997222522,
			"summary_team": 0.7999999999999999
		}]
	},
	"710144": {
		"nik": "710144",
		"nama": "LINDA IRMA HANDAYANI",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7904761904761906,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.927710843373494,
			"nilai_komparatif_unit": 0.934212141733184,
			"summary_team": 0.7333333333333334
		}]
	},
	"710146": {
		"nik": "710146",
		"nama": "RUSIANA",
		"band": "IV",
		"posisi": "OFF 1 BUDGET OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7583333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.959040959040959,
			"nilai_komparatif_unit": 0.9657618155000857,
			"summary_team": 0.7272727272727273
		}]
	},
	"710148": {
		"nik": "710148",
		"nama": "NILA PUSPANTI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8643678160919535,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0096711798839466,
			"nilai_komparatif_unit": 1.0167468475151842,
			"summary_team": 0.8727272727272728
		}]
	},
	"710152": {
		"nik": "710152",
		"nama": "LISMAYANTI",
		"band": "IV",
		"posisi": "OFF 1 HR ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7866666666666663,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0895883777239712,
			"nilai_komparatif_unit": 1.0972240965294937,
			"summary_team": 0.857142857142857
		}]
	},
	"710160": {
		"nik": "710160",
		"nama": "THONY CHORNELIS ROBINSON",
		"band": "IV",
		"posisi": "ASMAN CCAN FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8999999999999991,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1111111111111125,
			"nilai_komparatif_unit": 1.1188976589300774,
			"summary_team": 1.0000000000000002
		}]
	},
	"710205": {
		"nik": "710205",
		"nama": "L Nyoman M Sriwahyuni",
		"band": "IV",
		"posisi": "ASMAN SALES OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9400000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0147299509001635,
			"nilai_komparatif_unit": 1.0218410698575817,
			"summary_team": 0.9538461538461538
		}]
	},
	"710206": {
		"nik": "710206",
		"nama": "NI KETUT JUNI KARYAWATI",
		"band": "IV",
		"posisi": "OFF 1 HR DEVELOPMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8999999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.98989898989899,
			"nilai_komparatif_unit": 0.9968360961377042,
			"summary_team": 0.8909090909090909
		}]
	},
	"710208": {
		"nik": "710208",
		"nama": "RETNOSARI RAHAYU",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.016883116883117,
			"nilai_komparatif_unit": 1.0240093249974347,
			"summary_team": 0.8700000000000003
		}]
	},
	"710209": {
		"nik": "710209",
		"nama": "LUH PUTU SUSILAWATI",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200685,
			"nilai_komparatif_unit": 1.0570991947490322,
			"summary_team": 0.9272727272727272
		}]
	},
	"710211": {
		"nik": "710211",
		"nama": "AMBAR SADWI S.P",
		"band": "IV",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8294117647058812,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0303030303030318,
			"nilai_komparatif_unit": 1.0375232837351631,
			"summary_team": 0.8545454545454546
		}]
	},
	"710212": {
		"nik": "710212",
		"nama": "TETTY HERLINA PURBA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8416666666666658,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9936993699369947,
			"nilai_komparatif_unit": 1.0006631088325155,
			"summary_team": 0.8363636363636363
		}]
	},
	"710215": {
		"nik": "710215",
		"nama": "SOEDJIADI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8749999999999994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0597402597402603,
			"nilai_komparatif_unit": 1.0671668061275952,
			"summary_team": 0.9272727272727272
		}]
	},
	"710232": {
		"nik": "710232",
		"nama": "IIN WIYARTINI",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8205128205128207,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8863636363636361,
			"nilai_komparatif_unit": 0.8925751779192196,
			"summary_team": 0.7272727272727273
		}]
	},
	"710247": {
		"nik": "710247",
		"nama": "AGUSTINA MANALIP",
		"band": "IV",
		"posisi": "ASMAN GES TERITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999995,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9318181818181824,
			"nilai_komparatif_unit": 0.93834826396636,
			"summary_team": 0.7454545454545455
		}]
	},
	"710457": {
		"nik": "710457",
		"nama": "AGUS SUHARJA,ST",
		"band": "IV",
		"posisi": "ASMAN CDC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144382,
			"nilai_komparatif_unit": 1.0339332377706796,
			"summary_team": 0.8727272727272727
		}]
	},
	"710490": {
		"nik": "710490",
		"nama": "PUJI ASTUTI",
		"band": "IV",
		"posisi": "OFF 1 VERIFICATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.925925925925926,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0603636363636362,
			"nilai_komparatif_unit": 1.0677945513076696,
			"summary_team": 0.9818181818181818
		}]
	},
	"710495": {
		"nik": "710495",
		"nama": "ELISABETH ANNA KANA HEBI",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8105263157894733,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9490509490509496,
			"nilai_komparatif_unit": 0.955701796588627,
			"summary_team": 0.7692307692307693
		}]
	},
	"720022": {
		"nik": "720022",
		"nama": "SUHARTYANI",
		"band": "IV",
		"posisi": "ASMAN BUSINESS OBL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930064,
			"nilai_komparatif_unit": 1.0140499062750894,
			"summary_team": 0.8727272727272727
		}]
	},
	"720028": {
		"nik": "720028",
		"nama": "TRIMARDIJATI",
		"band": "IV",
		"posisi": "OFF 1 PKBL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9056603773584903,
			"nilai_komparatif_unit": 0.9120071484109297,
			"summary_team": 0.8
		}]
	},
	"720029": {
		"nik": "720029",
		"nama": "FLORIDA KOROH",
		"band": "IV",
		"posisi": "ASMAN BS TERITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8323232323232315,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0351008215085897,
			"nilai_komparatif_unit": 1.0423546973483038,
			"summary_team": 0.8615384615384616
		}]
	},
	"720031": {
		"nik": "720031",
		"nama": "LAELY ISTIARINI NIRMALA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8791666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9306333476949589,
			"nilai_komparatif_unit": 0.9371551266523341,
			"summary_team": 0.8181818181818182
		}]
	},
	"720041": {
		"nik": "720041",
		"nama": "DESAK PUTU MULIANI",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8358974358974361,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9371165644171779,
			"nilai_komparatif_unit": 0.9436837770638786,
			"summary_team": 0.7833333333333335
		}]
	},
	"720044": {
		"nik": "720044",
		"nama": "YULI PURWANTI",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0070078930370687,
			"summary_team": 0.8
		}]
	},
	"720053": {
		"nik": "720053",
		"nama": "GUSTI ELVERARIANA DWI SARI",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK & LEGAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.986456711546516,
			"summary_team": 0.8
		}]
	},
	"720153": {
		"nik": "720153",
		"nama": "RIDWAN NULCHOLIQ",
		"band": "IV",
		"posisi": "OFF 1 PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8460784313725501,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.052986411039712,
			"nilai_komparatif_unit": 1.060365627177765,
			"summary_team": 0.8909090909090909
		}]
	},
	"730004": {
		"nik": "730004",
		"nama": "MUTMAINNAH",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.76,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0526315789473684,
			"nilai_komparatif_unit": 1.060008308460072,
			"summary_team": 0.8
		}]
	},
	"730005": {
		"nik": "730005",
		"nama": "IIN SUSANTY",
		"band": "IV",
		"posisi": "ASMAN GES QUALITY & PROJECT MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999995,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000007,
			"nilai_komparatif_unit": 1.0070078930370692,
			"summary_team": 0.8
		}]
	},
	"730020": {
		"nik": "730020",
		"nama": "DAHLIA SAPHIRA",
		"band": "IV",
		"posisi": "ASMAN SALES OPERATION 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8488888888888886,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9967780910189291,
			"nilai_komparatif_unit": 1.003763405262483,
			"summary_team": 0.8461538461538461
		}]
	},
	"730029": {
		"nik": "730029",
		"nama": "NOERLAELA GAMGULU",
		"band": "IV",
		"posisi": "OFF 1 CASH BANK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0070078930370687,
			"summary_team": 0.8
		}]
	},
	"730468": {
		"nik": "730468",
		"nama": "RIRIN UMI NAZILAH",
		"band": "IV",
		"posisi": "ASMAN ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0140499062750898,
			"summary_team": 0.8727272727272728
		}]
	},
	"740012": {
		"nik": "740012",
		"nama": "YENI ROESTIKASIANA",
		"band": "IV",
		"posisi": "OFF 1 CASH BANK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8750000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8935064935064934,
			"nilai_komparatif_unit": 0.8997680914409131,
			"summary_team": 0.7818181818181817
		}]
	},
	"740018": {
		"nik": "740018",
		"nama": "RETNA DWI ERNAWATI",
		"band": "IV",
		"posisi": "OFF 1 HR DEVELOPMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8285714285714286,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9956896551724139,
			"nilai_komparatif_unit": 1.0026673417739778,
			"summary_team": 0.8250000000000002
		}]
	},
	"740020": {
		"nik": "740020",
		"nama": "YERMIDA UKY",
		"band": "IV",
		"posisi": "JM CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8606965174129362,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0456647398843921,
			"nilai_komparatif_unit": 1.052992646534136,
			"summary_team": 0.9000000000000001
		}]
	},
	"740027": {
		"nik": "740027",
		"nama": "AGUS ANDERIAS TUNLIU",
		"band": "IV",
		"posisi": "ASMAN ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8791666666666665,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9327014218009481,
			"nilai_komparatif_unit": 0.9392376936004508,
			"summary_team": 0.8200000000000001
		}]
	},
	"740029": {
		"nik": "740029",
		"nama": "YAKOBUS WAYENI",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777781,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9113924050632909,
			"nilai_komparatif_unit": 0.9177793455527711,
			"summary_team": 0.8
		}]
	},
	"740265": {
		"nik": "740265",
		"nama": "RUMIYATI",
		"band": "IV",
		"posisi": "ASMAN PARTNERSHIP & AGENCY 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8488888888888886,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9605316149818771,
			"nilai_komparatif_unit": 0.9672629177983928,
			"summary_team": 0.8153846153846154
		}]
	},
	"750004": {
		"nik": "750004",
		"nama": "LUISA DO ROSARIO BARRETO T.",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8416666666666658,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9072907290729083,
			"nilai_komparatif_unit": 0.9136489254557751,
			"summary_team": 0.7636363636363637
		}]
	},
	"750075": {
		"nik": "750075",
		"nama": "EVI MASTUTI",
		"band": "IV",
		"posisi": "ASMAN FINANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9910714285714287,
			"nilai_komparatif_unit": 0.998016751134952,
			"summary_team": 0.9250000000000002
		}]
	},
	"760006": {
		"nik": "760006",
		"nama": "YATI SARTIKA",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8596491228070172,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0081632653061228,
			"nilai_komparatif_unit": 1.0152283656332899,
			"summary_team": 0.8666666666666667
		}]
	},
	"760010": {
		"nik": "760010",
		"nama": "NURSIA DANA",
		"band": "IV",
		"posisi": "ASMAN CCAN WIFI FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8916666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9644859813084112,
			"nilai_komparatif_unit": 0.9712449959011726,
			"summary_team": 0.8600000000000001
		}]
	},
	"760015": {
		"nik": "760015",
		"nama": "BEATRIKS LENY WELING",
		"band": "IV",
		"posisi": "JM CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9090909090909093,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9075,
			"nilai_komparatif_unit": 0.9138596629311396,
			"summary_team": 0.8250000000000002
		}]
	},
	"760023": {
		"nik": "760023",
		"nama": "ASRIYANI",
		"band": "IV",
		"posisi": "ASMAN OM SWITCHING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8600000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.930232558139535,
			"nilai_komparatif_unit": 0.9367515284065755,
			"summary_team": 0.8000000000000003
		}]
	},
	"770005": {
		"nik": "770005",
		"nama": "SELI RAMBE",
		"band": "IV",
		"posisi": "ASMAN BUSINESS TERITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769228,
			"nilai_komparatif_unit": 0.9295457474188322,
			"summary_team": 0.8
		}]
	},
	"770014": {
		"nik": "770014",
		"nama": "FILOMENA BAREK ASAN",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9908256880733944,
			"nilai_komparatif_unit": 0.9977692885137927,
			"summary_team": 0.9000000000000001
		}]
	},
	"770015": {
		"nik": "770015",
		"nama": "AGUSTINA MEME GORAN",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8323232323232315,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9611650485436903,
			"nilai_komparatif_unit": 0.9679007903948533,
			"summary_team": 0.8
		}]
	},
	"770023": {
		"nik": "770023",
		"nama": "SOPHIA SAVSAV UBUN",
		"band": "IV",
		"posisi": "ASMAN ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8999999999999991,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9777777777777789,
			"nilai_komparatif_unit": 0.9846299398584681,
			"summary_team": 0.8800000000000001
		}]
	},
	"780007": {
		"nik": "780007",
		"nama": "DWI WAHYUNI DESIANTHIE",
		"band": "IV",
		"posisi": "ASMAN COMMUNICATION & SECRETARIATE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0070078930370687,
			"summary_team": 0.8
		}]
	},
	"780008": {
		"nik": "780008",
		"nama": "JOHANA WELHELMINTJE",
		"band": "IV",
		"posisi": "ASMAN OM IP NETWORK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8600000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.930232558139535,
			"nilai_komparatif_unit": 0.9367515284065755,
			"summary_team": 0.8000000000000003
		}]
	},
	"780016": {
		"nik": "780016",
		"nama": "DAHLIA NELIWATI LENDES",
		"band": "IV",
		"posisi": "ASMAN CCAN WIFI FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8999999999999991,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9555555555555566,
			"nilai_komparatif_unit": 0.9622519866798666,
			"summary_team": 0.8600000000000001
		}]
	},
	"780018": {
		"nik": "780018",
		"nama": "AIDAH",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0128440366972475,
			"nilai_komparatif_unit": 1.0199419393696545,
			"summary_team": 0.9200000000000002
		}]
	},
	"780055": {
		"nik": "780055",
		"nama": "REYNI OKTAVIA NURHALIMAH",
		"band": "IV",
		"posisi": "OFF 1 CASH BANK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8754098360655743,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9761661559414363,
			"nilai_komparatif_unit": 0.9830070239486802,
			"summary_team": 0.8545454545454546
		}]
	},
	"790017": {
		"nik": "790017",
		"nama": "SRI UTAMI",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8285714285714286,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9655172413793104,
			"nilai_komparatif_unit": 0.972283482932342,
			"summary_team": 0.8
		}]
	},
	"790026": {
		"nik": "790026",
		"nama": "IKA RINY WIDHIASTUTI, S.E",
		"band": "IV",
		"posisi": "ASMAN ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9238095238095239,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8930412371134022,
			"nilai_komparatif_unit": 0.8992995745807842,
			"summary_team": 0.8250000000000002
		}]
	},
	"800038": {
		"nik": "800038",
		"nama": "SITI NUR AISYAH",
		"band": "IV",
		"posisi": "OFF 1 BUDGET OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006993006993007,
			"nilai_komparatif_unit": 1.01404990627509,
			"summary_team": 0.8727272727272728
		}]
	},
	"800074": {
		"nik": "800074",
		"nama": "ERVINNA SMERU",
		"band": "IV",
		"posisi": "OFF 1 CASH BANK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.953221535745807,
			"nilai_komparatif_unit": 0.9599016103089438,
			"summary_team": 0.8181818181818181
		}]
	},
	"810101": {
		"nik": "810101",
		"nama": "I GUSTI NGURAH JUNAEDI DUWI PUTRA",
		"band": "IV",
		"posisi": "ASMAN CAPEX MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888888,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0125000000000002,
			"nilai_komparatif_unit": 1.019595491700032,
			"summary_team": 0.9000000000000001
		}]
	},
	"820002": {
		"nik": "820002",
		"nama": "YAYUK FITRIANA",
		"band": "IV",
		"posisi": "ASMAN ADVALJAR OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0070078930370687,
			"summary_team": 0.8
		}]
	},
	"820027": {
		"nik": "820027",
		"nama": "WAODE LAILA WAHYUNI",
		"band": "IV",
		"posisi": "ASMAN ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888892,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9613636363636361,
			"nilai_komparatif_unit": 0.9681007698969997,
			"summary_team": 0.8545454545454546
		}]
	},
	"820033": {
		"nik": "820033",
		"nama": "LISA PARMAWATI",
		"band": "IV",
		"posisi": "ASMAN GES TERITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.869135802469135,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9832128099173564,
			"nilai_komparatif_unit": 0.9901030601219328,
			"summary_team": 0.8545454545454546
		}]
	},
	"820060": {
		"nik": "820060",
		"nama": "YENI RIWAYANTI",
		"band": "IV",
		"posisi": "ASMAN ADVALJAR OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9500000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0105263157894737,
			"nilai_komparatif_unit": 1.0176079761216692,
			"summary_team": 0.9600000000000002
		}]
	},
	"830001": {
		"nik": "830001",
		"nama": "ARIF KURNIAWAN",
		"band": "IV",
		"posisi": "ASMAN OM IP NETWORK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.0339332377706798,
			"summary_team": 0.8727272727272727
		}]
	},
	"830005": {
		"nik": "830005",
		"nama": "DITA PAPILAYA",
		"band": "IV",
		"posisi": "ASMAN BUSINESS SALES ENGINEER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.869135802469135,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9492187500000011,
			"nilai_komparatif_unit": 0.955870773468781,
			"summary_team": 0.8250000000000002
		}]
	},
	"830009": {
		"nik": "830009",
		"nama": "HARIS FITRIADI, S.Ikom",
		"band": "IV",
		"posisi": "ASMAN CCAN FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8380952380952381,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0196280991735538,
			"nilai_komparatif_unit": 1.0267735438301515,
			"summary_team": 0.8545454545454546
		}]
	},
	"830011": {
		"nik": "830011",
		"nama": "DWI VITA ANGGRAINI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777781,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9735327963176061,
			"nilai_komparatif_unit": 0.9803552100222781,
			"summary_team": 0.8545454545454546
		}]
	},
	"830014": {
		"nik": "830014",
		"nama": "SITI DAHLIA",
		"band": "IV",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8358208955223883,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0224025974025972,
			"nilai_komparatif_unit": 1.0295674854460157,
			"summary_team": 0.8545454545454546
		}]
	},
	"830039": {
		"nik": "830039",
		"nama": "LIA MUSPIKAWATI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.869135802469135,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0878099173553728,
			"nilai_komparatif_unit": 1.0954331729008615,
			"summary_team": 0.9454545454545454
		}]
	},
	"830088": {
		"nik": "830088",
		"nama": "RISKI AMALIAH",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9380952380952381,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0495900039047246,
			"nilai_komparatif_unit": 1.0569454183848652,
			"summary_team": 0.9846153846153844
		}]
	},
	"830104": {
		"nik": "830104",
		"nama": "AGUSTINI FITRI WAHYUNI LUBIS",
		"band": "IV",
		"posisi": "KAKANDATEL MUARATEWEH",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8358208955223883,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9571428571428569,
			"nilai_komparatif_unit": 0.9638504119069081,
			"summary_team": 0.7999999999999999
		}]
	},
	"830121": {
		"nik": "830121",
		"nama": "NOVYANNA WISUDAMAYANTY SEMBODHO",
		"band": "IV",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8285714285714288,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0397877984084878,
			"nilai_komparatif_unit": 1.0470745200809835,
			"summary_team": 0.8615384615384616
		}]
	},
	"840014": {
		"nik": "840014",
		"nama": "AULIA AZIZAH",
		"band": "IV",
		"posisi": "ASMAN ADVALJAR OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999999,
			"nilai_komparatif_unit": 0.9667275773155857,
			"summary_team": 0.8
		}]
	},
	"840028": {
		"nik": "840028",
		"nama": "REMY SYAHDEINI",
		"band": "IV",
		"posisi": "KAKANDATEL SAMPIT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8358208955223883,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0369047619047616,
			"nilai_komparatif_unit": 1.0441712795658171,
			"summary_team": 0.8666666666666666
		}]
	},
	"840052": {
		"nik": "840052",
		"nama": "PUTU SUSI YUNITA DEWI",
		"band": "IV",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8416666666666658,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0369036903690378,
			"nilai_komparatif_unit": 1.0441702005208857,
			"summary_team": 0.8727272727272727
		}]
	},
	"840061": {
		"nik": "840061",
		"nama": "ANNAS KHARISMA",
		"band": "IV",
		"posisi": "ASMAN CRM & LEVERAGING CUSTOMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8400000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.972222222222222,
			"nilai_komparatif_unit": 0.9790354515638164,
			"summary_team": 0.8166666666666668
		}]
	},
	"840084": {
		"nik": "840084",
		"nama": "ENDAH PUSPITASARI",
		"band": "IV",
		"posisi": "ASMAN PARTNERSHIP & AGENCY 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8564102564102566,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0419161676646704,
			"nilai_komparatif_unit": 1.0492178047212568,
			"summary_team": 0.8923076923076922
		}]
	},
	"840118": {
		"nik": "840118",
		"nama": "NI LUH PUTU AGUSTINI SWASTI",
		"band": "IV",
		"posisi": "ASMAN CRM & LEVERAGING CUSTOMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8205128205128207,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0414772727272725,
			"nilai_komparatif_unit": 1.048775834055083,
			"summary_team": 0.8545454545454546
		}]
	},
	"850004": {
		"nik": "850004",
		"nama": "VITA CAHYA ARIZONA",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9185185185185188,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9714640198511161,
			"nilai_komparatif_unit": 0.9782719357915933,
			"summary_team": 0.8923076923076921
		}]
	},
	"850015": {
		"nik": "850015",
		"nama": "MAHYUDI NOOR",
		"band": "IV",
		"posisi": "ASMAN HOME SERVICE 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444446,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9631578947368419,
			"nilai_komparatif_unit": 0.9699076022409657,
			"summary_team": 0.8133333333333332
		}]
	},
	"850017": {
		"nik": "850017",
		"nama": "ARIEF RACHMANI",
		"band": "IV",
		"posisi": "ASMAN OM TRANSPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8133333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0506706408345754,
			"nilai_komparatif_unit": 1.0580336283027323,
			"summary_team": 0.8545454545454546
		}]
	},
	"850020": {
		"nik": "850020",
		"nama": "HERU PRAYOGO MEI NOFRIANS",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0123929619837908,
			"summary_team": 0.8545454545454546
		}]
	},
	"850028": {
		"nik": "850028",
		"nama": "MAJIDI",
		"band": "IV",
		"posisi": "ASMAN NETWORK PERF, QM & ADMIN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9585798816568045,
			"nilai_komparatif_unit": 0.9652975069349412,
			"summary_team": 0.8307692307692307
		}]
	},
	"850038": {
		"nik": "850038",
		"nama": "MUHAMMAD NOOR MUSHODDIQ",
		"band": "IV",
		"posisi": "ASMAN ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.99558693733451,
			"nilai_komparatif_unit": 1.0025639041004528,
			"summary_team": 0.8545454545454546
		}]
	},
	"850046": {
		"nik": "850046",
		"nama": "MOH RIZAL BIN MOH. FERRY Y.P. DARA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8749999999999994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.997402597402598,
			"nilai_komparatif_unit": 1.0043922881200897,
			"summary_team": 0.8727272727272727
		}]
	},
	"850057": {
		"nik": "850057",
		"nama": "YESRU MARDANIATI",
		"band": "IV",
		"posisi": "SENIOR AM GOVERNMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8787878787878791,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0241379310344825,
			"nilai_komparatif_unit": 1.0313149801103767,
			"summary_team": 0.9000000000000001
		}]
	},
	"850060": {
		"nik": "850060",
		"nama": "FALIHUDDINUL FALAH YUSMANA PUTRA",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.080317740511915,
			"nilai_komparatif_unit": 1.0878884916834701,
			"summary_team": 0.9272727272727274
		}]
	},
	"850062": {
		"nik": "850062",
		"nama": "BOBY SAPUTRA",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8321285140562269,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9613899613899591,
			"nilai_komparatif_unit": 0.9681272794062914,
			"summary_team": 0.8
		}]
	},
	"850163": {
		"nik": "850163",
		"nama": "I GUSTI AYU AGUNG NITYA P.N",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8749999999999994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649356,
			"nilai_komparatif_unit": 0.9416177701125841,
			"summary_team": 0.8181818181818181
		}]
	},
	"860002": {
		"nik": "860002",
		"nama": "ANDI RAKHMAN",
		"band": "IV",
		"posisi": "ASMAN OM RADIO & MUX",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8716049382716041,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9178470254957516,
			"nilai_komparatif_unit": 0.9242791992748173,
			"summary_team": 0.8
		}]
	},
	"860005": {
		"nik": "860005",
		"nama": "LIU VENDY",
		"band": "IV",
		"posisi": "OFF 1 ACCESS NEW FTTH PROJECT SUPERVISIO",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8222222222222222,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0033783783783787,
			"nilai_komparatif_unit": 1.0104099467297616,
			"summary_team": 0.8250000000000002
		}]
	},
	"860010": {
		"nik": "860010",
		"nama": "AULIA NUGRAHA",
		"band": "IV",
		"posisi": "ASMAN TGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.987993138936535,
			"nilai_komparatif_unit": 0.9949168891755598,
			"summary_team": 0.8727272727272727
		}]
	},
	"860011": {
		"nik": "860011",
		"nama": "ADE RAHMANI",
		"band": "IV",
		"posisi": "ASMAN HOME SERVICE 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8848484848484849,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9214963119072707,
			"nilai_komparatif_unit": 0.92795405949517,
			"summary_team": 0.8153846153846153
		}]
	},
	"860012": {
		"nik": "860012",
		"nama": "FERY KRISMANTO",
		"band": "IV",
		"posisi": "ASMAN ADVALJAR OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9499999999999998,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9952153110047849,
			"nilai_komparatif_unit": 1.0021896734531592,
			"summary_team": 0.9454545454545454
		}]
	},
	"860016": {
		"nik": "860016",
		"nama": "ARI FITRI HANDOKO",
		"band": "IV",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444446,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9812030075187967,
			"nilai_komparatif_unit": 0.9880791732431384,
			"summary_team": 0.8285714285714285
		}]
	},
	"860028": {
		"nik": "860028",
		"nama": "MUHAMMAD YUDHA PRATAMA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333328,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9588068181818188,
			"nilai_komparatif_unit": 0.965526033806849,
			"summary_team": 0.8181818181818182
		}]
	},
	"860034": {
		"nik": "860034",
		"nama": "ILHAM HIMAWAN",
		"band": "IV",
		"posisi": "MGR HR PLANNING & DEVELOPMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7866666666666663,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9624697336561746,
			"nilai_komparatif_unit": 0.969214618601053,
			"summary_team": 0.757142857142857
		}]
	},
	"860035": {
		"nik": "860035",
		"nama": "MUHAMMAD CHAIRIL",
		"band": "IV",
		"posisi": "ASMAN DEPLOYMENT & CAPEX MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.969230769230769,
			"nilai_komparatif_unit": 0.9760230347897739,
			"summary_team": 0.8400000000000001
		}]
	},
	"860037": {
		"nik": "860037",
		"nama": "IMAN PUTRA",
		"band": "IV",
		"posisi": "OFF 1 PROJECT DATA MGT & ADMINISTRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9674099485420241,
			"nilai_komparatif_unit": 0.9741894539844026,
			"summary_team": 0.8545454545454546
		}]
	},
	"860038": {
		"nik": "860038",
		"nama": "AFDAL SAPUTRA",
		"band": "IV",
		"posisi": "ASMAN OM IP NETWORK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8632478632478621,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9730693069306945,
			"nilai_komparatif_unit": 0.9798884725513192,
			"summary_team": 0.8400000000000001
		}]
	},
	"860044": {
		"nik": "860044",
		"nama": "JEFFRIANTO PABALIK",
		"band": "IV",
		"posisi": "ASMAN FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8999999999999991,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.000000000000001,
			"nilai_komparatif_unit": 1.0070078930370696,
			"summary_team": 0.9000000000000001
		}]
	},
	"860045": {
		"nik": "860045",
		"nama": "NURDIN SUWIRO",
		"band": "IV",
		"posisi": "ASMAN CCAN ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9705882352941174,
			"nilai_komparatif_unit": 0.9773900138300957,
			"summary_team": 0.825
		}]
	},
	"860050": {
		"nik": "860050",
		"nama": "RAHMI RELAWATI",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7333333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1363636363636358,
			"nilai_komparatif_unit": 1.1443271511784865,
			"summary_team": 0.8333333333333333
		}]
	},
	"860051": {
		"nik": "860051",
		"nama": "ONHI YULIARTI SEMARA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8722222222222226,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.000579038795599,
			"nilai_komparatif_unit": 1.0075909896746114,
			"summary_team": 0.8727272727272728
		}]
	},
	"860084": {
		"nik": "860084",
		"nama": "DEWI RIYANTI",
		"band": "IV",
		"posisi": "ASMAN CCAN FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8453333333333325,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9463722397476351,
			"nilai_komparatif_unit": 0.9530043151770374,
			"summary_team": 0.8
		}]
	},
	"860139": {
		"nik": "860139",
		"nama": "FANI RIZKIANI",
		"band": "IV",
		"posisi": "ASMAN CRM & LEVERAGING CUSTOMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8380952380952383,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9545454545454545,
			"nilai_komparatif_unit": 0.961234806989929,
			"summary_team": 0.8000000000000002
		}]
	},
	"870022": {
		"nik": "870022",
		"nama": "HAVEA PERTIWI",
		"band": "IV",
		"posisi": "ASMAN CCAN ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8549019607843138,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.953775582215949,
			"nilai_komparatif_unit": 0.9604595394774862,
			"summary_team": 0.8153846153846154
		}]
	},
	"870069": {
		"nik": "870069",
		"nama": "DENI THEO NUCIFERA",
		"band": "IV",
		"posisi": "SENIOR AM ENTERPRISE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999996,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9772727272727276,
			"nilai_komparatif_unit": 0.9841213500134991,
			"summary_team": 0.7818181818181817
		}]
	},
	"876937": {
		"nik": "876937",
		"nama": "ASTRI IRDIANA RYANTI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER ENTERPRISE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8395833333333326,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7987155159830688,
			"nilai_komparatif_unit": 0.8043128288861251,
			"summary_team": 0.6705882352941176
		}]
	},
	"890073": {
		"nik": "890073",
		"nama": "LANANG PRAYOGO",
		"band": "IV",
		"posisi": "OFF 1 IP & SERVICE NODE OPERATION SUPPOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346936,
			"nilai_komparatif_unit": 0.9864567115465158,
			"summary_team": 0.8
		}]
	},
	"890081": {
		"nik": "890081",
		"nama": "RATRI ANGGARDANI PRAYITNO",
		"band": "IV",
		"posisi": "ASMAN CRM & LEVERAGING CUSTOMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8388888888888892,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0186634557495482,
			"nilai_komparatif_unit": 1.0258021402882116,
			"summary_team": 0.8545454545454546
		}]
	},
	"890082": {
		"nik": "890082",
		"nama": "NUR FADILLAH A. PAREWE",
		"band": "IV",
		"posisi": "OFF 1 SMALL & MICRO CUSTOMER MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144382,
			"nilai_komparatif_unit": 1.0339332377706796,
			"summary_team": 0.8727272727272727
		}]
	},
	"890083": {
		"nik": "890083",
		"nama": "MUHAMMAD TITO THORIQ TAUFIK",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333332,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9375000000000002,
			"nilai_komparatif_unit": 0.944069899722252,
			"summary_team": 0.8
		}]
	},
	"890085": {
		"nik": "890085",
		"nama": "HARISUDDIN HAKIM",
		"band": "IV",
		"posisi": "ASMAN ADVALJAR OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7777777777777778,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857144,
			"nilai_komparatif_unit": 0.9710433254286019,
			"summary_team": 0.7500000000000001
		}]
	},
	"890099": {
		"nik": "890099",
		"nama": "NOVIEKA DISTIASARI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777781,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9942462600690446,
			"nilai_komparatif_unit": 1.001213831512114,
			"summary_team": 0.8727272727272728
		}]
	},
	"900007": {
		"nik": "900007",
		"nama": "ANDINA GUSTRIA CAESARY",
		"band": "IV",
		"posisi": "OFF 1 REVENUE ASSURANCE & FRAUD MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8539682539682542,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0088647412067484,
			"nilai_komparatif_unit": 1.015934757401995,
			"summary_team": 0.8615384615384616
		}]
	},
	"900017": {
		"nik": "900017",
		"nama": "I GUSTI NGURAH BARA. A",
		"band": "IV",
		"posisi": "ASMAN ACCESS OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8105263157894733,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0059940059940065,
			"nilai_komparatif_unit": 1.0130439043839445,
			"summary_team": 0.8153846153846154
		}]
	},
	"900021": {
		"nik": "900021",
		"nama": "NI KOMANG RIANG WIDNYANI",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8416666666666658,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0369036903690378,
			"nilai_komparatif_unit": 1.0441702005208857,
			"summary_team": 0.8727272727272727
		}]
	},
	"900040": {
		"nik": "900040",
		"nama": "MUHAMMAD ASWAN",
		"band": "IV",
		"posisi": "ASMAN DEPLOYMENT & CAPEX MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9142857142857144,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.06640625,
			"nilai_komparatif_unit": 1.0738795109340613,
			"summary_team": 0.9750000000000001
		}]
	},
	"900045": {
		"nik": "900045",
		"nama": "REISA ADILA PURNOMO",
		"band": "IV",
		"posisi": "OFF 1 BILLING SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.944055944055944,
			"nilai_komparatif_unit": 0.9506717871328968,
			"summary_team": 0.8181818181818182
		}]
	},
	"900048": {
		"nik": "900048",
		"nama": "HARYANDA EKA PUTRA",
		"band": "IV",
		"posisi": "ASMAN QUALITY ENHANCEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777781,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9113924050632909,
			"nilai_komparatif_unit": 0.9177793455527711,
			"summary_team": 0.8
		}]
	},
	"900061": {
		"nik": "900061",
		"nama": "KHOIRUL FAHMI",
		"band": "IV",
		"posisi": "ASMAN GES SALES ENGINEER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999995,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272734,
			"nilai_komparatif_unit": 1.029894436060639,
			"summary_team": 0.8181818181818181
		}]
	},
	"900071": {
		"nik": "900071",
		"nama": "FIRMAN SYAH",
		"band": "IV",
		"posisi": "OFF 1 COMMAND CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0307692307692304,
			"nilai_komparatif_unit": 1.0379927512843625,
			"summary_team": 0.8933333333333333
		}]
	},
	"900089": {
		"nik": "900089",
		"nama": "RACHDI ARIMAN",
		"band": "IV",
		"posisi": "OFF 1 SECRETARY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888892,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0730769230769226,
			"nilai_komparatif_unit": 1.0805969313743922,
			"summary_team": 0.9538461538461538
		}]
	},
	"900091": {
		"nik": "900091",
		"nama": "MUHAMMAD FAULANA HAFIZD NUR",
		"band": "IV",
		"posisi": "ASMAN SALES OPERATION 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8410256410256411,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0376940133037693,
			"nilai_komparatif_unit": 1.0449660619542085,
			"summary_team": 0.8727272727272728
		}]
	},
	"910008": {
		"nik": "910008",
		"nama": "NI LUH PUTU EKA ARYANTINI",
		"band": "IV",
		"posisi": "ASMAN BUSINESS QUALITY & DELIVERY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8361904761904755,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0119151918696345,
			"nilai_komparatif_unit": 1.0190065852968415,
			"summary_team": 0.8461538461538461
		}]
	},
	"910017": {
		"nik": "910017",
		"nama": "SAFIRINA FEBRYANTI",
		"band": "IV",
		"posisi": "ASMAN GES QUALITY & PROJECT MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8294117647058812,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9864603481624773,
			"nilai_komparatif_unit": 0.9933733567677093,
			"summary_team": 0.8181818181818182
		}]
	},
	"910024": {
		"nik": "910024",
		"nama": "VIVIN VIOLITA",
		"band": "IV",
		"posisi": "ASMAN BUSINESS TERITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8361904761904755,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0487121079376214,
			"nilai_komparatif_unit": 1.056061370216727,
			"summary_team": 0.8769230769230769
		}]
	},
	"910066": {
		"nik": "910066",
		"nama": "LUH GDE SRIPUSPA DEWI",
		"band": "IV",
		"posisi": "ASMAN ACCESS OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9400000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0147299509001635,
			"nilai_komparatif_unit": 1.0218410698575817,
			"summary_team": 0.9538461538461538
		}]
	},
	"910069": {
		"nik": "910069",
		"nama": "IMAM MUTTAQIN",
		"band": "IV",
		"posisi": "OFF 1 SALES PROMOTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0070078930370687,
			"summary_team": 0.8
		}]
	},
	"910076": {
		"nik": "910076",
		"nama": "PRATAMA ADHI GUNA",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9585798816568045,
			"nilai_komparatif_unit": 0.9652975069349412,
			"summary_team": 0.8307692307692306
		}]
	},
	"910079": {
		"nik": "910079",
		"nama": "MONICA MELISSA PUTERI",
		"band": "IV",
		"posisi": "ASMAN DEBT MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888893,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0022727272727268,
			"nilai_komparatif_unit": 1.009296547339425,
			"summary_team": 0.8909090909090909
		}]
	},
	"910088": {
		"nik": "910088",
		"nama": "RIZKY FALAHUDIN",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8643678160919535,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0727756286266932,
			"nilai_komparatif_unit": 1.0802935254848829,
			"summary_team": 0.9272727272727274
		}]
	},
	"910104": {
		"nik": "910104",
		"nama": "RADEN RORO WAHYU DWI ARDHYA GARINI",
		"band": "IV",
		"posisi": "OFF 1 CAPABILITY DEVELOPMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7866666666666663,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.14406779661017,
			"nilai_komparatif_unit": 1.1520853013559686,
			"summary_team": 0.8999999999999999
		}]
	},
	"910109": {
		"nik": "910109",
		"nama": "RIFANDA PUTRI INDRESWARI",
		"band": "IV",
		"posisi": "OFF 1 PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8484848484848488,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8544642857142855,
			"nilai_komparatif_unit": 0.8604522800325664,
			"summary_team": 0.7250000000000001
		}]
	},
	"910111": {
		"nik": "910111",
		"nama": "SATRIA ADI SURYA WIJAYA",
		"band": "IV",
		"posisi": "ASMAN BGES SALES ENGINEER, BIDDING & PM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888888,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0125000000000002,
			"nilai_komparatif_unit": 1.019595491700032,
			"summary_team": 0.9000000000000001
		}]
	},
	"910114": {
		"nik": "910114",
		"nama": "MARCELINA LYDIA FORTUNA DEWI br. SIMAMOR",
		"band": "IV",
		"posisi": "ASMAN HOME SERVICE 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0216579536967882,
			"nilai_komparatif_unit": 1.0288176233567656,
			"summary_team": 0.8769230769230768
		}]
	},
	"910125": {
		"nik": "910125",
		"nama": "MELVIN SIMON ZEPTA MARBUN",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8488888888888886,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.033024567055981,
			"nilai_komparatif_unit": 1.0402638927265733,
			"summary_team": 0.8769230769230769
		}]
	},
	"910145": {
		"nik": "910145",
		"nama": "ACHMAD MARIZKI CHILMI",
		"band": "IV",
		"posisi": "ASMAN ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8453333333333325,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9463722397476351,
			"nilai_komparatif_unit": 0.9530043151770374,
			"summary_team": 0.8
		}]
	},
	"910152": {
		"nik": "910152",
		"nama": "BOBY RAHMAWAN",
		"band": "IV",
		"posisi": "OFF 1 PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888893,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181814,
			"nilai_komparatif_unit": 0.9886986586182123,
			"summary_team": 0.8727272727272727
		}]
	},
	"910158": {
		"nik": "910158",
		"nama": "OKTARIANTORO ANGGIT KURNIAWAN",
		"band": "IV",
		"posisi": "ASMAN DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9318181818181819,
			"nilai_komparatif_unit": 0.9383482639663594,
			"summary_team": 0.7454545454545455
		}]
	},
	"910163": {
		"nik": "910163",
		"nama": "M. YUSRO MUHTADI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8722222222222226,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0839606253618987,
			"nilai_komparatif_unit": 1.0915569054808287,
			"summary_team": 0.9454545454545454
		}]
	},
	"910167": {
		"nik": "910167",
		"nama": "ARIF SETIAWAN IRSAL",
		"band": "IV",
		"posisi": "ASMAN NETWORK PERF, QM & ADMIN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8111111111111113,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9863013698630138,
			"nilai_komparatif_unit": 0.9932132643653279,
			"summary_team": 0.8000000000000003
		}]
	},
	"910181": {
		"nik": "910181",
		"nama": "MOH ZAINAL ARIFIN",
		"band": "IV",
		"posisi": "ASMAN HOME SERVICE 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.992307692307692,
			"nilai_komparatif_unit": 0.9992616784752446,
			"summary_team": 0.8599999999999999
		}]
	},
	"910187": {
		"nik": "910187",
		"nama": "ABDURRAHMAN",
		"band": "IV",
		"posisi": "OFF 1 VERIFICATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9955869373345098,
			"nilai_komparatif_unit": 1.0025639041004526,
			"summary_team": 0.8545454545454546
		}]
	},
	"910200": {
		"nik": "910200",
		"nama": "BAYU ANDIKA VIRGUNZENA",
		"band": "IV",
		"posisi": "OFF 1 WIRELESS OPERATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0129870129870127,
			"nilai_komparatif_unit": 1.0200859176219652,
			"summary_team": 0.9454545454545454
		}]
	},
	"910203": {
		"nik": "910203",
		"nama": "NI NYOMAN SRI PARAMITA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969697,
			"nilai_komparatif_unit": 0.9764925023389756,
			"summary_team": 0.8727272727272728
		}]
	},
	"920004": {
		"nik": "920004",
		"nama": "SHEENA MEGA REGISTRA",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8484848484848488,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0499999999999996,
			"nilai_komparatif_unit": 1.0573582876889216,
			"summary_team": 0.8909090909090909
		}]
	},
	"920010": {
		"nik": "920010",
		"nama": "RESPATI LOY AMANDA",
		"band": "IV",
		"posisi": "ASMAN OLO FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9674099485420238,
			"nilai_komparatif_unit": 0.9741894539844023,
			"summary_team": 0.8545454545454546
		}]
	},
	"920056": {
		"nik": "920056",
		"nama": "RINDI MAUREN VIOLITA",
		"band": "IV",
		"posisi": "JM CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0369036903690367,
			"nilai_komparatif_unit": 1.0441702005208846,
			"summary_team": 0.8727272727272728
		}]
	},
	"920057": {
		"nik": "920057",
		"nama": "RURIN MIRA NINDYA",
		"band": "IV",
		"posisi": "ASMAN BUSINESS TERITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8277777777777782,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.922513727882855,
			"nilai_komparatif_unit": 0.9289786054130854,
			"summary_team": 0.7636363636363637
		}]
	},
	"920061": {
		"nik": "920061",
		"nama": "NUR FIRMANSYAH SYARIFUDDIN",
		"band": "IV",
		"posisi": "OFF 1 COLLECTION PARTNERSHIP",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9846153846153844,
			"nilai_komparatif_unit": 0.9915154639134212,
			"summary_team": 0.8533333333333334
		}]
	},
	"920062": {
		"nik": "920062",
		"nama": "NURUL FATHIA BARKAH",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8787878787878791,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9310344827586204,
			"nilai_komparatif_unit": 0.9375590728276153,
			"summary_team": 0.8181818181818182
		}]
	},
	"920064": {
		"nik": "920064",
		"nama": "ERVINA HANDAYANI",
		"band": "IV",
		"posisi": "ASMAN BUSINESS SALES ENGINEER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8361904761904755,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0671105659716147,
			"nilai_komparatif_unit": 1.0745887626766695,
			"summary_team": 0.8923076923076922
		}]
	},
	"920069": {
		"nik": "920069",
		"nama": "NANDEZ DARRAS DHIYA`UL HAQ",
		"band": "IV",
		"posisi": "OFF 1 HELPDESK OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8060606060606064,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9924812030075183,
			"nilai_komparatif_unit": 0.999436405119496,
			"summary_team": 0.7999999999999999
		}]
	},
	"920076": {
		"nik": "920076",
		"nama": "ABDURRAZAK BAIHAQI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER (GES)",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8547008547008538,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9572727272727284,
			"nilai_komparatif_unit": 0.9639811921527586,
			"summary_team": 0.8181818181818182
		}]
	},
	"920078": {
		"nik": "920078",
		"nama": "I GUSTI BAGUS YOGISWARA GHEARTHA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8294117647058812,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9645390070922,
			"nilai_komparatif_unit": 0.9712983932839824,
			"summary_team": 0.8
		}]
	},
	"920080": {
		"nik": "920080",
		"nama": "ANNISA DEVI BRILLIANTINI",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8523809523809525,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9565964761495487,
			"nilai_komparatif_unit": 0.9633002019340414,
			"summary_team": 0.8153846153846154
		}]
	},
	"920084": {
		"nik": "920084",
		"nama": "I WAYAN HENDRA PERMADI",
		"band": "IV",
		"posisi": "ASMAN GES SALES ENGINEER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8285714285714288,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96551724137931,
			"nilai_komparatif_unit": 0.9722834829323417,
			"summary_team": 0.8
		}]
	},
	"920086": {
		"nik": "920086",
		"nama": "IBNU MUHARAM",
		"band": "IV",
		"posisi": "ASMAN DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.763636363636364,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.027472527472527,
			"nilai_komparatif_unit": 1.0346729450435808,
			"summary_team": 0.7846153846153846
		}]
	},
	"920088": {
		"nik": "920088",
		"nama": "EDWIN NUGROHO",
		"band": "IV",
		"posisi": "ASMAN DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9320388349514565,
			"nilai_komparatif_unit": 0.9385704634131902,
			"summary_team": 0.8000000000000003
		}]
	},
	"920094": {
		"nik": "920094",
		"nama": "MUHAMMAD AKBAR TANTU",
		"band": "IV",
		"posisi": "OFF 1 ACCESS NETWORK ELEMENT OM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8060606060606064,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.974758324382384,
			"nilai_komparatif_unit": 0.9815893264566479,
			"summary_team": 0.7857142857142856
		}]
	},
	"920100": {
		"nik": "920100",
		"nama": "GALUH PRATIWI",
		"band": "IV",
		"posisi": "OFF 1 PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9573283858998141,
			"nilai_komparatif_unit": 0.9640372408295494,
			"summary_team": 0.7818181818181817
		}]
	},
	"920122": {
		"nik": "920122",
		"nama": "MOHAMMAD IQBAL",
		"band": "IV",
		"posisi": "ASMAN DEPLOYMENT & CAPEX MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.967409948542024,
			"nilai_komparatif_unit": 0.9741894539844025,
			"summary_team": 0.8545454545454546
		}]
	},
	"920129": {
		"nik": "920129",
		"nama": "INDRA ARDIAN",
		"band": "IV",
		"posisi": "ASMAN HR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8724637681159438,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1253397765025648,
			"nilai_komparatif_unit": 1.1332260372866534,
			"summary_team": 0.9818181818181818
		}]
	},
	"920133": {
		"nik": "920133",
		"nama": "MIFTACHUL UMAM",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999996,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090915,
			"nilai_komparatif_unit": 1.0985540651313481,
			"summary_team": 0.8727272727272728
		}]
	},
	"920139": {
		"nik": "920139",
		"nama": "MOCHAMMAD REZALDI ANSHAR",
		"band": "IV",
		"posisi": "ASMAN DEPLOYMENT & CAPEX MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828875,
			"nilai_komparatif_unit": 1.0123929619837906,
			"summary_team": 0.8545454545454546
		}]
	},
	"920154": {
		"nik": "920154",
		"nama": "AFIF NICHI MULIA",
		"band": "IV",
		"posisi": "OFF 1 ACCESS NETWORK ELEMENT OM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365352,
			"nilai_komparatif_unit": 0.99491688917556,
			"summary_team": 0.8727272727272728
		}]
	},
	"920160": {
		"nik": "920160",
		"nama": "CHRISTOPHORUS DRYANTORO",
		"band": "IV",
		"posisi": "ASMAN HOME SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999999,
			"nilai_komparatif_unit": 0.9667275773155857,
			"summary_team": 0.8
		}]
	},
	"920168": {
		"nik": "920168",
		"nama": "GHULAM MAULANA RIZQI",
		"band": "IV",
		"posisi": "ASMAN TGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8537549407114624,
			"nilai_komparatif_unit": 0.8597379640158371,
			"summary_team": 0.6545454545454545
		}]
	},
	"920169": {
		"nik": "920169",
		"nama": "RIFQI LUTHFI",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9846153846153846,
			"nilai_komparatif_unit": 0.9915154639134213,
			"summary_team": 0.8533333333333334
		}]
	},
	"920179": {
		"nik": "920179",
		"nama": "YULIUS DAMAR NATANAEL YUWIKA PUTRA",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8974358974358976,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.077142857142857,
			"nilai_komparatif_unit": 1.0846913590713565,
			"summary_team": 0.9666666666666667
		}]
	},
	"920195": {
		"nik": "920195",
		"nama": "ARYA ANANDITA",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8761904761904764,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9130434782608696,
			"nilai_komparatif_unit": 0.9194419892947148,
			"summary_team": 0.8000000000000003
		}]
	},
	"920199": {
		"nik": "920199",
		"nama": "MUHAMMAD FAYYADH",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0070078930370687,
			"summary_team": 0.8
		}]
	},
	"920262": {
		"nik": "920262",
		"nama": "RIZKY SAPUTRA",
		"band": "IV",
		"posisi": "ASMAN BUSINESS OBL & DELIVERY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777779,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9968354430379748,
			"nilai_komparatif_unit": 1.0038211591983437,
			"summary_team": 0.8750000000000002
		}]
	},
	"930012": {
		"nik": "930012",
		"nama": "SYARAFINA RAMLAH",
		"band": "IV",
		"posisi": "OFF 1 SERVICE DELIVERY & PROJECT MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8800000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9902597402597397,
			"nilai_komparatif_unit": 0.9971993745983952,
			"summary_team": 0.8714285714285712
		}]
	},
	"930022": {
		"nik": "930022",
		"nama": "INDAH PURNAMA SARI",
		"band": "IV",
		"posisi": "ASMAN BUSINESS QUALITY & DELIVERY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8388888888888891,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9319686935580972,
			"nilai_komparatif_unit": 0.9384998304764488,
			"summary_team": 0.7818181818181817
		}]
	},
	"930026": {
		"nik": "930026",
		"nama": "THEO BRIAN ANANTO",
		"band": "IV",
		"posisi": "JM CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333332,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9375000000000002,
			"nilai_komparatif_unit": 0.944069899722252,
			"summary_team": 0.8
		}]
	},
	"930035": {
		"nik": "930035",
		"nama": "AARON HANI KARTADI",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8000000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0070078930370687,
			"summary_team": 0.8000000000000003
		}]
	},
	"930038": {
		"nik": "930038",
		"nama": "FADLI RAHMAWAN",
		"band": "IV",
		"posisi": "ASMAN DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.941176470588235,
			"nilai_komparatif_unit": 0.9477721346231229,
			"summary_team": 0.8
		}]
	},
	"930040": {
		"nik": "930040",
		"nama": "RIZKA DARMAWAN DWI PUTRA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9066666666666662,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0026737967914445,
			"nilai_komparatif_unit": 1.0097004275104304,
			"summary_team": 0.9090909090909092
		}]
	},
	"930047": {
		"nik": "930047",
		"nama": "NURTRIA IMAN SARI",
		"band": "IV",
		"posisi": "ASMAN BGES SALES ENGINEER, BIDDING & PM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8909090909090912,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.061224489795918,
			"nilai_komparatif_unit": 1.0686614375087253,
			"summary_team": 0.9454545454545454
		}]
	},
	"930069": {
		"nik": "930069",
		"nama": "SYADWINA MAYHANI",
		"band": "IV",
		"posisi": "ASMAN SALES SUPPORT 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8523809523809525,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9598781107160995,
			"nilai_komparatif_unit": 0.9666048338446214,
			"summary_team": 0.8181818181818182
		}]
	},
	"930086": {
		"nik": "930086",
		"nama": "M. HABIBIE RIDHONANTA",
		"band": "IV",
		"posisi": "OFF 1 CASH BANK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.925925925925926,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.987428571428571,
			"nilai_komparatif_unit": 0.9943483652388878,
			"summary_team": 0.914285714285714
		}]
	},
	"930119": {
		"nik": "930119",
		"nama": "RIFKY IQBAL YURIANDI",
		"band": "IV",
		"posisi": "ASMAN TGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365351,
			"nilai_komparatif_unit": 0.9949168891755599,
			"summary_team": 0.8727272727272727
		}]
	},
	"930120": {
		"nik": "930120",
		"nama": "RENO SATYOGANA",
		"band": "IV",
		"posisi": "OFF 1 ACCESS CAPEX QE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0169491525423728,
			"nilai_komparatif_unit": 1.0240758234275273,
			"summary_team": 1
		}]
	},
	"930132": {
		"nik": "930132",
		"nama": "FAUZUL AKBAR",
		"band": "IV",
		"posisi": "ASMAN CCAN ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090908,
			"nilai_komparatif_unit": 1.0985540651313475,
			"summary_team": 0.9272727272727272
		}]
	},
	"930180": {
		"nik": "930180",
		"nama": "FIRMAN FIQRI FIRDAUS",
		"band": "IV",
		"posisi": "ASMAN OM IP NETWORK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8133333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0491803278688525,
			"nilai_komparatif_unit": 1.0565328713831539,
			"summary_team": 0.8533333333333334
		}]
	},
	"930186": {
		"nik": "930186",
		"nama": "MUHAMMAD HAFIDZ HADRIAN",
		"band": "IV",
		"posisi": "OFF 1 GOVERNMENT ACCOUNT MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7703703703703708,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0013736263736255,
			"nilai_komparatif_unit": 1.0083911456373933,
			"summary_team": 0.7714285714285712
		}]
	},
	"940003": {
		"nik": "940003",
		"nama": "SHINTYA DWIARISA PUSPITA",
		"band": "IV",
		"posisi": "OFF 1 DESIGN & ENGINEERING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8740740740740743,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0608628659476114,
			"nilai_komparatif_unit": 1.0682972794391703,
			"summary_team": 0.9272727272727272
		}]
	},
	"940028": {
		"nik": "940028",
		"nama": "ZARKA LAZUARDI PUTERA",
		"band": "IV",
		"posisi": "ASMAN SALES OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9898989898989897,
			"nilai_komparatif_unit": 0.996836096137704,
			"summary_team": 0.8909090909090909
		}]
	},
	"950003": {
		"nik": "950003",
		"nama": "RIZKY WAHYUDI",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7611111111111115,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9794293297942928,
			"nilai_komparatif_unit": 0.9862930657748589,
			"summary_team": 0.7454545454545455
		}]
	}
};