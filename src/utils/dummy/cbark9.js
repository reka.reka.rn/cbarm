export const cbark9 = {
	"650477": {
		"nik": "650477",
		"nama": "HUDAYA MIUN, S.E",
		"band": "III",
		"posisi": "MGR CCAN & WAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8478873239436632,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9616662407360067,
			"nilai_komparatif_unit": 0.9667622415787924,
			"summary_team": 0.8153846153846153
		}]
	},
	"650668": {
		"nik": "650668",
		"nama": "NENDAR SUNANDAR",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8262626262626273,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9682151589242043,
			"nilai_komparatif_unit": 0.9733458633795249,
			"summary_team": 0.8
		}]
	},
	"650734": {
		"nik": "650734",
		"nama": "HARIS NURTONO",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8376470588235311,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1019878997407067,
			"nilai_komparatif_unit": 1.1078274842326405,
			"summary_team": 0.9230769230769231
		}]
	},
	"650753": {
		"nik": "650753",
		"nama": "TAVIF BAMBANG SUHARTO",
		"band": "III",
		"posisi": "MGR CCAN & WAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481474,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0339267719180392,
			"nilai_komparatif_unit": 1.0394056911915706,
			"summary_team": 0.8769230769230769
		}]
	},
	"650805": {
		"nik": "650805",
		"nama": "EDY JOKO SULISTYO HERLAMBANG",
		"band": "III",
		"posisi": "MGR SEKDIV & PUBLIC RELATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8404040404040394,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9519230769230781,
			"nilai_komparatif_unit": 0.9569674474091987,
			"summary_team": 0.8
		}]
	},
	"651055": {
		"nik": "651055",
		"nama": "TOTOK SUWITO, S.T.",
		"band": "III",
		"posisi": "MGR SALES BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8320512820512806,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9935285053929138,
			"nilai_komparatif_unit": 0.9987933487308052,
			"summary_team": 0.8266666666666667
		}]
	},
	"651089": {
		"nik": "651089",
		"nama": "UTARIONO",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8484848484848507,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9742857142857115,
			"nilai_komparatif_unit": 0.9794485874435696,
			"summary_team": 0.8266666666666665
		}]
	},
	"660011": {
		"nik": "660011",
		"nama": "I GUSTI KETUT MUSTIKA ARYA",
		"band": "III",
		"posisi": "MGR CCAN & WAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8596491228070169,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0391836734693884,
			"nilai_komparatif_unit": 1.0446904497827132,
			"summary_team": 0.8933333333333333
		}]
	},
	"660045": {
		"nik": "660045",
		"nama": "ISLAN",
		"band": "III",
		"posisi": "KAKANDATEL SITUBONDO - BONDOWOSO",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8682170542635647,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9214285714285727,
			"nilai_komparatif_unit": 0.926311347362324,
			"summary_team": 0.7999999999999999
		}]
	},
	"660089": {
		"nik": "660089",
		"nama": "ACHMAD HARYANTO",
		"band": "III",
		"posisi": "MGR SECURITY AND SAFETY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8404040404040394,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0359162895927614,
			"nilai_komparatif_unit": 1.0414057515923631,
			"summary_team": 0.8705882352941177
		}]
	},
	"660108": {
		"nik": "660108",
		"nama": "KAMAL ADINOTO",
		"band": "III",
		"posisi": "MGR CCAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8262626262626273,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9843520782396077,
			"nilai_komparatif_unit": 0.9895682944358504,
			"summary_team": 0.8133333333333334
		}]
	},
	"660115": {
		"nik": "660115",
		"nama": "HARRY CHRISTHIJANTO",
		"band": "III",
		"posisi": "MGR CCAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8484848484848507,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0153846153846127,
			"nilai_komparatif_unit": 1.0207652772364746,
			"summary_team": 0.8615384615384615
		}]
	},
	"660116": {
		"nik": "660116",
		"nama": "YUNIAR FADJARJANTO",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & CONSTRUCTION SPV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0191502895979057,
			"nilai_komparatif_unit": 1.024550906272087,
			"summary_team": 0.8588235294117648
		}]
	},
	"660125": {
		"nik": "660125",
		"nama": "EDY MEDYA UPAYANAYA",
		"band": "III",
		"posisi": "KAKANDATEL BANJARNEGARA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.046875,
			"nilai_komparatif_unit": 1.0524225337038018,
			"summary_team": 0.8933333333333332
		}]
	},
	"660149": {
		"nik": "660149",
		"nama": "ANALINDAITA C.M. DUPE",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606965174129362,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9604624277456638,
			"nilai_komparatif_unit": 0.9655520494188858,
			"summary_team": 0.8266666666666667
		}]
	},
	"660200": {
		"nik": "660200",
		"nama": "TONY HERRY SJAM",
		"band": "III",
		"posisi": "KAKANDATEL BANYUWANGI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8682170542635647,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0135714285714301,
			"nilai_komparatif_unit": 1.0189424820985566,
			"summary_team": 0.88
		}]
	},
	"660203": {
		"nik": "660203",
		"nama": "I GUSTI LANANG ARIWANGSA",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8754098360655743,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0052434456928834,
			"nilai_komparatif_unit": 1.010570368100532,
			"summary_team": 0.88
		}]
	},
	"660226": {
		"nik": "660226",
		"nama": "DANIEL RAYA",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8376470588235311,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8999567847882437,
			"nilai_komparatif_unit": 0.9047257787899897,
			"summary_team": 0.7538461538461538
		}]
	},
	"660236": {
		"nik": "660236",
		"nama": "AGUS SUGIYANTORO, IR, MM",
		"band": "III",
		"posisi": "MGR ACCESS DATA & INVENTORY MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8773333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9644610708440504,
			"nilai_komparatif_unit": 0.9695718818735545,
			"summary_team": 0.8461538461538461
		}]
	},
	"660262": {
		"nik": "660262",
		"nama": "DANDI AVIANTO D.",
		"band": "III",
		"posisi": "MGR FINANCIAL SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8416666666666653,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9644729178800249,
			"nilai_komparatif_unit": 0.9695837916885918,
			"summary_team": 0.8117647058823529
		}]
	},
	"660263": {
		"nik": "660263",
		"nama": "DJAJA WIGUNA",
		"band": "III",
		"posisi": "MGR TAX OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8416666666666653,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1042516016307529,
			"nilai_komparatif_unit": 1.1101031817883875,
			"summary_team": 0.9294117647058822
		}]
	},
	"660276": {
		"nik": "660276",
		"nama": "RISMAWATI",
		"band": "III",
		"posisi": "MGR LOGISTIC & PROCUREMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8416666666666653,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.035360678925037,
			"nilai_komparatif_unit": 1.040847196667774,
			"summary_team": 0.8714285714285713
		}]
	},
	"660287": {
		"nik": "660287",
		"nama": "WEKO WAHJU WIDODO",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8262626262626273,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9682151589242043,
			"nilai_komparatif_unit": 0.9733458633795249,
			"summary_team": 0.8
		}]
	},
	"660326": {
		"nik": "660326",
		"nama": "AGUS TRIWIBOWO",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8376470588235311,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9391385767790245,
			"nilai_komparatif_unit": 0.9441152004515948,
			"summary_team": 0.7866666666666667
		}]
	},
	"660328": {
		"nik": "660328",
		"nama": "ALI MAHMUDI",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8636815920398017,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.015331442750797,
			"nilai_komparatif_unit": 1.020711822833605,
			"summary_team": 0.8769230769230769
		}]
	},
	"660344": {
		"nik": "660344",
		"nama": "MOKHAMAD ERFAN",
		"band": "III",
		"posisi": "ENGINE TEAM BP III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8320512820512806,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.961479198767336,
			"nilai_komparatif_unit": 0.9665742084491663,
			"summary_team": 0.8
		}]
	},
	"660354": {
		"nik": "660354",
		"nama": "I GDE NYOMAN ARYANA,IR",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.841333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9508716323296369,
			"nilai_komparatif_unit": 0.9559104310671535,
			"summary_team": 0.7999999999999999
		}]
	},
	"660361": {
		"nik": "660361",
		"nama": "MOKHAMAD MUNIR",
		"band": "III",
		"posisi": "MGR ASSET & FACILITY MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8404040404040394,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9702292899408296,
			"nilai_komparatif_unit": 0.9753706675516832,
			"summary_team": 0.8153846153846154
		}]
	},
	"660382": {
		"nik": "660382",
		"nama": "DARTO",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9633064381130889,
			"nilai_komparatif_unit": 0.9684111305859453,
			"summary_team": 0.8117647058823529
		}]
	},
	"660399": {
		"nik": "660399",
		"nama": "CAHYO HANDADARI,DRA",
		"band": "III",
		"posisi": "MGR ACCESS DATA MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0284575981787083,
			"nilai_komparatif_unit": 1.0339075355531107,
			"summary_team": 0.8666666666666666
		}]
	},
	"660411": {
		"nik": "660411",
		"nama": "I KETUT PURWA",
		"band": "III",
		"posisi": "MGR SAS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8632478632478621,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9801980198019815,
			"nilai_komparatif_unit": 0.9853922230748186,
			"summary_team": 0.8461538461538461
		}]
	},
	"660434": {
		"nik": "660434",
		"nama": "ALBERTUS DJOKO SOESENO, IR, MT",
		"band": "III",
		"posisi": "MGR WHOLESALE & INTERNATIONAL MARKETING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8765432098765426,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267605633802823,
			"nilai_komparatif_unit": 1.0322015079353501,
			"summary_team": 0.8999999999999999
		}]
	},
	"660473": {
		"nik": "660473",
		"nama": "JOKO SUYONO, S.E.",
		"band": "III",
		"posisi": "KAKANDATEL TEMANGGUNG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.871485943775102,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0222873900293237,
			"nilai_komparatif_unit": 1.027704630627446,
			"summary_team": 0.8909090909090909
		}]
	},
	"660654": {
		"nik": "660654",
		"nama": "DYAH ARULITA, IR, MM",
		"band": "III",
		"posisi": "MGR PLAN & BUDGET CONTROL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.004524886877829,
			"nilai_komparatif_unit": 1.0098480015441094,
			"summary_team": 0.8705882352941177
		}]
	},
	"670101": {
		"nik": "670101",
		"nama": "PETRUS GETAP SETIANTO",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481474,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9746724890829702,
			"nilai_komparatif_unit": 0.9798374118133403,
			"summary_team": 0.8266666666666667
		}]
	},
	"670110": {
		"nik": "670110",
		"nama": "I NYOMAN SUSILA",
		"band": "III",
		"posisi": "MGR PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8632478632478621,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.926732673267328,
			"nilai_komparatif_unit": 0.9316435563616466,
			"summary_team": 0.8
		}]
	},
	"670112": {
		"nik": "670112",
		"nama": "DWI PRAMANA, ST",
		"band": "III",
		"posisi": "MGR WIFI FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564102564102553,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9841745081266051,
			"nilai_komparatif_unit": 0.9893897833545501,
			"summary_team": 0.8428571428571427
		}]
	},
	"670117": {
		"nik": "670117",
		"nama": "HERAWAN SETIYARDI",
		"band": "III",
		"posisi": "MGR BILLING & PAYMENT COLLECTION (CONS)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8266666666666662,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9677419354838716,
			"nilai_komparatif_unit": 0.9728701322635054,
			"summary_team": 0.8
		}]
	},
	"670121": {
		"nik": "670121",
		"nama": "AMRU BADJIDEH",
		"band": "III",
		"posisi": "MGR INFRA SERVICE DELIVERY & OPERATION S",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8805555555555551,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0154017442939325,
			"nilai_komparatif_unit": 1.0207824969142258,
			"summary_team": 0.8941176470588235
		}]
	},
	"670139": {
		"nik": "670139",
		"nama": "AENUROFIK",
		"band": "III",
		"posisi": "ENGINE TEAM BP III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333321,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9163636363636377,
			"nilai_komparatif_unit": 0.9212195725142437,
			"summary_team": 0.7636363636363637
		}]
	},
	"670182": {
		"nik": "670182",
		"nama": "AWAN SAIPUT BUDI SETIAWAN",
		"band": "III",
		"posisi": "MGR CCAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0331112524691097,
			"nilai_komparatif_unit": 1.0385858501936225,
			"summary_team": 0.8705882352941176
		}]
	},
	"670184": {
		"nik": "670184",
		"nama": "DARU SETYO RAHARDJO",
		"band": "III",
		"posisi": "KAKANDATEL KLATEN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8484848484848507,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0214285714285687,
			"nilai_komparatif_unit": 1.0268412610295488,
			"summary_team": 0.8666666666666666
		}]
	},
	"670185": {
		"nik": "670185",
		"nama": "DWI PUTRO ARIEF RIYANTO,",
		"band": "III",
		"posisi": "MGR LOGISTIC & PROCUREMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8404040404040394,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.937049278846155,
			"nilai_komparatif_unit": 0.9420148310434299,
			"summary_team": 0.7875
		}]
	},
	"670187": {
		"nik": "670187",
		"nama": "HARTONO, S.T., M.Kom.",
		"band": "III",
		"posisi": "MGR ACCESS DATA & INVENTORY MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606060606060602,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.999295774647888,
			"nilai_komparatif_unit": 1.0045911795337874,
			"summary_team": 0.8600000000000001
		}]
	},
	"670189": {
		"nik": "670189",
		"nama": "SIWI WIDIARTO",
		"band": "III",
		"posisi": "MGR CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9912283638554972,
			"nilai_komparatif_unit": 0.9964810184290162,
			"summary_team": 0.8352941176470587
		}]
	},
	"670192": {
		"nik": "670192",
		"nama": "TJATUR RESMI BUDI UTOMO",
		"band": "III",
		"posisi": "KAKANDATEL MOJOKERTO",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8376470588235311,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1460674157303348,
			"nilai_komparatif_unit": 1.152140583601946,
			"summary_team": 0.96
		}]
	},
	"670215": {
		"nik": "670215",
		"nama": "AGUS ARIYANTO",
		"band": "III",
		"posisi": "MGR PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8262626262626273,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9682151589242043,
			"nilai_komparatif_unit": 0.9733458633795249,
			"summary_team": 0.8
		}]
	},
	"670247": {
		"nik": "670247",
		"nama": "DEWA GDE ADHI WESESA, IR",
		"band": "III",
		"posisi": "MGR PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0588853377697944,
			"nilai_komparatif_unit": 1.0644965158949187,
			"summary_team": 0.8923076923076922
		}]
	},
	"670270": {
		"nik": "670270",
		"nama": "WIDYO WINARNO",
		"band": "III",
		"posisi": "MGR WIFI FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8619047619047607,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9281767955801118,
			"nilai_komparatif_unit": 0.9330953312759374,
			"summary_team": 0.8
		}]
	},
	"670281": {
		"nik": "670281",
		"nama": "HADI HERMAWAN, IR",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481474,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.816257977830031,
			"nilai_komparatif_unit": 0.8205834404143979,
			"summary_team": 0.6923076923076923
		}]
	},
	"670288": {
		"nik": "670288",
		"nama": "KURNIA WARAS TJAHJO",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9375,
			"nilai_komparatif_unit": 0.9424679406302703,
			"summary_team": 0.7999999999999999
		}]
	},
	"670293": {
		"nik": "670293",
		"nama": "FIRMAN AROEBOESMAN",
		"band": "III",
		"posisi": "KAKANDATEL TANGGUL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8682170542635647,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.010027472527474,
			"nilai_komparatif_unit": 1.015379746147163,
			"summary_team": 0.8769230769230769
		}]
	},
	"680016": {
		"nik": "680016",
		"nama": "SUNANTO",
		"band": "III",
		"posisi": "MGR ASSET & FACILITY MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8416666666666653,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9504950495049519,
			"nilai_komparatif_unit": 0.9555318526786121,
			"summary_team": 0.7999999999999999
		}]
	},
	"680018": {
		"nik": "680018",
		"nama": "MOCH.YUSRI SIDNYNURI",
		"band": "III",
		"posisi": "MGR IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8733333333333322,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9160305343511463,
			"nilai_komparatif_unit": 0.9208847053486623,
			"summary_team": 0.8
		}]
	},
	"680030": {
		"nik": "680030",
		"nama": "HIRMAG MART DANY",
		"band": "III",
		"posisi": "MGR SAS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8846173746572108,
			"nilai_komparatif_unit": 0.8893050830282002,
			"summary_team": 0.7454545454545455
		}]
	},
	"680038": {
		"nik": "680038",
		"nama": "SANTIAGO B.P.T CARDOSO",
		"band": "III",
		"posisi": "MGR FINANCE & PAYMENT COLLECTION",
		"category": [{
			"code": "OC-3-01",
			"kriteria": 0.8417910447761199,
			"kriteria_bp": 0.9957279425884743,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0965630114566278,
			"nilai_komparatif_unit": 1.1012676902548548,
			"summary_team": 0.9230769230769231
		}]
	},
	"680044": {
		"nik": "680044",
		"nama": "MUTOHAR QODRI",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8807843137254925,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0445235975066758,
			"nilai_komparatif_unit": 1.0500586708072939,
			"summary_team": 0.9199999999999999
		}]
	},
	"680048": {
		"nik": "680048",
		"nama": "AGUS BUDI SANTOSO",
		"band": "III",
		"posisi": "MGR TELKOMGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.880555555555555,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9388012618296536,
			"nilai_komparatif_unit": 0.9437760980242056,
			"summary_team": 0.8266666666666667
		}]
	},
	"680050": {
		"nik": "680050",
		"nama": "WAHYU TRIYANTO",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8636815920398017,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497695852534554,
			"nilai_komparatif_unit": 1.0553324577601249,
			"summary_team": 0.9066666666666666
		}]
	},
	"680060": {
		"nik": "680060",
		"nama": "ANANG ARIEFSJAM",
		"band": "III",
		"posisi": "MGR WIRELESS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8733333333333322,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0076335877862608,
			"nilai_komparatif_unit": 1.0129731758835283,
			"summary_team": 0.88
		}]
	},
	"680065": {
		"nik": "680065",
		"nama": "R. JOJOK KUSWARDOJO",
		"band": "III",
		"posisi": "MGR DELIVERY & SALES SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8320512820512806,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0255778120184917,
			"nilai_komparatif_unit": 1.0310124890124441,
			"summary_team": 0.8533333333333333
		}]
	},
	"680068": {
		"nik": "680068",
		"nama": "TRISNO MEIRIYANTO",
		"band": "III",
		"posisi": "MGR CCAN & WAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8636815920398017,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8953917050691237,
			"nilai_komparatif_unit": 0.9001365080895182,
			"summary_team": 0.7733333333333333
		}]
	},
	"680127": {
		"nik": "680127",
		"nama": "I MADE GUNA SETIAWAN",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.841333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0301109350237734,
			"nilai_komparatif_unit": 1.035569633656083,
			"summary_team": 0.8666666666666667
		}]
	},
	"680158": {
		"nik": "680158",
		"nama": "I GEDE SUDARMAWAN",
		"band": "III",
		"posisi": "MGR DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.859047619047618,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9778270509977839,
			"nilai_komparatif_unit": 0.9830086901828817,
			"summary_team": 0.84
		}]
	},
	"680167": {
		"nik": "680167",
		"nama": "WIWIT CARIWATI",
		"band": "III",
		"posisi": "MGR ACCESS NEW FTTH & MODERNIZATION DEPL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8402777777777762,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9838016528925638,
			"nilai_komparatif_unit": 0.9890149523096646,
			"summary_team": 0.8266666666666667
		}]
	},
	"680173": {
		"nik": "680173",
		"nama": "SALVINUS, IR",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606965174129362,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0534104046242765,
			"nilai_komparatif_unit": 1.058992570330391,
			"summary_team": 0.9066666666666667
		}]
	},
	"680286": {
		"nik": "680286",
		"nama": "SUHERMAN",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8765432098765426,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9529411764705891,
			"nilai_komparatif_unit": 0.9579909420053578,
			"summary_team": 0.835294117647059
		}]
	},
	"680348": {
		"nik": "680348",
		"nama": "DJOKO SUDIYONO",
		"band": "III",
		"posisi": "MGR HR SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8157894736842092,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0623655913978514,
			"nilai_komparatif_unit": 1.0679952118626048,
			"summary_team": 0.8666666666666667
		}]
	},
	"680353": {
		"nik": "680353",
		"nama": "ARIF KOERNIAWAN",
		"band": "III",
		"posisi": "MGR FULFILLMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564102564102553,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9341317365269474,
			"nilai_komparatif_unit": 0.9390818282687257,
			"summary_team": 0.8
		}]
	},
	"680586": {
		"nik": "680586",
		"nama": "AGUS BUDI SETYAWAN",
		"band": "III",
		"posisi": "KAKANDATEL PURBALINGGA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9735576923076923,
			"nilai_komparatif_unit": 0.9787167075775883,
			"summary_team": 0.8307692307692307
		}]
	},
	"680590": {
		"nik": "680590",
		"nama": "COK GEDE DHARMA YOGA, ST",
		"band": "III",
		"posisi": "MGR ACCESS DATA MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8632478632478621,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0194059405940608,
			"nilai_komparatif_unit": 1.0248079119978113,
			"summary_team": 0.88
		}]
	},
	"690015": {
		"nik": "690015",
		"nama": "CHRISTINA",
		"band": "III",
		"posisi": "MGR CUSTOMER RELATIONSHIP MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333321,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0080000000000016,
			"nilai_komparatif_unit": 1.013341529765668,
			"summary_team": 0.8400000000000001
		}]
	},
	"690042": {
		"nik": "690042",
		"nama": "UTOMO, S.E.",
		"band": "III",
		"posisi": "MGR HUBUNGAN ANTAR INSTANSI/PEMDA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8567901234567893,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9973801414723615,
			"nilai_komparatif_unit": 1.0026653951562499,
			"summary_team": 0.8545454545454546
		}]
	},
	"690044": {
		"nik": "690044",
		"nama": "AGUNG WIDYANTORO",
		"band": "III",
		"posisi": "MGR FINANCE & PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8596491228070169,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9843014128728422,
			"nilai_komparatif_unit": 0.9895173605863818,
			"summary_team": 0.8461538461538461
		}]
	},
	"690047": {
		"nik": "690047",
		"nama": "I PUTU SUTARKA",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.841333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.950871632329637,
			"nilai_komparatif_unit": 0.9559104310671536,
			"summary_team": 0.8
		}]
	},
	"690288": {
		"nik": "690288",
		"nama": "JONO",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666677,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999987,
			"nilai_komparatif_unit": 1.005299136672287,
			"summary_team": 0.8666666666666666
		}]
	},
	"690302": {
		"nik": "690302",
		"nama": "I WAYAN SUDIARTA",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8684210526315793,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0887052341597792,
			"nilai_komparatif_unit": 1.0944744319914275,
			"summary_team": 0.9454545454545454
		}]
	},
	"700024": {
		"nik": "700024",
		"nama": "AGUNG SUGIHARTO",
		"band": "III",
		"posisi": "MGR ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0284575981787083,
			"nilai_komparatif_unit": 1.0339075355531107,
			"summary_team": 0.8666666666666666
		}]
	},
	"700026": {
		"nik": "700026",
		"nama": "CHRISTIANA NILA DEWI",
		"band": "III",
		"posisi": "KAKANDATEL BATANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8478873239436632,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0523894709941204,
			"nilai_komparatif_unit": 1.0579662266333953,
			"summary_team": 0.8923076923076921
		}]
	},
	"700029": {
		"nik": "700029",
		"nama": "HARIS SUNANDAR, IR",
		"band": "III",
		"posisi": "MGR DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8370370370370359,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0541384695471123,
			"nilai_komparatif_unit": 1.0597244933687593,
			"summary_team": 0.8823529411764706
		}]
	},
	"700238": {
		"nik": "700238",
		"nama": "EDI BAHARUDIN",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8684210526315793,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.013333333333333,
			"nilai_komparatif_unit": 1.0187031251612517,
			"summary_team": 0.88
		}]
	},
	"700335": {
		"nik": "700335",
		"nama": "SARWIJI",
		"band": "III",
		"posisi": "MGR PERFORMANCE MGT & SLA MONITORING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564102564102553,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9753434307854894,
			"nilai_komparatif_unit": 0.9805119089276402,
			"summary_team": 0.835294117647059
		}]
	},
	"700416": {
		"nik": "700416",
		"nama": "ENDANG WULANDARI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8596491228070169,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.002197802197803,
			"nilai_komparatif_unit": 1.0075085853243162,
			"summary_team": 0.8615384615384616
		}]
	},
	"700432": {
		"nik": "700432",
		"nama": "WAHYU LISTYANINGRUM, SE",
		"band": "III",
		"posisi": "MGR FINANCE & PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.015625,
			"nilai_komparatif_unit": 1.0210069356827927,
			"summary_team": 0.8666666666666666
		}]
	},
	"700652": {
		"nik": "700652",
		"nama": "IRWAN HANDOKO",
		"band": "III",
		"posisi": "MGR CHANNEL ACTIVATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.859047619047618,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0088691796008882,
			"nilai_komparatif_unit": 1.0142153152680526,
			"summary_team": 0.8666666666666667
		}]
	},
	"710006": {
		"nik": "710006",
		"nama": "PURNOMO",
		"band": "III",
		"posisi": "MGR ACCESS NEW FTTH & MODERNIZATION DEPL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8567901234567893,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.950391107451627,
			"nilai_komparatif_unit": 0.9554273598221406,
			"summary_team": 0.8142857142857142
		}]
	},
	"710159": {
		"nik": "710159",
		"nama": "GABRIEL ADHANASIUS HERNIMUS AHAB",
		"band": "III",
		"posisi": "MGR FINANCE & PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.841333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9984152139461188,
			"nilai_komparatif_unit": 1.0037059526205112,
			"summary_team": 0.84
		}]
	},
	"710204": {
		"nik": "710204",
		"nama": "ERNA WIDAYATI",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8684210526315793,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.151515151515151,
			"nilai_komparatif_unit": 1.1576171876832404,
			"summary_team": 1
		}]
	},
	"710220": {
		"nik": "710220",
		"nama": "DWI KRISTIANTI, SH",
		"band": "III",
		"posisi": "MGR LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8416666666666653,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9644729178800249,
			"nilai_komparatif_unit": 0.9695837916885918,
			"summary_team": 0.8117647058823529
		}]
	},
	"710384": {
		"nik": "710384",
		"nama": "PANTJARINI TRISNANING S,ST",
		"band": "III",
		"posisi": "MGR BILLING & PAYMENT COLLECTION (EBIS)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8292397660818702,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0640504438728962,
			"nilai_komparatif_unit": 1.0696889926011877,
			"summary_team": 0.8823529411764706
		}]
	},
	"710413": {
		"nik": "710413",
		"nama": "WAHYU UTOMO",
		"band": "III",
		"posisi": "MGR NETWORK DEPLOYMENT & PROJECT SUPERVI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8567901234567893,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9886421427360578,
			"nilai_komparatif_unit": 0.9938810925704,
			"summary_team": 0.8470588235294116
		}]
	},
	"710418": {
		"nik": "710418",
		"nama": "ASTINAH, ST, MM",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8807843137254925,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.999109528049864,
			"nilai_komparatif_unit": 1.0044039459895857,
			"summary_team": 0.8800000000000001
		}]
	},
	"710429": {
		"nik": "710429",
		"nama": "MUHAMMAD WACHID MUSHLIH",
		"band": "III",
		"posisi": "MGR PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8596491228070174,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0004081632653063,
			"nilai_komparatif_unit": 1.005709462850522,
			"summary_team": 0.8600000000000001
		}]
	},
	"710431": {
		"nik": "710431",
		"nama": "PANDE KETUT MEGA ADIPUTRA",
		"band": "III",
		"posisi": "KAKANDATEL DENPASAR UTARA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8754098360655743,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9900124843945063,
			"nilai_komparatif_unit": 0.9952586958565844,
			"summary_team": 0.8666666666666667
		}]
	},
	"710446": {
		"nik": "710446",
		"nama": "I MADE MAHRUTA WIDIASTAWA",
		"band": "III",
		"posisi": "MGR INFRA SERVICE DELIVERY & OPR SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.880555555555555,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9388012618296536,
			"nilai_komparatif_unit": 0.9437760980242056,
			"summary_team": 0.8266666666666667
		}]
	},
	"710450": {
		"nik": "710450",
		"nama": "WINARNI MURDANINGSIH",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481474,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9976486395700379,
			"nilai_komparatif_unit": 1.002935316062042,
			"summary_team": 0.8461538461538463
		}]
	},
	"710454": {
		"nik": "710454",
		"nama": "MOHAMMAD BAKRI, ST",
		"band": "III",
		"posisi": "KAKANDATEL JEPARA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666677,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.011834319526626,
			"nilai_komparatif_unit": 1.0171961678755093,
			"summary_team": 0.8769230769230769
		}]
	},
	"710460": {
		"nik": "710460",
		"nama": "SYAMSUL BACHRI",
		"band": "III",
		"posisi": "KAKANDATEL TULUNG AGUNG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8636815920398017,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497695852534554,
			"nilai_komparatif_unit": 1.0553324577601249,
			"summary_team": 0.9066666666666667
		}]
	},
	"710483": {
		"nik": "710483",
		"nama": "KRISDIJANTO TJATUR UTOMO",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481474,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0702049042660406,
			"nilai_komparatif_unit": 1.0758760663210996,
			"summary_team": 0.9076923076923077
		}]
	},
	"710504": {
		"nik": "710504",
		"nama": "CHRISTIANUS WIDYA UTOMO",
		"band": "III",
		"posisi": "MGR SALES PROMOTION & PRICING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8370370370370359,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9979177511712665,
			"nilai_komparatif_unit": 1.0032058537224255,
			"summary_team": 0.835294117647059
		}]
	},
	"710509": {
		"nik": "710509",
		"nama": "KAIMIN",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8596491228070169,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0236734693877558,
			"nilai_komparatif_unit": 1.029098055009837,
			"summary_team": 0.88
		}]
	},
	"710510": {
		"nik": "710510",
		"nama": "SUGIHARTO",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8750000000000001,
			"nilai_komparatif_unit": 0.8796367445882524,
			"summary_team": 0.7466666666666667
		}]
	},
	"710525": {
		"nik": "710525",
		"nama": "SURAHMANTO",
		"band": "III",
		"posisi": "KAKANDATEL WONOGIRI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8484848484848507,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9972527472527446,
			"nilai_komparatif_unit": 1.0025373258572519,
			"summary_team": 0.8461538461538461
		}]
	},
	"720026": {
		"nik": "720026",
		"nama": "DIDIK TRIPRASETYO UTOMO",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8684210526315793,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9979797979797976,
			"nilai_komparatif_unit": 1.003268229325475,
			"summary_team": 0.8666666666666667
		}]
	},
	"720030": {
		"nik": "720030",
		"nama": "SYANNET MARIA REINHART",
		"band": "III",
		"posisi": "MGR FINANCE & PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8684210526315793,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0440404040404034,
			"nilai_komparatif_unit": 1.0495729168328045,
			"summary_team": 0.9066666666666666
		}]
	},
	"720052": {
		"nik": "720052",
		"nama": "NURSUCI KARTINI",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8684210526315793,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0747474747474743,
			"nilai_komparatif_unit": 1.080442708504358,
			"summary_team": 0.9333333333333333
		}]
	},
	"720054": {
		"nik": "720054",
		"nama": "PAULO FERNANDO",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606965174129362,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.068901734104045,
			"nilai_komparatif_unit": 1.0745659904823082,
			"summary_team": 0.9199999999999999
		}]
	},
	"720072": {
		"nik": "720072",
		"nama": "MASHUDI",
		"band": "III",
		"posisi": "MGR CCAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8376470588235311,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9767620020428991,
			"nilai_komparatif_unit": 0.9819379973880223,
			"summary_team": 0.8181818181818182
		}]
	},
	"720083": {
		"nik": "720083",
		"nama": "ENDAH MURDANINGSIH,ST",
		"band": "III",
		"posisi": "MGR MARKETING & ACCOUNT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8320512820512806,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0365947611710342,
			"nilai_komparatif_unit": 1.0420878184842575,
			"summary_team": 0.8625
		}]
	},
	"720090": {
		"nik": "720090",
		"nama": "SUTIKNO",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8484848484848507,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1589285714285682,
			"nilai_komparatif_unit": 1.165069892321988,
			"summary_team": 0.9833333333333333
		}]
	},
	"720112": {
		"nik": "720112",
		"nama": "FATHUL HIMNI",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8684210526315793,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9365656565656562,
			"nilai_komparatif_unit": 0.941528645982369,
			"summary_team": 0.8133333333333334
		}]
	},
	"720120": {
		"nik": "720120",
		"nama": "ENDANG SUSILOWATI, ST.",
		"band": "III",
		"posisi": "MGR ENTERPRISE, OLO & WIFI DEPLOYMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8402777777777762,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9520661157024811,
			"nilai_komparatif_unit": 0.9571112441706432,
			"summary_team": 0.8
		}]
	},
	"720135": {
		"nik": "720135",
		"nama": "TRI SUGIHARTI",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8682170542635647,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0631868131868147,
			"nilai_komparatif_unit": 1.0688207854180662,
			"summary_team": 0.923076923076923
		}]
	},
	"720139": {
		"nik": "720139",
		"nama": "TUTIK SRI ASTUTI,ST",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8262626262626273,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9682151589242043,
			"nilai_komparatif_unit": 0.9733458633795249,
			"summary_team": 0.8
		}]
	},
	"720142": {
		"nik": "720142",
		"nama": "WULANDARI,ST",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.871485943775102,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0640134059488877,
			"nilai_komparatif_unit": 1.0696517584081577,
			"summary_team": 0.9272727272727272
		}]
	},
	"720163": {
		"nik": "720163",
		"nama": "SUGENG WIYOGOTO",
		"band": "III",
		"posisi": "MGR REVENUE ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8157894736842092,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0296774193548406,
			"nilai_komparatif_unit": 1.035133820728371,
			"summary_team": 0.8400000000000001
		}]
	},
	"720168": {
		"nik": "720168",
		"nama": "I G A B SWASTIKA, ST.",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.841333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9142996464708049,
			"nilai_komparatif_unit": 0.9191446452568784,
			"summary_team": 0.7692307692307693
		}]
	},
	"720177": {
		"nik": "720177",
		"nama": "NARDI, ST",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8478873239436632,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9906976744186031,
			"nilai_komparatif_unit": 0.9959475167962655,
			"summary_team": 0.8399999999999999
		}]
	},
	"720178": {
		"nik": "720178",
		"nama": "PRASADJA",
		"band": "III",
		"posisi": "MGR FINANCE & PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8636815920398017,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.018894009216589,
			"nilai_komparatif_unit": 1.0242932678260035,
			"summary_team": 0.88
		}]
	},
	"720192": {
		"nik": "720192",
		"nama": "DWI HAPPY AFIANTORO, ST, MT",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8807843137254925,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9764024933214579,
			"nilai_komparatif_unit": 0.9815765835807313,
			"summary_team": 0.8600000000000001
		}]
	},
	"720207": {
		"nik": "720207",
		"nama": "TITIS NURSHOLICHAH",
		"band": "III",
		"posisi": "MGR PERF MGT, SLA MONITORING&KPI REPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.880555555555555,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0145110410094644,
			"nilai_komparatif_unit": 1.019887073671319,
			"summary_team": 0.8933333333333333
		}]
	},
	"720209": {
		"nik": "720209",
		"nama": "KOMANG JONI WAHYU PRASETYA",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & CONSTRUCTION SPV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8632478632478621,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.033663366336635,
			"nilai_komparatif_unit": 1.0391408897879904,
			"summary_team": 0.8923076923076922
		}]
	},
	"720220": {
		"nik": "720220",
		"nama": "MASHADI FAJAR KUNCORO, ST",
		"band": "III",
		"posisi": "KAKANDATEL NGAWI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481474,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0061135371179049,
			"nilai_komparatif_unit": 1.011445070258932,
			"summary_team": 0.8533333333333334
		}]
	},
	"720227": {
		"nik": "720227",
		"nama": "DYAH RINA MARWATI",
		"band": "III",
		"posisi": "MGR CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333321,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0080000000000013,
			"nilai_komparatif_unit": 1.0133415297656678,
			"summary_team": 0.84
		}]
	},
	"720242": {
		"nik": "720242",
		"nama": "AGUSTINUS WAHJUDI HENDRO S.",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & CONSTRUCTION SPV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8807843137254925,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9606822385094845,
			"nilai_komparatif_unit": 0.9657730249899861,
			"summary_team": 0.8461538461538463
		}]
	},
	"720249": {
		"nik": "720249",
		"nama": "BUDIYANTI E.AGUSTIN,ST",
		"band": "III",
		"posisi": "MGR CUSTOMER DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333321,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9600000000000014,
			"nilai_komparatif_unit": 0.9650871712053981,
			"summary_team": 0.8
		}]
	},
	"720250": {
		"nik": "720250",
		"nama": "AGUSTINI PRASETYOWATI",
		"band": "III",
		"posisi": "MGR FINANCE & PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8682170542635647,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9568681318681332,
			"nilai_komparatif_unit": 0.9619387068762596,
			"summary_team": 0.8307692307692308
		}]
	},
	"720266": {
		"nik": "720266",
		"nama": "NUNUK ENY KRISTANTI",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8807843137254925,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9991095280498637,
			"nilai_komparatif_unit": 1.0044039459895855,
			"summary_team": 0.8799999999999999
		}]
	},
	"720276": {
		"nik": "720276",
		"nama": "MURTADO",
		"band": "III",
		"posisi": "MGR BIDDING MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8292397660818702,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9789264083630644,
			"nilai_komparatif_unit": 0.9841138731930926,
			"summary_team": 0.8117647058823529
		}]
	},
	"720279": {
		"nik": "720279",
		"nama": "I NYOMAN DIRGAYASA",
		"band": "III",
		"posisi": "KAKANDATEL DENPASAR SELATAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8754098360655743,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0090511860174778,
			"nilai_komparatif_unit": 1.014398286161519,
			"summary_team": 0.8833333333333335
		}]
	},
	"720311": {
		"nik": "720311",
		"nama": "RULLY SUSILO",
		"band": "III",
		"posisi": "KAKANDATEL SRAGEN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8484848484848507,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9585714285714261,
			"nilai_komparatif_unit": 0.9636510295815767,
			"summary_team": 0.8133333333333334
		}]
	},
	"720317": {
		"nik": "720317",
		"nama": "DIDIN WIDIATMOKO, ST",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8484848484848507,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.958571428571426,
			"nilai_komparatif_unit": 0.9636510295815766,
			"summary_team": 0.8133333333333332
		}]
	},
	"720336": {
		"nik": "720336",
		"nama": "MUNARTI",
		"band": "III",
		"posisi": "MGR GOVERNMENT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8376470588235311,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0468885047536711,
			"nilai_komparatif_unit": 1.0524361100210082,
			"summary_team": 0.8769230769230769
		}]
	},
	"720359": {
		"nik": "720359",
		"nama": "NUR AHMAD",
		"band": "III",
		"posisi": "MGR ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8484848484848507,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0017857142857116,
			"nilai_komparatif_unit": 1.0070943137020576,
			"summary_team": 0.85
		}]
	},
	"720364": {
		"nik": "720364",
		"nama": "ISNIJAZATUN INDRIASTUTI",
		"band": "III",
		"posisi": "KAKANDATEL KEPANJEN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8262626262626273,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9868346812112081,
			"nilai_komparatif_unit": 0.9920640530599003,
			"summary_team": 0.8153846153846154
		}]
	},
	"720365": {
		"nik": "720365",
		"nama": "PARYONO, ST",
		"band": "III",
		"posisi": "MGR OLO FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8805555555555551,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9652996845425871,
			"nilai_komparatif_unit": 0.970414939500695,
			"summary_team": 0.8499999999999999
		}]
	},
	"720367": {
		"nik": "720367",
		"nama": "JUNI HARIANTO,ST",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1392145702902616,
			"nilai_komparatif_unit": 1.145251423997292,
			"summary_team": 0.96
		}]
	},
	"720368": {
		"nik": "720368",
		"nama": "TOTO HARJANTO, ST",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8478873239436632,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9592469545957903,
			"nilai_komparatif_unit": 0.9643301353106697,
			"summary_team": 0.8133333333333332
		}]
	},
	"720375": {
		"nik": "720375",
		"nama": "HARIKA YUNIANTA",
		"band": "III",
		"posisi": "KAKANDATEL TEGAL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8478873239436632,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.022148394241416,
			"nilai_komparatif_unit": 1.0275648982818613,
			"summary_team": 0.8666666666666666
		}]
	},
	"720384": {
		"nik": "720384",
		"nama": "NUGROHO BUDI CAHYONO",
		"band": "III",
		"posisi": "MGR SALES PROMOTION & PRICING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.859047619047618,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0088691796008882,
			"nilai_komparatif_unit": 1.0142153152680526,
			"summary_team": 0.8666666666666666
		}]
	},
	"720391": {
		"nik": "720391",
		"nama": "RETNO YULISTYAWATI, ST",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.871485943775102,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9356256646579209,
			"nilai_komparatif_unit": 0.9405836729290437,
			"summary_team": 0.8153846153846153
		}]
	},
	"720397": {
		"nik": "720397",
		"nama": "LOUIS SRI LESTARI,ST",
		"band": "III",
		"posisi": "KAKANDATEL PROBOLINGGO",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8596491228070169,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9616326530612253,
			"nilai_komparatif_unit": 0.9667284759183319,
			"summary_team": 0.8266666666666667
		}]
	},
	"720400": {
		"nik": "720400",
		"nama": "IWAN AGUS SUGIARTO,ST",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8945755439779297,
			"nilai_komparatif_unit": 0.8993160220491554,
			"summary_team": 0.7538461538461537
		}]
	},
	"720406": {
		"nik": "720406",
		"nama": "BAMBANG SETIAWAN",
		"band": "III",
		"posisi": "KAKANDATEL KENDAL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8945755439779297,
			"nilai_komparatif_unit": 0.8993160220491554,
			"summary_team": 0.7538461538461537
		}]
	},
	"720414": {
		"nik": "720414",
		"nama": "WAHYONO",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8484848484848507,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0528571428571398,
			"nilai_komparatif_unit": 1.0584363767535347,
			"summary_team": 0.8933333333333332
		}]
	},
	"720427": {
		"nik": "720427",
		"nama": "DJOKO BOEDHI WIARSO",
		"band": "III",
		"posisi": "MGR IP & SERVICE NODE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8733333333333322,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0839694656488563,
			"nilai_komparatif_unit": 1.089713567995917,
			"summary_team": 0.9466666666666665
		}]
	},
	"720443": {
		"nik": "720443",
		"nama": "NOR KRISTANTO ADI",
		"band": "III",
		"posisi": "MGR DELIVERY&SALES SUPPORT BUSINESS SERV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8292397660818702,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0073010868663417,
			"nilai_komparatif_unit": 1.012638912995791,
			"summary_team": 0.8352941176470587
		}]
	},
	"720466": {
		"nik": "720466",
		"nama": "ATIK RAHMAWATI TRI ASTUTI, ST",
		"band": "III",
		"posisi": "MGR HR SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8416666666666653,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9924286546301704,
			"nilai_komparatif_unit": 0.9976876697085508,
			"summary_team": 0.8352941176470587
		}]
	},
	"720475": {
		"nik": "720475",
		"nama": "WAHYU SURYANTO",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006993006993007,
			"nilai_komparatif_unit": 1.0123292005651015,
			"summary_team": 0.8727272727272727
		}]
	},
	"720476": {
		"nik": "720476",
		"nama": "HERMAN WIDODO",
		"band": "III",
		"posisi": "KAKANDATEL PANDAAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8376470588235311,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0664794007490614,
			"nilai_komparatif_unit": 1.0721308208518108,
			"summary_team": 0.8933333333333333
		}]
	},
	"720478": {
		"nik": "720478",
		"nama": "TEGUH CIPTO EDHI,ST",
		"band": "III",
		"posisi": "MGR WHOLESALE SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8596491228070169,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.977142857142858,
			"nilai_komparatif_unit": 0.9823208706912082,
			"summary_team": 0.8400000000000001
		}]
	},
	"720485": {
		"nik": "720485",
		"nama": "HERY NUGROHO, ST",
		"band": "III",
		"posisi": "MGR ACCESS DATA MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8596491228070174,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0379905808477237,
			"nilai_komparatif_unit": 1.0434910348001836,
			"summary_team": 0.8923076923076922
		}]
	},
	"720500": {
		"nik": "720500",
		"nama": "KING ARIMAMI",
		"band": "III",
		"posisi": "MGR HELPDESK OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8619047619047607,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9281767955801118,
			"nilai_komparatif_unit": 0.9330953312759374,
			"summary_team": 0.8
		}]
	},
	"720525": {
		"nik": "720525",
		"nama": "J. DONY AGUS TATANG IRAWAN",
		"band": "III",
		"posisi": "MGR ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8619047619047607,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9745856353591174,
			"nilai_komparatif_unit": 0.9797500978397342,
			"summary_team": 0.84
		}]
	},
	"720531": {
		"nik": "720531",
		"nama": "WIDJI KUAT PRABOWO",
		"band": "III",
		"posisi": "MGR ACCESS SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9375,
			"nilai_komparatif_unit": 0.9424679406302703,
			"summary_team": 0.7999999999999999
		}]
	},
	"720533": {
		"nik": "720533",
		"nama": "HIKMATUL FURQON",
		"band": "III",
		"posisi": "MGR FINANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8376470588235311,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1019878997407067,
			"nilai_komparatif_unit": 1.1078274842326405,
			"summary_team": 0.9230769230769231
		}]
	},
	"720575": {
		"nik": "720575",
		"nama": "MUNGKAS SETYABADI, ST",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.953125,
			"nilai_komparatif_unit": 0.9581757396407747,
			"summary_team": 0.8133333333333332
		}]
	},
	"720580": {
		"nik": "720580",
		"nama": "MARLINDUNG TURNIP",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666677,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139849,
			"nilai_komparatif_unit": 0.9912390088866607,
			"summary_team": 0.8545454545454546
		}]
	},
	"720600": {
		"nik": "720600",
		"nama": "EMANUEL FEBRY DJATMIKO ADJI",
		"band": "III",
		"posisi": "MGR ACCESS CAPEX QE & PERFORMANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606060606060602,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0022007042253525,
			"nilai_komparatif_unit": 1.007511502730106,
			"summary_team": 0.8624999999999998
		}]
	},
	"730011": {
		"nik": "730011",
		"nama": "AGUSTIN ALBERTINA",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606965174129362,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.947354379724321,
			"nilai_komparatif_unit": 0.952374540059571,
			"summary_team": 0.8153846153846154
		}]
	},
	"730014": {
		"nik": "730014",
		"nama": "VITA TRI WARDANI, S.E.",
		"band": "III",
		"posisi": "SENIOR AM GOVERNMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8292397660818702,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9647390691114258,
			"nilai_komparatif_unit": 0.9698513532917434,
			"summary_team": 0.7999999999999999
		}]
	},
	"730076": {
		"nik": "730076",
		"nama": "KARDINAH",
		"band": "III",
		"posisi": "SENIOR STAFF III (STAFF PENGEMBANGAN)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8266666666666662,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9237536656891501,
			"nilai_komparatif_unit": 0.9286487626151642,
			"summary_team": 0.7636363636363637
		}]
	},
	"730077": {
		"nik": "730077",
		"nama": "YUDHA BHAKTI DWI NUGROHO,ST",
		"band": "III",
		"posisi": "MGR ACCESS OM & PARTNERSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8773333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9644610708440504,
			"nilai_komparatif_unit": 0.9695718818735545,
			"summary_team": 0.8461538461538461
		}]
	},
	"730092": {
		"nik": "730092",
		"nama": "YANI QOYIMAH",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8807843137254925,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9839715048975932,
			"nilai_komparatif_unit": 0.9891857043836827,
			"summary_team": 0.8666666666666667
		}]
	},
	"730106": {
		"nik": "730106",
		"nama": "AJI WIDYO UTOMO, ST, MM",
		"band": "III",
		"posisi": "MGR SOLUTION & SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8292397660818702,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.066290550070523,
			"nilai_komparatif_unit": 1.0719409694277162,
			"summary_team": 0.8842105263157892
		}]
	},
	"730118": {
		"nik": "730118",
		"nama": "SUBEKTI WIBOWO, S.KOMP",
		"band": "III",
		"posisi": "MGR SERVICE DELIVERY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8619047619047607,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9745856353591175,
			"nilai_komparatif_unit": 0.9797500978397343,
			"summary_team": 0.8400000000000001
		}]
	},
	"730121": {
		"nik": "730121",
		"nama": "MOHAMAD KARSIYANTO",
		"band": "III",
		"posisi": "MGR CCAN & WAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8682170542635647,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9214285714285728,
			"nilai_komparatif_unit": 0.9263113473623241,
			"summary_team": 0.8
		}]
	},
	"730126": {
		"nik": "730126",
		"nama": "SHINTA PRATIWI DAMAYANTI",
		"band": "III",
		"posisi": "MGR FULFILLMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8619047619047607,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9745856353591175,
			"nilai_komparatif_unit": 0.9797500978397343,
			"summary_team": 0.8400000000000001
		}]
	},
	"730149": {
		"nik": "730149",
		"nama": "DEWI MUDRIKAH TRI ATMAJAWATI",
		"band": "III",
		"posisi": "MGR HR PLANNING & DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8157894736842092,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9969892473118296,
			"nilai_komparatif_unit": 1.0022724295941368,
			"summary_team": 0.8133333333333334
		}]
	},
	"730189": {
		"nik": "730189",
		"nama": "I KETUT DHARMASUTA",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.877777777777778,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.00253164556962,
			"nilai_komparatif_unit": 1.0078441977777874,
			"summary_team": 0.88
		}]
	},
	"730203": {
		"nik": "730203",
		"nama": "ABDUS SYAKUR, ST",
		"band": "III",
		"posisi": "KAKANDATEL PATI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666677,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999987,
			"nilai_komparatif_unit": 1.005299136672287,
			"summary_team": 0.8666666666666666
		}]
	},
	"730208": {
		"nik": "730208",
		"nama": "ENGGAR CAHYADI, ST,. MM.",
		"band": "III",
		"posisi": "KAKANDATEL SALATIGA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8484848484848507,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8957142857142834,
			"nilai_komparatif_unit": 0.9004607981336044,
			"summary_team": 0.76
		}]
	},
	"730210": {
		"nik": "730210",
		"nama": "MOHAMAD ARIF SETYABUDI, ST",
		"band": "III",
		"posisi": "MGR CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8624999999999998,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9420289855072463,
			"nilai_komparatif_unit": 0.9470209258507063,
			"summary_team": 0.8124999999999998
		}]
	},
	"730231": {
		"nik": "730231",
		"nama": "YOGI BAHTIAR, ST",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0014204545454546,
			"nilai_komparatif_unit": 1.006727118400516,
			"summary_team": 0.8545454545454546
		}]
	},
	"730245": {
		"nik": "730245",
		"nama": "DWI AGUNG SULISTYANTA, ST",
		"band": "III",
		"posisi": "MGR PERF MGT, SLA MONITORING & KPI REPOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8805555555555551,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0383055430374044,
			"nilai_komparatif_unit": 1.0438076660175541,
			"summary_team": 0.914285714285714
		}]
	},
	"730249": {
		"nik": "730249",
		"nama": "FATONI KURNIAWAN,ST",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8376470588235311,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.82649092480553,
			"nilai_komparatif_unit": 0.8308706131744804,
			"summary_team": 0.6923076923076924
		}]
	},
	"730268": {
		"nik": "730268",
		"nama": "KEMAS CEK AGUS ABDUSSOMAD A.",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0588853377697944,
			"nilai_komparatif_unit": 1.0644965158949187,
			"summary_team": 0.8923076923076922
		}]
	},
	"730271": {
		"nik": "730271",
		"nama": "BAHTIAR BARID,ST",
		"band": "III",
		"posisi": "MGR ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8376470588235311,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.955056179775279,
			"nilai_komparatif_unit": 0.9601171530016217,
			"summary_team": 0.8
		}]
	},
	"730285": {
		"nik": "730285",
		"nama": "MUHAMMAD ZEN EFFENDY",
		"band": "III",
		"posisi": "KAKANDATEL NGANJUK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8773333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.017068038344635,
			"nilai_komparatif_unit": 1.0224576208848395,
			"summary_team": 0.8923076923076924
		}]
	},
	"730288": {
		"nik": "730288",
		"nama": "HIDAYAT KURNIAWAN, S.T.,M.MT.",
		"band": "III",
		"posisi": "MGR PROJECT MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8292397660818702,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9789264083630644,
			"nilai_komparatif_unit": 0.9841138731930926,
			"summary_team": 0.8117647058823529
		}]
	},
	"730300": {
		"nik": "730300",
		"nama": "WIYATNO",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8262626262626273,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9682151589242043,
			"nilai_komparatif_unit": 0.9733458633795249,
			"summary_team": 0.8
		}]
	},
	"730322": {
		"nik": "730322",
		"nama": "WIJANARKO,ST",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8262626262626273,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0488997555012212,
			"nilai_komparatif_unit": 1.0544580186611519,
			"summary_team": 0.8666666666666667
		}]
	},
	"730341": {
		"nik": "730341",
		"nama": "I GUSTI BAGUS PUTRA SURYANTARA",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8262626262626273,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0240737257852162,
			"nilai_komparatif_unit": 1.0295004324206514,
			"summary_team": 0.8461538461538463
		}]
	},
	"730352": {
		"nik": "730352",
		"nama": "MATIUS YUDITH SANTOSO",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8682170542635647,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9675000000000014,
			"nilai_komparatif_unit": 0.9726269147304403,
			"summary_team": 0.84
		}]
	},
	"730382": {
		"nik": "730382",
		"nama": "OKA INDRYATNO, ST, MM",
		"band": "III",
		"posisi": "KAKANDATEL BATU",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8262626262626273,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9682151589242043,
			"nilai_komparatif_unit": 0.9733458633795249,
			"summary_team": 0.8
		}]
	},
	"730386": {
		"nik": "730386",
		"nama": "ATIK MASYANI",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8636815920398017,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9084544487770289,
			"nilai_komparatif_unit": 0.9132684730616466,
			"summary_team": 0.7846153846153847
		}]
	},
	"730387": {
		"nik": "730387",
		"nama": "AMBAR PRIHADI",
		"band": "III",
		"posisi": "KAKANDATEL UNGARAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9809903244166142,
			"nilai_komparatif_unit": 0.9861887262198902,
			"summary_team": 0.8266666666666665
		}]
	},
	"730393": {
		"nik": "730393",
		"nama": "NASHRUDDIN KINASIH AKBAR,ST",
		"band": "III",
		"posisi": "MGR PLANNING, ENGINEERING & CAPEX MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8402777777777762,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9520661157024811,
			"nilai_komparatif_unit": 0.9571112441706432,
			"summary_team": 0.8
		}]
	},
	"730410": {
		"nik": "730410",
		"nama": "ANTOK RAHMAT SUDIBYA",
		"band": "III",
		"posisi": "MGR HELPDESK OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564102564102553,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9880239520958096,
			"nilai_komparatif_unit": 0.9932596260534597,
			"summary_team": 0.846153846153846
		}]
	},
	"730440": {
		"nik": "730440",
		"nama": "NOER WIDJAYA",
		"band": "III",
		"posisi": "KAKANDATEL SLAWI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8478873239436632,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0342448249424978,
			"nilai_komparatif_unit": 1.039725429622475,
			"summary_team": 0.8769230769230769
		}]
	},
	"730450": {
		"nik": "730450",
		"nama": "PRASETYO WIDODO,ST",
		"band": "III",
		"posisi": "MGR HUBUNGAN ANTAR INSTANSI/PEMDA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8402777777777762,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9838016528925638,
			"nilai_komparatif_unit": 0.9890149523096646,
			"summary_team": 0.8266666666666667
		}]
	},
	"730453": {
		"nik": "730453",
		"nama": "ZAINAL ARIFIN",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8636815920398017,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9797057780928741,
			"nilai_komparatif_unit": 0.9848973729096188,
			"summary_team": 0.8461538461538461
		}]
	},
	"730456": {
		"nik": "730456",
		"nama": "BAMBANG PUJO LAKSONO",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8376470588235311,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9366897147796005,
			"nilai_komparatif_unit": 0.9416533615977443,
			"summary_team": 0.7846153846153846
		}]
	},
	"730470": {
		"nik": "730470",
		"nama": "ACHMAD BUDIANTORO, ST",
		"band": "III",
		"posisi": "KAKANDATEL KEBUMEN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.871485943775102,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0591988656504765,
			"nilai_komparatif_unit": 1.064811705202691,
			"summary_team": 0.9230769230769229
		}]
	},
	"730485": {
		"nik": "730485",
		"nama": "INDAH ANDRI WINDIARINI",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8596491228070169,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9616326530612253,
			"nilai_komparatif_unit": 0.9667284759183319,
			"summary_team": 0.8266666666666667
		}]
	},
	"730486": {
		"nik": "730486",
		"nama": "IMPERATA JOKO SUBROTO",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8385885031303315,
			"nilai_komparatif_unit": 0.8430322982202287,
			"summary_team": 0.7066666666666667
		}]
	},
	"730523": {
		"nik": "730523",
		"nama": "DANI CHRISTANTO",
		"band": "III",
		"posisi": "MGR ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8262626262626273,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0054542034982121,
			"nilai_komparatif_unit": 1.010782242740276,
			"summary_team": 0.8307692307692308
		}]
	},
	"730534": {
		"nik": "730534",
		"nama": "ARIEF TRIWIBOWO",
		"band": "III",
		"posisi": "KAKANDATEL PONOROGO",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481474,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9613705072220364,
			"nilai_komparatif_unit": 0.9664649409325131,
			"summary_team": 0.8153846153846154
		}]
	},
	"730535": {
		"nik": "730535",
		"nama": "ERNI PURWANTI DEWI",
		"band": "III",
		"posisi": "MGR HOME SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8807843137254925,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.074799643811217,
			"nilai_komparatif_unit": 1.0804951540190995,
			"summary_team": 0.9466666666666665
		}]
	},
	"730549": {
		"nik": "730549",
		"nama": "JOKO SUSANTOYO, ST",
		"band": "III",
		"posisi": "MGR SALES BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8292397660818702,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0250352609308897,
			"nilai_komparatif_unit": 1.0304670628724772,
			"summary_team": 0.8499999999999999
		}]
	},
	"730575": {
		"nik": "730575",
		"nama": "ARIS DONO WARIH UTOMO",
		"band": "III",
		"posisi": "MGR QUALITY & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9638009049773762,
			"nilai_komparatif_unit": 0.9689082176977265,
			"summary_team": 0.8352941176470587
		}]
	},
	"730587": {
		"nik": "730587",
		"nama": "AGUS SETIYAWAN AKHMADI",
		"band": "III",
		"posisi": "KAKANDATEL WONOSOBO",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.871485943775102,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0415455512229685,
			"nilai_komparatif_unit": 1.0470648434493128,
			"summary_team": 0.9076923076923076
		}]
	},
	"730591": {
		"nik": "730591",
		"nama": "KRIS WISNU WARDANA, ST",
		"band": "III",
		"posisi": "MGR DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8807843137254925,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0445235975066758,
			"nilai_komparatif_unit": 1.0500586708072939,
			"summary_team": 0.9199999999999999
		}]
	},
	"740019": {
		"nik": "740019",
		"nama": "TARSISIUS NUWA",
		"band": "III",
		"posisi": "MGR CCAN & WAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8684210526315793,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9212121212121209,
			"nilai_komparatif_unit": 0.9260937501465926,
			"summary_team": 0.8
		}]
	},
	"740021": {
		"nik": "740021",
		"nama": "SYARIFUDDIN",
		"band": "III",
		"posisi": "KAKANDATEL BIMA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8684210526315793,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0286868686868682,
			"nilai_komparatif_unit": 1.0341380209970281,
			"summary_team": 0.8933333333333332
		}]
	},
	"740022": {
		"nik": "740022",
		"nama": "YULIUS PASKALIS LEKY",
		"band": "III",
		"posisi": "MGR CCAN & WAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606965174129362,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.084393063583814,
			"nilai_komparatif_unit": 1.090139410634226,
			"summary_team": 0.9333333333333333
		}]
	},
	"740030": {
		"nik": "740030",
		"nama": "JUSUF JAMBRES LONA",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606965174129362,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9294797687861263,
			"nilai_komparatif_unit": 0.9344052091150509,
			"summary_team": 0.8
		}]
	},
	"740040": {
		"nik": "740040",
		"nama": "RULLI KURNIA",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666677,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0461538461538447,
			"nilai_komparatif_unit": 1.0516975583648538,
			"summary_team": 0.9066666666666665
		}]
	},
	"740051": {
		"nik": "740051",
		"nama": "AMIR SYARIFUDIN, ST",
		"band": "III",
		"posisi": "SM HUMAN CAPITAL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0219780219780228,
			"nilai_komparatif_unit": 1.0273936231925591,
			"summary_team": 0.8857142857142856
		}]
	},
	"740070": {
		"nik": "740070",
		"nama": "HENDRO CAHYADIANTO, A.Md, S.KOMP",
		"band": "III",
		"posisi": "MGR SEKDIV & PUBLIC RELATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8416666666666653,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.950495049504952,
			"nilai_komparatif_unit": 0.9555318526786122,
			"summary_team": 0.8
		}]
	},
	"740083": {
		"nik": "740083",
		"nama": "RONI SETIYAWAN, ST",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9968127490039789,
			"nilai_komparatif_unit": 1.0020949959976304,
			"summary_team": 0.8399999999999999
		}]
	},
	"740087": {
		"nik": "740087",
		"nama": "TRI HERU UTOMO, ST",
		"band": "III",
		"posisi": "KAKANDATEL CILACAP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9915865384615384,
			"nilai_komparatif_unit": 0.9968410910512473,
			"summary_team": 0.846153846153846
		}]
	},
	"740088": {
		"nik": "740088",
		"nama": "MOHAMMAD AZIZ YUSUF LATIF, ST",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8484848484848507,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9428571428571403,
			"nilai_komparatif_unit": 0.9478534717195835,
			"summary_team": 0.7999999999999999
		}]
	},
	"740089": {
		"nik": "740089",
		"nama": "ANDI SUSENA",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8596491228070169,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0379905808477243,
			"nilai_komparatif_unit": 1.0434910348001842,
			"summary_team": 0.8923076923076922
		}]
	},
	"740105": {
		"nik": "740105",
		"nama": "SUPRIYARSO,ST",
		"band": "III",
		"posisi": "MGR BILLING & PAYMENT COLLECTION (EBIS)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8266666666666662,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0049627791563283,
			"nilai_komparatif_unit": 1.0102882142736402,
			"summary_team": 0.8307692307692308
		}]
	},
	"740113": {
		"nik": "740113",
		"nama": "YAN ANDI SAIFUDIN, ST",
		"band": "III",
		"posisi": "MGR ENTERPRISE, OLO & WIFI DEPLOYMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8567901234567893,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0298355653500604,
			"nilai_komparatif_unit": 1.0352928047608336,
			"summary_team": 0.8823529411764706
		}]
	},
	"740126": {
		"nik": "740126",
		"nama": "SUKARNO",
		"band": "III",
		"posisi": "MGR CCAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8596491228070174,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9843014128728416,
			"nilai_komparatif_unit": 0.9895173605863812,
			"summary_team": 0.8461538461538461
		}]
	},
	"740129": {
		"nik": "740129",
		"nama": "I WAYAN GDE SUKA WIJAYA",
		"band": "III",
		"posisi": "KAKANDATEL TABANAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.841333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9825673534072915,
			"nilai_komparatif_unit": 0.9877741121027253,
			"summary_team": 0.8266666666666667
		}]
	},
	"740133": {
		"nik": "740133",
		"nama": "ABDUL AZIZ MUSLIM",
		"band": "III",
		"posisi": "MGR CHANNEL ACTIVATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8370370370370359,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.993479273404752,
			"nilai_komparatif_unit": 0.9987438558556093,
			"summary_team": 0.8315789473684209
		}]
	},
	"740136": {
		"nik": "740136",
		"nama": "IMAM SAMROZI",
		"band": "III",
		"posisi": "KAKANDATEL LUMAJANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8596491228070169,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0857142857142865,
			"nilai_komparatif_unit": 1.0914676341013423,
			"summary_team": 0.9333333333333332
		}]
	},
	"740141": {
		"nik": "740141",
		"nama": "TRINIL ANJAR LESTARI, ST",
		"band": "III",
		"posisi": "SENIOR AM ENTERPRISE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0185685828116056,
			"nilai_komparatif_unit": 1.023966116942023,
			"summary_team": 0.8583333333333333
		}]
	},
	"740146": {
		"nik": "740146",
		"nama": "ARIEF DARYATNO, ST",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8807843137254925,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9569393206971097,
			"nilai_komparatif_unit": 0.9620102729445704,
			"summary_team": 0.8428571428571427
		}]
	},
	"740149": {
		"nik": "740149",
		"nama": "NUNUNG SADTOMO PRAPTO, ST",
		"band": "III",
		"posisi": "MGR HOME SERVICE 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1075697211155322,
			"nilai_komparatif_unit": 1.1134388844418115,
			"summary_team": 0.9333333333333333
		}]
	},
	"740168": {
		"nik": "740168",
		"nama": "MU'MIN AMIN",
		"band": "III",
		"posisi": "MGR FINANCE & PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666677,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0828402366863892,
			"nilai_komparatif_unit": 1.0885783550948434,
			"summary_team": 0.9384615384615383
		}]
	},
	"740176": {
		"nik": "740176",
		"nama": "AMELIA YUDANINGRUM",
		"band": "III",
		"posisi": "MGR TOP PRIORITY SOLUTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8619047619047607,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.020994475138123,
			"nilai_komparatif_unit": 1.026404864403531,
			"summary_team": 0.88
		}]
	},
	"740178": {
		"nik": "740178",
		"nama": "LULU KURNIJATI, ST",
		"band": "III",
		"posisi": "MGR HR PLANNING & DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8807843137254925,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9082813891362399,
			"nilai_komparatif_unit": 0.9130944963541686,
			"summary_team": 0.8
		}]
	},
	"740182": {
		"nik": "740182",
		"nama": "SUGENG MULYONO",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666677,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9763313609467444,
			"nilai_komparatif_unit": 0.9815050742658424,
			"summary_team": 0.8461538461538461
		}]
	},
	"740189": {
		"nik": "740189",
		"nama": "MUHAMAD JUMHAN",
		"band": "III",
		"posisi": "MGR INFRA MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564102564102553,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9341317365269474,
			"nilai_komparatif_unit": 0.9390818282687257,
			"summary_team": 0.8
		}]
	},
	"740197": {
		"nik": "740197",
		"nama": "WALID WAHYUDI",
		"band": "III",
		"posisi": "MGR NETWORK DEPLOYMENT & PROJECT SUPERVI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8733333333333322,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9465648854961844,
			"nilai_komparatif_unit": 0.9515808621936176,
			"summary_team": 0.8266666666666667
		}]
	},
	"740198": {
		"nik": "740198",
		"nama": "INTAN SULISTIYATI, ST,  MM",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481474,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9903930131004376,
			"nilai_komparatif_unit": 0.9956412410361362,
			"summary_team": 0.8400000000000001
		}]
	},
	"740232": {
		"nik": "740232",
		"nama": "DWI SATRIANANDA, ST, MT.",
		"band": "III",
		"posisi": "MGR WHOLESALE ACCESS NETWORK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8484848484848507,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0371428571428543,
			"nilai_komparatif_unit": 1.0426388188915419,
			"summary_team": 0.8799999999999999
		}]
	},
	"740234": {
		"nik": "740234",
		"nama": "TRI ADI SANTOSA, ST",
		"band": "III",
		"posisi": "MGR DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0531801365964655,
			"nilai_komparatif_unit": 1.0587610820808293,
			"summary_team": 0.8874999999999997
		}]
	},
	"740237": {
		"nik": "740237",
		"nama": "GUNARSO EFENDI, ST",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.871485943775102,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0097695852534543,
			"nilai_komparatif_unit": 1.0151204922932322,
			"summary_team": 0.88
		}]
	},
	"740256": {
		"nik": "740256",
		"nama": "DONY EKO ARIESANTO, M.ENG",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8376470588235311,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0285220397579928,
			"nilai_komparatif_unit": 1.0339723186171312,
			"summary_team": 0.8615384615384616
		}]
	},
	"740291": {
		"nik": "740291",
		"nama": "SUTRISMANTO",
		"band": "III",
		"posisi": "OSM REGIONAL OPERATION CENTER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8402777777777762,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9996694214876052,
			"nilai_komparatif_unit": 1.0049668063791752,
			"summary_team": 0.8400000000000001
		}]
	},
	"740292": {
		"nik": "740292",
		"nama": "SUTEJO, ST",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8478873239436632,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0064230343300093,
			"nilai_komparatif_unit": 1.0117562075390631,
			"summary_team": 0.8533333333333332
		}]
	},
	"740296": {
		"nik": "740296",
		"nama": "SUHADI RUSDIANTORO",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.871485943775102,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0238922367954606,
			"nilai_komparatif_unit": 1.0293179816959346,
			"summary_team": 0.8923076923076922
		}]
	},
	"740297": {
		"nik": "740297",
		"nama": "MUHAMMAD SIDIK RAHARJA",
		"band": "III",
		"posisi": "MGR NETWORK AREA & IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8807843137254925,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9385574354407813,
			"nilai_komparatif_unit": 0.9435309795659743,
			"summary_team": 0.8266666666666668
		}]
	},
	"750009": {
		"nik": "750009",
		"nama": "I MADE SUKAYASO",
		"band": "III",
		"posisi": "SENIOR STAFF III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8684210526315793,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8793388429752063,
			"nilai_komparatif_unit": 0.8839985796853836,
			"summary_team": 0.7636363636363637
		}]
	},
	"750024": {
		"nik": "750024",
		"nama": "AGUS WIDHIARSANA",
		"band": "III",
		"posisi": "MGR CUSTOMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8754098360655743,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0661672908863913,
			"nilai_komparatif_unit": 1.0718170570763215,
			"summary_team": 0.9333333333333332
		}]
	},
	"750025": {
		"nik": "750025",
		"nama": "YUGO SUBARON",
		"band": "III",
		"posisi": "MGR BACKBONE & CME OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8733333333333322,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9618320610687037,
			"nilai_komparatif_unit": 0.9669289406160955,
			"summary_team": 0.8400000000000001
		}]
	},
	"750029": {
		"nik": "750029",
		"nama": "TARYOKO,ST, MM",
		"band": "III",
		"posisi": "MGR PARTNERSHIP MARKETING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8370370370370359,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9135866736074972,
			"nilai_komparatif_unit": 0.9184278942529246,
			"summary_team": 0.7647058823529411
		}]
	},
	"750038": {
		"nik": "750038",
		"nama": "ANAK AGUNG GDE PEMAYUN",
		"band": "III",
		"posisi": "MGR PERFORMANCE & WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8157894736842092,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9479569892473134,
			"nilai_komparatif_unit": 0.9529803428927858,
			"summary_team": 0.7733333333333333
		}]
	},
	"760009": {
		"nik": "760009",
		"nama": "GEDE JADIARTE",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8684210526315793,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9212121212121207,
			"nilai_komparatif_unit": 0.9260937501465923,
			"summary_team": 0.7999999999999999
		}]
	},
	"770004": {
		"nik": "770004",
		"nama": "WELLSON PATRESIUS",
		"band": "III",
		"posisi": "SENIOR AM ENTERPRISE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8754098360655743,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9595505617977519,
			"nilai_komparatif_unit": 0.9646353513686892,
			"summary_team": 0.8399999999999997
		}]
	},
	"770006": {
		"nik": "770006",
		"nama": "ROBERTH FERDINAND SINAY",
		"band": "III",
		"posisi": "KAKANDATEL DENPASAR CENTRUM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8754098360655743,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9747815230961293,
			"nilai_komparatif_unit": 0.979947023612637,
			"summary_team": 0.8533333333333334
		}]
	},
	"770010": {
		"nik": "770010",
		"nama": "MUHAMAD NATSIR ALAMSYAH H",
		"band": "III",
		"posisi": "MGR NETWORK AREA MAUMERE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606965174129362,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0379190751445078,
			"nilai_komparatif_unit": 1.0434191501784735,
			"summary_team": 0.8933333333333334
		}]
	},
	"770027": {
		"nik": "770027",
		"nama": "GEDE ARKA PUNIATMAJA",
		"band": "III",
		"posisi": "KAKANDATEL GIANYAR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.841333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0776545166402551,
			"nilai_komparatif_unit": 1.0833651552094405,
			"summary_team": 0.9066666666666666
		}]
	},
	"770033": {
		"nik": "770033",
		"nama": "YULITA WIJAYANTI, ST.",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8484848484848507,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.958571428571426,
			"nilai_komparatif_unit": 0.9636510295815766,
			"summary_team": 0.8133333333333332
		}]
	},
	"770045": {
		"nik": "770045",
		"nama": "AGUNG SUSILO",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8636815920398017,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9880184331797228,
			"nilai_komparatif_unit": 0.9932540778918822,
			"summary_team": 0.8533333333333334
		}]
	},
	"770053": {
		"nik": "770053",
		"nama": "ANNIF KUSTIYANINGSIH",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8596491228070169,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1012244897959191,
			"nilai_komparatif_unit": 1.1070600288742185,
			"summary_team": 0.9466666666666665
		}]
	},
	"770065": {
		"nik": "770065",
		"nama": "NI LUH PUTU PUTRI MAHARINI, ST.MM",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8754098360655743,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9519350811485637,
			"nilai_komparatif_unit": 0.9569795152467158,
			"summary_team": 0.8333333333333334
		}]
	},
	"770076": {
		"nik": "770076",
		"nama": "TATIK HERAWATI",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8376470588235311,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0101555747623145,
			"nilai_komparatif_unit": 1.015508527213254,
			"summary_team": 0.8461538461538463
		}]
	},
	"780001": {
		"nik": "780001",
		"nama": "ROBI CAHYONO",
		"band": "III",
		"posisi": "MGR PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8376470588235311,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.955056179775279,
			"nilai_komparatif_unit": 0.9601171530016217,
			"summary_team": 0.8
		}]
	},
	"780002": {
		"nik": "780002",
		"nama": "BUDI LEKSONO,S.KOM",
		"band": "III",
		"posisi": "MGR PLANNING, ENGINEERING & CAPEX MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8567901234567893,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.035842939481269,
			"nilai_komparatif_unit": 1.041332012788605,
			"summary_team": 0.8875
		}]
	},
	"780005": {
		"nik": "780005",
		"nama": "YULIANTO",
		"band": "III",
		"posisi": "MGR ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8807843137254925,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1050756901157583,
			"nilai_komparatif_unit": 1.110931637230905,
			"summary_team": 0.9733333333333332
		}]
	},
	"780020": {
		"nik": "780020",
		"nama": "YOHANES GERSON DJAMI",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606965174129362,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8984971098265887,
			"nilai_komparatif_unit": 0.9032583688112158,
			"summary_team": 0.7733333333333333
		}]
	},
	"780040": {
		"nik": "780040",
		"nama": "WIDYA MAHARANI",
		"band": "III",
		"posisi": "MGR CUSTOMER RELATIONSHIP MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8624999999999998,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0523968784838351,
			"nilai_komparatif_unit": 1.0579736733764105,
			"summary_team": 0.9076923076923076
		}]
	},
	"790007": {
		"nik": "790007",
		"nama": "HARTOYO, S.Kom.",
		"band": "III",
		"posisi": "MGR CCAN & WAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.871485943775102,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0062389223679526,
			"nilai_komparatif_unit": 1.0115711199425566,
			"summary_team": 0.8769230769230768
		}]
	},
	"790008": {
		"nik": "790008",
		"nama": "KURNIA TRININGSIH",
		"band": "III",
		"posisi": "MGR FINANCE & PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.871485943775102,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0415455512229685,
			"nilai_komparatif_unit": 1.0470648434493128,
			"summary_team": 0.9076923076923076
		}]
	},
	"790015": {
		"nik": "790015",
		"nama": "SARJONO",
		"band": "III",
		"posisi": "MGR ACCESS SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666677,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.011834319526626,
			"nilai_komparatif_unit": 1.0171961678755093,
			"summary_team": 0.8769230769230768
		}]
	},
	"790016": {
		"nik": "790016",
		"nama": "P. CH. DWI PRASETYO M.S.",
		"band": "III",
		"posisi": "KAKANDATEL BREBES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8478873239436632,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0064230343300096,
			"nilai_komparatif_unit": 1.0117562075390634,
			"summary_team": 0.8533333333333333
		}]
	},
	"790029": {
		"nik": "790029",
		"nama": "SUTRISNO TRI SUSILO",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8262626262626273,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.000488997555011,
			"nilai_komparatif_unit": 1.0057907254921759,
			"summary_team": 0.8266666666666667
		}]
	},
	"790032": {
		"nik": "790032",
		"nama": "MUHAMMAD LUKMAN HAKIM",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8684210526315793,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8905050505050501,
			"nilai_komparatif_unit": 0.8952239584750393,
			"summary_team": 0.7733333333333333
		}]
	},
	"790033": {
		"nik": "790033",
		"nama": "SIGIT WAHYU WIDHIYONO",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8636815920398017,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9262672811059901,
			"nilai_komparatif_unit": 0.9311756980236395,
			"summary_team": 0.8
		}]
	},
	"790044": {
		"nik": "790044",
		"nama": "DYAH SHINTA TRI WULANDARI",
		"band": "III",
		"posisi": "MGR PARTNERSHIP MARKETING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.859047619047618,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.993348115299336,
			"nilai_komparatif_unit": 0.9986120027254671,
			"summary_team": 0.8533333333333333
		}]
	},
	"790066": {
		"nik": "790066",
		"nama": "MIRA KOMALA DEWI",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0041154065058395,
			"nilai_komparatif_unit": 1.0094363512796642,
			"summary_team": 0.846153846153846
		}]
	},
	"790091": {
		"nik": "790091",
		"nama": "MIMIN DECA KURNIAWAN",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8765432098765426,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9529411764705888,
			"nilai_komparatif_unit": 0.9579909420053576,
			"summary_team": 0.8352941176470587
		}]
	},
	"790110": {
		"nik": "790110",
		"nama": "ANIK SRIASIH",
		"band": "III",
		"posisi": "MGR APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.859047619047618,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0554323725055446,
			"nilai_komparatif_unit": 1.0610252528958088,
			"summary_team": 0.9066666666666667
		}]
	},
	"790120": {
		"nik": "790120",
		"nama": "DEWI SETIAWATI SUKARNO",
		"band": "III",
		"posisi": "MGR GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8807843137254925,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0445235975066758,
			"nilai_komparatif_unit": 1.0500586708072939,
			"summary_team": 0.9199999999999999
		}]
	},
	"800001": {
		"nik": "800001",
		"nama": "AFIF EKO HARTARTO, S.Kom.",
		"band": "III",
		"posisi": "MGR TOP PRIORITY SOLUTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564102564102553,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0008554319931577,
			"nilai_komparatif_unit": 1.0061591017164915,
			"summary_team": 0.857142857142857
		}]
	},
	"800002": {
		"nik": "800002",
		"nama": "DANI ARUMANEGARA , S.KOM",
		"band": "III",
		"posisi": "MGR ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8564102564102553,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9341317365269473,
			"nilai_komparatif_unit": 0.9390818282687255,
			"summary_team": 0.7999999999999999
		}]
	},
	"800022": {
		"nik": "800022",
		"nama": "SITI KHOLIFAH",
		"band": "III",
		"posisi": "MGR QUALITY & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8157894736842092,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0671726755218234,
			"nilai_komparatif_unit": 1.0728277693823451,
			"summary_team": 0.8705882352941177
		}]
	},
	"800028": {
		"nik": "800028",
		"nama": "AMIN SUYONO, S.Kom.",
		"band": "III",
		"posisi": "MGR ACCESS NETWORK ELEMENT OM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606060606060602,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9760563380281696,
			"nilai_komparatif_unit": 0.981228593963234,
			"summary_team": 0.8400000000000001
		}]
	},
	"800029": {
		"nik": "800029",
		"nama": "EVA MARYAM SANI",
		"band": "III",
		"posisi": "MGR PERFORMANCE & WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769239,
			"nilai_komparatif_unit": 0.9279684338513439,
			"summary_team": 0.8
		}]
	},
	"800030": {
		"nik": "800030",
		"nama": "TRIAWAN SETIADI, SE",
		"band": "III",
		"posisi": "KAKANDATEL PEMALANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8478873239436632,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9979555328392522,
			"nilai_komparatif_unit": 1.0032438356006337,
			"summary_team": 0.846153846153846
		}]
	},
	"800034": {
		"nik": "800034",
		"nama": "RENNY AGUNG LESTARIANI",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8636815920398017,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9725806451612896,
			"nilai_komparatif_unit": 0.9777344829248216,
			"summary_team": 0.8400000000000001
		}]
	},
	"800075": {
		"nik": "800075",
		"nama": "BUDHI SUPRIYONO",
		"band": "III",
		"posisi": "MGR SERVICE DELIVERY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9493454752418845,
			"nilai_komparatif_unit": 0.9543761866644097,
			"summary_team": 0.7999999999999998
		}]
	},
	"800088": {
		"nik": "800088",
		"nama": "INDAH ARFIYATI KURNIA",
		"band": "III",
		"posisi": "SENIOR AM BUSINESS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9606472070900024,
			"nilai_komparatif_unit": 0.9657378079342244,
			"summary_team": 0.8095238095238095
		}]
	},
	"800099": {
		"nik": "800099",
		"nama": "MARTA DAMAYANTI",
		"band": "III",
		"posisi": "MGR COLLECTION PARTNERSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.004524886877829,
			"nilai_komparatif_unit": 1.0098480015441094,
			"summary_team": 0.8705882352941177
		}]
	},
	"810001": {
		"nik": "810001",
		"nama": "HUSNA MAULIDA , S.KOM",
		"band": "III",
		"posisi": "MGR APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8370370370370359,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0119729307652277,
			"nilai_komparatif_unit": 1.0173355136340088,
			"summary_team": 0.8470588235294116
		}]
	},
	"810014": {
		"nik": "810014",
		"nama": "DEWI ARIANI, S.KOM.",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8478873239436632,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9435215946843841,
			"nilai_komparatif_unit": 0.948521444567872,
			"summary_team": 0.8
		}]
	},
	"810016": {
		"nik": "810016",
		"nama": "ADHI WIDYANTO",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8478873239436632,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0142857142857131,
			"nilai_komparatif_unit": 1.0196605529104625,
			"summary_team": 0.8600000000000002
		}]
	},
	"810018": {
		"nik": "810018",
		"nama": "AHMAD MUJIB",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8596491228070169,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9485086342229206,
			"nilai_komparatif_unit": 0.9535349111105134,
			"summary_team": 0.8153846153846154
		}]
	},
	"810022": {
		"nik": "810022",
		"nama": "DWI PUSPITA INDAH ARI WIJAYANTI",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8636815920398017,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9797057780928741,
			"nilai_komparatif_unit": 0.9848973729096188,
			"summary_team": 0.8461538461538461
		}]
	},
	"810072": {
		"nik": "810072",
		"nama": "ADITA KRISMAWATI HADISWARA",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666677,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9408284023668626,
			"nilai_komparatif_unit": 0.9458139806561752,
			"summary_team": 0.8153846153846153
		}]
	},
	"810085": {
		"nik": "810085",
		"nama": "YULIA DWI MARTINI",
		"band": "III",
		"posisi": "MGR WHOLESALE SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8765432098765426,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9778672032193164,
			"nilai_komparatif_unit": 0.9830490551765239,
			"summary_team": 0.857142857142857
		}]
	},
	"810094": {
		"nik": "810094",
		"nama": "ACHMAD YUSUF",
		"band": "III",
		"posisi": "MGR GOVERNMENT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8484848484848507,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9742857142857115,
			"nilai_komparatif_unit": 0.9794485874435696,
			"summary_team": 0.8266666666666665
		}]
	},
	"810095": {
		"nik": "810095",
		"nama": "FERRY HASCARYANTO",
		"band": "III",
		"posisi": "MGR ACCESS SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8478873239436632,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0064230343300096,
			"nilai_komparatif_unit": 1.0117562075390634,
			"summary_team": 0.8533333333333333
		}]
	},
	"810113": {
		"nik": "810113",
		"nama": "ANNASTASIA KURNIAWATI SUKARNO",
		"band": "III",
		"posisi": "SENIOR AM ENTERPRISE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8777777777777779,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.005299136672288,
			"summary_team": 0.8777777777777777
		}]
	},
	"810120": {
		"nik": "810120",
		"nama": "RAMA KUMALA SARI",
		"band": "III",
		"posisi": "MGR SAS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8596491228070174,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9664050235478807,
			"nilai_komparatif_unit": 0.9715261358484468,
			"summary_team": 0.8307692307692307
		}]
	},
	"820005": {
		"nik": "820005",
		"nama": "DIAH ERNAWATI HARIMURNI",
		"band": "III",
		"posisi": "KAKANDATEL MUNTILAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.871485943775102,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8873732718893993,
			"nilai_komparatif_unit": 0.8920755841364768,
			"summary_team": 0.7733333333333333
		}]
	},
	"820016": {
		"nik": "820016",
		"nama": "NUGROHO ADI PRACOYO",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0340073529411764,
			"nilai_komparatif_unit": 1.0394866992245626,
			"summary_team": 0.8823529411764706
		}]
	},
	"830010": {
		"nik": "830010",
		"nama": "PRAMUHADIANTO",
		"band": "III",
		"posisi": "MGR TELKOMGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8805555555555551,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9802424041175497,
			"nilai_komparatif_unit": 0.985436842588941,
			"summary_team": 0.8631578947368419
		}]
	},
	"830015": {
		"nik": "830015",
		"nama": "PAMUJI SANTASA",
		"band": "III",
		"posisi": "MGR CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8624999999999998,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9275362318840581,
			"nilai_komparatif_unit": 0.932451373145311,
			"summary_team": 0.7999999999999999
		}]
	},
	"830017": {
		"nik": "830017",
		"nama": "TRI ASTUTI RETNO ANGGRAINI",
		"band": "III",
		"posisi": "KAKANDATEL BLORA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666677,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9763313609467442,
			"nilai_komparatif_unit": 0.9815050742658422,
			"summary_team": 0.846153846153846
		}]
	},
	"830018": {
		"nik": "830018",
		"nama": "AKHMAD SYARIFUDIN",
		"band": "III",
		"posisi": "MGR CCAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8632478632478621,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9801980198019815,
			"nilai_komparatif_unit": 0.9853922230748186,
			"summary_team": 0.8461538461538461
		}]
	},
	"830019": {
		"nik": "830019",
		"nama": "NORA ARI RAHMAWATI",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666677,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1043956043956027,
			"nilai_komparatif_unit": 1.1102479476435694,
			"summary_team": 0.9571428571428569
		}]
	},
	"830041": {
		"nik": "830041",
		"nama": "ANITA",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8478873239436632,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9435215946843838,
			"nilai_komparatif_unit": 0.9485214445678717,
			"summary_team": 0.7999999999999998
		}]
	},
	"830044": {
		"nik": "830044",
		"nama": "VANDY AULIA ABKHARI",
		"band": "III",
		"posisi": "MGR MARKETING & ACCOUNT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8292397660818702,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9789264083630644,
			"nilai_komparatif_unit": 0.9841138731930926,
			"summary_team": 0.8117647058823529
		}]
	},
	"830078": {
		"nik": "830078",
		"nama": "RIZKY ARSITA",
		"band": "III",
		"posisi": "MGR PAYMENT COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8484848484848507,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0335164835164807,
			"nilai_komparatif_unit": 1.0389932286156973,
			"summary_team": 0.8769230769230769
		}]
	},
	"830085": {
		"nik": "830085",
		"nama": "CITRA VIOLITA",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481474,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0702049042660406,
			"nilai_komparatif_unit": 1.0758760663210996,
			"summary_team": 0.9076923076923077
		}]
	},
	"830105": {
		"nik": "830105",
		"nama": "RUSTAM EFENDI",
		"band": "III",
		"posisi": "MGR LOGISTIK & GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8596491228070169,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9461224489795926,
			"nilai_komparatif_unit": 0.9511360811454556,
			"summary_team": 0.8133333333333334
		}]
	},
	"830107": {
		"nik": "830107",
		"nama": "DIAN ANNISA",
		"band": "III",
		"posisi": "SENIOR STAFF III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8157894736842092,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.047507331378301,
			"nilai_komparatif_unit": 1.0530582158924986,
			"summary_team": 0.8545454545454546
		}]
	},
	"830116": {
		"nik": "830116",
		"nama": "BAMBANG SUPRIYANTO",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9375,
			"nilai_komparatif_unit": 0.9424679406302703,
			"summary_team": 0.7999999999999999
		}]
	},
	"830117": {
		"nik": "830117",
		"nama": "IKKA NOVITASARI",
		"band": "III",
		"posisi": "MGR BIDDING MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8320512820512806,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.009553158705703,
			"nilai_komparatif_unit": 1.0149029188716248,
			"summary_team": 0.8400000000000001
		}]
	},
	"830123": {
		"nik": "830123",
		"nama": "I MADE DHARMA PUTRA",
		"band": "III",
		"posisi": "MGR BUSINESS, GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.841333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9984152139461188,
			"nilai_komparatif_unit": 1.0037059526205112,
			"summary_team": 0.84
		}]
	},
	"840004": {
		"nik": "840004",
		"nama": "FAJAR ADI NUGROHO",
		"band": "III",
		"posisi": "MGR ACCESS OPTIMA & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8682170542635647,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.028928571428573,
			"nilai_komparatif_unit": 1.0343810045545954,
			"summary_team": 0.8933333333333333
		}]
	},
	"840010": {
		"nik": "840010",
		"nama": "DONA ARDHY GUSEMA",
		"band": "III",
		"posisi": "KAKANDATEL PURWODADI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666677,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139849,
			"nilai_komparatif_unit": 0.9912390088866607,
			"summary_team": 0.8545454545454546
		}]
	},
	"840013": {
		"nik": "840013",
		"nama": "YOGI ANGGUN SALOKO YUDO",
		"band": "III",
		"posisi": "MGR GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.060102447353438,
			"nilai_komparatif_unit": 1.0657200751085911,
			"summary_team": 0.8933333333333333
		}]
	},
	"840022": {
		"nik": "840022",
		"nama": "ARZAD IWANTORO",
		"band": "III",
		"posisi": "MGR ACCESS NETWORK ELEMENT OM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8773333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0346036941781633,
			"nilai_komparatif_unit": 1.0400862005552678,
			"summary_team": 0.9076923076923078
		}]
	},
	"840063": {
		"nik": "840063",
		"nama": "SUMARNO HADI",
		"band": "III",
		"posisi": "MGR ACCESS SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.871485943775102,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.055668202764975,
			"nilai_komparatif_unit": 1.0612623328520157,
			"summary_team": 0.92
		}]
	},
	"840096": {
		"nik": "840096",
		"nama": "MOCHAMAD SUKMA PRADHONO",
		"band": "III",
		"posisi": "MGR NTE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8773333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9644610708440504,
			"nilai_komparatif_unit": 0.9695718818735545,
			"summary_team": 0.8461538461538461
		}]
	},
	"840123": {
		"nik": "840123",
		"nama": "ERLANGGA",
		"band": "III",
		"posisi": "MGR OLO FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.880555555555555,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9388012618296536,
			"nilai_komparatif_unit": 0.9437760980242056,
			"summary_team": 0.8266666666666667
		}]
	},
	"840125": {
		"nik": "840125",
		"nama": "ANGGAR PRASETYONINGRUM",
		"band": "III",
		"posisi": "MGR HR & CDC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.871485943775102,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0591988656504767,
			"nilai_komparatif_unit": 1.0648117052026913,
			"summary_team": 0.9230769230769231
		}]
	},
	"840140": {
		"nik": "840140",
		"nama": "HANDIKA RAHMAWAN AJI",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8807843137254925,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0142475512021345,
			"nilai_komparatif_unit": 1.0196221875954883,
			"summary_team": 0.8933333333333333
		}]
	},
	"840156": {
		"nik": "840156",
		"nama": "JAYANTI SUKMA MAULANI",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.871485943775102,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9944700460829475,
			"nilai_komparatif_unit": 0.9997398787736379,
			"summary_team": 0.8666666666666667
		}]
	},
	"840162": {
		"nik": "840162",
		"nama": "IRMA NUR RAHMAWATI",
		"band": "III",
		"posisi": "KAKANDATEL BOJONEGORO",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481474,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.021834061135372,
			"nilai_komparatif_unit": 1.0272488994817277,
			"summary_team": 0.8666666666666667
		}]
	},
	"840164": {
		"nik": "840164",
		"nama": "DIAH AYU TRISMIATI",
		"band": "III",
		"posisi": "MGR GOVT & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8754098360655743,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9328963795255926,
			"nilai_komparatif_unit": 0.9378399249417816,
			"summary_team": 0.8166666666666668
		}]
	},
	"840168": {
		"nik": "840168",
		"nama": "MUHAMMAD JAUHAR ARIBI",
		"band": "III",
		"posisi": "MGR PERFORMANCE MGT & SLA MONITORING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8619047619047607,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9900552486187858,
			"nilai_komparatif_unit": 0.995301686694333,
			"summary_team": 0.8533333333333333
		}]
	},
	"850006": {
		"nik": "850006",
		"nama": "EKO AGUS MAKHRUS",
		"band": "III",
		"posisi": "KAKANDATEL SUMBAWA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8684210526315793,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0629370629370625,
			"nilai_komparatif_unit": 1.0685697117076067,
			"summary_team": 0.9230769230769231
		}]
	},
	"850081": {
		"nik": "850081",
		"nama": "I WAYAN SURYAWAN",
		"band": "III",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.841333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9874436181884692,
			"nilai_komparatif_unit": 0.9926762168774287,
			"summary_team": 0.8307692307692308
		}]
	},
	"850099": {
		"nik": "850099",
		"nama": "RENI WULANSARI",
		"band": "III",
		"posisi": "SENIOR AM ENTERPRISE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8320512820512806,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.045608628659478,
			"nilai_komparatif_unit": 1.0511494516884683,
			"summary_team": 0.8700000000000001
		}]
	},
	"850100": {
		"nik": "850100",
		"nama": "FITRILIANI HAYU PUSPASARI",
		"band": "III",
		"posisi": "MGR ACCESS OM & PARTNERSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606060606060602,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9832069339111597,
			"nilai_komparatif_unit": 0.9884170818310963,
			"summary_team": 0.846153846153846
		}]
	},
	"850116": {
		"nik": "850116",
		"nama": "I. B. PUTU SANDHI YUDISTIRA",
		"band": "III",
		"posisi": "MGR BUSINESS SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8754098360655743,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0280898876404487,
			"nilai_komparatif_unit": 1.033537876466453,
			"summary_team": 0.9
		}]
	},
	"850121": {
		"nik": "850121",
		"nama": "RIAS SUKMAWATI",
		"band": "III",
		"posisi": "MGR CONSUMER SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8376470588235311,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.955056179775279,
			"nilai_komparatif_unit": 0.9601171530016217,
			"summary_team": 0.8
		}]
	},
	"850128": {
		"nik": "850128",
		"nama": "MONICA AFRISIA DINI KRISTIANI",
		"band": "III",
		"posisi": "MGR WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.871485943775102,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9885856079404448,
			"nilai_komparatif_unit": 0.9938242581891784,
			"summary_team": 0.8615384615384616
		}]
	},
	"850159": {
		"nik": "850159",
		"nama": "MYTA IRWANTI",
		"band": "III",
		"posisi": "MGR HOME SERVICE 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8426858513189491,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.964178998292539,
			"nilai_komparatif_unit": 0.9692883145810411,
			"summary_team": 0.8124999999999998
		}]
	},
	"860078": {
		"nik": "860078",
		"nama": "NORINA YUNIARTI",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8376470588235311,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.955056179775279,
			"nilai_komparatif_unit": 0.9601171530016217,
			"summary_team": 0.8
		}]
	},
	"860150": {
		"nik": "860150",
		"nama": "IMRON NAJIB ERLANGGA",
		"band": "III",
		"posisi": "MGR CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333321,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9600000000000014,
			"nilai_komparatif_unit": 0.9650871712053981,
			"summary_team": 0.8
		}]
	},
	"870036": {
		"nik": "870036",
		"nama": "I PUTU  AGUS PICASTANA",
		"band": "III",
		"posisi": "MGR SOLUTION & SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8320512820512806,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9935285053929138,
			"nilai_komparatif_unit": 0.9987933487308052,
			"summary_team": 0.8266666666666667
		}]
	},
	"880035": {
		"nik": "880035",
		"nama": "SRY HAZZAYANI BR BANGUN",
		"band": "III",
		"posisi": "MGR SALES PROMOTION & PRICING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8416666666666656,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9636963696369649,
			"nilai_komparatif_unit": 0.9688031284102592,
			"summary_team": 0.8111111111111111
		}]
	},
	"900010": {
		"nik": "900010",
		"nama": "JOSIA PRANANTA TARIGAN",
		"band": "III",
		"posisi": "MGR PERFORMANCE & WAR ROOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8466666666666655,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0292463442069752,
			"nilai_komparatif_unit": 1.034700461254381,
			"summary_team": 0.8714285714285712
		}]
	},
	"900086": {
		"nik": "900086",
		"nama": "ARIEF RIFQI PUTRANTO",
		"band": "III",
		"posisi": "KAKANDATEL BLITAR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8636815920398017,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9618929457639128,
			"nilai_komparatif_unit": 0.9669901479476257,
			"summary_team": 0.8307692307692308
		}]
	}
};