export const cbark17 = {
	"635653": {
		"nik": "635653",
		"nama": "DEDI SUKIADI",
		"band": "IV",
		"posisi": "SR STAFF REHIRE IV BUSS TERITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.847222222222222,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8154992548435174,
			"nilai_komparatif_unit": 0.8212141863932698,
			"summary_team": 0.6909090909090909
		}]
	},
	"645222": {
		"nik": "645222",
		"nama": "DIDI MULYADI",
		"band": "IV",
		"posisi": "SENIOR STAFF IV",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666663,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139866,
			"nilai_komparatif_unit": 0.9929238665610262,
			"summary_team": 0.8545454545454546
		}]
	},
	"650709": {
		"nik": "650709",
		"nama": "DEWA NYOMAN ARDIKA",
		"band": "IV",
		"posisi": "ASMAN CONSUMER ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8390804597701139,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9534246575342478,
			"nilai_komparatif_unit": 0.9601061555531515,
			"summary_team": 0.8
		}]
	},
	"660007": {
		"nik": "660007",
		"nama": "UUNG HARUN",
		"band": "IV",
		"posisi": "ASMAN DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8619047619047618,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9281767955801106,
			"nilai_komparatif_unit": 0.934681359283025,
			"summary_team": 0.8
		}]
	},
	"660040": {
		"nik": "660040",
		"nama": "WISNU SULISTYARTO",
		"band": "IV",
		"posisi": "ASMAN WIFI SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8592592592592582,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9892241379310358,
			"nilai_komparatif_unit": 0.9961565148793429,
			"summary_team": 0.8500000000000001
		}]
	},
	"660056": {
		"nik": "660056",
		"nama": "TUTY MAGDALENA SITUMORANG",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.807843137254902,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9141150112023899,
			"nilai_komparatif_unit": 0.920521031424475,
			"summary_team": 0.7384615384615385
		}]
	},
	"660140": {
		"nik": "660140",
		"nama": "HERLINA WIDIANI",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION EBIS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8829268292682917,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9884480160723267,
			"nilai_komparatif_unit": 0.9953749540416641,
			"summary_team": 0.8727272727272728
		}]
	},
	"660492": {
		"nik": "660492",
		"nama": "WISNU WARDANI",
		"band": "IV",
		"posisi": "ASMAN NTE MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8227642276422753,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8601398601398613,
			"nilai_komparatif_unit": 0.8661676282766405,
			"summary_team": 0.7076923076923077
		}]
	},
	"660510": {
		"nik": "660510",
		"nama": "WALUYA",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8450450450450468,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0112424888544271,
			"nilai_komparatif_unit": 1.018329168050858,
			"summary_team": 0.8545454545454546
		}]
	},
	"660516": {
		"nik": "660516",
		"nama": "SUDARWIYADI",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE DMG",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8000000000000005,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9772727272727266,
			"nilai_komparatif_unit": 0.9841213500134981,
			"summary_team": 0.7818181818181817
		}]
	},
	"660520": {
		"nik": "660520",
		"nama": "ATIM",
		"band": "IV",
		"posisi": "ASMAN SALES SUPPORT 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8448087431693994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.041655886157826,
			"nilai_komparatif_unit": 1.0489556991894529,
			"summary_team": 0.88
		}]
	},
	"660545": {
		"nik": "660545",
		"nama": "SITA KASMIRUN",
		"band": "IV",
		"posisi": "ASMAN DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8227642276422753,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9723320158102781,
			"nilai_komparatif_unit": 0.9791460145735937,
			"summary_team": 0.8
		}]
	},
	"660615": {
		"nik": "660615",
		"nama": "SUHERMAN",
		"band": "IV",
		"posisi": "ASMAN ADVALJAR OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8192592592592577,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9764918625678138,
			"nilai_komparatif_unit": 0.9833350130922569,
			"summary_team": 0.8
		}]
	},
	"660639": {
		"nik": "660639",
		"nama": "IRWAN",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666663,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9692307692307697,
			"nilai_komparatif_unit": 0.9760230347897746,
			"summary_team": 0.84
		}]
	},
	"670022": {
		"nik": "670022",
		"nama": "RITA RACHMAWATI",
		"band": "IV",
		"posisi": "ASMAN COMMUNICATION & SECRETARIATE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.807843137254902,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1478375992939098,
			"nilai_komparatif_unit": 1.1558815224136871,
			"summary_team": 0.9272727272727272
		}]
	},
	"670352": {
		"nik": "670352",
		"nama": "SUHARTO",
		"band": "IV",
		"posisi": "ASMAN BS DELIVERY & QUALITY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8453333333333325,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9463722397476351,
			"nilai_komparatif_unit": 0.9530043151770374,
			"summary_team": 0.8
		}]
	},
	"670359": {
		"nik": "670359",
		"nama": "ISKANDAR",
		"band": "IV",
		"posisi": "ASMAN WIFI FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8370370370370367,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.955752212389381,
			"nilai_komparatif_unit": 0.9624500216637474,
			"summary_team": 0.8
		}]
	},
	"670360": {
		"nik": "670360",
		"nama": "ABDUL RACHMAN",
		"band": "IV",
		"posisi": "ASMAN DAVAL & INVENTORY MGT PL & REVIEW",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7897435897435889,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.012987012987014,
			"nilai_komparatif_unit": 1.0200859176219665,
			"summary_team": 0.8
		}]
	},
	"670363": {
		"nik": "670363",
		"nama": "AHMAD IRIANTO",
		"band": "IV",
		"posisi": "ASMAN LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8487179487179478,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0282889316121957,
			"nilai_komparatif_unit": 1.0354950704561354,
			"summary_team": 0.8727272727272728
		}]
	},
	"670365": {
		"nik": "670365",
		"nama": "BUDIYANTO",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8296296296296299,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.964285714285714,
			"nilai_komparatif_unit": 0.9710433254286015,
			"summary_team": 0.8
		}]
	},
	"670366": {
		"nik": "670366",
		"nama": "CHOIRUL ISFANDI",
		"band": "IV",
		"posisi": "ASMAN PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8487179487179478,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0054380664652578,
			"nilai_komparatif_unit": 1.0124840688904433,
			"summary_team": 0.8533333333333333
		}]
	},
	"670369": {
		"nik": "670369",
		"nama": "DEDY ROESHADI, SE",
		"band": "IV",
		"posisi": "ASMAN OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8464912280701777,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9765803108808263,
			"nilai_komparatif_unit": 0.9834240812415863,
			"summary_team": 0.8266666666666667
		}]
	},
	"670372": {
		"nik": "670372",
		"nama": "JASMANTO",
		"band": "IV",
		"posisi": "ASMAN PLAN, DEPL, SOL PARTNER & INST REL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8390804597701139,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9534246575342478,
			"nilai_komparatif_unit": 0.9601061555531515,
			"summary_team": 0.8
		}]
	},
	"670374": {
		"nik": "670374",
		"nama": "MUHAMMAD NURYADIN",
		"band": "IV",
		"posisi": "ASMAN NTE MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8192592592592577,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9013771039087513,
			"nilai_komparatif_unit": 0.9076938582390064,
			"summary_team": 0.7384615384615385
		}]
	},
	"670375": {
		"nik": "670375",
		"nama": "MOCHAMAD SARIF",
		"band": "IV",
		"posisi": "ASMAN DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8571428571428562,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9955555555555567,
			"nilai_komparatif_unit": 1.0025323024013495,
			"summary_team": 0.8533333333333334
		}]
	},
	"670379": {
		"nik": "670379",
		"nama": "PURWITO",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8464912280701777,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0395854922279764,
			"nilai_komparatif_unit": 1.0468707961603982,
			"summary_team": 0.88
		}]
	},
	"670389": {
		"nik": "670389",
		"nama": "ACHMAD SUHELMI",
		"band": "IV",
		"posisi": "ASMAN BS SALES ENGINEER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8464912280701777,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0095148374941094,
			"nilai_komparatif_unit": 1.0165894094946017,
			"summary_team": 0.8545454545454546
		}]
	},
	"670392": {
		"nik": "670392",
		"nama": "DEDI SETIAWAN",
		"band": "IV",
		"posisi": "ASMAN BGES SALES ENGINEER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8450450450450468,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9102837461046396,
			"nilai_komparatif_unit": 0.9166629172307229,
			"summary_team": 0.7692307692307692
		}]
	},
	"670403": {
		"nik": "670403",
		"nama": "SUTRISNO",
		"band": "IV",
		"posisi": "ASMAN COMMUNICATION & SECRETARIATE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.842105263157894,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9500000000000008,
			"nilai_komparatif_unit": 0.956657498385216,
			"summary_team": 0.8
		}]
	},
	"670404": {
		"nik": "670404",
		"nama": "ZAENAL ABIDIN",
		"band": "IV",
		"posisi": "ASMAN NETWORK PERF, QM & ADMIN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8464912280701777,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.035950577919487,
			"nilai_komparatif_unit": 1.0432104087612362,
			"summary_team": 0.8769230769230769
		}]
	},
	"670412": {
		"nik": "670412",
		"nama": "SULISTYANTA RACHMAT WIBAWA",
		"band": "IV",
		"posisi": "ASMAN BS TERRITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8457516339869264,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1350850077279773,
			"nilai_komparatif_unit": 1.143039562050115,
			"summary_team": 0.9599999999999999
		}]
	},
	"670413": {
		"nik": "670413",
		"nama": "LUKMAN",
		"band": "IV",
		"posisi": "ASMAN WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8611111111111102,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9290322580645172,
			"nilai_komparatif_unit": 0.9355428167570196,
			"summary_team": 0.8
		}]
	},
	"670418": {
		"nik": "670418",
		"nama": "SYAHRIZAL",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SERVICE 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8799999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9393939393939394,
			"nilai_komparatif_unit": 0.9459771116408826,
			"summary_team": 0.8266666666666667
		}]
	},
	"670424": {
		"nik": "670424",
		"nama": "BAMBANG SUPRIYADI",
		"band": "IV",
		"posisi": "OFF 1 COMMAND CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0472727272727271,
			"nilai_komparatif_unit": 1.0546119025260934,
			"summary_team": 0.8727272727272727
		}]
	},
	"670437": {
		"nik": "670437",
		"nama": "SUHERMAN",
		"band": "IV",
		"posisi": "ASMAN ACCESS PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8464912280701777,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9665567593028708,
			"nilai_komparatif_unit": 0.9733302856863209,
			"summary_team": 0.8181818181818182
		}]
	},
	"670462": {
		"nik": "670462",
		"nama": "ENDRO SUGIRI",
		"band": "IV",
		"posisi": "ASMAN DAVAL & INVENTORY MGT PL & REVIEW",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8464912280701777,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0080829015544013,
			"nilai_komparatif_unit": 1.0151474387009922,
			"summary_team": 0.8533333333333333
		}]
	},
	"670473": {
		"nik": "670473",
		"nama": "TRIYONO HARDJO",
		"band": "IV",
		"posisi": "ASMAN BGES ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9769736842105261,
			"nilai_komparatif_unit": 0.9838202112895043,
			"summary_team": 0.8250000000000002
		}]
	},
	"670480": {
		"nik": "670480",
		"nama": "NASONI",
		"band": "IV",
		"posisi": "ASMAN DAVAL & INVENTORY MGT PL & REVIEW",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8390804597701139,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8884184308841854,
			"nilai_komparatif_unit": 0.894644372219982,
			"summary_team": 0.7454545454545455
		}]
	},
	"670494": {
		"nik": "670494",
		"nama": "BUDI JANUARSYAH",
		"band": "IV",
		"posisi": "ASMAN BS TERRITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.84102564102564,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9878048780487817,
			"nilai_komparatif_unit": 0.9947273089756421,
			"summary_team": 0.8307692307692308
		}]
	},
	"670519": {
		"nik": "670519",
		"nama": "SUCOKO",
		"band": "IV",
		"posisi": "ASMAN LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559453,
			"nilai_komparatif_unit": 0.9506717871328981,
			"summary_team": 0.8181818181818182
		}]
	},
	"670521": {
		"nik": "670521",
		"nama": "SUTARDI",
		"band": "IV",
		"posisi": "ASMAN SALES SUPPORT 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.851141552511417,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.065236051502144,
			"nilai_komparatif_unit": 1.0727011118103003,
			"summary_team": 0.9066666666666666
		}]
	},
	"670536": {
		"nik": "670536",
		"nama": "CATUR HARTO SANTOSO",
		"band": "IV",
		"posisi": "ASMAN DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8000000000000005,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9090909090909085,
			"nilai_komparatif_unit": 0.915461720942789,
			"summary_team": 0.7272727272727273
		}]
	},
	"670546": {
		"nik": "670546",
		"nama": "EDY MUHAMAD",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8611111111111102,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0683870967741949,
			"nilai_komparatif_unit": 1.0758742392705727,
			"summary_team": 0.92
		}]
	},
	"670552": {
		"nik": "670552",
		"nama": "GAWAT ASHAR SANTOSO",
		"band": "IV",
		"posisi": "ASMAN LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8245614035087718,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0025531914893617,
			"nilai_komparatif_unit": 1.009578977019291,
			"summary_team": 0.8266666666666667
		}]
	},
	"670553": {
		"nik": "670553",
		"nama": "SUKARI",
		"band": "IV",
		"posisi": "ASMAN DEBT MGT & COLLECTION PARTNERSHIP",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8773333333333325,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9346504559270526,
			"nilai_komparatif_unit": 0.9412003863492368,
			"summary_team": 0.8200000000000001
		}]
	},
	"670555": {
		"nik": "670555",
		"nama": "TJEP SUHENDI",
		"band": "IV",
		"posisi": "ASMAN IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333323,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0080000000000013,
			"nilai_komparatif_unit": 1.0150639561813664,
			"summary_team": 0.84
		}]
	},
	"670578": {
		"nik": "670578",
		"nama": "NARGI TOHIM",
		"band": "IV",
		"posisi": "MGR ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8597701149425278,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0020567667626503,
			"nilai_komparatif_unit": 1.0090790734011936,
			"summary_team": 0.8615384615384616
		}]
	},
	"670585": {
		"nik": "670585",
		"nama": "DWIPA RAYA",
		"band": "IV",
		"posisi": "ASMAN NE FTM ACCESS MTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8200000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0487804878048779,
			"nilai_komparatif_unit": 1.056130229282779,
			"summary_team": 0.8600000000000001
		}]
	},
	"670589": {
		"nik": "670589",
		"nama": "EDI SARJONO",
		"band": "IV",
		"posisi": "ASMAN SECURITY & SAFETY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0472727272727267,
			"nilai_komparatif_unit": 1.054611902526093,
			"summary_team": 0.8727272727272727
		}]
	},
	"670596": {
		"nik": "670596",
		"nama": "I DEWA PUTU SUREADNYANA",
		"band": "IV",
		"posisi": "ASMAN BGES ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9735576923076921,
			"nilai_komparatif_unit": 0.9803802804807996,
			"summary_team": 0.8307692307692308
		}]
	},
	"670597": {
		"nik": "670597",
		"nama": "DWI INDRIANTO",
		"band": "IV",
		"posisi": "MGR SECURITY & SAFETY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8656084656084662,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.016625916870415,
			"nilai_komparatif_unit": 1.0237503225545546,
			"summary_team": 0.88
		}]
	},
	"680118": {
		"nik": "680118",
		"nama": "WIWIK RUSDIYANTI",
		"band": "IV",
		"posisi": "ASMAN FINANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8684684684684674,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0049038098830643,
			"nilai_komparatif_unit": 1.0119460682952675,
			"summary_team": 0.8727272727272728
		}]
	},
	"680184": {
		"nik": "680184",
		"nama": "BUDIYONO",
		"band": "IV",
		"posisi": "ASMAN DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8487179487179478,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9425981873111793,
			"nilai_komparatif_unit": 0.9492038145847908,
			"summary_team": 0.8
		}]
	},
	"680186": {
		"nik": "680186",
		"nama": "MINTARTO",
		"band": "IV",
		"posisi": "ASMAN SERVICE AREA PGG",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.851141552511417,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0025751072961357,
			"nilai_komparatif_unit": 1.0096010464096945,
			"summary_team": 0.8533333333333334
		}]
	},
	"680187": {
		"nik": "680187",
		"nama": "HIDAYAT",
		"band": "IV",
		"posisi": "ASMAN CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8611111111111102,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9826302729528547,
			"nilai_komparatif_unit": 0.9895164408006937,
			"summary_team": 0.8461538461538461
		}]
	},
	"680198": {
		"nik": "680198",
		"nama": "SULISTIYO",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE BEKASI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8450450450450468,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.899360341151384,
			"nilai_komparatif_unit": 0.9056629622239544,
			"summary_team": 0.76
		}]
	},
	"680210": {
		"nik": "680210",
		"nama": "MOCHAMAD ASMA'IN",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE KEB BARU",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8676190476190493,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9835345773874844,
			"nilai_komparatif_unit": 0.9904270825040743,
			"summary_team": 0.8533333333333334
		}]
	},
	"680216": {
		"nik": "680216",
		"nama": "AMALIA",
		"band": "IV",
		"posisi": "ASMAN HUMAN CAPITAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8450450450450468,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0013121207151034,
			"nilai_komparatif_unit": 1.0083292089537952,
			"summary_team": 0.846153846153846
		}]
	},
	"680218": {
		"nik": "680218",
		"nama": "ASMAJI",
		"band": "IV",
		"posisi": "MGR HOME SERVICE 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8448087431693994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9469598965071147,
			"nilai_komparatif_unit": 0.95359609017223,
			"summary_team": 0.8
		}]
	},
	"680224": {
		"nik": "680224",
		"nama": "DONY HARMAN",
		"band": "IV",
		"posisi": "ASMAN BGES FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9374999999999998,
			"nilai_komparatif_unit": 0.9440698997222515,
			"summary_team": 0.8
		}]
	},
	"680226": {
		"nik": "680226",
		"nama": "EDY SUSANTO",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE KRANGGAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8507246376811579,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0814310051107343,
			"nilai_komparatif_unit": 1.0890095579215198,
			"summary_team": 0.9199999999999999
		}]
	},
	"680234": {
		"nik": "680234",
		"nama": "MUKHSIN",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9763313609467468,
			"nilai_komparatif_unit": 0.9831733866929971,
			"summary_team": 0.8461538461538461
		}]
	},
	"680243": {
		"nik": "680243",
		"nama": "EDI PURNOMO",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.807843137254902,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9902912621359223,
			"nilai_komparatif_unit": 0.9972311173765145,
			"summary_team": 0.8
		}]
	},
	"680245": {
		"nik": "680245",
		"nama": "GUSTITA",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8904761904761906,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9732620320855614,
			"nilai_komparatif_unit": 0.980082548303457,
			"summary_team": 0.8666666666666667
		}]
	},
	"680252": {
		"nik": "680252",
		"nama": "SURATA",
		"band": "IV",
		"posisi": "ASMAN FINANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8405797101449269,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1031347962382452,
			"nilai_komparatif_unit": 1.1108654468957513,
			"summary_team": 0.9272727272727272
		}]
	},
	"680253": {
		"nik": "680253",
		"nama": "UDIN SUHARDI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.84102564102564,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9296008869179611,
			"nilai_komparatif_unit": 0.9361154305006462,
			"summary_team": 0.7818181818181817
		}]
	},
	"680254": {
		"nik": "680254",
		"nama": "UMAR AGUS TAMAR",
		"band": "IV",
		"posisi": "ASMAN CONSUMER ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8375,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.010332950631458,
			"nilai_komparatif_unit": 1.0174132558813092,
			"summary_team": 0.8461538461538461
		}]
	},
	"680268": {
		"nik": "680268",
		"nama": "SAMSUDIN",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8375,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0654420206659012,
			"nilai_komparatif_unit": 1.072908524383926,
			"summary_team": 0.8923076923076922
		}]
	},
	"680271": {
		"nik": "680271",
		"nama": "AGUS ISKANDAR",
		"band": "IV",
		"posisi": "ASMAN OLO FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7851851851851855,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9600870827285917,
			"nilai_komparatif_unit": 0.9668152703106248,
			"summary_team": 0.7538461538461538
		}]
	},
	"680272": {
		"nik": "680272",
		"nama": "AHMAD KOSASIH",
		"band": "IV",
		"posisi": "ASMAN COMMUNITY DEVELOPMENT CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8051282051282056,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9936305732484071,
			"nilai_komparatif_unit": 1.000593830024093,
			"summary_team": 0.8
		}]
	},
	"680287": {
		"nik": "680287",
		"nama": "AHMAD ZEIN",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7897435897435889,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9623376623376634,
			"nilai_komparatif_unit": 0.9690816217408683,
			"summary_team": 0.76
		}]
	},
	"680299": {
		"nik": "680299",
		"nama": "EDI SUGITO",
		"band": "IV",
		"posisi": "OFF 1 FINANCE SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8592592592592582,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9310344827586219,
			"nilai_komparatif_unit": 0.9375590728276167,
			"summary_team": 0.8
		}]
	},
	"680300": {
		"nik": "680300",
		"nama": "JONNER BINTON SIAHAAN",
		"band": "IV",
		"posisi": "ASMAN WHOLESALE & WIFI ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9904306220095691,
			"nilai_komparatif_unit": 0.9973714538692494,
			"summary_team": 0.8363636363636364
		}]
	},
	"680302": {
		"nik": "680302",
		"nama": "MULYADI",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION CONSUMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8773333333333325,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0638297872340439,
			"nilai_komparatif_unit": 1.0712849925926273,
			"summary_team": 0.9333333333333336
		}]
	},
	"680310": {
		"nik": "680310",
		"nama": "IIS WIARSIH",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8592592592592582,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9601293103448291,
			"nilai_komparatif_unit": 0.96685779385348,
			"summary_team": 0.8250000000000002
		}]
	},
	"680315": {
		"nik": "680315",
		"nama": "RANTA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8829268292682917,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9884480160723266,
			"nilai_komparatif_unit": 0.995374954041664,
			"summary_team": 0.8727272727272727
		}]
	},
	"680324": {
		"nik": "680324",
		"nama": "APIF JAMALUDIN",
		"band": "IV",
		"posisi": "ASMAN BS OBL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.847222222222222,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0086438152011925,
			"nilai_komparatif_unit": 1.0157122831706231,
			"summary_team": 0.8545454545454545
		}]
	},
	"680325": {
		"nik": "680325",
		"nama": "M. SUHADA TRIONO",
		"band": "IV",
		"posisi": "ASMAN PROJECT DELIVERY & SUPERVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8044444444444445,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.994475138121547,
			"nilai_komparatif_unit": 1.0014443135175268,
			"summary_team": 0.8
		}]
	},
	"680372": {
		"nik": "680372",
		"nama": "UJANG SYUKUR",
		"band": "IV",
		"posisi": "ASMAN DATA CENTER & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8529914529914517,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0100200400801618,
			"nilai_komparatif_unit": 1.0170981524863392,
			"summary_team": 0.8615384615384616
		}]
	},
	"680396": {
		"nik": "680396",
		"nama": "TARYONO",
		"band": "IV",
		"posisi": "ASMAN NE FTM ACCESS MTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8192592592592577,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9927667269439441,
			"nilai_komparatif_unit": 0.9997239299771279,
			"summary_team": 0.8133333333333334
		}]
	},
	"680407": {
		"nik": "680407",
		"nama": "DODI TOHIDIN",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.847222222222222,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0086438152011925,
			"nilai_komparatif_unit": 1.0157122831706231,
			"summary_team": 0.8545454545454546
		}]
	},
	"680408": {
		"nik": "680408",
		"nama": "ENDANG SUPRIATNA",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION CONSUMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7791666666666665,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.050072921730676,
			"nilai_komparatif_unit": 1.0574317204472865,
			"summary_team": 0.8181818181818181
		}]
	},
	"680414": {
		"nik": "680414",
		"nama": "SODIKIN",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8370370370370372,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0844111640571814,
			"nilai_komparatif_unit": 1.092010601503097,
			"summary_team": 0.9076923076923077
		}]
	},
	"680424": {
		"nik": "680424",
		"nama": "LUSWANTO",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8453333333333325,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8818468597648418,
			"nilai_komparatif_unit": 0.8880267482331485,
			"summary_team": 0.7454545454545455
		}]
	},
	"680429": {
		"nik": "680429",
		"nama": "WIDIYASTUTI",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666663,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.082840236686391,
			"nilai_komparatif_unit": 1.090428665241323,
			"summary_team": 0.9384615384615385
		}]
	},
	"680440": {
		"nik": "680440",
		"nama": "WAHYU WIDYANTO KUMORO",
		"band": "IV",
		"posisi": "OFF 1 SECURITY & SAFETY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.0339332377706798,
			"summary_team": 0.8727272727272727
		}]
	},
	"680456": {
		"nik": "680456",
		"nama": "SURAHMAN",
		"band": "IV",
		"posisi": "ASMAN CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8784313725490193,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0017857142857147,
			"nilai_komparatif_unit": 1.0088061214174924,
			"summary_team": 0.88
		}]
	},
	"680462": {
		"nik": "680462",
		"nama": "MADRIN",
		"band": "IV",
		"posisi": "ASMAN COMMUNITY DEVELOPMENT CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8800000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0537190082644623,
			"nilai_komparatif_unit": 1.0611033583655056,
			"summary_team": 0.9272727272727272
		}]
	},
	"680463": {
		"nik": "680463",
		"nama": "RAKWAN",
		"band": "IV",
		"posisi": "ASMAN NETWORK PERF, QM & ADMIN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.870270270270269,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0028232636928305,
			"nilai_komparatif_unit": 1.0098509418598738,
			"summary_team": 0.8727272727272728
		}]
	},
	"680491": {
		"nik": "680491",
		"nama": "SUWARNA",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SERVICE 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8656084656084662,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0130712807974416,
			"nilai_komparatif_unit": 1.020170775972196,
			"summary_team": 0.8769230769230769
		}]
	},
	"680498": {
		"nik": "680498",
		"nama": "RUSLI",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8457516339869264,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9931993817619804,
			"nilai_komparatif_unit": 1.000159616793851,
			"summary_team": 0.8400000000000001
		}]
	},
	"680501": {
		"nik": "680501",
		"nama": "DARSONO",
		"band": "IV",
		"posisi": "ASMAN COMMUNITY DEVELOPMENT CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8285714285714286,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.056034482758621,
			"nilai_komparatif_unit": 1.0634350594572495,
			"summary_team": 0.8750000000000002
		}]
	},
	"680531": {
		"nik": "680531",
		"nama": "LILI WAHYUDIYANTO",
		"band": "IV",
		"posisi": "ASMAN SALES SUPPORT 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8448087431693994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9469598965071147,
			"nilai_komparatif_unit": 0.95359609017223,
			"summary_team": 0.8
		}]
	},
	"680534": {
		"nik": "680534",
		"nama": "AGUNG SISMANTO",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION EBIS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8944444444444447,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9757199322416712,
			"nilai_komparatif_unit": 0.9825576731609565,
			"summary_team": 0.8727272727272728
		}]
	},
	"680541": {
		"nik": "680541",
		"nama": "YULI SUNARKA",
		"band": "IV",
		"posisi": "ASMAN NETWORK PERF, QM & ADMIN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7897435897435889,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0324675324675334,
			"nilai_komparatif_unit": 1.039702954499312,
			"summary_team": 0.8153846153846154
		}]
	},
	"680544": {
		"nik": "680544",
		"nama": "SOFIAN TRI SYAPUTRA, S.E",
		"band": "IV",
		"posisi": "ASMAN HR SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8051282051282056,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9936305732484071,
			"nilai_komparatif_unit": 1.000593830024093,
			"summary_team": 0.8
		}]
	},
	"680545": {
		"nik": "680545",
		"nama": "SUDIRNO",
		"band": "IV",
		"posisi": "ASMAN SURVEILLANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8464912280701777,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9632522917496984,
			"nilai_komparatif_unit": 0.9700026607779914,
			"summary_team": 0.8153846153846154
		}]
	},
	"680546": {
		"nik": "680546",
		"nama": "DARMO SETIAWAN",
		"band": "IV",
		"posisi": "ASMAN PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8571428571428562,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0051282051282062,
			"nilai_komparatif_unit": 1.0121720360782853,
			"summary_team": 0.8615384615384616
		}]
	},
	"680551": {
		"nik": "680551",
		"nama": "RIFRINALDI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8805555555555546,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9704617149412113,
			"nilai_komparatif_unit": 0.9772626068360893,
			"summary_team": 0.8545454545454546
		}]
	},
	"680561": {
		"nik": "680561",
		"nama": "ASMADI",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8799999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0139860139860142,
			"nilai_komparatif_unit": 1.0210919195131116,
			"summary_team": 0.8923076923076924
		}]
	},
	"680564": {
		"nik": "680564",
		"nama": "TEGUH RISWONO",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8390804597701139,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9534246575342478,
			"nilai_komparatif_unit": 0.9601061555531515,
			"summary_team": 0.8
		}]
	},
	"680570": {
		"nik": "680570",
		"nama": "ASEP NURHERAWAN",
		"band": "IV",
		"posisi": "ASMAN DATA CENTER & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7703703703703707,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.090384615384615,
			"nilai_komparatif_unit": 1.0980259141384956,
			"summary_team": 0.8400000000000001
		}]
	},
	"680572": {
		"nik": "680572",
		"nama": "MUSTOFA",
		"band": "IV",
		"posisi": "ASMAN LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.842105263157894,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.165909090909092,
			"nilai_komparatif_unit": 1.1740796571091285,
			"summary_team": 0.9818181818181818
		}]
	},
	"680576": {
		"nik": "680576",
		"nama": "NURIDIN",
		"band": "IV",
		"posisi": "ASMAN COMMUNITY DEVELOPMENT CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8303030303030305,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9936131386861313,
			"nilai_komparatif_unit": 1.0005762732822696,
			"summary_team": 0.8250000000000002
		}]
	},
	"690040": {
		"nik": "690040",
		"nama": "INDA IRAWATI",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SERVICE 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.851141552511417,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9399141630901272,
			"nilai_komparatif_unit": 0.9465009810090886,
			"summary_team": 0.8
		}]
	},
	"690066": {
		"nik": "690066",
		"nama": "A. AZIZ",
		"band": "IV",
		"posisi": "OFF 1 RUMAH KREATIF BUMN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8457516339869264,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8986089644513156,
			"nilai_komparatif_unit": 0.9049063199563413,
			"summary_team": 0.76
		}]
	},
	"690073": {
		"nik": "690073",
		"nama": "SYAMSUDIN",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.851141552511417,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0302905249257164,
			"nilai_komparatif_unit": 1.037510690721501,
			"summary_team": 0.8769230769230769
		}]
	},
	"690089": {
		"nik": "690089",
		"nama": "DARMANTO",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE SKJ, PCM,CSL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8196969696969681,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9384331010948403,
			"nilai_komparatif_unit": 0.9450095398897576,
			"summary_team": 0.7692307692307692
		}]
	},
	"690096": {
		"nik": "690096",
		"nama": "ACHYARUL BANI",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE DEPOK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8431372549019593,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9488372093023273,
			"nilai_komparatif_unit": 0.9554865589747086,
			"summary_team": 0.8
		}]
	},
	"690100": {
		"nik": "690100",
		"nama": "AHMAD SUBHAN",
		"band": "IV",
		"posisi": "ASMAN WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8285714285714273,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9655172413793119,
			"nilai_komparatif_unit": 0.9722834829323437,
			"summary_team": 0.8
		}]
	},
	"690112": {
		"nik": "690112",
		"nama": "EDI SANTOSO",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION CONSUMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8684684684684674,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9565911267156093,
			"nilai_komparatif_unit": 0.9632948150118411,
			"summary_team": 0.8307692307692308
		}]
	},
	"690113": {
		"nik": "690113",
		"nama": "EKO WICAKSONO",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.84102564102564,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.016075388026609,
			"nilai_komparatif_unit": 1.0231959356634972,
			"summary_team": 0.8545454545454546
		}]
	},
	"690120": {
		"nik": "690120",
		"nama": "LUASMAN PANJAITAN",
		"band": "IV",
		"posisi": "OFF 1 SECURITY & SAFETY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181818,
			"nilai_komparatif_unit": 0.9886986586182127,
			"summary_team": 0.8181818181818182
		}]
	},
	"690122": {
		"nik": "690122",
		"nama": "MOCHAMMAD ALISYAWALUDIN",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE TJ PRIOK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8739583333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9916567342073896,
			"nilai_komparatif_unit": 0.9986061585302037,
			"summary_team": 0.8666666666666667
		}]
	},
	"690125": {
		"nik": "690125",
		"nama": "MURSITO",
		"band": "IV",
		"posisi": "ASMAN QUALITY ENHANCEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8227642276422753,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0386273805246151,
			"nilai_komparatif_unit": 1.0459059701127023,
			"summary_team": 0.8545454545454546
		}]
	},
	"690134": {
		"nik": "690134",
		"nama": "SAMSU HILAL",
		"band": "IV",
		"posisi": "ASMAN CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0140499062750898,
			"summary_team": 0.8727272727272728
		}]
	},
	"690141": {
		"nik": "690141",
		"nama": "SUPRANOWO",
		"band": "IV",
		"posisi": "ASMAN BGES ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8285714285714273,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0137931034482774,
			"nilai_komparatif_unit": 1.0208976570789607,
			"summary_team": 0.8400000000000001
		}]
	},
	"690148": {
		"nik": "690148",
		"nama": "WAHIDI ROMADHON",
		"band": "IV",
		"posisi": "ASMAN PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8245614035087718,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0363636363636364,
			"nilai_komparatif_unit": 1.04362636187478,
			"summary_team": 0.8545454545454545
		}]
	},
	"690160": {
		"nik": "690160",
		"nama": "FAISOL ZULKARNAIN",
		"band": "IV",
		"posisi": "ASMAN PROJECT DELIVERY & SUPERVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.851141552511417,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0182403433476377,
			"nilai_komparatif_unit": 1.0253760627598458,
			"summary_team": 0.8666666666666667
		}]
	},
	"690174": {
		"nik": "690174",
		"nama": "NGATIMAN",
		"band": "IV",
		"posisi": "ASMAN DATA CENTER & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8380952380952383,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9545454545454541,
			"nilai_komparatif_unit": 0.9612348069899287,
			"summary_team": 0.7999999999999999
		}]
	},
	"690175": {
		"nik": "690175",
		"nama": "RUSTANDI",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7966666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0621177985194723,
			"nilai_komparatif_unit": 1.0695610064442636,
			"summary_team": 0.8461538461538461
		}]
	},
	"690178": {
		"nik": "690178",
		"nama": "SAPARULLAH",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8592592592592582,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0384615384615397,
			"nilai_komparatif_unit": 1.0457389658461878,
			"summary_team": 0.8923076923076922
		}]
	},
	"690182": {
		"nik": "690182",
		"nama": "SYAHRUDIN PERMANA",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8192592592592577,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9764918625678138,
			"nilai_komparatif_unit": 0.9833350130922569,
			"summary_team": 0.8
		}]
	},
	"690185": {
		"nik": "690185",
		"nama": "ZAKARIJA",
		"band": "IV",
		"posisi": "ASMAN COMMUNICATION & SECRETARIATE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181815,
			"nilai_komparatif_unit": 0.9886986586182124,
			"summary_team": 0.8181818181818182
		}]
	},
	"690200": {
		"nik": "690200",
		"nama": "SUGIYARTO",
		"band": "IV",
		"posisi": "ASMAN DATA CENTER & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8464912280701777,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9814268632921457,
			"nilai_komparatif_unit": 0.9883045977738028,
			"summary_team": 0.8307692307692308
		}]
	},
	"690210": {
		"nik": "690210",
		"nama": "ASMAN",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE CBI, BJD",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.851141552511417,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0122152525585986,
			"nilai_komparatif_unit": 1.0193087487790184,
			"summary_team": 0.8615384615384616
		}]
	},
	"690215": {
		"nik": "690215",
		"nama": "RANO NOORBANA",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8227642276422753,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0284280936454864,
			"nilai_komparatif_unit": 1.0356352077220703,
			"summary_team": 0.8461538461538461
		}]
	},
	"690217": {
		"nik": "690217",
		"nama": "YUSUP KURNIA",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CIKARANG",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8450450450450468,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0112424888544271,
			"nilai_komparatif_unit": 1.018329168050858,
			"summary_team": 0.8545454545454546
		}]
	},
	"690225": {
		"nik": "690225",
		"nama": "CECEP",
		"band": "IV",
		"posisi": "ASMAN SERVICE AREA CIK, JBB, SUE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8450450450450468,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9466950959488253,
			"nilai_komparatif_unit": 0.953329433919952,
			"summary_team": 0.8
		}]
	},
	"690230": {
		"nik": "690230",
		"nama": "MAS SOEDARTO",
		"band": "IV",
		"posisi": "OFF 1 SECURITY & SAFETY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8656084656084662,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9452100466770387,
			"nilai_komparatif_unit": 0.951833977581714,
			"summary_team": 0.8181818181818181
		}]
	},
	"690238": {
		"nik": "690238",
		"nama": "ESTY SUKAESIH",
		"band": "IV",
		"posisi": "ASMAN FINANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346935,
			"nilai_komparatif_unit": 0.9864567115465157,
			"summary_team": 0.8
		}]
	},
	"690249": {
		"nik": "690249",
		"nama": "MOHAMAD TATANG",
		"band": "IV",
		"posisi": "OFF 1 RUMAH KREATIF BUMN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8784313725490193,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0169642857142862,
			"nilai_komparatif_unit": 1.0240910626510906,
			"summary_team": 0.8933333333333334
		}]
	},
	"690257": {
		"nik": "690257",
		"nama": "BUDI HARTONO",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8829268292682917,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.050226017076847,
			"nilai_komparatif_unit": 1.057585888669268,
			"summary_team": 0.9272727272727272
		}]
	},
	"690270": {
		"nik": "690270",
		"nama": "YADI SUPRIYADI",
		"band": "IV",
		"posisi": "ASMAN PROJECT DELIVERY & SUPERVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8487179487179478,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0282889316121957,
			"nilai_komparatif_unit": 1.0354950704561354,
			"summary_team": 0.8727272727272728
		}]
	},
	"690310": {
		"nik": "690310",
		"nama": "SAIFULLOH",
		"band": "IV",
		"posisi": "ASMAN DEBT MGT & COLLECTION PARTNERSHIP",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8083333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0121836925960634,
			"nilai_komparatif_unit": 1.0192769676476416,
			"summary_team": 0.8181818181818182
		}]
	},
	"690328": {
		"nik": "690328",
		"nama": "SUHANDANA",
		"band": "IV",
		"posisi": "OFF 1 RUMAH KREATIF BUMN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8245614035087718,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0143133462282399,
			"nilai_komparatif_unit": 1.0214215456646785,
			"summary_team": 0.8363636363636363
		}]
	},
	"690329": {
		"nik": "690329",
		"nama": "TARSONO",
		"band": "IV",
		"posisi": "ASMAN WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8095238095238096,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9882352941176472,
			"nilai_komparatif_unit": 0.9951607413542797,
			"summary_team": 0.8000000000000003
		}]
	},
	"690337": {
		"nik": "690337",
		"nama": "ACHMAD THAMBARA",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.046382189239332,
			"nilai_komparatif_unit": 1.0537151236974147,
			"summary_team": 0.8545454545454546
		}]
	},
	"690343": {
		"nik": "690343",
		"nama": "FAUZI",
		"band": "IV",
		"posisi": "OFF 1 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8192592592592577,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0430708531974375,
			"nilai_komparatif_unit": 1.050380582166729,
			"summary_team": 0.8545454545454546
		}]
	},
	"690353": {
		"nik": "690353",
		"nama": "SISWAN SUBADRI",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SERVICE 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8419753086419747,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0049627791563283,
			"nilai_komparatif_unit": 1.012005450818891,
			"summary_team": 0.8461538461538461
		}]
	},
	"690361": {
		"nik": "690361",
		"nama": "ARIS MULYANA",
		"band": "IV",
		"posisi": "ASMAN OLO FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8285714285714273,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9655172413793119,
			"nilai_komparatif_unit": 0.9722834829323437,
			"summary_team": 0.8
		}]
	},
	"690363": {
		"nik": "690363",
		"nama": "GATOT ISWANTO",
		"band": "IV",
		"posisi": "ASMAN NE FTM ACCESS MTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8227642276422753,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0386273805246151,
			"nilai_komparatif_unit": 1.0459059701127023,
			"summary_team": 0.8545454545454546
		}]
	},
	"690364": {
		"nik": "690364",
		"nama": "HAPOSAN RINGORINGO",
		"band": "IV",
		"posisi": "ASMAN DEBT MGT & COLLECTION PARTNERSHIP",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8405797101449269,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9517241379310352,
			"nilai_komparatif_unit": 0.9583937188904521,
			"summary_team": 0.8
		}]
	},
	"690365": {
		"nik": "690365",
		"nama": "ISTI LESTARI",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8944444444444447,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0163749294184075,
			"nilai_komparatif_unit": 1.0234975762093297,
			"summary_team": 0.9090909090909092
		}]
	},
	"690367": {
		"nik": "690367",
		"nama": "KOKO NURMAN, ST",
		"band": "IV",
		"posisi": "ASMAN SERVICE AREA CWG",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.851141552511417,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9555793991416291,
			"nilai_komparatif_unit": 0.9622759973592399,
			"summary_team": 0.8133333333333332
		}]
	},
	"690398": {
		"nik": "690398",
		"nama": "SARWOKO",
		"band": "IV",
		"posisi": "ASMAN BGES PROJECT MGT & QUALITY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8829268292682917,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0106247343816417,
			"nilai_komparatif_unit": 1.017707084420804,
			"summary_team": 0.8923076923076924
		}]
	},
	"690416": {
		"nik": "690416",
		"nama": "ASEP SUPRIATNA",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION EBIS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8684684684684674,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9420973217653728,
			"nilai_komparatif_unit": 0.9486994390268132,
			"summary_team": 0.8181818181818182
		}]
	},
	"690423": {
		"nik": "690423",
		"nama": "KUNTJORO HADI",
		"band": "IV",
		"posisi": "ASMAN HUMAN CAPITAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9763313609467468,
			"nilai_komparatif_unit": 0.9831733866929971,
			"summary_team": 0.8461538461538461
		}]
	},
	"690424": {
		"nik": "690424",
		"nama": "PRIYONO",
		"band": "IV",
		"posisi": "ASMAN OM TRANSPORT, RADIO, & MUX",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.870270270270269,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9192546583850946,
			"nilai_komparatif_unit": 0.9256966967048843,
			"summary_team": 0.8
		}]
	},
	"690429": {
		"nik": "690429",
		"nama": "HERI PRASETIYO",
		"band": "IV",
		"posisi": "ASMAN OM TRANSPORT, RADIO & MUX",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8464912280701777,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0080829015544013,
			"nilai_komparatif_unit": 1.0151474387009922,
			"summary_team": 0.8533333333333333
		}]
	},
	"690436": {
		"nik": "690436",
		"nama": "SUKISWAN",
		"band": "IV",
		"posisi": "ASMAN HUMAN CAPITAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8370370370370375,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9557522123893801,
			"nilai_komparatif_unit": 0.9624500216637465,
			"summary_team": 0.8
		}]
	},
	"690438": {
		"nik": "690438",
		"nama": "SOLIHIN",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8453333333333325,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9678806997418995,
			"nilai_komparatif_unit": 0.9746635041583338,
			"summary_team": 0.8181818181818182
		}]
	},
	"690440": {
		"nik": "690440",
		"nama": "ENDANG WAHYUDIN",
		"band": "IV",
		"posisi": "ASMAN DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8464912280701777,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.035950577919487,
			"nilai_komparatif_unit": 1.0432104087612362,
			"summary_team": 0.8769230769230769
		}]
	},
	"690452": {
		"nik": "690452",
		"nama": "SUGIONO",
		"band": "IV",
		"posisi": "ASMAN SALES SUPPORT 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.807843137254902,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0353045013239186,
			"nilai_komparatif_unit": 1.0425598045299922,
			"summary_team": 0.8363636363636363
		}]
	},
	"690455": {
		"nik": "690455",
		"nama": "ROBERTUS BEKTI BUDIONO",
		"band": "IV",
		"posisi": "ASMAN DEBT MGT & COLLECTION PARTNERSHIP",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8684684684684674,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9420973217653728,
			"nilai_komparatif_unit": 0.9486994390268132,
			"summary_team": 0.8181818181818182
		}]
	},
	"690460": {
		"nik": "690460",
		"nama": "IWAN SETIAWAN",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8196969696969681,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0885823972700148,
			"nilai_komparatif_unit": 1.0962110662721187,
			"summary_team": 0.8923076923076922
		}]
	},
	"690462": {
		"nik": "690462",
		"nama": "MULIAMAN SIAGIAN",
		"band": "IV",
		"posisi": "ASMAN CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8375,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9552238805970149,
			"nilai_komparatif_unit": 0.9619179873786923,
			"summary_team": 0.8
		}]
	},
	"690469": {
		"nik": "690469",
		"nama": "SUPRIYONO",
		"band": "IV",
		"posisi": "OFF 1 COMMAND CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8684684684684674,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9420973217653728,
			"nilai_komparatif_unit": 0.9486994390268132,
			"summary_team": 0.8181818181818182
		}]
	},
	"690471": {
		"nik": "690471",
		"nama": "YUDIANTO",
		"band": "IV",
		"posisi": "ASMAN BGES FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8597701149425278,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0855614973262044,
			"nilai_komparatif_unit": 1.0931689961846265,
			"summary_team": 0.9333333333333332
		}]
	},
	"690472": {
		"nik": "690472",
		"nama": "SUDARYO",
		"band": "IV",
		"posisi": "ASMAN OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8196969696969681,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0247689463955656,
			"nilai_komparatif_unit": 1.031950417559615,
			"summary_team": 0.84
		}]
	},
	"690496": {
		"nik": "690496",
		"nama": "MATYASIN",
		"band": "IV",
		"posisi": "OFF 1 SECURITY & SAFETY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.967409948542024,
			"nilai_komparatif_unit": 0.9741894539844025,
			"summary_team": 0.8545454545454546
		}]
	},
	"690498": {
		"nik": "690498",
		"nama": "R. AGUS SETIAJI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8805555555555546,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1149985661026682,
			"nilai_komparatif_unit": 1.1228123567904005,
			"summary_team": 0.9818181818181818
		}]
	},
	"690505": {
		"nik": "690505",
		"nama": "SUMARDI",
		"band": "IV",
		"posisi": "ASMAN CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8571428571428562,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0266666666666677,
			"nilai_komparatif_unit": 1.0338614368513914,
			"summary_team": 0.88
		}]
	},
	"690508": {
		"nik": "690508",
		"nama": "NASRUDIN",
		"band": "IV",
		"posisi": "OFF 1 MAINTENANCE ENGINEERING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8464912280701777,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9923316062176138,
			"nilai_komparatif_unit": 0.9992857599712893,
			"summary_team": 0.84
		}]
	},
	"690515": {
		"nik": "690515",
		"nama": "SUGIYARTO",
		"band": "IV",
		"posisi": "ASMAN DAVAL & INVENTORY MGT PL & REVIEW",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8192592592592577,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9764918625678138,
			"nilai_komparatif_unit": 0.9833350130922569,
			"summary_team": 0.8
		}]
	},
	"690525": {
		"nik": "690525",
		"nama": "ENDRA LUKITA",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.842105263157894,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9500000000000008,
			"nilai_komparatif_unit": 0.956657498385216,
			"summary_team": 0.8
		}]
	},
	"690526": {
		"nik": "690526",
		"nama": "AHMAD KOSASIH",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.84102564102564,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.016075388026609,
			"nilai_komparatif_unit": 1.0231959356634972,
			"summary_team": 0.8545454545454546
		}]
	},
	"690528": {
		"nik": "690528",
		"nama": "WILSON HAMONANGAN",
		"band": "IV",
		"posisi": "ASMAN BS OBL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8448087431693994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0562244999502433,
			"nilai_komparatif_unit": 1.0636264082690257,
			"summary_team": 0.8923076923076922
		}]
	},
	"690529": {
		"nik": "690529",
		"nama": "ZAKARIA",
		"band": "IV",
		"posisi": "ASMAN NETWORK PERF, QM & ADMIN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8529914529914517,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.108398615412645,
			"nilai_komparatif_unit": 1.1161661543518917,
			"summary_team": 0.9454545454545454
		}]
	},
	"690536": {
		"nik": "690536",
		"nama": "SUDARMANTO",
		"band": "IV",
		"posisi": "ASMAN HUBUNGAN ANTAR INSTANSI/PEMDA",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8192592592592577,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9099128719381901,
			"nilai_komparatif_unit": 0.9162894440177848,
			"summary_team": 0.7454545454545455
		}]
	},
	"690545": {
		"nik": "690545",
		"nama": "SUSWANTO",
		"band": "IV",
		"posisi": "ASMAN DAVAL & INVENTORY MGT PL & REVIEW",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8227642276422753,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9910307084220141,
			"nilai_komparatif_unit": 0.9979757456230859,
			"summary_team": 0.8153846153846154
		}]
	},
	"690546": {
		"nik": "690546",
		"nama": "YAHYA",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444447,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0980861244019136,
			"nilai_komparatif_unit": 1.1057813945072112,
			"summary_team": 0.9272727272727272
		}]
	},
	"690548": {
		"nik": "690548",
		"nama": "LUTFI FAISAL",
		"band": "IV",
		"posisi": "ASMAN CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8196969696969681,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9384331010948405,
			"nilai_komparatif_unit": 0.9450095398897577,
			"summary_team": 0.7692307692307693
		}]
	},
	"690552": {
		"nik": "690552",
		"nama": "SUNARSO",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "BS-2-01",
			"kriteria": 0.7933333333333331,
			"kriteria_bp": 0.9904860154304124,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9854851031321621,
			"nilai_komparatif_unit": 0.9949510520892341,
			"summary_team": 0.7818181818181817
		}]
	},
	"690558": {
		"nik": "690558",
		"nama": "SAHLAN",
		"band": "IV",
		"posisi": "MGR NETWORK DEPLOYMENT&PROJ SUPERVISION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8597701149425278,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0235294117647071,
			"nilai_komparatif_unit": 1.030702196402648,
			"summary_team": 0.88
		}]
	},
	"690560": {
		"nik": "690560",
		"nama": "SUROSO",
		"band": "IV",
		"posisi": "ASMAN IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.870270270270269,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9192546583850946,
			"nilai_komparatif_unit": 0.9256966967048843,
			"summary_team": 0.8
		}]
	},
	"690571": {
		"nik": "690571",
		"nama": "GITO",
		"band": "IV",
		"posisi": "ASMAN NE FTM ACCESS MTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8464912280701777,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0632124352331578,
			"nilai_komparatif_unit": 1.0706633142549529,
			"summary_team": 0.9000000000000001
		}]
	},
	"690586": {
		"nik": "690586",
		"nama": "ISMIANTI",
		"band": "IV",
		"posisi": "ASMAN DEBT MGT & COLLECTION PARTNERSHIP",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.76,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9649122807017546,
			"nilai_komparatif_unit": 0.9716742827550664,
			"summary_team": 0.7333333333333335
		}]
	},
	"690594": {
		"nik": "690594",
		"nama": "ELLY RAHMALYA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8829268292682917,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0296333500753403,
			"nilai_komparatif_unit": 1.0368489104600669,
			"summary_team": 0.9090909090909092
		}]
	},
	"700035": {
		"nik": "700035",
		"nama": "SUHARNO",
		"band": "IV",
		"posisi": "ASMAN WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9291497975708498,
			"nilai_komparatif_unit": 0.9356611799676402,
			"summary_team": 0.7846153846153846
		}]
	},
	"700046": {
		"nik": "700046",
		"nama": "MISWAN ADIL",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.851141552511417,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9941399801914806,
			"nilai_komparatif_unit": 1.0011068068365359,
			"summary_team": 0.8461538461538461
		}]
	},
	"700049": {
		"nik": "700049",
		"nama": "PAHMI",
		"band": "IV",
		"posisi": "ASMAN BGES ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9277777777777779,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0347305389221555,
			"nilai_komparatif_unit": 1.0419818198611102,
			"summary_team": 0.96
		}]
	},
	"700051": {
		"nik": "700051",
		"nama": "TONO SUTRISNO",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8487179487179478,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9897280966767383,
			"nilai_komparatif_unit": 0.9966640053140303,
			"summary_team": 0.8400000000000001
		}]
	},
	"700060": {
		"nik": "700060",
		"nama": "GUNAWAN",
		"band": "IV",
		"posisi": "ASMAN SALES SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8450450450450468,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9682108935840259,
			"nilai_komparatif_unit": 0.9749960119635873,
			"summary_team": 0.8181818181818182
		}]
	},
	"700062": {
		"nik": "700062",
		"nama": "NANA AGUS WIJAYA",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.842105263157894,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9500000000000008,
			"nilai_komparatif_unit": 0.956657498385216,
			"summary_team": 0.8
		}]
	},
	"700071": {
		"nik": "700071",
		"nama": "AGUS INDARTO",
		"band": "IV",
		"posisi": "ASMAN OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333323,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9600000000000013,
			"nilai_komparatif_unit": 0.9667275773155871,
			"summary_team": 0.8
		}]
	},
	"700073": {
		"nik": "700073",
		"nama": "ANTONIUS HARI SUSANTO",
		"band": "IV",
		"posisi": "ASMAN BGES FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9277777777777779,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9916167664670658,
			"nilai_komparatif_unit": 0.9985659107002307,
			"summary_team": 0.92
		}]
	},
	"700081": {
		"nik": "700081",
		"nama": "EDI SUNARYO",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8196969696969681,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0322764112043243,
			"nilai_komparatif_unit": 1.0395104938787332,
			"summary_team": 0.8461538461538461
		}]
	},
	"700090": {
		"nik": "700090",
		"nama": "IRWANTO",
		"band": "IV",
		"posisi": "ASMAN BS TERRITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8616666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0128362933005113,
			"nilai_komparatif_unit": 1.0199341417080223,
			"summary_team": 0.8727272727272728
		}]
	},
	"700092": {
		"nik": "700092",
		"nama": "KOMARUDIN",
		"band": "IV",
		"posisi": "ASMAN WIFI ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8611111111111102,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9647642679900755,
			"nilai_komparatif_unit": 0.9715252327861357,
			"summary_team": 0.8307692307692308
		}]
	},
	"700093": {
		"nik": "700093",
		"nama": "KOMARUDIN",
		"band": "IV",
		"posisi": "ASMAN WHOLESALE & WIFI ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7851851851851855,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1114922813036014,
			"nilai_komparatif_unit": 1.1192815003225043,
			"summary_team": 0.8727272727272727
		}]
	},
	"700094": {
		"nik": "700094",
		"nama": "LILIK HENDRIANTO",
		"band": "IV",
		"posisi": "ASMAN BS SALES ENGINEER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.847222222222222,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1159463487332342,
			"nilai_komparatif_unit": 1.1237667813802639,
			"summary_team": 0.9454545454545454
		}]
	},
	"700097": {
		"nik": "700097",
		"nama": "MOHAMAD SOLEH",
		"band": "IV",
		"posisi": "OFF 1 PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8457516339869264,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9931993817619803,
			"nilai_komparatif_unit": 1.000159616793851,
			"summary_team": 0.84
		}]
	},
	"700100": {
		"nik": "700100",
		"nama": "NUR RACHMADIDJAYA",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8904761904761906,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.061740398638794,
			"nilai_komparatif_unit": 1.0691809617855892,
			"summary_team": 0.9454545454545454
		}]
	},
	"700101": {
		"nik": "700101",
		"nama": "NUR SODIK",
		"band": "IV",
		"posisi": "ASMAN WHOLESALE & WIFI ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333323,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9600000000000013,
			"nilai_komparatif_unit": 0.9667275773155871,
			"summary_team": 0.8
		}]
	},
	"700102": {
		"nik": "700102",
		"nama": "PUJI WAHJONO",
		"band": "IV",
		"posisi": "ASMAN PARTNERSHIP & AGENCY 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8457516339869264,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.945904173106648,
			"nilai_komparatif_unit": 0.9525329683750962,
			"summary_team": 0.8
		}]
	},
	"700104": {
		"nik": "700104",
		"nama": "RISWANTO",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8390804597701139,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0451001053740792,
			"nilai_komparatif_unit": 1.0524240551255697,
			"summary_team": 0.8769230769230769
		}]
	},
	"700105": {
		"nik": "700105",
		"nama": "SAFRIZAL",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8370370370370367,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.019469026548673,
			"nilai_komparatif_unit": 1.0266133564413304,
			"summary_team": 0.8533333333333334
		}]
	},
	"700108": {
		"nik": "700108",
		"nama": "SONI KABUL SUASONO",
		"band": "IV",
		"posisi": "ASMAN BS TERRITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8419753086419747,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9501466275659832,
			"nilai_komparatif_unit": 0.956805153501497,
			"summary_team": 0.8
		}]
	},
	"700117": {
		"nik": "700117",
		"nama": "WAGIRAN",
		"band": "IV",
		"posisi": "ASMAN ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8464912280701777,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.992331606217614,
			"nilai_komparatif_unit": 0.9992857599712894,
			"summary_team": 0.8400000000000001
		}]
	},
	"700119": {
		"nik": "700119",
		"nama": "WAHYUDIN",
		"band": "IV",
		"posisi": "ASMAN NE FTM ACCESS MTNC & PARTNER CTRL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8390804597701139,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0834371108343726,
			"nilai_komparatif_unit": 1.0910297222194905,
			"summary_team": 0.9090909090909092
		}]
	},
	"700120": {
		"nik": "700120",
		"nama": "YUDI PRIHADI",
		"band": "IV",
		"posisi": "ASMAN PLANNING DESIGN & INSTANSI REL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8044444444444445,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0709732256693583,
			"nilai_komparatif_unit": 1.0784784914804133,
			"summary_team": 0.8615384615384616
		}]
	},
	"700121": {
		"nik": "700121",
		"nama": "YUDI ROSTIADI",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0140499062750898,
			"summary_team": 0.8727272727272727
		}]
	},
	"700124": {
		"nik": "700124",
		"nama": "AGUS SUTRISNO",
		"band": "IV",
		"posisi": "ASMAN OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7703703703703707,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9951923076923074,
			"nilai_komparatif_unit": 1.0021665089359284,
			"summary_team": 0.7666666666666667
		}]
	},
	"700126": {
		"nik": "700126",
		"nama": "AHMAD JAZIM",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8370370370370368,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9716814159292037,
			"nilai_komparatif_unit": 0.9784908553581428,
			"summary_team": 0.8133333333333332
		}]
	},
	"700132": {
		"nik": "700132",
		"nama": "FIRMAN ARIEF",
		"band": "IV",
		"posisi": "ASMAN WIFI FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8285714285714273,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9655172413793119,
			"nilai_komparatif_unit": 0.9722834829323437,
			"summary_team": 0.8
		}]
	},
	"700174": {
		"nik": "700174",
		"nama": "ABDUL HADI",
		"band": "IV",
		"posisi": "ASMAN PARTNERSHIP & AGENCY 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8448087431693994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.041655886157826,
			"nilai_komparatif_unit": 1.0489556991894529,
			"summary_team": 0.88
		}]
	},
	"700185": {
		"nik": "700185",
		"nama": "BESARI",
		"band": "IV",
		"posisi": "ASMAN COMMUNICATION & SECRETARIATE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8944444444444447,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9960474308300392,
			"nilai_komparatif_unit": 1.003027624685143,
			"summary_team": 0.8909090909090909
		}]
	},
	"700202": {
		"nik": "700202",
		"nama": "MOHAMMAD FADLI",
		"band": "IV",
		"posisi": "ASMAN BGES FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8285714285714273,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9856321839080476,
			"nilai_komparatif_unit": 0.9925393888267675,
			"summary_team": 0.8166666666666668
		}]
	},
	"700220": {
		"nik": "700220",
		"nama": "CHRISTIN SRI WAHYUNINGSIH",
		"band": "IV",
		"posisi": "ASMAN DEBT MGT & COLLECTION PARTNERSHIP",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8606060606060609,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9653304442036833,
			"nilai_komparatif_unit": 0.9720953767020886,
			"summary_team": 0.8307692307692308
		}]
	},
	"700223": {
		"nik": "700223",
		"nama": "ZUKRI",
		"band": "IV",
		"posisi": "ASMAN CAPEX MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8571428571428562,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0888888888888901,
			"nilai_komparatif_unit": 1.096519705751476,
			"summary_team": 0.9333333333333333
		}]
	},
	"700226": {
		"nik": "700226",
		"nama": "SUGIANTO",
		"band": "IV",
		"posisi": "ASMAN COMMUNITY DEVELOPMENT CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8370370370370375,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9925119128658947,
			"nilai_komparatif_unit": 0.9994673301892751,
			"summary_team": 0.8307692307692308
		}]
	},
	"700228": {
		"nik": "700228",
		"nama": "BAHRUMSYAH",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION CONSUMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8083333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0121836925960634,
			"nilai_komparatif_unit": 1.0192769676476416,
			"summary_team": 0.8181818181818182
		}]
	},
	"700235": {
		"nik": "700235",
		"nama": "SUPAIDI",
		"band": "IV",
		"posisi": "ASMAN CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8419753086419747,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9501466275659832,
			"nilai_komparatif_unit": 0.956805153501497,
			"summary_team": 0.8
		}]
	},
	"700239": {
		"nik": "700239",
		"nama": "EFFENDI",
		"band": "IV",
		"posisi": "ASMAN BS TERRITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8829268292682917,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.050226017076847,
			"nilai_komparatif_unit": 1.057585888669268,
			"summary_team": 0.9272727272727272
		}]
	},
	"700241": {
		"nik": "700241",
		"nama": "MARDANI",
		"band": "IV",
		"posisi": "OFF 1 COMMAND CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8529914529914517,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0231371834578262,
			"nilai_komparatif_unit": 1.0303072194017462,
			"summary_team": 0.8727272727272727
		}]
	},
	"700243": {
		"nik": "700243",
		"nama": "ZAENAL SABAR",
		"band": "IV",
		"posisi": "ASMAN SERVICE AREA PDK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.851141552511417,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0965665236051483,
			"nilai_komparatif_unit": 1.1042511445106031,
			"summary_team": 0.9333333333333333
		}]
	},
	"700245": {
		"nik": "700245",
		"nama": "SUJATMIKO",
		"band": "IV",
		"posisi": "ASMAN SERVICE AREA PGB",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.851141552511417,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9869098712446335,
			"nilai_komparatif_unit": 0.993826030059543,
			"summary_team": 0.84
		}]
	},
	"700301": {
		"nik": "700301",
		"nama": "TAUHID DERMAWAN",
		"band": "IV",
		"posisi": "ASMAN WITEL OPERATION CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8571428571428562,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0111111111111122,
			"nilai_komparatif_unit": 1.0181968696263703,
			"summary_team": 0.8666666666666667
		}]
	},
	"700306": {
		"nik": "700306",
		"nama": "ACHMAD SUPRIADI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8453333333333325,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9936908517350167,
			"nilai_komparatif_unit": 1.0006545309358892,
			"summary_team": 0.84
		}]
	},
	"700307": {
		"nik": "700307",
		"nama": "DEDY KURNIADI",
		"band": "IV",
		"posisi": "ASMAN OM TRANSPORT, RADIO & MUX",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8380952380952383,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9545454545454544,
			"nilai_komparatif_unit": 0.9612348069899289,
			"summary_team": 0.8
		}]
	},
	"700311": {
		"nik": "700311",
		"nama": "ABDUL HASJIM",
		"band": "IV",
		"posisi": "ASMAN WHOLESALE & WIFI ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8285714285714273,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9655172413793119,
			"nilai_komparatif_unit": 0.9722834829323437,
			"summary_team": 0.8
		}]
	},
	"700312": {
		"nik": "700312",
		"nama": "ABDUL KADAR",
		"band": "IV",
		"posisi": "ASMAN CONSUMER ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8611111111111102,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0768328445747812,
			"nilai_komparatif_unit": 1.0843791739683635,
			"summary_team": 0.9272727272727272
		}]
	},
	"700315": {
		"nik": "700315",
		"nama": "ACHMAD BUSTHOMI",
		"band": "IV",
		"posisi": "ASMAN COMMUNICATION & SECRETARIATE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769242,
			"nilai_komparatif_unit": 0.9295457474188337,
			"summary_team": 0.8
		}]
	},
	"700324": {
		"nik": "700324",
		"nama": "FERRY ARIJANTHO",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8450450450450468,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0255863539445607,
			"nilai_komparatif_unit": 1.0327735534132811,
			"summary_team": 0.8666666666666667
		}]
	},
	"700326": {
		"nik": "700326",
		"nama": "IHIN SOLIHIN",
		"band": "IV",
		"posisi": "ASMAN SALES SUPPORT 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8457516339869264,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.945904173106648,
			"nilai_komparatif_unit": 0.9525329683750962,
			"summary_team": 0.8
		}]
	},
	"700336": {
		"nik": "700336",
		"nama": "SUDARNO",
		"band": "IV",
		"posisi": "ASMAN CONSUMER ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8571428571428562,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9488888888888898,
			"nilai_komparatif_unit": 0.9555386007262859,
			"summary_team": 0.8133333333333332
		}]
	},
	"700337": {
		"nik": "700337",
		"nama": "SUHIRMAN",
		"band": "IV",
		"posisi": "ASMAN OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8529914529914517,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9919839679358732,
			"nilai_komparatif_unit": 0.9989356854776545,
			"summary_team": 0.8461538461538461
		}]
	},
	"700356": {
		"nik": "700356",
		"nama": "HARTAWAN",
		"band": "IV",
		"posisi": "ASMAN BGES ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8597701149425278,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0080213903743327,
			"nilai_komparatif_unit": 1.0150854964571532,
			"summary_team": 0.8666666666666667
		}]
	},
	"700365": {
		"nik": "700365",
		"nama": "MUHAMAD NURROCHMAN",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9846153846153859,
			"nilai_komparatif_unit": 0.9915154639134226,
			"summary_team": 0.8533333333333334
		}]
	},
	"700369": {
		"nik": "700369",
		"nama": "ROCHMAT",
		"band": "IV",
		"posisi": "ASMAN SALES SUPPORT 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8457516339869264,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.01866603257639,
			"nilai_komparatif_unit": 1.0258047351731805,
			"summary_team": 0.8615384615384616
		}]
	},
	"700372": {
		"nik": "700372",
		"nama": "SJAHRUDDIN",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION CONSUMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8405797101449269,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1031347962382452,
			"nilai_komparatif_unit": 1.1108654468957513,
			"summary_team": 0.9272727272727272
		}]
	},
	"700376": {
		"nik": "700376",
		"nama": "SUPRIDIYANTO",
		"band": "IV",
		"posisi": "ASMAN HUMAN CAPITAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8051282051282056,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9936305732484071,
			"nilai_komparatif_unit": 1.000593830024093,
			"summary_team": 0.8
		}]
	},
	"700379": {
		"nik": "700379",
		"nama": "TRIATMA SUPRAPTA",
		"band": "IV",
		"posisi": "ASMAN PARTNERSHIP & AGENCY 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.851141552511417,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0182403433476377,
			"nilai_komparatif_unit": 1.0253760627598458,
			"summary_team": 0.8666666666666667
		}]
	},
	"700381": {
		"nik": "700381",
		"nama": "ANDI BUDIANTY",
		"band": "IV",
		"posisi": "OFF 1 QUALITY, CHANGE & RISK MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8405797101449269,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0066312997347489,
			"nilai_komparatif_unit": 1.0136856642110552,
			"summary_team": 0.8461538461538463
		}]
	},
	"700391": {
		"nik": "700391",
		"nama": "ISNAINI NUR",
		"band": "IV",
		"posisi": "ASMAN CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8370370370370368,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9876106194690268,
			"nilai_komparatif_unit": 0.9945316890525387,
			"summary_team": 0.8266666666666667
		}]
	},
	"700399": {
		"nik": "700399",
		"nama": "SUGENG EDY KUNCORO",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE BINTARO",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8676190476190493,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9835345773874844,
			"nilai_komparatif_unit": 0.9904270825040743,
			"summary_team": 0.8533333333333334
		}]
	},
	"700451": {
		"nik": "700451",
		"nama": "WINDARTI",
		"band": "IV",
		"posisi": "ASMAN BGES BIDDING MGT & OBL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8722222222222226,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.979733642154024,
			"nilai_komparatif_unit": 0.986599510723057,
			"summary_team": 0.8545454545454546
		}]
	},
	"700479": {
		"nik": "700479",
		"nama": "BUDIMAN",
		"band": "IV",
		"posisi": "ASMAN OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.870270270270269,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9958592132505191,
			"nilai_komparatif_unit": 1.0028380880969578,
			"summary_team": 0.8666666666666667
		}]
	},
	"700482": {
		"nik": "700482",
		"nama": "BENNI",
		"band": "IV",
		"posisi": "ASMAN COMMUNITY DEVELOPMENT CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8484848484848487,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.007142857142857,
			"nilai_komparatif_unit": 1.0142008065587615,
			"summary_team": 0.8545454545454546
		}]
	},
	"700486": {
		"nik": "700486",
		"nama": "SUPRIADI",
		"band": "IV",
		"posisi": "ASMAN COMMUNITY DEVELOPMENT CENTER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1538461538461553,
			"nilai_komparatif_unit": 1.1619321842735422,
			"summary_team": 1
		}]
	},
	"700515": {
		"nik": "700515",
		"nama": "MUHLIS",
		"band": "IV",
		"posisi": "ASMAN ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8944444444444447,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0163749294184075,
			"nilai_komparatif_unit": 1.0234975762093297,
			"summary_team": 0.9090909090909092
		}]
	},
	"700518": {
		"nik": "700518",
		"nama": "NUR HAKIM",
		"band": "IV",
		"posisi": "ASMAN ADVALJAR OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8227642276422753,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9075098814229262,
			"nilai_komparatif_unit": 0.9138696136020208,
			"summary_team": 0.7466666666666667
		}]
	},
	"700523": {
		"nik": "700523",
		"nama": "MACHMUDDIN",
		"band": "IV",
		"posisi": "ASMAN OM TRANSPORT, RADIO & MUX",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8529914529914517,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0641282565130277,
			"nilai_komparatif_unit": 1.0715855535123933,
			"summary_team": 0.9076923076923077
		}]
	},
	"700525": {
		"nik": "700525",
		"nama": "AGUS WINARTO",
		"band": "IV",
		"posisi": "ASMAN ACCESS PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8227642276422753,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0386273805246151,
			"nilai_komparatif_unit": 1.0459059701127023,
			"summary_team": 0.8545454545454546
		}]
	},
	"700533": {
		"nik": "700533",
		"nama": "NOERSIANA",
		"band": "IV",
		"posisi": "ASMAN FINANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8944444444444447,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9757199322416712,
			"nilai_komparatif_unit": 0.9825576731609565,
			"summary_team": 0.8727272727272728
		}]
	},
	"700536": {
		"nik": "700536",
		"nama": "MUHAJIR",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION CONSUMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8606060606060609,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9605633802816897,
			"nilai_komparatif_unit": 0.9672949057060288,
			"summary_team": 0.8266666666666667
		}]
	},
	"700543": {
		"nik": "700543",
		"nama": "TRI WIDIATI NINGRUM",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444447,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0334928229665068,
			"nilai_komparatif_unit": 1.040735430124434,
			"summary_team": 0.8727272727272727
		}]
	},
	"700558": {
		"nik": "700558",
		"nama": "EKO WAHYUDI",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8448087431693994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9833814309881576,
			"nilai_komparatif_unit": 0.9902728628711619,
			"summary_team": 0.8307692307692308
		}]
	},
	"700561": {
		"nik": "700561",
		"nama": "ISGAR",
		"band": "IV",
		"posisi": "OFF 1 FINANCE SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8944444444444447,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.955392433653303,
			"nilai_komparatif_unit": 0.9620877216367699,
			"summary_team": 0.8545454545454546
		}]
	},
	"700562": {
		"nik": "700562",
		"nama": "DJINTEN",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION EBIS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346935,
			"nilai_komparatif_unit": 0.9864567115465157,
			"summary_team": 0.8
		}]
	},
	"700564": {
		"nik": "700564",
		"nama": "SUSI NURSULASIAH",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8829268292682917,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8854846810647925,
			"nilai_komparatif_unit": 0.8916900629956573,
			"summary_team": 0.7818181818181817
		}]
	},
	"700577": {
		"nik": "700577",
		"nama": "MAKSUDI",
		"band": "IV",
		"posisi": "ASMAN BS DELIVERY & QUALITY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8419753086419747,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181826,
			"nilai_komparatif_unit": 0.9886986586182135,
			"summary_team": 0.8266666666666667
		}]
	},
	"700597": {
		"nik": "700597",
		"nama": "SRI MURTININGSIH",
		"band": "IV",
		"posisi": "ASMAN HUMAN CAPITAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8800000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9848484848484845,
			"nilai_komparatif_unit": 0.9917501976880218,
			"summary_team": 0.8666666666666667
		}]
	},
	"700617": {
		"nik": "700617",
		"nama": "HIDAYAT",
		"band": "IV",
		"posisi": "ASMAN FINANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8484848484848487,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857141,
			"nilai_komparatif_unit": 0.9710433254286016,
			"summary_team": 0.8181818181818182
		}]
	},
	"700619": {
		"nik": "700619",
		"nama": "MUGIYONO",
		"band": "IV",
		"posisi": "ASMAN SURVEILLANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8597701149425278,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9925133689839584,
			"nilai_komparatif_unit": 0.9994687965116584,
			"summary_team": 0.8533333333333334
		}]
	},
	"700624": {
		"nik": "700624",
		"nama": "KLIWON",
		"band": "IV",
		"posisi": "ASMAN DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7897435897435889,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9545454545454555,
			"nilai_komparatif_unit": 0.96123480698993,
			"summary_team": 0.7538461538461538
		}]
	},
	"700663": {
		"nik": "700663",
		"nama": "LILI MULYATI",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8370370370370368,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9398230088495578,
			"nilai_komparatif_unit": 0.9464091879693515,
			"summary_team": 0.7866666666666667
		}]
	},
	"710025": {
		"nik": "710025",
		"nama": "ASEP SUKARMA",
		"band": "IV",
		"posisi": "ASMAN BS OBL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8419753086419747,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9501466275659832,
			"nilai_komparatif_unit": 0.956805153501497,
			"summary_team": 0.8
		}]
	},
	"710026": {
		"nik": "710026",
		"nama": "BUDI TRIYONO",
		"band": "IV",
		"posisi": "ASMAN SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8450450450450468,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9466950959488253,
			"nilai_komparatif_unit": 0.953329433919952,
			"summary_team": 0.8
		}]
	},
	"710033": {
		"nik": "710033",
		"nama": "HENDI SURACHMAN",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION EBIS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8405797101449269,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.930094043887148,
			"nilai_komparatif_unit": 0.9366120434611237,
			"summary_team": 0.7818181818181819
		}]
	},
	"710038": {
		"nik": "710038",
		"nama": "MISWANTORO",
		"band": "IV",
		"posisi": "ASMAN IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8464912280701777,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8591615638247739,
			"nilai_komparatif_unit": 0.8651824761656185,
			"summary_team": 0.7272727272727273
		}]
	},
	"710047": {
		"nik": "710047",
		"nama": "SYARIFUDDIN",
		"band": "IV",
		"posisi": "ASMAN GES TERITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8450450450450468,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9649007708709181,
			"nilai_komparatif_unit": 0.9716626922645665,
			"summary_team": 0.8153846153846154
		}]
	},
	"710052": {
		"nik": "710052",
		"nama": "SUROSO",
		"band": "IV",
		"posisi": "OFF 1 QUALITY, CHANGE & RISK MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8619047619047618,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0969362129583125,
			"nilai_komparatif_unit": 1.1046234246072113,
			"summary_team": 0.9454545454545454
		}]
	},
	"710056": {
		"nik": "710056",
		"nama": "YUNI ADY",
		"band": "IV",
		"posisi": "ASMAN LOGISTIK & LEGAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254545454545452,
			"nilai_komparatif_unit": 1.0326408212234663,
			"summary_team": 0.8545454545454546
		}]
	},
	"710094": {
		"nik": "710094",
		"nama": "YADI SELAMET DRAJAT",
		"band": "IV",
		"posisi": "ASMAN DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8390804597701139,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9317559153175602,
			"nilai_komparatif_unit": 0.9382855611087615,
			"summary_team": 0.7818181818181817
		}]
	},
	"710097": {
		"nik": "710097",
		"nama": "HADI RAHARJO",
		"band": "IV",
		"posisi": "ASMAN DEBT MGT & COLLECTION PARTNERSHIP",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8461538461538464,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0545454545454542,
			"nilai_komparatif_unit": 1.0619355962936357,
			"summary_team": 0.8923076923076924
		}]
	},
	"710105": {
		"nik": "710105",
		"nama": "JANUARTO",
		"band": "IV",
		"posisi": "ASMAN PARTNERSHIP & AGENCY 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.851141552511417,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0467225907140052,
			"nilai_komparatif_unit": 1.0540579106692123,
			"summary_team": 0.8909090909090909
		}]
	},
	"710112": {
		"nik": "710112",
		"nama": "ERWANDI",
		"band": "IV",
		"posisi": "MGR SERVICE DELIVERY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8196969696969681,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9094269870609999,
			"nilai_komparatif_unit": 0.9158001541113469,
			"summary_team": 0.7454545454545455
		}]
	},
	"710114": {
		"nik": "710114",
		"nama": "ICHWAN KURNIA",
		"band": "IV",
		"posisi": "ASMAN SERVICE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8419753086419747,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0293255131964818,
			"nilai_komparatif_unit": 1.0365389162932883,
			"summary_team": 0.8666666666666667
		}]
	},
	"710172": {
		"nik": "710172",
		"nama": "KURNIADI",
		"band": "IV",
		"posisi": "ASMAN ACCESS PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8192592592592577,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0328279315621107,
			"nilai_komparatif_unit": 1.0400658792321946,
			"summary_team": 0.8461538461538461
		}]
	},
	"710177": {
		"nik": "710177",
		"nama": "ACHMAD SYAIPUDIN",
		"band": "IV",
		"posisi": "ASMAN DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8044444444444445,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9613259668508286,
			"nilai_komparatif_unit": 0.9680628364002757,
			"summary_team": 0.7733333333333333
		}]
	},
	"710184": {
		"nik": "710184",
		"nama": "HERU NURWOKO",
		"band": "IV",
		"posisi": "ASMAN OM TRANSPORT, RADIO & MUX",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333323,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440000000000013,
			"nilai_komparatif_unit": 0.950615451026994,
			"summary_team": 0.7866666666666667
		}]
	},
	"710188": {
		"nik": "710188",
		"nama": "NURDIN",
		"band": "IV",
		"posisi": "ASMAN PARTNERSHIP & AGENCY 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8448087431693994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9785252263906851,
			"nilai_komparatif_unit": 0.9853826265113043,
			"summary_team": 0.8266666666666667
		}]
	},
	"710190": {
		"nik": "710190",
		"nama": "RAHMAT ASWADI",
		"band": "IV",
		"posisi": "ASMAN BGES FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8200000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9756097560975607,
			"nilai_komparatif_unit": 0.9824467249142129,
			"summary_team": 0.8
		}]
	},
	"710219": {
		"nik": "710219",
		"nama": "ENDANG WIDIASTUTI",
		"band": "IV",
		"posisi": "OFF 1 CASH BANK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8684684684684674,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0049038098830643,
			"nilai_komparatif_unit": 1.0119460682952675,
			"summary_team": 0.8727272727272728
		}]
	},
	"710226": {
		"nik": "710226",
		"nama": "MARDIANI",
		"band": "IV",
		"posisi": "ASMAN PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8450450450450468,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1105461702476604,
			"nilai_komparatif_unit": 1.118328759021482,
			"summary_team": 0.9384615384615385
		}]
	},
	"710248": {
		"nik": "710248",
		"nama": "SRI MAENAWATI",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8419753086419747,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0451612903225815,
			"nilai_komparatif_unit": 1.0524856688516466,
			"summary_team": 0.88
		}]
	},
	"710258": {
		"nik": "710258",
		"nama": "IWAN SOESILO SOEHERMAN",
		"band": "IV",
		"posisi": "ASMAN DATA CENTER & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.870270270270269,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0418219461697737,
			"nilai_komparatif_unit": 1.049122922932202,
			"summary_team": 0.9066666666666666
		}]
	},
	"710267": {
		"nik": "710267",
		"nama": "WALUYO",
		"band": "IV",
		"posisi": "ASMAN WHOLESALE & WIFI ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8095238095238096,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0780748663101603,
			"nilai_komparatif_unit": 1.0856298996592137,
			"summary_team": 0.8727272727272727
		}]
	},
	"710275": {
		"nik": "710275",
		"nama": "MISBAH HIDAYAT",
		"band": "IV",
		"posisi": "ASMAN SALES SUPPORT 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666656,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9053254437869835,
			"nilai_komparatif_unit": 0.9116698676607793,
			"summary_team": 0.7846153846153847
		}]
	},
	"710276": {
		"nik": "710276",
		"nama": "RACHMAT",
		"band": "IV",
		"posisi": "ASMAN WHOLESALE & WIFI FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333323,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9920000000000013,
			"nilai_komparatif_unit": 0.9989518298927733,
			"summary_team": 0.8266666666666667
		}]
	},
	"710279": {
		"nik": "710279",
		"nama": "HENDRIYAN",
		"band": "IV",
		"posisi": "ASMAN DAVAL & INVENTORY MGT PL & REVIEW",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8000000000000005,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9772727272727267,
			"nilai_komparatif_unit": 0.9841213500134982,
			"summary_team": 0.7818181818181819
		}]
	},
	"710280": {
		"nik": "710280",
		"nama": "YULIANTO SAPUTRA",
		"band": "IV",
		"posisi": "ASMAN BS TERRITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8448087431693994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0115253439962362,
			"nilai_komparatif_unit": 1.0186140054112458,
			"summary_team": 0.8545454545454546
		}]
	},
	"710282": {
		"nik": "710282",
		"nama": "SUSANTO",
		"band": "IV",
		"posisi": "ASMAN OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8380952380952383,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9545454545454544,
			"nilai_komparatif_unit": 0.9612348069899289,
			"summary_team": 0.8
		}]
	},
	"710288": {
		"nik": "710288",
		"nama": "JAMALUDIN",
		"band": "IV",
		"posisi": "ASMAN COMMUNICATION & SECRETARIATE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8296296296296299,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.964285714285714,
			"nilai_komparatif_unit": 0.9710433254286015,
			"summary_team": 0.8
		}]
	},
	"710290": {
		"nik": "710290",
		"nama": "MAKMUN SANTOSA",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SERVICE 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8799999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.053719008264463,
			"nilai_komparatif_unit": 1.0611033583655063,
			"summary_team": 0.9272727272727272
		}]
	},
	"710300": {
		"nik": "710300",
		"nama": "SUPRIYANTO",
		"band": "IV",
		"posisi": "ASMAN CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8370370370370367,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9398230088495578,
			"nilai_komparatif_unit": 0.9464091879693515,
			"summary_team": 0.7866666666666666
		}]
	},
	"710312": {
		"nik": "710312",
		"nama": "TAMAN",
		"band": "IV",
		"posisi": "ASMAN DATA CENTER & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333323,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9600000000000013,
			"nilai_komparatif_unit": 0.9667275773155871,
			"summary_team": 0.8
		}]
	},
	"710316": {
		"nik": "710316",
		"nama": "LEDIYA HASTUTI",
		"band": "IV",
		"posisi": "ASMAN BS DELIVERY & QUALITY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.847222222222222,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.030104321907601,
			"nilai_komparatif_unit": 1.0373231828125513,
			"summary_team": 0.8727272727272728
		}]
	},
	"710317": {
		"nik": "710317",
		"nama": "AHMAD SUROJO",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8419753086419747,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9501466275659832,
			"nilai_komparatif_unit": 0.956805153501497,
			"summary_team": 0.8
		}]
	},
	"710326": {
		"nik": "710326",
		"nama": "SUPRI DASMINI",
		"band": "IV",
		"posisi": "ASMAN CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8784313725490193,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0683379120879124,
			"nilai_komparatif_unit": 1.0758247099032696,
			"summary_team": 0.9384615384615385
		}]
	},
	"710331": {
		"nik": "710331",
		"nama": "ARIF SETIAWAN",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE GANDARIA",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8507246376811579,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0344122657580939,
			"nilai_komparatif_unit": 1.0416613162727584,
			"summary_team": 0.88
		}]
	},
	"710332": {
		"nik": "710332",
		"nama": "PATRIADJI FORMAN",
		"band": "IV",
		"posisi": "OFF 1 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8448087431693994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0015921982286788,
			"nilai_komparatif_unit": 1.0086112492206278,
			"summary_team": 0.8461538461538461
		}]
	},
	"710333": {
		"nik": "710333",
		"nama": "RIZAL FAHLEVI",
		"band": "IV",
		"posisi": "ASMAN IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8529914529914517,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0231371834578262,
			"nilai_komparatif_unit": 1.0303072194017462,
			"summary_team": 0.8727272727272727
		}]
	},
	"710336": {
		"nik": "710336",
		"nama": "BENSON SINAGA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8829268292682917,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0502260170768472,
			"nilai_komparatif_unit": 1.0575858886692682,
			"summary_team": 0.9272727272727274
		}]
	},
	"710338": {
		"nik": "710338",
		"nama": "TEGUH HENDARSO",
		"band": "IV",
		"posisi": "ASMAN DEPLOY & SOLUTION PARTNERSHIP SPV",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8245614035087718,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0635024549918168,
			"nilai_komparatif_unit": 1.0709553664410594,
			"summary_team": 0.8769230769230769
		}]
	},
	"710349": {
		"nik": "710349",
		"nama": "SIHOL MARPAUNG",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE JATINEGARA",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8507246376811579,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9560477001703594,
			"nilai_komparatif_unit": 0.9627475801914886,
			"summary_team": 0.8133333333333334
		}]
	},
	"710355": {
		"nik": "710355",
		"nama": "HAMONANGAN SINAGA",
		"band": "IV",
		"posisi": "OFF 1 COMMAND CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8448087431693994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9469598965071147,
			"nilai_komparatif_unit": 0.95359609017223,
			"summary_team": 0.8
		}]
	},
	"710356": {
		"nik": "710356",
		"nama": "HASMULYANI",
		"band": "IV",
		"posisi": "ASMAN NTE MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8464912280701777,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8905540055799099,
			"nilai_komparatif_unit": 0.8967949127947469,
			"summary_team": 0.7538461538461538
		}]
	},
	"720005": {
		"nik": "720005",
		"nama": "RAHAYU RUSYANTATI",
		"band": "IV",
		"posisi": "ASMAN BGES PROJECT MGT & QUALITY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.84102564102564,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9512195121951231,
			"nilai_komparatif_unit": 0.957885556791359,
			"summary_team": 0.8
		}]
	},
	"720015": {
		"nik": "720015",
		"nama": "INDAH YANI",
		"band": "IV",
		"posisi": "ASMAN HUMAN CAPITAL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8285714285714286,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8931034482758623,
			"nilai_komparatif_unit": 0.8993622217124166,
			"summary_team": 0.7400000000000002
		}]
	},
	"720025": {
		"nik": "720025",
		"nama": "INDARTI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8419753086419747,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9501466275659832,
			"nilai_komparatif_unit": 0.956805153501497,
			"summary_team": 0.8
		}]
	},
	"720038": {
		"nik": "720038",
		"nama": "ENDANG SULISTYOWATI",
		"band": "IV",
		"posisi": "ASMAN FINANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444447,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9473684210526314,
			"nilai_komparatif_unit": 0.9540074776140648,
			"summary_team": 0.8
		}]
	},
	"810058": {
		"nik": "810058",
		"nama": "DYAH INDRAYANI RUM ASTUTI",
		"band": "IV",
		"posisi": "ASMAN GES TERITORY SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8829268292682917,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9266700150678062,
			"nilai_komparatif_unit": 0.9331640194140601,
			"summary_team": 0.8181818181818182
		}]
	},
	"830020": {
		"nik": "830020",
		"nama": "NOVI PUSPITASARI",
		"band": "IV",
		"posisi": "ASMAN PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8196969696969681,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0510450732262213,
			"nilai_komparatif_unit": 1.0584106846765284,
			"summary_team": 0.8615384615384616
		}]
	},
	"830025": {
		"nik": "830025",
		"nama": "RINI WAHYUNI",
		"band": "IV",
		"posisi": "OFF 1 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8431372549019593,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0350951374207207,
			"nilai_komparatif_unit": 1.0423489734269549,
			"summary_team": 0.8727272727272728
		}]
	},
	"840139": {
		"nik": "840139",
		"nama": "CITRA OCTAVIANA",
		"band": "IV",
		"posisi": "OFF 1 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0085763293310461,
			"nilai_komparatif_unit": 1.0156443243667173,
			"summary_team": 0.8909090909090909
		}]
	},
	"840146": {
		"nik": "840146",
		"nama": "NOVITA ANGGRAENI",
		"band": "IV",
		"posisi": "ASMAN BGES BIDDING MGT & OBL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.84102564102564,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9695121951219523,
			"nilai_komparatif_unit": 0.9763064328835005,
			"summary_team": 0.8153846153846154
		}]
	},
	"850013": {
		"nik": "850013",
		"nama": "EVA MARINI",
		"band": "IV",
		"posisi": "ASMAN CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666663,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0153846153846158,
			"nilai_komparatif_unit": 1.022500322160716,
			"summary_team": 0.88
		}]
	},
	"880074": {
		"nik": "880074",
		"nama": "IMAN FIRMANSYAH",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE BOGOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8431372549019593,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9305903398926672,
			"nilai_komparatif_unit": 0.9371118174559642,
			"summary_team": 0.7846153846153847
		}]
	},
	"890006": {
		"nik": "890006",
		"nama": "MARIANA",
		"band": "IV",
		"posisi": "MGR DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8431372549019593,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0350951374207207,
			"nilai_komparatif_unit": 1.0423489734269549,
			"summary_team": 0.8727272727272728
		}]
	},
	"890080": {
		"nik": "890080",
		"nama": "IDZHAR DIEN ISKANDAR",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8448087431693994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0015921982286788,
			"nilai_komparatif_unit": 1.0086112492206278,
			"summary_team": 0.8461538461538461
		}]
	},
	"890102": {
		"nik": "890102",
		"nama": "FARADILLA KARNESIA",
		"band": "IV",
		"posisi": "OFF 1 QUALITY, CHANGE& RISK MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.967409948542024,
			"nilai_komparatif_unit": 0.9741894539844025,
			"summary_team": 0.8545454545454546
		}]
	},
	"900029": {
		"nik": "900029",
		"nama": "ASTUTI HENING SITORESMI",
		"band": "IV",
		"posisi": "ASMAN SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8450450450450468,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9682108935840259,
			"nilai_komparatif_unit": 0.9749960119635873,
			"summary_team": 0.8181818181818182
		}]
	},
	"900030": {
		"nik": "900030",
		"nama": "VIVIN ANGELINA MENDROFA",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8448087431693994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0562244999502433,
			"nilai_komparatif_unit": 1.0636264082690257,
			"summary_team": 0.8923076923076922
		}]
	},
	"900035": {
		"nik": "900035",
		"nama": "RADEN RORO DEASY RAHINA KRISHNA DEWI",
		"band": "IV",
		"posisi": "ASMAN APARTMENT & PREMIUM CLUSTER 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8457516339869264,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0004755677089545,
			"nilai_komparatif_unit": 1.0074867934736593,
			"summary_team": 0.8461538461538461
		}]
	},
	"900055": {
		"nik": "900055",
		"nama": "NIKEN ASRI PRATIWI",
		"band": "IV",
		"posisi": "OFF 1 QUALITY, CHANGE & RISK MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8529914529914517,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0657678994352358,
			"nilai_komparatif_unit": 1.073236686876819,
			"summary_team": 0.9090909090909092
		}]
	},
	"900056": {
		"nik": "900056",
		"nama": "SIRAJUDDIN",
		"band": "IV",
		"posisi": "MGR ACCESS & SERVICE OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8569105691056935,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9515399211793871,
			"nilai_komparatif_unit": 0.9582082111675129,
			"summary_team": 0.8153846153846154
		}]
	},
	"900063": {
		"nik": "900063",
		"nama": "REZA PRADIKTA",
		"band": "IV",
		"posisi": "OFF 1 QUALITY, CHANGE & RISK MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8684684684684674,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9420973217653728,
			"nilai_komparatif_unit": 0.9486994390268132,
			"summary_team": 0.8181818181818182
		}]
	},
	"910019": {
		"nik": "910019",
		"nama": "MUHAMMAD NUR RAHMAN",
		"band": "IV",
		"posisi": "ASMAN PARTNERSHIP & AGENCY 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8457516339869264,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9640946379740835,
			"nilai_komparatif_unit": 0.9708509100746171,
			"summary_team": 0.8153846153846154
		}]
	},
	"910098": {
		"nik": "910098",
		"nama": "TYA FITRIE APRILYANTI",
		"band": "IV",
		"posisi": "OFF 1 PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0463821892393317,
			"nilai_komparatif_unit": 1.0537151236974145,
			"summary_team": 0.8545454545454546
		}]
	},
	"910102": {
		"nik": "910102",
		"nama": "INDITA AGUSTIANI YUSUF",
		"band": "IV",
		"posisi": "ASMAN CRM & CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8592592592592582,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9620689655172426,
			"nilai_komparatif_unit": 0.9688110419218706,
			"summary_team": 0.8266666666666667
		}]
	},
	"910121": {
		"nik": "910121",
		"nama": "SYAM HABIB PAMUNGKAS",
		"band": "IV",
		"posisi": "ASMAN SERVICE AREA KLD",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.851141552511417,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.049570815450642,
			"nilai_komparatif_unit": 1.056926095460149,
			"summary_team": 0.8933333333333333
		}]
	},
	"910143": {
		"nik": "910143",
		"nama": "REYNALDO NATHANAEL GULTOM",
		"band": "IV",
		"posisi": "OFF 1 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666663,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769236,
			"nilai_komparatif_unit": 0.929545747418833,
			"summary_team": 0.8
		}]
	},
	"920066": {
		"nik": "920066",
		"nama": "FIRRA MONICHA MUKHNERI",
		"band": "IV",
		"posisi": "ASMAN BS SALES ENGINEER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8453333333333325,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0555690366415929,
			"nilai_komparatif_unit": 1.0629663515436185,
			"summary_team": 0.8923076923076924
		}]
	},
	"920137": {
		"nik": "920137",
		"nama": "ABDUL GAFUR",
		"band": "IV",
		"posisi": "ASMAN DAVAL & INVENTORY MGT SUPERVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8192592592592577,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9764918625678138,
			"nilai_komparatif_unit": 0.9833350130922569,
			"summary_team": 0.8
		}]
	},
	"930010": {
		"nik": "930010",
		"nama": "VITA HASYANTIERSI",
		"band": "IV",
		"posisi": "ASMAN PROJECT DELIVERY & SUPERVISORY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8227642276422753,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9944304707150571,
			"nilai_komparatif_unit": 1.00139933308663,
			"summary_team": 0.8181818181818182
		}]
	},
	"930020": {
		"nik": "930020",
		"nama": "AULIA MUHAMMAD",
		"band": "IV",
		"posisi": "ASMAN HOME SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8619047619047618,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.974585635359116,
			"nilai_komparatif_unit": 0.9814154272471761,
			"summary_team": 0.84
		}]
	},
	"930082": {
		"nik": "930082",
		"nama": "DELIMA",
		"band": "IV",
		"posisi": "ASMAN PAYMENT COLLECTION CONSUMER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8461538461538464,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0099173553719007,
			"nilai_komparatif_unit": 1.0169947481746262,
			"summary_team": 0.8545454545454546
		}]
	},
	"930089": {
		"nik": "930089",
		"nama": "SAYID HASAN ASH SHIDDIQY",
		"band": "IV",
		"posisi": "OFF 1 COMMAND CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8928571428571421,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0589090909090917,
			"nilai_komparatif_unit": 1.066329812554162,
			"summary_team": 0.9454545454545454
		}]
	},
	"940017": {
		"nik": "940017",
		"nama": "CHINTYA FARADITA PUTRI",
		"band": "IV",
		"posisi": "ASMAN DIGITAL & WIFI SERVICE 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7611111111111114,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1227604512276042,
			"nilai_komparatif_unit": 1.130628636376058,
			"summary_team": 0.8545454545454546
		}]
	},
	"940069": {
		"nik": "940069",
		"nama": "RAHMANDA NOFAL ARIF",
		"band": "IV",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CIPETE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8676190476190493,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0058876359644726,
			"nilai_komparatif_unit": 1.0129367889246215,
			"summary_team": 0.8727272727272727
		}]
	}
};