export const cbark7 = {
	"660265": {
		"nik": "660265",
		"nama": "GIGIH HASTANU, SMB",
		"band": "III",
		"posisi": "MGR SECRETARIATE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.911111111111111,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9951219512195123,
			"nilai_komparatif_unit": 1.0003952384446186,
			"summary_team": 0.9066666666666666
		}]
	},
	"660496": {
		"nik": "660496",
		"nama": "MULYADI",
		"band": "III",
		"posisi": "MGR FINANCE SERVICE (DPD)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8646153846153859,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0987544483985747,
			"nilai_komparatif_unit": 1.1045768983899236,
			"summary_team": 0.95
		}]
	},
	"660653": {
		"nik": "660653",
		"nama": "DIKHYAH DARIRUDIN",
		"band": "III",
		"posisi": "MGR HC INTEGRATED SOLUTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7888888888888886,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.014084507042254,
			"nilai_komparatif_unit": 1.0194582794423208,
			"summary_team": 0.8
		}]
	},
	"670019": {
		"nik": "670019",
		"nama": "SRI WAHYUNI, SAB, ME",
		"band": "III",
		"posisi": "MGR PERBENDAHARAAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8294117647058813,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9645390070921998,
			"nilai_komparatif_unit": 0.9696502311165345,
			"summary_team": 0.7999999999999999
		}]
	},
	"670026": {
		"nik": "670026",
		"nama": "RIZKI FIRMAN",
		"band": "III",
		"posisi": "MGR ASSET & FACILITY MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8411764705882361,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300691,
			"nilai_komparatif_unit": 1.0755997756004194,
			"summary_team": 0.9000000000000001
		}]
	},
	"670027": {
		"nik": "670027",
		"nama": "MALIKOESWARI S.",
		"band": "III",
		"posisi": "SO SECURITY & SAFETY SPV & REPORTING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8206349206349192,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9461827284105148,
			"nilai_komparatif_unit": 0.9511966800053206,
			"summary_team": 0.776470588235294
		}]
	},
	"670049": {
		"nik": "670049",
		"nama": "DENY MAULANA",
		"band": "III",
		"posisi": "KABAG LOGISTIK & SARANA PENUNJANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8888888888888888,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.02,
			"nilai_komparatif_unit": 1.025405119405734,
			"summary_team": 0.9066666666666666
		}]
	},
	"670054": {
		"nik": "670054",
		"nama": "FAJAR HARIANTO",
		"band": "III",
		"posisi": "AUDITOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8400000000000001,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0714285714285712,
			"nilai_komparatif_unit": 1.0771062178631656,
			"summary_team": 0.8999999999999999
		}]
	},
	"670064": {
		"nik": "670064",
		"nama": "JEFFY WENSDI LUMESAR",
		"band": "III",
		"posisi": "MGR PROCUREMENT & LOGISTIC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.733333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8429752066115707,
			"nilai_komparatif_unit": 0.8474422474427558,
			"summary_team": 0.6181818181818182
		}]
	},
	"670083": {
		"nik": "670083",
		"nama": "NANAN HERNAWAN W.",
		"band": "III",
		"posisi": "MGR PERSONNEL COST PLAN & CONTROL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0105263157894746,
			"nilai_komparatif_unit": 1.0158812328477869,
			"summary_team": 0.8421052631578947
		}]
	},
	"670094": {
		"nik": "670094",
		"nama": "SUMARSONO",
		"band": "III",
		"posisi": "SO CULTURAL & CHANGE MGT EVALUATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8277777777777777,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0659297275957362,
			"nilai_komparatif_unit": 1.0715782349053211,
			"summary_team": 0.8823529411764706
		}]
	},
	"670102": {
		"nik": "670102",
		"nama": "IWAN SUSIAWAN",
		"band": "III",
		"posisi": "MANAGER LEARNING DEVT DIGITAL PLATFORM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8757575757575752,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9991349480968865,
			"nilai_komparatif_unit": 1.0044295007409116,
			"summary_team": 0.875
		}]
	},
	"670186": {
		"nik": "670186",
		"nama": "DARWIN",
		"band": "III",
		"posisi": "MANAGER LEARNING DEVT DIG TELECOMM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8980392156862743,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96042576419214,
			"nilai_komparatif_unit": 0.9655151915801811,
			"summary_team": 0.8625
		}]
	},
	"670200": {
		"nik": "670200",
		"nama": "KENCANA WULAN",
		"band": "III",
		"posisi": "SO MEDIA RELATIONSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8911111111111103,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0825876485257453,
			"nilai_komparatif_unit": 1.0883244284350144,
			"summary_team": 0.9647058823529411
		}]
	},
	"670214": {
		"nik": "670214",
		"nama": "AGUS HERIYANTO",
		"band": "III",
		"posisi": "SO SERVICE DELIVERY AUDIT REPORTING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9164334266293429,
			"nilai_komparatif_unit": 0.9212897326081052,
			"summary_team": 0.7999999999999998
		}]
	},
	"670217": {
		"nik": "670217",
		"nama": "ATY KUSTIATI S",
		"band": "III",
		"posisi": "MANAGER LEARNING DEVELOPMENT MEDIA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8980392156862743,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9250923748740345,
			"nilai_komparatif_unit": 0.9299945658029838,
			"summary_team": 0.8307692307692308
		}]
	},
	"670221": {
		"nik": "670221",
		"nama": "FLORENCE SALIKUNNA T,IR",
		"band": "III",
		"posisi": "SENIOR STAFF III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9257142857142865,
			"nilai_komparatif_unit": 0.9306197722337762,
			"summary_team": 0.7714285714285714
		}]
	},
	"670280": {
		"nik": "670280",
		"nama": "MULYADI",
		"band": "III",
		"posisi": "SO BUDGET MONITORING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8235294117647056,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8714285714285716,
			"nilai_komparatif_unit": 0.8760463905287085,
			"summary_team": 0.7176470588235293
		}]
	},
	"670326": {
		"nik": "670326",
		"nama": "MUHANI",
		"band": "III",
		"posisi": "MGR HC INTEGRATED SOLUTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8708333333333331,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9395389299695521,
			"nilai_komparatif_unit": 0.9445176751683962,
			"summary_team": 0.8181818181818181
		}]
	},
	"670327": {
		"nik": "670327",
		"nama": "LELI OKTAVIANI",
		"band": "III",
		"posisi": "SO BUDGET PLANNING & CONTROL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8206349206349192,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9870406189555144,
			"nilai_komparatif_unit": 0.9922710820964596,
			"summary_team": 0.81
		}]
	},
	"670341": {
		"nik": "670341",
		"nama": "BENEDICTUS HARJO BASKORO, IR.MT.",
		"band": "III",
		"posisi": "SENIOR RESEARCHER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.002960796204468,
			"nilai_komparatif_unit": 1.0082756225405025,
			"summary_team": 0.8352941176470587
		}]
	},
	"670346": {
		"nik": "670346",
		"nama": "NINDYA SUMANTRI, IR, MM.",
		"band": "III",
		"posisi": "MANAGER TCU AREA-5",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.979472464407897,
			"nilai_komparatif_unit": 0.9846628228635375,
			"summary_team": 0.8352941176470587
		}]
	},
	"670618": {
		"nik": "670618",
		"nama": "ATMASARI, BA.SH.LL.M",
		"band": "III",
		"posisi": "SO PERDATA & NON LITIGASI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8392156862745095,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9532710280373833,
			"nilai_komparatif_unit": 0.9583225415006861,
			"summary_team": 0.7999999999999998
		}]
	},
	"670624": {
		"nik": "670624",
		"nama": "DIAH INDAH FITRI,SE",
		"band": "III",
		"posisi": "MGR NON TAX OBLIGATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8646153846153859,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.049822064056938,
			"nilai_komparatif_unit": 1.0553852146559595,
			"summary_team": 0.9076923076923077
		}]
	},
	"680010": {
		"nik": "680010",
		"nama": "EMA EVIATIN",
		"band": "III",
		"posisi": "MGR TAX OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.933333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9890109890109893,
			"nilai_komparatif_unit": 0.9942518934121535,
			"summary_team": 0.923076923076923
		}]
	},
	"680031": {
		"nik": "680031",
		"nama": "HENDI KURNIA, M.M",
		"band": "III",
		"posisi": "MANAGER LOG & PROC RESEARCH & INNOVATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8411764705882361,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0629370629370618,
			"nilai_komparatif_unit": 1.068569711707606,
			"summary_team": 0.8941176470588235
		}]
	},
	"680055": {
		"nik": "680055",
		"nama": "KHAIRUL FAHMI",
		"band": "III",
		"posisi": "SO REGIONAL PROCESS CONTROL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365351,
			"nilai_komparatif_unit": 0.9932286496110428,
			"summary_team": 0.8727272727272727
		}]
	},
	"680056": {
		"nik": "680056",
		"nama": "AINUR ROFIQ",
		"band": "III",
		"posisi": "MGR DISTRIBUSI BL TELKOM GROUP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8235294117647057,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.942857142857143,
			"nilai_komparatif_unit": 0.9478534717195861,
			"summary_team": 0.776470588235294
		}]
	},
	"680108": {
		"nik": "680108",
		"nama": "MELITA",
		"band": "III",
		"posisi": "MGR CASH OPERATION (NITS & DSS)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8646153846153859,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.069839857651244,
			"nilai_komparatif_unit": 1.0755090852743994,
			"summary_team": 0.925
		}]
	},
	"680120": {
		"nik": "680120",
		"nama": "SUMARNA",
		"band": "III",
		"posisi": "MGR HC SERVICE AREA (BANDUNG)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.846296296296295,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0339168490153188,
			"nilai_komparatif_unit": 1.0393957157060325,
			"summary_team": 0.875
		}]
	},
	"680181": {
		"nik": "680181",
		"nama": "ANIS SIRWAN ZUKHRUFI",
		"band": "III",
		"posisi": "SENIOR INNOVATOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9605821710127298,
			"nilai_komparatif_unit": 0.9656724272218896,
			"summary_team": 0.7999999999999998
		}]
	},
	"680202": {
		"nik": "680202",
		"nama": "ERYANTO SETIADI",
		"band": "III",
		"posisi": "SO EMPLOYEE COC/ETHICS & COMPLIANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666657,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006993006993008,
			"nilai_komparatif_unit": 1.0123292005651023,
			"summary_team": 0.8727272727272727
		}]
	},
	"680217": {
		"nik": "680217",
		"nama": "ARDIMAS",
		"band": "III",
		"posisi": "MGR INTERNAL FIXED ASSET SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8583333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.041690462592805,
			"nilai_komparatif_unit": 1.0472105227243034,
			"summary_team": 0.8941176470588235
		}]
	},
	"680363": {
		"nik": "680363",
		"nama": "HARJONO",
		"band": "III",
		"posisi": "MGR QUALITY & CHANGE MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8318840579710138,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8929815828770538,
			"nilai_komparatif_unit": 0.8977136143305555,
			"summary_team": 0.7428571428571427
		}]
	},
	"690064": {
		"nik": "690064",
		"nama": "ANDRI",
		"band": "III",
		"posisi": "MGR CASH OPERATION (DES)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8469696969696957,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.007513416815744,
			"nilai_komparatif_unit": 1.0128523681106147,
			"summary_team": 0.8533333333333334
		}]
	},
	"690102": {
		"nik": "690102",
		"nama": "ARIF WURYANTO",
		"band": "III",
		"posisi": "SO BUSINESS & PRODUCT LINE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555552,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649353,
			"nilai_komparatif_unit": 0.9400199719533087,
			"summary_team": 0.7999999999999998
		}]
	},
	"690192": {
		"nik": "690192",
		"nama": "SUMINI",
		"band": "III",
		"posisi": "MANAGER LOG & PROC LEARNING & FULFILL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8411764705882361,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0144522144522135,
			"nilai_komparatif_unit": 1.019827935384101,
			"summary_team": 0.8533333333333333
		}]
	},
	"690312": {
		"nik": "690312",
		"nama": "SULAIMAN",
		"band": "III",
		"posisi": "MGR DIGITAL SERVICE & WIRELESS BILLING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8825396825396817,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9550359712230223,
			"nilai_komparatif_unit": 0.9600968373614847,
			"summary_team": 0.8428571428571427
		}]
	},
	"690379": {
		"nik": "690379",
		"nama": "SYEPTADJI",
		"band": "III",
		"posisi": "MGR HC SOLUTION AREA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.846296296296295,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9768052516411394,
			"nilai_komparatif_unit": 0.9819814761717948,
			"summary_team": 0.8266666666666667
		}]
	},
	"690426": {
		"nik": "690426",
		"nama": "IHWAN DJAYANTO",
		"band": "III",
		"posisi": "MGR TAX OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8469696969696957,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0535296546030013,
			"nilai_komparatif_unit": 1.0591124522310513,
			"summary_team": 0.8923076923076922
		}]
	},
	"690487": {
		"nik": "690487",
		"nama": "ADI SUBROTO",
		"band": "III",
		"posisi": "MANAGER LEARNING DEVT CONS BUSINESS&GTM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8703703703703702,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.919148936170213,
			"nilai_komparatif_unit": 0.9240196320051673,
			"summary_team": 0.8
		}]
	},
	"690598": {
		"nik": "690598",
		"nama": "MERIE HERYANI, SH",
		"band": "III",
		"posisi": "SO COMMERCIAL & OPERATIONAL CONTRACT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.856249999999999,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9755259768140844,
			"nilai_komparatif_unit": 0.9806954222925897,
			"summary_team": 0.8352941176470587
		}]
	},
	"690605": {
		"nik": "690605",
		"nama": "SUSILAWATI",
		"band": "III",
		"posisi": "KABAG HUKUM & KEPATUHAN",
		"category": [{
			"code": "BS-3-01",
			"kriteria": 0.7466666666666666,
			"kriteria_bp": 0.995439146985193,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9821428571428573,
			"nilai_komparatif_unit": 0.9866427898856449,
			"summary_team": 0.7333333333333334
		}]
	},
	"690609": {
		"nik": "690609",
		"nama": "AGUS SUPRIJANTO, S.SI",
		"band": "III",
		"posisi": "SO ENTERPRISE & SUBSIDIARIES RISK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8259259259259255,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9686098654708523,
			"nilai_komparatif_unit": 0.973742661530109,
			"summary_team": 0.7999999999999998
		}]
	},
	"700262": {
		"nik": "700262",
		"nama": "LAURA FERYANTI",
		"band": "III",
		"posisi": "MGR SECRETARIATE & ADMINISTRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8318840579710138,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0465259274441492,
			"nilai_komparatif_unit": 1.052071611364769,
			"summary_team": 0.8705882352941177
		}]
	},
	"700506": {
		"nik": "700506",
		"nama": "RAHMAT HIDAYAT",
		"band": "III",
		"posisi": "SO EXECUTIVE DATA & REPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8355555555555556,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1097678916827851,
			"nilai_komparatif_unit": 1.1156487034153293,
			"summary_team": 0.9272727272727272
		}]
	},
	"700643": {
		"nik": "700643",
		"nama": "INDAH PERMATASARI NOEGROHO POETRI",
		"band": "III",
		"posisi": "SO TREASURY MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0180995475113124,
			"nilai_komparatif_unit": 1.0234945961595696,
			"summary_team": 0.8823529411764706
		}]
	},
	"700650": {
		"nik": "700650",
		"nama": "PURWO ARIANDONO",
		"band": "III",
		"posisi": "MANAGER DIGITAL LEARNING DESIGN & EXP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8703703703703702,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9651063829787236,
			"nilai_komparatif_unit": 0.9702206136054257,
			"summary_team": 0.8400000000000001
		}]
	},
	"700654": {
		"nik": "700654",
		"nama": "DIKDIK KOSTAMAN,ST",
		"band": "III",
		"posisi": "MANAGER LEARNING DEVT HC & GENERAL AFF",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.005000000000001,
			"nilai_komparatif_unit": 1.0103256323556506,
			"summary_team": 0.8375
		}]
	},
	"700656": {
		"nik": "700656",
		"nama": "AHMAD SUMADI, SE",
		"band": "III",
		"posisi": "MGR ASSET SETTLEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8472222222222213,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.985920925747349,
			"nilai_komparatif_unit": 0.9911454554809532,
			"summary_team": 0.8352941176470587
		}]
	},
	"700667": {
		"nik": "700667",
		"nama": "NURBAYANTI",
		"band": "III",
		"posisi": "MGR FINANCE SERVICE (EBIS)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8469696969696957,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0089445438282663,
			"nilai_komparatif_unit": 1.0142910788607717,
			"summary_team": 0.8545454545454546
		}]
	},
	"700672": {
		"nik": "700672",
		"nama": "ANWAR ZUBAIDI, ST",
		"band": "III",
		"posisi": "MGR DISCIPLINARY ACTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8388888888888891,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0172185430463572,
			"nilai_komparatif_unit": 1.0226089231315458,
			"summary_team": 0.8533333333333333
		}]
	},
	"700673": {
		"nik": "700673",
		"nama": "HERU RAMDHANI",
		"band": "III",
		"posisi": "SENIOR RESEARCHER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.029195183227925,
			"nilai_komparatif_unit": 1.0346490291663104,
			"summary_team": 0.857142857142857
		}]
	},
	"700675": {
		"nik": "700675",
		"nama": "ERLIANA SRI LESTARI",
		"band": "III",
		"posisi": "SO CULTURAL DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8277777777777777,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8811685748124753,
			"nilai_komparatif_unit": 0.8858380075217321,
			"summary_team": 0.7294117647058823
		}]
	},
	"710018": {
		"nik": "710018",
		"nama": "JUWARTONO",
		"band": "III",
		"posisi": "SO HOUSEKEEPING MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8368421052631614,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9981502034776132,
			"nilai_komparatif_unit": 1.0034395378253134,
			"summary_team": 0.8352941176470587
		}]
	},
	"710036": {
		"nik": "710036",
		"nama": "IWAN SETIARAMAH",
		"band": "III",
		"posisi": "MGR CREDIT CONTROL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8765432098765427,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.001408450704226,
			"nilai_komparatif_unit": 1.0067150509492921,
			"summary_team": 0.8777777777777778
		}]
	},
	"710049": {
		"nik": "710049",
		"nama": "SUGIARTO",
		"band": "III",
		"posisi": "MGR CASH OPERATION (SUPP CONS)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8761904761904756,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0347826086956529,
			"nilai_komparatif_unit": 1.040266063165238,
			"summary_team": 0.9066666666666666
		}]
	},
	"710059": {
		"nik": "710059",
		"nama": "EDI ROHEDI",
		"band": "III",
		"posisi": "SO DELIVERY SERVICE & ASSURANCE II",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0178571428571426,
			"nilai_komparatif_unit": 1.0232509069700073,
			"summary_team": 0.8142857142857141
		}]
	},
	"710175": {
		"nik": "710175",
		"nama": "SUBANI WIDODO RAHARDJO",
		"band": "III",
		"posisi": "SO EVENT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8368421052631614,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9981502034776132,
			"nilai_komparatif_unit": 1.0034395378253134,
			"summary_team": 0.8352941176470587
		}]
	},
	"710231": {
		"nik": "710231",
		"nama": "WIDAYANTI",
		"band": "III",
		"posisi": "MGR SPOT BUYING CATEGORY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8450980392156857,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9762180974477964,
			"nilai_komparatif_unit": 0.9813912105681335,
			"summary_team": 0.825
		}]
	},
	"710376": {
		"nik": "710376",
		"nama": "SUPRIYONO,ST",
		"band": "III",
		"posisi": "MGR ANGGARAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8294117647058813,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0162107396149964,
			"nilai_komparatif_unit": 1.0215957792120633,
			"summary_team": 0.842857142857143
		}]
	},
	"710386": {
		"nik": "710386",
		"nama": "ERNI AMBARSARI",
		"band": "III",
		"posisi": "SO CONTENT MASTER NON FINANCIAL DATA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8499999999999998,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9965397923875436,
			"nilai_komparatif_unit": 1.001820592946779,
			"summary_team": 0.8470588235294119
		}]
	},
	"710398": {
		"nik": "710398",
		"nama": "ELY AGUS TRI SULISTYO PRIANTO",
		"band": "III",
		"posisi": "MGR WHOLESALE BILLING DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8804597701149418,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.04223621563508,
			"nilai_komparatif_unit": 1.0477591677865388,
			"summary_team": 0.9176470588235295
		}]
	},
	"710406": {
		"nik": "710406",
		"nama": "TRANS TITI TJENDRAWATI",
		"band": "III",
		"posisi": "MGR HC DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0125000000000002,
			"nilai_komparatif_unit": 1.017865375880692,
			"summary_team": 0.9000000000000002
		}]
	},
	"710439": {
		"nik": "710439",
		"nama": "LAKSONO SUGENG SANTOSO",
		"band": "III",
		"posisi": "MGR BUDGET OPEX GROUP BUSINESS 02",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8463768115942021,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0008058017727648,
			"nilai_komparatif_unit": 1.0061092084987777,
			"summary_team": 0.8470588235294118
		}]
	},
	"710456": {
		"nik": "710456",
		"nama": "SADIYO",
		"band": "III",
		"posisi": "MGR STRATEGIC PROJECT PLANNING &ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8422222222222211,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.047648610895547,
			"nilai_komparatif_unit": 1.0532002440692154,
			"summary_team": 0.8823529411764706
		}]
	},
	"710467": {
		"nik": "710467",
		"nama": "RUSMITA LELYANAWATI",
		"band": "III",
		"posisi": "SO TREG TRANSFORMATION RELATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9555555555555555,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0465116279069768,
			"nilai_komparatif_unit": 1.0520572360523948,
			"summary_team": 1
		}]
	},
	"710473": {
		"nik": "710473",
		"nama": "DUMA IRENE PASARIBU, SE, AKT",
		"band": "III",
		"posisi": "SO PROFITABILITY ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8499999999999998,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8403361344537815,
			"nilai_komparatif_unit": 0.8447891904809145,
			"summary_team": 0.7142857142857141
		}]
	},
	"710476": {
		"nik": "710476",
		"nama": "HELMI RAHMAYADI",
		"band": "III",
		"posisi": "KABAG PELAYANAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8500000000000003,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.991596638655462,
			"nilai_komparatif_unit": 0.9968512447674789,
			"summary_team": 0.842857142857143
		}]
	},
	"710513": {
		"nik": "710513",
		"nama": "ADE IRVAN FADLI NASUTION",
		"band": "III",
		"posisi": "MGR PROFESIONNAL SERVICE CATEGORY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8450980392156857,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0719257540603253,
			"nilai_komparatif_unit": 1.0776060351336365,
			"summary_team": 0.9058823529411764
		}]
	},
	"720002": {
		"nik": "720002",
		"nama": "NOVA DESWIRA",
		"band": "III",
		"posisi": "MGR GENERAL AFFAIR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8422222222222211,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0057426664597249,
			"nilai_komparatif_unit": 1.0110722343064467,
			"summary_team": 0.8470588235294116
		}]
	},
	"720066": {
		"nik": "720066",
		"nama": "MERY AFIORITA, ST",
		"band": "III",
		"posisi": "MGR FINANCE SERVICE BUSINESS 05",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.862015503875968,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0005620503597135,
			"nilai_komparatif_unit": 1.0058641654136746,
			"summary_team": 0.8625
		}]
	},
	"720134": {
		"nik": "720134",
		"nama": "ISWATOEN HASANAH",
		"band": "III",
		"posisi": "SO INSURANCE MANAGEMENT & BCM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.826666666666666,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0246679316888052,
			"nilai_komparatif_unit": 1.0300977871025352,
			"summary_team": 0.8470588235294116
		}]
	},
	"720137": {
		"nik": "720137",
		"nama": "HUMI ERMA PRATIWI, ST.",
		"band": "III",
		"posisi": "SO GCT CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0241187384044523,
			"nilai_komparatif_unit": 1.0295456835679089,
			"summary_team": 0.8363636363636363
		}]
	},
	"720174": {
		"nik": "720174",
		"nama": "IRMA DEWIJANI GUSTIKA",
		"band": "III",
		"posisi": "MANAGER LEARNING DEVT DIGL LEADERSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8749999999999997,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9571428571428575,
			"nilai_komparatif_unit": 0.962214887957762,
			"summary_team": 0.8375
		}]
	},
	"720176": {
		"nik": "720176",
		"nama": "ASHAR",
		"band": "III",
		"posisi": "SENIOR EXPERT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.938750758035168,
			"nilai_komparatif_unit": 0.9437253266032106,
			"summary_team": 0.7818181818181819
		}]
	},
	"720181": {
		"nik": "720181",
		"nama": "HELDIANA PURWANINGSIH",
		"band": "III",
		"posisi": "SO HC EFFECTIVENESS MEASUREMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8484848484848487,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8923469387755097,
			"nilai_komparatif_unit": 0.8970756071631792,
			"summary_team": 0.757142857142857
		}]
	},
	"720200": {
		"nik": "720200",
		"nama": "MUHAMMAD SIGIT PRAMUDYA",
		"band": "III",
		"posisi": "PRINCIPAL INNOVATOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8471014492753652,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0087876195660592,
			"nilai_komparatif_unit": 1.0141333230354521,
			"summary_team": 0.8545454545454545
		}]
	},
	"720248": {
		"nik": "720248",
		"nama": "GEDE AGUS PUTRAWIRAWAN, ST",
		"band": "III",
		"posisi": "MGR BUSINESS PLANNING & PERFORMANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8509803921568628,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0282258064516128,
			"nilai_komparatif_unit": 1.0336745155299736,
			"summary_team": 0.875
		}]
	},
	"720260": {
		"nik": "720260",
		"nama": "TATI KRISNAYANTI, ST",
		"band": "III",
		"posisi": "SO RISK POLICY & ERM APPLICATION SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.826666666666666,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0104364326375719,
			"nilai_komparatif_unit": 1.0157908733927778,
			"summary_team": 0.8352941176470587
		}]
	},
	"720272": {
		"nik": "720272",
		"nama": "ATO RIYANTO",
		"band": "III",
		"posisi": "SO INFRASTURCTURE & SUPPLY AUDIT REPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8166666666666669,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0686456400742113,
			"nilai_komparatif_unit": 1.0743085393752094,
			"summary_team": 0.8727272727272727
		}]
	},
	"720310": {
		"nik": "720310",
		"nama": "TERRI ASTRADIKA",
		"band": "III",
		"posisi": "SO CAPITAL PLANNING & ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.857575757575757,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0426106838495122,
			"nilai_komparatif_unit": 1.0481356203592187,
			"summary_team": 0.8941176470588235
		}]
	},
	"720322": {
		"nik": "720322",
		"nama": "AMILICIA ADI HARTANTI",
		"band": "III",
		"posisi": "SENIOR LEARNING ANALYST",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9380863039399576,
			"nilai_komparatif_unit": 0.9430573514749372,
			"summary_team": 0.7999999999999998
		}]
	},
	"720325": {
		"nik": "720325",
		"nama": "RUDI SUDIRO MURBONEGORO",
		"band": "III",
		"posisi": "SO FINANCIAL & ICFR RISK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8259259259259255,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.982854128198365,
			"nilai_komparatif_unit": 0.9880624065526109,
			"summary_team": 0.8117647058823529
		}]
	},
	"720346": {
		"nik": "720346",
		"nama": "ANNA MULYANI",
		"band": "III",
		"posisi": "MGR DISTRIBUSI & PEMBINAAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.826666666666667,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9677419354838703,
			"nilai_komparatif_unit": 0.9728701322635042,
			"summary_team": 0.7999999999999998
		}]
	},
	"720355": {
		"nik": "720355",
		"nama": "TINA RAHMAWATI",
		"band": "III",
		"posisi": "MGR DIGITAL PAYMENT MODE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.852941176470587,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8965517241379323,
			"nilai_komparatif_unit": 0.9013026742579149,
			"summary_team": 0.7647058823529411
		}]
	},
	"720361": {
		"nik": "720361",
		"nama": "RUDIYANTO",
		"band": "III",
		"posisi": "MGR PENGELOLAAN PIUTANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.826666666666667,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.088709677419354,
			"nilai_komparatif_unit": 1.094478898796442,
			"summary_team": 0.8999999999999998
		}]
	},
	"720396": {
		"nik": "720396",
		"nama": "DENNY KUSTONO",
		"band": "III",
		"posisi": "SENIOR EXPERT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9605821710127298,
			"nilai_komparatif_unit": 0.9656724272218896,
			"summary_team": 0.7999999999999998
		}]
	},
	"720421": {
		"nik": "720421",
		"nama": "JOHNI PURWANTORO",
		"band": "III",
		"posisi": "MGR BILLING INFO & CONVERGENT INVOICING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.852941176470587,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.034482758620691,
			"nilai_komparatif_unit": 1.039964624143748,
			"summary_team": 0.8823529411764706
		}]
	},
	"720423": {
		"nik": "720423",
		"nama": "YADI SUPRIYADI",
		"band": "III",
		"posisi": "MGR OPERATION FRAUD MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8722222222222213,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0520794304983148,
			"nilai_komparatif_unit": 1.0576545431906286,
			"summary_team": 0.9176470588235293
		}]
	},
	"720430": {
		"nik": "720430",
		"nama": "DODI HARYANTO",
		"band": "III",
		"posisi": "MGR SEKRETARIAT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8793650793650787,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1087545126353797,
			"nilai_komparatif_unit": 1.114629954333851,
			"summary_team": 0.9749999999999999
		}]
	},
	"720431": {
		"nik": "720431",
		"nama": "EDI WIDIARSO",
		"band": "III",
		"posisi": "MGR DISTRIBUSI BINA LINGKUNGAN TELKOM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8235294117647057,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0142857142857145,
			"nilai_komparatif_unit": 1.0196605529104639,
			"summary_team": 0.8352941176470587
		}]
	},
	"720437": {
		"nik": "720437",
		"nama": "NOVERI WANDY",
		"band": "III",
		"posisi": "MGR BUDGET REPORTING & ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8463768115942021,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.052615193026153,
			"nilai_komparatif_unit": 1.0581931447973256,
			"summary_team": 0.8909090909090909
		}]
	},
	"720440": {
		"nik": "720440",
		"nama": "HARTONO, ST",
		"band": "III",
		"posisi": "SO GCT CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.816666666666667,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649346,
			"nilai_komparatif_unit": 0.940019971953308,
			"summary_team": 0.7636363636363636
		}]
	},
	"720456": {
		"nik": "720456",
		"nama": "MAMET SANTOSA",
		"band": "III",
		"posisi": "MGR COSUMER BILLING DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8825396825396817,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0110680686220264,
			"nilai_komparatif_unit": 1.016425856502641,
			"summary_team": 0.8923076923076924
		}]
	},
	"720473": {
		"nik": "720473",
		"nama": "PURNAMA RAHARDJA",
		"band": "III",
		"posisi": "SENIOR EXPERT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8405093996361386,
			"nilai_komparatif_unit": 0.8449633738191534,
			"summary_team": 0.6999999999999998
		}]
	},
	"720492": {
		"nik": "720492",
		"nama": "YUWANA , MM",
		"band": "III",
		"posisi": "SO MONITORING & RISK ADM PARTNER 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139859,
			"nilai_komparatif_unit": 0.9912390088866617,
			"summary_team": 0.8545454545454546
		}]
	},
	"720496": {
		"nik": "720496",
		"nama": "SIGIT SUPRAPTO",
		"band": "III",
		"posisi": "SO COMPLIANCE A&A AUDIT REPORTING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8266666666666668,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0195852534562209,
			"nilai_komparatif_unit": 1.024988175063335,
			"summary_team": 0.8428571428571427
		}]
	},
	"720528": {
		"nik": "720528",
		"nama": "MUHAMMAD YUSRON NUR SHODIQ, ST",
		"band": "III",
		"posisi": "MGR SYSTEM COMPLIANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555548,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272736,
			"nilai_komparatif_unit": 1.028146844323932,
			"summary_team": 0.875
		}]
	},
	"720540": {
		"nik": "720540",
		"nama": "SARWONO",
		"band": "III",
		"posisi": "MANAGER DIG LEARNING DESIGN & EXP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8980392156862743,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9825327510917032,
			"nilai_komparatif_unit": 0.9877393264247375,
			"summary_team": 0.8823529411764706
		}]
	},
	"720582": {
		"nik": "720582",
		"nama": "NOFRIANDI ROSA",
		"band": "III",
		"posisi": "SO REVASS & ANTI FRAUD MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.826666666666666,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0104364326375719,
			"nilai_komparatif_unit": 1.0157908733927778,
			"summary_team": 0.8352941176470587
		}]
	},
	"720583": {
		"nik": "720583",
		"nama": "EKO ADESUFWIANT",
		"band": "III",
		"posisi": "KABID MANAJEMEN SDM & LOGISTIK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8181818181818186,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0301587301587298,
			"nilai_komparatif_unit": 1.035617682063992,
			"summary_team": 0.842857142857143
		}]
	},
	"720594": {
		"nik": "720594",
		"nama": "ROSMIDA",
		"band": "III",
		"posisi": "TRIBE LEADER LEARNING DIGITIZATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.002046733754046,
			"nilai_komparatif_unit": 1.0073567163482287,
			"summary_team": 0.8545454545454546
		}]
	},
	"720598": {
		"nik": "720598",
		"nama": "TRIANSONI, MSC.",
		"band": "III",
		"posisi": "SENIOR EXPERT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9777354240665286,
			"nilai_komparatif_unit": 0.9829165777079948,
			"summary_team": 0.8142857142857142
		}]
	},
	"720601": {
		"nik": "720601",
		"nama": "MOHAMAD WAHYU SAPARI",
		"band": "III",
		"posisi": "MGR STRATEGIC PARTNERSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8422222222222211,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9079621294428072,
			"nilai_komparatif_unit": 0.9127735448599865,
			"summary_team": 0.764705882352941
		}]
	},
	"720605": {
		"nik": "720605",
		"nama": "WIDIA DIAN WULANDARI",
		"band": "III",
		"posisi": "SO LEADERSHIP & CAPABILITY SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8289855072463761,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9366515837104079,
			"nilai_komparatif_unit": 0.9416150284668046,
			"summary_team": 0.776470588235294
		}]
	},
	"730053": {
		"nik": "730053",
		"nama": "POEJI OETAMININGSIH GATOT",
		"band": "III",
		"posisi": "MANAGER BRAND & COMMUNICATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.829885057471264,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0908295669922736,
			"nilai_komparatif_unit": 1.0966100219539388,
			"summary_team": 0.905263157894737
		}]
	},
	"730059": {
		"nik": "730059",
		"nama": "TENNIE CHRISANTIANA,ST",
		"band": "III",
		"posisi": "SO BUSINESS DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8083333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9189985272459498,
			"nilai_komparatif_unit": 0.9238684260434576,
			"summary_team": 0.7428571428571427
		}]
	},
	"730085": {
		"nik": "730085",
		"nama": "IFFON LELIANA WINIARTI, ST.",
		"band": "III",
		"posisi": "MGR HC INTEGRATED SOLUTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.005,
			"nilai_komparatif_unit": 1.0103256323556495,
			"summary_team": 0.8933333333333334
		}]
	},
	"730093": {
		"nik": "730093",
		"nama": "DWI SUJARWO",
		"band": "III",
		"posisi": "MGR ENTERPRISE & BUSINESS BILLING OPR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.902777777777777,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0246153846153856,
			"nilai_komparatif_unit": 1.0300449615749918,
			"summary_team": 0.925
		}]
	},
	"730111": {
		"nik": "730111",
		"nama": "SULEKSONO",
		"band": "III",
		"posisi": "SO HC COMMUNICATION EVALUATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8374999999999996,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006396588486141,
			"nilai_komparatif_unit": 1.0117296215550537,
			"summary_team": 0.8428571428571426
		}]
	},
	"730129": {
		"nik": "730129",
		"nama": "MK. SUTRISNO",
		"band": "III",
		"posisi": "MANAGER DIG LEARNING DESIGN & EXPERIENCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8757575757575752,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9563148788927343,
			"nilai_komparatif_unit": 0.9613825221377297,
			"summary_team": 0.8375
		}]
	},
	"730131": {
		"nik": "730131",
		"nama": "MUHAMMAD ARNADI",
		"band": "III",
		"posisi": "MGR FINANCIAL APPLICATION SYSTEM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555548,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272736,
			"nilai_komparatif_unit": 1.028146844323932,
			"summary_team": 0.875
		}]
	},
	"730163": {
		"nik": "730163",
		"nama": "ANITA RAHAYU",
		"band": "III",
		"posisi": "SENIOR ADVISOR - III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8222222222222224,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.003378378378378,
			"nilai_komparatif_unit": 1.008695417539424,
			"summary_team": 0.825
		}]
	},
	"730190": {
		"nik": "730190",
		"nama": "TRIANA WIJAYANTI",
		"band": "III",
		"posisi": "SO BENEFIT & FACILITY SYSTEM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8238095238095232,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.91397483849031,
			"nilai_komparatif_unit": 0.9188181160745028,
			"summary_team": 0.7529411764705882
		}]
	},
	"730224": {
		"nik": "730224",
		"nama": "BUDIATI SUWARNO",
		"band": "III",
		"posisi": "MGR RA WHOLESALE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8722222222222213,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9306856500562017,
			"nilai_komparatif_unit": 0.935617480514787,
			"summary_team": 0.8117647058823529
		}]
	},
	"730237": {
		"nik": "730237",
		"nama": "AGNESIA CANDRA SULYANI",
		"band": "III",
		"posisi": "MANAGER SUPPORT RESEARCH & INNOVATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8766666666666664,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0353904650482602,
			"nilai_komparatif_unit": 1.040877140631735,
			"summary_team": 0.9076923076923077
		}]
	},
	"730240": {
		"nik": "730240",
		"nama": "SUSUN DWIANA PRIYATIN",
		"band": "III",
		"posisi": "MGR ASSESSMENT DELIVERY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.733333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0129870129870133,
			"nilai_komparatif_unit": 1.0183549696160845,
			"summary_team": 0.7428571428571427
		}]
	},
	"730244": {
		"nik": "730244",
		"nama": "BANU SAPTONO",
		"band": "III",
		"posisi": "SO SERVICE OPERATION & SUPPORT AUDIT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9594882729211085,
			"nilai_komparatif_unit": 0.9645727324147753,
			"summary_team": 0.857142857142857
		}]
	},
	"730248": {
		"nik": "730248",
		"nama": "GUSTI AYU ESCHARA KUSUMASTUTI",
		"band": "III",
		"posisi": "AUDITOR MUDA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.024249123879854,
			"nilai_komparatif_unit": 1.0296767599737648,
			"summary_team": 0.8941176470588235
		}]
	},
	"730251": {
		"nik": "730251",
		"nama": "MARTINI,ST",
		"band": "III",
		"posisi": "MANAGER PRODUCT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.829885057471264,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9639889196675906,
			"nilai_komparatif_unit": 0.9690972287034807,
			"summary_team": 0.8
		}]
	},
	"730269": {
		"nik": "730269",
		"nama": "MISNE ERAWATY",
		"band": "III",
		"posisi": "SO GCT STRATEGY PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8790123456790117,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9588683788121997,
			"nilai_komparatif_unit": 0.963949553402261,
			"summary_team": 0.8428571428571427
		}]
	},
	"730329": {
		"nik": "730329",
		"nama": "SRI WIBOWO HERLAMBANG",
		"band": "III",
		"posisi": "MGR RISK & QUALITY MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8509803921568628,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9400921658986174,
			"nilai_komparatif_unit": 0.9450738427702617,
			"summary_team": 0.8
		}]
	},
	"730359": {
		"nik": "730359",
		"nama": "ANNA INDRAWENI",
		"band": "III",
		"posisi": "SENIOR LEARNING ANALYST",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8060606060606065,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9924812030075182,
			"nilai_komparatif_unit": 0.9977404965469321,
			"summary_team": 0.7999999999999999
		}]
	},
	"730375": {
		"nik": "730375",
		"nama": "MIKHAIL MEOKO",
		"band": "III",
		"posisi": "SO WORKING ENVIRONMENT & CRISIS EVAL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8944444444444447,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9733284618195102,
			"nilai_komparatif_unit": 0.9784862623657199,
			"summary_team": 0.8705882352941177
		}]
	},
	"730389": {
		"nik": "730389",
		"nama": "TRI SUSANTO",
		"band": "III",
		"posisi": "SENIOR RESEARCHER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9323297542182379,
			"nilai_komparatif_unit": 0.9372702970094813,
			"summary_team": 0.7764705882352941
		}]
	},
	"730395": {
		"nik": "730395",
		"nama": "SUPRIYATI NUR ASIYAH",
		"band": "III",
		"posisi": "KABAG PERENCANAAN & PENGENDALIAN PROGRAM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8500000000000003,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9579831932773107,
			"nilai_komparatif_unit": 0.9630596771482424,
			"summary_team": 0.8142857142857144
		}]
	},
	"730407": {
		"nik": "730407",
		"nama": "BAMBANG GUNAWAN HADIYANTA",
		"band": "III",
		"posisi": "KABAG SISTEM INFORMASI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0384615384615385,
			"nilai_komparatif_unit": 1.043964488082761,
			"summary_team": 0.9000000000000001
		}]
	},
	"730431": {
		"nik": "730431",
		"nama": "LEONARD LOLO SUTARDODO PARAPAT, MS.",
		"band": "III",
		"posisi": "SO QUALITY & GOVERNANCE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.83030303030303,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0667361835245048,
			"nilai_komparatif_unit": 1.0723889643542763,
			"summary_team": 0.8857142857142855
		}]
	},
	"730442": {
		"nik": "730442",
		"nama": "BAYU BIROWO",
		"band": "III",
		"posisi": "MGR ENERGY MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8472222222222213,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.985920925747349,
			"nilai_komparatif_unit": 0.9911454554809532,
			"summary_team": 0.8352941176470587
		}]
	},
	"730449": {
		"nik": "730449",
		"nama": "NURWULANSARI",
		"band": "III",
		"posisi": "MANAGER WORKFORCE & OUTSOURCES MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8411764705882361,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.052947052947052,
			"nilai_komparatif_unit": 1.0585267632893014,
			"summary_team": 0.8857142857142857
		}]
	},
	"730455": {
		"nik": "730455",
		"nama": "SOFIAR YUSLIANSYAH",
		"band": "III",
		"posisi": "MGR HC INTEGRATED SOLUTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333337,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999997,
			"nilai_komparatif_unit": 0.9650871712053964,
			"summary_team": 0.8000000000000002
		}]
	},
	"730473": {
		"nik": "730473",
		"nama": "JEFFREY ANDREAS BUDISETIANTO",
		"band": "III",
		"posisi": "AUDITOR MUDA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0052991366722885,
			"summary_team": 0.8
		}]
	},
	"730492": {
		"nik": "730492",
		"nama": "EKA YULISRI",
		"band": "III",
		"posisi": "SO AUDIT RESOURCE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8509738961558183,
			"nilai_komparatif_unit": 0.8554833231360977,
			"summary_team": 0.7428571428571427
		}]
	},
	"730499": {
		"nik": "730499",
		"nama": "YUSUF HENDRIARTO",
		"band": "III",
		"posisi": "MANAGER DIG LEARNING DESIGN & EXP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.020000000000001,
			"nilai_komparatif_unit": 1.025405119405735,
			"summary_team": 0.85
		}]
	},
	"730511": {
		"nik": "730511",
		"nama": "BAYU DHIAN SASONGKO",
		"band": "III",
		"posisi": "MGR PROC PLANNING STRATEGIC CATEGORY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8499999999999998,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769234,
			"nilai_komparatif_unit": 0.9279684338513433,
			"summary_team": 0.7846153846153846
		}]
	},
	"730512": {
		"nik": "730512",
		"nama": "LINTONG PARLUHUTAN SIMAREMARE, ST",
		"band": "III",
		"posisi": "SO ENGAGEMENT & PARTNERSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8375,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9130816505706758,
			"nilai_komparatif_unit": 0.9179201950300083,
			"summary_team": 0.764705882352941
		}]
	},
	"730518": {
		"nik": "730518",
		"nama": "DRAJAD PUTRANDONO",
		"band": "III",
		"posisi": "MGR ANALISA, EVALUASI & ADMINISTRASI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8235294117647057,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9887755102040817,
			"nilai_komparatif_unit": 0.9940151667708647,
			"summary_team": 0.8142857142857142
		}]
	},
	"730533": {
		"nik": "730533",
		"nama": "ROLING MENAK ASMARA S.",
		"band": "III",
		"posisi": "MGR CONSUMER BILLING OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8825396825396817,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.027338129496404,
			"nilai_komparatif_unit": 1.0327821346532584,
			"summary_team": 0.9066666666666667
		}]
	},
	"730567": {
		"nik": "730567",
		"nama": "JOKO PURNOMO, M.M.",
		"band": "III",
		"posisi": "MGR HC DEVELOPMENT 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7888888888888886,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8957746478873244,
			"nilai_komparatif_unit": 0.9005214801740502,
			"summary_team": 0.7066666666666668
		}]
	},
	"730569": {
		"nik": "730569",
		"nama": "CARLI",
		"band": "III",
		"posisi": "MGR CME & PROPERTY CATEGORY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8450980392156857,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9883990719257546,
			"nilai_komparatif_unit": 0.993636733694652,
			"summary_team": 0.8352941176470587
		}]
	},
	"730588": {
		"nik": "730588",
		"nama": "AGUS RIYANTO, ST",
		"band": "III",
		"posisi": "MGR INFRASTRUCTURE CATEGORY ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8851851851851846,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.049963081466897,
			"nilai_komparatif_unit": 1.055526979336447,
			"summary_team": 0.9294117647058824
		}]
	},
	"730608": {
		"nik": "730608",
		"nama": "NURUL ISNAINI",
		"band": "III",
		"posisi": "MGR TALENT ACQUISITION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.846296296296295,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0240700218818395,
			"nilai_komparatif_unit": 1.0294967088897846,
			"summary_team": 0.8666666666666666
		}]
	},
	"740053": {
		"nik": "740053",
		"nama": "YULI SUSANTI",
		"band": "III",
		"posisi": "MGR BILLING & PAYMENT APPLICATION SYSTEM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555548,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9788961038961047,
			"nilai_komparatif_unit": 0.9840834081386206,
			"summary_team": 0.8375
		}]
	},
	"740056": {
		"nik": "740056",
		"nama": "DISA NURSANTI KARLINA",
		"band": "III",
		"posisi": "MGR TRANSPORT NETWORK CATEGORY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8361111111111104,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.04123509869064,
			"nilai_komparatif_unit": 1.0467527457865853,
			"summary_team": 0.8705882352941177
		}]
	},
	"740065": {
		"nik": "740065",
		"nama": "PRASASTY SOFIANTIANY",
		"band": "III",
		"posisi": "MANAGER LEARNING DEVT CORP BUSINESS&GTM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8703703703703702,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0340425531914896,
			"nilai_komparatif_unit": 1.0395220860058132,
			"summary_team": 0.9
		}]
	},
	"740080": {
		"nik": "740080",
		"nama": "RINI AMAN NASUTION, SE",
		"band": "III",
		"posisi": "SO CORPORATE OFFICE FACILITY SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8368421052631614,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9559748427672914,
			"nilai_komparatif_unit": 0.9610406841143846,
			"summary_team": 0.7999999999999998
		}]
	},
	"740093": {
		"nik": "740093",
		"nama": "BUDI LISTIANA",
		"band": "III",
		"posisi": "SO GCT COMMUNICATION STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7666666666666666,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0434782608695654,
			"nilai_komparatif_unit": 1.0490077947884748,
			"summary_team": 0.8
		}]
	},
	"740097": {
		"nik": "740097",
		"nama": "ALFA INDRIAWAN HARJANTO",
		"band": "III",
		"posisi": "MANAGER DELIVERY & FULFILLMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0358036272670368,
			"nilai_komparatif_unit": 1.0412924922535767,
			"summary_team": 0.8833333333333333
		}]
	},
	"740138": {
		"nik": "740138",
		"nama": "LUK LU UL ILMA",
		"band": "III",
		"posisi": "SENIOR RESEARCHER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9605821710127298,
			"nilai_komparatif_unit": 0.9656724272218896,
			"summary_team": 0.7999999999999998
		}]
	},
	"740147": {
		"nik": "740147",
		"nama": "YULI WIDYA HENNY",
		"band": "III",
		"posisi": "MGR PRO-HIRE SOURCING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.846296296296295,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9610503282275727,
			"nilai_komparatif_unit": 0.9661430652657982,
			"summary_team": 0.8133333333333335
		}]
	},
	"740159": {
		"nik": "740159",
		"nama": "FIKRI SYAHRIZA RIZANI",
		"band": "III",
		"posisi": "MGR GENERAL ADMINISTRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8756756756756745,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0613652868554844,
			"nilai_komparatif_unit": 1.066989606569754,
			"summary_team": 0.9294117647058824
		}]
	},
	"740170": {
		"nik": "740170",
		"nama": "RONI FIRDAUS",
		"band": "III",
		"posisi": "MGR SUPPLY INFORMATION SYSTEM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8499999999999998,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.073529411764706,
			"nilai_komparatif_unit": 1.0792181908393685,
			"summary_team": 0.9125
		}]
	},
	"740177": {
		"nik": "740177",
		"nama": "RADEN AHMAD SYABAR JAJULI",
		"band": "III",
		"posisi": "MGR OPERATIONAL CATEGORY ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8851851851851846,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.002615062761507,
			"nilai_komparatif_unit": 1.007928057008775,
			"summary_team": 0.8875
		}]
	},
	"740190": {
		"nik": "740190",
		"nama": "LILIK HARTADI",
		"band": "III",
		"posisi": "MGR EMPLOYEE DATA ADMNISTRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.846296296296295,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9452954048140059,
			"nilai_komparatif_unit": 0.9503046543598014,
			"summary_team": 0.8
		}]
	},
	"740193": {
		"nik": "740193",
		"nama": "LILIS SUSANTI",
		"band": "III",
		"posisi": "SO OFFICE ADMINISTRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8766666666666662,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9125475285171106,
			"nilai_komparatif_unit": 0.9173832425906817,
			"summary_team": 0.7999999999999998
		}]
	},
	"740211": {
		"nik": "740211",
		"nama": "HARIYANTO",
		"band": "III",
		"posisi": "MGR SPEND MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8318840579710138,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9051035048165614,
			"nilai_komparatif_unit": 0.9098997719911515,
			"summary_team": 0.7529411764705882
		}]
	},
	"740217": {
		"nik": "740217",
		"nama": "FIRMAN GUNAWAN",
		"band": "III",
		"posisi": "SENIOR EXPERT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8899511290264998,
			"nilai_komparatif_unit": 0.8946671016908684,
			"summary_team": 0.7411764705882352
		}]
	},
	"740231": {
		"nik": "740231",
		"nama": "SINTHA SETYO ASIH",
		"band": "III",
		"posisi": "SO GCT BUDGET & RESOURCING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.875362318840579,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0348656018698876,
			"nilai_komparatif_unit": 1.040349496131646,
			"summary_team": 0.9058823529411764
		}]
	},
	"740236": {
		"nik": "740236",
		"nama": "DIDIK DANARDI",
		"band": "III",
		"posisi": "MGR IT & DIGITAL CATEGORY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8361111111111104,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.04123509869064,
			"nilai_komparatif_unit": 1.0467527457865853,
			"summary_team": 0.8705882352941177
		}]
	},
	"740243": {
		"nik": "740243",
		"nama": "TRI PUJIANTO",
		"band": "III",
		"posisi": "MANAGER DIG LEARNING DESIGN & EXPERIENCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8733333333333334,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0217263652378155,
			"nilai_komparatif_unit": 1.027140632888891,
			"summary_team": 0.8923076923076922
		}]
	},
	"740252": {
		"nik": "740252",
		"nama": "DEWI ANGGRAENI",
		"band": "III",
		"posisi": "MGR TRANSPORT NTWRK CAT ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8851851851851846,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9037656903765694,
			"nilai_komparatif_unit": 0.9085548682895997,
			"summary_team": 0.7999999999999998
		}]
	},
	"740261": {
		"nik": "740261",
		"nama": "UWES QORNI",
		"band": "III",
		"posisi": "SM GENERAL SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8318840579710138,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0182414429186315,
			"nilai_komparatif_unit": 1.0236372434900454,
			"summary_team": 0.8470588235294116
		}]
	},
	"740287": {
		"nik": "740287",
		"nama": "RUDY HARINUGROHO",
		"band": "III",
		"posisi": "SENIOR STAFF III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8158730158730153,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8443579766536969,
			"nilai_komparatif_unit": 0.8488323449723216,
			"summary_team": 0.6888888888888888
		}]
	},
	"740293": {
		"nik": "740293",
		"nama": "TRI AYUNINGSIH",
		"band": "III",
		"posisi": "SENIOR RESEARCHER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0120419301741261,
			"nilai_komparatif_unit": 1.0174048786802052,
			"summary_team": 0.8428571428571427
		}]
	},
	"740299": {
		"nik": "740299",
		"nama": "WAHYU NOVIAN CONDRO MURWANTO",
		"band": "III",
		"posisi": "SM PROCUREMENT PLANNING & MONITORING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8361111111111104,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9568106312292368,
			"nilai_komparatif_unit": 0.9618809015336189,
			"summary_team": 0.8
		}]
	},
	"740313": {
		"nik": "740313",
		"nama": "ARI ANANTA",
		"band": "III",
		"posisi": "AUDITOR MUDA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Down Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.6873250699720072,
			"nilai_komparatif_unit": 0.6909672994560789,
			"summary_team": 0.5999999999999999
		}]
	},
	"750030": {
		"nik": "750030",
		"nama": "TUTUT VATY HUSNAWATI",
		"band": "III",
		"posisi": "SO GCT GOVERNANCE & DESIGN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8790123456790117,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9101123595505624,
			"nilai_komparatif_unit": 0.9149351693309595,
			"summary_team": 0.7999999999999998
		}]
	},
	"750036": {
		"nik": "750036",
		"nama": "IRA AGUSTIYAH",
		"band": "III",
		"posisi": "MGR PROC MONITOR&CONTROL STRTGC CAT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8450980392156857,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0570765661252908,
			"nilai_komparatif_unit": 1.0626781593222618,
			"summary_team": 0.8933333333333333
		}]
	},
	"750051": {
		"nik": "750051",
		"nama": "JAUHARUN NI'AM",
		"band": "III",
		"posisi": "SO SYSTEM DEVELOPMENT & REPORTING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8592592592592581,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9975369458128093,
			"nilai_komparatif_unit": 1.0028230304243284,
			"summary_team": 0.8571428571428571
		}]
	},
	"750057": {
		"nik": "750057",
		"nama": "SURASTYO SAMIAJI, S.E., M.M.",
		"band": "III",
		"posisi": "MGR ENTERPRS & BUSINESS BILL DATA MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.902777777777777,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0190769230769239,
			"nilai_komparatif_unit": 1.0244771509718835,
			"summary_team": 0.92
		}]
	},
	"750062": {
		"nik": "750062",
		"nama": "YUDI NANDAR",
		"band": "III",
		"posisi": "MGR PERFORMANSI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8294117647058813,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0070921985815615,
			"nilai_komparatif_unit": 1.0124289177834405,
			"summary_team": 0.8352941176470587
		}]
	},
	"750069": {
		"nik": "750069",
		"nama": "RETNO DWI ASTUTI, SH",
		"band": "III",
		"posisi": "MGR PROCUREMENT CONTRACT DRAFTING 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8969696969696972,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.926195426195426,
			"nilai_komparatif_unit": 0.9311034623440839,
			"summary_team": 0.8307692307692308
		}]
	},
	"750078": {
		"nik": "750078",
		"nama": "SONI GALIH RIADI",
		"band": "III",
		"posisi": "MGR PLANNING & EVALUATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481481,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9589519650655023,
			"nilai_komparatif_unit": 0.9640335825905437,
			"summary_team": 0.8133333333333334
		}]
	},
	"750083": {
		"nik": "750083",
		"nama": "MARISI P. PURBA",
		"band": "III",
		"posisi": "SO PROCESS MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8071428571428563,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9203539823008857,
			"nilai_komparatif_unit": 0.9252310638399829,
			"summary_team": 0.7428571428571427
		}]
	},
	"760041": {
		"nik": "760041",
		"nama": "DIAN SUSILOWATI",
		"band": "III",
		"posisi": "MGR FINANCE SERVICE (DBS)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8469696969696957,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.991771019677998,
			"nilai_komparatif_unit": 0.9970265498588864,
			"summary_team": 0.8400000000000001
		}]
	},
	"770029": {
		"nik": "770029",
		"nama": "WELLY ARDHANA",
		"band": "III",
		"posisi": "MGR DOMESTIC INTERCONNECTION BILLING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8804597701149418,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9843342036553533,
			"nilai_komparatif_unit": 0.989550325131731,
			"summary_team": 0.8666666666666667
		}]
	},
	"770037": {
		"nik": "770037",
		"nama": "TINTON TADARUS SUWARJANA",
		"band": "III",
		"posisi": "CAPITAL/FINANCE RESTRUCTURING COORD",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8352941176470572,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000018,
			"nilai_komparatif_unit": 1.00529913667229,
			"summary_team": 0.8352941176470587
		}]
	},
	"770039": {
		"nik": "770039",
		"nama": "DYDY SUKANDAR",
		"band": "III",
		"posisi": "MGR MANAGED & NETWORK SERVICE BILLING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8804597701149418,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9086161879895569,
			"nilai_komparatif_unit": 0.9134310693523671,
			"summary_team": 0.8
		}]
	},
	"770041": {
		"nik": "770041",
		"nama": "BAYU SASONGKO PUTRO",
		"band": "III",
		"posisi": "SO COLLABORATION & ALIGNMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8083333333333332,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9720176730486009,
			"nilai_komparatif_unit": 0.9771685275459651,
			"summary_team": 0.7857142857142856
		}]
	},
	"770048": {
		"nik": "770048",
		"nama": "LENI ISKANDAR",
		"band": "III",
		"posisi": "AUDITOR MUDA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9164334266293429,
			"nilai_komparatif_unit": 0.9212897326081052,
			"summary_team": 0.7999999999999998
		}]
	},
	"770078": {
		"nik": "770078",
		"nama": "MUHAMAD PATRIA NAROTAMA WIDJAJA",
		"band": "III",
		"posisi": "SO CSR & PROGRAM REVIEW",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8799999999999992,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9491978609625676,
			"nilai_komparatif_unit": 0.954227790156852,
			"summary_team": 0.8352941176470587
		}]
	},
	"780004": {
		"nik": "780004",
		"nama": "KADARWATI",
		"band": "III",
		"posisi": "MGR DUNNING MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8765432098765427,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0207561156412164,
			"nilai_komparatif_unit": 1.0261652418070732,
			"summary_team": 0.8947368421052633
		}]
	},
	"780010": {
		"nik": "780010",
		"nama": "HERDIAN ARI AVIANSYAH",
		"band": "III",
		"posisi": "SO BUSINESS & PRODUCT LINE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555552,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1138273491214672,
			"nilai_komparatif_unit": 1.1197296724737944,
			"summary_team": 0.9529411764705882
		}]
	},
	"780015": {
		"nik": "780015",
		"nama": "SAPRIN",
		"band": "III",
		"posisi": "SO BUSINESS STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8375,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0575692963752665,
			"nilai_komparatif_unit": 1.0631735006171747,
			"summary_team": 0.8857142857142857
		}]
	},
	"780046": {
		"nik": "780046",
		"nama": "LIDWIN SAPUTRA",
		"band": "III",
		"posisi": "MANAGER LEARNING DEVT DIG SERVICES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8757575757575752,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9420415224913502,
			"nilai_komparatif_unit": 0.9470335292700024,
			"summary_team": 0.8250000000000001
		}]
	},
	"780056": {
		"nik": "780056",
		"nama": "PUJI LESTARI",
		"band": "III",
		"posisi": "MGR ACCOUNTING OPER & REPORTING TLKM REG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8761904761904756,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0652173913043483,
			"nilai_komparatif_unit": 1.0708621238465685,
			"summary_team": 0.9333333333333332
		}]
	},
	"780058": {
		"nik": "780058",
		"nama": "DESY RAHMAWATI",
		"band": "III",
		"posisi": "SO COMPLIANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8694444444444439,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9742529599699312,
			"nilai_komparatif_unit": 0.9794156595581932,
			"summary_team": 0.8470588235294119
		}]
	},
	"790006": {
		"nik": "790006",
		"nama": "WIDA SUHARTINI",
		"band": "III",
		"posisi": "MGR HC DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8708333333333331,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.041148325358852,
			"nilai_komparatif_unit": 1.0466655126310527,
			"summary_team": 0.9066666666666667
		}]
	},
	"790031": {
		"nik": "790031",
		"nama": "IMAM SAFII",
		"band": "III",
		"posisi": "MGR HC DATA CONSOLIDATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8848484848484851,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9643835616438354,
			"nilai_komparatif_unit": 0.9694939619414943,
			"summary_team": 0.8533333333333334
		}]
	},
	"790034": {
		"nik": "790034",
		"nama": "ANDI SETIAWAN",
		"band": "III",
		"posisi": "MGR CONSUMER BILLING SUPPORT & SOLUTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8825396825396817,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9239070282235758,
			"nilai_komparatif_unit": 0.9288029378386202,
			"summary_team": 0.8153846153846154
		}]
	},
	"790050": {
		"nik": "790050",
		"nama": "DELYA BUDHI MAYASARI",
		"band": "III",
		"posisi": "MGR BUSINESS PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8158730158730153,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.120622568093386,
			"nilai_komparatif_unit": 1.1265609002397634,
			"summary_team": 0.9142857142857141
		}]
	},
	"790076": {
		"nik": "790076",
		"nama": "ANDRIAN",
		"band": "III",
		"posisi": "AUDITOR MUDA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.030987604958011,
			"nilai_komparatif_unit": 1.0364509491841185,
			"summary_team": 0.9
		}]
	},
	"790092": {
		"nik": "790092",
		"nama": "HENDRA FERNANDO HUTAPEA",
		"band": "III",
		"posisi": "MGR ACCOUNTING CONTROL SUBSIDIARY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8925925925925922,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9709543568464736,
			"nilai_komparatif_unit": 0.9760995766859568,
			"summary_team": 0.8666666666666667
		}]
	},
	"790104": {
		"nik": "790104",
		"nama": "NINING SURYANTIKA",
		"band": "III",
		"posisi": "MANAGER LIR SUPPORT READINESS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9849906191369558,
			"nilai_komparatif_unit": 0.9902102190486844,
			"summary_team": 0.8400000000000001
		}]
	},
	"790111": {
		"nik": "790111",
		"nama": "ILYA NOVA",
		"band": "III",
		"posisi": "MGR CASH OPERATION (WINS, ISP, & DWS)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8646153846153859,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0320284697508881,
			"nilai_komparatif_unit": 1.0374973296617906,
			"summary_team": 0.8923076923076922
		}]
	},
	"790122": {
		"nik": "790122",
		"nama": "EKARISTI NUSA UTAMA SARAGIH",
		"band": "III",
		"posisi": "MGR FINANCE SERVICE (TCUC & SSO PSC)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.862015503875968,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9435251798561162,
			"nilai_komparatif_unit": 0.9485250487379191,
			"summary_team": 0.8133333333333334
		}]
	},
	"790125": {
		"nik": "790125",
		"nama": "ACHYAR BASYARI",
		"band": "III",
		"posisi": "MGR POST ASSESSMENT DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8231884057971014,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9718309859154929,
			"nilai_komparatif_unit": 0.9769808511322238,
			"summary_team": 0.7999999999999999
		}]
	},
	"800036": {
		"nik": "800036",
		"nama": "AKAS TRIONO HADI",
		"band": "III",
		"posisi": "SENIOR INNOVATOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8471014492753652,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9443969204448215,
			"nilai_komparatif_unit": 0.9494014087991467,
			"summary_team": 0.8
		}]
	},
	"800046": {
		"nik": "800046",
		"nama": "FERNANDEZ REDWINLY SIAHAAN",
		"band": "III",
		"posisi": "SM BUDGET OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8646153846153859,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8540925266903903,
			"nilai_komparatif_unit": 0.8586184797201026,
			"summary_team": 0.7384615384615385
		}]
	},
	"800059": {
		"nik": "800059",
		"nama": "RATRI NATARINI",
		"band": "III",
		"posisi": "MGR GROUP PROJECT PLANNING & ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8583333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.986864648772131,
			"nilai_komparatif_unit": 0.9920941794230242,
			"summary_team": 0.8470588235294116
		}]
	},
	"800077": {
		"nik": "800077",
		"nama": "NOVA YUANITA",
		"band": "III",
		"posisi": "SO ACTIVATION & BRANDING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8977777777777771,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0221316249271992,
			"nilai_komparatif_unit": 1.0275480401047565,
			"summary_team": 0.9176470588235293
		}]
	},
	"800079": {
		"nik": "800079",
		"nama": "FIKRI AKHMADI",
		"band": "III",
		"posisi": "MGR PROJECT FINANCING & PERFORMANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8422222222222211,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9498680738786291,
			"nilai_komparatif_unit": 0.9549015546227552,
			"summary_team": 0.7999999999999999
		}]
	},
	"800094": {
		"nik": "800094",
		"nama": "NOVY KARTIKAYANTI",
		"band": "III",
		"posisi": "SO SHAREHOLDER RELATIONS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9166666666666661,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9754010695187171,
			"nilai_komparatif_unit": 0.9805698530963929,
			"summary_team": 0.8941176470588235
		}]
	},
	"800110": {
		"nik": "800110",
		"nama": "SRI KUNTADI",
		"band": "III",
		"posisi": "MANAGER INNOVATION OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8766666666666664,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.075502444323737,
			"nilai_komparatif_unit": 1.0812016787675887,
			"summary_team": 0.9428571428571426
		}]
	},
	"810008": {
		"nik": "810008",
		"nama": "JUNAIDY",
		"band": "III",
		"posisi": "SO INVESMENT CONTROL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8777777777777772,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9516008935219663,
			"nilai_komparatif_unit": 0.9566435567142109,
			"summary_team": 0.8352941176470587
		}]
	},
	"810013": {
		"nik": "810013",
		"nama": "EYEN EKAYATRI",
		"band": "III",
		"posisi": "MANAGER TCU AREA-7",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0208586248758365,
			"nilai_komparatif_unit": 1.0262682942521377,
			"summary_team": 0.8705882352941177
		}]
	},
	"810031": {
		"nik": "810031",
		"nama": "HETI TRIASWATI",
		"band": "III",
		"posisi": "MGR FINANCE SERVICE JKT (HCM,KUG,CEO OFF",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.862015503875968,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.015062949640289,
			"nilai_komparatif_unit": 1.020441906941409,
			"summary_team": 0.875
		}]
	},
	"810037": {
		"nik": "810037",
		"nama": "MAULINA VIDYANTI",
		"band": "III",
		"posisi": "MANAGER DIGITAL LEARNING OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9594064472113204,
			"nilai_komparatif_unit": 0.9644904730993676,
			"summary_team": 0.8181818181818181
		}]
	},
	"810078": {
		"nik": "810078",
		"nama": "DITA KUSUMARINI",
		"band": "III",
		"posisi": "MGR ACCOUNTING OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8469696969696957,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0547406082289819,
			"nilai_komparatif_unit": 1.0603298228657996,
			"summary_team": 0.8933333333333333
		}]
	},
	"810080": {
		"nik": "810080",
		"nama": "DONDA IMAN PRABOWO",
		"band": "III",
		"posisi": "MGR PROCUREMENT CONTRACT DRAFTING 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8969696969696972,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0451858108108105,
			"nilai_komparatif_unit": 1.0507243932702335,
			"summary_team": 0.9375
		}]
	},
	"810081": {
		"nik": "810081",
		"nama": "ANDRI MR, M.S.M.",
		"band": "III",
		"posisi": "SO PEOPLE ANALYTICS PROJECT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8777777777777769,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0052122114668662,
			"nilai_komparatif_unit": 1.0105389683600823,
			"summary_team": 0.8823529411764706
		}]
	},
	"810088": {
		"nik": "810088",
		"nama": "TUNAS RESTIANI",
		"band": "III",
		"posisi": "MGR EXTERNAL ASSESSMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.842857142857143,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.949152542372881,
			"nilai_komparatif_unit": 0.9541822314177648,
			"summary_team": 0.7999999999999998
		}]
	},
	"810097": {
		"nik": "810097",
		"nama": "RETNO WULANSARI",
		"band": "III",
		"posisi": "SENIOR RESEARCHER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.029195183227925,
			"nilai_komparatif_unit": 1.0346490291663104,
			"summary_team": 0.8571428571428571
		}]
	},
	"810109": {
		"nik": "810109",
		"nama": "ARIE RACHMAWATI",
		"band": "III",
		"posisi": "SO OPERATIONAL ACCOUNTING POLICY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8904761904761893,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9380308273041849,
			"nilai_komparatif_unit": 0.9430015808608894,
			"summary_team": 0.8352941176470587
		}]
	},
	"810110": {
		"nik": "810110",
		"nama": "PUTU MIARTI",
		"band": "III",
		"posisi": "MGR ACCOUNTING ASESSMENT & BUDGET OPR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.862015503875968,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.017293857221916,
			"nilai_komparatif_unit": 1.0226846364072142,
			"summary_team": 0.8769230769230769
		}]
	},
	"810115": {
		"nik": "810115",
		"nama": "LILIES NURAINI",
		"band": "III",
		"posisi": "SO GROUP ORGANIZATION DESIGN & EVAL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8377777777777772,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.912778904665315,
			"nilai_komparatif_unit": 0.9176158448327181,
			"summary_team": 0.7647058823529411
		}]
	},
	"810116": {
		"nik": "810116",
		"nama": "DITALIA ADISTI",
		"band": "III",
		"posisi": "MGR TELKOM SELECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.842857142857143,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.966101694915254,
			"nilai_komparatif_unit": 0.9712211998359392,
			"summary_team": 0.8142857142857142
		}]
	},
	"820014": {
		"nik": "820014",
		"nama": "DEWI JUWITA",
		"band": "III",
		"posisi": "MGR OUTSOURCING EVALUATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.080000000000001,
			"nilai_komparatif_unit": 1.0857230676060723,
			"summary_team": 0.8999999999999999
		}]
	},
	"820023": {
		"nik": "820023",
		"nama": "YANDHY YULIANSYAH",
		"band": "III",
		"posisi": "SO AUDIT QUALITY ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8592592592592581,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0131845841785003,
			"nilai_komparatif_unit": 1.0185535877643177,
			"summary_team": 0.8705882352941177
		}]
	},
	"820040": {
		"nik": "820040",
		"nama": "MUTHMAINNAH",
		"band": "III",
		"posisi": "SO BUDGET ALIGNMENT & SOLUTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8235294117647056,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0714285714285718,
			"nilai_komparatif_unit": 1.0771062178631663,
			"summary_team": 0.8823529411764706
		}]
	},
	"820053": {
		"nik": "820053",
		"nama": "NADIA EKA HERDIANA",
		"band": "III",
		"posisi": "MGR DISCLOSURE REPORTING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9022222222222221,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0572186434255402,
			"nilai_komparatif_unit": 1.0628209895095433,
			"summary_team": 0.9538461538461539
		}]
	},
	"830027": {
		"nik": "830027",
		"nama": "FEBRINA INDIASTUTI",
		"band": "III",
		"posisi": "MGR PARTNERSHIP ACCOUNT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8811594202898547,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.978824013157895,
			"nilai_komparatif_unit": 0.9840109353817363,
			"summary_team": 0.8624999999999999
		}]
	},
	"830034": {
		"nik": "830034",
		"nama": "DARINA  FITRIANI",
		"band": "III",
		"posisi": "MGR HC DEVELOPMENT 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333337,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999996,
			"nilai_komparatif_unit": 0.9650871712053963,
			"summary_team": 0.8
		}]
	},
	"830046": {
		"nik": "830046",
		"nama": "MOKHAMAD KHAMIM",
		"band": "III",
		"posisi": "MGR ACCOUNTING OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8756756756756745,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.035390946502059,
			"nilai_komparatif_unit": 1.0408776246368232,
			"summary_team": 0.9066666666666666
		}]
	},
	"830049": {
		"nik": "830049",
		"nama": "KURNIA RIMADANI",
		"band": "III",
		"posisi": "SO BUSINESS ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9166666666666661,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0139037433155087,
			"nilai_komparatif_unit": 1.0192765578238823,
			"summary_team": 0.9294117647058824
		}]
	},
	"830067": {
		"nik": "830067",
		"nama": "TRI ARIYANTI",
		"band": "III",
		"posisi": "MGR PPH OPERATION CONTROL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.933333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9560439560439564,
			"nilai_komparatif_unit": 0.9611101636317485,
			"summary_team": 0.8923076923076924
		}]
	},
	"830075": {
		"nik": "830075",
		"nama": "ACHMAD NURJAYA, M.M.",
		"band": "III",
		"posisi": "MGR ENTERPRISE&BUSINESS BILLING SOLUTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.902777777777777,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.94523076923077,
			"nilai_komparatif_unit": 0.950239676263776,
			"summary_team": 0.8533333333333333
		}]
	},
	"830093": {
		"nik": "830093",
		"nama": "JUNIKE LAURA MERRYANNA",
		"band": "III",
		"posisi": "SO BROADBAND REGULATION STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9688581314878886,
			"nilai_komparatif_unit": 0.9739922431427007,
			"summary_team": 0.8235294117647057
		}]
	},
	"830108": {
		"nik": "830108",
		"nama": "INDAH WAHYUNI",
		"band": "III",
		"posisi": "SO PROCUREMENT POLICY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8807017543859643,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9909453096704101,
			"nilai_komparatif_unit": 0.9961964643011165,
			"summary_team": 0.8727272727272728
		}]
	},
	"830113": {
		"nik": "830113",
		"nama": "HERAWAN PRASETYO",
		"band": "III",
		"posisi": "MGR PENGELOLAAN SISTEM INFORMASI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8294117647058813,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0070921985815615,
			"nilai_komparatif_unit": 1.0124289177834405,
			"summary_team": 0.8352941176470587
		}]
	},
	"830114": {
		"nik": "830114",
		"nama": "DIAN HENDRAYANA",
		"band": "III",
		"posisi": "SENIOR RESEARCHER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8328282828282871,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0120419301741261,
			"nilai_komparatif_unit": 1.0174048786802052,
			"summary_team": 0.8428571428571427
		}]
	},
	"830115": {
		"nik": "830115",
		"nama": "YOHAN OKTAVIANUS NAPOH",
		"band": "III",
		"posisi": "MGR PPN OPERATION CONTROL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.933333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0714285714285718,
			"nilai_komparatif_unit": 1.0771062178631663,
			"summary_team": 1
		}]
	},
	"830126": {
		"nik": "830126",
		"nama": "DEWI NATALIA SAGALA",
		"band": "III",
		"posisi": "MGR FINANCE SERVICE BUSINESS 02",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8469696969696957,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9888193202146706,
			"nilai_komparatif_unit": 0.9940592089366873,
			"summary_team": 0.8375
		}]
	},
	"830135": {
		"nik": "830135",
		"nama": "ENDAH SILVIANINGRUM",
		"band": "III",
		"posisi": "MGR ACCOUNTING ASESSMENT & BUDGET OPR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8761904761904756,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9558423913043484,
			"nilai_komparatif_unit": 0.960907530773037,
			"summary_team": 0.8375
		}]
	},
	"830138": {
		"nik": "830138",
		"nama": "QORIATUL HUSNA",
		"band": "III",
		"posisi": "SO STRATEGIC ACCOUNTING ADVISORY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8904761904761893,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.977665932683235,
			"nilai_komparatif_unit": 0.9828467180803636,
			"summary_team": 0.8705882352941177
		}]
	},
	"830140": {
		"nik": "830140",
		"nama": "FRIANDY PARULIAN",
		"band": "III",
		"posisi": "MGR CASH EVALUATION & MONITORING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9041666666666665,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0549450549450552,
			"nilai_komparatif_unit": 1.0605353529729638,
			"summary_team": 0.9538461538461539
		}]
	},
	"840029": {
		"nik": "840029",
		"nama": "TRI DEWI LISTIYANINGTIYAS",
		"band": "III",
		"posisi": "MGR DEBT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8765432098765427,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9847294292068205,
			"nilai_komparatif_unit": 0.9899476450374118,
			"summary_team": 0.8631578947368421
		}]
	},
	"840062": {
		"nik": "840062",
		"nama": "YENTY NOOR RASPRY",
		"band": "III",
		"posisi": "SO TREASURY POLICY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9151515151515154,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9898714452668483,
			"nilai_komparatif_unit": 0.9951169093433129,
			"summary_team": 0.9058823529411766
		}]
	},
	"840064": {
		"nik": "840064",
		"nama": "TIA SRI UTARI",
		"band": "III",
		"posisi": "MGR FINANCIAL STATEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9022222222222221,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9890109890109892,
			"nilai_komparatif_unit": 0.9942518934121534,
			"summary_team": 0.8923076923076924
		}]
	},
	"840067": {
		"nik": "840067",
		"nama": "RIRI FEBRIA RAHMI",
		"band": "III",
		"posisi": "MGR AKUNTANSI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8294117647058813,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9128672745694036,
			"nilai_komparatif_unit": 0.9177046830210062,
			"summary_team": 0.7571428571428573
		}]
	},
	"840094": {
		"nik": "840094",
		"nama": "ROGER PARDEDE",
		"band": "III",
		"posisi": "SO DIGITAL REGULATION STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8622222222222212,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9824135839902981,
			"nilai_komparatif_unit": 0.9876195278405753,
			"summary_team": 0.8470588235294116
		}]
	},
	"840097": {
		"nik": "840097",
		"nama": "DEDI JUNIARDI DAMSIK",
		"band": "III",
		"posisi": "AUDITOR MUDA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9164334266293431,
			"nilai_komparatif_unit": 0.9212897326081054,
			"summary_team": 0.8
		}]
	},
	"840102": {
		"nik": "840102",
		"nama": "YUNITA SETYANINGSIH",
		"band": "III",
		"posisi": "MGR ACCOUNTING ASESSMENT & BUDGET OPR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8469696969696957,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9445438282647599,
			"nilai_komparatif_unit": 0.9495490951037012,
			"summary_team": 0.8
		}]
	},
	"840108": {
		"nik": "840108",
		"nama": "LEUSRINA ARDINI TRISNANINGRUM",
		"band": "III",
		"posisi": "MGR PERSONNEL COST PAYMENT & REPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9882352941176479,
			"nilai_komparatif_unit": 0.9934720880055563,
			"summary_team": 0.8235294117647058
		}]
	},
	"840115": {
		"nik": "840115",
		"nama": "ANDINA AGITA ANGGRAINI",
		"band": "III",
		"posisi": "MGR ACCOUNTING CONTROL PARENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8925925925925922,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9489870637051504,
			"nilai_komparatif_unit": 0.9540158758559575,
			"summary_team": 0.8470588235294116
		}]
	},
	"840120": {
		"nik": "840120",
		"nama": "MAS MOCHAMAD GUS NURULLAH",
		"band": "III",
		"posisi": "AUDITOR MUDA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9703412752545985,
			"nilai_komparatif_unit": 0.9754832462909351,
			"summary_team": 0.8470588235294116
		}]
	},
	"840137": {
		"nik": "840137",
		"nama": "SAHRU MASKUR",
		"band": "III",
		"posisi": "MGR RESEARCH & IT SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8158730158730153,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0028298549699335,
			"nilai_komparatif_unit": 1.00814398743047,
			"summary_team": 0.8181818181818181
		}]
	},
	"840149": {
		"nik": "840149",
		"nama": "DIAH SETYOWATI AYUNINGTYAS",
		"band": "III",
		"posisi": "MGR CRM & MARKETING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8158730158730153,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0670634012359812,
			"nilai_komparatif_unit": 1.0727179160371274,
			"summary_team": 0.8705882352941174
		}]
	},
	"840150": {
		"nik": "840150",
		"nama": "WIEKE WIDOWATI",
		"band": "III",
		"posisi": "MGR ASSET INSURANCE & PROTECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8472222222222213,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.999807135969143,
			"nilai_komparatif_unit": 1.0051052506285725,
			"summary_team": 0.8470588235294119
		}]
	},
	"840153": {
		"nik": "840153",
		"nama": "DZURROTUN NAFI'AH",
		"band": "III",
		"posisi": "MGR REVENUE ACCOUNT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8765432098765427,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0066280033140023,
			"nilai_komparatif_unit": 1.0119622626817157,
			"summary_team": 0.8823529411764706
		}]
	},
	"840159": {
		"nik": "840159",
		"nama": "HEDI KRISHNA",
		"band": "III",
		"posisi": "MANAGER RESEARCH OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8766666666666664,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0646387832699624,
			"nilai_komparatif_unit": 1.0702804496891287,
			"summary_team": 0.9333333333333333
		}]
	},
	"840167": {
		"nik": "840167",
		"nama": "HENDRA PRIATNA",
		"band": "III",
		"posisi": "SO DATABASE & REPORTING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8499999999999998,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0380622837370246,
			"nilai_komparatif_unit": 1.0435631176528948,
			"summary_team": 0.8823529411764706
		}]
	},
	"840171": {
		"nik": "840171",
		"nama": "TATI NOVIANTI",
		"band": "III",
		"posisi": "MGR ACCOUNTING ASESSMENT & BUDGET OPR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8646153846153859,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0698398576512438,
			"nilai_komparatif_unit": 1.0755090852743991,
			"summary_team": 0.9249999999999999
		}]
	},
	"840175": {
		"nik": "840175",
		"nama": "SHINTA ALIFIANTY",
		"band": "III",
		"posisi": "MGR ACCOUNTING ASESSMENT & BUDGET OPR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8646153846153859,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9975533807829168,
			"nilai_komparatif_unit": 1.0028395524855886,
			"summary_team": 0.8625
		}]
	},
	"840181": {
		"nik": "840181",
		"nama": "JOEHERDHI MUHAMMAD YUSUF",
		"band": "III",
		"posisi": "MGR SECRETARIATE & ADMINISTRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8411764705882361,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9668997668997662,
			"nilai_komparatif_unit": 0.9720235009129717,
			"summary_team": 0.8133333333333335
		}]
	},
	"840183": {
		"nik": "840183",
		"nama": "AAN YULIA LUFTI",
		"band": "III",
		"posisi": "SO APPLICATION TRANSACTION AUDIT REPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969696,
			"nilai_komparatif_unit": 0.9748355264700976,
			"summary_team": 0.8727272727272727
		}]
	},
	"840185": {
		"nik": "840185",
		"nama": "GANDUNG PRATIDHINA",
		"band": "III",
		"posisi": "MGR PERENCANAAN & PENGEMBANGAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8294117647058813,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0070921985815615,
			"nilai_komparatif_unit": 1.0124289177834405,
			"summary_team": 0.8352941176470587
		}]
	},
	"840191": {
		"nik": "840191",
		"nama": "DIDIT DWIANTORO",
		"band": "III",
		"posisi": "SO REPORTING MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8368421052631614,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9981502034776132,
			"nilai_komparatif_unit": 1.0034395378253134,
			"summary_team": 0.8352941176470587
		}]
	},
	"850008": {
		"nik": "850008",
		"nama": "SYAFRIZA BRANDO GINTING",
		"band": "III",
		"posisi": "MGR INTL INTERCONNECTION BILLING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8804597701149418,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9435629644506938,
			"nilai_komparatif_unit": 0.9485630335582275,
			"summary_team": 0.8307692307692308
		}]
	},
	"850084": {
		"nik": "850084",
		"nama": "YOGA ADITAMA",
		"band": "III",
		"posisi": "MGR INTERCOMPANY SETTLEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8811594202898547,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0538063909774438,
			"nilai_komparatif_unit": 1.0593906550693641,
			"summary_team": 0.9285714285714284
		}]
	},
	"850086": {
		"nik": "850086",
		"nama": "HANNA MUDITA",
		"band": "III",
		"posisi": "MGR QUALITY & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481481,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0532751091703056,
			"nilai_komparatif_unit": 1.0588565579273184,
			"summary_team": 0.8933333333333333
		}]
	},
	"850103": {
		"nik": "850103",
		"nama": "MELINDHA PUTRI KURNIAHADI",
		"band": "III",
		"posisi": "MGR RETAIL PARTNERSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8583333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.041690462592805,
			"nilai_komparatif_unit": 1.0472105227243034,
			"summary_team": 0.8941176470588235
		}]
	},
	"850105": {
		"nik": "850105",
		"nama": "LUTHFIA RAHMANI",
		"band": "III",
		"posisi": "MGR CONSOLIDATION CONTROL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8925925925925922,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0223029045643157,
			"nilai_komparatif_unit": 1.0277202273760793,
			"summary_team": 0.9125
		}]
	},
	"850108": {
		"nik": "850108",
		"nama": "KIRANA RISKIANTA ADIWIRIA",
		"band": "III",
		"posisi": "MGR LEVERAGING SYNERGY GROUP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8583333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0279840091376367,
			"nilai_komparatif_unit": 1.0334314368989839,
			"summary_team": 0.8823529411764706
		}]
	},
	"850109": {
		"nik": "850109",
		"nama": "ERNA DWIYANTI, M.T",
		"band": "III",
		"posisi": "MGR BACKBONE NODES CATEGORY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8361111111111104,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9867109634551504,
			"nilai_komparatif_unit": 0.9919396797065445,
			"summary_team": 0.8250000000000001
		}]
	},
	"850111": {
		"nik": "850111",
		"nama": "LINDA HIMAWATI",
		"band": "III",
		"posisi": "SO CAREER MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8222222222222221,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.001589825119237,
			"nilai_komparatif_unit": 1.0068973864921171,
			"summary_team": 0.8235294117647058
		}]
	},
	"850117": {
		"nik": "850117",
		"nama": "RENI RIANTI",
		"band": "III",
		"posisi": "MGR SUPPORTING AUDIT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8499999999999998,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9915966386554622,
			"nilai_komparatif_unit": 0.9968512447674791,
			"summary_team": 0.8428571428571426
		}]
	},
	"850118": {
		"nik": "850118",
		"nama": "HANNA LAILA YUSTITIA DEWI",
		"band": "III",
		"posisi": "MGR EMPLOYEE EXPERIENCE & MTM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8388888888888891,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9834437086092713,
			"nilai_komparatif_unit": 0.9886551112306938,
			"summary_team": 0.8250000000000001
		}]
	},
	"850119": {
		"nik": "850119",
		"nama": "ERVIANA",
		"band": "III",
		"posisi": "MGR RA CONSUMER & ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8722222222222213,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9306856500562017,
			"nilai_komparatif_unit": 0.935617480514787,
			"summary_team": 0.8117647058823529
		}]
	},
	"850126": {
		"nik": "850126",
		"nama": "RYAN VAMONDO",
		"band": "III",
		"posisi": "SO TELKOM ORGANIZATION DESIGN & EVAL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8377777777777772,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.940864409424248,
			"nilai_komparatif_unit": 0.9458501785198788,
			"summary_team": 0.7882352941176471
		}]
	},
	"850127": {
		"nik": "850127",
		"nama": "FEBRI HARTINI RAHMAWATI",
		"band": "III",
		"posisi": "SO STATUTORY ACCOUNTING POLICY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8904761904761893,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0833595470273687,
			"nilai_komparatif_unit": 1.0891004173322951,
			"summary_team": 0.9647058823529413
		}]
	},
	"850145": {
		"nik": "850145",
		"nama": "FENNY LINTING",
		"band": "III",
		"posisi": "SO LEGAL ADVISOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8743589743589737,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9803100125680778,
			"nilai_komparatif_unit": 0.9855048093058887,
			"summary_team": 0.8571428571428571
		}]
	},
	"850146": {
		"nik": "850146",
		"nama": "MOCHAMAD FADILLAH RIZKY",
		"band": "III",
		"posisi": "MANAGER DIG LEARNING DESIGN & EXP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8749999999999997,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9285714285714289,
			"nilai_komparatif_unit": 0.9334920554814109,
			"summary_team": 0.8125
		}]
	},
	"850148": {
		"nik": "850148",
		"nama": "ALFIANITA DEWI SULISTYANINGRUM",
		"band": "III",
		"posisi": "MGR ACCOUNTING OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8646153846153859,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9430604982206392,
			"nilai_komparatif_unit": 0.9480579046909466,
			"summary_team": 0.8153846153846154
		}]
	},
	"850149": {
		"nik": "850149",
		"nama": "ANGGA PUTSAL INDHARTA",
		"band": "III",
		"posisi": "MGR ACCOUNTING INTERCOMPANY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8756756756756745,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9992283950617298,
			"nilai_komparatif_unit": 1.004523442893993,
			"summary_team": 0.875
		}]
	},
	"850160": {
		"nik": "850160",
		"nama": "MEGA TRIWIBOWO",
		"band": "III",
		"posisi": "SO PMS & KPI SETTING & EVALUATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8694444444444437,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.041909415523399,
			"nilai_komparatif_unit": 1.0474306359164014,
			"summary_team": 0.9058823529411766
		}]
	},
	"850167": {
		"nik": "850167",
		"nama": "WIDYA HAPSARI CHAERANI",
		"band": "III",
		"posisi": "MANAGER LEGAL & PARTNERSHIP ADM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.829885057471264,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9214599967410791,
			"nilai_komparatif_unit": 0.9263429392018564,
			"summary_team": 0.7647058823529411
		}]
	},
	"850176": {
		"nik": "850176",
		"nama": "FIQI DIPOWICAKSONO",
		"band": "III",
		"posisi": "SO SEKRETARIAT BOC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8368421052631614,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9137994820569698,
			"nilai_komparatif_unit": 0.9186418304034559,
			"summary_team": 0.7647058823529411
		}]
	},
	"850177": {
		"nik": "850177",
		"nama": "ALY TAUFAN",
		"band": "III",
		"posisi": "MGR CASH OUT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9041666666666665,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9880184331797237,
			"nilai_komparatif_unit": 0.9932540778918831,
			"summary_team": 0.8933333333333333
		}]
	},
	"860054": {
		"nik": "860054",
		"nama": "ANDRE MARGA PRADJA",
		"band": "III",
		"posisi": "MGR AR DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8811594202898547,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0071957236842108,
			"nilai_komparatif_unit": 1.0125329914797578,
			"summary_team": 0.8875
		}]
	},
	"860056": {
		"nik": "860056",
		"nama": "RANY AULIA PARAMITHA",
		"band": "III",
		"posisi": "MGR TAX OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8646153846153859,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0093820769977337,
			"nilai_komparatif_unit": 1.014730930578303,
			"summary_team": 0.8727272727272727
		}]
	},
	"860060": {
		"nik": "860060",
		"nama": "PRAMITA UTAMI",
		"band": "III",
		"posisi": "MGR ASSET OPTIMALIZATION & MONITORING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8472222222222213,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.944262295081968,
			"nilai_komparatif_unit": 0.9492660700380959,
			"summary_team": 0.7999999999999998
		}]
	},
	"860062": {
		"nik": "860062",
		"nama": "MUHAMAD TOMI HAETAMI",
		"band": "III",
		"posisi": "SO PEOPLE ANALYTICS PROJECT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8777777777777769,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9516008935219666,
			"nilai_komparatif_unit": 0.9566435567142112,
			"summary_team": 0.8352941176470587
		}]
	},
	"860065": {
		"nik": "860065",
		"nama": "RISKA NURMINDHA",
		"band": "III",
		"posisi": "SO PERFORMANCE ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9075268817204291,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.024114859213829,
			"nilai_komparatif_unit": 1.0295417838209242,
			"summary_team": 0.9294117647058824
		}]
	},
	"860066": {
		"nik": "860066",
		"nama": "NUGRAHA HADI WIBAWA, M.T",
		"band": "III",
		"posisi": "SENIOR INNOVATOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8471014492753652,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0329341317365235,
			"nilai_komparatif_unit": 1.0384077908740668,
			"summary_team": 0.875
		}]
	},
	"860069": {
		"nik": "860069",
		"nama": "ENI SETIAWATI",
		"band": "III",
		"posisi": "MGR ENTERPRISE&BUSINESS BILLING SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.902777777777777,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.983076923076924,
			"nilai_komparatif_unit": 0.9882863820516812,
			"summary_team": 0.8875000000000001
		}]
	},
	"860077": {
		"nik": "860077",
		"nama": "ALAN RIZKI",
		"band": "III",
		"posisi": "MGR FINANCE SERVICE (DGS)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8469696969696957,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9445438282647599,
			"nilai_komparatif_unit": 0.9495490951037012,
			"summary_team": 0.8
		}]
	},
	"860079": {
		"nik": "860079",
		"nama": "TIFFANY KRISNANDYA",
		"band": "III",
		"posisi": "SENIOR INNOVATOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8691358024691347,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9204545454545464,
			"nilai_komparatif_unit": 0.925332159891539,
			"summary_team": 0.7999999999999998
		}]
	},
	"860080": {
		"nik": "860080",
		"nama": "RIKA AGUSTINI",
		"band": "III",
		"posisi": "MGR FINANCE SERVICE (DDS)",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8646153846153859,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0332147093712913,
			"nilai_komparatif_unit": 1.0386898553280683,
			"summary_team": 0.8933333333333333
		}]
	},
	"860082": {
		"nik": "860082",
		"nama": "I MADE ANANTHA SETIAWAN",
		"band": "III",
		"posisi": "SO INFORMATION SECURITY AUDIT REPORTING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0472727272727271,
			"nilai_komparatif_unit": 1.0528223685877054,
			"summary_team": 0.8727272727272727
		}]
	},
	"860095": {
		"nik": "860095",
		"nama": "DEWI RISTIASSARI",
		"band": "III",
		"posisi": "MGR FINANCE SERVICE BUSINESS 03",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8646153846153859,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9830960854092512,
			"nilai_komparatif_unit": 0.9883056459278264,
			"summary_team": 0.85
		}]
	},
	"860105": {
		"nik": "860105",
		"nama": "DIAN WAHYUNI",
		"band": "III",
		"posisi": "MGR FINANCE SERVICE BUSINESS 01",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8761904761904756,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9130434782608701,
			"nilai_komparatif_unit": 0.9178818204399158,
			"summary_team": 0.7999999999999999
		}]
	},
	"860106": {
		"nik": "860106",
		"nama": "MARTIUS",
		"band": "III",
		"posisi": "SO TELCO REGULATION STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8622222222222212,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9687689508793218,
			"nilai_komparatif_unit": 0.9739025899539007,
			"summary_team": 0.8352941176470587
		}]
	},
	"860123": {
		"nik": "860123",
		"nama": "WOROMITA FATHLISTYA",
		"band": "III",
		"posisi": "MGR DEVELOPMENT BUSINESS TOOLS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8231884057971014,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9575393537696768,
			"nilai_komparatif_unit": 0.9626134856743969,
			"summary_team": 0.7882352941176469
		}]
	},
	"860125": {
		"nik": "860125",
		"nama": "HERIYANTO SAPUTRA",
		"band": "III",
		"posisi": "MGR ACCOUNTING OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8646153846153859,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0553825622775785,
			"nilai_komparatif_unit": 1.0609751787166373,
			"summary_team": 0.9125
		}]
	},
	"860128": {
		"nik": "860128",
		"nama": "RIZKY PONTI ANNASTUTI",
		"band": "III",
		"posisi": "SO PROCESS MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8071428571428563,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.049453409682458,
			"nilai_komparatif_unit": 1.0550146067315642,
			"summary_team": 0.8470588235294116
		}]
	},
	"860136": {
		"nik": "860136",
		"nama": "ARIE HESTININGDARU",
		"band": "III",
		"posisi": "SO PROCESS MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8071428571428563,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9911504424778771,
			"nilai_komparatif_unit": 0.9964026841353664,
			"summary_team": 0.7999999999999999
		}]
	},
	"860145": {
		"nik": "860145",
		"nama": "RESSA LUTHFI RUSMINAR",
		"band": "III",
		"posisi": "SO FINANCIAL MODEL & BUDGET DATABASE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8235294117647056,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9714285714285715,
			"nilai_komparatif_unit": 0.9765763041959372,
			"summary_team": 0.7999999999999998
		}]
	},
	"860149": {
		"nik": "860149",
		"nama": "SIGIT PRIMASATYA",
		"band": "III",
		"posisi": "MGR SECRETARIATE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8481481481481481,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8803493449781661,
			"nilai_komparatif_unit": 0.8850144364765649,
			"summary_team": 0.7466666666666667
		}]
	},
	"870006": {
		"nik": "870006",
		"nama": "RANGGA SUMA AJI",
		"band": "III",
		"posisi": "SO CEO DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8368421052631614,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9559748427672916,
			"nilai_komparatif_unit": 0.9610406841143848,
			"summary_team": 0.8
		}]
	},
	"870010": {
		"nik": "870010",
		"nama": "AFIS HERMAN REZA DEVARA",
		"band": "III",
		"posisi": "SENIOR INNOVATOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8471014492753652,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0731783186872972,
			"nilai_komparatif_unit": 1.0788652372717578,
			"summary_team": 0.9090909090909092
		}]
	},
	"870012": {
		"nik": "870012",
		"nama": "KOMANG GITA AGASTYA",
		"band": "III",
		"posisi": "SO REVIEW MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8368421052631614,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9559748427672914,
			"nilai_komparatif_unit": 0.9610406841143846,
			"summary_team": 0.7999999999999998
		}]
	},
	"870018": {
		"nik": "870018",
		"nama": "HADARY MALLAFI",
		"band": "III",
		"posisi": "SO IS OPERATION AUDIT REPORTING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139859,
			"nilai_komparatif_unit": 0.9912390088866617,
			"summary_team": 0.8545454545454546
		}]
	},
	"870023": {
		"nik": "870023",
		"nama": "SOCA WASKITHA",
		"band": "III",
		"posisi": "MGR HC DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8460317460317445,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0834896810506585,
			"nilai_komparatif_unit": 1.0892312409535598,
			"summary_team": 0.9166666666666665
		}]
	},
	"870031": {
		"nik": "870031",
		"nama": "TIRTA AMANDA NATHASIA",
		"band": "III",
		"posisi": "MGR ACCOUNTING OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.862015503875968,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0440647482014402,
			"nilai_komparatif_unit": 1.0495973899968778,
			"summary_team": 0.9000000000000001
		}]
	},
	"870037": {
		"nik": "870037",
		"nama": "KARTIKA DWI AYUNINGTIAS",
		"band": "III",
		"posisi": "SENIOR INNOVATOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8604938271604932,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9961057593769221,
			"nilai_komparatif_unit": 1.001384259935914,
			"summary_team": 0.857142857142857
		}]
	},
	"870040": {
		"nik": "870040",
		"nama": "MAJU SINAGA",
		"band": "III",
		"posisi": "MGR ASSET COMPLIANCE & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8422222222222211,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0197113146049988,
			"nilai_komparatif_unit": 1.0251149042273693,
			"summary_team": 0.8588235294117645
		}]
	},
	"870042": {
		"nik": "870042",
		"nama": "I GUSTI AGUNG AYU KOMANG TIRTA RATNADI",
		"band": "III",
		"posisi": "MGR CORPORATE TAX",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.933333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9725274725274728,
			"nilai_komparatif_unit": 0.9776810285219509,
			"summary_team": 0.9076923076923077
		}]
	},
	"870051": {
		"nik": "870051",
		"nama": "ENY SHOLIKAH PUJI HASTUTI",
		"band": "III",
		"posisi": "PROJECT TEAM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.875362318840579,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9878262563303477,
			"nilai_komparatif_unit": 0.993060882671117,
			"summary_team": 0.8647058823529413
		}]
	},
	"870055": {
		"nik": "870055",
		"nama": "DITA AYU HANDARI",
		"band": "III",
		"posisi": "MGR SYSTEM INTEGRATION BILLING & PAYMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555548,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9935064935064943,
			"nilai_komparatif_unit": 0.9987712202003911,
			"summary_team": 0.85
		}]
	},
	"870058": {
		"nik": "870058",
		"nama": "YUDHA ADITYA",
		"band": "III",
		"posisi": "MGR HC DEVELOPMENT 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7888888888888886,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0886495443247726,
			"nilai_komparatif_unit": 1.0944184470483738,
			"summary_team": 0.8588235294117648
		}]
	},
	"870059": {
		"nik": "870059",
		"nama": "IMANESA HAQUE DINASTI",
		"band": "III",
		"posisi": "MGR MANAGED SERVICE CATEGORY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8450980392156857,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9762180974477965,
			"nilai_komparatif_unit": 0.9813912105681336,
			"summary_team": 0.8250000000000001
		}]
	},
	"880008": {
		"nik": "880008",
		"nama": "NI LUH YENI PURNAMA WATI",
		"band": "III",
		"posisi": "MGR RA FRAMEWORK & PERFORMANCE MGMT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8722222222222213,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.089171974522294,
			"nilai_komparatif_unit": 1.0949436456749138,
			"summary_team": 0.95
		}]
	},
	"880011": {
		"nik": "880011",
		"nama": "ANITA PUSPITASARI",
		"band": "III",
		"posisi": "MGR CASH & CASHLESS PAYMENT MODE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.852941176470587,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0551724137931047,
			"nilai_komparatif_unit": 1.0607639166266227,
			"summary_team": 0.8999999999999998
		}]
	},
	"880019": {
		"nik": "880019",
		"nama": "WULANDARI",
		"band": "III",
		"posisi": "MGR HC DATA EXPOSURE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8848484848484851,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9910432033719702,
			"nilai_komparatif_unit": 0.9962948767547806,
			"summary_team": 0.8769230769230769
		}]
	},
	"880023": {
		"nik": "880023",
		"nama": "ARTI ANANDITA",
		"band": "III",
		"posisi": "MGR RECIPROCAL ELIMINATION 02",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9185185185185186,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.870967741935484,
			"nilai_komparatif_unit": 0.8755831190371544,
			"summary_team": 0.8000000000000002
		}]
	},
	"880032": {
		"nik": "880032",
		"nama": "LINDA AGNES DEWINTASARI HARIE SURYA",
		"band": "III",
		"posisi": "MGR VAS & MES BILLING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8804597701149418,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9086161879895569,
			"nilai_komparatif_unit": 0.9134310693523671,
			"summary_team": 0.8
		}]
	},
	"880036": {
		"nik": "880036",
		"nama": "RIZKI PRIMASAKTI",
		"band": "III",
		"posisi": "SENIOR INNOVATOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8691358024691347,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0668904958677699,
			"nilai_komparatif_unit": 1.0725440944197386,
			"summary_team": 0.9272727272727272
		}]
	},
	"880042": {
		"nik": "880042",
		"nama": "ABDIANUL HAIQAL",
		"band": "III",
		"posisi": "MGR FINANCIAL REPORTING REVIEW",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9022222222222221,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0450387051372272,
			"nilai_komparatif_unit": 1.0505765080635805,
			"summary_team": 0.9428571428571427
		}]
	},
	"880044": {
		"nik": "880044",
		"nama": "ADITYA FERRY SANJAYA",
		"band": "III",
		"posisi": "SO BENEFIT & FACILITY SYSTEM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8238095238095232,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.899693981638899,
			"nilai_komparatif_unit": 0.9044615830108388,
			"summary_team": 0.7411764705882353
		}]
	},
	"880052": {
		"nik": "880052",
		"nama": "SALMAN ABDURRAHMAN",
		"band": "III",
		"posisi": "MGR CONSOLIDATION PROCESS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9185185185185186,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0049627791563274,
			"nilai_komparatif_unit": 1.0102882142736394,
			"summary_team": 0.923076923076923
		}]
	},
	"880053": {
		"nik": "880053",
		"nama": "GISA RISMALA",
		"band": "III",
		"posisi": "MGR CASH OPERATION(KUG,SSO FC,CDC,PMO,IA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.862015503875968,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860611510791378,
			"nilai_komparatif_unit": 0.99128642388594,
			"summary_team": 0.8500000000000001
		}]
	},
	"880056": {
		"nik": "880056",
		"nama": "AMALIA DESTI ARTHANINGTYAS",
		"band": "III",
		"posisi": "MGR PAYMENT INTERFACE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.852941176470587,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0216748768472919,
			"nilai_komparatif_unit": 1.027088871654349,
			"summary_team": 0.8714285714285712
		}]
	},
	"880057": {
		"nik": "880057",
		"nama": "BUNGA DZIKRILLAH MARTIN",
		"band": "III",
		"posisi": "MGR ACCOUNTING ASSESSMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8756756756756745,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.089268755935424,
			"nilai_komparatif_unit": 1.095040939945979,
			"summary_team": 0.9538461538461538
		}]
	},
	"890002": {
		"nik": "890002",
		"nama": "CHINDIA FERDINANSARI, M.AB",
		"band": "III",
		"posisi": "SO STRATEGIC INVESTMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8777777777777772,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9516008935219663,
			"nilai_komparatif_unit": 0.9566435567142109,
			"summary_team": 0.8352941176470587
		}]
	},
	"890010": {
		"nik": "890010",
		"nama": "AFRIZILLA YULIKA HANIFAH",
		"band": "III",
		"posisi": "MGR BENEFIT & FACILITY ADMINISTRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.846296296296295,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0083150984682727,
			"nilai_komparatif_unit": 1.0136582979837878,
			"summary_team": 0.8533333333333333
		}]
	},
	"890015": {
		"nik": "890015",
		"nama": "SWASTI SRI HARJANTI",
		"band": "III",
		"posisi": "MGR HC DIGITAL TALENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8708333333333331,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.949282296650718,
			"nilai_komparatif_unit": 0.9543126732812538,
			"summary_team": 0.8266666666666667
		}]
	},
	"890066": {
		"nik": "890066",
		"nama": "PATHYA MADHYASTHA BUDHIPUTRA",
		"band": "III",
		"posisi": "SENIOR INNOVATOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8604938271604932,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.929698708751794,
			"nilai_komparatif_unit": 0.9346253092735196,
			"summary_team": 0.7999999999999998
		}]
	},
	"900003": {
		"nik": "900003",
		"nama": "BURHANUDDIN",
		"band": "III",
		"posisi": "SO LEGAL ADVISOR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8743589743589737,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9149560117302058,
			"nilai_komparatif_unit": 0.9198044886854959,
			"summary_team": 0.7999999999999998
		}]
	},
	"900004": {
		"nik": "900004",
		"nama": "MERRY ARIZONA",
		"band": "III",
		"posisi": "SO COMMUNICATION PLAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8911111111111103,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0297784949391238,
			"nilai_komparatif_unit": 1.0352354319259895,
			"summary_team": 0.9176470588235295
		}]
	},
	"900013": {
		"nik": "900013",
		"nama": "YULIO GUNTUR WICAKSANA",
		"band": "III",
		"posisi": "SO GENERAL AFFAIR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8355555555555556,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.013767209011264,
			"nilai_komparatif_unit": 1.0191393000056987,
			"summary_team": 0.8470588235294116
		}]
	},
	"900032": {
		"nik": "900032",
		"nama": "BIMA ARYO PUTRO",
		"band": "III",
		"posisi": "SO EXECUTIVE SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8766666666666662,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.99306642809215,
			"nilai_komparatif_unit": 0.9983288228192714,
			"summary_team": 0.8705882352941177
		}]
	},
	"910156": {
		"nik": "910156",
		"nama": "DYAH DIWASASRI RATNANINGTYAS",
		"band": "III",
		"posisi": "SENIOR EXPERT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7999999999999998,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9545454545454547,
			"nilai_komparatif_unit": 0.9596037213690026,
			"summary_team": 0.7636363636363636
		}]
	}
};