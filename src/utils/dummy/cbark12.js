export const cbark12 = {
	"650637": {
		"nik": "650637",
		"nama": "NUR WIJAYANTO",
		"band": "IV",
		"posisi": "OFF 1 ENT SERVICE QUALITY MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8769230769230767,
			"nilai_komparatif_unit": 0.8830684600478906,
			"summary_team": 0.76
		}]
	},
	"660038": {
		"nik": "660038",
		"nama": "ENDANG BUDI SULISTYA DEWI",
		"band": "IV",
		"posisi": "OFF 1 EXECUTIVE SECRETARY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7972222222222217,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0182414429186313,
			"nilai_komparatif_unit": 1.0253771700365153,
			"summary_team": 0.8117647058823528
		}]
	},
	"660061": {
		"nik": "660061",
		"nama": "ENDANG WIJAYANTI",
		"band": "IV",
		"posisi": "OFF 1 ACCESS PROJECT ADMIN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8200000000000004,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0243902439024386,
			"nilai_komparatif_unit": 1.0315690611599235,
			"summary_team": 0.84
		}]
	},
	"660082": {
		"nik": "660082",
		"nama": "PUJIYANTO",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9199999999999998,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.948616600790514,
			"nilai_komparatif_unit": 0.9552644044620415,
			"summary_team": 0.8727272727272727
		}]
	},
	"660136": {
		"nik": "660136",
		"nama": "VICTOR WOLTER NATALIS ROMPAS",
		"band": "IV",
		"posisi": "OFF 1 WIRELESS PROJECT ADMIN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8070175438596496,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0621118012422353,
			"nilai_komparatif_unit": 1.069554967138749,
			"summary_team": 0.857142857142857
		}]
	},
	"660150": {
		"nik": "660150",
		"nama": "DIDA RACHMA WANDYATI",
		"band": "IV",
		"posisi": "OFF 1 SALES - 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8361111111111105,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0271643541137392,
			"nilai_komparatif_unit": 1.0343626120388578,
			"summary_team": 0.8588235294117645
		}]
	},
	"660307": {
		"nik": "660307",
		"nama": "OTONG",
		"band": "IV",
		"posisi": "OFF 1 RISK MANAGEMENT OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8000000000000005,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9583333333333327,
			"nilai_komparatif_unit": 0.9650492308271901,
			"summary_team": 0.7666666666666666
		}]
	},
	"660317": {
		"nik": "660317",
		"nama": "Rijana Mauluddin",
		"band": "IV",
		"posisi": "MANAGER OPERATION REGIONAL FIELD BATAM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8200000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0060975609756095,
			"nilai_komparatif_unit": 1.0131481850677821,
			"summary_team": 0.8250000000000001
		}]
	},
	"660454": {
		"nik": "660454",
		"nama": "KUATIN",
		"band": "IV",
		"posisi": "ENG 1 FIBER ACCESS QUALITY ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8138888888888886,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.997390082312789,
			"nilai_komparatif_unit": 1.00437968532587,
			"summary_team": 0.8117647058823529
		}]
	},
	"670030": {
		"nik": "670030",
		"nama": "ISWAHYUDI",
		"band": "IV",
		"posisi": "OFF 1 IT COMPLIANCE CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9466666666666662,
			"nilai_komparatif_unit": 0.9533008054084244,
			"summary_team": 0.7888888888888888
		}]
	},
	"670072": {
		"nik": "670072",
		"nama": "R.MAULUD SYAFAAT AHMAD ANWAR",
		"band": "IV",
		"posisi": "OFF I BUSINESS ANALYST",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.018099547511312,
			"nilai_komparatif_unit": 1.025234280241359,
			"summary_team": 0.8823529411764706
		}]
	},
	"670088": {
		"nik": "670088",
		"nama": "BAMBANG IRIANTO",
		"band": "IV",
		"posisi": "OFF 1 DIGITAL CONNECTIVITY DTP MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9245283018867925,
			"nilai_komparatif_unit": 0.9310072973361577,
			"summary_team": 0.8166666666666667
		}]
	},
	"670115": {
		"nik": "670115",
		"nama": "EDDY YUNIARTO",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER CARE RELATIONSHIP",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7537037037037038,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.061425061425061,
			"nilai_komparatif_unit": 1.0688634147223919,
			"summary_team": 0.7999999999999998
		}]
	},
	"670147": {
		"nik": "670147",
		"nama": "R, AHADIYAT RIVAI",
		"band": "IV",
		"posisi": "OFF 1 TRANSPORT BACKBONE & RMJ 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.993710691823899,
			"nilai_komparatif_unit": 1.0006745100619923,
			"summary_team": 0.8777777777777777
		}]
	},
	"670317": {
		"nik": "670317",
		"nama": "RUBEN SEPTIANUS SILABAN",
		"band": "IV",
		"posisi": "OFF 1 NMS TRANSPORT, ME & SERVICE NODE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.019354838709677,
			"nilai_komparatif_unit": 1.0264983683861726,
			"summary_team": 0.8777777777777777
		}]
	},
	"670357": {
		"nik": "670357",
		"nama": "SYAIFUL BAHRI",
		"band": "IV",
		"posisi": "OFF 1 NMS DATA NW & SERVICE APPLICATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8944444444444445,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0186335403726705,
			"nilai_komparatif_unit": 1.0257720152675727,
			"summary_team": 0.911111111111111
		}]
	},
	"670367": {
		"nik": "670367",
		"nama": "DARJAT SUKMAJAYA",
		"band": "IV",
		"posisi": "OFF 1 OPTIMALISASI ACCESS & WIRELESS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9066666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9852941176470587,
			"nilai_komparatif_unit": 0.992198953433582,
			"summary_team": 0.8933333333333332
		}]
	},
	"670370": {
		"nik": "670370",
		"nama": "DWI JATMIKO",
		"band": "IV",
		"posisi": "OFF 1 PROBLEM IDENTIFICATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7750000000000004,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9384164222873895,
			"nilai_komparatif_unit": 0.944992744199008,
			"summary_team": 0.7272727272727272
		}]
	},
	"670381": {
		"nik": "670381",
		"nama": "SUHERMAN",
		"band": "IV",
		"posisi": "OFF 1 SERVICE WIFI RETAIL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9055555555555558,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0184049079754598,
			"nilai_komparatif_unit": 1.0255417806389775,
			"summary_team": 0.9222222222222222
		}]
	},
	"670411": {
		"nik": "670411",
		"nama": "SULISTIAWAN",
		"band": "IV",
		"posisi": "OFF 1 PENGEMBANGAN DAN PEMBINAAN UKM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0323529411764705,
			"nilai_komparatif_unit": 1.0395875601647382,
			"summary_team": 0.9176470588235295
		}]
	},
	"670432": {
		"nik": "670432",
		"nama": "ZULKARNAINI",
		"band": "IV",
		"posisi": "OFF 1 IP NETWORK SERVICE ACTIVATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8711111111111115,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9821428571428567,
			"nilai_komparatif_unit": 0.9890256092328347,
			"summary_team": 0.8555555555555555
		}]
	},
	"670518": {
		"nik": "670518",
		"nama": "SEPI YANTO",
		"band": "IV",
		"posisi": "OFF 1 SUPPORT & SECRETARY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0307692307692307,
			"nilai_komparatif_unit": 1.0379927512843627,
			"summary_team": 0.8933333333333332
		}]
	},
	"670523": {
		"nik": "670523",
		"nama": "ELIAS PARDOMUAN SITUMEANG",
		"band": "IV",
		"posisi": "OFF 1 NETWORK ASSURANCE TRANSPORT & CME",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555547,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9870129870129878,
			"nilai_komparatif_unit": 0.9939298684521723,
			"summary_team": 0.8444444444444443
		}]
	},
	"670533": {
		"nik": "670533",
		"nama": "SARYATONO",
		"band": "IV",
		"posisi": "OFF 1 INBOUND LOGISTIC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8758620689655164,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0074108383510894,
			"nilai_komparatif_unit": 1.0144706657506373,
			"summary_team": 0.8823529411764706
		}]
	},
	"670549": {
		"nik": "670549",
		"nama": "JUNIAR",
		"band": "IV",
		"posisi": "OFF 1 ENTERPRISE SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8399999999999997,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.969387755102041,
			"nilai_komparatif_unit": 0.9761811208012401,
			"summary_team": 0.8142857142857142
		}]
	},
	"670591": {
		"nik": "670591",
		"nama": "LUKMAN",
		"band": "IV",
		"posisi": "OFF 1 PROBLEM IDENTIFICATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8250000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0358126721763083,
			"nilai_komparatif_unit": 1.0430715365893601,
			"summary_team": 0.8545454545454546
		}]
	},
	"680264": {
		"nik": "680264",
		"nama": "EDWIN HAMONANGAN PARULIAN",
		"band": "IV",
		"posisi": "OFF 1 RESOURCE ALLOCATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7000000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9870129870129869,
			"nilai_komparatif_unit": 0.9939298684521715,
			"summary_team": 0.6909090909090909
		}]
	},
	"680303": {
		"nik": "680303",
		"nama": "SOBARUDIN",
		"band": "IV",
		"posisi": "OFF 1 BASIC DELIVERY OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.043181818181818,
			"nilai_komparatif_unit": 1.0504923247818507,
			"summary_team": 0.9272727272727272
		}]
	},
	"680306": {
		"nik": "680306",
		"nama": "DARI INDRIATI",
		"band": "IV",
		"posisi": "OFF 1 COMM&CONVERGE PROD PORT & QUAL MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8400000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0158730158730154,
			"nilai_komparatif_unit": 1.0229921453074977,
			"summary_team": 0.8533333333333332
		}]
	},
	"680314": {
		"nik": "680314",
		"nama": "MOKHAMAD AMRI",
		"band": "IV",
		"posisi": "OFF 1 SERVICE MIGRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0085763293310461,
			"nilai_komparatif_unit": 1.0156443243667173,
			"summary_team": 0.8909090909090909
		}]
	},
	"680374": {
		"nik": "680374",
		"nama": "ARIYANTO",
		"band": "IV",
		"posisi": "OFF 1 ACCESS PROJECT ADM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9066666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9852941176470587,
			"nilai_komparatif_unit": 0.992198953433582,
			"summary_team": 0.8933333333333332
		}]
	},
	"680386": {
		"nik": "680386",
		"nama": "KURNIANDOKO",
		"band": "IV",
		"posisi": "OFF 1 MANAGED OPERATION READINESS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555556,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9488158899923606,
			"nilai_komparatif_unit": 0.955465090261298,
			"summary_team": 0.8117647058823529
		}]
	},
	"680393": {
		"nik": "680393",
		"nama": "SUDARMAWAN",
		"band": "IV",
		"posisi": "OFF 1 CAPEX & OPEX PLANNING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8277777777777782,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9664429530201337,
			"nilai_komparatif_unit": 0.9732156818613275,
			"summary_team": 0.7999999999999999
		}]
	},
	"680415": {
		"nik": "680415",
		"nama": "SOFIAN",
		"band": "IV",
		"posisi": "OFF 1 WIRELESS PROJECT ADMIN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8070175438596496,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9381987577639744,
			"nilai_komparatif_unit": 0.9447735543058949,
			"summary_team": 0.757142857142857
		}]
	},
	"680471": {
		"nik": "680471",
		"nama": "MUKSIRIN",
		"band": "IV",
		"posisi": "OFF 1 TELKOMGROUP & OLO PROJECT ADMIN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8600000000000004,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.03875968992248,
			"nilai_komparatif_unit": 1.0460392067206752,
			"summary_team": 0.8933333333333332
		}]
	},
	"680487": {
		"nik": "680487",
		"nama": "SLAMET BASUKI",
		"band": "IV",
		"posisi": "OFF 1 OLO CORE NETWORK FAULT HANDLING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.92156862745098,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9957446808510644,
			"nilai_komparatif_unit": 1.0027227530666987,
			"summary_team": 0.9176470588235295
		}]
	},
	"680497": {
		"nik": "680497",
		"nama": "MUHARAM",
		"band": "IV",
		"posisi": "OFF 1 WHOLESALE SERVICE MGT TR4,5,6,7",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9898989898989898,
			"nilai_komparatif_unit": 0.9968360961377041,
			"summary_team": 0.8909090909090909
		}]
	},
	"680512": {
		"nik": "680512",
		"nama": "JOKO IRAWAN",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8958333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9539112050739958,
			"nilai_komparatif_unit": 0.9605961127660155,
			"summary_team": 0.8545454545454546
		}]
	},
	"680515": {
		"nik": "680515",
		"nama": "DEVI ROOSFIANTY",
		"band": "IV",
		"posisi": "OFF 1 CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8175438596491225,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.94976016157536,
			"nilai_komparatif_unit": 0.956415979198549,
			"summary_team": 0.776470588235294
		}]
	},
	"680519": {
		"nik": "680519",
		"nama": "RUSDIANTORO",
		"band": "IV",
		"posisi": "OFF 1 OLO & IP TRANSIT 1 OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.860606060606061,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0199530516431918,
			"nilai_komparatif_unit": 1.0271007735319388,
			"summary_team": 0.8777777777777777
		}]
	},
	"680526": {
		"nik": "680526",
		"nama": "WIDADI",
		"band": "IV",
		"posisi": "OFF 1 INBOUND LOGISTIC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.941176470588235,
			"nilai_komparatif_unit": 0.9477721346231229,
			"summary_team": 0.8
		}]
	},
	"680532": {
		"nik": "680532",
		"nama": "MOHAMAD TAUFIQ",
		"band": "IV",
		"posisi": "OFF 1 HARDWARE & ASSET METRO",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888884,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625000000000005,
			"nilai_komparatif_unit": 0.969245097048179,
			"summary_team": 0.8555555555555555
		}]
	},
	"680543": {
		"nik": "680543",
		"nama": "BENNY SOFYAN",
		"band": "IV",
		"posisi": "OFF 1 O & M METRO OPERATION 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888884,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9125000000000004,
			"nilai_komparatif_unit": 0.9188947023963254,
			"summary_team": 0.811111111111111
		}]
	},
	"680552": {
		"nik": "680552",
		"nama": "ENTIS SUTISNA",
		"band": "IV",
		"posisi": "OFF 1 OLO FIBER ACCESS FAULT HANDLING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777778,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0856291883842144,
			"nilai_komparatif_unit": 1.0932371616143306,
			"summary_team": 0.9529411764705882
		}]
	},
	"680559": {
		"nik": "680559",
		"nama": "PARMO",
		"band": "IV",
		"posisi": "OFF 1 TELKOMGROUP&OLO NW PROJECT CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8231884057971008,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9070422535211274,
			"nilai_komparatif_unit": 0.913398708613905,
			"summary_team": 0.7466666666666667
		}]
	},
	"680573": {
		"nik": "680573",
		"nama": "SUGIHARTO",
		"band": "IV",
		"posisi": "OFF 1 SALES PLAN & EVALUATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8904761904761901,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8983957219251342,
			"nilai_komparatif_unit": 0.9046915830493455,
			"summary_team": 0.8
		}]
	},
	"680577": {
		"nik": "680577",
		"nama": "HAERUDIN",
		"band": "IV",
		"posisi": "OFF 1 DESIGN ACCESS NODE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555551,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0259740259740264,
			"nilai_komparatif_unit": 1.033163942206863,
			"summary_team": 0.8777777777777777
		}]
	},
	"690028": {
		"nik": "690028",
		"nama": "IDA FARIDA",
		"band": "IV",
		"posisi": "OFF 1 OPERATION&MAINTENANCE ADM SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.882962962962962,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9186241610738266,
			"nilai_komparatif_unit": 0.9250617809358987,
			"summary_team": 0.8111111111111111
		}]
	},
	"690035": {
		"nik": "690035",
		"nama": "LUKMAN DINAR",
		"band": "IV",
		"posisi": "OFF 1 NMS ACCESS INFRASTRUCTURE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8944444444444445,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.981366459627329,
			"nilai_komparatif_unit": 0.988243770806564,
			"summary_team": 0.8777777777777777
		}]
	},
	"690067": {
		"nik": "690067",
		"nama": "ALDRIAN",
		"band": "IV",
		"posisi": "OFF 1 IP NETWORK INFRAST INTEGRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8791666666666659,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9099526066350718,
			"nilai_komparatif_unit": 0.9163294571711721,
			"summary_team": 0.7999999999999999
		}]
	},
	"690076": {
		"nik": "690076",
		"nama": "NURDIN",
		"band": "IV",
		"posisi": "OFF 1 IP MULTIMEDIA SUB SYSTEM 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.980392156862746,
			"nilai_komparatif_unit": 0.9872626402324209,
			"summary_team": 0.8333333333333333
		}]
	},
	"690078": {
		"nik": "690078",
		"nama": "ABDUL RAHMAN",
		"band": "IV",
		"posisi": "OFF 1 QUALITY STANDARIZATION & PERFORM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.877777777777778,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9620253164556959,
			"nilai_komparatif_unit": 0.9687670869723694,
			"summary_team": 0.8444444444444443
		}]
	},
	"690092": {
		"nik": "690092",
		"nama": "TRI PUDJIANTORO",
		"band": "IV",
		"posisi": "OFF 1 FACILITIES MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8799999999999993,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272734,
			"nilai_komparatif_unit": 1.029894436060639,
			"summary_team": 0.8999999999999999
		}]
	},
	"690093": {
		"nik": "690093",
		"nama": "TRIAS SURYOARSIH",
		"band": "IV",
		"posisi": "OFF 1 OPEX MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0158730158730154,
			"nilai_komparatif_unit": 1.0229921453074977,
			"summary_team": 0.914285714285714
		}]
	},
	"690099": {
		"nik": "690099",
		"nama": "AHMAD IBRAHIM AZIS",
		"band": "IV",
		"posisi": "OFF 1 TGROUP&OLO WLESS ACC PROJ INTEGRAT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8400000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0158730158730154,
			"nilai_komparatif_unit": 1.0229921453074977,
			"summary_team": 0.8533333333333332
		}]
	},
	"690149": {
		"nik": "690149",
		"nama": "WURYANTO",
		"band": "IV",
		"posisi": "MGR  ENT BIDDING MANAGEMENT 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9066666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.0070078930370683,
			"summary_team": 0.9066666666666665
		}]
	},
	"690156": {
		"nik": "690156",
		"nama": "BASUKI RAHMAD",
		"band": "IV",
		"posisi": "OFF 1 ADMINISTRATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8133333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.983606557377049,
			"nilai_komparatif_unit": 0.9904995669217066,
			"summary_team": 0.7999999999999999
		}]
	},
	"690157": {
		"nik": "690157",
		"nama": "CEPI SUHENDRI",
		"band": "IV",
		"posisi": "OFF 1 OLO ACCESS NETWORK SURVEILANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777778,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9381980640357407,
			"nilai_komparatif_unit": 0.9447728557160879,
			"summary_team": 0.8235294117647057
		}]
	},
	"690163": {
		"nik": "690163",
		"nama": "HANAFI",
		"band": "IV",
		"posisi": "OFF 1 NETWORK ASSURANCE SERV NODE & IPTV",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555547,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0259740259740269,
			"nilai_komparatif_unit": 1.0331639422068635,
			"summary_team": 0.8777777777777777
		}]
	},
	"690165": {
		"nik": "690165",
		"nama": "HARTO TRIYONO",
		"band": "IV",
		"posisi": "OFF 1 ENT SERVICE MGT TREG 4,5,6,7",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8800000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.0070078930370683,
			"summary_team": 0.88
		}]
	},
	"690177": {
		"nik": "690177",
		"nama": "SAHRONI",
		"band": "IV",
		"posisi": "OFF 1 SALES PLAN & EVALUATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8904761904761901,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0413223140495873,
			"nilai_komparatif_unit": 1.0486197894435594,
			"summary_team": 0.9272727272727272
		}]
	},
	"690180": {
		"nik": "690180",
		"nama": "SIHJONO",
		"band": "IV",
		"posisi": "OFF 1 SOLUTION INTERNET 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9433962264150939,
			"nilai_komparatif_unit": 0.950007446261385,
			"summary_team": 0.8333333333333331
		}]
	},
	"690184": {
		"nik": "690184",
		"nama": "YUSUF SARTONO",
		"band": "IV",
		"posisi": "OFF 1 CONNECTIVITY SERVICE HANDLING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8476190476190479,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0617977528089884,
			"nilai_komparatif_unit": 1.0692387178876734,
			"summary_team": 0.8999999999999999
		}]
	},
	"690216": {
		"nik": "690216",
		"nama": "RIDWAN",
		"band": "IV",
		"posisi": "OFF 1 OFFERING 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0129870129870129,
			"nilai_komparatif_unit": 1.0200859176219654,
			"summary_team": 0.9454545454545454
		}]
	},
	"690226": {
		"nik": "690226",
		"nama": "HARYO ISMAIL",
		"band": "IV",
		"posisi": "OFF 1 LEGAL & LITIGATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9915598221764022,
			"nilai_komparatif_unit": 0.9985085673500691,
			"summary_team": 0.8181818181818181
		}]
	},
	"690240": {
		"nik": "690240",
		"nama": "NUR IDRIS FAHMI",
		"band": "IV",
		"posisi": "OFF 1 PLANNING MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8866666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.052631578947368,
			"nilai_komparatif_unit": 1.0600083084600715,
			"summary_team": 0.9333333333333332
		}]
	},
	"690241": {
		"nik": "690241",
		"nama": "RIDUAN TAMPUBOLON",
		"band": "IV",
		"posisi": "OFF 1 LAYER 3 SERVICE ACTIVATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8711111111111115,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9693877551020402,
			"nilai_komparatif_unit": 0.9761811208012393,
			"summary_team": 0.8444444444444443
		}]
	},
	"690327": {
		"nik": "690327",
		"nama": "SATRIO",
		"band": "IV",
		"posisi": "OFF 1 ENT SERVICE MGT TREG 4,5,6,7",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0195852534562209,
			"nilai_komparatif_unit": 1.0267303978546145,
			"summary_team": 0.8428571428571427
		}]
	},
	"690333": {
		"nik": "690333",
		"nama": "WARNO",
		"band": "IV",
		"posisi": "OFF 1 TG & OLO WiFi PROJECT MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8070175438596496,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.013833992094861,
			"nilai_komparatif_unit": 1.0209388322688058,
			"summary_team": 0.8181818181818181
		}]
	},
	"690347": {
		"nik": "690347",
		"nama": "JONATHAN PATTINASARANY",
		"band": "IV",
		"posisi": "OFF 1 REDIRECTOR & PE-SPEEDY 1 OPR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8711111111111103,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.982142857142858,
			"nilai_komparatif_unit": 0.989025609232836,
			"summary_team": 0.8555555555555555
		}]
	},
	"690371": {
		"nik": "690371",
		"nama": "NOVI KUSUMA INDAH",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8390804597701145,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0936341659951658,
			"nilai_komparatif_unit": 1.1012982372521436,
			"summary_team": 0.9176470588235295
		}]
	},
	"690372": {
		"nik": "690372",
		"nama": "PUADI",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER ANALYSIS & SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0044642857142854,
			"nilai_komparatif_unit": 1.0115034639881264,
			"summary_team": 0.857142857142857
		}]
	},
	"690380": {
		"nik": "690380",
		"nama": "TEGUH DWIYANTO NUGROHO",
		"band": "IV",
		"posisi": "OFF 1 BUDGET OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9066666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999997,
			"nilai_komparatif_unit": 1.0070078930370683,
			"summary_team": 0.9066666666666665
		}]
	},
	"690417": {
		"nik": "690417",
		"nama": "DARYANTO",
		"band": "IV",
		"posisi": "OFF 1 ACCESS PLANNING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8733333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0229007633587783,
			"nilai_komparatif_unit": 1.0300691424959323,
			"summary_team": 0.8933333333333333
		}]
	},
	"690419": {
		"nik": "690419",
		"nama": "ACHMAD ZULKARNAIN",
		"band": "IV",
		"posisi": "OFF 1 BROADBAND NODE DEPLOYMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.766666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.113043478260869,
			"nilai_komparatif_unit": 1.1208435679021278,
			"summary_team": 0.8533333333333333
		}]
	},
	"690448": {
		"nik": "690448",
		"nama": "AGUS PURWANTO",
		"band": "IV",
		"posisi": "OFF 1 DATA & REPORTING ACCESS MIGRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9692307692307689,
			"nilai_komparatif_unit": 0.9760230347897738,
			"summary_team": 0.84
		}]
	},
	"690463": {
		"nik": "690463",
		"nama": "GUNTORO BUDI",
		"band": "IV",
		"posisi": "OFF 1 DEBT MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.806666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.991735537190082,
			"nilai_komparatif_unit": 0.9986855137557697,
			"summary_team": 0.7999999999999998
		}]
	},
	"690465": {
		"nik": "690465",
		"nama": "SUPARDIJONO",
		"band": "IV",
		"posisi": "OFF 1 SOLUTION TELEPON 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7999999999999997,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0972222222222225,
			"nilai_komparatif_unit": 1.1049114381934506,
			"summary_team": 0.8777777777777777
		}]
	},
	"690482": {
		"nik": "690482",
		"nama": "RAJA YENI ROZALINA",
		"band": "IV",
		"posisi": "OFF 1 CASH COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9787595581988102,
			"nilai_komparatif_unit": 0.9856186004916759,
			"summary_team": 0.8727272727272727
		}]
	},
	"690490": {
		"nik": "690490",
		"nama": "JULIA WATY",
		"band": "IV",
		"posisi": "OFF 1 WIRELESS OPERATION WAG",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8944444444444447,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0186335403726703,
			"nilai_komparatif_unit": 1.0257720152675724,
			"summary_team": 0.911111111111111
		}]
	},
	"690497": {
		"nik": "690497",
		"nama": "SULAEMAN",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER HANDLING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8250000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969694,
			"nilai_komparatif_unit": 0.9764925023389752,
			"summary_team": 0.8
		}]
	},
	"690509": {
		"nik": "690509",
		"nama": "SRI IDAYATUN",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9826989619377171,
			"nilai_komparatif_unit": 0.9895856111506149,
			"summary_team": 0.8352941176470587
		}]
	},
	"690518": {
		"nik": "690518",
		"nama": "MOHAMMAD SUHAENI",
		"band": "IV",
		"posisi": "OFF 1 PCEF & PCRF OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8711111111111103,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0459183673469397,
			"nilai_komparatif_unit": 1.0532480513908125,
			"summary_team": 0.911111111111111
		}]
	},
	"690520": {
		"nik": "690520",
		"nama": "JERRY SONYA PUTRA",
		"band": "IV",
		"posisi": "OFF 1 IT EXPOSURE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8499999999999995,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9747899159663872,
			"nilai_komparatif_unit": 0.9816211394310926,
			"summary_team": 0.8285714285714286
		}]
	},
	"690537": {
		"nik": "690537",
		"nama": "JULIHARTO",
		"band": "IV",
		"posisi": "OFF 1 TRANSPORT OSP PLANNING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9850746268656716,
			"nilai_komparatif_unit": 0.9919779244842765,
			"summary_team": 0.88
		}]
	},
	"690542": {
		"nik": "690542",
		"nama": "ERNAWATI",
		"band": "IV",
		"posisi": "OFF 1 ACCOUNT SETTLEMENT&RECONCILIATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0703259005145793,
			"nilai_komparatif_unit": 1.0778266299401895,
			"summary_team": 0.9454545454545454
		}]
	},
	"690554": {
		"nik": "690554",
		"nama": "ACHMAD HADI",
		"band": "IV",
		"posisi": "OFF 1 ENTERPRISE SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.015301530153015,
			"nilai_komparatif_unit": 1.0224166546766993,
			"summary_team": 0.8545454545454546
		}]
	},
	"690557": {
		"nik": "690557",
		"nama": "YANTI KOMALASARI",
		"band": "IV",
		"posisi": "OFF 1 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8644444444444436,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.966278542265236,
			"nilai_komparatif_unit": 0.9730501189334453,
			"summary_team": 0.8352941176470587
		}]
	},
	"700012": {
		"nik": "700012",
		"nama": "FENY IRIANI",
		"band": "IV",
		"posisi": "MGR PENGEMBANGAN & PEMBINAAN UKM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9151515151515153,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9898714452668484,
			"nilai_komparatif_unit": 0.9968083584757269,
			"summary_team": 0.9058823529411766
		}]
	},
	"700014": {
		"nik": "700014",
		"nama": "TUTI ADAWIYAH",
		"band": "IV",
		"posisi": "OFF 1 PERFORMANCE & ORDER ALIGNMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9055555555555558,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0842163970998324,
			"nilai_komparatif_unit": 1.0918144696397438,
			"summary_team": 0.9818181818181818
		}]
	},
	"700053": {
		"nik": "700053",
		"nama": "ROZAL ADHAM",
		"band": "IV",
		"posisi": "OFF 1 SOLUTION TELEPON 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999997,
			"nilai_komparatif_unit": 1.0070078930370683,
			"summary_team": 0.8666666666666665
		}]
	},
	"700055": {
		"nik": "700055",
		"nama": "PRIYO UTOMO",
		"band": "IV",
		"posisi": "OFF 1 TRANSPORT OSP DEPLOYMENT AREA-1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8600000000000004,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.023255813953488,
			"nilai_komparatif_unit": 1.0304266812472325,
			"summary_team": 0.88
		}]
	},
	"700067": {
		"nik": "700067",
		"nama": "SUDIRMAN",
		"band": "IV",
		"posisi": "OFF 1 FACILITY & ASSET MGT JKT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8695035460992895,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9606563669513496,
			"nilai_komparatif_unit": 0.9673885440163235,
			"summary_team": 0.8352941176470587
		}]
	},
	"700075": {
		"nik": "700075",
		"nama": "BUDI SETIAWAN",
		"band": "IV",
		"posisi": "OFF 1 BROADBAND APPLICATION OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8711111111111103,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.020408163265307,
			"nilai_komparatif_unit": 1.027559074527622,
			"summary_team": 0.8888888888888888
		}]
	},
	"700084": {
		"nik": "700084",
		"nama": "HANAFI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8111111111111103,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0878323932312661,
			"nilai_komparatif_unit": 1.095455806285289,
			"summary_team": 0.8823529411764706
		}]
	},
	"700087": {
		"nik": "700087",
		"nama": "IMAM HAMBALI",
		"band": "IV",
		"posisi": "OFF 1 O & M METRO OPERATION 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888884,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000004,
			"nilai_komparatif_unit": 1.007007893037069,
			"summary_team": 0.8888888888888888
		}]
	},
	"700089": {
		"nik": "700089",
		"nama": "IRWAN AZHARI GUMAY",
		"band": "IV",
		"posisi": "MGR DECISION SUPPORT & EIS OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8589147286821694,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9605144404332142,
			"nilai_komparatif_unit": 0.9672456228923298,
			"summary_team": 0.825
		}]
	},
	"700130": {
		"nik": "700130",
		"nama": "DEDI SUTARDI",
		"band": "IV",
		"posisi": "OFF 1 IH3P INTEGRATION 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888884,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9875000000000004,
			"nilai_komparatif_unit": 0.9944202943741055,
			"summary_team": 0.8777777777777777
		}]
	},
	"700135": {
		"nik": "700135",
		"nama": "KUSNI",
		"band": "IV",
		"posisi": "OFF 1 MANAGED OPERATION HANDLING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.792592592592593,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.955273698264352,
			"nilai_komparatif_unit": 0.9619681541629135,
			"summary_team": 0.7571428571428571
		}]
	},
	"700143": {
		"nik": "700143",
		"nama": "SADIYO",
		"band": "IV",
		"posisi": "OFF 1 IH3P INTEGRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8791666666666659,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0399458361543676,
			"nilai_komparatif_unit": 1.0472336653384822,
			"summary_team": 0.9142857142857141
		}]
	},
	"700172": {
		"nik": "700172",
		"nama": "BUDHI HARIYADI",
		"band": "IV",
		"posisi": "OFF 1 CME DEPLOYMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0597014925373134,
			"nilai_komparatif_unit": 1.0671277672482367,
			"summary_team": 0.9466666666666667
		}]
	},
	"700186": {
		"nik": "700186",
		"nama": "DADANG SURYADI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8361111111111105,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9990228649599381,
			"nilai_komparatif_unit": 1.006023910339163,
			"summary_team": 0.8352941176470587
		}]
	},
	"700216": {
		"nik": "700216",
		"nama": "PURWANTO",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER HANDLING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181817,
			"nilai_komparatif_unit": 0.9886986586182126,
			"summary_team": 0.8181818181818181
		}]
	},
	"700237": {
		"nik": "700237",
		"nama": "TOMY CHERIAWAN",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8390804597701145,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.967445608380339,
			"nilai_komparatif_unit": 0.9742253637230501,
			"summary_team": 0.8117647058823529
		}]
	},
	"700247": {
		"nik": "700247",
		"nama": "ANDI HARYADI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9965397923875441,
			"nilai_komparatif_unit": 1.0035234366597785,
			"summary_team": 0.8470588235294116
		}]
	},
	"700299": {
		"nik": "700299",
		"nama": "SYAIFUL BACHRI",
		"band": "IV",
		"posisi": "OFF 1 REAL TIME PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9846153846153841,
			"nilai_komparatif_unit": 0.9915154639134208,
			"summary_team": 0.8533333333333332
		}]
	},
	"700323": {
		"nik": "700323",
		"nama": "EVAN HUTAJULU",
		"band": "IV",
		"posisi": "OFF 1 NETWORK ASSURANCE IP BACKBONE & BR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555547,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0259740259740269,
			"nilai_komparatif_unit": 1.0331639422068635,
			"summary_team": 0.8777777777777777
		}]
	},
	"700333": {
		"nik": "700333",
		"nama": "NASOHAN",
		"band": "IV",
		"posisi": "OFF 1 NETWORK  PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8158730158730151,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9280155642023353,
			"nilai_komparatif_unit": 0.9345189980130001,
			"summary_team": 0.757142857142857
		}]
	},
	"700334": {
		"nik": "700334",
		"nama": "NURSAMSU",
		"band": "IV",
		"posisi": "OFF 1 SERVICE WIFI MANAGE SERVICE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9055555555555558,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9815950920245395,
			"nilai_komparatif_unit": 0.988474005435159,
			"summary_team": 0.8888888888888888
		}]
	},
	"700373": {
		"nik": "700373",
		"nama": "SUHARDI",
		"band": "IV",
		"posisi": "OFF 1 ACCESS PARTNERSHIP CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8722222222222225,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006369426751592,
			"nilai_komparatif_unit": 1.013421956050043,
			"summary_team": 0.8777777777777777
		}]
	},
	"700393": {
		"nik": "700393",
		"nama": "MAIDA KUSTANTINA",
		"band": "IV",
		"posisi": "OFF 1 SALES - 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8595238095238087,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.012872739123351,
			"nilai_komparatif_unit": 1.01997084293929,
			"summary_team": 0.8705882352941174
		}]
	},
	"700400": {
		"nik": "700400",
		"nama": "SUGESTI SRI WIDODO",
		"band": "IV",
		"posisi": "OFF 1 VVIP SERVICE & EVENT HANDLING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333332,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365353,
			"nilai_komparatif_unit": 0.9949168891755601,
			"summary_team": 0.8727272727272727
		}]
	},
	"700484": {
		"nik": "700484",
		"nama": "PARIYANTO",
		"band": "IV",
		"posisi": "OFF 1 BUSINESS PERFORMANCE SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.849382716049382,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0478197674418614,
			"nilai_komparatif_unit": 1.05516277629422,
			"summary_team": 0.8900000000000001
		}]
	},
	"700485": {
		"nik": "700485",
		"nama": "NOVI CHANDRAWAN",
		"band": "IV",
		"posisi": "OFF 1 COMMUNICATION RELATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8611111111111115,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.019354838709677,
			"nilai_komparatif_unit": 1.0264983683861726,
			"summary_team": 0.8777777777777777
		}]
	},
	"700491": {
		"nik": "700491",
		"nama": "ABDUL FATTAH",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8111111111111103,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0008058017727646,
			"nilai_komparatif_unit": 1.0078193417824657,
			"summary_team": 0.8117647058823527
		}]
	},
	"700498": {
		"nik": "700498",
		"nama": "RINO SASMITO",
		"band": "IV",
		"posisi": "OFF 1 ENT SERVICE MGT TREG 1,2,3",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0303030303030303,
			"nilai_komparatif_unit": 1.0375232837351616,
			"summary_team": 0.9272727272727272
		}]
	},
	"700501": {
		"nik": "700501",
		"nama": "ROSYANA",
		"band": "IV",
		"posisi": "OFF 1 ICM IP CORE NETWORK",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8711111111111115,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9839650145772588,
			"nilai_komparatif_unit": 0.9908605361516338,
			"summary_team": 0.857142857142857
		}]
	},
	"700502": {
		"nik": "700502",
		"nama": "MOCHAMAD SOLEH",
		"band": "IV",
		"posisi": "OFF 1 ACCESS ISP PROJECT INTEGRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0322580645161288,
			"nilai_komparatif_unit": 1.0394920186189092,
			"summary_team": 0.8533333333333332
		}]
	},
	"700509": {
		"nik": "700509",
		"nama": "AFRIZAL",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8595238095238087,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9307479224376739,
			"nilai_komparatif_unit": 0.9372705043225908,
			"summary_team": 0.7999999999999999
		}]
	},
	"700526": {
		"nik": "700526",
		"nama": "MUSTAKIM",
		"band": "IV",
		"posisi": "OFF 1 IP MULTIMEDIA SUB SYSTEM 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0326797385620923,
			"nilai_komparatif_unit": 1.0399166477114834,
			"summary_team": 0.8777777777777777
		}]
	},
	"700540": {
		"nik": "700540",
		"nama": "SUTISNA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.794202898550724,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9924860455130966,
			"nilai_komparatif_unit": 0.9994412815608356,
			"summary_team": 0.7882352941176471
		}]
	},
	"700542": {
		"nik": "700542",
		"nama": "SUHADA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.794202898550724,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9184199227136118,
			"nilai_komparatif_unit": 0.9248561112951016,
			"summary_team": 0.7294117647058824
		}]
	},
	"700544": {
		"nik": "700544",
		"nama": "AHMADI",
		"band": "IV",
		"posisi": "OFF 1 SLG PERFORMANCE ANALYSIS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200685,
			"nilai_komparatif_unit": 1.0570991947490322,
			"summary_team": 0.9272727272727272
		}]
	},
	"700552": {
		"nik": "700552",
		"nama": "SANTI WIDIANINGSIH",
		"band": "IV",
		"posisi": "OFF 1 SUPPORT FACILITY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8611111111111115,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0322580645161283,
			"nilai_komparatif_unit": 1.0394920186189087,
			"summary_team": 0.8888888888888887
		}]
	},
	"700557": {
		"nik": "700557",
		"nama": "RUSDY SUGIH ALAM",
		"band": "IV",
		"posisi": "OFF 1 CONS SERVICE QUALITY MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139859,
			"nilai_komparatif_unit": 0.9929238665610255,
			"summary_team": 0.8545454545454546
		}]
	},
	"700560": {
		"nik": "700560",
		"nama": "SUKENDAR",
		"band": "IV",
		"posisi": "OFF 1 PROPOSAL EDITOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559436,
			"nilai_komparatif_unit": 0.9506717871328965,
			"summary_team": 0.8181818181818181
		}]
	},
	"700580": {
		"nik": "700580",
		"nama": "MURTEJO",
		"band": "IV",
		"posisi": "OFF 1 CME O&M & QUALITY CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8944444444444449,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9813664596273285,
			"nilai_komparatif_unit": 0.9882437708065634,
			"summary_team": 0.8777777777777777
		}]
	},
	"700582": {
		"nik": "700582",
		"nama": "ISMAIL",
		"band": "IV",
		"posisi": "OFF 1 OLO ACCESS NW READINESS & INTEGRAT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.855555555555556,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0649350649350642,
			"nilai_komparatif_unit": 1.0723980159615527,
			"summary_team": 0.911111111111111
		}]
	},
	"700593": {
		"nik": "700593",
		"nama": "NURMAYANI",
		"band": "IV",
		"posisi": "OFF 1 BUDGET MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9538461538461536,
			"nilai_komparatif_unit": 0.9605306056661266,
			"summary_team": 0.8266666666666665
		}]
	},
	"700607": {
		"nik": "700607",
		"nama": "WAGIYO",
		"band": "IV",
		"posisi": "OFF 1 PROCUREMENT PROCESS I",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444447,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9891640866873062,
			"nilai_komparatif_unit": 0.9960960428029204,
			"summary_team": 0.8352941176470587
		}]
	},
	"700611": {
		"nik": "700611",
		"nama": "MUKY WIBOWO",
		"band": "IV",
		"posisi": "OFF 1 OLO RADIO ACCESS FAULT HANDLING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777778,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.032017870439315,
			"nilai_komparatif_unit": 1.039250141287697,
			"summary_team": 0.9058823529411766
		}]
	},
	"700628": {
		"nik": "700628",
		"nama": "BUDIYANTI",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER ANALYSIS & SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8399999999999989,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9523809523809534,
			"nilai_komparatif_unit": 0.9590551362257806,
			"summary_team": 0.7999999999999998
		}]
	},
	"700630": {
		"nik": "700630",
		"nama": "HUSNI WARDANA",
		"band": "IV",
		"posisi": "OFF 1 QUALITY ENHANCEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7750000000000004,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0322580645161286,
			"nilai_komparatif_unit": 1.039492018618909,
			"summary_team": 0.8
		}]
	},
	"700631": {
		"nik": "700631",
		"nama": "INDRA RITAWAN",
		"band": "IV",
		"posisi": "OFF 1 WIFI INFRASTRUCTURE INTEGRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8791666666666659,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0399458361543676,
			"nilai_komparatif_unit": 1.0472336653384822,
			"summary_team": 0.9142857142857141
		}]
	},
	"700636": {
		"nik": "700636",
		"nama": "KARNADI",
		"band": "IV",
		"posisi": "OFF 1 RADIO IP OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9074074074074071,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346942,
			"nilai_komparatif_unit": 0.9864567115465164,
			"summary_team": 0.8888888888888888
		}]
	},
	"700637": {
		"nik": "700637",
		"nama": "MOCHAMAD MUGI HARTONO",
		"band": "IV",
		"posisi": "OFF 1 SECRETARY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8564102564102558,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9553620032661957,
			"nilai_komparatif_unit": 0.9620570779967647,
			"summary_team": 0.8181818181818183
		}]
	},
	"710010": {
		"nik": "710010",
		"nama": "SAEFUDIN",
		"band": "IV",
		"posisi": "OFF 1 ALLOCATION SERVICE NODE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777781,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0126582278481007,
			"nilai_komparatif_unit": 1.0197548283919675,
			"summary_team": 0.8888888888888887
		}]
	},
	"710042": {
		"nik": "710042",
		"nama": "MUDJI TEGUH IMANTO",
		"band": "IV",
		"posisi": "OFF 1 NW ASSURANCE WIRELESS BROADBAND",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555547,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.03896103896104,
			"nilai_komparatif_unit": 1.0462419667917606,
			"summary_team": 0.8888888888888888
		}]
	},
	"710048": {
		"nik": "710048",
		"nama": "SOMANTRI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8925925925925923,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0676104466682943,
			"nilai_komparatif_unit": 1.0750921464838026,
			"summary_team": 0.9529411764705882
		}]
	},
	"710062": {
		"nik": "710062",
		"nama": "MUCHTAR AWALUDDIN",
		"band": "IV",
		"posisi": "OFF 1 PROJECT ADMIN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9037037037037039,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9774590163934423,
			"nilai_komparatif_unit": 0.9843089446284458,
			"summary_team": 0.8833333333333333
		}]
	},
	"710071": {
		"nik": "710071",
		"nama": "SUTARYOKO",
		"band": "IV",
		"posisi": "OFF 1 DIGI CONNECTIVIT QUAL MONITORING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9380952380952379,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0659898477157363,
			"nilai_komparatif_unit": 1.0734601905471293,
			"summary_team": 1
		}]
	},
	"710088": {
		"nik": "710088",
		"nama": "BUDI KAMARUDIN",
		"band": "IV",
		"posisi": "OFF 1 ACCESS MAINTENANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8388888888888892,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.993377483443708,
			"nilai_komparatif_unit": 1.000338966593114,
			"summary_team": 0.8333333333333331
		}]
	},
	"710091": {
		"nik": "710091",
		"nama": "NAHASON SINAGA",
		"band": "IV",
		"posisi": "OFF 1 SALES - 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.794202898550724,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.061261730969761,
			"nilai_komparatif_unit": 1.0686989396647313,
			"summary_team": 0.8428571428571427
		}]
	},
	"710099": {
		"nik": "710099",
		"nama": "MURWANINGSIH",
		"band": "IV",
		"posisi": "OFF 1 ACCOUNT TEAM PLANNING AND DEVELOPM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666684,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0388994307400354,
			"nilai_komparatif_unit": 1.046179926826933,
			"summary_team": 0.8588235294117645
		}]
	},
	"710110": {
		"nik": "710110",
		"nama": "BUDI",
		"band": "IV",
		"posisi": "OFF 1 DESAIN ADDITIONAL LINE UNIT AREA 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8400000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9523809523809521,
			"nilai_komparatif_unit": 0.9590551362257793,
			"summary_team": 0.7999999999999999
		}]
	},
	"710123": {
		"nik": "710123",
		"nama": "INDRIATI HASIBUAN",
		"band": "IV",
		"posisi": "OFF 1 SUBSIDIARIES GOVERNANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8571428571428567,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8696969696969702,
			"nilai_komparatif_unit": 0.8757917130352691,
			"summary_team": 0.7454545454545455
		}]
	},
	"710126": {
		"nik": "710126",
		"nama": "RACHMAWATI PENE",
		"band": "IV",
		"posisi": "OFF 1 NW CAPITALIZATION  MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8800000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0151515151515147,
			"nilai_komparatif_unit": 1.0222655883861145,
			"summary_team": 0.8933333333333332
		}]
	},
	"710166": {
		"nik": "710166",
		"nama": "ADANG",
		"band": "IV",
		"posisi": "OFF 1 REVENUE ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.855555555555556,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9075630252100835,
			"nilai_komparatif_unit": 0.9139231298151541,
			"summary_team": 0.776470588235294
		}]
	},
	"710193": {
		"nik": "710193",
		"nama": "SYARIPUDDIN",
		"band": "IV",
		"posisi": "OFF 1 BRAS OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8711111111111103,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.058673469387756,
			"nilai_komparatif_unit": 1.0660925398224077,
			"summary_team": 0.9222222222222222
		}]
	},
	"710201": {
		"nik": "710201",
		"nama": "SUTARTO",
		"band": "IV",
		"posisi": "OFF 1 LAYER 2 SERVICE ACTIVATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8711111111111115,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0076530612244892,
			"nilai_komparatif_unit": 1.014714586096025,
			"summary_team": 0.8777777777777777
		}]
	},
	"710252": {
		"nik": "710252",
		"nama": "GIYARTO",
		"band": "IV",
		"posisi": "OFF 1 SOLUTION DELIVERY OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272727,
			"nilai_komparatif_unit": 1.0298944360606384,
			"summary_team": 0.9090909090909092
		}]
	},
	"710260": {
		"nik": "710260",
		"nama": "SARIFUDIN",
		"band": "IV",
		"posisi": "OFF 1 TRANSPORT INTEGRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8791666666666659,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0399458361543676,
			"nilai_komparatif_unit": 1.0472336653384822,
			"summary_team": 0.9142857142857141
		}]
	},
	"710289": {
		"nik": "710289",
		"nama": "MOHAMAD DIMYATI",
		"band": "IV",
		"posisi": "OFF 1 INTERNET ROUTER OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9083333333333327,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0030581039755357,
			"nilai_komparatif_unit": 1.010087427878161,
			"summary_team": 0.911111111111111
		}]
	},
	"710309": {
		"nik": "710309",
		"nama": "NAKHDUDIN",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.85528455284553,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0522813688212909,
			"nilai_komparatif_unit": 1.0596556440988905,
			"summary_team": 0.8999999999999999
		}]
	},
	"710318": {
		"nik": "710318",
		"nama": "SUDRAJAT",
		"band": "IV",
		"posisi": "OFF 1 METRO-E PLANNING AREA 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8933333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999999,
			"nilai_komparatif_unit": 1.0070078930370685,
			"summary_team": 0.8933333333333332
		}]
	},
	"710335": {
		"nik": "710335",
		"nama": "AIP SYAIFUDIN",
		"band": "IV",
		"posisi": "OFF 1 GLOBAL & ENHANCED SERVICES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9380952380952379,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0233502538071069,
			"nilai_komparatif_unit": 1.030521782925244,
			"summary_team": 0.96
		}]
	},
	"710337": {
		"nik": "710337",
		"nama": "M. ARIFIN",
		"band": "IV",
		"posisi": "OFF 1 ENTERPRISE SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8592592592592595,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0474137931034477,
			"nilai_komparatif_unit": 1.0547539569310669,
			"summary_team": 0.8999999999999998
		}]
	},
	"710342": {
		"nik": "710342",
		"nama": "ARUM SETIANA",
		"band": "IV",
		"posisi": "OFF 1 BUDGET & GENERAL SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8533333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.03125,
			"nilai_komparatif_unit": 1.038476889694477,
			"summary_team": 0.88
		}]
	},
	"710345": {
		"nik": "710345",
		"nama": "SITI LAELA",
		"band": "IV",
		"posisi": "OFF 1 FACILITY MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.846666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0461192350956126,
			"nilai_komparatif_unit": 1.0534503267991826,
			"summary_team": 0.8857142857142857
		}]
	},
	"710346": {
		"nik": "710346",
		"nama": "SHINTA KANYA PUSPITA",
		"band": "IV",
		"posisi": "OFF 1 SEGMENT ADMINISTRATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365353,
			"nilai_komparatif_unit": 0.9949168891755601,
			"summary_team": 0.8727272727272728
		}]
	},
	"710354": {
		"nik": "710354",
		"nama": "SULASTRI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8553191489361723,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9353233830845746,
			"nilai_komparatif_unit": 0.9418780293083004,
			"summary_team": 0.7999999999999998
		}]
	},
	"710369": {
		"nik": "710369",
		"nama": "HANDI JUNAEDI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8553191489361723,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9353233830845746,
			"nilai_komparatif_unit": 0.9418780293083004,
			"summary_team": 0.7999999999999998
		}]
	},
	"720004": {
		"nik": "720004",
		"nama": "MAINELWATI",
		"band": "IV",
		"posisi": "OFF 1 WHOLESALE SERVICE MGT TR1,2,3",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.048951048951049,
			"nilai_komparatif_unit": 1.0563019857032188,
			"summary_team": 0.9090909090909092
		}]
	},
	"720008": {
		"nik": "720008",
		"nama": "SRI RACHMAWATI",
		"band": "IV",
		"posisi": "OFF 1 PROJECT MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0497427101200685,
			"nilai_komparatif_unit": 1.0570991947490322,
			"summary_team": 0.9272727272727272
		}]
	},
	"720043": {
		"nik": "720043",
		"nama": "SUTARSIH",
		"band": "IV",
		"posisi": "OFF 1 BUDGET CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8758620689655164,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.060179977502813,
			"nilai_komparatif_unit": 1.0676096053851942,
			"summary_team": 0.9285714285714284
		}]
	},
	"720045": {
		"nik": "720045",
		"nama": "SUDI ROSMINI",
		"band": "IV",
		"posisi": "OFF 1 LOGISTIC PLANNING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0695187165775402,
			"nilai_komparatif_unit": 1.0770137893444585,
			"summary_team": 0.9090909090909092
		}]
	},
	"720422": {
		"nik": "720422",
		"nama": "SONNY KOMARA",
		"band": "IV",
		"posisi": "TRIBE INNOVATOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8194444444444434,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.976271186440679,
			"nilai_komparatif_unit": 0.9831127904904272,
			"summary_team": 0.7999999999999998
		}]
	},
	"720508": {
		"nik": "720508",
		"nama": "Hery Nugroho",
		"band": "IV",
		"posisi": "SR OFF GROUND INFRASTRUCTURE PLANNING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333332,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9935064935064937,
			"nilai_komparatif_unit": 1.0004688807446203,
			"summary_team": 0.9272727272727274
		}]
	},
	"720554": {
		"nik": "720554",
		"nama": "Alex Chandra",
		"band": "IV",
		"posisi": "JUNIOR EXPERT NETWORK OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8111111111111113,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9863013698630136,
			"nilai_komparatif_unit": 0.9932132643653278,
			"summary_team": 0.8000000000000002
		}]
	},
	"720592": {
		"nik": "720592",
		"nama": "ADNAN SUDRAJAT",
		"band": "IV",
		"posisi": "OFF 1 KNOWLEDGE MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8407407407407402,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9935216377299824,
			"nilai_komparatif_unit": 1.0004841310972072,
			"summary_team": 0.8352941176470587
		}]
	},
	"730028": {
		"nik": "730028",
		"nama": "Meljanus Alexius Joktetimera ",
		"band": "IV",
		"posisi": "MGR OPERAT REG FIELD AREA AMBON & SORONG",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8200000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.997782705099778,
			"nilai_komparatif_unit": 1.004775059571354,
			"summary_team": 0.8181818181818182
		}]
	},
	"730116": {
		"nik": "730116",
		"nama": "Achib Mahshun",
		"band": "IV",
		"posisi": "MANAGER MISSION EVALUATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.013489736070381,
			"nilai_komparatif_unit": 1.020592163734929,
			"summary_team": 0.8727272727272727
		}]
	},
	"730364": {
		"nik": "730364",
		"nama": "DARSO",
		"band": "IV",
		"posisi": "ENG 1 SYSTEM INTGRTN PLAN&DESIGN ANLYSIS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7537037037037038,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.890838890838891,
			"nilai_komparatif_unit": 0.8970817944991507,
			"summary_team": 0.6714285714285716
		}]
	},
	"730572": {
		"nik": "730572",
		"nama": "RAHMI MULYATI",
		"band": "IV",
		"posisi": "OFF 1 ACCESS QUALITY PERFORM ANALYSIS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777781,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0506329113924044,
			"nilai_komparatif_unit": 1.0579956344566663,
			"summary_team": 0.922222222222222
		}]
	},
	"730573": {
		"nik": "730573",
		"nama": "EKO HARYONO",
		"band": "IV",
		"posisi": "OFF 1 PROBLEM & FAULT MGT OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0161290322580643,
			"nilai_komparatif_unit": 1.0232499558279888,
			"summary_team": 0.84
		}]
	},
	"740073": {
		"nik": "740073",
		"nama": "LASMI IVAWATY",
		"band": "IV",
		"posisi": "OFF 1 API CAPABILITY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8400000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0374149659863943,
			"nilai_komparatif_unit": 1.044685059103081,
			"summary_team": 0.8714285714285712
		}]
	},
	"740214": {
		"nik": "740214",
		"nama": "BUDI WIDODO",
		"band": "IV",
		"posisi": "OFF 1 SDN CLOUD COMPUTING DESIGN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8944444444444445,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.981366459627329,
			"nilai_komparatif_unit": 0.988243770806564,
			"summary_team": 0.8777777777777777
		}]
	},
	"775522": {
		"nik": "775522",
		"nama": "SUNARWAN",
		"band": "IV",
		"posisi": "TRIBE INNOVATOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.749999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8000000000000009,
			"nilai_komparatif_unit": 0.8056063144296558,
			"summary_team": 0.5999999999999999
		}]
	},
	"780014": {
		"nik": "780014",
		"nama": "A.D. STEFEN RATU EDA",
		"band": "IV",
		"posisi": "OFF 1 SLA RECONCILIATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444443,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9769736842105264,
			"nilai_komparatif_unit": 0.9838202112895046,
			"summary_team": 0.825
		}]
	},
	"800103": {
		"nik": "800103",
		"nama": "IRMA HILDAWATY",
		"band": "IV",
		"posisi": "OFF 1 QUALITY MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8250000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9917355371900822,
			"nilai_komparatif_unit": 0.99868551375577,
			"summary_team": 0.8181818181818181
		}]
	},
	"800112": {
		"nik": "800112",
		"nama": "FINKA MEIRISSA ANGGARINI",
		"band": "IV",
		"posisi": "OFF 1 API OPERATION & INTEGRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9083333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0408673894912424,
			"nilai_komparatif_unit": 1.0481616768225697,
			"summary_team": 0.9454545454545454
		}]
	},
	"805820": {
		"nik": "805820",
		"nama": "HARRY PRIHATMOKO",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8949771689497721,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1173469387755097,
			"nilai_komparatif_unit": 1.1251771866077445,
			"summary_team": 1
		}]
	},
	"810047": {
		"nik": "810047",
		"nama": "ELFIDA NURMATASARI",
		"band": "IV",
		"posisi": "OFF 1 PARTNERSHIP INITIATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.980392156862745,
			"nilai_komparatif_unit": 0.9872626402324199,
			"summary_team": 0.8823529411764706
		}]
	},
	"810064": {
		"nik": "810064",
		"nama": "ASTRI WAHYUNI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8553191489361723,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0021321961620444,
			"nilai_komparatif_unit": 1.0091550314017506,
			"summary_team": 0.8571428571428571
		}]
	},
	"810103": {
		"nik": "810103",
		"nama": "MUHAMAD SUHARSO NUGROHO, M.T",
		"band": "IV",
		"posisi": "OFF 1 MANAGED OPERATION HANDLING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.99558693733451,
			"nilai_komparatif_unit": 1.0025639041004528,
			"summary_team": 0.8545454545454546
		}]
	},
	"820010": {
		"nik": "820010",
		"nama": "ELLISA MARTHA RUSDIANA",
		"band": "IV",
		"posisi": "OFF 1 SEGMENT ADMINISTRATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7166666666666665,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0401691331923892,
			"nilai_komparatif_unit": 1.0474585272182617,
			"summary_team": 0.7454545454545454
		}]
	},
	"820052": {
		"nik": "820052",
		"nama": "VIVI YARTI",
		"band": "IV",
		"posisi": "OFF 1 DATACOMM PROD PORTFOLIO & QUAL MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8722222222222225,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9171974522292992,
			"nilai_komparatif_unit": 0.9236250738683939,
			"summary_team": 0.8
		}]
	},
	"830023": {
		"nik": "830023",
		"nama": "EKO NUR RUDIHANTO",
		"band": "IV",
		"posisi": "MGR DIGITAL WORKPLACE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559441,
			"nilai_komparatif_unit": 0.9506717871328969,
			"summary_team": 0.8181818181818183
		}]
	},
	"830098": {
		"nik": "830098",
		"nama": "NUNING YUNI WAHYUNINGTYAS",
		"band": "IV",
		"posisi": "OFF 1 VENDOR ANALYSIS I",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444447,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0030959752321977,
			"nilai_komparatif_unit": 1.0101255645325389,
			"summary_team": 0.8470588235294116
		}]
	},
	"830101": {
		"nik": "830101",
		"nama": "DIANA IMAWATI",
		"band": "IV",
		"posisi": "OFF 1 TRIBE SUPPORT MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.031674208144796,
			"nilai_komparatif_unit": 1.0389040706445771,
			"summary_team": 0.8941176470588235
		}]
	},
	"830128": {
		"nik": "830128",
		"nama": "RISKA TANIA TAMBUNAN",
		"band": "IV",
		"posisi": "TRIBE INNOVATOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8422222222222243,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9498680738786254,
			"nilai_komparatif_unit": 0.956524647739693,
			"summary_team": 0.7999999999999998
		}]
	},
	"840068": {
		"nik": "840068",
		"nama": "YUSTINE PITASARI",
		"band": "IV",
		"posisi": "OFF 1 MARKETING STRATEGY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8800000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0743801652892557,
			"nilai_komparatif_unit": 1.0819093065687508,
			"summary_team": 0.9454545454545454
		}]
	},
	"840095": {
		"nik": "840095",
		"nama": "JAKA NURHAYAT INDRAWAN",
		"band": "IV",
		"posisi": "OFF 1 TRIBE PLANNING & PERFORMANCE II",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8238095238095239,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9884393063583814,
			"nilai_komparatif_unit": 0.9953661832909751,
			"summary_team": 0.8142857142857143
		}]
	},
	"840106": {
		"nik": "840106",
		"nama": "HAJAR ANNISA ISLAM",
		"band": "IV",
		"posisi": "MGR COMMERCIAL MANAGEMENT - 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9486166007905137,
			"nilai_komparatif_unit": 0.9552644044620411,
			"summary_team": 0.7272727272727272
		}]
	},
	"840134": {
		"nik": "840134",
		"nama": "AHMAD IRFAN EFFENDI",
		"band": "IV",
		"posisi": "OFF 1 INTERFACING DATA MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9411764705882352,
			"nilai_komparatif_unit": 0.9477721346231232,
			"summary_team": 0.8
		}]
	},
	"840148": {
		"nik": "840148",
		"nama": "ELIANDRI SHINTANI WULANDARI",
		"band": "IV",
		"posisi": "MGR DEVICE & ENERGY QUALITY ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7537037037037038,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0770342535048416,
			"nilai_komparatif_unit": 1.0845819943506625,
			"summary_team": 0.8117647058823529
		}]
	},
	"840182": {
		"nik": "840182",
		"nama": "R. Haryo Senoadji Pronomochitro",
		"band": "IV",
		"posisi": "MANAGER SPACECRAFT ANALYST",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0451612903225804,
			"nilai_komparatif_unit": 1.0524856688516455,
			"summary_team": 0.9000000000000001
		}]
	},
	"840196": {
		"nik": "840196",
		"nama": "I GEDE BENDESA ABHIYANA",
		"band": "IV",
		"posisi": "OFF 1 O & M METRO OPERATION 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888884,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0250000000000004,
			"nilai_komparatif_unit": 1.0321830903629956,
			"summary_team": 0.911111111111111
		}]
	},
	"845702": {
		"nik": "845702",
		"nama": "GELENT PUTRI SYAH",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8433333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7114624505928853,
			"nilai_komparatif_unit": 0.7164483033465309,
			"summary_team": 0.5999999999999999
		}]
	},
	"845765": {
		"nik": "845765",
		"nama": "LUTPI JANUAR",
		"band": "IV",
		"posisi": "TRIBE INNOVATOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7636363636363636,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8935574229691876,
			"nilai_komparatif_unit": 0.8998193778118343,
			"summary_team": 0.6823529411764705
		}]
	},
	"850050": {
		"nik": "850050",
		"nama": "ERYK LESMONO",
		"band": "IV",
		"posisi": "MGR TGROUP&OLO FIBER ACC PROJ CTRL&INTE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8231884057971008,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9718309859154936,
			"nilai_komparatif_unit": 0.9786414735148982,
			"summary_team": 0.7999999999999999
		}]
	},
	"850068": {
		"nik": "850068",
		"nama": "ARGIYANTO SUGIMAN",
		"band": "IV",
		"posisi": "OFF 1 VENDOR ANALYSIS II",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8791666666666665,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.958700067704807,
			"nilai_komparatif_unit": 0.9654185352339126,
			"summary_team": 0.8428571428571426
		}]
	},
	"850079": {
		"nik": "850079",
		"nama": "DIAN PURIYANTI WARDHANI",
		"band": "IV",
		"posisi": "OFF 1 SEGMENT ADMINISTRATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 1,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1,
			"nilai_komparatif_unit": 1.0070078930370685,
			"summary_team": 1
		}]
	},
	"850106": {
		"nik": "850106",
		"nama": "RIZKIANA AMALIA",
		"band": "IV",
		"posisi": "MGR IP TRANSIT & OLO OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888884,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0125000000000004,
			"nilai_komparatif_unit": 1.0195954917000323,
			"summary_team": 0.8999999999999999
		}]
	},
	"850123": {
		"nik": "850123",
		"nama": "ANJUNITA NAINGGOLAN",
		"band": "IV",
		"posisi": "OFF 1 SEGMENT ADMINISTRATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333338,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0591350397175634,
			"nilai_komparatif_unit": 1.0665573447877155,
			"summary_team": 0.9090909090909092
		}]
	},
	"850138": {
		"nik": "850138",
		"nama": "HAJAR NURI FIBRIYANTI",
		"band": "IV",
		"posisi": "OFF 1 SERVICE PERFORMANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8158730158730151,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0505836575875496,
			"nilai_komparatif_unit": 1.0579460354864154,
			"summary_team": 0.857142857142857
		}]
	},
	"850139": {
		"nik": "850139",
		"nama": "Maria Uliarta Pasaribu, M.M",
		"band": "IV",
		"posisi": "SENIOR ACCOUNT MANAGER DOMESTIC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0101010101010102,
			"nilai_komparatif_unit": 1.017179689936433,
			"summary_team": 0.9090909090909092
		}]
	},
	"850164": {
		"nik": "850164",
		"nama": "Noveisza Insyiah",
		"band": "IV",
		"posisi": "MANAGER FINANCE AND SUPPORT MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7888888888888892,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9126760563380281,
			"nilai_komparatif_unit": 0.9190719925183385,
			"summary_team": 0.7200000000000002
		}]
	},
	"850174": {
		"nik": "850174",
		"nama": "FISRI TARIKA",
		"band": "IV",
		"posisi": "OFF 1 DATA QUALITY MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444444,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0657894736842104,
			"nilai_komparatif_unit": 1.0732584123158229,
			"summary_team": 0.8999999999999999
		}]
	},
	"850175": {
		"nik": "850175",
		"nama": "RIMA DIMIATI",
		"band": "IV",
		"posisi": "OFF 1 SEGMENT ADMINISTRATION SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197861,
			"nilai_komparatif_unit": 0.9693124104100125,
			"summary_team": 0.8181818181818181
		}]
	},
	"860047": {
		"nik": "860047",
		"nama": "LAILY SAVINA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333338,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0591350397175634,
			"nilai_komparatif_unit": 1.0665573447877155,
			"summary_team": 0.9090909090909092
		}]
	},
	"860048": {
		"nik": "860048",
		"nama": "IRMA HAYATI",
		"band": "IV",
		"posisi": "OFF 1 SALES - 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8111111111111103,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0008058017727648,
			"nilai_komparatif_unit": 1.007819341782466,
			"summary_team": 0.8117647058823529
		}]
	},
	"860070": {
		"nik": "860070",
		"nama": "NURKHOLIS MADJID",
		"band": "IV",
		"posisi": "OFF 1 INCIDENT HANDLING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8313725490196076,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9783018867924531,
			"nilai_komparatif_unit": 0.9851577217730569,
			"summary_team": 0.8133333333333332
		}]
	},
	"860087": {
		"nik": "860087",
		"nama": "REVI FAJAR MARTA",
		"band": "IV",
		"posisi": "SENIOR PRODUCT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8527777777777763,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9519064954972231,
			"nilai_komparatif_unit": 0.9585773543989583,
			"summary_team": 0.8117647058823527
		}]
	},
	"860102": {
		"nik": "860102",
		"nama": "ADI KUSWANTO",
		"band": "IV",
		"posisi": "OFF 1 DESIGN INTEGRASI ACCESS NODE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555551,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9870129870129876,
			"nilai_komparatif_unit": 0.9939298684521721,
			"summary_team": 0.8444444444444444
		}]
	},
	"860108": {
		"nik": "860108",
		"nama": "NURINA FEBRYANTI, M.T",
		"band": "IV",
		"posisi": "OFF 1 LOGISTIC PLANNING & PROGRAM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8799999999999993,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0389610389610395,
			"nilai_komparatif_unit": 1.0462419667917602,
			"summary_team": 0.9142857142857141
		}]
	},
	"860132": {
		"nik": "860132",
		"nama": "ROSMA FEBRI DIANDARI",
		"band": "IV",
		"posisi": "OFF 1 INCUBATION PROGRAM&BUDGET CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9890109890109884,
			"nilai_komparatif_unit": 0.9959418722344627,
			"summary_team": 0.857142857142857
		}]
	},
	"860141": {
		"nik": "860141",
		"nama": "RANI GUSTIANI",
		"band": "IV",
		"posisi": "OFF 1 INTERNAL & EXTERNAL COMMUNICATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7972222222222217,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.151055544168888,
			"nilai_komparatif_unit": 1.1591220183021482,
			"summary_team": 0.9176470588235295
		}]
	},
	"860148": {
		"nik": "860148",
		"nama": "AZMAN NURGOZALI",
		"band": "IV",
		"posisi": "OFF 1 IT BUSINESS SERVICE TESTING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8400000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0158730158730154,
			"nilai_komparatif_unit": 1.0229921453074977,
			"summary_team": 0.8533333333333332
		}]
	},
	"860165": {
		"nik": "860165",
		"nama": "BEPPIELHO HANANTO SIDHARTA",
		"band": "IV",
		"posisi": "OFF 1 BUSINESS MODEL & CUST ENGAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9773755656108591,
			"nilai_komparatif_unit": 0.9842249090317043,
			"summary_team": 0.8470588235294116
		}]
	},
	"865615": {
		"nik": "865615",
		"nama": "DESQI KUNFIYYATI CAHYANINGRUM",
		"band": "IV",
		"posisi": "OFF 1 CX CONSUMER DIGITIZATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7944444444444441,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8589058000822709,
			"nilai_komparatif_unit": 0.8649249200581652,
			"summary_team": 0.6823529411764705
		}]
	},
	"865619": {
		"nik": "865619",
		"nama": "MUHAMAD FAISAL BAEHAKI",
		"band": "IV",
		"posisi": "OFF 1 CX ENTERPRISE DIGITIZATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8176100628930801,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7338461538461551,
			"nilai_komparatif_unit": 0.7389888691979731,
			"summary_team": 0.5999999999999999
		}]
	},
	"870027": {
		"nik": "870027",
		"nama": "AZHAR HARRIS",
		"band": "IV",
		"posisi": "OFF 1 ACCESS IT TOOL MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8484848484848488,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9821428571428567,
			"nilai_komparatif_unit": 0.9890256092328347,
			"summary_team": 0.8333333333333333
		}]
	},
	"870045": {
		"nik": "870045",
		"nama": "NOMARHINTA SOLIHAH",
		"band": "IV",
		"posisi": "ENG 1 TRANSMISSION QUALITY ASSURANCE",
		"category": [{
			"code": "DI-2-01",
			"kriteria": 0.766666666666666,
			"kriteria_bp": 0.9898473495304185,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9820971867007681,
			"nilai_komparatif_unit": 0.9921703454241433,
			"summary_team": 0.7529411764705882
		}]
	},
	"870052": {
		"nik": "870052",
		"nama": "VANI RAKHMAWATI",
		"band": "IV",
		"posisi": "TRIBE INNOVATOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.872222222222221,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0251030348445125,
			"nilai_komparatif_unit": 1.0322868472646771,
			"summary_team": 0.8941176470588235
		}]
	},
	"870053": {
		"nik": "870053",
		"nama": "ALDI AFRIANSYAH",
		"band": "IV",
		"posisi": "SENIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8527777777777763,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9794979881203312,
			"nilai_komparatif_unit": 0.9863622052511023,
			"summary_team": 0.8352941176470587
		}]
	},
	"880015": {
		"nik": "880015",
		"nama": "ASTRIA SYAM",
		"band": "IV",
		"posisi": "OFF 1 TRANSACTION VALIDATION 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365351,
			"nilai_komparatif_unit": 0.9949168891755599,
			"summary_team": 0.8727272727272727
		}]
	},
	"880024": {
		"nik": "880024",
		"nama": "NIKI ADYTIA PUTRA",
		"band": "IV",
		"posisi": "TRIBE INNOVATOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.749999999999999,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0666666666666678,
			"nilai_komparatif_unit": 1.0741417525728743,
			"summary_team": 0.7999999999999998
		}]
	},
	"880026": {
		"nik": "880026",
		"nama": "HANUNG TYAS SAKSONO",
		"band": "IV",
		"posisi": "MGR SERVICE ACTIVATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888884,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625000000000005,
			"nilai_komparatif_unit": 0.969245097048179,
			"summary_team": 0.8555555555555555
		}]
	},
	"880037": {
		"nik": "880037",
		"nama": "NAILA FITHRIA",
		"band": "IV",
		"posisi": "OFF 1 PARTNERSHIP SYSTEM DEVELOPMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8222222222222224,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9729729729729726,
			"nilai_komparatif_unit": 0.9797914634955257,
			"summary_team": 0.7999999999999998
		}]
	},
	"880040": {
		"nik": "880040",
		"nama": "ANGGI ANGGRAENI",
		"band": "IV",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333338,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9320388349514558,
			"nilai_komparatif_unit": 0.9385704634131895,
			"summary_team": 0.8
		}]
	},
	"880043": {
		"nik": "880043",
		"nama": "DESITA MUSTIKANINGRUM",
		"band": "IV",
		"posisi": "OFF 1 RADIO IP OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9074074074074071,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.016326530612245,
			"nilai_komparatif_unit": 1.0234488382295106,
			"summary_team": 0.9222222222222222
		}]
	},
	"880045": {
		"nik": "880045",
		"nama": "MARCHO SENDA DJISOKO",
		"band": "IV",
		"posisi": "SENIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8422222222222243,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9498680738786255,
			"nilai_komparatif_unit": 0.9565246477396931,
			"summary_team": 0.7999999999999999
		}]
	},
	"880047": {
		"nik": "880047",
		"nama": "TEGUH PRIBADI MANULLANG",
		"band": "IV",
		"posisi": "MGR BIDDING & OFFERING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9066666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0084033613445376,
			"nilai_komparatif_unit": 1.0154701442390603,
			"summary_team": 0.9142857142857141
		}]
	},
	"880054": {
		"nik": "880054",
		"nama": "BUDI HARI SULAKSONO",
		"band": "IV",
		"posisi": "TRIBE INNOVATOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8422222222222243,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9498680738786254,
			"nilai_komparatif_unit": 0.956524647739693,
			"summary_team": 0.7999999999999998
		}]
	},
	"880058": {
		"nik": "880058",
		"nama": "Mulya Erik Hidayatulloh",
		"band": "IV",
		"posisi": "MANAGER SATELLITE EXECUTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8740740740740733,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0133171912832941,
			"nilai_komparatif_unit": 1.02041840977243,
			"summary_team": 0.8857142857142859
		}]
	},
	"880059": {
		"nik": "880059",
		"nama": "PRAMESTI PUJI LESTARI",
		"band": "IV",
		"posisi": "SENIOR TRIBE INNOVATOR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.986456711546516,
			"summary_team": 0.8
		}]
	},
	"880060": {
		"nik": "880060",
		"nama": "ASTRI OCTAVINA HAMID",
		"band": "IV",
		"posisi": "OFF 1 ORDER MANAGEMENT 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139859,
			"nilai_komparatif_unit": 0.9929238665610255,
			"summary_team": 0.8545454545454546
		}]
	},
	"880062": {
		"nik": "880062",
		"nama": "GALUH BENINGKINASIH WOROSUNTI",
		"band": "IV",
		"posisi": "MGR CUSTOMER ANALYSIS & SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.985571587125416,
			"nilai_komparatif_unit": 0.9924783673883648,
			"summary_team": 0.8705882352941177
		}]
	},
	"880063": {
		"nik": "880063",
		"nama": "CORRY THERESIA SILITONGA",
		"band": "IV",
		"posisi": "OFF 1 SALES TAM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8111111111111112,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9863013698630134,
			"nilai_komparatif_unit": 0.9932132643653275,
			"summary_team": 0.7999999999999999
		}]
	},
	"880069": {
		"nik": "880069",
		"nama": "ROLAND DANIEL PARULIAN SIDABUTAR",
		"band": "IV",
		"posisi": "MGR DATACOMM PRODUCT DEVELOPMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0161290322580643,
			"nilai_komparatif_unit": 1.0232499558279888,
			"summary_team": 0.84
		}]
	},
	"880070": {
		"nik": "880070",
		"nama": "GHULAM TAFRIHI",
		"band": "IV",
		"posisi": "OFF 1 WIRELESS PROD PORTFOLIO & QUAL MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0259740259740255,
			"nilai_komparatif_unit": 1.0331639422068621,
			"summary_team": 0.8777777777777777
		}]
	},
	"880078": {
		"nik": "880078",
		"nama": "FARAINY ADINDA GITAWARDHANI",
		"band": "IV",
		"posisi": "OFF 1 REWARD MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888892,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9160714285714281,
			"nilai_komparatif_unit": 0.9224911591571712,
			"summary_team": 0.8142857142857142
		}]
	},
	"880092": {
		"nik": "880092",
		"nama": "RONA NANDANA UTDITYASAN",
		"band": "IV",
		"posisi": "OFF 1 SUBSIDIARIES EVALUATION  & SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8571428571428567,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.954545454545455,
			"nilai_komparatif_unit": 0.9612348069899296,
			"summary_team": 0.8181818181818181
		}]
	},
	"880097": {
		"nik": "880097",
		"nama": "ANDRIAN DWI ANTORO",
		"band": "IV",
		"posisi": "OFF 1 CORE NETWORK PROGRAM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.866666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0045248868778287,
			"nilai_komparatif_unit": 1.011564489838142,
			"summary_team": 0.8705882352941177
		}]
	},
	"885940": {
		"nik": "885940",
		"nama": "DITHA NATASIA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8156249999999994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9633278598795846,
			"nilai_komparatif_unit": 0.9700787584812488,
			"summary_team": 0.7857142857142856
		}]
	},
	"890004": {
		"nik": "890004",
		"nama": "ALDY PRADANA",
		"band": "IV",
		"posisi": "OFF 1 ENTERPRISE TRANSFORMATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8499999999999998,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8983957219251338,
			"nilai_komparatif_unit": 0.9046915830493452,
			"summary_team": 0.7636363636363636
		}]
	},
	"890024": {
		"nik": "890024",
		"nama": "VIVI ANGGRAENI",
		"band": "IV",
		"posisi": "OFF 1 ORDER MANAGEMENT 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8857142857142856,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9838709677419354,
			"nilai_komparatif_unit": 0.990765830246148,
			"summary_team": 0.8714285714285712
		}]
	},
	"890026": {
		"nik": "890026",
		"nama": "Angga Risnando",
		"band": "IV",
		"posisi": "SENIOR OFFICER CORPORATE STRATEGY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0123929619837908,
			"summary_team": 0.8545454545454546
		}]
	},
	"890027": {
		"nik": "890027",
		"nama": "RIRIS YUNITA",
		"band": "IV",
		"posisi": "OFF 1 GENERAL AFFAIR",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666662,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9677419354838713,
			"nilai_komparatif_unit": 0.974523767455228,
			"summary_team": 0.7999999999999998
		}]
	},
	"890030": {
		"nik": "890030",
		"nama": "GEDE DOKO HARIKUSUMA",
		"band": "IV",
		"posisi": "MGR CHPTR DATA SCNTST PLANNG,PRFRM&SUPPT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7972222222222217,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7673703627792584,
			"nilai_komparatif_unit": 0.772748012201432,
			"summary_team": 0.6117647058823529
		}]
	},
	"890036": {
		"nik": "890036",
		"nama": "ADITHYA OCTARYAN",
		"band": "IV",
		"posisi": "OFF 1 PORTFOLIO GOVERNANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8434782608695645,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9624014554275327,
			"nilai_komparatif_unit": 0.9691458618858879,
			"summary_team": 0.8117647058823529
		}]
	},
	"890037": {
		"nik": "890037",
		"nama": "YERIS PERMATA OCTARINA",
		"band": "IV",
		"posisi": "OFF 1 PERFORMANCE ANALYSIS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8499999999999989,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0380622837370255,
			"nilai_komparatif_unit": 1.0453369131872696,
			"summary_team": 0.8823529411764706
		}]
	},
	"890042": {
		"nik": "890042",
		"nama": "Lolo Ardy Boangmanalu",
		"band": "IV",
		"posisi": "MANAGER MISSION PLANNING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8611111111111114,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9290322580645158,
			"nilai_komparatif_unit": 0.9355428167570182,
			"summary_team": 0.8
		}]
	},
	"890044": {
		"nik": "890044",
		"nama": "YUDI ISKANDAR",
		"band": "IV",
		"posisi": "OFF 1 PROJECT MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.939560439560439,
			"nilai_komparatif_unit": 0.9461447786227396,
			"summary_team": 0.8142857142857142
		}]
	},
	"890045": {
		"nik": "890045",
		"nama": "AGUS BUDIYANTO",
		"band": "IV",
		"posisi": "OFF 1 WIFI EBIS PROJECT INTEGRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8363636363636366,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9130434782608693,
			"nilai_komparatif_unit": 0.9194419892947144,
			"summary_team": 0.7636363636363637
		}]
	},
	"890047": {
		"nik": "890047",
		"nama": "HERI SETIAWAN",
		"band": "IV",
		"posisi": "OFF 1 ORDER MANAGEMENT 3",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8439108061749571,
			"nilai_komparatif_unit": 0.8498248428374574,
			"summary_team": 0.7454545454545455
		}]
	},
	"890050": {
		"nik": "890050",
		"nama": "ARFAN SENO AJI",
		"band": "IV",
		"posisi": "OFF 1 PERFORMANCE & ORDER ALIGNMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9055555555555558,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0239821528165083,
			"nilai_komparatif_unit": 1.0311581102153136,
			"summary_team": 0.9272727272727272
		}]
	},
	"890053": {
		"nik": "890053",
		"nama": "DHIKA DWIPUTRA",
		"band": "IV",
		"posisi": "OFF 1 EUC TECHNICAL OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9473684210526311,
			"nilai_komparatif_unit": 0.9540074776140645,
			"summary_team": 0.7999999999999999
		}]
	},
	"890055": {
		"nik": "890055",
		"nama": "IBRAHIM NUROKTI AL IRSYAD",
		"band": "IV",
		"posisi": "OFF 1 PORTFOLIO DATA ANALYSIS",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8434782608695645,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0460885385081877,
			"nilai_komparatif_unit": 1.0534194150933565,
			"summary_team": 0.8823529411764706
		}]
	},
	"890058": {
		"nik": "890058",
		"nama": "TAUFIQ SYOBRI",
		"band": "IV",
		"posisi": "OFF 1 RADIO ACCESS PLANNING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.871264367816091,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9182058047493413,
			"nilai_komparatif_unit": 0.9246404928150401,
			"summary_team": 0.7999999999999999
		}]
	},
	"890060": {
		"nik": "890060",
		"nama": "MUHAMMAD TAUFIQ ASMARA",
		"band": "IV",
		"posisi": "OFF 1 WORKFORCE MANAGEMENT SYSTEM ADM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8746031746031734,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0417422867513624,
			"nilai_komparatif_unit": 1.0490427052691071,
			"summary_team": 0.911111111111111
		}]
	},
	"890067": {
		"nik": "890067",
		"nama": "ARFIYAH CITRA EKA DEWI",
		"band": "IV",
		"posisi": "OFF 1 PROCUREMENT PROCESS II",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8791666666666665,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9634792305547811,
			"nilai_komparatif_unit": 0.9702311899459461,
			"summary_team": 0.8470588235294116
		}]
	},
	"890068": {
		"nik": "890068",
		"nama": "MUHAMMAD PRIZA ANGGARA DWI PUTRA",
		"band": "IV",
		"posisi": "OFF 1 SOLUTION DEPLOYMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0140499062750898,
			"summary_team": 0.8727272727272727
		}]
	},
	"890084": {
		"nik": "890084",
		"nama": "ACHMAD HADI",
		"band": "IV",
		"posisi": "OFF 1 DIGITAL LIFESTYLE ALIGNMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9850230414746541,
			"nilai_komparatif_unit": 0.9919259775883564,
			"summary_team": 0.8142857142857142
		}]
	},
	"890087": {
		"nik": "890087",
		"nama": "HAYUDYA WITASARI",
		"band": "IV",
		"posisi": "OFF 1 BUSINESS ECOSYSTEM PLANNING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9900687547746383,
			"nilai_komparatif_unit": 0.9970070507074426,
			"summary_team": 0.8470588235294119
		}]
	},
	"890094": {
		"nik": "890094",
		"nama": "GRINANO ARDHI WIBOWO",
		"band": "IV",
		"posisi": "OFF 1 QUALITY & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9695251594613711,
			"nilai_komparatif_unit": 0.9763194880756232,
			"summary_team": 0.8
		}]
	},
	"890100": {
		"nik": "890100",
		"nama": "BAYU ADHI PRABOWO",
		"band": "IV",
		"posisi": "OFF 1 EXECUTIVE SUPPORT SYSTEM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666662,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9819734345351049,
			"nilai_komparatif_unit": 0.9888549993295697,
			"summary_team": 0.8117647058823529
		}]
	},
	"900009": {
		"nik": "900009",
		"nama": "FITRIANA PASSA",
		"band": "IV",
		"posisi": "OFF 1 BUSINESS PRIDECTIVE ANALYTIC",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9520661157024793,
			"nilai_komparatif_unit": 0.9587380932055396,
			"summary_team": 0.8727272727272727
		}]
	},
	"900011": {
		"nik": "900011",
		"nama": "PUTRIANA SARI",
		"band": "IV",
		"posisi": "OFF 1 OFFERING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8750000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9974025974025972,
			"nilai_komparatif_unit": 1.0043922881200888,
			"summary_team": 0.8727272727272727
		}]
	},
	"900016": {
		"nik": "900016",
		"nama": "MUKHAMMAD IFANTO",
		"band": "IV",
		"posisi": "OFF 1 CUSTOMER DATA ACQUISITION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9741176470588233,
			"nilai_komparatif_unit": 0.9809441593349324,
			"summary_team": 0.8117647058823529
		}]
	},
	"900024": {
		"nik": "900024",
		"nama": "YURY ASMARALDA",
		"band": "IV",
		"posisi": "OFF 1 MARKET & PORFOLIO RESEARCH",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7849462365591385,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9496886674968882,
			"nilai_komparatif_unit": 0.9563439840972225,
			"summary_team": 0.7454545454545455
		}]
	},
	"900025": {
		"nik": "900025",
		"nama": "DWI ASTO YULIARDI",
		"band": "IV",
		"posisi": "OFF 1 ECOSYSTEM ANALYST",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8362318840579697,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.021900110288327,
			"nilai_komparatif_unit": 1.029061476955796,
			"summary_team": 0.8545454545454546
		}]
	},
	"900026": {
		"nik": "900026",
		"nama": "LIRAINI TARIGAN",
		"band": "IV",
		"posisi": "OFF 1 SUBSIDIARIES COLLECTION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8571428571428567,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1030303030303037,
			"nilai_komparatif_unit": 1.1107602214105854,
			"summary_team": 0.9454545454545454
		}]
	},
	"900046": {
		"nik": "900046",
		"nama": "STEVANY PRIESCILA PALILU",
		"band": "IV",
		"posisi": "OFF 1 WIRELESS & OLO PROJECT REPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8266666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.967741935483871,
			"nilai_komparatif_unit": 0.9745237674552276,
			"summary_team": 0.8
		}]
	},
	"900050": {
		"nik": "900050",
		"nama": "ABDUL MUTALIB",
		"band": "IV",
		"posisi": "OFF 1 OFFERING 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0710743801652893,
			"nilai_komparatif_unit": 1.078580354856232,
			"summary_team": 0.9818181818181818
		}]
	},
	"900054": {
		"nik": "900054",
		"nama": "FURI DIAH AYU HAPSARI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8394557823129278,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8880212170325594,
			"nilai_komparatif_unit": 0.894244374736171,
			"summary_team": 0.7454545454545455
		}]
	},
	"900059": {
		"nik": "900059",
		"nama": "ADRIENA AMORETTA",
		"band": "IV",
		"posisi": "OFF 1 INDIGO STARTUP DISCOVERY MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9773755656108595,
			"nilai_komparatif_unit": 0.9842249090317047,
			"summary_team": 0.8470588235294116
		}]
	},
	"900072": {
		"nik": "900072",
		"nama": "FERIALDI RASONI PRADHANA",
		"band": "IV",
		"posisi": "OFF 1 ACCOUNT TEAM COMPETENCY DEVT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9037037037037039,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0260804769001488,
			"nilai_komparatif_unit": 1.0332711391296894,
			"summary_team": 0.9272727272727272
		}]
	},
	"900076": {
		"nik": "900076",
		"nama": "YUNIA MANDALA SARI",
		"band": "IV",
		"posisi": "OFF 1 CHANNEL OPTIMIZATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.880392156862744,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9898186446070644,
			"nilai_komparatif_unit": 0.9967551877945668,
			"summary_team": 0.8714285714285712
		}]
	},
	"900079": {
		"nik": "900079",
		"nama": "PRIMA ARTI HASTUTI",
		"band": "IV",
		"posisi": "OFF 1 INVENTORY OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9714285714285711,
			"nilai_komparatif_unit": 0.9782362389502948,
			"summary_team": 0.9066666666666666
		}]
	},
	"900085": {
		"nik": "900085",
		"nama": "GHAZALI AL NAFI",
		"band": "IV",
		"posisi": "OFF 1 OFFERING 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.031404958677686,
			"nilai_komparatif_unit": 1.0386329343060012,
			"summary_team": 0.9454545454545454
		}]
	},
	"900093": {
		"nik": "900093",
		"nama": "NUGROHO WANDI",
		"band": "IV",
		"posisi": "OFF 1 ORDER MANAGEMENT 3",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365351,
			"nilai_komparatif_unit": 0.9949168891755599,
			"summary_team": 0.8727272727272727
		}]
	},
	"900098": {
		"nik": "900098",
		"nama": "AHMAD RANGGABUWANA PRAKOSA",
		"band": "IV",
		"posisi": "OFF 1 PERFORM & QUALITY MGT SYSTEM ADM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0641025641025634,
			"nilai_komparatif_unit": 1.0715596810522645,
			"summary_team": 0.922222222222222
		}]
	},
	"900106": {
		"nik": "900106",
		"nama": "HENDRA SUTRISNO NAPITUPULU",
		"band": "IV",
		"posisi": "OFF 1 INFRA & SERVICE SLA CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9296296296296293,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0159362549800801,
			"nilai_komparatif_unit": 1.0230558275874604,
			"summary_team": 0.9444444444444444
		}]
	},
	"900107": {
		"nik": "900107",
		"nama": "PUSPITA SULISTYANINGRUM",
		"band": "IV",
		"posisi": "OFF 1 TRANSPORT PROGRAM",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8242424242424247,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9705882352941171,
			"nilai_komparatif_unit": 0.9773900138300954,
			"summary_team": 0.7999999999999999
		}]
	},
	"906369": {
		"nik": "906369",
		"nama": "NIRMALA TWINTA VIDYARANI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8878048780487822,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.997645211930924,
			"nilai_komparatif_unit": 1.0046366028650795,
			"summary_team": 0.8857142857142855
		}]
	},
	"906409": {
		"nik": "906409",
		"nama": "FITRIA DARWIN NURHANIFA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.85528455284553,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.020394054614585,
			"nilai_komparatif_unit": 1.0275448670049847,
			"summary_team": 0.8727272727272727
		}]
	},
	"910001": {
		"nik": "910001",
		"nama": "MUHAMMAD RICKY FEBRIAN",
		"band": "IV",
		"posisi": "OFF 1 SYNERGY INTEG&IMPLEMT CROSS CFU/FU",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8121212121212125,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9987562189054722,
			"nilai_komparatif_unit": 1.0057553956576688,
			"summary_team": 0.8111111111111111
		}]
	},
	"910010": {
		"nik": "910010",
		"nama": "SITI NURJAMILLAH",
		"band": "IV",
		"posisi": "OFF 1 IH3P INTEGRATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8791666666666659,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0110584518167465,
			"nilai_komparatif_unit": 1.0181438413013024,
			"summary_team": 0.8888888888888888
		}]
	},
	"910016": {
		"nik": "910016",
		"nama": "ALFI NURHAFID",
		"band": "IV",
		"posisi": "PRODUCT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666665,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0316742081447965,
			"nilai_komparatif_unit": 1.0389040706445776,
			"summary_team": 0.8941176470588235
		}]
	},
	"910020": {
		"nik": "910020",
		"nama": "GRAYNIE SYAHNI FRATAMI",
		"band": "IV",
		"posisi": "OFF 1 SERVICE OPERATION PROCUREMENT  -1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8222222222222214,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.972972972972974,
			"nilai_komparatif_unit": 0.9797914634955271,
			"summary_team": 0.8
		}]
	},
	"910025": {
		"nik": "910025",
		"nama": "MUHAMMAD MAHRUS SYAMSURRIJAL",
		"band": "IV",
		"posisi": "OFF 1 TRANSPORT SYNERGY PLANNING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8745098039215675,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9757847533632298,
			"nilai_komparatif_unit": 0.9826229485420016,
			"summary_team": 0.8533333333333332
		}]
	},
	"910028": {
		"nik": "910028",
		"nama": "KADEK ANGGRIAN MAHENDRAPUTRA",
		"band": "IV",
		"posisi": "OFF 1 DATABASE MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0196078431372548,
			"nilai_komparatif_unit": 1.026753145841717,
			"summary_team": 0.9176470588235295
		}]
	},
	"910031": {
		"nik": "910031",
		"nama": "RR. CERIA GANINDA HAPSARI",
		"band": "IV",
		"posisi": "OFF 1 HCM SYSTEM OPERATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8000000000000005,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999993,
			"nilai_komparatif_unit": 1.0070078930370678,
			"summary_team": 0.7999999999999999
		}]
	},
	"910070": {
		"nik": "910070",
		"nama": "MUHAMMAD ABDUL JA'FAR SIDIQ",
		"band": "IV",
		"posisi": "OFF 1 CEM DESIGN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9200000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9710144927536228,
			"nilai_komparatif_unit": 0.9778192584562836,
			"summary_team": 0.8933333333333332
		}]
	},
	"910071": {
		"nik": "910071",
		"nama": "ANINDITA PRITA DWIPUTRI",
		"band": "IV",
		"posisi": "PRODUCT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0450725744843403,
			"nilai_komparatif_unit": 1.0523963313023004,
			"summary_team": 0.8941176470588235
		}]
	},
	"910072": {
		"nik": "910072",
		"nama": "IMAM INDRADI RAMADHAN",
		"band": "IV",
		"posisi": "OFF 1 PERFORMANCE & ORDER ALIGNMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0505050505050504,
			"nilai_komparatif_unit": 1.05786687753389,
			"summary_team": 0.9454545454545454
		}]
	},
	"910073": {
		"nik": "910073",
		"nama": "LISA RAMADHANI KEMBAREN",
		"band": "IV",
		"posisi": "OFF 1 PAYMENT SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9007506255212674,
			"nilai_komparatif_unit": 0.907062989557993,
			"summary_team": 0.8181818181818181
		}]
	},
	"910081": {
		"nik": "910081",
		"nama": "ANNA YULIA SARI",
		"band": "IV",
		"posisi": "OFF 1 OLO SERVICE QUALITY ENHANCEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9753086419753083,
			"nilai_komparatif_unit": 0.9821435006163999,
			"summary_team": 0.8777777777777777
		}]
	},
	"910095": {
		"nik": "910095",
		"nama": "AKHIRUL AKBAR",
		"band": "IV",
		"posisi": "ENG 1 DEVICE & ENERGY QUALITY ASSURANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.7537037037037038,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0302066772655007,
			"nilai_komparatif_unit": 1.037426255465851,
			"summary_team": 0.7764705882352941
		}]
	},
	"910108": {
		"nik": "910108",
		"nama": "ALFA PAHLAWAN PUTRA",
		"band": "IV",
		"posisi": "OFF 1 ORDER MANAGEMENT 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8857142857142856,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0161290322580645,
			"nilai_komparatif_unit": 1.023249955827989,
			"summary_team": 0.8999999999999998
		}]
	},
	"910131": {
		"nik": "910131",
		"nama": "PANALING GOTRA PRAMANA",
		"band": "IV",
		"posisi": "OFF 1 OFFERING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0123929619837908,
			"summary_team": 0.8545454545454545
		}]
	},
	"910132": {
		"nik": "910132",
		"nama": "DESINTA RIANI PRAMUDITA",
		"band": "IV",
		"posisi": "OFF 1 SEGMENT PERFORMANCE SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139859,
			"nilai_komparatif_unit": 0.9929238665610255,
			"summary_team": 0.8545454545454546
		}]
	},
	"910162": {
		"nik": "910162",
		"nama": "AKHMADANNA PRADIPTA PUTRA",
		"band": "IV",
		"posisi": "OFF 1 OBL SUPPORT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9599999999999997,
			"nilai_komparatif_unit": 0.9667275773155856,
			"summary_team": 0.8
		}]
	},
	"910190": {
		"nik": "910190",
		"nama": "LUTHFIA AYU INDRIANI",
		"band": "IV",
		"posisi": "OFF 1 DATACOMM PRODUCT MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8722222222222225,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.146496815286624,
			"nilai_komparatif_unit": 1.1545313423354924,
			"summary_team": 1
		}]
	},
	"910195": {
		"nik": "910195",
		"nama": "ADITHYA WIRYAWAN IRSYAD",
		"band": "IV",
		"posisi": "OFF 1 LEGAL SETTLEMENT & COMPLIANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8750000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0389610389610386,
			"nilai_komparatif_unit": 1.0462419667917593,
			"summary_team": 0.9090909090909092
		}]
	},
	"916256": {
		"nik": "916256",
		"nama": "YODA DIPUTRA",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8617283950617275,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0760614743422778,
			"nilai_komparatif_unit": 1.0836023980557787,
			"summary_team": 0.9272727272727272
		}]
	},
	"920003": {
		"nik": "920003",
		"nama": "RESTY FAUZIAH",
		"band": "IV",
		"posisi": "OFF 1 TRANSACTION VALIDATION 1",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.789473684210526,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9442424242424247,
			"nilai_komparatif_unit": 0.9508595741525778,
			"summary_team": 0.7454545454545455
		}]
	},
	"920006": {
		"nik": "920006",
		"nama": "I PUTU ARIYASA",
		"band": "IV",
		"posisi": "OFF 1 API SERVICE CREATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8400000000000001,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0034013605442174,
			"nilai_komparatif_unit": 1.0104330899521603,
			"summary_team": 0.8428571428571427
		}]
	},
	"920011": {
		"nik": "920011",
		"nama": "I GUSTI AGUNG AYU MADE DIASTIKA INKASARI",
		"band": "IV",
		"posisi": "OFF 1 TRIBE SYSTEM DEVELOPMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666664,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9773755656108599,
			"nilai_komparatif_unit": 0.9842249090317051,
			"summary_team": 0.8470588235294116
		}]
	},
	"920051": {
		"nik": "920051",
		"nama": "ALRIENDHY PRATAMA AKBAR",
		"band": "IV",
		"posisi": "OFF 1 RESOURCE & BUDGET CONTROL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8500000000000003,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197858,
			"nilai_komparatif_unit": 0.9693124104100123,
			"summary_team": 0.8181818181818182
		}]
	},
	"920053": {
		"nik": "920053",
		"nama": "ERIKA CHRISTINA SIRAIT",
		"band": "IV",
		"posisi": "OFF 1 SOLUTION DEPLOYMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0085763293310461,
			"nilai_komparatif_unit": 1.0156443243667173,
			"summary_team": 0.8909090909090909
		}]
	},
	"920070": {
		"nik": "920070",
		"nama": "YESSICA SARDINA PURBA, SE, M.M",
		"band": "IV",
		"posisi": "OFF 1 LOGISTIC PROCUREMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8799999999999993,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9090909090909096,
			"nilai_komparatif_unit": 0.9154617209427901,
			"summary_team": 0.7999999999999998
		}]
	},
	"920077": {
		"nik": "920077",
		"nama": "DINA APRIASARI",
		"band": "IV",
		"posisi": "OFF 1 SOLUTION DEPLOYMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9166666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0314049586776857,
			"nilai_komparatif_unit": 1.038632934306001,
			"summary_team": 0.9454545454545454
		}]
	},
	"920089": {
		"nik": "920089",
		"nama": "CHIKITA MAULIDYA PRAMONO PUTRI",
		"band": "IV",
		"posisi": "OFF 1 OFFERING 2",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666664,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9230769230769232,
			"nilai_komparatif_unit": 0.9295457474188327,
			"summary_team": 0.7999999999999998
		}]
	},
	"920092": {
		"nik": "920092",
		"nama": "RATRI WULANDARI",
		"band": "IV",
		"posisi": "OFF 1 PORTFOLIO EVALUATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8777777777777771,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9735327963176073,
			"nilai_komparatif_unit": 0.9803552100222792,
			"summary_team": 0.8545454545454546
		}]
	},
	"920098": {
		"nik": "920098",
		"nama": "AHMAD SYIHABUDDIN PERMANA",
		"band": "IV",
		"posisi": "OFF 1 BUSINESS MODEL ENGINEERING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0369036903690365,
			"nilai_komparatif_unit": 1.0441702005208844,
			"summary_team": 0.8727272727272727
		}]
	},
	"920101": {
		"nik": "920101",
		"nama": "GUSTI AYU MADE MAS MARHAENI",
		"band": "IV",
		"posisi": "OFF 1 ENTERPRISE SALES",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8811188811188808,
			"nilai_komparatif_unit": 0.8872936679907034,
			"summary_team": 0.7636363636363636
		}]
	},
	"920113": {
		"nik": "920113",
		"nama": "AJI PRADIPTA RAHMADI",
		"band": "IV",
		"posisi": "OFF 1 COMPLEX EVENT DESIGN",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.0070078930370683,
			"summary_team": 0.8888888888888888
		}]
	},
	"920118": {
		"nik": "920118",
		"nama": "RIFQI MEILANI",
		"band": "IV",
		"posisi": "OFF 1 BIDDING & OFFERING",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8644444444444436,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.020716769998489,
			"nilai_komparatif_unit": 1.0278698439437803,
			"summary_team": 0.8823529411764706
		}]
	},
	"920120": {
		"nik": "920120",
		"nama": "ISTANTO DWI NUGROHO",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8904761904761901,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8983957219251342,
			"nilai_komparatif_unit": 0.9046915830493455,
			"summary_team": 0.8
		}]
	},
	"920225": {
		"nik": "920225",
		"nama": "RAHADIAN FARIZI",
		"band": "IV",
		"posisi": "OFF 1 PERFORMANCE MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8400000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9523809523809521,
			"nilai_komparatif_unit": 0.9590551362257793,
			"summary_team": 0.7999999999999999
		}]
	},
	"920229": {
		"nik": "920229",
		"nama": "FERNANDES PARULIAN SINAGA",
		"band": "IV",
		"posisi": "OFF 1 BILLING & REVENUE MGT SYSTEM DEVEL",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8571428571428564,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9666666666666676,
			"nilai_komparatif_unit": 0.9734409632691672,
			"summary_team": 0.8285714285714286
		}]
	},
	"920245": {
		"nik": "920245",
		"nama": "DIMAS AGUNG SAPUTRA",
		"band": "IV",
		"posisi": "OFF 1 RESOURCE & INVENTORY MGT DEVT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96,
			"nilai_komparatif_unit": 0.9667275773155858,
			"summary_team": 0.8
		}]
	},
	"928213": {
		"nik": "928213",
		"nama": "FAIZ RAMADHANI RAHMAN",
		"band": "IV",
		"posisi": "DATA SCIENTIST",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8158730158730153,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9805447470817128,
			"nilai_komparatif_unit": 0.9874162997873208,
			"summary_team": 0.8
		}]
	},
	"930017": {
		"nik": "930017",
		"nama": "RIA MELINDA PUTRI",
		"band": "IV",
		"posisi": "OFF 1 LEGAL SETTLEMENT & COMPLIANCE",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8583333333333337,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0803177405119146,
			"nilai_komparatif_unit": 1.0878884916834697,
			"summary_team": 0.9272727272727272
		}]
	},
	"930027": {
		"nik": "930027",
		"nama": "EKA FAHDINA EFFENDI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.85528455284553,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.999135845143448,
			"nilai_komparatif_unit": 1.0061376822757144,
			"summary_team": 0.8545454545454546
		}]
	},
	"930048": {
		"nik": "930048",
		"nama": "SHINTA KURNIA ILLAHI",
		"band": "IV",
		"posisi": "ACCOUNT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9199999999999998,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0671936758893283,
			"nilai_komparatif_unit": 1.0746724550197968,
			"summary_team": 0.9818181818181818
		}]
	},
	"930062": {
		"nik": "930062",
		"nama": "YOGA PURNA TAMA",
		"band": "IV",
		"posisi": "OFF 1 EXECUTIVE SUPPORT MANAGEMENT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8699999999999994,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0344827586206902,
			"nilai_komparatif_unit": 1.0417323031417955,
			"summary_team": 0.8999999999999999
		}]
	},
	"930071": {
		"nik": "930071",
		"nama": "MAYANGSEKAR AGINTIARA",
		"band": "IV",
		"posisi": "OFF 1 HCM USER REQUIREMENT & DELIVERY",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9333333333333336,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9857142857142854,
			"nilai_komparatif_unit": 0.9926220659936815,
			"summary_team": 0.9199999999999999
		}]
	},
	"940002": {
		"nik": "940002",
		"nama": "BIMO EKA PUTRA",
		"band": "IV",
		"posisi": "PRODUCT MANAGER",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0326797385620912,
			"nilai_komparatif_unit": 1.0399166477114823,
			"summary_team": 0.9294117647058824
		}]
	},
	"940011": {
		"nik": "940011",
		"nama": "PAULINE OLIVIA ANATRI PUTRI",
		"band": "IV",
		"posisi": "OFF 1 SALES PLAN & EVALUATION",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.8433333333333333,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0132950053898673,
			"nilai_komparatif_unit": 1.0203960684026352,
			"summary_team": 0.8545454545454546
		}]
	},
	"940027": {
		"nik": "940027",
		"nama": "LISA RENITA VIRGINIA",
		"band": "IV",
		"posisi": "OFF 1 CFUC CFUW INET &WIFI INTERFACE MGT",
		"category": [{
			"code": "MD-2-03",
			"kriteria": 0.9075268817204296,
			"kriteria_bp": 0.9930408757612285,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9917061611374413,
			"nilai_komparatif_unit": 0.9986559318388943,
			"summary_team": 0.8999999999999999
		}]
	}
};