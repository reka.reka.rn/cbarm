export const cbark2 = {
	"650953": {
		"nik": "650953",
		"nama": "RUWANTO",
		"band": "II",
		"posisi": "PRINCIPAL CAPABILITY ACCOUNT MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.829885057471264,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9467748318163832,
			"nilai_komparatif_unit": 0.9500147364484447,
			"summary_team": 0.7857142857142855
		}]
	},
	"651100": {
		"nik": "651100",
		"nama": "IMANUDDIN",
		"band": "II",
		"posisi": "PRINCIPAL EXPERT PROJECT MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9591346153846103,
			"nilai_komparatif_unit": 0.9624168157333388,
			"summary_team": 0.8
		}]
	},
	"651259": {
		"nik": "651259",
		"nama": "BROCHARDUS WIDJAJATJANDRA",
		"band": "II",
		"posisi": "SM SCHOOL OF DIG PLATFORM & DIG SERVICES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.016260162601621,
			"nilai_komparatif_unit": 1.0197378490562512,
			"summary_team": 0.8666666666666666
		}]
	},
	"651279": {
		"nik": "651279",
		"nama": "MOHAMAD BERLIANTOKO",
		"band": "II",
		"posisi": "SENIOR EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9915598221764022,
			"nilai_komparatif_unit": 0.9949529829923399,
			"summary_team": 0.8181818181818181
		}]
	},
	"651298": {
		"nik": "651298",
		"nama": "UYAN SETIAWAN ASKAM",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8367816091954015,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8998060762766654,
			"nilai_komparatif_unit": 0.9028852517854745,
			"summary_team": 0.7529411764705882
		}]
	},
	"651326": {
		"nik": "651326",
		"nama": "SYARIF PATRIANA",
		"band": "II",
		"posisi": "PRINCIPAL EXPERT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8471014492753652,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9443969204448212,
			"nilai_komparatif_unit": 0.9476286877607981,
			"summary_team": 0.7999999999999998
		}]
	},
	"651327": {
		"nik": "651327",
		"nama": "EDY SISWANTO",
		"band": "II",
		"posisi": "PRINCIPAL EXPERT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8471014492753652,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0087876195660592,
			"nilai_komparatif_unit": 1.01223973465358,
			"summary_team": 0.8545454545454546
		}]
	},
	"660178": {
		"nik": "660178",
		"nama": "SURYA FACHRUDIANSYAH",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9695251594613711,
			"nilai_komparatif_unit": 0.9728429167036213,
			"summary_team": 0.8
		}]
	},
	"660186": {
		"nik": "660186",
		"nama": "NOVA GRATANATA GLORI",
		"band": "II",
		"posisi": "SENIOR STAFF II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.817142857142856,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9484265734265747,
			"nilai_komparatif_unit": 0.9516721303902297,
			"summary_team": 0.7749999999999999
		}]
	},
	"660199": {
		"nik": "660199",
		"nama": "G. ADHI WIBAWA",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9591346153846103,
			"nilai_komparatif_unit": 0.9624168157333388,
			"summary_team": 0.8
		}]
	},
	"660230": {
		"nik": "660230",
		"nama": "ADRIANI, SH, M.Hum",
		"band": "II",
		"posisi": "AVP ORG EFFECTIVENESS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8545454545454537,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.044189852700492,
			"nilai_komparatif_unit": 1.0477631157687821,
			"summary_team": 0.8923076923076924
		}]
	},
	"660234": {
		"nik": "660234",
		"nama": "SJAFJATININGSIH, DRA.",
		"band": "II",
		"posisi": "SENIOR STAFF II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8471014492753652,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0302511859398051,
			"nilai_komparatif_unit": 1.0337767502845072,
			"summary_team": 0.8727272727272727
		}]
	},
	"660242": {
		"nik": "660242",
		"nama": "MUHAMMAD RAMLI",
		"band": "II",
		"posisi": "AVP INFRASTRUCTURE SLA MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9333333333333333,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346941,
			"nilai_komparatif_unit": 0.9829440425841858,
			"summary_team": 0.9142857142857145
		}]
	},
	"660247": {
		"nik": "660247",
		"nama": "ROMLES SIMANJUNTAK,IR.",
		"band": "II",
		"posisi": "SM KEMITRAAN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8793650793650787,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9855595667870044,
			"nilai_komparatif_unit": 0.9889321944681604,
			"summary_team": 0.8666666666666667
		}]
	},
	"660252": {
		"nik": "660252",
		"nama": "ABDUL AZIS, IR, MT",
		"band": "II",
		"posisi": "SM GENERAL AFFAIR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8555555555555552,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649354,
			"nilai_komparatif_unit": 0.9382647679212683,
			"summary_team": 0.7999999999999999
		}]
	},
	"660254": {
		"nik": "660254",
		"nama": "AGUS SUSILOWARNO",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8133333333333318,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.005961251862893,
			"nilai_komparatif_unit": 1.0094036949972678,
			"summary_team": 0.8181818181818181
		}]
	},
	"660258": {
		"nik": "660258",
		"nama": "ACEP DUDUNG GANJAR, IR.MENG.",
		"band": "II",
		"posisi": "VP RISK OPERATION & PROCESS MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.839285714285713,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0167375886524839,
			"nilai_komparatif_unit": 1.0202169088798145,
			"summary_team": 0.8533333333333334
		}]
	},
	"660266": {
		"nik": "660266",
		"nama": "HERU YUDHIANTO",
		"band": "II",
		"posisi": "SM FINANCE CENTER BUSINESS PARTNER 01",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8666666666666653,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9846153846153863,
			"nilai_komparatif_unit": 0.9879847812641062,
			"summary_team": 0.8533333333333334
		}]
	},
	"660270": {
		"nik": "660270",
		"nama": "LIVINGSTONE BUTARBUTAR",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.839285714285713,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0181818181818199,
			"nilai_komparatif_unit": 1.0216660806253823,
			"summary_team": 0.8545454545454546
		}]
	},
	"660279": {
		"nik": "660279",
		"nama": "SUPARDI",
		"band": "II",
		"posisi": "AVP INFRASTRUCTURE & SUPPLY AUDIT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8962962962962957,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0320247933884303,
			"nilai_komparatif_unit": 1.035556427094923,
			"summary_team": 0.9249999999999999
		}]
	},
	"660284": {
		"nik": "660284",
		"nama": "YUDHI DESWIANDI",
		"band": "II",
		"posisi": "SM FINANCE CENTER BUSINESS PARTNER 05",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8666666666666653,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9692307692307709,
			"nilai_komparatif_unit": 0.9725475190568545,
			"summary_team": 0.8400000000000001
		}]
	},
	"660296": {
		"nik": "660296",
		"nama": "FIRDAUS, IR.",
		"band": "II",
		"posisi": "SENIOR STAFF II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8266666666666684,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7969639468690684,
			"nilai_komparatif_unit": 0.7996911921403579,
			"summary_team": 0.6588235294117646
		}]
	},
	"660320": {
		"nik": "660320",
		"nama": "MOHAMAD NURCAHYONO",
		"band": "II",
		"posisi": "AVP GROUP PERFORMANCE MANAGEMENT&SUPPORT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8222222222222213,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0945945945945956,
			"nilai_komparatif_unit": 1.0983403448808098,
			"summary_team": 0.8999999999999999
		}]
	},
	"660329": {
		"nik": "660329",
		"nama": "BUDI WIDJAJANTO, IR.",
		"band": "II",
		"posisi": "PRINCIPAL EXPERT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8471014492753652,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7941519558285997,
			"nilai_komparatif_unit": 0.7968695783443076,
			"summary_team": 0.6727272727272726
		}]
	},
	"660349": {
		"nik": "660349",
		"nama": "IRMA DILARAMA",
		"band": "II",
		"posisi": "GM ESS FINANCIAL MGT SERVICES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8596153846153877,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9461595824011899,
			"nilai_komparatif_unit": 0.9493973816230069,
			"summary_team": 0.8133333333333335
		}]
	},
	"660350": {
		"nik": "660350",
		"nama": "YOYOK SETYONO",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9034211713162776,
			"nilai_komparatif_unit": 0.9065127178374653,
			"summary_team": 0.7454545454545455
		}]
	},
	"660352": {
		"nik": "660352",
		"nama": "NOOR HIDAYAT, IR, MInfSys",
		"band": "II",
		"posisi": "SM BSS & CEM PLATFORM DEVELOPMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8666666666666657,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9576923076923088,
			"nilai_komparatif_unit": 0.9609695724014152,
			"summary_team": 0.8300000000000001
		}]
	},
	"660368": {
		"nik": "660368",
		"nama": "JHON HENDRY",
		"band": "II",
		"posisi": "GM MILITARY & POLICE SERVICES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8634920634920623,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9264705882352955,
			"nilai_komparatif_unit": 0.9296410108631695,
			"summary_team": 0.8
		}]
	},
	"660394": {
		"nik": "660394",
		"nama": "YANYAN NURYANA",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8699999999999994,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.003134796238245,
			"nilai_komparatif_unit": 1.0065675671186023,
			"summary_team": 0.8727272727272727
		}]
	},
	"660395": {
		"nik": "660395",
		"nama": "RETNO DYAH ARUMSARI",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8699999999999994,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0658307210031355,
			"nilai_komparatif_unit": 1.069478040063515,
			"summary_team": 0.9272727272727272
		}]
	},
	"660398": {
		"nik": "660398",
		"nama": "DESNAIDI AZIZ",
		"band": "II",
		"posisi": "SM SCHOOL OF  TELECOMMUNICATION & MEDIA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0006253908692884,
			"nilai_komparatif_unit": 1.0040495744553857,
			"summary_team": 0.8533333333333333
		}]
	},
	"660400": {
		"nik": "660400",
		"nama": "DENDI FERIANDI",
		"band": "II",
		"posisi": "AVP INTEGRATED GOVERNANCE & QUALITY MGT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8479999999999996,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0691823899371073,
			"nilai_komparatif_unit": 1.0728411785542806,
			"summary_team": 0.9066666666666666
		}]
	},
	"660401": {
		"nik": "660401",
		"nama": "Dr. Ir. ABDUL MUKTI SOMA, MM.",
		"band": "II",
		"posisi": "PRINCIPAL EXPERT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8471014492753652,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0277260604840706,
			"nilai_komparatif_unit": 1.0312429837396926,
			"summary_team": 0.8705882352941178
		}]
	},
	"660405": {
		"nik": "660405",
		"nama": "WINARTA, IR.MT.",
		"band": "II",
		"posisi": "AVP INTEGRATED OPEX MGT & OPT POLICY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8476190476190469,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9299405155320564,
			"nilai_komparatif_unit": 0.9331228124019824,
			"summary_team": 0.7882352941176471
		}]
	},
	"660410": {
		"nik": "660410",
		"nama": "FERDINAL",
		"band": "II",
		"posisi": "OSM INFRA MAINTENANCE SUPPORT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8535947712418291,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0075038284839215,
			"nilai_komparatif_unit": 1.0109515503825512,
			"summary_team": 0.8600000000000001
		}]
	},
	"660420": {
		"nik": "660420",
		"nama": "SISWANTO",
		"band": "II",
		"posisi": "SM DIGITAL PLATFORM & SERVICE ASSURANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8496296296296325,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0043591979075817,
			"nilai_komparatif_unit": 1.0077961587436777,
			"summary_team": 0.8533333333333334
		}]
	},
	"660440": {
		"nik": "660440",
		"nama": "SISGIYANTO",
		"band": "II",
		"posisi": "SENIOR STAFF II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9380863039399576,
			"nilai_komparatif_unit": 0.9412964760519239,
			"summary_team": 0.7999999999999998
		}]
	},
	"660485": {
		"nik": "660485",
		"nama": "CHAIRUL SALEH",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9809331293706242,
			"nilai_komparatif_unit": 0.9842899251818238,
			"summary_team": 0.8181818181818181
		}]
	},
	"670018": {
		"nik": "670018",
		"nama": "ZAMZAM BARJAMAN F.",
		"band": "II",
		"posisi": "SM CENTRALIZED PAYMENT MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.839285714285713,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9849645390070937,
			"nilai_komparatif_unit": 0.9883351304773201,
			"summary_team": 0.8266666666666667
		}]
	},
	"670040": {
		"nik": "670040",
		"nama": "ASRAAL FATONI",
		"band": "II",
		"posisi": "AVP INTEGRATED CAPEX MGT PROG ALIGNMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8476190476190469,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9700374531835213,
			"nilai_komparatif_unit": 0.9733569635171588,
			"summary_team": 0.8222222222222222
		}]
	},
	"670090": {
		"nik": "670090",
		"nama": "ARIF HALMAN",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.024530157342652,
			"nilai_komparatif_unit": 1.0280361440787937,
			"summary_team": 0.8545454545454546
		}]
	},
	"670092": {
		"nik": "670092",
		"nama": "SEDIYOKO",
		"band": "II",
		"posisi": "PRINCIPAL EXPERT CORE NETWORK",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8577777777777765,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0233160621761672,
			"nilai_komparatif_unit": 1.026817894225871,
			"summary_team": 0.8777777777777777
		}]
	},
	"670103": {
		"nik": "670103",
		"nama": "Mohamad Ramdan",
		"band": "II",
		"posisi": "VP HUMAN CAPITAL MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8444444444444442,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0263157894736845,
			"nilai_komparatif_unit": 1.0298278867206025,
			"summary_team": 0.8666666666666667
		}]
	},
	"670156": {
		"nik": "670156",
		"nama": "BAMBANG TRIWIRYONO, Ir.",
		"band": "II",
		"posisi": "PRINCIPAL EXPERT INFRAST & PLATFORM MGT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8367816091954015,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.86913086913087,
			"nilai_komparatif_unit": 0.8721050727473334,
			"summary_team": 0.7272727272727273
		}]
	},
	"670158": {
		"nik": "670158",
		"nama": "DANANG TAUFIK KARUNIA, IR",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8471014492753652,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8800062213235836,
			"nilai_komparatif_unit": 0.8830176408680167,
			"summary_team": 0.7454545454545455
		}]
	},
	"670164": {
		"nik": "670164",
		"nama": "HERI PURNOMO",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.024530157342652,
			"nilai_komparatif_unit": 1.0280361440787937,
			"summary_team": 0.8545454545454546
		}]
	},
	"670167": {
		"nik": "670167",
		"nama": "WIDHIOWATI",
		"band": "II",
		"posisi": "AVP PROCESS MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8333333333333326,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9600000000000009,
			"nilai_komparatif_unit": 0.9632851617325027,
			"summary_team": 0.8
		}]
	},
	"670168": {
		"nik": "670168",
		"nama": "ARDI IMAWAN",
		"band": "II",
		"posisi": "OSM COLLECTION & DEBT MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8596153846153877,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0237136465324348,
			"nilai_komparatif_unit": 1.0272168391330894,
			"summary_team": 0.88
		}]
	},
	"670197": {
		"nik": "670197",
		"nama": "BUDI RAHAYU",
		"band": "II",
		"posisi": "AVP ENTERPRISE DIVISION GOVERNANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0550714970609039,
			"nilai_komparatif_unit": 1.058681997589235,
			"summary_team": 0.8705882352941177
		}]
	},
	"670205": {
		"nik": "670205",
		"nama": "AFIANTO MUKTI HARIBOWO",
		"band": "II",
		"posisi": "TRIBE LEADER OF CONSUMER DIGITIZATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8176100628930801,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0763076923076944,
			"nilai_komparatif_unit": 1.0799908640193263,
			"summary_team": 0.88
		}]
	},
	"670209": {
		"nik": "670209",
		"nama": "AGUS RIYANTO, IR.MSC",
		"band": "II",
		"posisi": "PRINCIPAL EXPERT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8471014492753652,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0277260604840703,
			"nilai_komparatif_unit": 1.0312429837396924,
			"summary_team": 0.8705882352941177
		}]
	},
	"670223": {
		"nik": "670223",
		"nama": "HEPTA YUNIARITA",
		"band": "II",
		"posisi": "SM GENERAL AFFAIRS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8496296296296325,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0828247602441112,
			"nilai_komparatif_unit": 1.0865302336455274,
			"summary_team": 0.9199999999999999
		}]
	},
	"670226": {
		"nik": "670226",
		"nama": "MUKTI WIBOWO",
		"band": "II",
		"posisi": "SM PARTNERSHIP & INTERFACE MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8490196078431361,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.981524249422634,
			"nilai_komparatif_unit": 0.9848830680723485,
			"summary_team": 0.8333333333333333
		}]
	},
	"670234": {
		"nik": "670234",
		"nama": "SIGIT MARSONO,IR",
		"band": "II",
		"posisi": "PROJECT TEAM",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8644444444444433,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9798880991985495,
			"nilai_komparatif_unit": 0.9832413188710714,
			"summary_team": 0.8470588235294116
		}]
	},
	"670237": {
		"nik": "670237",
		"nama": "MOH AHMAD,IR",
		"band": "II",
		"posisi": "AVP RISK OPERATION & SUPPORT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8333333333333326,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9600000000000009,
			"nilai_komparatif_unit": 0.9632851617325027,
			"summary_team": 0.8
		}]
	},
	"670241": {
		"nik": "670241",
		"nama": "NUGROHO BUDI PURNOMO, IR. M.KOM.",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.733333333333333,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0129870129870133,
			"nilai_komparatif_unit": 1.016453498581374,
			"summary_team": 0.7428571428571427
		}]
	},
	"670250": {
		"nik": "670250",
		"nama": "CHRISNA JULIA, IR.",
		"band": "II",
		"posisi": "SM ENTERPRISE & ANALYTIC PLATFORM DEVT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8666666666666657,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.980769230769232,
			"nilai_komparatif_unit": 0.9841254657122928,
			"summary_team": 0.8500000000000001
		}]
	},
	"670252": {
		"nik": "670252",
		"nama": "YULIARDY",
		"band": "II",
		"posisi": "COORDINATOR WIFI PROJECT MGT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8490196078431361,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8452655889145508,
			"nilai_komparatif_unit": 0.8481581245046579,
			"summary_team": 0.7176470588235294
		}]
	},
	"670256": {
		"nik": "670256",
		"nama": "SASONGKO, IR.MT.",
		"band": "II",
		"posisi": "SM TELKOMGROUP & OLO DEPLOYMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8608695652173906,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9292929292929301,
			"nilai_komparatif_unit": 0.9324730100945943,
			"summary_team": 0.7999999999999999
		}]
	},
	"670262": {
		"nik": "670262",
		"nama": "HARY NUGROHO, IR",
		"band": "II",
		"posisi": "TRIBE LEADER RESEARCH MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8471014492753652,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9443969204448212,
			"nilai_komparatif_unit": 0.9476286877607981,
			"summary_team": 0.7999999999999998
		}]
	},
	"670269": {
		"nik": "670269",
		"nama": "JUDOKO ADI",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9695251594613711,
			"nilai_komparatif_unit": 0.9728429167036213,
			"summary_team": 0.8
		}]
	},
	"670277": {
		"nik": "670277",
		"nama": "IR GATOT WAHYUDIANTO",
		"band": "II",
		"posisi": "VP FINANCE, BILLING AND COLLECTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8205128205128209,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9424999999999994,
			"nilai_komparatif_unit": 0.9457252759717526,
			"summary_team": 0.7733333333333333
		}]
	},
	"670298": {
		"nik": "670298",
		"nama": "BUNGARAN ADIL PARUHUM SIAGIAN",
		"band": "II",
		"posisi": "OSM SME INDUSTRY SOLUTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9591346153846103,
			"nilai_komparatif_unit": 0.9624168157333388,
			"summary_team": 0.8
		}]
	},
	"670299": {
		"nik": "670299",
		"nama": "CHARINDRA PURNOMO, IR.MT.",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8133333333333318,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9660421545667464,
			"nilai_komparatif_unit": 0.9693479928148363,
			"summary_team": 0.7857142857142856
		}]
	},
	"670303": {
		"nik": "670303",
		"nama": "IDA BAGUS MAHA PUTRA ARTA",
		"band": "II",
		"posisi": "SM DATA MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8755555555555548,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0203045685279197,
			"nilai_komparatif_unit": 1.0237960951154454,
			"summary_team": 0.8933333333333333
		}]
	},
	"670306": {
		"nik": "670306",
		"nama": "MUHAMAD WAHYUDI, IR",
		"band": "II",
		"posisi": "SM PERENCANAAN & PENGENDALIAN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8793650793650787,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0007220216606505,
			"nilai_komparatif_unit": 1.0041465359215167,
			"summary_team": 0.88
		}]
	},
	"670308": {
		"nik": "670308",
		"nama": "RADEN DEKKI KRISMASASONO, IR",
		"band": "II",
		"posisi": "AVP DIGITAL BUSINESS PARENTING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8749999999999996,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0666666666666673,
			"nilai_komparatif_unit": 1.0703168463694472,
			"summary_team": 0.9333333333333333
		}]
	},
	"670310": {
		"nik": "670310",
		"nama": "WAHYU ARI BHINUKO",
		"band": "II",
		"posisi": "PRINCIPAL EXPERT SATELLITE SYSTEM",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8361111111111127,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.956810631229234,
			"nilai_komparatif_unit": 0.9600848788031562,
			"summary_team": 0.7999999999999999
		}]
	},
	"670395": {
		"nik": "670395",
		"nama": "ISMANTO",
		"band": "II",
		"posisi": "AVP SECURITY & SAFETY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.852631578947368,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9695473251028812,
			"nilai_komparatif_unit": 0.9728651581969203,
			"summary_team": 0.8266666666666667
		}]
	},
	"670452": {
		"nik": "670452",
		"nama": "JAROT WIDYATMOKO",
		"band": "II",
		"posisi": "AVP ICOFR & RISK MANAGEMENT AUDIT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.948148148148148,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8964843750000001,
			"nilai_komparatif_unit": 0.8995521835026417,
			"summary_team": 0.85
		}]
	},
	"670619": {
		"nik": "670619",
		"nama": "RADEN ACHMAD FAISAL (IVAN)",
		"band": "II",
		"posisi": "AVP SHAREHOLDER RELATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8888888888888893,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0049999999999994,
			"nilai_komparatif_unit": 1.0084391536887123,
			"summary_team": 0.8933333333333333
		}]
	},
	"680006": {
		"nik": "680006",
		"nama": "HARI SULAKSONO",
		"band": "II",
		"posisi": "SM FIXED ACCESS DEPLOYMENT (FAD)",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8608695652173906,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9912457912457919,
			"nilai_komparatif_unit": 0.9946378774342338,
			"summary_team": 0.8533333333333332
		}]
	},
	"680025": {
		"nik": "680025",
		"nama": "AAH AHADIAT, RD",
		"band": "II",
		"posisi": "SM GENERAL SUPPORT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9380863039399578,
			"nilai_komparatif_unit": 0.9412964760519241,
			"summary_team": 0.8
		}]
	},
	"680063": {
		"nik": "680063",
		"nama": "JAN ESSON SARAGIH",
		"band": "II",
		"posisi": "SM LEARNING,INNOVATION,&RESEARCH FULFIL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0208586248758365,
			"nilai_komparatif_unit": 1.0243520474682704,
			"summary_team": 0.8705882352941177
		}]
	},
	"680064": {
		"nik": "680064",
		"nama": "KARTITAH YULIANTI, IR.MT.",
		"band": "II",
		"posisi": "SM CFU/FU SUPPORT & ITSM",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8367816091954015,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0584772370486668,
			"nilai_komparatif_unit": 1.0620993921672883,
			"summary_team": 0.8857142857142859
		}]
	},
	"680074": {
		"nik": "680074",
		"nama": "IMAM RIJANTO, IR",
		"band": "II",
		"posisi": "AVP FINANCIAL & PORTFOLIO PLANNING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8520833333333323,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8684596577017127,
			"nilai_komparatif_unit": 0.871431564403487,
			"summary_team": 0.7400000000000002
		}]
	},
	"680075": {
		"nik": "680075",
		"nama": "ANANG ASRIANTO, IR",
		"band": "II",
		"posisi": "AVP NETWORK/IT PROG MGT & DIGITIZATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8479999999999996,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0465801886792458,
			"nilai_komparatif_unit": 1.0501616315811664,
			"summary_team": 0.8875000000000001
		}]
	},
	"680089": {
		"nik": "680089",
		"nama": "SOLIHIN, IR, MT",
		"band": "II",
		"posisi": "PRINCIPAL LEARNING ANALYST",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0050924685070974,
			"nilai_komparatif_unit": 1.0085319386270613,
			"summary_team": 0.857142857142857
		}]
	},
	"680166": {
		"nik": "680166",
		"nama": "ABDULLAH MASYHURI",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0472727272727271,
			"nilai_komparatif_unit": 1.0508565400718202,
			"summary_team": 0.8727272727272727
		}]
	},
	"680169": {
		"nik": "680169",
		"nama": "SASMITO",
		"band": "II",
		"posisi": "OPRTNG MODEL ALIGNMENT&CONTROLLING COORD",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8944444444444447,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9782608695652171,
			"nilai_komparatif_unit": 0.9816085207871958,
			"summary_team": 0.875
		}]
	},
	"680170": {
		"nik": "680170",
		"nama": "ARIYA BAYU",
		"band": "II",
		"posisi": "SR MANAGING CONSULTANT DELIVERY & OPRTN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8222222222222225,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9729729729729724,
			"nilai_komparatif_unit": 0.9763025287829404,
			"summary_team": 0.7999999999999998
		}]
	},
	"680174": {
		"nik": "680174",
		"nama": "DINOOR SUSATIJO",
		"band": "II",
		"posisi": "SM INDIGO MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8648148148148137,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0021413276231277,
			"nilai_komparatif_unit": 1.0055706988106965,
			"summary_team": 0.8666666666666667
		}]
	},
	"680177": {
		"nik": "680177",
		"nama": "JOHANES ADI PURNAMA PUTRA",
		"band": "II",
		"posisi": "AVP GROUP RETAIL STRATEGY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8520833333333323,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.985819070904647,
			"nilai_komparatif_unit": 0.9891925866201746,
			"summary_team": 0.8400000000000003
		}]
	},
	"680183": {
		"nik": "680183",
		"nama": "ROFIQ SONI",
		"band": "II",
		"posisi": "OSM CORE NETWORK OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8535947712418291,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9957886676875969,
			"nilai_komparatif_unit": 0.9991962997967077,
			"summary_team": 0.8500000000000001
		}]
	},
	"680354": {
		"nik": "680354",
		"nama": "BRILIYAN FARID FATONI",
		"band": "II",
		"posisi": "GM ESS REGIONAL BANKING SERVICES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8596153846153877,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.085756897837431,
			"nilai_komparatif_unit": 1.0894724051411555,
			"summary_team": 0.9333333333333333
		}]
	},
	"680356": {
		"nik": "680356",
		"nama": "ARIEYANTO,IR",
		"band": "II",
		"posisi": "SM CONSUMER & DIGITAL BILLING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.839285714285713,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.048510638297874,
			"nilai_komparatif_unit": 1.0520986872823086,
			"summary_team": 0.88
		}]
	},
	"680361": {
		"nik": "680361",
		"nama": "NURUL FATMAWATI",
		"band": "II",
		"posisi": "VP TELECOMMUNICATION SALES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9244444444444446,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9735576923076921,
			"nilai_komparatif_unit": 0.9768892490526421,
			"summary_team": 0.8999999999999999
		}]
	},
	"680592": {
		"nik": "680592",
		"nama": "TRI INDRA BASOEKI",
		"band": "II",
		"posisi": "EVP ENTERPRISE SHARED SERVICES BUSINESS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9222222222222223,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0068846815834769,
			"nilai_komparatif_unit": 1.0103302847344982,
			"summary_team": 0.9285714285714288
		}]
	},
	"690019": {
		"nik": "690019",
		"nama": "MINARFA MUIS",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9034211713162776,
			"nilai_komparatif_unit": 0.9065127178374653,
			"summary_team": 0.7454545454545455
		}]
	},
	"690059": {
		"nik": "690059",
		"nama": "MOHAMMAD IZZUDIN",
		"band": "II",
		"posisi": "SM SCHOOL OF DIG GO TO MARKET",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9380863039399578,
			"nilai_komparatif_unit": 0.9412964760519241,
			"summary_team": 0.8
		}]
	},
	"690167": {
		"nik": "690167",
		"nama": "MA'RUF,S.H,M.H",
		"band": "II",
		"posisi": "AVP CONTRACT TRANSACTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.875362318840579,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0138658940397358,
			"nilai_komparatif_unit": 1.017335387203265,
			"summary_team": 0.8875
		}]
	},
	"690291": {
		"nik": "690291",
		"nama": "DWI WIDODO HERU KURNIAWAN",
		"band": "II",
		"posisi": "AVP DIGITAL BUSINESS INNOVATION&PLANNING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8648148148148137,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9558886509635987,
			"nilai_komparatif_unit": 0.959159743480972,
			"summary_team": 0.8266666666666667
		}]
	},
	"690292": {
		"nik": "690292",
		"nama": "AMBARUKMI DEWI SRIWARNI,",
		"band": "II",
		"posisi": "SYNERGY PROJECT LEADER EDUCATION & FU",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8696969696969686,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0233449477351928,
			"nilai_komparatif_unit": 1.0268468786325353,
			"summary_team": 0.89
		}]
	},
	"690294": {
		"nik": "690294",
		"nama": "OCTA ISTIADI",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9809331293706242,
			"nilai_komparatif_unit": 0.9842899251818238,
			"summary_team": 0.8181818181818181
		}]
	},
	"690596": {
		"nik": "690596",
		"nama": "AGUS SUPRIATNA",
		"band": "II",
		"posisi": "OSM ACCESS NETWORK OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8535947712418291,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9372128637059735,
			"nilai_komparatif_unit": 0.9404200468674895,
			"summary_team": 0.8
		}]
	},
	"690608": {
		"nik": "690608",
		"nama": "DONNY KERTAPUTRA WIDJAJA, SH, M.Hum",
		"band": "II",
		"posisi": "AVP A&A COMPLIANCE AUDIT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.948148148148148,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9624023437500001,
			"nilai_komparatif_unit": 0.9656957264072477,
			"summary_team": 0.9125
		}]
	},
	"690610": {
		"nik": "690610",
		"nama": "SRI PRAPTINI RAHAYU, IR",
		"band": "II",
		"posisi": "SM SCHOOL OF DIGITAL CONNECTIVITY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9693558474046231,
			"nilai_komparatif_unit": 0.972673025253655,
			"summary_team": 0.8266666666666667
		}]
	},
	"700001": {
		"nik": "700001",
		"nama": "ANKA JANUAR",
		"band": "II",
		"posisi": "AVP NETWORK OPERATION STRATEGY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8688888888888879,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9700401899890403,
			"nilai_komparatif_unit": 0.9733597096881453,
			"summary_team": 0.842857142857143
		}]
	},
	"700295": {
		"nik": "700295",
		"nama": "KUNCORO WASTUWIBOWO",
		"band": "II",
		"posisi": "SYNERGY PROJECT LEADER MSME,LOGISTIC&SOE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8696969696969686,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9888501742160292,
			"nilai_komparatif_unit": 0.9922340624988545,
			"summary_team": 0.8600000000000001
		}]
	},
	"700671": {
		"nik": "700671",
		"nama": "TARWA MIHARDJA",
		"band": "II",
		"posisi": "PRINCIPAL EXPERT ICFR ADVISORY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8690476190476184,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0042341220423419,
			"nilai_komparatif_unit": 1.0076706548633898,
			"summary_team": 0.8727272727272727
		}]
	},
	"700674": {
		"nik": "700674",
		"nama": "DEDY KURNIADHIE",
		"band": "II",
		"posisi": "SM PARTNERSHIP OPERATION MGT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8496296296296325,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0514385353094995,
			"nilai_komparatif_unit": 1.0550366036847876,
			"summary_team": 0.8933333333333333
		}]
	},
	"710378": {
		"nik": "710378",
		"nama": "ARIS INDRA BUDIARTO, MM",
		"band": "II",
		"posisi": "PRINCIPAL EXPERT SOLUTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8133333333333318,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0506706408345774,
			"nilai_komparatif_unit": 1.054266081441591,
			"summary_team": 0.8545454545454546
		}]
	},
	"710381": {
		"nik": "710381",
		"nama": "ISKANDAR SATYOGO PRASETYO",
		"band": "II",
		"posisi": "OSM ASSURANCE CENTER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9431490384615335,
			"nilai_komparatif_unit": 0.9463765354711166,
			"summary_team": 0.7866666666666667
		}]
	},
	"710387": {
		"nik": "710387",
		"nama": "ANDANG ASHARI, ST.",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9591346153846103,
			"nilai_komparatif_unit": 0.9624168157333388,
			"summary_team": 0.8
		}]
	},
	"710388": {
		"nik": "710388",
		"nama": "AGUS SOFIAN",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.839285714285713,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9531914893617034,
			"nilai_komparatif_unit": 0.9564533520748258,
			"summary_team": 0.7999999999999998
		}]
	},
	"710391": {
		"nik": "710391",
		"nama": "DIANI NURHIDAYATI",
		"band": "II",
		"posisi": "SM CASH OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8666666666666653,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9384615384615401,
			"nilai_komparatif_unit": 0.9416729946423512,
			"summary_team": 0.8133333333333335
		}]
	},
	"710400": {
		"nik": "710400",
		"nama": "ZAKARIAH",
		"band": "II",
		"posisi": "AVP SEKRETARIAT DIREKTORAT SP",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.849999999999999,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0252100840336145,
			"nilai_komparatif_unit": 1.0287183975084502,
			"summary_team": 0.8714285714285713
		}]
	},
	"710401": {
		"nik": "710401",
		"nama": "AGUS KAYANTO,ST",
		"band": "II",
		"posisi": "STRATEGIC INV DIG TELCO PROJ TEAM LEAD 3",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.88235294117647,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9973333333333341,
			"nilai_komparatif_unit": 1.0007462513554333,
			"summary_team": 0.8800000000000001
		}]
	},
	"710405": {
		"nik": "710405",
		"nama": "SUHARSONO",
		"band": "II",
		"posisi": "SM BINA LINGKUNGAN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8793650793650787,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0007220216606505,
			"nilai_komparatif_unit": 1.0041465359215167,
			"summary_team": 0.88
		}]
	},
	"710409": {
		"nik": "710409",
		"nama": "ARIEF RACHMATSJAH",
		"band": "II",
		"posisi": "AVP ORCHESTRATION STRATEGY & ALIGNMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8222222222222213,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8391891891891904,
			"nilai_komparatif_unit": 0.8420609310752879,
			"summary_team": 0.6900000000000002
		}]
	},
	"710420": {
		"nik": "710420",
		"nama": "IRWAN SOBRIAN",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8133333333333318,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.948477751756442,
			"nilai_komparatif_unit": 0.9517234838545666,
			"summary_team": 0.7714285714285714
		}]
	},
	"710422": {
		"nik": "710422",
		"nama": "ASEP MULYANA",
		"band": "II",
		"posisi": "SM SCHOOL OF DIG LEADERSHIP & TRANSFORM",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9380863039399578,
			"nilai_komparatif_unit": 0.9412964760519241,
			"summary_team": 0.8
		}]
	},
	"710430": {
		"nik": "710430",
		"nama": "ASRUL SANI, ST.",
		"band": "II",
		"posisi": "GM COMMERCE & COMMUNITY SERVICE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8537037037037024,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9839479392624744,
			"nilai_komparatif_unit": 0.9873150518841818,
			"summary_team": 0.84
		}]
	},
	"710458": {
		"nik": "710458",
		"nama": "HAZIM AHMADI",
		"band": "II",
		"posisi": "AVP ADVANCE NETWORK/IT ORCHESTRATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8648148148148137,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0483940042826565,
			"nilai_komparatif_unit": 1.0519816541404208,
			"summary_team": 0.9066666666666666
		}]
	},
	"710500": {
		"nik": "710500",
		"nama": "AMINI KUSUMAWATI, RR",
		"band": "II",
		"posisi": "COORD GROUP OF CX CHANGE MGT&DIGITIZATN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8176100628930801,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.04369230769231,
			"nilai_komparatif_unit": 1.0472638681399529,
			"summary_team": 0.8533333333333334
		}]
	},
	"710501": {
		"nik": "710501",
		"nama": "RANDI PERMANA",
		"band": "II",
		"posisi": "OSM IP SERVICE OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8535947712418291,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0022970903522215,
			"nilai_komparatif_unit": 1.0057269945666205,
			"summary_team": 0.8555555555555554
		}]
	},
	"710522": {
		"nik": "710522",
		"nama": "HANANTO MULYONO",
		"band": "II",
		"posisi": "EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.85528455284553,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.999135845143448,
			"nilai_komparatif_unit": 1.002554931439319,
			"summary_team": 0.8545454545454546
		}]
	},
	"710536": {
		"nik": "710536",
		"nama": "AGUS LAKSONO",
		"band": "II",
		"posisi": "SM BIG DATA ANALYTIC",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8755555555555548,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9593908629441633,
			"nilai_komparatif_unit": 0.9626739401831801,
			"summary_team": 0.84
		}]
	},
	"720064": {
		"nik": "720064",
		"nama": "IRENA SANTI WIDJAJA",
		"band": "II",
		"posisi": "GM ESS COMMERCIAL & TOURISM SERVICES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8596153846153877,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9306487695749408,
			"nilai_komparatif_unit": 0.9338334901209904,
			"summary_team": 0.8
		}]
	},
	"720068": {
		"nik": "720068",
		"nama": "SURATMAN",
		"band": "II",
		"posisi": "GM TRADING BUSINESS SERVICE SEGMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8537037037037024,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.999566160520609,
			"nilai_komparatif_unit": 1.002986719374407,
			"summary_team": 0.8533333333333334
		}]
	},
	"720069": {
		"nik": "720069",
		"nama": "MOHAMAD ABDUL MUQTADIR SR",
		"band": "II",
		"posisi": "AVP ENTERPRISE PORTFOLIO MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8133333333333318,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.098360655737707,
			"nilai_komparatif_unit": 1.1021192936488686,
			"summary_team": 0.8933333333333333
		}]
	},
	"720076": {
		"nik": "720076",
		"nama": "FX.ALI SARTONO,ST",
		"band": "II",
		"posisi": "OSM BS PROJECT OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.071033653846148,
			"nilai_komparatif_unit": 1.074698777568895,
			"summary_team": 0.8933333333333332
		}]
	},
	"720077": {
		"nik": "720077",
		"nama": "ERVIA TISSYARAKSITA DEVI",
		"band": "II",
		"posisi": "HUMAN CAPITAL RESTRUCTURING COORD",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8352941176470572,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9577464788732414,
			"nilai_komparatif_unit": 0.9610239289584839,
			"summary_team": 0.8
		}]
	},
	"720080": {
		"nik": "720080",
		"nama": "PATRICIA EUGENE GASPERSZ",
		"band": "II",
		"posisi": "TRIBE LEADER OF ENT, W'SALE & GOV'T DGTL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8311111111111102,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197872,
			"nilai_komparatif_unit": 0.9658607905071888,
			"summary_team": 0.8
		}]
	},
	"720084": {
		"nik": "720084",
		"nama": "RINCAN SIMATUPANG",
		"band": "II",
		"posisi": "SM COMMUNICATION PRODUCT MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8347826086956516,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.038194444444445,
			"nilai_komparatif_unit": 1.0417471909650544,
			"summary_team": 0.8666666666666666
		}]
	},
	"720091": {
		"nik": "720091",
		"nama": "IWAN SETIAWAN,ST",
		"band": "II",
		"posisi": "OSM ENTERPRISE SOLUTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0070913461538409,
			"nilai_komparatif_unit": 1.010537656520006,
			"summary_team": 0.8400000000000001
		}]
	},
	"720095": {
		"nik": "720095",
		"nama": "BAMBANG SATYA WIENANTONO,",
		"band": "II",
		"posisi": "PRINCIPAL LEARNING ANALYST",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8528000000000042,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0050924685070974,
			"nilai_komparatif_unit": 1.0085319386270613,
			"summary_team": 0.857142857142857
		}]
	},
	"720103": {
		"nik": "720103",
		"nama": "EVI RINA MAHENDRA",
		"band": "II",
		"posisi": "GM ESS HEALTHCARE & WELFARE SERVICES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8596153846153877,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9461595824011897,
			"nilai_komparatif_unit": 0.9493973816230067,
			"summary_team": 0.8133333333333332
		}]
	},
	"720140": {
		"nik": "720140",
		"nama": "AUGUST HOTH MERCYON PURBA, ST, MM",
		"band": "II",
		"posisi": "GM ESS BANKING SOE MANAGEMENT SERVICES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8596153846153877,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0237136465324348,
			"nilai_komparatif_unit": 1.0272168391330894,
			"summary_team": 0.88
		}]
	},
	"720141": {
		"nik": "720141",
		"nama": "VERLIAN ULIANDRES GUSTIANTY",
		"band": "II",
		"posisi": "GM ESS TRANSPORTATION MANAGEMENT SERVICE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8596153846153877,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9616703952274388,
			"nilai_komparatif_unit": 0.9649612731250233,
			"summary_team": 0.8266666666666667
		}]
	},
	"720143": {
		"nik": "720143",
		"nama": "RINA SUSANTI",
		"band": "II",
		"posisi": "VP BUSINESS PLANNING & PERFORMANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9882352941176464,
			"nilai_komparatif_unit": 0.9916170782540454,
			"summary_team": 0.8399999999999999
		}]
	},
	"720148": {
		"nik": "720148",
		"nama": "IR. R. ADAM KAMIL KHUSNUN. M",
		"band": "II",
		"posisi": "VP HUMAN CAPITAL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8205128205128209,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0180147058823525,
			"nilai_komparatif_unit": 1.0214983964603617,
			"summary_team": 0.8352941176470589
		}]
	},
	"720154": {
		"nik": "720154",
		"nama": "SAMUDRA PRASETIO",
		"band": "II",
		"posisi": "EVP SOLUTION DEVELOPMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8800000000000002,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0037878787878785,
			"nilai_komparatif_unit": 1.007222884545111,
			"summary_team": 0.8833333333333333
		}]
	},
	"720156": {
		"nik": "720156",
		"nama": "ANDRI NOPENDRA",
		"band": "II",
		"posisi": "AUDITOR UTAMA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8690476190476184,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0027397260273978,
			"nilai_komparatif_unit": 1.0061711449603192,
			"summary_team": 0.8714285714285712
		}]
	},
	"720157": {
		"nik": "720157",
		"nama": "SLAMET SAPUTRO",
		"band": "II",
		"posisi": "AVP PARLIAMENT RELATION & SUPPORT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.868627450980391,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9497742663656897,
			"nilai_komparatif_unit": 0.9530244351931685,
			"summary_team": 0.825
		}]
	},
	"720158": {
		"nik": "720158",
		"nama": "JEAMI GUMILARSJAH",
		"band": "II",
		"posisi": "TRIBE LEADER OF TRAVEL & TOURISM",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8496296296296325,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9102005231037459,
			"nilai_komparatif_unit": 0.913315268861458,
			"summary_team": 0.7733333333333334
		}]
	},
	"720170": {
		"nik": "720170",
		"nama": "AHMAD HIDAYAT",
		"band": "II",
		"posisi": "STRATEGIC INV DIG TELCO PROJ TEAM LEAD 2",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.88235294117647,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9520000000000007,
			"nilai_komparatif_unit": 0.9552577853847317,
			"summary_team": 0.8400000000000001
		}]
	},
	"720171": {
		"nik": "720171",
		"nama": "LILIS MULIA WIJATI, ST.",
		"band": "II",
		"posisi": "SM BUSINESS DEVELOPMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.733333333333333,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0181818181818185,
			"nilai_komparatif_unit": 1.021666080625381,
			"summary_team": 0.7466666666666666
		}]
	},
	"720179": {
		"nik": "720179",
		"nama": "DIDI MUHARWOKO",
		"band": "II",
		"posisi": "AVP BROADBAND & RESOURCES REGULATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.868627450980391,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0648984198645612,
			"nilai_komparatif_unit": 1.0685425485499163,
			"summary_team": 0.9249999999999999
		}]
	},
	"720185": {
		"nik": "720185",
		"nama": "SRI SAPTADI",
		"band": "II",
		"posisi": "SM EMPLOYEE WELLNESS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8478632478632463,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0536290322580666,
			"nilai_komparatif_unit": 1.0572345966091365,
			"summary_team": 0.8933333333333334
		}]
	},
	"720188": {
		"nik": "720188",
		"nama": "ERA AZURA",
		"band": "II",
		"posisi": "AVP QUALITY ASSURANCE & SYSTEM DEV",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8690476190476184,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9358904109589049,
			"nilai_komparatif_unit": 0.9390930686296316,
			"summary_team": 0.8133333333333335
		}]
	},
	"720194": {
		"nik": "720194",
		"nama": "INDRAMA YUSUF MUDA PURBA",
		"band": "II",
		"posisi": "STRATEGIC INV ICT&SERV PROJ TEAM LEAD 3",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.88235294117647,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9733333333333342,
			"nilai_komparatif_unit": 0.9766641223121207,
			"summary_team": 0.8588235294117649
		}]
	},
	"720203": {
		"nik": "720203",
		"nama": "MOCHAMMAD HASAN JAUHARI, ST",
		"band": "II",
		"posisi": "SM TRANSPORT DEPLOYMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8577777777777765,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9637305699481878,
			"nilai_komparatif_unit": 0.9670284978532253,
			"summary_team": 0.8266666666666665
		}]
	},
	"720206": {
		"nik": "720206",
		"nama": "MOCH. WASI`UL HAKIM",
		"band": "II",
		"posisi": "GM ESS MEDIA & COMMUNICATION SERVICES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8596153846153877,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.008202833706186,
			"nilai_komparatif_unit": 1.011652947631073,
			"summary_team": 0.8666666666666667
		}]
	},
	"720212": {
		"nik": "720212",
		"nama": "HENGKY PERMADI",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8444444444444446,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.121052631578947,
			"nilai_komparatif_unit": 1.1248889224178884,
			"summary_team": 0.9466666666666665
		}]
	},
	"720215": {
		"nik": "720215",
		"nama": "DIATHERMAN ANGGEN",
		"band": "II",
		"posisi": "VP SALES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9244444444444446,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9615384615384616,
			"nilai_komparatif_unit": 0.9648288879532271,
			"summary_team": 0.8888888888888891
		}]
	},
	"720216": {
		"nik": "720216",
		"nama": "UUT PONCO ARI PRABOWO",
		"band": "II",
		"posisi": "GM REGIONAL BARAT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8307692307692298,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0532407407407423,
			"nilai_komparatif_unit": 1.0568449763413603,
			"summary_team": 0.8750000000000002
		}]
	},
	"720221": {
		"nik": "720221",
		"nama": "DEDY",
		"band": "II",
		"posisi": "GM HOSPITALITY BUSINESS SERVICE SEGMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8537037037037024,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9370932754880709,
			"nilai_komparatif_unit": 0.9403000494135065,
			"summary_team": 0.8
		}]
	},
	"720229": {
		"nik": "720229",
		"nama": "TEGUH HERMAN PRASONGKO",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8444444444444446,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8999999999999999,
			"nilai_komparatif_unit": 0.9030798391242204,
			"summary_team": 0.76
		}]
	},
	"720233": {
		"nik": "720233",
		"nama": "ONY WIRJAWAN",
		"band": "II",
		"posisi": "SM HUMAN CAPITAL BUSINESS PARTNER 02",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8460317460317445,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9258911819887448,
			"nilai_komparatif_unit": 0.9290596218632555,
			"summary_team": 0.7833333333333334
		}]
	},
	"720237": {
		"nik": "720237",
		"nama": "GUNAWAN EDI WIBOWO",
		"band": "II",
		"posisi": "EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8596153846153877,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9517998779743713,
			"nilai_komparatif_unit": 0.955056978532831,
			"summary_team": 0.8181818181818182
		}]
	},
	"720238": {
		"nik": "720238",
		"nama": "Suroso Yulianto, St. Mt.",
		"band": "II",
		"posisi": "VP SYSTEM PLANNING & MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8555555555555557,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9851576994434138,
			"nilai_komparatif_unit": 0.9885289519170504,
			"summary_team": 0.8428571428571431
		}]
	},
	"720240": {
		"nik": "720240",
		"nama": "DUMOLI HAPOSAN MANUARANGSIRAIT",
		"band": "II",
		"posisi": "TRIBE LEADER OF LOGISTIC TRANSPORTATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8555555555555553,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9662337662337666,
			"nilai_komparatif_unit": 0.9695402601853106,
			"summary_team": 0.8266666666666667
		}]
	},
	"720246": {
		"nik": "720246",
		"nama": "RINDO FITEGA",
		"band": "II",
		"posisi": "AVP ENTERPRISE MARKETING MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8133333333333318,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0327868852459037,
			"nilai_komparatif_unit": 1.0363211268638617,
			"summary_team": 0.8400000000000001
		}]
	},
	"720251": {
		"nik": "720251",
		"nama": "RADEN BAGUS PRABANGKARA",
		"band": "II",
		"posisi": "SM SERVICE PLATFORM DEVELOPMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8666666666666657,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9807692307692321,
			"nilai_komparatif_unit": 0.9841254657122929,
			"summary_team": 0.8500000000000002
		}]
	},
	"720253": {
		"nik": "720253",
		"nama": "Mochamad Riza Rosadi",
		"band": "II",
		"posisi": "VP SHARED SERVICE HCM",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8111111111111113,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9863013698630133,
			"nilai_komparatif_unit": 0.9896765360265426,
			"summary_team": 0.7999999999999999
		}]
	},
	"720263": {
		"nik": "720263",
		"nama": "YUDI WIJAYANTO",
		"band": "II",
		"posisi": "SM ENTERPRISE & BUSINESS BILLING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.839285714285713,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9690780141843989,
			"nilai_komparatif_unit": 0.9723942412760733,
			"summary_team": 0.8133333333333335
		}]
	},
	"720268": {
		"nik": "720268",
		"nama": "Sugeng Sugiarto",
		"band": "II",
		"posisi": "VP MINING, MARITIME & AVIATION SERVICE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8694444444444439,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0121405750798729,
			"nilai_komparatif_unit": 1.0156041641269196,
			"summary_team": 0.88
		}]
	},
	"720270": {
		"nik": "720270",
		"nama": "I GEDE ASTAWA",
		"band": "II",
		"posisi": "SM INFRASTRUCTURE ASSURANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8648148148148137,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9250535331905794,
			"nilai_komparatif_unit": 0.9282191065944891,
			"summary_team": 0.8
		}]
	},
	"720280": {
		"nik": "720280",
		"nama": "ANTON WIDARYANTO, ST",
		"band": "II",
		"posisi": "SM CORE & IP SERVICE PLANNING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8577777777777765,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9637305699481878,
			"nilai_komparatif_unit": 0.9670284978532253,
			"summary_team": 0.8266666666666665
		}]
	},
	"720284": {
		"nik": "720284",
		"nama": "RONNY RAHMAT HIDAYAT, ST. MT",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8460317460317445,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0100631076240851,
			"nilai_komparatif_unit": 1.0135195874871878,
			"summary_team": 0.8545454545454546
		}]
	},
	"720285": {
		"nik": "720285",
		"nama": "EKO PRIHANANTO, ST.",
		"band": "II",
		"posisi": "TRIBE LEADER OF LOGISTIC CONTROL TOWER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8555555555555553,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.05974025974026,
			"nilai_komparatif_unit": 1.0633667369774376,
			"summary_team": 0.9066666666666666
		}]
	},
	"720286": {
		"nik": "720286",
		"nama": "KONANG PRIHANDOKO",
		"band": "II",
		"posisi": "GM ENTERPRISE SERVICE 1",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8562770562770574,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0218655207280067,
			"nilai_komparatif_unit": 1.025362388961818,
			"summary_team": 0.875
		}]
	},
	"720287": {
		"nik": "720287",
		"nama": "NOVRIWAN",
		"band": "II",
		"posisi": "AVP PLAN,GOVERNANCE,MONITOR&REPORT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8352941176470572,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9896713615023495,
			"nilai_komparatif_unit": 0.9930580599237667,
			"summary_team": 0.8266666666666668
		}]
	},
	"720294": {
		"nik": "720294",
		"nama": "I MADE ARYA SUDIARTANA",
		"band": "II",
		"posisi": "OSM FULFILLMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8555555555555552,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0402597402597409,
			"nilai_komparatif_unit": 1.0438195543124114,
			"summary_team": 0.8900000000000001
		}]
	},
	"720297": {
		"nik": "720297",
		"nama": "ARIS DWI BASUKI , ST.",
		"band": "II",
		"posisi": "PRINCIPAL EXPERT SOLUTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0463286713286657,
			"nilai_komparatif_unit": 1.0499092535272787,
			"summary_team": 0.8727272727272727
		}]
	},
	"720300": {
		"nik": "720300",
		"nama": "AHMAD ROSADI DJARKASIH",
		"band": "II",
		"posisi": "TRIBE LEADER OF COMMUNICATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8311111111111102,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.930481283422461,
			"nilai_komparatif_unit": 0.9336654308236159,
			"summary_team": 0.7733333333333334
		}]
	},
	"720327": {
		"nik": "720327",
		"nama": "GURUH ADHI LAKSANA",
		"band": "II",
		"posisi": "OSM GS PROJECT OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9775795118343144,
			"nilai_komparatif_unit": 0.9809248314205185,
			"summary_team": 0.8153846153846154
		}]
	},
	"720332": {
		"nik": "720332",
		"nama": "ARIWIATI",
		"band": "II",
		"posisi": "VP ENTERPRISE BUSINESS ORCHESTRATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0503189227498186,
			"nilai_komparatif_unit": 1.0539131597622564,
			"summary_team": 0.8666666666666667
		}]
	},
	"720338": {
		"nik": "720338",
		"nama": "ANDI SYAHRUDDIN",
		"band": "II",
		"posisi": "EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8394557823129278,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9529983792544541,
			"nilai_komparatif_unit": 0.9562595811363948,
			"summary_team": 0.8
		}]
	},
	"720352": {
		"nik": "720352",
		"nama": "SALEH ABDURACHMAN",
		"band": "II",
		"posisi": "GM ESS ENERGY & RESOURCES SERVICES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8596153846153877,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9306487695749408,
			"nilai_komparatif_unit": 0.9338334901209904,
			"summary_team": 0.8
		}]
	},
	"720353": {
		"nik": "720353",
		"nama": "ARIS BACHTIAR",
		"band": "II",
		"posisi": "HEAD OF CHAPTER DESIGNER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8496296296296325,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0200523103748875,
			"nilai_komparatif_unit": 1.0235429737240476,
			"summary_team": 0.8666666666666666
		}]
	},
	"720354": {
		"nik": "720354",
		"nama": "SIGIT HARTONO",
		"band": "II",
		"posisi": "OSM BACKBONE, DATA CENTER &CME OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8535947712418291,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9840735068912723,
			"nilai_komparatif_unit": 0.9874410492108641,
			"summary_team": 0.8400000000000001
		}]
	},
	"720366": {
		"nik": "720366",
		"nama": "AGUNG KERTIOSO",
		"band": "II",
		"posisi": "AVP SEKRETARIAT DIREKTORAT NITS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8377192982456159,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0231862378459213,
			"nilai_komparatif_unit": 1.0266876256311235,
			"summary_team": 0.8571428571428571
		}]
	},
	"720389": {
		"nik": "720389",
		"nama": "AHMAD JUNAEDI",
		"band": "II",
		"posisi": "GM ESS AGRICULTURE & FORESTRY SERVICES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8596153846153877,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9151379567486917,
			"nilai_komparatif_unit": 0.9182695986189737,
			"summary_team": 0.7866666666666666
		}]
	},
	"720442": {
		"nik": "720442",
		"nama": "Hendra Gunawan",
		"band": "II",
		"posisi": "VP OTHER LISENCE OPERATOR SERVICE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8694444444444439,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9776357827476044,
			"nilai_komparatif_unit": 0.9809812948953199,
			"summary_team": 0.85
		}]
	},
	"720452": {
		"nik": "720452",
		"nama": "EKO HERDOYO FEBRI IRIANTO",
		"band": "II",
		"posisi": "SM PROJECT PERFORMANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8714285714285706,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9792349726775966,
			"nilai_komparatif_unit": 0.9825859573227715,
			"summary_team": 0.8533333333333334
		}]
	},
	"720453": {
		"nik": "720453",
		"nama": "DEWI SIMATUPANG",
		"band": "II",
		"posisi": "AVP REPORTING & COMPLIANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8888888888888893,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0499999999999996,
			"nilai_komparatif_unit": 1.0535931456449235,
			"summary_team": 0.9333333333333333
		}]
	},
	"720462": {
		"nik": "720462",
		"nama": "ROLAN ERICK MARASAL, ST",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9155375874125825,
			"nilai_komparatif_unit": 0.9186705968363689,
			"summary_team": 0.7636363636363636
		}]
	},
	"720471": {
		"nik": "720471",
		"nama": "FOPO RAHMAT FAUZI",
		"band": "II",
		"posisi": "SM IT PLANNING & ARCHITECTURE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8666666666666657,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9807692307692318,
			"nilai_komparatif_unit": 0.9841254657122926,
			"summary_team": 0.85
		}]
	},
	"720481": {
		"nik": "720481",
		"nama": "HIDAYAT, ST",
		"band": "II",
		"posisi": "SM HUMAN CAPITAL BUSINESS PARTNER 04",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8460317460317445,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9613508442776755,
			"nilai_komparatif_unit": 0.9646406286580186,
			"summary_team": 0.8133333333333335
		}]
	},
	"720486": {
		"nik": "720486",
		"nama": "ANDRI HERAWAN SASOKO",
		"band": "II",
		"posisi": "AVP NEWS & INFORMATION MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8699999999999994,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9482758620689662,
			"nilai_komparatif_unit": 0.9515209032918038,
			"summary_team": 0.8250000000000001
		}]
	},
	"720513": {
		"nik": "720513",
		"nama": "IWAN SETIADINATA, S.T, M.M",
		"band": "II",
		"posisi": "SM VERTICAL ECOSYSTEM & DIGITALIZATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8311111111111102,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144395,
			"nilai_komparatif_unit": 1.0302515098743346,
			"summary_team": 0.8533333333333333
		}]
	},
	"720521": {
		"nik": "720521",
		"nama": "AHMAD SYAFRI, ST",
		"band": "II",
		"posisi": "AVP AUDIT PARTNER 3",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8690476190476184,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0017727639000813,
			"nilai_komparatif_unit": 1.0052008738465679,
			"summary_team": 0.8705882352941177
		}]
	},
	"720523": {
		"nik": "720523",
		"nama": "DANDIT HARDIARTO",
		"band": "II",
		"posisi": "SVP CORPORATE STRATEGY & TECHN PLANNING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8851851851851847,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.054393305439331,
			"nilai_komparatif_unit": 1.0580014851664514,
			"summary_team": 0.9333333333333333
		}]
	},
	"720530": {
		"nik": "720530",
		"nama": "MURYANI",
		"band": "II",
		"posisi": "VP DATA CENTER SOLUTION MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.854545454545454,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9499374217772222,
			"nilai_komparatif_unit": 0.9531881489296118,
			"summary_team": 0.811764705882353
		}]
	},
	"720538": {
		"nik": "720538",
		"nama": "BUDI WAHYUDI",
		"band": "II",
		"posisi": "SM GENERAL AFFAIR",
		"category": [{
			"code": "DI-4-01",
			"kriteria": 0.8592592592592588,
			"kriteria_bp": 0.9949531890435437,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9775862068965522,
			"nilai_komparatif_unit": 0.9825449253912272,
			"summary_team": 0.84
		}]
	},
	"720539": {
		"nik": "720539",
		"nama": "EDWIN PURWANDESI",
		"band": "II",
		"posisi": "SM DIGITAL INFRASTRUCTURE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8755555555555548,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9898477157360415,
			"nilai_komparatif_unit": 0.9932350176493128,
			"summary_team": 0.8666666666666666
		}]
	},
	"720542": {
		"nik": "720542",
		"nama": "JANUAR SETYO WIDODO",
		"band": "II",
		"posisi": "STRATEGIC INV DIG TELCO PROJ TEAM LEAD 1",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.88235294117647,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9444444444444451,
			"nilai_komparatif_unit": 0.9476763743896147,
			"summary_team": 0.8333333333333334
		}]
	},
	"720543": {
		"nik": "720543",
		"nama": "DJUFRI ARDIAN",
		"band": "II",
		"posisi": "TRIBE LEADER OF EDUCATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8222222222222223,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9903474903474904,
			"nilai_komparatif_unit": 0.9937365025112079,
			"summary_team": 0.8142857142857144
		}]
	},
	"720552": {
		"nik": "720552",
		"nama": "REYNALDO MARGANDA P P",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9111111111111112,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.034843205574913,
			"nilai_komparatif_unit": 1.038384484010428,
			"summary_team": 0.9428571428571431
		}]
	},
	"720570": {
		"nik": "720570",
		"nama": "YANTO SETIAWAN",
		"band": "II",
		"posisi": "GM CENTRAL GOVERNMENT SERVICES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8634920634920623,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9727941176470601,
			"nilai_komparatif_unit": 0.9761230614063279,
			"summary_team": 0.84
		}]
	},
	"720589": {
		"nik": "720589",
		"nama": "MUHAMAD DONI KURNIAWAN",
		"band": "II",
		"posisi": "VP SOLUTION DEVELOPMENT & PROVISIONING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8555555555555557,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9194805194805193,
			"nilai_komparatif_unit": 0.9226270217892468,
			"summary_team": 0.7866666666666667
		}]
	},
	"720603": {
		"nik": "720603",
		"nama": "DODY DJUNAEDI PRIATNA",
		"band": "II",
		"posisi": "AVP DIGITAL BUSINESS SCALE-UP&MARKETING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8749999999999996,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8990476190476194,
			"nilai_komparatif_unit": 0.9021241990828196,
			"summary_team": 0.7866666666666666
		}]
	},
	"725496": {
		"nik": "725496",
		"nama": "DEDI HERMANSYAH",
		"band": "II",
		"posisi": "PRINCIPAL TRIBE INNOVATOR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.749999999999999,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.066666666666668,
			"nilai_komparatif_unit": 1.070316846369448,
			"summary_team": 0.7999999999999999
		}]
	},
	"730042": {
		"nik": "730042",
		"nama": "IRENA ALDANITUTI",
		"band": "II",
		"posisi": "DIREKTUR BUSINESS & MARKETING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8851851851851847,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9683203825463244,
			"nilai_komparatif_unit": 0.9716340169895981,
			"summary_team": 0.8571428571428571
		}]
	},
	"730047": {
		"nik": "730047",
		"nama": "I GUSTI AGUNG AYU TRIANA DEWI, ST, MAIT",
		"band": "II",
		"posisi": "AVP INFRASTRUCTURE & SERVICE PERFORMANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9333333333333333,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0714285714285716,
			"nilai_komparatif_unit": 1.075095046576453,
			"summary_team": 1.0000000000000002
		}]
	},
	"730052": {
		"nik": "730052",
		"nama": "HENRI SETIAWAN WYATNO",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.749999999999999,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.03529411764706,
			"nilai_komparatif_unit": 1.0388369391232875,
			"summary_team": 0.776470588235294
		}]
	},
	"730058": {
		"nik": "730058",
		"nama": "JENNY VERDIANA DYAH VITRIANY",
		"band": "II",
		"posisi": "AVP STRATEGIC PLAN&SUPPORT&PDI DIG TELCO",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.88235294117647,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0086666666666675,
			"nilai_komparatif_unit": 1.0121183678481087,
			"summary_team": 0.8900000000000002
		}]
	},
	"730062": {
		"nik": "730062",
		"nama": "NOVI ANDRIYANI",
		"band": "II",
		"posisi": "GM BUSINESS SOLUTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8782608695652168,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0046592894583586,
			"nilai_komparatif_unit": 1.0080972772207868,
			"summary_team": 0.8823529411764708
		}]
	},
	"730063": {
		"nik": "730063",
		"nama": "MADE WISNU SATRYA WIBAWA D, ST",
		"band": "II",
		"posisi": "SM BACKBONE, DATA CENTER & CME PLANNING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8577777777777765,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9637305699481878,
			"nilai_komparatif_unit": 0.9670284978532253,
			"summary_team": 0.8266666666666665
		}]
	},
	"730071": {
		"nik": "730071",
		"nama": "ABDUL HAMID ARROZI, MM",
		"band": "II",
		"posisi": "SM ASSET OPERATION & MONITORING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8703703703703696,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9651063829787244,
			"nilai_komparatif_unit": 0.9684090189757608,
			"summary_team": 0.8400000000000001
		}]
	},
	"730074": {
		"nik": "730074",
		"nama": "PRASASTA ARISANTI",
		"band": "II",
		"posisi": "SM GENERAL SUPPORT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9271634615384567,
			"nilai_komparatif_unit": 0.9303362552088943,
			"summary_team": 0.7733333333333334
		}]
	},
	"730078": {
		"nik": "730078",
		"nama": "EMY NETTY ONDYAWATI",
		"band": "II",
		"posisi": "VP MARKETING MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9222222222222219,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0843373493975905,
			"nilai_komparatif_unit": 1.0880479989448442,
			"summary_team": 0.9999999999999999
		}]
	},
	"730087": {
		"nik": "730087",
		"nama": "TB DICKY ANFIADI",
		"band": "II",
		"posisi": "DIREKTUR UTAMA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0441040178814767,
			"nilai_komparatif_unit": 1.0476769872192846,
			"summary_team": 0.8615384615384617
		}]
	},
	"730102": {
		"nik": "730102",
		"nama": "MOHAMAD RAKHTOMO",
		"band": "II",
		"posisi": "EVP ENTERPRISE BUSINESS INTEGRATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8799999999999996,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9740259740259747,
			"nilai_komparatif_unit": 0.9773591332513215,
			"summary_team": 0.8571428571428573
		}]
	},
	"730112": {
		"nik": "730112",
		"nama": "Mochamad Yazid Saktiono, St",
		"band": "II",
		"posisi": "VP ASSET & PROCUREMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8444444444444442,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9789473684210529,
			"nilai_komparatif_unit": 0.9822973688719594,
			"summary_team": 0.8266666666666667
		}]
	},
	"730136": {
		"nik": "730136",
		"nama": "ANDAR JAN PIETER H MANURUNG, M.T",
		"band": "II",
		"posisi": "AVP CONS BIZ PORTFOLIO ORCHESTRATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8222222222222213,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1189189189189201,
			"nilai_komparatif_unit": 1.1227479081003835,
			"summary_team": 0.9199999999999999
		}]
	},
	"730143": {
		"nik": "730143",
		"nama": "ZAMZAMI, ST",
		"band": "II",
		"posisi": "GM ESS FOREIGN&NATIONAL BANKING SERVICES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8596153846153877,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.070246085011182,
			"nilai_komparatif_unit": 1.0739085136391389,
			"summary_team": 0.9199999999999999
		}]
	},
	"730159": {
		"nik": "730159",
		"nama": "MILONO WAHYU WIBOWO, R.",
		"band": "II",
		"posisi": "EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.7863636363636347,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9248554913294816,
			"nilai_komparatif_unit": 0.9280203870255336,
			"summary_team": 0.7272727272727272
		}]
	},
	"730160": {
		"nik": "730160",
		"nama": "WAWAN ISKANDAR M.S.M",
		"band": "II",
		"posisi": "PRINCIPAL EXPERT DIGITAL MASTERY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.749999999999999,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0666666666666678,
			"nilai_komparatif_unit": 1.0703168463694477,
			"summary_team": 0.7999999999999998
		}]
	},
	"730161": {
		"nik": "730161",
		"nama": "FREDDY BAIN",
		"band": "II",
		"posisi": "AVP WS & MOBILE BIZ PORT ORCHESTRATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8222222222222213,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.033783783783785,
			"nilai_komparatif_unit": 1.0373214368318762,
			"summary_team": 0.8500000000000001
		}]
	},
	"730164": {
		"nik": "730164",
		"nama": "ESTU LESTARI DWIANTI",
		"band": "II",
		"posisi": "AVP GROUP ENTERPRISE STRATEGY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8520833333333323,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0092909535452337,
			"nilai_komparatif_unit": 1.012744791063512,
			"summary_team": 0.8600000000000002
		}]
	},
	"730167": {
		"nik": "730167",
		"nama": "SALEH DWI MARDIYANTO, S.T, M.T.",
		"band": "II",
		"posisi": "SENIOR STAFF II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8460317460317445,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.859628176701349,
			"nilai_komparatif_unit": 0.8625698616912235,
			"summary_team": 0.7272727272727272
		}]
	},
	"730168": {
		"nik": "730168",
		"nama": "RENDRO KASWORO",
		"band": "II",
		"posisi": "GM ESS MANUFACTURING MGT SERVICES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8596153846153877,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.039224459358684,
			"nilai_komparatif_unit": 1.0427807306351058,
			"summary_team": 0.8933333333333334
		}]
	},
	"730195": {
		"nik": "730195",
		"nama": "Benny Susatyo",
		"band": "II",
		"posisi": "GM DIGITAL ADVERTISING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0250000000000001,
			"nilai_komparatif_unit": 1.0285075945581401,
			"summary_team": 0.9111111111111113
		}]
	},
	"730204": {
		"nik": "730204",
		"nama": "AGUS SETYAWAN",
		"band": "II",
		"posisi": "OSM ES PROJECT OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.991105769230764,
			"nilai_komparatif_unit": 0.9944973762577834,
			"summary_team": 0.8266666666666667
		}]
	},
	"730205": {
		"nik": "730205",
		"nama": "WAHYU SAMARI",
		"band": "II",
		"posisi": "SM BROADBAND NODE DEPLOYMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8577777777777765,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9326424870466334,
			"nilai_komparatif_unit": 0.9358340301805407,
			"summary_team": 0.7999999999999999
		}]
	},
	"730213": {
		"nik": "730213",
		"nama": "DANIEL SIHOMBING,ST",
		"band": "II",
		"posisi": "SM GENERAL AFFAIR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8733333333333327,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9351145038167945,
			"nilai_komparatif_unit": 0.9383145062995512,
			"summary_team": 0.8166666666666667
		}]
	},
	"730214": {
		"nik": "730214",
		"nama": "TOTO RUDIARTO",
		"band": "II",
		"posisi": "DEPUTY EGM DIVISI SOLUTION, DELIVERY&ASS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9751201923076873,
			"nilai_komparatif_unit": 0.9784570959955613,
			"summary_team": 0.8133333333333335
		}]
	},
	"730241": {
		"nik": "730241",
		"nama": "IBRAHIM, MM",
		"band": "II",
		"posisi": "TRIBE LEADER OF LOGISTIC DISTRIBUTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8555555555555553,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9974025974025977,
			"nilai_komparatif_unit": 1.0008157524493528,
			"summary_team": 0.8533333333333334
		}]
	},
	"730252": {
		"nik": "730252",
		"nama": "RUBY HENDRARTO WIJOSENO",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9899999999999998,
			"nilai_komparatif_unit": 0.9933878230366423,
			"summary_team": 0.88
		}]
	},
	"730267": {
		"nik": "730267",
		"nama": "ARLIEK ARNASTOTO, ST, M.Si",
		"band": "II",
		"posisi": "GM ESS EDUCATION MANAGEMENT SERVICES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8596153846153877,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9771812080536879,
			"nilai_komparatif_unit": 0.9805251646270399,
			"summary_team": 0.8400000000000001
		}]
	},
	"730270": {
		"nik": "730270",
		"nama": "FEBRI EGASMARA, ST.MM",
		"band": "II",
		"posisi": "OSM OSS PLATFORM OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8367816091954015,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0755494505494518,
			"nilai_komparatif_unit": 1.0792300275248252,
			"summary_team": 0.9000000000000002
		}]
	},
	"730280": {
		"nik": "730280",
		"nama": "WAHYONO",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8133333333333318,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9165424739195248,
			"nilai_komparatif_unit": 0.9196789221086217,
			"summary_team": 0.7454545454545455
		}]
	},
	"730287": {
		"nik": "730287",
		"nama": "ROOSDIONO",
		"band": "II",
		"posisi": "VP SOLUTION DESIGN & PRODUCT DEVELOPMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8555555555555557,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0441558441558438,
			"nilai_komparatif_unit": 1.0477289908454157,
			"summary_team": 0.8933333333333332
		}]
	},
	"730291": {
		"nik": "730291",
		"nama": "RISMANTO",
		"band": "II",
		"posisi": "VP PUBLIC SECTOR & PROPERTY SERVICES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9222222222222219,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9542168674698797,
			"nilai_komparatif_unit": 0.9574822390714628,
			"summary_team": 0.8799999999999999
		}]
	},
	"730295": {
		"nik": "730295",
		"nama": "NUR RATIH SURYAWATI",
		"band": "II",
		"posisi": "SM ENTERPRISE MARKETING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8596153846153877,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9941020947732323,
			"nilai_komparatif_unit": 0.9975039553565125,
			"summary_team": 0.8545454545454546
		}]
	},
	"730299": {
		"nik": "730299",
		"nama": "IDA BAGUS PUTU ARIARTHA",
		"band": "II",
		"posisi": "AVP DIGITAL BUSINESS ORCHESTRATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8648148148148137,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0021413276231277,
			"nilai_komparatif_unit": 1.0055706988106965,
			"summary_team": 0.8666666666666667
		}]
	},
	"730308": {
		"nik": "730308",
		"nama": "HARDYONO MANAMPIN PASARIBU",
		"band": "II",
		"posisi": "OSM WIRELESS BROADBAND OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8535947712418291,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9254977029096488,
			"nilai_komparatif_unit": 0.9286647962816459,
			"summary_team": 0.79
		}]
	},
	"730314": {
		"nik": "730314",
		"nama": "RIDWAN MISBAH",
		"band": "II",
		"posisi": "GM ESS MARITIME & LOGISTIC SERVICES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8596153846153877,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.008202833706186,
			"nilai_komparatif_unit": 1.011652947631073,
			"summary_team": 0.8666666666666667
		}]
	},
	"730317": {
		"nik": "730317",
		"nama": "TATOK ANUNTA",
		"band": "II",
		"posisi": "SM ACCESS PLANNING",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8608695652173906,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0377104377104387,
			"nilai_komparatif_unit": 1.0412615279389639,
			"summary_team": 0.8933333333333333
		}]
	},
	"730325": {
		"nik": "730325",
		"nama": "RENCES SIAHAAN",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365351,
			"nilai_komparatif_unit": 0.9913740944073774,
			"summary_team": 0.8727272727272727
		}]
	},
	"730326": {
		"nik": "730326",
		"nama": "Fitra Akmal",
		"band": "II",
		"posisi": "VP ENTERPRISE SERVICE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8694444444444439,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.035143769968052,
			"nilai_komparatif_unit": 1.038686076947986,
			"summary_team": 0.9000000000000001
		}]
	},
	"730349": {
		"nik": "730349",
		"nama": "ZUHED NUR",
		"band": "II",
		"posisi": "TRIBE LEADER OF FINSERV",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8311111111111102,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9946524064171133,
			"nilai_komparatif_unit": 0.9980561501907615,
			"summary_team": 0.8266666666666667
		}]
	},
	"730355": {
		"nik": "730355",
		"nama": "I GUSTI BAGUS ASTAWA, M.T",
		"band": "II",
		"posisi": "OSM BIDDING LEGAL & COMPLIANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8634920634920623,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.019117647058825,
			"nilai_komparatif_unit": 1.0226051119494866,
			"summary_team": 0.88
		}]
	},
	"730363": {
		"nik": "730363",
		"nama": "ROSIHAN MUHAMMAD",
		"band": "II",
		"posisi": "GM GOVERMENT SERVICE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8562770562770574,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9791942132690348,
			"nilai_komparatif_unit": 0.9825450584337417,
			"summary_team": 0.8384615384615383
		}]
	},
	"730368": {
		"nik": "730368",
		"nama": "SETYO NUGROHO, ST",
		"band": "II",
		"posisi": "COORDINATOR PROJECT SERVICE DELIVERY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8490196078431361,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9422632794457287,
			"nilai_komparatif_unit": 0.9454877453494546,
			"summary_team": 0.8
		}]
	},
	"730376": {
		"nik": "730376",
		"nama": "ARIEF PURNOMO",
		"band": "II",
		"posisi": "EVP ENTERPRISE BUSINESS SOLUTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8799999999999996,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.961538461538462,
			"nilai_komparatif_unit": 0.9648288879532275,
			"summary_team": 0.8461538461538461
		}]
	},
	"730384": {
		"nik": "730384",
		"nama": "BINSAR",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96,
			"nilai_komparatif_unit": 0.9632851617325018,
			"summary_team": 0.8
		}]
	},
	"730417": {
		"nik": "730417",
		"nama": "ARDHI MUTIARTO",
		"band": "II",
		"posisi": "SM PROJECT ADMINISTRATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.991105769230764,
			"nilai_komparatif_unit": 0.9944973762577834,
			"summary_team": 0.8266666666666667
		}]
	},
	"730432": {
		"nik": "730432",
		"nama": "NIKITA IDDI BAYU AJI",
		"band": "II",
		"posisi": "GM GOVERNMENT AGENCY SERVICES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8634920634920623,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0036764705882366,
			"nilai_komparatif_unit": 1.0071110951017668,
			"summary_team": 0.8666666666666667
		}]
	},
	"730435": {
		"nik": "730435",
		"nama": "DENNY KRISTANTO",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.024530157342652,
			"nilai_komparatif_unit": 1.0280361440787937,
			"summary_team": 0.8545454545454546
		}]
	},
	"730478": {
		"nik": "730478",
		"nama": "SIGIT HADI PRAYOGA",
		"band": "II",
		"posisi": "TRIBE LEADER OF E-HEALTH",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.749999999999999,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0888888888888903,
			"nilai_komparatif_unit": 1.0926151140021447,
			"summary_team": 0.8166666666666667
		}]
	},
	"730481": {
		"nik": "730481",
		"nama": "Silahuddin Sumantri",
		"band": "II",
		"posisi": "VP CORPORATE FINANCE & PERFORMANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8111111111111113,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9863013698630135,
			"nilai_komparatif_unit": 0.9896765360265428,
			"summary_team": 0.8
		}]
	},
	"730484": {
		"nik": "730484",
		"nama": "H. MOHAMAD RAHMAT YUSUF,",
		"band": "II",
		"posisi": "AVP SEKRETARIAT DIGITAL BUSINESS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8648148148148137,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9973233404710934,
			"nilai_komparatif_unit": 1.0007362242971836,
			"summary_team": 0.8625
		}]
	},
	"730489": {
		"nik": "730489",
		"nama": "YOKE YUNI KARNIDA, ST, MTI",
		"band": "II",
		"posisi": "AVP DIGICO SERV STRA ALIGNMEN GOVERNANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8476190476190469,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9715796430931933,
			"nilai_komparatif_unit": 0.9749044308677429,
			"summary_team": 0.8235294117647061
		}]
	},
	"730493": {
		"nik": "730493",
		"nama": "ARIYANTO AGUS SETYAWAN",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9155375874125825,
			"nilai_komparatif_unit": 0.9186705968363689,
			"summary_team": 0.7636363636363636
		}]
	},
	"730494": {
		"nik": "730494",
		"nama": "SOSRO HUTOMO KARSOSOEMO",
		"band": "II",
		"posisi": "SENIOR STAFF II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96,
			"nilai_komparatif_unit": 0.9632851617325018,
			"summary_team": 0.8
		}]
	},
	"730497": {
		"nik": "730497",
		"nama": "FAIZAL AIDUL FITRI",
		"band": "II",
		"posisi": "SM DATACOMM PRODUCT MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8347826086956516,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8846153846153851,
			"nilai_komparatif_unit": 0.8876425769169694,
			"summary_team": 0.7384615384615384
		}]
	},
	"730509": {
		"nik": "730509",
		"nama": "FAJAR ADI NUGROHO",
		"band": "II",
		"posisi": "GM GOVERNMENT PROGRAM & OPERATIONS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8634920634920623,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.019117647058825,
			"nilai_komparatif_unit": 1.0226051119494866,
			"summary_team": 0.88
		}]
	},
	"730528": {
		"nik": "730528",
		"nama": "JUSUF HIDAJAT",
		"band": "II",
		"posisi": "SM BUSINESS PLANNING, PERFORMANCE & SUPP",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8537037037037024,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9839479392624744,
			"nilai_komparatif_unit": 0.9873150518841818,
			"summary_team": 0.84
		}]
	},
	"730537": {
		"nik": "730537",
		"nama": "TAUFIK ZAMZAMI",
		"band": "II",
		"posisi": "SYNERGY PROJ LEAD TRAVEL&TOURISM&CFU CON",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8696969696969686,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.069337979094078,
			"nilai_komparatif_unit": 1.07299730014411,
			"summary_team": 0.93
		}]
	},
	"730539": {
		"nik": "730539",
		"nama": "CANDRA KUSUMA WARDHANA, ST.",
		"band": "II",
		"posisi": "AVP ENT BIZ PORTFOLIO ORCHESTRATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8222222222222213,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0540540540540553,
			"nilai_komparatif_unit": 1.0576610728481874,
			"summary_team": 0.8666666666666667
		}]
	},
	"730540": {
		"nik": "730540",
		"nama": "TONI PARLAGUTAN HARAHAP",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8479999999999996,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9156492785793565,
			"nilai_komparatif_unit": 0.9187826702151709,
			"summary_team": 0.776470588235294
		}]
	},
	"730543": {
		"nik": "730543",
		"nama": "ANDY SIHAR HASUDUNGAN SIREGAR",
		"band": "II",
		"posisi": "OSM CYBER SECURITY OPERATION CENTER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8361111111111127,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9727574750830545,
			"nilai_komparatif_unit": 0.9760862934498754,
			"summary_team": 0.8133333333333332
		}]
	},
	"730556": {
		"nik": "730556",
		"nama": "SUTOYO, ST",
		"band": "II",
		"posisi": "EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.7863636363636347,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9248554913294817,
			"nilai_komparatif_unit": 0.9280203870255337,
			"summary_team": 0.7272727272727273
		}]
	},
	"730562": {
		"nik": "730562",
		"nama": "CHOLIS SAFRUDIN",
		"band": "II",
		"posisi": "SM QUALITY & PERFORMANCE MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8555555555555552,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181824,
			"nilai_komparatif_unit": 0.985178006317332,
			"summary_team": 0.8400000000000001
		}]
	},
	"730565": {
		"nik": "730565",
		"nama": "ACEP ARNA HIKMAT",
		"band": "II",
		"posisi": "SYNERGY PROJ LEADER ONEDATA & CFU MOBILE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8696969696969686,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0635888501742174,
			"nilai_komparatif_unit": 1.0672284974551633,
			"summary_team": 0.925
		}]
	},
	"730586": {
		"nik": "730586",
		"nama": "DODDI RAHAYU NUGROHO",
		"band": "II",
		"posisi": "AVP STRATEGIC PLAN&SUPPORT&PDI ICT&SERV",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.88235294117647,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9406666666666673,
			"nilai_komparatif_unit": 0.9438856688920563,
			"summary_team": 0.8300000000000001
		}]
	},
	"730604": {
		"nik": "730604",
		"nama": "DHINTA DARMANTORO, ST. MSCS.",
		"band": "II",
		"posisi": "SM BIG DATA PLATFORM,AI & CYBER SECURITY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8755555555555548,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9593908629441634,
			"nilai_komparatif_unit": 0.9626739401831802,
			"summary_team": 0.8400000000000001
		}]
	},
	"730613": {
		"nik": "730613",
		"nama": "BUDDY RESTIADY",
		"band": "II",
		"posisi": "AVP ENTERPRISE PERFORMANCE MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.7849462365591385,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0191780821917826,
			"nilai_komparatif_unit": 1.0226657538940962,
			"summary_team": 0.8000000000000002
		}]
	},
	"740036": {
		"nik": "740036",
		"nama": "SRI SUMARI",
		"band": "II",
		"posisi": "GM MARKETING BGES, GOV & ENTERPRISE SERV",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8782608695652168,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0171617161716178,
			"nilai_komparatif_unit": 1.0206424877817561,
			"summary_team": 0.8933333333333333
		}]
	},
	"740058": {
		"nik": "740058",
		"nama": "DIDIK ARIADI",
		"band": "II",
		"posisi": "SM PLANNING & PERFORMANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9591346153846103,
			"nilai_komparatif_unit": 0.9624168157333388,
			"summary_team": 0.8
		}]
	},
	"740059": {
		"nik": "740059",
		"nama": "ARI KURNIAWAN, ST",
		"band": "II",
		"posisi": "TRIBE LEADER OF ENT, W'SALE DIGITIZATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8311111111111102,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197872,
			"nilai_komparatif_unit": 0.9658607905071888,
			"summary_team": 0.8
		}]
	},
	"740061": {
		"nik": "740061",
		"nama": "IWAN BUDIMAN SAPUTRA, ST",
		"band": "II",
		"posisi": "AVP GROUP OF DIG BUSS GOVERNANCE&CONTROL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8749999999999996,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8990476190476194,
			"nilai_komparatif_unit": 0.9021241990828196,
			"summary_team": 0.7866666666666666
		}]
	},
	"740074": {
		"nik": "740074",
		"nama": "NUGROHO SETIO BUDI",
		"band": "II",
		"posisi": "OSM GOVT SOLUTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9751201923076873,
			"nilai_komparatif_unit": 0.9784570959955613,
			"summary_team": 0.8133333333333335
		}]
	},
	"740081": {
		"nik": "740081",
		"nama": "ANDRI QIANTORI, Ph.D.",
		"band": "II",
		"posisi": "TRIBE LEADER OF VACCINATION & PL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.749999999999999,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0666666666666682,
			"nilai_komparatif_unit": 1.0703168463694481,
			"summary_team": 0.8
		}]
	},
	"740084": {
		"nik": "740084",
		"nama": "EUGENE JOHANNES SIAGIAN",
		"band": "II",
		"posisi": "VP DC ECOSYSTEM SOLUTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.854545454545454,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0463078848560707,
			"nilai_komparatif_unit": 1.049888395922471,
			"summary_team": 0.8941176470588236
		}]
	},
	"740086": {
		"nik": "740086",
		"nama": "CHANDRA TAMRIN",
		"band": "II",
		"posisi": "VP GAMES & OTT BUSINESS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9025641025641028,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9694602272727268,
			"nilai_komparatif_unit": 0.9727777623142049,
			"summary_team": 0.8749999999999999
		}]
	},
	"740090": {
		"nik": "740090",
		"nama": "IRWAN ANDRIYANTO NUGROHO",
		"band": "II",
		"posisi": "VP ENTERPRISE BUSINESS STRATEGY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0301204819277068,
			"nilai_komparatif_unit": 1.0336455989975977,
			"summary_team": 0.8500000000000001
		}]
	},
	"740091": {
		"nik": "740091",
		"nama": "ERMONO LIMAN PRABOWO",
		"band": "II",
		"posisi": "OSM CNOP NETWORK QUALITY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8555555555555552,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9740259740259742,
			"nilai_komparatif_unit": 0.9773591332513211,
			"summary_team": 0.8333333333333331
		}]
	},
	"740128": {
		"nik": "740128",
		"nama": "DIAN RAHAYU",
		"band": "II",
		"posisi": "OSM SERVICE PLATFORM OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8666666666666657,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9638009049773766,
			"nilai_komparatif_unit": 0.9670990735719415,
			"summary_team": 0.8352941176470587
		}]
	},
	"740150": {
		"nik": "740150",
		"nama": "HERU YULIANTO",
		"band": "II",
		"posisi": "VP MARKETING & SALES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8333333333333329,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0447058823529414,
			"nilai_komparatif_unit": 1.0482809112971345,
			"summary_team": 0.8705882352941174
		}]
	},
	"740158": {
		"nik": "740158",
		"nama": "EKO RAMANDA HIDAYAT",
		"band": "II",
		"posisi": "OSM SERVICE OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0390624999999944,
			"nilai_komparatif_unit": 1.0426182170444505,
			"summary_team": 0.8666666666666666
		}]
	},
	"740171": {
		"nik": "740171",
		"nama": "RONNY K. KHRISNA AJI",
		"band": "II",
		"posisi": "SENIOR ADVISOR II",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.749999999999999,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9098039215686285,
			"nilai_komparatif_unit": 0.9129173101386466,
			"summary_team": 0.6823529411764705
		}]
	},
	"740172": {
		"nik": "740172",
		"nama": "BAMBANG MUJIONO",
		"band": "II",
		"posisi": "OSM CNOP SERVICE OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8555555555555552,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9480519480519485,
			"nilai_komparatif_unit": 0.9512962230312861,
			"summary_team": 0.8111111111111111
		}]
	},
	"740205": {
		"nik": "740205",
		"nama": "MUH. NAZARUDIN LATIEF",
		"band": "II",
		"posisi": "VP INFRSTRCTR,NTWK&SCRTY(BALICMP5)&DLVRY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9773755656108597,
			"nilai_komparatif_unit": 0.9807201872842213,
			"summary_team": 0.8470588235294119
		}]
	},
	"740215": {
		"nik": "740215",
		"nama": "HARWISNU PAMUNGKAS",
		"band": "II",
		"posisi": "TRIBE LEADER OF INDIHOME DIGITALIZATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8176100628930801,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.994769230769233,
			"nilai_komparatif_unit": 0.9981733743208927,
			"summary_team": 0.8133333333333335
		}]
	},
	"740219": {
		"nik": "740219",
		"nama": "WAHYUDI",
		"band": "II",
		"posisi": "TRIBE LEADER OF SMART CITY SOLUTION DELI",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.87037037037037,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0832826747720372,
			"nilai_komparatif_unit": 1.0869897151768741,
			"summary_team": 0.9428571428571431
		}]
	},
	"740257": {
		"nik": "740257",
		"nama": "YUSUF ARIFIN",
		"band": "II",
		"posisi": "GENERAL MANAGER IT SOLUTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9111111111111114,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0662020905923342,
			"nilai_komparatif_unit": 1.0698506804955918,
			"summary_team": 0.9714285714285715
		}]
	},
	"740275": {
		"nik": "740275",
		"nama": "UTAMI SARITIDAR",
		"band": "II",
		"posisi": "AVP IT FSA DEVELOPMENT & OPERATION STRA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8479999999999996,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.998890122086571,
			"nilai_komparatif_unit": 1.0023083675074596,
			"summary_team": 0.8470588235294119
		}]
	},
	"740282": {
		"nik": "740282",
		"nama": "RADEN CATUR HARI SUHARTO",
		"band": "II",
		"posisi": "GM LOCAL GOVERNMENT SERVICES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8634920634920623,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9419117647058838,
			"nilai_komparatif_unit": 0.9451350277108891,
			"summary_team": 0.8133333333333335
		}]
	},
	"740289": {
		"nik": "740289",
		"nama": "RIDWAN WIDIYANTO",
		"band": "II",
		"posisi": "SM WIRELESS ACCESS DEPLOYMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8608695652173906,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9650349650349658,
			"nilai_komparatif_unit": 0.9683373566366941,
			"summary_team": 0.8307692307692307
		}]
	},
	"740305": {
		"nik": "740305",
		"nama": "DADAN GUMBIRA PRAMUDIA",
		"band": "II",
		"posisi": "VP SYNERGY SERVICE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9222222222222219,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9831325301204822,
			"nilai_komparatif_unit": 0.9864968523766587,
			"summary_team": 0.9066666666666666
		}]
	},
	"740311": {
		"nik": "740311",
		"nama": "AUGUSTRIANA INDRIASARY",
		"band": "II",
		"posisi": "VP SETTLEMENT & BILLING COLLECTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8279999999999987,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9471141622171385,
			"nilai_komparatif_unit": 0.9503552280525825,
			"summary_team": 0.7842105263157895
		}]
	},
	"750046": {
		"nik": "750046",
		"nama": "INDRI SONWASKITO",
		"band": "II",
		"posisi": "AVP STRATEGY ALIGNMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8520833333333323,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9388753056234733,
			"nilai_komparatif_unit": 0.9420881777334996,
			"summary_team": 0.8000000000000003
		}]
	},
	"750074": {
		"nik": "750074",
		"nama": "LAILA FITRIANA",
		"band": "II",
		"posisi": "VP LEGAL & COMPLIANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8379487179487183,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9706242350061197,
			"nilai_komparatif_unit": 0.9739457533326624,
			"summary_team": 0.8133333333333335
		}]
	},
	"750076": {
		"nik": "750076",
		"nama": "ADRIAN SANI HARAHAP",
		"band": "II",
		"posisi": "GM MANUFACTURE BUSINESS SERVICE SEGMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8537037037037024,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0464208242950124,
			"nilai_komparatif_unit": 1.0500017218450821,
			"summary_team": 0.8933333333333333
		}]
	},
	"750081": {
		"nik": "750081",
		"nama": "JOVE MELDI PRIYATAMA SINAGA",
		"band": "II",
		"posisi": "VP AUDIT & RISK MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9111111111111114,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9740853658536581,
			"nilai_komparatif_unit": 0.9774187283204212,
			"summary_team": 0.8875
		}]
	},
	"760042": {
		"nik": "760042",
		"nama": "FUSHENG MANRE, ST",
		"band": "II",
		"posisi": "PRINCIPAL EXPERT CORE NETWORK SYSTEM",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8535947712418291,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0348392036753455,
			"nilai_komparatif_unit": 1.0383804684161861,
			"summary_team": 0.8833333333333333
		}]
	},
	"760059": {
		"nik": "760059",
		"nama": "IMAN PRAMUDITO HANGGORO EDHI",
		"band": "II",
		"posisi": "GM ESS RETAIL & DISTRIBUTION SERVICES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8596153846153877,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9926920208799368,
			"nilai_komparatif_unit": 0.9960890561290563,
			"summary_team": 0.8533333333333333
		}]
	},
	"770026": {
		"nik": "770026",
		"nama": "Hari Usmayadi",
		"band": "II",
		"posisi": "VP GOVERNMENT & REGIONAL SERVICES",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8694444444444439,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0063897763578282,
			"nilai_komparatif_unit": 1.009833685921653,
			"summary_team": 0.8750000000000001
		}]
	},
	"770031": {
		"nik": "770031",
		"nama": "Sigit Adi Pramono",
		"band": "II",
		"posisi": "DIREKTUR",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9333333333333333,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0255102040816328,
			"nilai_komparatif_unit": 1.0290195445803194,
			"summary_team": 0.9571428571428573
		}]
	},
	"770034": {
		"nik": "770034",
		"nama": "TANTANG YUDHA SANTOSO",
		"band": "II",
		"posisi": "DIREKTUR FINANCE & BUSINESS DEVELOPMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9111111111111112,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0034843205574915,
			"nilai_komparatif_unit": 1.0069182875252634,
			"summary_team": 0.9142857142857145
		}]
	},
	"770052": {
		"nik": "770052",
		"nama": "HERNADI YOGA ADHITYA TAMA",
		"band": "II",
		"posisi": "GM E-COMMERCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8307692307692298,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9509259259259271,
			"nilai_komparatif_unit": 0.9541800357824851,
			"summary_team": 0.79
		}]
	},
	"770068": {
		"nik": "770068",
		"nama": "SELLY ROSALINE",
		"band": "II",
		"posisi": "TRIBE LEADER OF MYINDIHOME X",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8176100628930801,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.060000000000002,
			"nilai_komparatif_unit": 1.0636273660796396,
			"summary_team": 0.8666666666666667
		}]
	},
	"780026": {
		"nik": "780026",
		"nama": "KUSUMOAJI SRI HARYOTO",
		"band": "II",
		"posisi": "OSM SERVICE ASSURANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9591346153846103,
			"nilai_komparatif_unit": 0.9624168157333388,
			"summary_team": 0.8
		}]
	},
	"780028": {
		"nik": "780028",
		"nama": "IRWAN INDRIASTANTO, ST.",
		"band": "II",
		"posisi": "SM WIRELESS PRODUCT MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8347826086956516,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.038194444444445,
			"nilai_komparatif_unit": 1.0417471909650544,
			"summary_team": 0.8666666666666666
		}]
	},
	"780034": {
		"nik": "780034",
		"nama": "RADEN ADJI SAKTIONO BOETORO",
		"band": "II",
		"posisi": "OSM BSS & CEM PLATFORM OPERATION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8367816091954015,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.97010342598578,
			"nilai_komparatif_unit": 0.9734231620812148,
			"summary_team": 0.811764705882353
		}]
	},
	"780037": {
		"nik": "780037",
		"nama": "RINI FITRIANI",
		"band": "II",
		"posisi": "VP FINANCIAL ACCOUNTING & ASSET MGT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8279999999999987,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0514350667803372,
			"nilai_komparatif_unit": 1.0550331232861676,
			"summary_team": 0.8705882352941178
		}]
	},
	"780043": {
		"nik": "780043",
		"nama": "JOHAN EKO PRASETIO",
		"band": "II",
		"posisi": "AVP CORE NETWORK & TRANSPORT DEV STRA",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8688888888888879,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9207161125319705,
			"nilai_komparatif_unit": 0.9238668430938329,
			"summary_team": 0.8000000000000002
		}]
	},
	"780048": {
		"nik": "780048",
		"nama": "ABDUL GHONY",
		"band": "II",
		"posisi": "AVP GROUP OF DIG BUSS PRFRM MSRMNT&ANLYS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8749999999999996,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0514285714285718,
			"nilai_komparatif_unit": 1.0550266057070263,
			"summary_team": 0.9199999999999999
		}]
	},
	"780057": {
		"nik": "780057",
		"nama": "CAROLUS BORROMEUS WIDIYATMOKO",
		"band": "II",
		"posisi": "AVP ENTERPRISE COMPLIANCE & CHANGE MGMT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8133333333333318,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0491803278688545,
			"nilai_komparatif_unit": 1.0527706685601135,
			"summary_team": 0.8533333333333334
		}]
	},
	"780072": {
		"nik": "780072",
		"nama": "SAMUEL MAY RATIFIL",
		"band": "II",
		"posisi": "VP DIGITAL BUSINESS DEVELOPMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.9025641025641028,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9971590909090906,
			"nilai_komparatif_unit": 1.0005714126660394,
			"summary_team": 0.8999999999999999
		}]
	},
	"790051": {
		"nik": "790051",
		"nama": "PAULUS CAHYO WIDHIATMOKO",
		"band": "II",
		"posisi": "OSM BIDDING LEGAL & COMPLIANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8596153846153877,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0237136465324348,
			"nilai_komparatif_unit": 1.0272168391330894,
			"summary_team": 0.88
		}]
	},
	"790074": {
		"nik": "790074",
		"nama": "R. RIFA HERDIAN",
		"band": "II",
		"posisi": "EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8222222222222225,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9729729729729727,
			"nilai_komparatif_unit": 0.9763025287829408,
			"summary_team": 0.8
		}]
	},
	"790078": {
		"nik": "790078",
		"nama": "ROSNAWATI SIREGAR",
		"band": "II",
		"posisi": "OSM BUSINESS BIDDING AND COLLECTION",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8537037037037024,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9683297180043399,
			"nilai_komparatif_unit": 0.9716433843939567,
			"summary_team": 0.8266666666666667
		}]
	},
	"790098": {
		"nik": "790098",
		"nama": "Fino Arfiantono",
		"band": "II",
		"posisi": "VP CORPORATE SECRETARY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8388888888888881,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8855250709555355,
			"nilai_komparatif_unit": 0.888555376243321,
			"summary_team": 0.742857142857143
		}]
	},
	"790116": {
		"nik": "790116",
		"nama": "JAUHAR ANWARI",
		"band": "II",
		"posisi": "AVP ENTERPRISE RESOURCE MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.7849462365591385,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0616438356164402,
			"nilai_komparatif_unit": 1.0652768269730168,
			"summary_team": 0.8333333333333335
		}]
	},
	"800007": {
		"nik": "800007",
		"nama": "AWALLUDIN",
		"band": "II",
		"posisi": "HEAD OF CHAPTER DATA SCIENTIST",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8496296296296325,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9857236268526558,
			"nilai_komparatif_unit": 0.9890968159544884,
			"summary_team": 0.8375
		}]
	},
	"800042": {
		"nik": "800042",
		"nama": "FAJAR ARIEF NUGRAHA, ST.",
		"band": "II",
		"posisi": "HEAD OF CHAPTER DEVELOPER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8496296296296325,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9415867480383578,
			"nilai_komparatif_unit": 0.944808898822198,
			"summary_team": 0.8
		}]
	},
	"800048": {
		"nik": "800048",
		"nama": "JOKOADI WIBOWO",
		"band": "II",
		"posisi": "SM TRIBE MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8496296296296325,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0514385353094995,
			"nilai_komparatif_unit": 1.0550366036847876,
			"summary_team": 0.8933333333333333
		}]
	},
	"800056": {
		"nik": "800056",
		"nama": "MELANY ULFA",
		"band": "II",
		"posisi": "EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8583333333333338,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9320388349514558,
			"nilai_komparatif_unit": 0.9352283123616517,
			"summary_team": 0.8
		}]
	},
	"800108": {
		"nik": "800108",
		"nama": "RADEN BAYU HARTOKO",
		"band": "II",
		"posisi": "SYNERGY PROJECT LEADER HEALTHCARE & CFUE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8696969696969686,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9709639953542406,
			"nilai_komparatif_unit": 0.9742866763554644,
			"summary_team": 0.8444444444444446
		}]
	},
	"810073": {
		"nik": "810073",
		"nama": "LIA SOVIA EKAWATI",
		"band": "II",
		"posisi": "COORDINATOR GROUP OF CX DESIGN&INTEGRATN",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8176100628930801,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9784615384615405,
			"nilai_komparatif_unit": 0.9818098763812059,
			"summary_team": 0.8
		}]
	},
	"810076": {
		"nik": "810076",
		"nama": "MAULIZA",
		"band": "II",
		"posisi": "OSM ASSURANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8555555555555552,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9701298701298706,
			"nilai_komparatif_unit": 0.973449696718316,
			"summary_team": 0.8300000000000001
		}]
	},
	"810082": {
		"nik": "810082",
		"nama": "WIWEKA REKSA WHUANA",
		"band": "II",
		"posisi": "VP CORPORATE FINANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8279999999999987,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9803921568627467,
			"nilai_komparatif_unit": 0.9837471014425077,
			"summary_team": 0.811764705882353
		}]
	},
	"810093": {
		"nik": "810093",
		"nama": "RACKA RADITYA ROEMANTO",
		"band": "II",
		"posisi": "VP STRATEGIC PLANNING & INVESTMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8562770562770574,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0393832153690585,
			"nilai_komparatif_unit": 1.0429400299154494,
			"summary_team": 0.8900000000000001
		}]
	},
	"810107": {
		"nik": "810107",
		"nama": "RULLY JAUVAN SAGALA",
		"band": "II",
		"posisi": "EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.7863636363636347,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9248554913294816,
			"nilai_komparatif_unit": 0.9280203870255336,
			"summary_team": 0.7272727272727272
		}]
	},
	"815041": {
		"nik": "815041",
		"nama": "ANGGA PRASETHYA HARIMURTI",
		"band": "II",
		"posisi": "STRATEGIC INV ICT&SERV PROJ TEAM LEAD 1",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.88235294117647,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9671111111111118,
			"nilai_komparatif_unit": 0.9704206073749655,
			"summary_team": 0.8533333333333334
		}]
	},
	"820032": {
		"nik": "820032",
		"nama": "MOCHAMAD TEEZAR DWIPUTRA",
		"band": "II",
		"posisi": "EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8878048780487822,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9010989010988995,
			"nilai_komparatif_unit": 0.9041825007104511,
			"summary_team": 0.8
		}]
	},
	"820059": {
		"nik": "820059",
		"nama": "ARIFIN SETIAWAN",
		"band": "II",
		"posisi": "SM BUSINESS PLANNING & PERFORMANCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8490196078431361,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0207852193995395,
			"nilai_komparatif_unit": 1.0242783907952424,
			"summary_team": 0.8666666666666666
		}]
	},
	"820068": {
		"nik": "820068",
		"nama": "I KETUT ALIT ATMAJA",
		"band": "II",
		"posisi": "VP CARRIER ENTERPRISE SALES REGION I",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8313725490196077,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9715166908563138,
			"nilai_komparatif_unit": 0.9748412632055721,
			"summary_team": 0.8076923076923077
		}]
	},
	"820076": {
		"nik": "820076",
		"nama": "DICKE ADHITYA RUSTIADI",
		"band": "II",
		"posisi": "EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8333333333333334,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0472727272727271,
			"nilai_komparatif_unit": 1.0508565400718202,
			"summary_team": 0.8727272727272727
		}]
	},
	"820085": {
		"nik": "820085",
		"nama": "REINE DEBORAH ELIZABETH",
		"band": "II",
		"posisi": "VP PORTFOLIO MGMT, CHANNEL &PARTNERSHIP",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8799999999999996,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9893048128342252,
			"nilai_komparatif_unit": 0.9926902569101658,
			"summary_team": 0.8705882352941178
		}]
	},
	"830033": {
		"nik": "830033",
		"nama": "WITA ASTARI",
		"band": "II",
		"posisi": "VP CLOUD SOLUTION DELIVERY",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.854545454545454,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9712765957446817,
			"nilai_komparatif_unit": 0.9746003464780308,
			"summary_team": 0.8300000000000003
		}]
	},
	"840132": {
		"nik": "840132",
		"nama": "BERNADETTA RARAS INDAH ROSARI",
		"band": "II",
		"posisi": "SYNERGY PROJ LEADER AGRICULTURE&CFU WIBS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8696969696969686,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9837398373983752,
			"nilai_komparatif_unit": 0.9871062378864572,
			"summary_team": 0.8555555555555556
		}]
	},
	"850080": {
		"nik": "850080",
		"nama": "DEDY EDWARD",
		"band": "II",
		"posisi": "VP FINANCE & HUMAN CAPITAL",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8333333333333329,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0164705882352945,
			"nilai_komparatif_unit": 1.0199489947755906,
			"summary_team": 0.8470588235294116
		}]
	},
	"850134": {
		"nik": "850134",
		"nama": "AKHMAD DENIAR PERDANA KUSUMA",
		"band": "II",
		"posisi": "VP PRODUCT MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8637037037037028,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9744854202401383,
			"nilai_komparatif_unit": 0.9778201517104027,
			"summary_team": 0.8416666666666667
		}]
	},
	"860059": {
		"nik": "860059",
		"nama": "AKHMAD GHOZALI",
		"band": "II",
		"posisi": "OSM COMMERCIAL CENTER OF EXCELLENCE",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8340852130325859,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9591346153846103,
			"nilai_komparatif_unit": 0.9624168157333388,
			"summary_team": 0.8
		}]
	},
	"860097": {
		"nik": "860097",
		"nama": "DODIK ICHROM RESANTO",
		"band": "II",
		"posisi": "SM INTERNET PRODUCT MANAGEMENT",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8347826086956516,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9902777777777784,
			"nilai_komparatif_unit": 0.9936665513820518,
			"summary_team": 0.8266666666666665
		}]
	},
	"865663": {
		"nik": "865663",
		"nama": "GUSTOMMY BISONO",
		"band": "II",
		"posisi": "AVP SEKRETARIAT DIREKTORAT EBIS",
		"category": [{
			"code": "MD-4-03",
			"kriteria": 0.8251461988304126,
			"kriteria_bp": 0.9965896269733945,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0333097094259351,
			"nilai_komparatif_unit": 1.036845740170965,
			"summary_team": 0.8526315789473685
		}]
	}
};