export const cbark4 = {
	"660004": {
		"nik": "660004",
		"nama": "ERI ERNAWAN",
		"band": "III",
		"posisi": "MGR KONSTRUKSI CIREBON",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8181818181818185,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9451851851851848,
			"nilai_komparatif_unit": 0.9501938506621032,
			"summary_team": 0.7733333333333333
		}]
	},
	"660065": {
		"nik": "660065",
		"nama": "HERRY SUNANDI",
		"band": "III",
		"posisi": "MGR OPERATION KARAWANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8291666666666665,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8990406578346279,
			"nilai_komparatif_unit": 0.9038047971544376,
			"summary_team": 0.7454545454545455
		}]
	},
	"660093": {
		"nik": "660093",
		"nama": "ARIS TRIARSO",
		"band": "III",
		"posisi": "SENIOR ADVISOR III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8345679012345671,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9803657880580967,
			"nilai_komparatif_unit": 0.9855608803578522,
			"summary_team": 0.8181818181818182
		}]
	},
	"660122": {
		"nik": "660122",
		"nama": "EKO SRI KUNDARWANTO",
		"band": "III",
		"posisi": "MGR PROV & MIG BEKASI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8518518518518514,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9547826086956527,
			"nilai_komparatif_unit": 0.9598421322314549,
			"summary_team": 0.8133333333333334
		}]
	},
	"660146": {
		"nik": "660146",
		"nama": "ROSIDAH",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8583333333333338,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0167696381288607,
			"nilai_komparatif_unit": 1.0221576394055385,
			"summary_team": 0.8727272727272727
		}]
	},
	"660191": {
		"nik": "660191",
		"nama": "MADE PASTIMA",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.897435897435897,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9831932773109248,
			"nilai_komparatif_unit": 0.9884033528626703,
			"summary_team": 0.8823529411764706
		}]
	},
	"660207": {
		"nik": "660207",
		"nama": "BAMBANG TRISAKTIAWAN",
		"band": "III",
		"posisi": "MANAGER GENERAL AFFAIRS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9444444444444444,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0455882352941177,
			"nilai_komparatif_unit": 1.0511289502558778,
			"summary_team": 0.9874999999999999
		}]
	},
	"660217": {
		"nik": "660217",
		"nama": "M.RAMAHER ASMANUDIN DAMANIK",
		"band": "III",
		"posisi": "MGR ASSET & FACILITY MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.050000000000001,
			"nilai_komparatif_unit": 1.0555640935059036,
			"summary_team": 0.91
		}]
	},
	"660221": {
		"nik": "660221",
		"nama": "MOEHAMAD SOEMAWILAGA",
		"band": "III",
		"posisi": "MGR SALES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8583333333333338,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9955869373345096,
			"nilai_komparatif_unit": 1.0008626885845902,
			"summary_team": 0.8545454545454546
		}]
	},
	"660272": {
		"nik": "660272",
		"nama": "OTONG SARJU ",
		"band": "III",
		"posisi": "VP INTERNAL AUDIT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8535714285714283,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9192146765368524,
			"nilai_komparatif_unit": 0.9240857207389944,
			"summary_team": 0.7846153846153845
		}]
	},
	"660306": {
		"nik": "660306",
		"nama": "PUTU SUKERATA",
		"band": "III",
		"posisi": "MGR PROJECT SUPERVISION & CONTROL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8148148148148152,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0384615384615379,
			"nilai_komparatif_unit": 1.0439644880827603,
			"summary_team": 0.8461538461538461
		}]
	},
	"660311": {
		"nik": "660311",
		"nama": "ILYAS",
		"band": "III",
		"posisi": "MGR SEGMENT SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8553191489361723,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0432453119020257,
			"nilai_komparatif_unit": 1.0487736113925186,
			"summary_team": 0.8923076923076924
		}]
	},
	"660314": {
		"nik": "660314",
		"nama": "AGUS ROHJAT",
		"band": "III",
		"posisi": "MGR BUSINESS SOLUTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7696969696969703,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1321709786276708,
			"nilai_komparatif_unit": 1.1381705073798172,
			"summary_team": 0.8714285714285716
		}]
	},
	"660323": {
		"nik": "660323",
		"nama": "DANIEL KARANA PUTRA MANULLANG",
		"band": "III",
		"posisi": "MGR SALES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8619883040935664,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0005936227951164,
			"nilai_komparatif_unit": 1.0058959051557277,
			"summary_team": 0.8625
		}]
	},
	"660325": {
		"nik": "660325",
		"nama": "BAMBANG SETIANA",
		"band": "III",
		"posisi": "MGR MANAGED OPT SPBU REG JATIMBALNUSRA 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.852777777777777,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0233935445661841,
			"nilai_komparatif_unit": 1.028816646828378,
			"summary_team": 0.8727272727272728
		}]
	},
	"660484": {
		"nik": "660484",
		"nama": "MUH NAIM ",
		"band": "III",
		"posisi": "VP PROCUREMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.898412698412698,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9724753580063236,
			"nilai_komparatif_unit": 0.9776286378388315,
			"summary_team": 0.8736842105263157
		}]
	},
	"660488": {
		"nik": "660488",
		"nama": "FAJRI",
		"band": "III",
		"posisi": "MGR PROCUREMENT 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.01010101010101,
			"nilai_komparatif_unit": 1.0154536734063515,
			"summary_team": 0.9090909090909092
		}]
	},
	"660623": {
		"nik": "660623",
		"nama": "YUL KHAIDIR WIJAYA",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8617283950617275,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8439697837978648,
			"nilai_komparatif_unit": 0.8484420950294913,
			"summary_team": 0.7272727272727272
		}]
	},
	"670060": {
		"nik": "670060",
		"nama": "IKA SETIAWATI",
		"band": "III",
		"posisi": "MANAGER CASH MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555558,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0285714285714282,
			"nilai_komparatif_unit": 1.034021969148639,
			"summary_team": 0.88
		}]
	},
	"670098": {
		"nik": "670098",
		"nama": "YOYOK TEGUH SUPRIYONO",
		"band": "III",
		"posisi": "SO PARENTING INTEGRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333328,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.037946428571429,
			"nilai_komparatif_unit": 1.0434466485549425,
			"summary_team": 0.8857142857142857
		}]
	},
	"670201": {
		"nik": "670201",
		"nama": "IVAN WIRYAWAN HEROWARDANA",
		"band": "III",
		"posisi": "STAFF KHUSUS III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8519999999999984,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.967420685730547,
			"nilai_komparatif_unit": 0.9725471801638319,
			"summary_team": 0.8242424242424244
		}]
	},
	"670344": {
		"nik": "670344",
		"nama": "IRAWAN, IR",
		"band": "III",
		"posisi": "MANAGER VOICE & MOBILITY SVC SETTLEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8476190476190469,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.000774893452151,
			"nilai_komparatif_unit": 1.0060781363907487,
			"summary_team": 0.8482758620689653
		}]
	},
	"670447": {
		"nik": "670447",
		"nama": "JUNAIDI IRIANTO, ST",
		"band": "III",
		"posisi": "MANAGER OM & MS REGIONAL OFFICE SUMBAGUT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.911111111111111,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9878048780487809,
			"nilai_komparatif_unit": 0.9930393911031145,
			"summary_team": 0.9000000000000002
		}]
	},
	"670622": {
		"nik": "670622",
		"nama": "DADANG JUHAENDI, S.SI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.897435897435897,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9438655462184877,
			"nilai_komparatif_unit": 0.9488672187481635,
			"summary_team": 0.8470588235294116
		}]
	},
	"680017": {
		"nik": "680017",
		"nama": "WASISTO TRIRINO RAHARJO , MT.",
		"band": "III",
		"posisi": "MGR SALES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8394557823129278,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0985953538627733,
			"nilai_komparatif_unit": 1.104416960790433,
			"summary_team": 0.9222222222222222
		}]
	},
	"680043": {
		"nik": "680043",
		"nama": "HADJAR HARIO TEDJO",
		"band": "III",
		"posisi": "MGR TRAFFIC BUSINESS SOLUTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8493827160493818,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9418604651162801,
			"nilai_komparatif_unit": 0.9468515124471563,
			"summary_team": 0.7999999999999999
		}]
	},
	"680104": {
		"nik": "680104",
		"nama": "SRI ISWIRDANINGSIH",
		"band": "III",
		"posisi": "MGR CUSTOMER CARE CONTROL CENTER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8799999999999992,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.036931818181819,
			"nilai_komparatif_unit": 1.0424266616062088,
			"summary_team": 0.9125
		}]
	},
	"680123": {
		"nik": "680123",
		"nama": "TRI SETIAWATI",
		"band": "III",
		"posisi": "MGR SEGMENT SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8394557823129278,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9700162074554263,
			"nilai_komparatif_unit": 0.9751564559130673,
			"summary_team": 0.8142857142857142
		}]
	},
	"680204": {
		"nik": "680204",
		"nama": "SUNARDI",
		"band": "III",
		"posisi": "SO FACILITY & SUPPORT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333336,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9843749999999997,
			"nilai_komparatif_unit": 0.9895913376617834,
			"summary_team": 0.84
		}]
	},
	"680309": {
		"nik": "680309",
		"nama": "HAFIZ",
		"band": "III",
		"posisi": "MGR BUSINESS LEGAL & COMPLIANCE 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740751,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0525423728813548,
			"nilai_komparatif_unit": 1.0581199387686278,
			"summary_team": 0.92
		}]
	},
	"680350": {
		"nik": "680350",
		"nama": "NURUDDIN YAHYA",
		"band": "III",
		"posisi": "MGR MANAGED OPT SPBU REG JAKARTA 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8592592592592608,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9945141065830704,
			"nilai_komparatif_unit": 0.9997841727563728,
			"summary_team": 0.8545454545454546
		}]
	},
	"690009": {
		"nik": "690009",
		"nama": "ADILITA SURBAKTI, IR, MM",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8156249999999994,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9107827038861526,
			"nilai_komparatif_unit": 0.9156090659128016,
			"summary_team": 0.7428571428571427
		}]
	},
	"690011": {
		"nik": "690011",
		"nama": "BAGUS FIRSTIYANTO W HIDAYAT",
		"band": "III",
		"posisi": "MGR SEGMENT SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8619883040935664,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.017325957624466,
			"nilai_komparatif_unit": 1.0227169069141846,
			"summary_team": 0.8769230769230768
		}]
	},
	"690024": {
		"nik": "690024",
		"nama": "MULYADI MUSTAFA",
		"band": "III",
		"posisi": "MGR KONSTRUKSI PEKANBARU",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8411764705882342,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9510489510489523,
			"nilai_komparatif_unit": 0.9560886894225971,
			"summary_team": 0.7999999999999999
		}]
	},
	"690071": {
		"nik": "690071",
		"nama": "ARIFADILLAH S",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8394557823129278,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9529983792544539,
			"nilai_komparatif_unit": 0.9580484479145924,
			"summary_team": 0.7999999999999998
		}]
	},
	"690080": {
		"nik": "690080",
		"nama": "WAGIYO",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8324074074074084,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0812013348164613,
			"nilai_komparatif_unit": 1.0869307684599143,
			"summary_team": 0.8999999999999999
		}]
	},
	"690203": {
		"nik": "690203",
		"nama": "HARIS FAUZI",
		"band": "III",
		"posisi": "MGR KONSTRUKSI JAKARTA UTARA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0384615384615385,
			"nilai_komparatif_unit": 1.043964488082761,
			"summary_team": 0.9000000000000001
		}]
	},
	"690229": {
		"nik": "690229",
		"nama": "JANNES PARLINDUNGAN HUTAJULU",
		"band": "III",
		"posisi": "MGR ASSURANCE & MAINTENANCE JAKUUT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.923076923076923,
			"nilai_komparatif_unit": 0.927968433851343,
			"summary_team": 0.8
		}]
	},
	"690321": {
		"nik": "690321",
		"nama": "ERY SUDEWO",
		"band": "III",
		"posisi": "MGR OPEX & CAPEX MONITORING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8370370370370374,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9898862199747154,
			"nilai_komparatif_unit": 0.9951317623443762,
			"summary_team": 0.8285714285714287
		}]
	},
	"690418": {
		"nik": "690418",
		"nama": "WAWAN SAPUTRA",
		"band": "III",
		"posisi": "MGR COMM & PERF REG JAWA TENGAH",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9142857142857135,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006250000000001,
			"nilai_komparatif_unit": 1.0115822562764911,
			"summary_team": 0.9200000000000002
		}]
	},
	"690519": {
		"nik": "690519",
		"nama": "JAMALUDIN",
		"band": "III",
		"posisi": "MGR MANAGED OPT SPBU REG JAKARTA 3",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8592592592592608,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9521943573667695,
			"nilai_komparatif_unit": 0.9572401654050376,
			"summary_team": 0.8181818181818182
		}]
	},
	"695014": {
		"nik": "695014",
		"nama": "RUDI HOTMAN",
		"band": "III",
		"posisi": "MGR SERVICE DELIVERY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7696969696969703,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9170912459471969,
			"nilai_komparatif_unit": 0.9219510378004302,
			"summary_team": 0.7058823529411764
		}]
	},
	"700004": {
		"nik": "700004",
		"nama": "SUBEKTI",
		"band": "III",
		"posisi": "MGR INV & ASSET MGT REG SUMATERA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8411764705882342,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9693383539537398,
			"nilai_komparatif_unit": 0.9744750103730316,
			"summary_team": 0.8153846153846154
		}]
	},
	"700018": {
		"nik": "700018",
		"nama": "HENNY JULIASTUTI KOESRIN",
		"band": "III",
		"posisi": "MGR LOGISTIC & PROCUREMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9335664335664347,
			"nilai_komparatif_unit": 0.9385135296905639,
			"summary_team": 0.8090909090909093
		}]
	},
	"700032": {
		"nik": "700032",
		"nama": "DISKAERI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8293040293040309,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0163427561837433,
			"nilai_komparatif_unit": 1.021728495354651,
			"summary_team": 0.8428571428571426
		}]
	},
	"700036": {
		"nik": "700036",
		"nama": "ANTON BASYUNI HARTADI",
		"band": "III",
		"posisi": "MGR SALES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8324074074074084,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9610678531701881,
			"nilai_komparatif_unit": 0.9661606830754795,
			"summary_team": 0.8
		}]
	},
	"700113": {
		"nik": "700113",
		"nama": "SULAIMAN",
		"band": "III",
		"posisi": "MGR WHOLESALE TRANSACTION RECORDING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333328,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0560000000000007,
			"nilai_komparatif_unit": 1.061595888325937,
			"summary_team": 0.88
		}]
	},
	"700234": {
		"nik": "700234",
		"nama": "PURWANTO",
		"band": "III",
		"posisi": "MGR RESOURCE & BUDGET CONTROL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8999999999999992,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.987654320987655,
			"nilai_komparatif_unit": 0.9928880362195447,
			"summary_team": 0.8888888888888888
		}]
	},
	"700462": {
		"nik": "700462",
		"nama": "ENY LISTIJARINI",
		"band": "III",
		"posisi": "MGR SEGMENT SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8293040293040309,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.055101590106005,
			"nilai_komparatif_unit": 1.0606927176351253,
			"summary_team": 0.875
		}]
	},
	"700529": {
		"nik": "700529",
		"nama": "SUDIONO",
		"band": "III",
		"posisi": "MGR SERVICE INTEGRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8133333333333329,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9098360655737711,
			"nilai_komparatif_unit": 0.9146574112346235,
			"summary_team": 0.7400000000000001
		}]
	},
	"700546": {
		"nik": "700546",
		"nama": "SUPADMI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8293040293040309,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9646643109540616,
			"nilai_komparatif_unit": 0.9697761989806859,
			"summary_team": 0.7999999999999998
		}]
	},
	"700549": {
		"nik": "700549",
		"nama": "DWI PUDJI IRAWATI",
		"band": "III",
		"posisi": "MGR OFFERING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8156249999999994,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9107827038861526,
			"nilai_komparatif_unit": 0.9156090659128016,
			"summary_team": 0.7428571428571427
		}]
	},
	"700614": {
		"nik": "700614",
		"nama": "SOLIHATI",
		"band": "III",
		"posisi": "SO REPORTING & SUPPORT SYSTEM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8044444444444433,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0383490412739695,
			"nilai_komparatif_unit": 1.0438513947572199,
			"summary_team": 0.8352941176470587
		}]
	},
	"700623": {
		"nik": "700623",
		"nama": "CAHAYA RUMENTA",
		"band": "III",
		"posisi": "MGR SEGMENT SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8949771689497721,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0142072213500781,
			"nilai_komparatif_unit": 1.0195816440300338,
			"summary_team": 0.9076923076923077
		}]
	},
	"700625": {
		"nik": "700625",
		"nama": "SUHERMAN",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9060109289617485,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0517987653444973,
			"nilai_komparatif_unit": 1.057372390753802,
			"summary_team": 0.9529411764705882
		}]
	},
	"700641": {
		"nik": "700641",
		"nama": "OCTOVIANUS MABRUWARU",
		"band": "III",
		"posisi": "MGR KONSTRUKSI SORONG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0052991366722885,
			"summary_team": 0.8
		}]
	},
	"700646": {
		"nik": "700646",
		"nama": "FERRYMANSYAH, ST",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8619883040935664,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8783679007559612,
			"nilai_komparatif_unit": 0.8830224923106179,
			"summary_team": 0.757142857142857
		}]
	},
	"700669": {
		"nik": "700669",
		"nama": "NUR AFANDI",
		"band": "III",
		"posisi": "MGR OTT SERVICE PERFORMANCE MONITORING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9350649350649362,
			"nilai_komparatif_unit": 0.9400199719533096,
			"summary_team": 0.8
		}]
	},
	"710024": {
		"nik": "710024",
		"nama": "AHMAD JUHANI",
		"band": "III",
		"posisi": "MGR DEBT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8523809523809527,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0089385474860333,
			"nilai_komparatif_unit": 1.0142850507431018,
			"summary_team": 0.8600000000000001
		}]
	},
	"710165": {
		"nik": "710165",
		"nama": "AA NUGRAHA",
		"band": "III",
		"posisi": "MGR PROV & MIG JAKARTA TIMUR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9444444444444444,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9741176470588235,
			"nilai_komparatif_unit": 0.9792796296054761,
			"summary_team": 0.9199999999999999
		}]
	},
	"710173": {
		"nik": "710173",
		"nama": "LUKMAN RIVAI",
		"band": "III",
		"posisi": "MGR BGES REGIONAL JAKARTA 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8592592592592608,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.931034482758619,
			"nilai_komparatif_unit": 0.9359681617293701,
			"summary_team": 0.8
		}]
	},
	"710249": {
		"nik": "710249",
		"nama": "DODI INDRA GUNAWAN",
		"band": "III",
		"posisi": "MGR TRAFFIC DELIVERY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8876190476190465,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9359524595576109,
			"nilai_komparatif_unit": 0.940912199559571,
			"summary_team": 0.8307692307692307
		}]
	},
	"710265": {
		"nik": "710265",
		"nama": "MUSTOPA, S.T",
		"band": "III",
		"posisi": "MGR ASSURANCE & MAINTENANCE SERANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8722222222222225,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9171974522292992,
			"nilai_komparatif_unit": 0.9220578068841367,
			"summary_team": 0.8
		}]
	},
	"710314": {
		"nik": "710314",
		"nama": "SRI SUGIARTI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8539007092198568,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9201471286188907,
			"nilai_komparatif_unit": 0.9250231140120558,
			"summary_team": 0.7857142857142856
		}]
	},
	"710371": {
		"nik": "710371",
		"nama": "HAMDAN SYAKURA",
		"band": "III",
		"posisi": "MGR FINANCE REG JAKARTA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8592592592592608,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0086206896551706,
			"nilai_komparatif_unit": 1.0139655085401509,
			"summary_team": 0.8666666666666667
		}]
	},
	"710375": {
		"nik": "710375",
		"nama": "DIAN RUSTANDI, ST",
		"band": "III",
		"posisi": "MANAGER GROUP SYNERGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9366515837104072,
			"nilai_komparatif_unit": 0.9416150284668039,
			"summary_team": 0.8117647058823529
		}]
	},
	"710412": {
		"nik": "710412",
		"nama": "SUSI WINDIAWATI,ST",
		"band": "III",
		"posisi": "MGR ES COLLECT PERFORM&COMPLAIN HANDLING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7745098039215677,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0329113924050646,
			"nilai_komparatif_unit": 1.0383849310437825,
			"summary_team": 0.8
		}]
	},
	"710424": {
		"nik": "710424",
		"nama": "DJOKO SUHARTONO",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8293040293040309,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0163427561837435,
			"nilai_komparatif_unit": 1.0217284953546513,
			"summary_team": 0.8428571428571427
		}]
	},
	"710442": {
		"nik": "710442",
		"nama": "DEDA BUDIANTO",
		"band": "III",
		"posisi": "MGR SEGMENT SUPPORT CLOSED USER GROUP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.897435897435897,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9471428571428574,
			"nilai_komparatif_unit": 0.952161896591039,
			"summary_team": 0.8499999999999999
		}]
	},
	"710449": {
		"nik": "710449",
		"nama": "SUKARDIMAN",
		"band": "III",
		"posisi": "MGR ADS SALES SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7716049382716039,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9979200000000015,
			"nilai_komparatif_unit": 1.0032081144680114,
			"summary_team": 0.7700000000000001
		}]
	},
	"710464": {
		"nik": "710464",
		"nama": "AGUNG BUDIYANTO, M.T",
		"band": "III",
		"posisi": "MGR OFFERING 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8553191489361723,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0668532338308432,
			"nilai_komparatif_unit": 1.0725066349261856,
			"summary_team": 0.9125
		}]
	},
	"710478": {
		"nik": "710478",
		"nama": "TRENGGONO ADHI WIBOWO",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8878048780487822,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9654631083202493,
			"nilai_komparatif_unit": 0.9705792292832905,
			"summary_team": 0.8571428571428571
		}]
	},
	"710496": {
		"nik": "710496",
		"nama": "MULYADI, ST",
		"band": "III",
		"posisi": "MGR SALES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8156249999999994,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0333880678708272,
			"nilai_komparatif_unit": 1.0388641324779866,
			"summary_team": 0.8428571428571427
		}]
	},
	"710524": {
		"nik": "710524",
		"nama": "IMANUEL GINTING S.",
		"band": "III",
		"posisi": "GM TA MEDAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8411764705882342,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9510489510489523,
			"nilai_komparatif_unit": 0.9560886894225971,
			"summary_team": 0.7999999999999999
		}]
	},
	"720023": {
		"nik": "720023",
		"nama": "JEENI JEEVI MANOPPO",
		"band": "III",
		"posisi": "MGR BUSINESS PROCESS & COMPLIANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8641025641025636,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.064688427299704,
			"nilai_komparatif_unit": 1.0703303567893687,
			"summary_team": 0.92
		}]
	},
	"720049": {
		"nik": "720049",
		"nama": "GAMYA RIZKI, SE",
		"band": "III",
		"posisi": "MGR ES COLLECTION & DEBT MGT SEGMENT C",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7745098039215677,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9137293086660188,
			"nilai_komparatif_unit": 0.9185712851541155,
			"summary_team": 0.7076923076923077
		}]
	},
	"720063": {
		"nik": "720063",
		"nama": "SUGENG SULISTIOADI",
		"band": "III",
		"posisi": "MGR PROG SDI & PRO ADM REG KTI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606060606060597,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9140845070422544,
			"nilai_komparatif_unit": 0.9189283657750925,
			"summary_team": 0.7866666666666666
		}]
	},
	"720081": {
		"nik": "720081",
		"nama": "ARJANI ROSALINA, RADEN",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.85528455284553,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9520640956002154,
			"nilai_komparatif_unit": 0.9571092133635795,
			"summary_team": 0.8142857142857142
		}]
	},
	"720116": {
		"nik": "720116",
		"nama": "RENI SURYATI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8324074074074084,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0484376580038415,
			"nilai_komparatif_unit": 1.0539934724459776,
			"summary_team": 0.8727272727272727
		}]
	},
	"720119": {
		"nik": "720119",
		"nama": "AHMAD HUNEN",
		"band": "III",
		"posisi": "VP INTERNAL AUDIT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8186046511627898,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0336538461538474,
			"nilai_komparatif_unit": 1.039131319156453,
			"summary_team": 0.8461538461538463
		}]
	},
	"720123": {
		"nik": "720123",
		"nama": "HENDRA SUPARYANTO, ST",
		"band": "III",
		"posisi": "MGR SHARED SERVICE SOLO",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0079999999999998,
			"nilai_komparatif_unit": 1.0133415297656663,
			"summary_team": 0.84
		}]
	},
	"720125": {
		"nik": "720125",
		"nama": "HERU SAM SETIADJI, M.T",
		"band": "III",
		"posisi": "MGR SALES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333321,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9877232142857156,
			"nilai_komparatif_unit": 0.9929572945926075,
			"summary_team": 0.8428571428571427
		}]
	},
	"720126": {
		"nik": "720126",
		"nama": "EDI SUKAMTO, M.T",
		"band": "III",
		"posisi": "MGR SEG SUPP VAS, CONTENT,&SERV PROVIDER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9060109289617485,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9618300878855764,
			"nilai_komparatif_unit": 0.9669269569768011,
			"summary_team": 0.8714285714285713
		}]
	},
	"720151": {
		"nik": "720151",
		"nama": "SIGIT NURWIJAYA, ST",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8293040293040309,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0163427561837433,
			"nilai_komparatif_unit": 1.021728495354651,
			"summary_team": 0.8428571428571426
		}]
	},
	"720160": {
		"nik": "720160",
		"nama": "ENING SUSILO",
		"band": "III",
		"posisi": "MGR CUSTOMER RELATIONSHIP MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8277777777777782,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9906040268456372,
			"nilai_komparatif_unit": 0.9958533729720114,
			"summary_team": 0.8200000000000001
		}]
	},
	"720164": {
		"nik": "720164",
		"nama": "WIDODO",
		"band": "III",
		"posisi": "MGR ACCOUNT DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8362318840579697,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9417244367417693,
			"nilai_komparatif_unit": 0.9467147632396976,
			"summary_team": 0.7875
		}]
	},
	"720167": {
		"nik": "720167",
		"nama": "ALOYSIA DYAH TRI WIDAWATI,ST",
		"band": "III",
		"posisi": "MGR QUALITY & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8999999999999992,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0493827160493836,
			"nilai_komparatif_unit": 1.0549435384832664,
			"summary_team": 0.9444444444444444
		}]
	},
	"720172": {
		"nik": "720172",
		"nama": "INDRA ADITYAWAN, ST",
		"band": "III",
		"posisi": "MGR INDUSTRIAL ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333321,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0044642857142871,
			"nilai_komparatif_unit": 1.0097870792467196,
			"summary_team": 0.857142857142857
		}]
	},
	"720183": {
		"nik": "720183",
		"nama": "TONY EKO SAPUTRO",
		"band": "III",
		"posisi": "MGR OPERATION MAGELANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8705882352941177,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9763513513513514,
			"nilai_komparatif_unit": 0.9815251706023356,
			"summary_team": 0.8500000000000001
		}]
	},
	"720184": {
		"nik": "720184",
		"nama": "SUMARSONO",
		"band": "III",
		"posisi": "VP SUBMARINE PLANNING & ENGINEERING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.859649122807017,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9853541416566635,
			"nilai_komparatif_unit": 0.9905756679239074,
			"summary_team": 0.8470588235294119
		}]
	},
	"720196": {
		"nik": "720196",
		"nama": "PANCA SUSILOWATI, ST",
		"band": "III",
		"posisi": "OVP CONSTRUCTION SUPERVISION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7490196078431375,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0064438179621422,
			"nilai_komparatif_unit": 1.0117771013065031,
			"summary_team": 0.7538461538461538
		}]
	},
	"720213": {
		"nik": "720213",
		"nama": "BAMBANG TOTOK MARJITO,ST",
		"band": "III",
		"posisi": "MGR CORPORATE GOVERNANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7733333333333337,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9988109393579069,
			"nilai_komparatif_unit": 1.004103775035341,
			"summary_team": 0.7724137931034483
		}]
	},
	"720236": {
		"nik": "720236",
		"nama": "YONATSON KERISTUA",
		"band": "III",
		"posisi": "SO SALES STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8844444444444439,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.944428022465268,
			"nilai_komparatif_unit": 0.9494326756334504,
			"summary_team": 0.8352941176470587
		}]
	},
	"720255": {
		"nik": "720255",
		"nama": "FERI KOSTARIA HARI BANAWATI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.85528455284553,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8852525801194986,
			"nilai_komparatif_unit": 0.8899436545310476,
			"summary_team": 0.757142857142857
		}]
	},
	"720256": {
		"nik": "720256",
		"nama": "RUDY WIBOWO",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8293040293040309,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9646643109540616,
			"nilai_komparatif_unit": 0.9697761989806859,
			"summary_team": 0.7999999999999998
		}]
	},
	"720264": {
		"nik": "720264",
		"nama": "THERESIA FENTI EKARISTIANTI",
		"band": "III",
		"posisi": "MGR QUALITY OF SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8461538461538465,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9593582887700532,
			"nilai_komparatif_unit": 0.9644420594599383,
			"summary_team": 0.811764705882353
		}]
	},
	"720265": {
		"nik": "720265",
		"nama": "ESTER INDAH PURWAHYUNINGSIH",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8916666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9933244325767698,
			"nilai_komparatif_unit": 0.9985881945049173,
			"summary_team": 0.8857142857142857
		}]
	},
	"720274": {
		"nik": "720274",
		"nama": "OKTA YUFIANDRI",
		"band": "III",
		"posisi": "GM TA SUMBAGUT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8411764705882342,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0550699300699313,
			"nilai_komparatif_unit": 1.0606608898281935,
			"summary_team": 0.8875
		}]
	},
	"720283": {
		"nik": "720283",
		"nama": "PALUPI UTAMI",
		"band": "III",
		"posisi": "MGR REVENUE ASSURANCE & FRAUD MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8876190476190465,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0214592274678123,
			"nilai_komparatif_unit": 1.0268720795193342,
			"summary_team": 0.9066666666666666
		}]
	},
	"720299": {
		"nik": "720299",
		"nama": "KOMANG SUTA WIRAWAN, ST, MM",
		"band": "III",
		"posisi": "MGR ORDER MANAGEMENT 3",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740751,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9915254237288124,
			"nilai_komparatif_unit": 0.9967796524631999,
			"summary_team": 0.8666666666666667
		}]
	},
	"720341": {
		"nik": "720341",
		"nama": "MAMAN ROCHAMAN",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8949771689497721,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8938775510204076,
			"nilai_komparatif_unit": 0.898614330331555,
			"summary_team": 0.7999999999999998
		}]
	},
	"720342": {
		"nik": "720342",
		"nama": "INDRA PRASETIO, ST",
		"band": "III",
		"posisi": "MGR TECHNICAL SALES SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8493827160493818,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9104651162790708,
			"nilai_komparatif_unit": 0.9152897953655844,
			"summary_team": 0.7733333333333333
		}]
	},
	"720356": {
		"nik": "720356",
		"nama": "ACHMAD BAGJA",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8324074074074084,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0047527555870146,
			"nilai_komparatif_unit": 1.0100770777607284,
			"summary_team": 0.8363636363636363
		}]
	},
	"720358": {
		"nik": "720358",
		"nama": "I GDE MEIARSA, ST",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8916666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.961281708945261,
			"nilai_komparatif_unit": 0.9663756721015327,
			"summary_team": 0.857142857142857
		}]
	},
	"720362": {
		"nik": "720362",
		"nama": "SAKTA KUNTADI PRABOWO",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8583333333333338,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.965325936199722,
			"nilai_komparatif_unit": 0.9704413302689489,
			"summary_team": 0.8285714285714285
		}]
	},
	"720376": {
		"nik": "720376",
		"nama": "MOH. ZAINUL ARIFIN",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8583333333333338,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0319001386962545,
			"nilai_komparatif_unit": 1.037368318563359,
			"summary_team": 0.8857142857142856
		}]
	},
	"720378": {
		"nik": "720378",
		"nama": "GDE ASBAWA PUTRA",
		"band": "III",
		"posisi": "MANAGER PRESALES SOLT. II",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8222222222222224,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9443561208267087,
			"nilai_komparatif_unit": 0.9493603929782815,
			"summary_team": 0.776470588235294
		}]
	},
	"720379": {
		"nik": "720379",
		"nama": "RUDI HAERUDIAT",
		"band": "III",
		"posisi": "MGR SERVICE PLATFORM READINESS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555552,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.007613076578594,
			"nilai_komparatif_unit": 1.0129525559841688,
			"summary_team": 0.8620689655172412
		}]
	},
	"720380": {
		"nik": "720380",
		"nama": "YULFIANA, M.M",
		"band": "III",
		"posisi": "MGR MARKETING SERVICE PLAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7716049382716039,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0756800000000015,
			"nilai_komparatif_unit": 1.0813801753356485,
			"summary_team": 0.8300000000000001
		}]
	},
	"720386": {
		"nik": "720386",
		"nama": "RACHADIAN ACHMAD KOMARA",
		"band": "III",
		"posisi": "MGR OFFERING 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8878048780487822,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0397295012679608,
			"nilai_komparatif_unit": 1.0452391699973897,
			"summary_team": 0.923076923076923
		}]
	},
	"720390": {
		"nik": "720390",
		"nama": "PRAJOGI JULYANTORO",
		"band": "III",
		"posisi": "MGR OFFERING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8771929824561402,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9975000000000002,
			"nilai_komparatif_unit": 1.0027858888306076,
			"summary_team": 0.875
		}]
	},
	"720401": {
		"nik": "720401",
		"nama": "RUDI ISKANDAR",
		"band": "III",
		"posisi": "MANAGER PARTNERSHIP & SOURCING II",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8238095238095239,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9931686810299528,
			"nilai_komparatif_unit": 0.9984316176093668,
			"summary_team": 0.8181818181818183
		}]
	},
	"720403": {
		"nik": "720403",
		"nama": "ANDI PRIJASDANTO",
		"band": "III",
		"posisi": "MANAGER BILLING OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8476190476190469,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0049937578027477,
			"nilai_komparatif_unit": 1.010319357080141,
			"summary_team": 0.8518518518518521
		}]
	},
	"720405": {
		"nik": "720405",
		"nama": "DODO BASTIAN TAMBOOS SIAGIAN, ST, MM",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8293040293040309,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9129858657243798,
			"nilai_komparatif_unit": 0.9178239026067206,
			"summary_team": 0.7571428571428571
		}]
	},
	"720413": {
		"nik": "720413",
		"nama": "RINALDI",
		"band": "III",
		"posisi": "MGR MANAGED SERVICE CAPACITY PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8823529411764705,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.967111111111111,
			"nilai_komparatif_unit": 0.9722359650661774,
			"summary_team": 0.8533333333333332
		}]
	},
	"720420": {
		"nik": "720420",
		"nama": "PANDE GEDE PRADNYA JAYA",
		"band": "III",
		"posisi": "MGR SALES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8771929824561402,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0640000000000003,
			"nilai_komparatif_unit": 1.069638281419315,
			"summary_team": 0.9333333333333333
		}]
	},
	"720426": {
		"nik": "720426",
		"nama": "HUSNI MUBARAK, ST",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8619883040935664,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0440976933514254,
			"nilai_komparatif_unit": 1.0496305097277157,
			"summary_team": 0.8999999999999998
		}]
	},
	"720439": {
		"nik": "720439",
		"nama": "YUDI CATUR WIBOWO,ST",
		"band": "III",
		"posisi": "MGR CONSTRUCTION SUPERVISION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8592592592592608,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0384615384615365,
			"nilai_komparatif_unit": 1.043964488082759,
			"summary_team": 0.8923076923076924
		}]
	},
	"720458": {
		"nik": "720458",
		"nama": "IMAM KUSWORO",
		"band": "III",
		"posisi": "SO REPORTING & SUPPORT SYSTEM",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9533333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0039960039960039,
			"nilai_komparatif_unit": 1.00931631603961,
			"summary_team": 0.9571428571428569
		}]
	},
	"720474": {
		"nik": "720474",
		"nama": "NUR ISMURROCHMAH, ST",
		"band": "III",
		"posisi": "MGR ES INVOICING & RECONCILIATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7745098039215677,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9775768535262217,
			"nilai_komparatif_unit": 0.9827571668807227,
			"summary_team": 0.757142857142857
		}]
	},
	"720488": {
		"nik": "720488",
		"nama": "HARTANTO",
		"band": "III",
		"posisi": "MGR SEGMENT SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333321,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0546875000000016,
			"nilai_komparatif_unit": 1.0602764332090555,
			"summary_team": 0.9
		}]
	},
	"720509": {
		"nik": "720509",
		"nama": "MOH CHUDURI",
		"band": "III",
		"posisi": "MGR NMS & DCN OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.795555555555556,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9713052310817669,
			"nilai_komparatif_unit": 0.9764523102517777,
			"summary_team": 0.7727272727272728
		}]
	},
	"720536": {
		"nik": "720536",
		"nama": "ANIS FUADI",
		"band": "III",
		"posisi": "MGR PLATFORM & IT PERFORMANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9407407407407407,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000004,
			"nilai_komparatif_unit": 1.0052991366722888,
			"summary_team": 0.940740740740741
		}]
	},
	"720541": {
		"nik": "720541",
		"nama": "SAT SETIJO EDI POERNOMO",
		"band": "III",
		"posisi": "MANAGER OPERATION SUPPORT & PERFORMANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8407407407407405,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0025173064820647,
			"nilai_komparatif_unit": 1.0078297827054474,
			"summary_team": 0.8428571428571431
		}]
	},
	"720544": {
		"nik": "720544",
		"nama": "RINI ROOSILAWATIE",
		"band": "III",
		"posisi": "MGR INDUSTRIAL ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8878048780487822,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.022400676246828,
			"nilai_komparatif_unit": 1.0278185171640999,
			"summary_team": 0.9076923076923077
		}]
	},
	"720545": {
		"nik": "720545",
		"nama": "YOZAR ALBAR",
		"band": "III",
		"posisi": "SO CUSTOMER INTERFACE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9133333333333329,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.976979225154408,
			"nilai_komparatif_unit": 0.9821563715944875,
			"summary_team": 0.8923076923076922
		}]
	},
	"720555": {
		"nik": "720555",
		"nama": "JOKO EKO PUTRO, ST",
		"band": "III",
		"posisi": "MGR SURVEILLANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8454545454545451,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.040860215053764,
			"nilai_komparatif_unit": 1.0463758755900812,
			"summary_team": 0.8800000000000002
		}]
	},
	"720564": {
		"nik": "720564",
		"nama": "RADIAN SIGIT DWIANANTO",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8293040293040309,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9302120141342737,
			"nilai_komparatif_unit": 0.9351413347313757,
			"summary_team": 0.7714285714285714
		}]
	},
	"720602": {
		"nik": "720602",
		"nama": "EVY SUSANTY",
		"band": "III",
		"posisi": "MGR LEGAL & COMPLIANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8999999999999992,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9777777777777787,
			"nilai_komparatif_unit": 0.9829591558573495,
			"summary_team": 0.8800000000000001
		}]
	},
	"730095": {
		"nik": "730095",
		"nama": "PETRUS VIVYAN DHARVYANTA",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8878048780487822,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9171899529042368,
			"nilai_komparatif_unit": 0.9220502678191259,
			"summary_team": 0.8142857142857142
		}]
	},
	"730139": {
		"nik": "730139",
		"nama": "ENGGAL SWASANA",
		"band": "III",
		"posisi": "MGR SALES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8878048780487822,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.062009419152274,
			"nilai_komparatif_unit": 1.0676371522116193,
			"summary_team": 0.9428571428571426
		}]
	},
	"730140": {
		"nik": "730140",
		"nama": "CAHYO BUWONO, ST",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8394557823129278,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9529983792544539,
			"nilai_komparatif_unit": 0.9580484479145924,
			"summary_team": 0.7999999999999998
		}]
	},
	"730142": {
		"nik": "730142",
		"nama": "ANDREAS NUKY KRISTIAWAN",
		"band": "III",
		"posisi": "MGR CARRIER SERVICE ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8876190476190465,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9763948497854089,
			"nilai_komparatif_unit": 0.9815688995405402,
			"summary_team": 0.8666666666666667
		}]
	},
	"730147": {
		"nik": "730147",
		"nama": "DWI IRMIYANTI",
		"band": "III",
		"posisi": "MGR SALES SUPERVISION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8453333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9227129337539441,
			"nilai_komparatif_unit": 0.9276025156991944,
			"summary_team": 0.78
		}]
	},
	"730150": {
		"nik": "730150",
		"nama": "SULISTYO HARTATI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333321,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272743,
			"nilai_komparatif_unit": 1.0281468443239328,
			"summary_team": 0.8727272727272727
		}]
	},
	"730166": {
		"nik": "730166",
		"nama": "BERNADUS TRIAJI BANGUN KAHONO",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.897435897435897,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.943865546218488,
			"nilai_komparatif_unit": 0.9488672187481637,
			"summary_team": 0.8470588235294119
		}]
	},
	"730181": {
		"nik": "730181",
		"nama": "DWI MULYONO NUGROHO",
		"band": "III",
		"posisi": "MANAGER TECHNOLOGY PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8592592592592593,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.076508620689655,
			"nilai_komparatif_unit": 1.0822131869995861,
			"summary_team": 0.9249999999999999
		}]
	},
	"730186": {
		"nik": "730186",
		"nama": "YOHANANTO",
		"band": "III",
		"posisi": "SO RESOURCES MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8844444444444437,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9577298255985821,
			"nilai_komparatif_unit": 0.9628049668395557,
			"summary_team": 0.8470588235294119
		}]
	},
	"730188": {
		"nik": "730188",
		"nama": "RADEN IMAN YUNASTO ADIWARSO",
		"band": "III",
		"posisi": "MGR SEGMENT SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8878048780487822,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.027815934065932,
			"nilai_komparatif_unit": 1.033262471174503,
			"summary_team": 0.9125
		}]
	},
	"730201": {
		"nik": "730201",
		"nama": "ZULKARNAIN",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8394557823129278,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.952998379254454,
			"nilai_komparatif_unit": 0.9580484479145925,
			"summary_team": 0.7999999999999999
		}]
	},
	"730216": {
		"nik": "730216",
		"nama": "MOHAMAD SAEFURAHMAN",
		"band": "III",
		"posisi": "SR ACCOUNT MANAGER VOICE & MOBILITY I",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8111111111111113,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0252343186733952,
			"nilai_komparatif_unit": 1.0306671754491659,
			"summary_team": 0.8315789473684209
		}]
	},
	"730239": {
		"nik": "730239",
		"nama": "HERU NUGROHO",
		"band": "III",
		"posisi": "MGR REVENUE ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740751,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.042372881355931,
			"nilai_komparatif_unit": 1.047896557717723,
			"summary_team": 0.9111111111111111
		}]
	},
	"730246": {
		"nik": "730246",
		"nama": "JAPPY HERMAN",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9060109289617485,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9868729156318741,
			"nilai_komparatif_unit": 0.992102490089987,
			"summary_team": 0.8941176470588235
		}]
	},
	"730247": {
		"nik": "730247",
		"nama": "MUHAMMAD ZULKARNAIN",
		"band": "III",
		"posisi": "MGR OFFERING 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8583333333333338,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0575056011949209,
			"nilai_komparatif_unit": 1.0631094679073632,
			"summary_team": 0.9076923076923077
		}]
	},
	"730254": {
		"nik": "730254",
		"nama": "IBNU RADHI, ST",
		"band": "III",
		"posisi": "MGR AP MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7745098039215677,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9624856156501739,
			"nilai_komparatif_unit": 0.9675859584726156,
			"summary_team": 0.7454545454545455
		}]
	},
	"730261": {
		"nik": "730261",
		"nama": "SAMSU",
		"band": "III",
		"posisi": "MGR OM REGIONAL OFFICE JABODATABEB INNER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9111111111111112,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.019163763066202,
			"nilai_komparatif_unit": 1.0245644511381335,
			"summary_team": 0.9285714285714287
		}]
	},
	"730272": {
		"nik": "730272",
		"nama": "YAFRINO, ST, M.M",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8394557823129278,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9529983792544539,
			"nilai_komparatif_unit": 0.9580484479145924,
			"summary_team": 0.7999999999999998
		}]
	},
	"730279": {
		"nik": "730279",
		"nama": "BAMBANG SURYADI",
		"band": "III",
		"posisi": "MGR INDUSTRIAL ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8949771689497721,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9497448979591833,
			"nilai_komparatif_unit": 0.9547777259772774,
			"summary_team": 0.85
		}]
	},
	"730281": {
		"nik": "730281",
		"nama": "RAKHMAT DRIYARKORO",
		"band": "III",
		"posisi": "MANAGER PROJECT SUPPORT & CONTROL ETG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9841269841269842,
			"nilai_komparatif_unit": 0.9893420075187599,
			"summary_team": 0.8857142857142859
		}]
	},
	"730284": {
		"nik": "730284",
		"nama": "YUSTINUS NURWIDYANTO",
		"band": "III",
		"posisi": "MANAGER ASSET  SETTLEMENT & DATA VERIFIC",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8888888888888888,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857145,
			"nilai_komparatif_unit": 0.9693955960768497,
			"summary_team": 0.8571428571428573
		}]
	},
	"730293": {
		"nik": "730293",
		"nama": "ENDANG WIDYANINGRUM",
		"band": "III",
		"posisi": "MGR INDUSTRIAL ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8619883040935664,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9994781338064931,
			"nilai_komparatif_unit": 1.0047745050384973,
			"summary_team": 0.8615384615384616
		}]
	},
	"730305": {
		"nik": "730305",
		"nama": "SIHMUNADI",
		"band": "III",
		"posisi": "MGR BILLING VALIDATION 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7745098039215677,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9775768535262217,
			"nilai_komparatif_unit": 0.9827571668807227,
			"summary_team": 0.757142857142857
		}]
	},
	"730315": {
		"nik": "730315",
		"nama": "BAMBANG MULYANTO",
		"band": "III",
		"posisi": "MGR ORDER MANAGEMENT 1",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740751,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.996973365617432,
			"nilai_komparatif_unit": 1.00225646374047,
			"summary_team": 0.8714285714285712
		}]
	},
	"730320": {
		"nik": "730320",
		"nama": "SONI SUSANTO",
		"band": "III",
		"posisi": "MANAGER BUSINESS EVAL. & PERF DESIGN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7966666666666664,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.972803347280335,
			"nilai_komparatif_unit": 0.977958365172833,
			"summary_team": 0.775
		}]
	},
	"730331": {
		"nik": "730331",
		"nama": "DANNY ARIFIAN IDIARTO",
		"band": "III",
		"posisi": "MGR EAST AREA PLATFORM O&M",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8041666666666654,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.028732925105984,
			"nilai_komparatif_unit": 1.0341843214754034,
			"summary_team": 0.8272727272727275
		}]
	},
	"730336": {
		"nik": "730336",
		"nama": "I KADEK YOGI",
		"band": "III",
		"posisi": "VP COMMERCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740738,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.022033898305085,
			"nilai_komparatif_unit": 1.0274497956159152,
			"summary_team": 0.8933333333333333
		}]
	},
	"730350": {
		"nik": "730350",
		"nama": "AMER FADILLAH, ST",
		"band": "III",
		"posisi": "SENIOR STAFF III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8869565217391299,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9502801120448184,
			"nilai_komparatif_unit": 0.9553157762355013,
			"summary_team": 0.8428571428571427
		}]
	},
	"730404": {
		"nik": "730404",
		"nama": "DYAH LUSI BUDILESTARI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333321,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8735795454545467,
			"nilai_komparatif_unit": 0.8782087628600258,
			"summary_team": 0.7454545454545455
		}]
	},
	"730430": {
		"nik": "730430",
		"nama": "JERRY ALVIJANO HARJADI",
		"band": "III",
		"posisi": "MANAGER CUSTOMER CARE & HELPDESK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.788888888888889,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0140845070422533,
			"nilai_komparatif_unit": 1.0194582794423201,
			"summary_team": 0.7999999999999999
		}]
	},
	"730437": {
		"nik": "730437",
		"nama": "BARIA AKHMAD FIKRI",
		"band": "III",
		"posisi": "MGR INDUSTRIAL ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8539007092198568,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9515157807308985,
			"nilai_komparatif_unit": 0.9565579928988306,
			"summary_team": 0.8125
		}]
	},
	"730443": {
		"nik": "730443",
		"nama": "MEYANA NUR PATRIA KRISNA",
		"band": "III",
		"posisi": "MGR WALK IN CHANNEL OPERATION & PARTNSHP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8799999999999992,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9491978609625678,
			"nilai_komparatif_unit": 0.9542277901568521,
			"summary_team": 0.8352941176470589
		}]
	},
	"730445": {
		"nik": "730445",
		"nama": "CHARLES TOBING",
		"band": "III",
		"posisi": "OVP BUSINESS EXPANSION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7490196078431375,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9968586387434553,
			"nilai_komparatif_unit": 1.0021411289131081,
			"summary_team": 0.7466666666666667
		}]
	},
	"730498": {
		"nik": "730498",
		"nama": "NOVA RACHMAN",
		"band": "III",
		"posisi": "MGR PROV & MIG JAKARTA BARAT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8777777777777773,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9417721518987346,
			"nilai_komparatif_unit": 0.946762731245801,
			"summary_team": 0.8266666666666667
		}]
	},
	"730550": {
		"nik": "730550",
		"nama": "SULUH RIAWAN",
		"band": "III",
		"posisi": "MGR PROG SDI & PRO ADM REG JATIM BALNUS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.852777777777777,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0102731145076431,
			"nilai_komparatif_unit": 1.0156266898177575,
			"summary_team": 0.8615384615384616
		}]
	},
	"730585": {
		"nik": "730585",
		"nama": "RADEN TOMI ARIYO TEJO",
		"band": "III",
		"posisi": "MGR OTT PLATFORM OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0413223140495882,
			"nilai_komparatif_unit": 1.0468404233116404,
			"summary_team": 0.8909090909090911
		}]
	},
	"730593": {
		"nik": "730593",
		"nama": "RIRIS SIGIT PRAMONO",
		"band": "III",
		"posisi": "MGR OFFERING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8324074074074084,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.036151279199109,
			"nilai_komparatif_unit": 1.0416419864407513,
			"summary_team": 0.8625
		}]
	},
	"730606": {
		"nik": "730606",
		"nama": "OBED CHRISTIAN ALEXANDERMARPAUNG",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8878048780487822,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8689167974882244,
			"nilai_komparatif_unit": 0.8735213063549615,
			"summary_team": 0.7714285714285714
		}]
	},
	"740001": {
		"nik": "740001",
		"nama": "YENI SUPARTINI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8394557823129278,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.004051863857371,
			"nilai_komparatif_unit": 1.009372471910017,
			"summary_team": 0.8428571428571426
		}]
	},
	"740005": {
		"nik": "740005",
		"nama": "TOM DORUS RUMSOWEK",
		"band": "III",
		"posisi": "MGR MANAGED OPT SPBU REG KTI 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606060606060597,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0985915492957758,
			"nilai_komparatif_unit": 1.1044131360625151,
			"summary_team": 0.9454545454545454
		}]
	},
	"740060": {
		"nik": "740060",
		"nama": "NURIL HUDHA PRAMONO",
		"band": "III",
		"posisi": "MANAGER PROJECT PLANNING & INISIASI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9666666666666668,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9827586206896551,
			"nilai_komparatif_unit": 0.9879663929365591,
			"summary_team": 0.9500000000000001
		}]
	},
	"740078": {
		"nik": "740078",
		"nama": "ANISAH SOFWATI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8771929824561402,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0570909090909093,
			"nilai_komparatif_unit": 1.0626925782932155,
			"summary_team": 0.9272727272727272
		}]
	},
	"740085": {
		"nik": "740085",
		"nama": "HERAS ANG CHANDRA WIJAYA, ST",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8771929824561402,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9608571428571429,
			"nilai_komparatif_unit": 0.9659488561796873,
			"summary_team": 0.8428571428571427
		}]
	},
	"740103": {
		"nik": "740103",
		"nama": "SYAHRIZAL",
		"band": "III",
		"posisi": "MGR SALES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8553191489361723,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0355366027007789,
			"nilai_komparatif_unit": 1.0410240526876473,
			"summary_team": 0.8857142857142855
		}]
	},
	"740118": {
		"nik": "740118",
		"nama": "SAMUEL SAHAT HALOMOAN",
		"band": "III",
		"posisi": "MGR SALES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.85528455284553,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9483523447401758,
			"nilai_komparatif_unit": 0.9533777934284391,
			"summary_team": 0.8111111111111111
		}]
	},
	"740161": {
		"nik": "740161",
		"nama": "SYAFRULLAH",
		"band": "III",
		"posisi": "MGR WIFI SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8904761904761904,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9994652406417113,
			"nilai_komparatif_unit": 1.0047615435510733,
			"summary_team": 0.89
		}]
	},
	"740167": {
		"nik": "740167",
		"nama": "INDRA WAHYU SETIAWAN",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8894308943089421,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8994515539305312,
			"nilai_komparatif_unit": 0.9042178706449111,
			"summary_team": 0.8
		}]
	},
	"740184": {
		"nik": "740184",
		"nama": "RIA FARANI RAHAYU",
		"band": "III",
		"posisi": "MGR SALES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8949771689497721,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0552721088435368,
			"nilai_komparatif_unit": 1.0608641399747525,
			"summary_team": 0.9444444444444444
		}]
	},
	"740192": {
		"nik": "740192",
		"nama": "AWALUDIN HANAFI",
		"band": "III",
		"posisi": "MGR NETWORK DELIVERY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8876190476190465,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.961373390557941,
			"nilai_komparatif_unit": 0.9664678395476087,
			"summary_team": 0.8533333333333333
		}]
	},
	"740206": {
		"nik": "740206",
		"nama": "MIMBAR MURDIYATI , ST.",
		"band": "III",
		"posisi": "SO STRATEGIC PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8433333333333329,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.050254093732355,
			"nilai_komparatif_unit": 1.055819533715673,
			"summary_team": 0.8857142857142857
		}]
	},
	"740238": {
		"nik": "740238",
		"nama": "ACHMAD SUTARJONO",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8949771689497721,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9751391465677175,
			"nilai_komparatif_unit": 0.9803065421798783,
			"summary_team": 0.8727272727272727
		}]
	},
	"740239": {
		"nik": "740239",
		"nama": "MHM. THOHIRUN",
		"band": "III",
		"posisi": "MGR ES COLLECTION & DEBT MGT SEGMENT B",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7745098039215677,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.051356238698012,
			"nilai_komparatif_unit": 1.0569275190981358,
			"summary_team": 0.8142857142857142
		}]
	},
	"740262": {
		"nik": "740262",
		"nama": "I KETUT SUDAMA WIBAWA",
		"band": "III",
		"posisi": "MGR SEGMENT SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8324074074074084,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0812013348164615,
			"nilai_komparatif_unit": 1.0869307684599145,
			"summary_team": 0.9
		}]
	},
	"740272": {
		"nik": "740272",
		"nama": "ROHMAN ARDI SAKSANA",
		"band": "III",
		"posisi": "MGR SALES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8293040293040309,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9646643109540616,
			"nilai_komparatif_unit": 0.9697761989806859,
			"summary_team": 0.7999999999999998
		}]
	},
	"740301": {
		"nik": "740301",
		"nama": "HENRI MANOLA",
		"band": "III",
		"posisi": "MGR PROV & MIG YOGYAKARTA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8705882352941177,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0414414414414415,
			"nilai_komparatif_unit": 1.0469601819758245,
			"summary_team": 0.9066666666666666
		}]
	},
	"740306": {
		"nik": "740306",
		"nama": "DADE EARLY",
		"band": "III",
		"posisi": "SR ACC MANAGER CARRIER ENTERPRISE SA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9777777777777779,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9971590909090909,
			"nilai_komparatif_unit": 1.002443173215833,
			"summary_team": 0.9750000000000001
		}]
	},
	"750010": {
		"nik": "750010",
		"nama": "KRISTOFORUS BOLI PITO",
		"band": "III",
		"posisi": "MGR KONSTRUKSI KUPANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.846666666666667,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9251968503937007,
			"nilai_komparatif_unit": 0.9300995949527076,
			"summary_team": 0.7833333333333335
		}]
	},
	"750013": {
		"nik": "750013",
		"nama": "I GUSTI BAGUS WIDYANTARA",
		"band": "III",
		"posisi": "MGR SHARED SERVICE NUSA TENGGARA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.846666666666667,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9842519685039368,
			"nilai_komparatif_unit": 0.9894676542050079,
			"summary_team": 0.8333333333333335
		}]
	},
	"750017": {
		"nik": "750017",
		"nama": "MERBY",
		"band": "III",
		"posisi": "MGR PROV & MIG MANADO",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8999999999999999,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0074074074074075,
			"nilai_komparatif_unit": 1.0127457969439349,
			"summary_team": 0.9066666666666666
		}]
	},
	"750056": {
		"nik": "750056",
		"nama": "YOHANNIS BRITTO DEBBIE RI",
		"band": "III",
		"posisi": "SO PRODUCT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9877232142857142,
			"nilai_komparatif_unit": 0.9929572945926061,
			"summary_team": 0.8428571428571427
		}]
	},
	"750068": {
		"nik": "750068",
		"nama": "HENDRA CAHYA YUSWANTO",
		"band": "III",
		"posisi": "GM TA SAMARINDA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8515151515151508,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0177935943060505,
			"nilai_komparatif_unit": 1.0231870216664578,
			"summary_team": 0.8666666666666665
		}]
	},
	"750082": {
		"nik": "750082",
		"nama": "VALIAN KUSUMAWARDHANA",
		"band": "III",
		"posisi": "SO STRATEGIC BUDGETING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.893333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0234541577825162,
			"nilai_komparatif_unit": 1.0288775812424276,
			"summary_team": 0.9142857142857141
		}]
	},
	"760035": {
		"nik": "760035",
		"nama": "ADITYA INDRAWAN",
		"band": "III",
		"posisi": "MGR SUBSIDIARY PARENTING ORCHESTRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9047619047619049,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9772853185595566,
			"nilai_komparatif_unit": 0.9824640870304244,
			"summary_team": 0.8842105263157894
		}]
	},
	"760036": {
		"nik": "760036",
		"nama": "WILLY GUMILAR",
		"band": "III",
		"posisi": "MGR OFFERING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8949771689497721,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0313971742543167,
			"nilai_komparatif_unit": 1.0368626888441022,
			"summary_team": 0.923076923076923
		}]
	},
	"760050": {
		"nik": "760050",
		"nama": "GIDEON OCTORA",
		"band": "III",
		"posisi": "MANAGER GROUP PORTFOLIO & BUSINESS DEV",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9777777777777777,
			"nilai_komparatif_unit": 0.9829591558573485,
			"summary_team": 0.88
		}]
	},
	"760052": {
		"nik": "760052",
		"nama": "OCTORYZA PUTRA",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8771929824561402,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9934285714285713,
			"nilai_komparatif_unit": 0.9986928852027274,
			"summary_team": 0.8714285714285712
		}]
	},
	"760054": {
		"nik": "760054",
		"nama": "CHRISTINA NOVITARINI",
		"band": "III",
		"posisi": "MGR MS BUSINESS PERFORMANCE & EVALUATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8823529411764705,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9822222222222223,
			"nilai_komparatif_unit": 0.9874271520203366,
			"summary_team": 0.8666666666666666
		}]
	},
	"760056": {
		"nik": "760056",
		"nama": "SURIA RAHMAINI",
		"band": "III",
		"posisi": "MGR ORDER MANAGEMENT 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8740740740740751,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.042372881355931,
			"nilai_komparatif_unit": 1.047896557717723,
			"summary_team": 0.9111111111111111
		}]
	},
	"770002": {
		"nik": "770002",
		"nama": "DESY DWI PURNOMO, M.T",
		"band": "III",
		"posisi": "MGR WEST AREA PLATFORM O&M",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8041666666666654,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1530852567122019,
			"nilai_komparatif_unit": 1.1591956130823204,
			"summary_team": 0.9272727272727275
		}]
	},
	"770035": {
		"nik": "770035",
		"nama": "MOHAMAD ALIFIKRI, ST.",
		"band": "III",
		"posisi": "MGR IT & MARKETPLACE FULL&FAULT HANDLING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8148148148148151,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181815,
			"nilai_komparatif_unit": 0.9870209705509736,
			"summary_team": 0.8
		}]
	},
	"770044": {
		"nik": "770044",
		"nama": "YESSI MARLINA",
		"band": "III",
		"posisi": "SO BUSINESS GOVERNANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8869565217391299,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.989651416122005,
			"nilai_komparatif_unit": 0.9948957142339592,
			"summary_team": 0.8777777777777778
		}]
	},
	"770047": {
		"nik": "770047",
		"nama": "NISA TUNGGADEWI",
		"band": "III",
		"posisi": "SO PERFORMANCE ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.893333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9594882729211089,
			"nilai_komparatif_unit": 0.9645727324147757,
			"summary_team": 0.857142857142857
		}]
	},
	"770060": {
		"nik": "770060",
		"nama": "DON FERNANDO",
		"band": "III",
		"posisi": "MGR IPTV PRODUCT PLANNING & DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8878787878787874,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9937763501304966,
			"nilai_komparatif_unit": 0.9990425068315258,
			"summary_team": 0.8823529411764707
		}]
	},
	"770062": {
		"nik": "770062",
		"nama": "KRISTIN THERESIA SIANTURI",
		"band": "III",
		"posisi": "MGR SEGMENT SUPPORT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8539007092198568,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9807931893687724,
			"nilai_komparatif_unit": 0.985990546526487,
			"summary_team": 0.8375
		}]
	},
	"770073": {
		"nik": "770073",
		"nama": "ERWIN AZIS",
		"band": "III",
		"posisi": "MGR NTE MGT & BUSINESS ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139858,
			"nilai_komparatif_unit": 0.9912390088866616,
			"summary_team": 0.8545454545454546
		}]
	},
	"770083": {
		"nik": "770083",
		"nama": "BENY CANDRA FEBRIANTO",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8553191489361723,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9854299928926767,
			"nilai_komparatif_unit": 0.9906519211059871,
			"summary_team": 0.8428571428571426
		}]
	},
	"770084": {
		"nik": "770084",
		"nama": "FERY KURNIAWAN",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8553191489361723,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0203527815468088,
			"nilai_komparatif_unit": 1.0257597703901749,
			"summary_team": 0.8727272727272727
		}]
	},
	"770085": {
		"nik": "770085",
		"nama": "IRMA SILVIA ADYATINI",
		"band": "III",
		"posisi": "MGR ES COLLECTION & DEBT MGT SEGMENT A",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7745098039215677,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.014466546112117,
			"nilai_komparatif_unit": 1.0198423429894292,
			"summary_team": 0.7857142857142856
		}]
	},
	"775667": {
		"nik": "775667",
		"nama": "YOHANES INDRASARI",
		"band": "III",
		"posisi": "MGR BROADCAST INFRA & FACILITY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8296296296296292,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8889508928571435,
			"nilai_komparatif_unit": 0.8936615651333462,
			"summary_team": 0.7375
		}]
	},
	"780029": {
		"nik": "780029",
		"nama": "BANGUN KUNTORO, ST.",
		"band": "III",
		"posisi": "MGR SALES",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8539007092198568,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.905647840531563,
			"nilai_komparatif_unit": 0.9104469922155025,
			"summary_team": 0.7733333333333334
		}]
	},
	"780036": {
		"nik": "780036",
		"nama": "JOSEPHINE YVONNE",
		"band": "III",
		"posisi": "MGR INDUSTRIAL ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8553191489361723,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0230099502487537,
			"nilai_komparatif_unit": 1.0284310197922326,
			"summary_team": 0.875
		}]
	},
	"780038": {
		"nik": "780038",
		"nama": "CESILIA KRISAN",
		"band": "III",
		"posisi": "MGR FINANCE PLAN & EVALUATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7833333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0444874274661509,
			"nilai_komparatif_unit": 1.0500223090967808,
			"summary_team": 0.8181818181818181
		}]
	},
	"780053": {
		"nik": "780053",
		"nama": "EKA SAPUTRA",
		"band": "III",
		"posisi": "MANAGER CARRIER ENT SVC REV ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8476190476190469,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9787765293383281,
			"nilai_komparatif_unit": 0.9839631999389199,
			"summary_team": 0.8296296296296298
		}]
	},
	"780059": {
		"nik": "780059",
		"nama": "ANDRI KURNIA RIYADI",
		"band": "III",
		"posisi": "SO PRODUCT DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8966666666666664,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9559214020180564,
			"nilai_komparatif_unit": 0.9609869601753155,
			"summary_team": 0.857142857142857
		}]
	},
	"780067": {
		"nik": "780067",
		"nama": "YANUAR SETIO LAKSONO",
		"band": "III",
		"posisi": "MGR MS BUSINESS DEVELOPMENT & MODELLING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8823529411764705,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0635897435897437,
			"nilai_komparatif_unit": 1.0692258510042698,
			"summary_team": 0.9384615384615383
		}]
	},
	"780068": {
		"nik": "780068",
		"nama": "WITA ANGGRAINI",
		"band": "III",
		"posisi": "MGR OPERATION CONTACT CENTER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8799999999999992,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9886363636363645,
			"nilai_komparatif_unit": 0.9938752828464676,
			"summary_team": 0.87
		}]
	},
	"780070": {
		"nik": "780070",
		"nama": "WINIE SEPTANTRI",
		"band": "III",
		"posisi": "SENIOR STAFF III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8362318840579697,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8697022215219802,
			"nilai_komparatif_unit": 0.8743108924580179,
			"summary_team": 0.7272727272727272
		}]
	},
	"790004": {
		"nik": "790004",
		"nama": "MUHAMMAD HAMKA",
		"band": "III",
		"posisi": "MGR COMM & PERF REG KTI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606060606060597,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0070422535211279,
			"nilai_komparatif_unit": 1.0123787080573055,
			"summary_team": 0.8666666666666667
		}]
	},
	"790030": {
		"nik": "790030",
		"nama": "ARIS PRAMONO",
		"band": "III",
		"posisi": "MGR CUSTOMER DATA MANAGEMENT & SOLUTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8461538461538465,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9827751196172244,
			"nilai_komparatif_unit": 0.9879829792942005,
			"summary_team": 0.831578947368421
		}]
	},
	"790037": {
		"nik": "790037",
		"nama": "THERESIA DINA GIRININGTYAS, ST.",
		"band": "III",
		"posisi": "MGR MARKETING COMMUNICATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8362318840579697,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0079227531567234,
			"nilai_komparatif_unit": 1.01326387358081,
			"summary_team": 0.8428571428571427
		}]
	},
	"790040": {
		"nik": "790040",
		"nama": "FRISDA NURAFNI, ST.",
		"band": "III",
		"posisi": "SO CX STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9133333333333329,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.993823694553622,
			"nilai_komparatif_unit": 0.99909010213922,
			"summary_team": 0.9076923076923076
		}]
	},
	"790059": {
		"nik": "790059",
		"nama": "RIO AGUS WILIANTO",
		"band": "III",
		"posisi": "SO PORTFOLIO MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8433333333333329,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9655561829474876,
			"nilai_komparatif_unit": 0.9706727971256993,
			"summary_team": 0.8142857142857142
		}]
	},
	"790061": {
		"nik": "790061",
		"nama": "HENDRIANA",
		"band": "III",
		"posisi": "MGR LEGAL & COMPLIANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8345679012345671,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0384615384615392,
			"nilai_komparatif_unit": 1.0439644880827617,
			"summary_team": 0.8666666666666666
		}]
	},
	"790062": {
		"nik": "790062",
		"nama": "WENI NOVIA MARDANTI",
		"band": "III",
		"posisi": "MGR IT BROADCAST DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8133333333333329,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.028315946348734,
			"nilai_komparatif_unit": 1.0337651330907294,
			"summary_team": 0.8363636363636365
		}]
	},
	"790063": {
		"nik": "790063",
		"nama": "WIDYA RICKARDA",
		"band": "III",
		"posisi": "MANAGER PROCUREMENT ADMINISTRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8619047619047622,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9613259668508286,
			"nilai_komparatif_unit": 0.9664201645357908,
			"summary_team": 0.8285714285714287
		}]
	},
	"790065": {
		"nik": "790065",
		"nama": "DEWI PUSPITASARI",
		"band": "III",
		"posisi": "MGR OFFERING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333321,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9667968750000016,
			"nilai_komparatif_unit": 0.9719200637749678,
			"summary_team": 0.8250000000000001
		}]
	},
	"790077": {
		"nik": "790077",
		"nama": "REZA PRAKASA",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8878048780487822,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9976452119309243,
			"nilai_komparatif_unit": 1.0029318702594001,
			"summary_team": 0.8857142857142857
		}]
	},
	"790079": {
		"nik": "790079",
		"nama": "EVY YULIA RAMADHAN",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8949771689497721,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9751391465677175,
			"nilai_komparatif_unit": 0.9803065421798783,
			"summary_team": 0.8727272727272727
		}]
	},
	"790080": {
		"nik": "790080",
		"nama": "LAKSMI JUWITA",
		"band": "III",
		"posisi": "MGR OSS & BSS OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8456140350877192,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9756224066390042,
			"nilai_komparatif_unit": 0.9807923631123311,
			"summary_team": 0.825
		}]
	},
	"790081": {
		"nik": "790081",
		"nama": "EDVYA NOER",
		"band": "III",
		"posisi": "MGR OFFERING",
		"category": [{
			"code": "CF-3-01",
			"kriteria": 0.917283950617284,
			"kriteria_bp": 0.9955470402588408,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0492934051144012,
			"nilai_komparatif_unit": 1.0539867657500006,
			"summary_team": 0.9625
		}]
	},
	"790084": {
		"nik": "790084",
		"nama": "NYAYU RODIAH UTAMI",
		"band": "III",
		"posisi": "MANAGER FINANCIAL AUDIT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8571428571428573,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9888888888888887,
			"nilai_komparatif_unit": 0.9941291462648182,
			"summary_team": 0.8476190476190476
		}]
	},
	"790103": {
		"nik": "790103",
		"nama": "I MADE TISNA JATMIKA",
		"band": "III",
		"posisi": "MGR DEPLOYMENT & PROJECT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8770833333333327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9609772650152705,
			"nilai_komparatif_unit": 0.9660696148815482,
			"summary_team": 0.8428571428571429
		}]
	},
	"790123": {
		"nik": "790123",
		"nama": "RINALDI NAINGGOLAN",
		"band": "III",
		"posisi": "MGR SELFCARE & DIGITAL INTERACTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8799999999999992,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0962566844919794,
			"nilai_komparatif_unit": 1.102065898491012,
			"summary_team": 0.9647058823529411
		}]
	},
	"800004": {
		"nik": "800004",
		"nama": "MARYANTO EKO SAPUTRO",
		"band": "III",
		"posisi": "MGR PROV & MIG SEMARANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555557,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9506493506493505,
			"nilai_komparatif_unit": 0.9556869714858635,
			"summary_team": 0.8133333333333334
		}]
	},
	"800017": {
		"nik": "800017",
		"nama": "ALMIAH SYAM",
		"band": "III",
		"posisi": "MGR PROCUREMENT REG KTI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606060606060597,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0225352112676065,
			"nilai_komparatif_unit": 1.0279537651043407,
			"summary_team": 0.8799999999999999
		}]
	},
	"800023": {
		"nik": "800023",
		"nama": "AMRAN QURAISY",
		"band": "III",
		"posisi": "GM TA SULBAGUT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606060606060597,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0070422535211279,
			"nilai_komparatif_unit": 1.0123787080573055,
			"summary_team": 0.8666666666666667
		}]
	},
	"800033": {
		"nik": "800033",
		"nama": "AMIN KARUNIYAWAN",
		"band": "III",
		"posisi": "MGR ASSURANCE & MAINTENANCE SUPERVISION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9466666666666667,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9750812567713977,
			"nilai_komparatif_unit": 0.9802483456176159,
			"summary_team": 0.9230769230769231
		}]
	},
	"800047": {
		"nik": "800047",
		"nama": "RAKHMAT FIRDAUS",
		"band": "III",
		"posisi": "MGR OTT SERVICE FULFILLMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9563164108618666,
			"nilai_komparatif_unit": 0.9613840622249757,
			"summary_team": 0.8181818181818183
		}]
	},
	"800054": {
		"nik": "800054",
		"nama": "I DEWA MADE AGUS PRAMANA",
		"band": "III",
		"posisi": "MGR MANAGED SERVICE DELIVERY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8839506172839499,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9050279329608945,
			"nilai_komparatif_unit": 0.9098237996698928,
			"summary_team": 0.7999999999999998
		}]
	},
	"800057": {
		"nik": "800057",
		"nama": "AMY ROOSITA MARYANA",
		"band": "III",
		"posisi": "SO PRODUCT DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8644444444444435,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9662785422652361,
			"nilai_komparatif_unit": 0.971398984324199,
			"summary_team": 0.8352941176470587
		}]
	},
	"800061": {
		"nik": "800061",
		"nama": "DANANJAYA",
		"band": "III",
		"posisi": "MGR CX & PARTNER PERFORMANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555552,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.080161218092253,
			"nilai_komparatif_unit": 1.0858851400150293,
			"summary_team": 0.9241379310344827
		}]
	},
	"800065": {
		"nik": "800065",
		"nama": "FITRI HIDAYATI",
		"band": "III",
		"posisi": "MGR BILLING VALIDATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333328,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0080000000000007,
			"nilai_komparatif_unit": 1.0133415297656672,
			"summary_team": 0.84
		}]
	},
	"800066": {
		"nik": "800066",
		"nama": "DWI ADHI SETIONO",
		"band": "III",
		"posisi": "MGR CONNECTIVITY PERFORMANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8454545454545451,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9964157706093197,
			"nilai_komparatif_unit": 1.001695913960202,
			"summary_team": 0.8424242424242427
		}]
	},
	"800069": {
		"nik": "800069",
		"nama": "EKO SUHARDIANTO",
		"band": "III",
		"posisi": "MGR STRATEGIC INVESTMENT PLAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8379487179487183,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.140350877192982,
			"nilai_komparatif_unit": 1.1463937523455914,
			"summary_team": 0.9555555555555556
		}]
	},
	"800076": {
		"nik": "800076",
		"nama": "FITRAH ANSORI NASUTION",
		"band": "III",
		"posisi": "MGR SHARED SERVICE SUMBAGTENG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.84,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9706959706959707,
			"nilai_komparatif_unit": 0.9758398213119281,
			"summary_team": 0.8153846153846154
		}]
	},
	"800078": {
		"nik": "800078",
		"nama": "WANDI HARDIAN",
		"band": "III",
		"posisi": "MGR CONNECTIVITY PLANNING & DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8456140350877192,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0156211862338298,
			"nilai_komparatif_unit": 1.0210031017069543,
			"summary_team": 0.8588235294117648
		}]
	},
	"800085": {
		"nik": "800085",
		"nama": "IKHSAN",
		"band": "III",
		"posisi": "MGR PROV & MIG BANDA ACEH",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8888888888888891,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0049999999999997,
			"nilai_komparatif_unit": 1.0103256323556493,
			"summary_team": 0.8933333333333333
		}]
	},
	"800087": {
		"nik": "800087",
		"nama": "ISDIANTO",
		"band": "III",
		"posisi": "MGR HCM REG KALIMANTAN",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8515151515151508,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9395017793594315,
			"nilai_komparatif_unit": 0.9444803276921151,
			"summary_team": 0.8
		}]
	},
	"805528": {
		"nik": "805528",
		"nama": "TITIS RETNO HAPSARI",
		"band": "III",
		"posisi": "MGR BUSINESS PLANNING & BUDGET CTRL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.856249999999999,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9980092899800943,
			"nilai_komparatif_unit": 1.0032978776079122,
			"summary_team": 0.8545454545454547
		}]
	},
	"810003": {
		"nik": "810003",
		"nama": "M. SURATMAN R.",
		"band": "III",
		"posisi": "GM TA PAPUA & AMBON",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606060606060597,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0380281690140856,
			"nilai_komparatif_unit": 1.0435288221513763,
			"summary_team": 0.8933333333333333
		}]
	},
	"810005": {
		"nik": "810005",
		"nama": "MUHAMMAD JATI NAQOSHO",
		"band": "III",
		"posisi": "MGR WAREHOUSE & DISTRIBUTION MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0307692307692307,
			"nilai_komparatif_unit": 1.0362314178006662,
			"summary_team": 0.8933333333333334
		}]
	},
	"810007": {
		"nik": "810007",
		"nama": "RIZKIYANTO",
		"band": "III",
		"posisi": "MGR OTT PRODUCT PLANNING & DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8878787878787874,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9332033154558759,
			"nilai_komparatif_unit": 0.9381484873675091,
			"summary_team": 0.8285714285714287
		}]
	},
	"810020": {
		"nik": "810020",
		"nama": "ENI RIAWATI SEBAYANG, ST",
		"band": "III",
		"posisi": "SO PRODUCT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8644444444444435,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.020716769998489,
			"nilai_komparatif_unit": 1.0261256876664075,
			"summary_team": 0.8823529411764706
		}]
	},
	"810035": {
		"nik": "810035",
		"nama": "RAHAYU DIAH RATNAWATI",
		"band": "III",
		"posisi": "MGR INVOICING & COLLECTION MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333328,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9760000000000006,
			"nilai_komparatif_unit": 0.981171957392154,
			"summary_team": 0.8133333333333334
		}]
	},
	"810040": {
		"nik": "810040",
		"nama": "MUTIA RAHMA",
		"band": "III",
		"posisi": "MGR CARRIER SERVICE CAPACITY PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8876190476190465,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9012875536480697,
			"nilai_komparatif_unit": 0.9060635995758831,
			"summary_team": 0.7999999999999999
		}]
	},
	"810043": {
		"nik": "810043",
		"nama": "E. SITI MAEMUNAH",
		"band": "III",
		"posisi": "MGR FRAUD & AUDIT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8641025641025636,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0299703264094962,
			"nilai_komparatif_unit": 1.0354282799375414,
			"summary_team": 0.89
		}]
	},
	"810048": {
		"nik": "810048",
		"nama": "WILLY KOESPRASETYO",
		"band": "III",
		"posisi": "SR ACCOUNT MANAGER VOICE & MOBILITY I",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8111111111111113,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9343907714491704,
			"nilai_komparatif_unit": 0.9393422358524044,
			"summary_team": 0.7578947368421051
		}]
	},
	"810053": {
		"nik": "810053",
		"nama": "ABDURRAHMAN",
		"band": "III",
		"posisi": "MGR KONSTRUKSI JAKARTA TIMUR",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8515151515151508,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9214344374486729,
			"nilai_komparatif_unit": 0.9263172444672665,
			"summary_team": 0.7846153846153845
		}]
	},
	"810066": {
		"nik": "810066",
		"nama": "RIEKY ZAINAL",
		"band": "III",
		"posisi": "MGR RESOURCE MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8345679012345671,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0138825671370058,
			"nilai_komparatif_unit": 1.0192552694299153,
			"summary_team": 0.846153846153846
		}]
	},
	"810071": {
		"nik": "810071",
		"nama": "FARULINA DIANA DEWI",
		"band": "III",
		"posisi": "MGR BUSINESS ALLIANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7716049382716039,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1145600000000015,
			"nilai_komparatif_unit": 1.1204662057694672,
			"summary_team": 0.86
		}]
	},
	"810075": {
		"nik": "810075",
		"nama": "ADI SELAMET RAHMADI",
		"band": "III",
		"posisi": "SR ACCOUNT MANAGER VOICE & MOBILITY II",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9531249999999999,
			"nilai_komparatif_unit": 0.9581757396407746,
			"summary_team": 0.7625
		}]
	},
	"810084": {
		"nik": "810084",
		"nama": "ABDILLAH RIFQI",
		"band": "III",
		"posisi": "VP RISK MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.942028985507246,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9472189349112429,
			"nilai_komparatif_unit": 0.9522383775059169,
			"summary_team": 0.8923076923076922
		}]
	},
	"810108": {
		"nik": "810108",
		"nama": "BENEDICTUS DAMAR SURYO LAKSONO",
		"band": "III",
		"posisi": "MGR IT DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8848484848484851,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.043203371970495,
			"nilai_komparatif_unit": 1.0487314492155586,
			"summary_team": 0.9230769230769231
		}]
	},
	"820008": {
		"nik": "820008",
		"nama": "MUHAMMAD HAKY RUFIANTO",
		"band": "III",
		"posisi": "MGR OTT SERVICE ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8555555555555546,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9701298701298713,
			"nilai_komparatif_unit": 0.9752707209015588,
			"summary_team": 0.8300000000000001
		}]
	},
	"820012": {
		"nik": "820012",
		"nama": "TIARA PRIHARTINI",
		"band": "III",
		"posisi": "MGR TRAFFIC PARTNERSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8493827160493818,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0143112701252246,
			"nilai_komparatif_unit": 1.0196862441738606,
			"summary_team": 0.8615384615384614
		}]
	},
	"820018": {
		"nik": "820018",
		"nama": "DIANANDA FITRIA PITARI",
		"band": "III",
		"posisi": "EXECUTIVE ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.845714285714285,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0090090090090098,
			"nilai_komparatif_unit": 1.0143558856513186,
			"summary_team": 0.8533333333333333
		}]
	},
	"820022": {
		"nik": "820022",
		"nama": "LELY DIANA",
		"band": "III",
		"posisi": "MGR UI/UX DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8133333333333329,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0171385991058128,
			"nilai_komparatif_unit": 1.0225285555571344,
			"summary_team": 0.8272727272727274
		}]
	},
	"820067": {
		"nik": "820067",
		"nama": "NOVI PURNAMA SARI",
		"band": "III",
		"posisi": "MGR NETWORK BUSINESS SOLUTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8493827160493818,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0046511627906989,
			"nilai_komparatif_unit": 1.0099749466103,
			"summary_team": 0.8533333333333334
		}]
	},
	"820074": {
		"nik": "820074",
		"nama": "TAUFIQ AJI HIDAYAT",
		"band": "III",
		"posisi": "MGR ASSURANCE & MAINTENANCE YOGYAKARTA",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8705882352941177,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0108108108108107,
			"nilai_komparatif_unit": 1.0161672354471236,
			"summary_team": 0.88
		}]
	},
	"820077": {
		"nik": "820077",
		"nama": "DWI ANGGRENI",
		"band": "III",
		"posisi": "MGR IPTV SERVICE FULLFILMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8041666666666654,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.028732925105984,
			"nilai_komparatif_unit": 1.0341843214754034,
			"summary_team": 0.8272727272727275
		}]
	},
	"830003": {
		"nik": "830003",
		"nama": "CECEP NURYANTO",
		"band": "III",
		"posisi": "MGR MANAGED SERVICE ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8839506172839499,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0558659217877102,
			"nilai_komparatif_unit": 1.061461099614875,
			"summary_team": 0.9333333333333332
		}]
	},
	"830029": {
		"nik": "830029",
		"nama": "SHINTA ARIESTIKA",
		"band": "III",
		"posisi": "MGR ADVANCED MANAGED SERVICE OPERATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8839506172839499,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0094542329179208,
			"nilai_komparatif_unit": 1.0148034688625727,
			"summary_team": 0.8923076923076922
		}]
	},
	"830052": {
		"nik": "830052",
		"nama": "TRISANDY PAMUNGKAS",
		"band": "III",
		"posisi": "MGR SERVICE DELIVERY CONSUMER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8733333333333336,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0381679389312972,
			"nilai_komparatif_unit": 1.043669332728482,
			"summary_team": 0.9066666666666666
		}]
	},
	"830074": {
		"nik": "830074",
		"nama": "ANDI RIZQI",
		"band": "III",
		"posisi": "MGR IPTV INFRASTRUCTURE DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8133333333333329,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0204918032786892,
			"nilai_komparatif_unit": 1.0258995288172128,
			"summary_team": 0.8300000000000001
		}]
	},
	"830094": {
		"nik": "830094",
		"nama": "DEWI HAJAR",
		"band": "III",
		"posisi": "SO INVESTMENT ANALYSIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333328,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.037946428571429,
			"nilai_komparatif_unit": 1.0434466485549425,
			"summary_team": 0.8857142857142857
		}]
	},
	"830097": {
		"nik": "830097",
		"nama": "AYUNITRI KURNIATI",
		"band": "III",
		"posisi": "MGR BUSINESS PERFORMANCE & SYNERGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.856249999999999,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9767750497677519,
			"nilai_komparatif_unit": 0.9819511142545524,
			"summary_team": 0.8363636363636365
		}]
	},
	"830099": {
		"nik": "830099",
		"nama": "BURHAN JUNIARAHMAN",
		"band": "III",
		"posisi": "MGR FINANCE REG KTI",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8606060606060597,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0140845070422546,
			"nilai_komparatif_unit": 1.0194582794423215,
			"summary_team": 0.8727272727272727
		}]
	},
	"830119": {
		"nik": "830119",
		"nama": "MUHAMMAD IKSAN",
		"band": "III",
		"posisi": "MGR INV & ASSET MGT REG JAWA TENGAH",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9142857142857135,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9423076923076932,
			"nilai_komparatif_unit": 0.9473011095565802,
			"summary_team": 0.8615384615384616
		}]
	},
	"830130": {
		"nik": "830130",
		"nama": "SIGIT PRAMONO",
		"band": "III",
		"posisi": "GM TA SUMBAGSEL",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8411764705882342,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9510489510489523,
			"nilai_komparatif_unit": 0.9560886894225971,
			"summary_team": 0.7999999999999999
		}]
	},
	"830139": {
		"nik": "830139",
		"nama": "MUHAMMAD ZEN",
		"band": "III",
		"posisi": "MGR PROV & MIG PALEMBANG",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8111111111111113,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0520547945205476,
			"nilai_komparatif_unit": 1.0576297766634482,
			"summary_team": 0.8533333333333334
		}]
	},
	"830149": {
		"nik": "830149",
		"nama": "TANTRI ARMA FITRI",
		"band": "III",
		"posisi": "MGR ON AIR PROMOTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8083333333333336,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0073637702503682,
			"nilai_komparatif_unit": 1.0127019285476364,
			"summary_team": 0.8142857142857145
		}]
	},
	"830155": {
		"nik": "830155",
		"nama": "BERRY RAHADITIO",
		"band": "III",
		"posisi": "MGR BUSINESS PERFORMANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.845714285714285,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0005197505197512,
			"nilai_komparatif_unit": 1.005821641421079,
			"summary_team": 0.846153846153846
		}]
	},
	"830156": {
		"nik": "830156",
		"nama": "ANDJAS WAHYU ARDIANSYAH",
		"band": "III",
		"posisi": "MGR IPTV SERVICE ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8041666666666654,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.00612341026849,
			"nilai_komparatif_unit": 1.0114549957286914,
			"summary_team": 0.8090909090909093
		}]
	},
	"835869": {
		"nik": "835869",
		"nama": "FINNA NANCY WAHYUNI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7716049382716039,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9661090909090922,
			"nilai_komparatif_unit": 0.9712286350221597,
			"summary_team": 0.7454545454545455
		}]
	},
	"840092": {
		"nik": "840092",
		"nama": "INTAN KARTIKA KUSUMAWARDHANI",
		"band": "III",
		"posisi": "SO BUSINESS SOLUTION MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8844444444444437,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9577298255985818,
			"nilai_komparatif_unit": 0.9628049668395554,
			"summary_team": 0.8470588235294116
		}]
	},
	"840114": {
		"nik": "840114",
		"nama": "MIRASARI ASTUTI",
		"band": "III",
		"posisi": "MGR DIGITAL SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8904761904761904,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0106951871657754,
			"nilai_komparatif_unit": 1.016050999096591,
			"summary_team": 0.9
		}]
	},
	"840121": {
		"nik": "840121",
		"nama": "RUMONDANG AGUSTINA",
		"band": "III",
		"posisi": "MANAGER PARTNERSHIP & SOURCING I",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8238095238095239,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9600630583289543,
			"nilai_komparatif_unit": 0.9651505636890545,
			"summary_team": 0.790909090909091
		}]
	},
	"840124": {
		"nik": "840124",
		"nama": "DINI ANDRIANI",
		"band": "III",
		"posisi": "MANAGER ASSET MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.851851851851852,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9913043478260871,
			"nilai_komparatif_unit": 0.9965574050490511,
			"summary_team": 0.8444444444444447
		}]
	},
	"840138": {
		"nik": "840138",
		"nama": "DETRIANA MARGITA SARI",
		"band": "III",
		"posisi": "MANAGER CORPORATE STRATEGIC PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.7966666666666664,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0041841004184104,
			"nilai_komparatif_unit": 1.0095054092106663,
			"summary_team": 0.8
		}]
	},
	"840147": {
		"nik": "840147",
		"nama": "AGUS SRI BUDI CAHYONO",
		"band": "III",
		"posisi": "MANAGER PRESALES SOLT. III",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8222222222222224,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0015898251192366,
			"nilai_komparatif_unit": 1.0068973864921167,
			"summary_team": 0.8235294117647058
		}]
	},
	"840154": {
		"nik": "840154",
		"nama": "RIZKA AULIA",
		"band": "III",
		"posisi": "MGR SHARED SERVICE BBS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8181818181818185,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0388888888888888,
			"nilai_komparatif_unit": 1.0443941030984327,
			"summary_team": 0.8500000000000002
		}]
	},
	"840184": {
		"nik": "840184",
		"nama": "LUTHFI KURNIADI",
		"band": "III",
		"posisi": "MGR BUSINESS DEVELOPMENT & INNOVATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.845714285714285,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9641372141372149,
			"nilai_komparatif_unit": 0.9692463090057672,
			"summary_team": 0.8153846153846153
		}]
	},
	"840189": {
		"nik": "840189",
		"nama": "SONIA ALINA HERININGKATRI",
		"band": "III",
		"posisi": "MGR SEG SUPP ADVANCED MANAGED SERVICE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9294117647058822,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0221518987341773,
			"nilai_komparatif_unit": 1.0275684213454086,
			"summary_team": 0.95
		}]
	},
	"840190": {
		"nik": "840190",
		"nama": "ANTO MARTUA CHRISTIAN SIHOMBING",
		"band": "III",
		"posisi": "MGR MARKET RESEARCH & BUSINESS INTELLIGE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.856249999999999,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1041804910418065,
			"nilai_komparatif_unit": 1.1100316943747113,
			"summary_team": 0.9454545454545457
		}]
	},
	"840194": {
		"nik": "840194",
		"nama": "KRISTIAN LUAS SAUTAN JEFRY",
		"band": "III",
		"posisi": "MGR SALES OPERATION SOLUTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8453333333333325,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0173501577287076,
			"nilai_komparatif_unit": 1.022741235258086,
			"summary_team": 0.86
		}]
	},
	"845988": {
		"nik": "845988",
		"nama": "ARIEF WICAKSONO",
		"band": "III",
		"posisi": "MGR CONTENT ACQUISITION & DITRIBUTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8083333333333336,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8659793814432987,
			"nilai_komparatif_unit": 0.8705683245409503,
			"summary_team": 0.7
		}]
	},
	"846112": {
		"nik": "846112",
		"nama": "SHEPTY LIANA",
		"band": "III",
		"posisi": "SENIOR EXPERT CREATIVE & PRODUCTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8296296296296292,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8638392857142864,
			"nilai_komparatif_unit": 0.8684168881521782,
			"summary_team": 0.7166666666666668
		}]
	},
	"850056": {
		"nik": "850056",
		"nama": "HADI SUSILO",
		"band": "III",
		"posisi": "GM TA PONTIANAK",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8515151515151508,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.986476868327403,
			"nilai_komparatif_unit": 0.9917043440767208,
			"summary_team": 0.84
		}]
	},
	"850075": {
		"nik": "850075",
		"nama": "FARIDA KARTIKA SARI",
		"band": "III",
		"posisi": "MGR LEGAL & RISK MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0080971659919038,
			"nilai_komparatif_unit": 1.0134392106534413,
			"summary_team": 0.8736842105263158
		}]
	},
	"850082": {
		"nik": "850082",
		"nama": "ULFAH MUSFIRAH",
		"band": "III",
		"posisi": "MGR DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8848484848484851,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9493150684931504,
			"nilai_komparatif_unit": 0.9543456187861583,
			"summary_team": 0.84
		}]
	},
	"850085": {
		"nik": "850085",
		"nama": "LIDYA SARI",
		"band": "III",
		"posisi": "MGR PERFORMANCE & REVAS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8641025641025636,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9643916913946594,
			"nilai_komparatif_unit": 0.969502134772979,
			"summary_team": 0.8333333333333334
		}]
	},
	"850092": {
		"nik": "850092",
		"nama": "NIKEN PRATIWI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9060109289617485,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0517987653444976,
			"nilai_komparatif_unit": 1.0573723907538022,
			"summary_team": 0.9529411764705884
		}]
	},
	"850107": {
		"nik": "850107",
		"nama": "SABARIAH",
		"band": "III",
		"posisi": "SO PARTNERSHIP STRATEGY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8844444444444439,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.02423884126515,
			"nilai_komparatif_unit": 1.0296664228700803,
			"summary_team": 0.9058823529411766
		}]
	},
	"850113": {
		"nik": "850113",
		"nama": "AGUNG TRI NUGROHO",
		"band": "III",
		"posisi": "MGR SURVEY & DRAWING EAST",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8444444444444446,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.994736842105263,
			"nilai_komparatif_unit": 1.0000080885845393,
			"summary_team": 0.84
		}]
	},
	"850150": {
		"nik": "850150",
		"nama": "ABBAS PAUL RICARDO GIRSANG",
		"band": "III",
		"posisi": "MGR NETWORK PARTNERSHIP",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8493827160493818,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9961985688729884,
			"nilai_komparatif_unit": 1.0014775612421842,
			"summary_team": 0.846153846153846
		}]
	},
	"860015": {
		"nik": "860015",
		"nama": "ROMMI SYAFARI",
		"band": "III",
		"posisi": "MGR VERIFICATION & SETTLEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8333333333333328,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0880000000000005,
			"nilai_komparatif_unit": 1.0937654606994502,
			"summary_team": 0.9066666666666665
		}]
	},
	"860057": {
		"nik": "860057",
		"nama": "FARIS ARIF RAHMAN",
		"band": "III",
		"posisi": "MGR BUSINESS FEASIBILITY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.6857142857142859,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.2462121212121209,
			"nilai_komparatif_unit": 1.2528159695650862,
			"summary_team": 0.8545454545454546
		}]
	},
	"860058": {
		"nik": "860058",
		"nama": "DERY PARTONI",
		"band": "III",
		"posisi": "MANAGER EXE ASSISTANT & KNOWLEDGE MGT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8044444444444433,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9944751381215484,
			"nilai_komparatif_unit": 0.9997449977956473,
			"summary_team": 0.8
		}]
	},
	"860063": {
		"nik": "860063",
		"nama": "FEBRIAN SETIADI",
		"band": "III",
		"posisi": "MGR CONNECTIVITY READINESS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8770833333333327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.019421545340227,
			"nilai_komparatif_unit": 1.0248235994356603,
			"summary_team": 0.8941176470588236
		}]
	},
	"860068": {
		"nik": "860068",
		"nama": "RADEN MEILIDA NADIA",
		"band": "III",
		"posisi": "MGR COLLECTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8523809523809527,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0206703910614523,
			"nilai_komparatif_unit": 1.0260790629610448,
			"summary_team": 0.8700000000000001
		}]
	},
	"860074": {
		"nik": "860074",
		"nama": "PUTRI KUSUMAPRATIWI",
		"band": "III",
		"posisi": "MGR SETTLEMENT & RECONCILIATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000009,
			"nilai_komparatif_unit": 1.0052991366722892,
			"summary_team": 0.8666666666666667
		}]
	},
	"860081": {
		"nik": "860081",
		"nama": "ANISA YUSTIKASARI",
		"band": "III",
		"posisi": "MGR QUALITY MGT & REVENUE ASSURANCE",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.856249999999999,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.934306569343067,
			"nilai_komparatif_unit": 0.9392575875478326,
			"summary_team": 0.8000000000000002
		}]
	},
	"860086": {
		"nik": "860086",
		"nama": "GUSTI AYU MELIATI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8894308943089421,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9391332401333486,
			"nilai_komparatif_unit": 0.9441098355263041,
			"summary_team": 0.8352941176470587
		}]
	},
	"860090": {
		"nik": "860090",
		"nama": "FAJAR SYAMSUDIN",
		"band": "III",
		"posisi": "SO CAPEX MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8433333333333329,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9486166007905142,
			"nilai_komparatif_unit": 0.9536434498077047,
			"summary_team": 0.7999999999999999
		}]
	},
	"860098": {
		"nik": "860098",
		"nama": "ARIEF SUKMAWAN",
		"band": "III",
		"posisi": "MGR DIGITAL DELIVERY",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8876190476190465,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0064377682403445,
			"nilai_komparatif_unit": 1.011771019526403,
			"summary_team": 0.8933333333333333
		}]
	},
	"860131": {
		"nik": "860131",
		"nama": "FRANLY de FRETES",
		"band": "III",
		"posisi": "MGR BUSINESS PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.845714285714285,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0005197505197512,
			"nilai_komparatif_unit": 1.005821641421079,
			"summary_team": 0.846153846153846
		}]
	},
	"860137": {
		"nik": "860137",
		"nama": "BUDI ANDANASARI",
		"band": "III",
		"posisi": "MGR SERVICES, PAYROLL MGT & HCIS",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8533333333333334,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0096153846153846,
			"nilai_komparatif_unit": 1.0149654745249064,
			"summary_team": 0.8615384615384616
		}]
	},
	"860151": {
		"nik": "860151",
		"nama": "RADEN ARY RELYMAN",
		"band": "III",
		"posisi": "MANAGER PROGRAM MANAGEMENT OFFICE 2",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9599999999999999,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9548611111111114,
			"nilai_komparatif_unit": 0.9599210506419422,
			"summary_team": 0.9166666666666667
		}]
	},
	"860152": {
		"nik": "860152",
		"nama": "ELIZAR ARSAN",
		"band": "III",
		"posisi": "MANAGER STRATEGY & PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9714285714285713,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0294117647058825,
			"nilai_komparatif_unit": 1.0348667583391205,
			"summary_team": 1
		}]
	},
	"870005": {
		"nik": "870005",
		"nama": "DIAN KURNIA RIZKI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8833333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9674099485420249,
			"nilai_komparatif_unit": 0.9725363860774804,
			"summary_team": 0.8545454545454546
		}]
	},
	"870011": {
		"nik": "870011",
		"nama": "RADEN SARI EKA PRATIWI",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9294117647058822,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0172612197928657,
			"nilai_komparatif_unit": 1.0226518260279667,
			"summary_team": 0.9454545454545454
		}]
	},
	"870020": {
		"nik": "870020",
		"nama": "IMAN SETIADI",
		"band": "III",
		"posisi": "MGR WHOLESALE DATA MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.845714285714285,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0563063063063072,
			"nilai_komparatif_unit": 1.0619038177912243,
			"summary_team": 0.8933333333333333
		}]
	},
	"870021": {
		"nik": "870021",
		"nama": "I PUTU ANGGARA",
		"band": "III",
		"posisi": "MGR MIGRATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8733333333333336,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9160305343511448,
			"nilai_komparatif_unit": 0.9208847053486607,
			"summary_team": 0.8
		}]
	},
	"870025": {
		"nik": "870025",
		"nama": "NURUL HUDA",
		"band": "III",
		"posisi": "MGR SEGMENT SUPPORT CARRIER SERVICE OLO",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8000000000000002,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.005299136672288,
			"summary_team": 0.7999999999999999
		}]
	},
	"870030": {
		"nik": "870030",
		"nama": "RAHMAT FAUZI KAMAL",
		"band": "III",
		"posisi": "MGR QUAL & PERF MGT CONSTRUCTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.92,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.020066889632107,
			"nilai_komparatif_unit": 1.0254723634951435,
			"summary_team": 0.9384615384615385
		}]
	},
	"870070": {
		"nik": "870070",
		"nama": "REDHO PATI",
		"band": "III",
		"posisi": "MGR CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8277777777777782,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9319271332694146,
			"nilai_komparatif_unit": 0.936865542517223,
			"summary_team": 0.7714285714285714
		}]
	},
	"870075": {
		"nik": "870075",
		"nama": "BHARATA DWI CAHYANTO",
		"band": "III",
		"posisi": "MGR CONNECTIVITY FULFILMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8770833333333327,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0201275159394934,
			"nilai_komparatif_unit": 1.0255333110696185,
			"summary_team": 0.8947368421052633
		}]
	},
	"870076": {
		"nik": "870076",
		"nama": "ANGGA REZA FARDANA",
		"band": "III",
		"posisi": "MGR PLATFORM & IT PLANNING & DEVELOPMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.795555555555556,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9959174903308977,
			"nilai_komparatif_unit": 1.0011949932264834,
			"summary_team": 0.7923076923076924
		}]
	},
	"880001": {
		"nik": "880001",
		"nama": "AMAR BILHAQ",
		"band": "III",
		"posisi": "VP HC STRATEGY MGT & INDUSTRIAL RELATION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8476190476190472,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1434745030250653,
			"nilai_komparatif_unit": 1.149533930697872,
			"summary_team": 0.9692307692307691
		}]
	},
	"880005": {
		"nik": "880005",
		"nama": "ANNA TASIA",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.897435897435897,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.035630252100841,
			"nilai_komparatif_unit": 1.0411181983486797,
			"summary_team": 0.9294117647058824
		}]
	},
	"880041": {
		"nik": "880041",
		"nama": "AYRTON IKA RINJANI",
		"band": "III",
		"posisi": "SO BUSINESS SOLUTION",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8044444444444433,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9652258693532674,
			"nilai_komparatif_unit": 0.9703407331545986,
			"summary_team": 0.776470588235294
		}]
	},
	"880088": {
		"nik": "880088",
		"nama": "ANGGITA HAPSARI GRISATYA, ST, MS.",
		"band": "III",
		"posisi": "SO PRODUCT MANAGEMENT",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8966666666666664,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9081253319171536,
			"nilai_komparatif_unit": 0.9129376121665498,
			"summary_team": 0.8142857142857142
		}]
	},
	"880089": {
		"nik": "880089",
		"nama": "RHESA NANDA WARDHANA, BBA, MIntBus.",
		"band": "III",
		"posisi": "SENIOR ACCOUNT MANAGER",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.8833333333333326,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9262435677530025,
			"nilai_komparatif_unit": 0.9311518590103536,
			"summary_team": 0.8181818181818182
		}]
	},
	"890029": {
		"nik": "890029",
		"nama": "SUKMA NANDINI",
		"band": "III",
		"posisi": "SO PRODUCT STRATEGY & PLANNING",
		"category": [{
			"code": "MD-3-03",
			"kriteria": 0.9533333333333333,
			"kriteria_bp": 0.9947287961573017,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9890109890109888,
			"nilai_komparatif_unit": 0.994251893412153,
			"summary_team": 0.9428571428571426
		}]
	}
};