export const cbark25 = {
	"670491": {
		"nik": "670491",
		"nama": "DJOKO WINARSO",
		"band": "VI",
		"posisi": "STAFF FINANCE & GENERAL AFFAIR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8380952380952381,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9664772727272728,
			"nilai_komparatif_unit": 0.9696198408809925,
			"summary_team": 0.81
		}]
	},
	"755720": {
		"nik": "755720",
		"nama": "NANA SURYANA",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER-2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8547619047619041,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7232210686249688,
			"nilai_komparatif_unit": 0.7255726722917022,
			"summary_team": 0.618181818181818
		}]
	},
	"760063": {
		"nik": "760063",
		"nama": "SRI TITI HANDAYANI",
		"band": "VI",
		"posisi": "OFF 3 LEARNING EXPERIENCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272723,
			"nilai_komparatif_unit": 1.0260527416730074,
			"summary_team": 0.8999999999999999
		}]
	},
	"830163": {
		"nik": "830163",
		"nama": "FAHMI KHAUDZI",
		"band": "VI",
		"posisi": "OFF 3 SEKRETARIAT BOC",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8368421052631614,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0122086570477207,
			"nilai_komparatif_unit": 1.0154999239821012,
			"summary_team": 0.8470588235294119
		}]
	},
	"850025": {
		"nik": "850025",
		"nama": "BUNGARAN VERIANDO PURBA",
		"band": "VI",
		"posisi": "OFF 3 OLO FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0074626865671639,
			"nilai_komparatif_unit": 1.0107385216480373,
			"summary_team": 0.8999999999999999
		}]
	},
	"865879": {
		"nik": "865879",
		"nama": "JULIUS RUBEN A. P. SIBARANI",
		"band": "VI",
		"posisi": "OFF 3 TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.749999999999999,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.085714285714287,
			"nilai_komparatif_unit": 1.0892445613189023,
			"summary_team": 0.8142857142857141
		}]
	},
	"866140": {
		"nik": "866140",
		"nama": "LEDITYA MARINA FERISCA",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER-2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8874999999999998,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9942004971002487,
			"nilai_komparatif_unit": 0.9974332092485468,
			"summary_team": 0.8823529411764706
		}]
	},
	"866152": {
		"nik": "866152",
		"nama": "IVAN SANDY",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8318181818181802,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9617486338797832,
			"nilai_komparatif_unit": 0.9648758265350076,
			"summary_team": 0.7999999999999999
		}]
	},
	"870078": {
		"nik": "870078",
		"nama": "FEBI HARYADI",
		"band": "VI",
		"posisi": "OFF 3 DIG BUSINESS GOVERNANCE ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444438,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0030959752321988,
			"nilai_komparatif_unit": 1.0063576116470871,
			"summary_team": 0.8470588235294116
		}]
	},
	"877134": {
		"nik": "877134",
		"nama": "MEGA ANGGARA",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER-2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8461538461538458,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9024793388429755,
			"nilai_komparatif_unit": 0.9054138132581212,
			"summary_team": 0.7636363636363636
		}]
	},
	"877136": {
		"nik": "877136",
		"nama": "LEONARDO SILALAHI",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8285714285714281,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0172413793103454,
			"nilai_komparatif_unit": 1.0205490104916206,
			"summary_team": 0.8428571428571427
		}]
	},
	"880104": {
		"nik": "880104",
		"nama": "YAUMIL FAUZI, S.T., M.T.",
		"band": "VI",
		"posisi": "OFF 3 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9836065573770488,
			"nilai_komparatif_unit": 0.9868048225926193,
			"summary_team": 0.7999999999999999
		}]
	},
	"888327": {
		"nik": "888327",
		"nama": "RADINAL REZA",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333331,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8957845433255269,
			"nilai_komparatif_unit": 0.89869724914685,
			"summary_team": 0.7285714285714284
		}]
	},
	"888342": {
		"nik": "888342",
		"nama": "RIZAL AGUSTA",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.869135802469135,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8786157024793396,
			"nilai_komparatif_unit": 0.8814725826190848,
			"summary_team": 0.7636363636363637
		}]
	},
	"896568": {
		"nik": "896568",
		"nama": "FANNY DESTIANI",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER-2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7939393939393942,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8061068702290074,
			"nilai_komparatif_unit": 0.808727982851478,
			"summary_team": 0.64
		}]
	},
	"900148": {
		"nik": "900148",
		"nama": "STEPHEN AMOS SIMANGUNSONG",
		"band": "VI",
		"posisi": "OFF 3 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9740259740259736,
			"nilai_komparatif_unit": 0.9771930873076261,
			"summary_team": 0.857142857142857
		}]
	},
	"910246": {
		"nik": "910246",
		"nama": "Azza Maulydia",
		"band": "VI",
		"posisi": "OFFICER PROMOTIF, PREVENTIF & WAP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9638009049773755,
			"nilai_komparatif_unit": 0.9669347707349853,
			"summary_team": 0.8352941176470587
		}]
	},
	"910271": {
		"nik": "910271",
		"nama": "MOHAMMAD FULAZZAKY",
		"band": "VI",
		"posisi": "OFF 3 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9827770478065272,
			"summary_team": 0.8
		}]
	},
	"910283": {
		"nik": "910283",
		"nama": "ELLEN SAFITRI",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139859,
			"nilai_komparatif_unit": 0.9892200791514126,
			"summary_team": 0.8545454545454546
		}]
	},
	"910287": {
		"nik": "910287",
		"nama": "YENI PURTIKA SIMANJUNTAK",
		"band": "VI",
		"posisi": "OFF 3 PAYMENT & COLLECTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.972527472527472,
			"nilai_komparatif_unit": 0.9756897133271527,
			"summary_team": 0.8428571428571427
		}]
	},
	"916716": {
		"nik": "916716",
		"nama": "NOVENTI ERSA PUTRI",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8099999999999994,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9876543209876549,
			"nilai_komparatif_unit": 0.9908657477884746,
			"summary_team": 0.7999999999999999
		}]
	},
	"920328": {
		"nik": "920328",
		"nama": "ANITA FITRIA",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8318181818181802,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9617486338797832,
			"nilai_komparatif_unit": 0.9648758265350076,
			"summary_team": 0.7999999999999999
		}]
	},
	"920356": {
		"nik": "920356",
		"nama": "PUTU DEWANTI PRIMAYANI",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7833333333333332,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8820116054158609,
			"nilai_komparatif_unit": 0.8848795275704807,
			"summary_team": 0.6909090909090909
		}]
	},
	"920358": {
		"nik": "920358",
		"nama": "RIDHO AL FURQAN",
		"band": "VI",
		"posisi": "OFF 3 SUBSIDIARY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.044705882352941,
			"nilai_komparatif_unit": 1.0481028162783725,
			"summary_team": 0.8705882352941177
		}]
	},
	"920364": {
		"nik": "920364",
		"nama": "APRILIA KURNIASARI",
		"band": "VI",
		"posisi": "OFF 3 CASH OUT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8866666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0794844253490865,
			"nilai_komparatif_unit": 1.082994444128903,
			"summary_team": 0.9571428571428569
		}]
	},
	"920365": {
		"nik": "920365",
		"nama": "RIZKA PRICILIA FITRIANA RESTU PUTRI",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL BUSINESS MARKETING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9550173010380631,
			"nilai_komparatif_unit": 0.9581226062958107,
			"summary_team": 0.8117647058823529
		}]
	},
	"920367": {
		"nik": "920367",
		"nama": "ULIL AMRI",
		"band": "VI",
		"posisi": "OFF 3 SURVEILLANCE & LOGIC NE SPV",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8589743589743583,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9313432835820904,
			"nilai_komparatif_unit": 0.9343716111235201,
			"summary_team": 0.8
		}]
	},
	"920374": {
		"nik": "920374",
		"nama": "MOHAMAD IQBAL SIDDIQ",
		"band": "VI",
		"posisi": "OFF 3 APARTEMEN & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8466666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9448818897637792,
			"nilai_komparatif_unit": 0.9479542390259806,
			"summary_team": 0.7999999999999999
		}]
	},
	"930060": {
		"nik": "930060",
		"nama": "MUHAMMAD PURWA MANGGALA",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL REGULATION INITIATIVE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8622222222222212,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9278350515463927,
			"nilai_komparatif_unit": 0.9308519718270596,
			"summary_team": 0.7999999999999998
		}]
	},
	"930409": {
		"nik": "930409",
		"nama": "MEYLIA CANDRAWATI",
		"band": "VI",
		"posisi": "OFF 3 QUALITY & GOVERNANCE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.83030303030303,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8784886217260628,
			"nilai_komparatif_unit": 0.8813450886538893,
			"summary_team": 0.7294117647058822
		}]
	},
	"930413": {
		"nik": "930413",
		"nama": "ELLIZA WULANDARI",
		"band": "VI",
		"posisi": "OFF 3 SPOT BUYING CATEGORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7000000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0389610389610386,
			"nilai_komparatif_unit": 1.0423392931281346,
			"summary_team": 0.7272727272727272
		}]
	},
	"930423": {
		"nik": "930423",
		"nama": "I MADE KURNIA RESTU PUTRA",
		"band": "VI",
		"posisi": "OFF 3 OPERATIONAL ACCOUNTING POLICY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8904761904761893,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9512425290972015,
			"nilai_komparatif_unit": 0.954335560421124,
			"summary_team": 0.8470588235294116
		}]
	},
	"930427": {
		"nik": "930427",
		"nama": "KHALID ARDHI NURRAHMAN",
		"band": "VI",
		"posisi": "OFF 3 FINANCE SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.018181818181818,
			"nilai_komparatif_unit": 1.021492507265572,
			"summary_team": 0.9333333333333331
		}]
	},
	"930434": {
		"nik": "930434",
		"nama": "SITI NUUR FAUZIYAH ANBARSARI",
		"band": "VI",
		"posisi": "OFF 3 MICRO DEMAND",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0181818181818183,
			"nilai_komparatif_unit": 1.0214925072655723,
			"summary_team": 0.9333333333333335
		}]
	},
	"930436": {
		"nik": "930436",
		"nama": "BASKORO NUGROHO",
		"band": "VI",
		"posisi": "OFF 3 INFRA OPERATION PERFORMANCE MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8083333333333337,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9896907216494841,
			"nilai_komparatif_unit": 0.9929087699488622,
			"summary_team": 0.8
		}]
	},
	"930437": {
		"nik": "930437",
		"nama": "RAHMAWATI SULISTYANINGSIH",
		"band": "VI",
		"posisi": "OFF 3 PEOPLE DATA PREPARATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777769,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9381980640357418,
			"nilai_komparatif_unit": 0.9412486803731548,
			"summary_team": 0.8235294117647058
		}]
	},
	"930443": {
		"nik": "930443",
		"nama": "SAIFUL JAIS",
		"band": "VI",
		"posisi": "OFF 3 INDIGO INCUBATION PROGRAM MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8592592592592588,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9721095334685602,
			"nilai_komparatif_unit": 0.9752704153102874,
			"summary_team": 0.8352941176470587
		}]
	},
	"930445": {
		"nik": "930445",
		"nama": "HAMZAH ASSADUDDIN",
		"band": "VI",
		"posisi": "OFF 3 DATA EVALUATION & REPORTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8060606060606065,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9393125671321153,
			"nilai_komparatif_unit": 0.9423668073539555,
			"summary_team": 0.757142857142857
		}]
	},
	"930448": {
		"nik": "930448",
		"nama": "MUSTIKA HAQQI ARASY",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE ARJAWINAGN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0129870129870129,
			"nilai_komparatif_unit": 1.0162808107999315,
			"summary_team": 0.9454545454545454
		}]
	},
	"930453": {
		"nik": "930453",
		"nama": "BAYU ANNAFI PUTRA",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8491228070175435,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0599173553719012,
			"nilai_komparatif_unit": 1.0633637504611178,
			"summary_team": 0.8999999999999999
		}]
	},
	"930457": {
		"nik": "930457",
		"nama": "NOVITA ISTIKHOMAH",
		"band": "VI",
		"posisi": "OFF 3 OLO FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0135746606334841,
			"nilai_komparatif_unit": 1.0168703692236465,
			"summary_team": 0.8615384615384616
		}]
	},
	"930464": {
		"nik": "930464",
		"nama": "PUTI QISTHI NERISSA",
		"band": "VI",
		"posisi": "OFF 3 CAREER & TALENT DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8488888888888888,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0602094240837696,
			"nilai_komparatif_unit": 1.0636567688547411,
			"summary_team": 0.8999999999999999
		}]
	},
	"930465": {
		"nik": "930465",
		"nama": "RIZKI YUNIASARI",
		"band": "VI",
		"posisi": "OFF 3 PRO-HIRE SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9773755656108595,
			"nilai_komparatif_unit": 0.9805535703228019,
			"summary_team": 0.8470588235294119
		}]
	},
	"936131": {
		"nik": "936131",
		"nama": "TOMY HARDIYANTO",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER-2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8270270270270257,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.099227569815807,
			"nilai_komparatif_unit": 1.1028017848046872,
			"summary_team": 0.9090909090909092
		}]
	},
	"936157": {
		"nik": "936157",
		"nama": "ILHAM DWI KUSUMA",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.944055944055944,
			"nilai_komparatif_unit": 0.947125607698161,
			"summary_team": 0.8181818181818182
		}]
	},
	"940331": {
		"nik": "940331",
		"nama": "AFIF NAUVAL",
		"band": "VI",
		"posisi": "OFF 3 ACCOUNTING INTERCOMPANY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0084033613445376,
			"nilai_komparatif_unit": 1.0116822550949542,
			"summary_team": 0.9142857142857141
		}]
	},
	"940364": {
		"nik": "940364",
		"nama": "MISA AMIRA AZWAR",
		"band": "VI",
		"posisi": "OFF 3 FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000003,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9974025974025971,
			"nilai_komparatif_unit": 1.0006457214030093,
			"summary_team": 0.8727272727272728
		}]
	},
	"940374": {
		"nik": "940374",
		"nama": "GIANA RAMADIANTI",
		"band": "VI",
		"posisi": "OFF 3 MEDIATION OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8472222222222214,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0275795564127297,
			"nilai_komparatif_unit": 1.030920802896761,
			"summary_team": 0.8705882352941174
		}]
	},
	"940382": {
		"nik": "940382",
		"nama": "JULIANA PUTRI INDERADJAJA",
		"band": "VI",
		"posisi": "OFF 3 ACCOUNTING ASSESSMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8799999999999993,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9740259740259746,
			"nilai_komparatif_unit": 0.9771930873076271,
			"summary_team": 0.857142857142857
		}]
	},
	"940386": {
		"nik": "940386",
		"nama": "DIMAS PRAMUDYA RINALDI",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999994,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0588235294117654,
			"nilai_komparatif_unit": 1.0622663678497029,
			"summary_team": 0.8999999999999999
		}]
	},
	"940403": {
		"nik": "940403",
		"nama": "SHAFA CHOIRUNNISA",
		"band": "VI",
		"posisi": "OFF 3 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8740740740740743,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.040061633281972,
			"nilai_komparatif_unit": 1.0434434661081433,
			"summary_team": 0.9090909090909092
		}]
	},
	"940407": {
		"nik": "940407",
		"nama": "RATNA AISYAH SAVITRIE",
		"band": "VI",
		"posisi": "OFF 3 DATA SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8472222222222214,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0414657666345235,
			"nilai_komparatif_unit": 1.0448521650980687,
			"summary_team": 0.8823529411764706
		}]
	},
	"940408": {
		"nik": "940408",
		"nama": "NADHILA FAJRINA",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.858333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9532215357458079,
			"nilai_komparatif_unit": 0.9563210019476581,
			"summary_team": 0.8181818181818181
		}]
	},
	"940413": {
		"nik": "940413",
		"nama": "MUHAMAD RIDHO CHANDRA SENA",
		"band": "VI",
		"posisi": "SENIOR ACCESS NETWORK SUPERVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.871794871794872,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9941176470588234,
			"nilai_komparatif_unit": 0.9973500898144426,
			"summary_team": 0.8666666666666668
		}]
	},
	"940414": {
		"nik": "940414",
		"nama": "NURUL ANISA",
		"band": "VI",
		"posisi": "OFF 3 TAX OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8266666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.019585253456221,
			"nilai_komparatif_unit": 1.0229005059074994,
			"summary_team": 0.8428571428571427
		}]
	},
	"940416": {
		"nik": "940416",
		"nama": "DIMAS ADHITAMA",
		"band": "VI",
		"posisi": "OFF 3 PL DSG, DEPL, SOL PARTNER&INS REL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666663,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300704,
			"nilai_komparatif_unit": 1.0734090220579164,
			"summary_team": 0.9272727272727272
		}]
	},
	"940432": {
		"nik": "940432",
		"nama": "SHANISSALAM WIDHIANA PUTRI PERTIWI",
		"band": "VI",
		"posisi": "OFF 3 DESIGNER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7636363636363636,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1224489795918366,
			"nilai_komparatif_unit": 1.1260987006116456,
			"summary_team": 0.857142857142857
		}]
	},
	"940434": {
		"nik": "940434",
		"nama": "AMALUL AHLI",
		"band": "VI",
		"posisi": "OFF 3 IS OPERATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8633333333333328,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9266409266409272,
			"nilai_komparatif_unit": 0.9296539641413102,
			"summary_team": 0.8
		}]
	},
	"940440": {
		"nik": "940440",
		"nama": "BASTIAN RAMADHAN, SST., M.T.",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE MAJALAYA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.863888888888888,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9668999432570464,
			"nilai_komparatif_unit": 0.9700438857534267,
			"summary_team": 0.8352941176470587
		}]
	},
	"940443": {
		"nik": "940443",
		"nama": "SISPA NURADIANA",
		"band": "VI",
		"posisi": "OFF 3 PUSH CHANNEL INTEGRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000004,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828873,
			"nilai_komparatif_unit": 1.008616551291636,
			"summary_team": 0.8545454545454546
		}]
	},
	"940447": {
		"nik": "940447",
		"nama": "FADHLI IWANDA",
		"band": "VI",
		"posisi": "OFF 3 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8518518518518521,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0031620553359681,
			"nilai_komparatif_unit": 1.0064239066149152,
			"summary_team": 0.8545454545454546
		}]
	},
	"940449": {
		"nik": "940449",
		"nama": "MUHAMMAD IHSAN HIDAYAT",
		"band": "VI",
		"posisi": "OFF 3 QUALITY & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7833333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0212765957446805,
			"nilai_komparatif_unit": 1.0245973477131878,
			"summary_team": 0.7999999999999998
		}]
	},
	"940450": {
		"nik": "940450",
		"nama": "SUDARSONO",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER-2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0102673148780383,
			"summary_team": 0.8727272727272727
		}]
	},
	"940453": {
		"nik": "940453",
		"nama": "ROBERTUS RADITYA MAHENDRIYA",
		"band": "VI",
		"posisi": "OFF 3 PROMO PLAN & EXECUTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8596491228070169,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9306122448979599,
			"nilai_komparatif_unit": 0.9336381954162016,
			"summary_team": 0.8
		}]
	},
	"940454": {
		"nik": "940454",
		"nama": "AUDY PRISTIKASARI",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8733333333333331,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9993060374739767,
			"nilai_komparatif_unit": 1.0025553506423286,
			"summary_team": 0.8727272727272728
		}]
	},
	"940457": {
		"nik": "940457",
		"nama": "DINDA ANGGUN RATNASARI",
		"band": "VI",
		"posisi": "OFF 3 APARTMENT & PREMIUM CLUSTER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9906542056074763,
			"nilai_komparatif_unit": 0.9938753867420368,
			"summary_team": 0.8833333333333333
		}]
	},
	"940458": {
		"nik": "940458",
		"nama": "FIQIE BUDI ZULFIKAR",
		"band": "VI",
		"posisi": "OFF 3 DIG BUSINESS GOVERNANCE ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444438,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.961300309597524,
			"nilai_komparatif_unit": 0.9644260444951251,
			"summary_team": 0.8117647058823529
		}]
	},
	"940461": {
		"nik": "940461",
		"nama": "OCTAVIA RENIAR PUTRI",
		"band": "VI",
		"posisi": "OFF 3 SETTLEMENT & REPORTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0316742081447965,
			"nilai_komparatif_unit": 1.035028768674069,
			"summary_team": 0.8941176470588235
		}]
	},
	"946032": {
		"nik": "946032",
		"nama": "I GUSTI BAGUS YOGISWARA",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8749999999999994,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.997402597402598,
			"nilai_komparatif_unit": 1.0006457214030102,
			"summary_team": 0.8727272727272727
		}]
	},
	"946052": {
		"nik": "946052",
		"nama": "RINGKI PUTRA AZALIKA",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8172839506172833,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.7690979715148903,
			"nilai_komparatif_unit": 0.7715987471260466,
			"summary_team": 0.6285714285714284
		}]
	},
	"950152": {
		"nik": "950152",
		"nama": "HASRIMA DEWI MAGFIRA",
		"band": "VI",
		"posisi": "OFF 3 PROCUREMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777779,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0186150409530899,
			"nilai_komparatif_unit": 1.0219271386908526,
			"summary_team": 0.8941176470588235
		}]
	},
	"950175": {
		"nik": "950175",
		"nama": "MUHAMMAD HANIF ABDURRAHMAN",
		"band": "VI",
		"posisi": "OFF 3 DESIGN & ENGINEERING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8400000000000003,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9013605442176866,
			"nilai_komparatif_unit": 0.9042913807942,
			"summary_team": 0.757142857142857
		}]
	},
	"950206": {
		"nik": "950206",
		"nama": "KAPAS VANDER CHIKO PASARIBU",
		"band": "VI",
		"posisi": "OFF 3 DC & CME OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8740740740740742,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.964285714285714,
			"nilai_komparatif_unit": 0.96742115643455,
			"summary_team": 0.8428571428571427
		}]
	},
	"950237": {
		"nik": "950237",
		"nama": "DARISA SYAHRINI",
		"band": "VI",
		"posisi": "OFF 3 DIG BUSS GOVERNANCE IMPLEMENTATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444438,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9891640866873072,
			"nilai_komparatif_unit": 0.992380422596433,
			"summary_team": 0.8352941176470587
		}]
	},
	"950241": {
		"nik": "950241",
		"nama": "IRTANTY OCVITASARI",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER PROFILING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0000000000000002,
			"nilai_komparatif_unit": 1.0032515696358302,
			"summary_team": 0.8
		}]
	},
	"950242": {
		"nik": "950242",
		"nama": "OBET GABRIEL FILEMON SIMANUNGKALIT",
		"band": "VI",
		"posisi": "OFF 3 ACCESS NEW FTTH PROJECT SUPERVISIO",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0044642857142854,
			"nilai_komparatif_unit": 1.0077303712859895,
			"summary_team": 0.857142857142857
		}]
	},
	"950262": {
		"nik": "950262",
		"nama": "SHEILA CHAIRUNISHA",
		"band": "VI",
		"posisi": "AUDITOR PRATAMA III",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.030987604958011,
			"nilai_komparatif_unit": 1.0343399329492093,
			"summary_team": 0.8999999999999999
		}]
	},
	"950264": {
		"nik": "950264",
		"nama": "RAFLI ARONTA SITEPU",
		"band": "VI",
		"posisi": "AUDITOR PRATAMA III",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.076809276289478,
			"nilai_komparatif_unit": 1.0803105966358408,
			"summary_team": 0.9400000000000001
		}]
	},
	"950269": {
		"nik": "950269",
		"nama": "EMI FENTINY BAKARA",
		"band": "VI",
		"posisi": "OFF 3 PAYMENT & COLLECTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9611344537815122,
			"nilai_komparatif_unit": 0.9642596493873781,
			"summary_team": 0.8714285714285712
		}]
	},
	"950275": {
		"nik": "950275",
		"nama": "KRISTIAN BASTANTA SINULINGGA",
		"band": "VI",
		"posisi": "OFF 3 ASSET LEVERAGING PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0101010101010102,
			"nilai_komparatif_unit": 1.0133854238745756,
			"summary_team": 0.9090909090909092
		}]
	},
	"950288": {
		"nik": "950288",
		"nama": "PATRICIA HANNA GIOVANI SIBARANI",
		"band": "VI",
		"posisi": "AUDITOR PRATAMA III",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0653538584566113,
			"nilai_komparatif_unit": 1.0688179307141832,
			"summary_team": 0.93
		}]
	},
	"950290": {
		"nik": "950290",
		"nama": "ARINA INDANA FAHMA",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0053475935828877,
			"nilai_komparatif_unit": 1.0086165512916365,
			"summary_team": 0.8545454545454546
		}]
	},
	"950291": {
		"nik": "950291",
		"nama": "MUHAMAD DIMAS RAHMANDA",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8980392156862743,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.950218340611354,
			"nilai_komparatif_unit": 0.9533080417150945,
			"summary_team": 0.8533333333333334
		}]
	},
	"950293": {
		"nik": "950293",
		"nama": "DZAKI IBNU GRAHA PAMUNGKAS",
		"band": "VI",
		"posisi": "AUDITOR PRATAMA III",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9838182374109122,
			"nilai_komparatif_unit": 0.9870171909188532,
			"summary_team": 0.8588235294117645
		}]
	},
	"950299": {
		"nik": "950299",
		"nama": "ARANDHIKA PUTRI MAHARANI",
		"band": "VI",
		"posisi": "OFF 3 LOGISTIC PLANNING & EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9177777777777774,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9998575701467033,
			"nilai_komparatif_unit": 1.003108676661947,
			"summary_team": 0.9176470588235295
		}]
	},
	"950301": {
		"nik": "950301",
		"nama": "ADITYA SHOFWAN ZULMA",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9262435677530018,
			"nilai_komparatif_unit": 0.9292553132132901,
			"summary_team": 0.8181818181818182
		}]
	},
	"950308": {
		"nik": "950308",
		"nama": "ADE RACHMAWATI",
		"band": "VI",
		"posisi": "OFF 3 SELECTION METHOD EVALUATION TELKOM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.842857142857143,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9707241910631739,
			"nilai_komparatif_unit": 0.9738805683676004,
			"summary_team": 0.8181818181818181
		}]
	},
	"950315": {
		"nik": "950315",
		"nama": "NI PUTU DESY CRISTIANA YANTHI",
		"band": "VI",
		"posisi": "OFF 3 ACCOUNTING ASSESSMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8909090909090912,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0262390670553931,
			"nilai_komparatif_unit": 1.0295759548449328,
			"summary_team": 0.9142857142857141
		}]
	},
	"950321": {
		"nik": "950321",
		"nama": "KARINA ADITYA PUTRI",
		"band": "VI",
		"posisi": "OFF 3 PREPARATION RECRUIT & SELECT SUPP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8727272727272729,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.014880952380952,
			"nilai_komparatif_unit": 1.0181809084696962,
			"summary_team": 0.8857142857142857
		}]
	},
	"950323": {
		"nik": "950323",
		"nama": "RIZKI RODHIA MARDHATILLAH",
		"band": "VI",
		"posisi": "OFF 3 ADVALJAR OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8583333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9532215357458075,
			"nilai_komparatif_unit": 0.9563210019476577,
			"summary_team": 0.8181818181818182
		}]
	},
	"950331": {
		"nik": "950331",
		"nama": "MUHAMMAD FAKHRI FEBRIAN",
		"band": "VI",
		"posisi": "OFF 3 REPORTING MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8206349206349192,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9748549323017424,
			"nilai_komparatif_unit": 0.9780247409989538,
			"summary_team": 0.7999999999999998
		}]
	},
	"950338": {
		"nik": "950338",
		"nama": "FITRI ARYANI",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8410256410256403,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9512195121951229,
			"nilai_komparatif_unit": 0.9543124686779855,
			"summary_team": 0.8
		}]
	},
	"950340": {
		"nik": "950340",
		"nama": "DIANSASI PROBORINI",
		"band": "VI",
		"posisi": "OFF 3 QUALITY & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9974025974025974,
			"nilai_komparatif_unit": 1.0006457214030096,
			"summary_team": 0.8727272727272728
		}]
	},
	"950343": {
		"nik": "950343",
		"nama": "WIDI ANGGUN FITRIANA",
		"band": "VI",
		"posisi": "OFF 3 HC CAPACITY SOLUTION ADVISORY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8363636363636368,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0248447204968938,
			"nilai_komparatif_unit": 1.028177074471502,
			"summary_team": 0.8571428571428571
		}]
	},
	"950345": {
		"nik": "950345",
		"nama": "EDELWEIS PERMAI",
		"band": "VI",
		"posisi": "OFF 3 ACCOUNTING OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8980392156862742,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0218340611353716,
			"nilai_komparatif_unit": 1.0251566257414162,
			"summary_team": 0.9176470588235295
		}]
	},
	"950347": {
		"nik": "950347",
		"nama": "FIKO RAHARDITO BASKORO",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0102673148780383,
			"summary_team": 0.8727272727272727
		}]
	},
	"950351": {
		"nik": "950351",
		"nama": "CORAIMA AJENG HAPSARI",
		"band": "VI",
		"posisi": "OFF 3 DELIVERY ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9773755656108596,
			"nilai_komparatif_unit": 0.980553570322802,
			"summary_team": 0.8470588235294119
		}]
	},
	"950356": {
		"nik": "950356",
		"nama": "QORIRA RETNOARDHINI",
		"band": "VI",
		"posisi": "OFF 3 COMMAND CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8245614035087716,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9922630560928436,
			"nilai_komparatif_unit": 0.9954894685167909,
			"summary_team": 0.8181818181818181
		}]
	},
	"950366": {
		"nik": "950366",
		"nama": "REIHAN RIZKY AZIZA",
		"band": "VI",
		"posisi": "OFF 3 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9827770478065272,
			"summary_team": 0.8
		}]
	},
	"950372": {
		"nik": "950372",
		"nama": "ZULHAM AKBAR ALFARABI",
		"band": "VI",
		"posisi": "OFF 3 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.974025974025974,
			"nilai_komparatif_unit": 0.9771930873076266,
			"summary_team": 0.9090909090909092
		}]
	},
	"950374": {
		"nik": "950374",
		"nama": "OKI AKBARSYAH",
		"band": "VI",
		"posisi": "OFF 3 QOS, SLG & DATA CUSTOMER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9926470588235292,
			"nilai_komparatif_unit": 0.9958747198590957,
			"summary_team": 0.8999999999999999
		}]
	},
	"950376": {
		"nik": "950376",
		"nama": "ANETA INDRA PAWESTRI",
		"band": "VI",
		"posisi": "OFF 3 ACCESS OM & PARTNERSHIP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9222222222222223,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9930247305009511,
			"nilai_komparatif_unit": 0.9962536195622762,
			"summary_team": 0.9157894736842105
		}]
	},
	"950377": {
		"nik": "950377",
		"nama": "HARIS ALTAMIRA",
		"band": "VI",
		"posisi": "OFF 3 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0461538461538458,
			"nilai_komparatif_unit": 1.0495554882344063,
			"summary_team": 0.9066666666666665
		}]
	},
	"950378": {
		"nik": "950378",
		"nama": "NURSEPMA RISMAWATI",
		"band": "VI",
		"posisi": "OFF 3 MICRO DEMAND",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8523809523809527,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0558659217877089,
			"nilai_komparatif_unit": 1.0592991433585013,
			"summary_team": 0.8999999999999998
		}]
	},
	"950382": {
		"nik": "950382",
		"nama": "DARMAWANSYAH",
		"band": "VI",
		"posisi": "OFF 3 BACKBONE OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8740740740740742,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9984591679506932,
			"nilai_komparatif_unit": 1.0017057274638177,
			"summary_team": 0.8727272727272728
		}]
	},
	"950385": {
		"nik": "950385",
		"nama": "RATNAWIA",
		"band": "VI",
		"posisi": "OFF 3 PARTNERSHIP & AGENCY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8172839506172833,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9788519637462243,
			"nilai_komparatif_unit": 0.982034769069514,
			"summary_team": 0.7999999999999999
		}]
	},
	"950386": {
		"nik": "950386",
		"nama": "ARYA PRATAMA",
		"band": "VI",
		"posisi": "OFF 3 ACCESS PARTNERSHIP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9200000000000003,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.071428571428571,
			"nilai_komparatif_unit": 1.0749123960383886,
			"summary_team": 0.9857142857142855
		}]
	},
	"950387": {
		"nik": "950387",
		"nama": "RIFAYANI FADHILAH",
		"band": "VI",
		"posisi": "OFF 3 AR DATA MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9500000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9952153110047844,
			"nilai_komparatif_unit": 0.9984513228911607,
			"summary_team": 0.9454545454545454
		}]
	},
	"950388": {
		"nik": "950388",
		"nama": "PATHIYAH ARDIYANTI",
		"band": "VI",
		"posisi": "OFF 3 MIKRO WORKFORCE PLANNING & EVAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8629629629629628,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.008836152486746,
			"nilai_komparatif_unit": 1.0121164534876994,
			"summary_team": 0.8705882352941177
		}]
	},
	"950392": {
		"nik": "950392",
		"nama": "DEA SONA ALAMANDA",
		"band": "VI",
		"posisi": "OFF 3 EXTERNAL CONTENT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8911111111111103,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9373624761625357,
			"nilai_komparatif_unit": 0.940410375527792,
			"summary_team": 0.8352941176470587
		}]
	},
	"950393": {
		"nik": "950393",
		"nama": "INDRI ALVI KUSUMADARMA",
		"band": "VI",
		"posisi": "OFF 3 QUALITY & DELIVERY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8266666666666663,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.92741935483871,
			"nilai_komparatif_unit": 0.9304349234525845,
			"summary_team": 0.7666666666666666
		}]
	},
	"950401": {
		"nik": "950401",
		"nama": "SITI ROCHMANA",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000003,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.997402597402597,
			"nilai_komparatif_unit": 1.0006457214030091,
			"summary_team": 0.8727272727272727
		}]
	},
	"950405": {
		"nik": "950405",
		"nama": "SENDY ACHDIKA MAULANA",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE SUKARESMI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.880701754385964,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9909453096704103,
			"nilai_komparatif_unit": 0.9941674373501026,
			"summary_team": 0.8727272727272727
		}]
	},
	"950406": {
		"nik": "950406",
		"nama": "ARIEN SHILVINNUR",
		"band": "VI",
		"posisi": "OFF 3 TAX OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.862015503875968,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0274922918807818,
			"nilai_komparatif_unit": 1.0308332546181107,
			"summary_team": 0.8857142857142855
		}]
	},
	"950407": {
		"nik": "950407",
		"nama": "MOCH. RISTRA MAHARDINATA",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.0300764779148628,
			"summary_team": 0.8727272727272727
		}]
	},
	"950409": {
		"nik": "950409",
		"nama": "SALINA AZKIA",
		"band": "VI",
		"posisi": "OFF 3 PAYMENT COLLECTION CONSUMER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 1,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1,
			"nilai_komparatif_unit": 1.00325156963583,
			"summary_team": 1
		}]
	},
	"950412": {
		"nik": "950412",
		"nama": "SEPTI INDAH RAHMAWATI",
		"band": "VI",
		"posisi": "OFF 3 OLO FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9111111111111114,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0051347881899868,
			"nilai_komparatif_unit": 1.0084030539471818,
			"summary_team": 0.9157894736842105
		}]
	},
	"950413": {
		"nik": "950413",
		"nama": "DICKY CHRISTIAN TAMBA",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8285714285714281,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0344827586206902,
			"nilai_komparatif_unit": 1.0378464513474108,
			"summary_team": 0.857142857142857
		}]
	},
	"950417": {
		"nik": "950417",
		"nama": "HENRY PAMUNGKAS",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9111111111111114,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.037694013303769,
			"nilai_komparatif_unit": 1.04106814764871,
			"summary_team": 0.9454545454545454
		}]
	},
	"950418": {
		"nik": "950418",
		"nama": "BELLA HARUM ASHARI",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9720972097209719,
			"nilai_komparatif_unit": 0.9752580514911756,
			"summary_team": 0.8181818181818182
		}]
	},
	"950419": {
		"nik": "950419",
		"nama": "NADIFA AMALIA GUNADI PUTRI",
		"band": "VI",
		"posisi": "OFF 3 DATA MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666661,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0524064171123002,
			"nilai_komparatif_unit": 1.0558283898627352,
			"summary_team": 0.9647058823529413
		}]
	},
	"950420": {
		"nik": "950420",
		"nama": "DEA KUSUMA RIYADI",
		"band": "VI",
		"posisi": "OFF 3 WHOLESALE&INTERNATIONAL MARKETING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7939393939393933,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.809705561613959,
			"nilai_komparatif_unit": 0.8123383756320656,
			"summary_team": 0.6428571428571427
		}]
	},
	"950421": {
		"nik": "950421",
		"nama": "MAULANA FAKHRUDIN FANIA PUTRA",
		"band": "VI",
		"posisi": "OFF 3 CCAN FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9175870858113849,
			"nilai_komparatif_unit": 0.9205706841178389,
			"summary_team": 0.8181818181818182
		}]
	},
	"950422": {
		"nik": "950422",
		"nama": "TIO HANIF YANUARY",
		"band": "VI",
		"posisi": "OFF 3 OM TRANSPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9877232142857141,
			"nilai_komparatif_unit": 0.9909348650978899,
			"summary_team": 0.8428571428571427
		}]
	},
	"950423": {
		"nik": "950423",
		"nama": "ILHAM MIFTAHUL FAUZI",
		"band": "VI",
		"posisi": "OFF 3 WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8733333333333331,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0201249132546846,
			"nilai_komparatif_unit": 1.023441920447377,
			"summary_team": 0.8909090909090909
		}]
	},
	"950427": {
		"nik": "950427",
		"nama": "MUHAMMAD HAFIDZ HILMAWAN",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777778,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.139240506329114,
			"nilai_komparatif_unit": 1.1429448261674013,
			"summary_team": 1
		}]
	},
	"950428": {
		"nik": "950428",
		"nama": "ARDYANI NALENDRI STYADI",
		"band": "VI",
		"posisi": "OFF 3 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0212053571428568,
			"nilai_komparatif_unit": 1.0245258774740893,
			"summary_team": 0.8714285714285713
		}]
	},
	"950429": {
		"nik": "950429",
		"nama": "BIMO ASTIN PERDANA",
		"band": "VI",
		"posisi": "OFF 3 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8207407407407394,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9921351211964945,
			"nilai_komparatif_unit": 0.9953611176312175,
			"summary_team": 0.8142857142857142
		}]
	},
	"950433": {
		"nik": "950433",
		"nama": "NADIRA ANJANI",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8266666666666663,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9850230414746547,
			"nilai_komparatif_unit": 0.9882259124869065,
			"summary_team": 0.8142857142857142
		}]
	},
	"950436": {
		"nik": "950436",
		"nama": "AZAM FIKRI ILHAM",
		"band": "VI",
		"posisi": "OFF 3 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0392156862745097,
			"nilai_komparatif_unit": 1.042594768445078,
			"summary_team": 0.8833333333333333
		}]
	},
	"950437": {
		"nik": "950437",
		"nama": "VEDJI MEDHYCI",
		"band": "VI",
		"posisi": "OFF 3 TELKOMGROUP FULFILLMENT &ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8533333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9542410714285712,
			"nilai_komparatif_unit": 0.9573438527216901,
			"summary_team": 0.8142857142857142
		}]
	},
	"950439": {
		"nik": "950439",
		"nama": "SANCITA UTAMI RUPAKASARI",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8545454545454549,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0531914893617016,
			"nilai_komparatif_unit": 1.0566160148292245,
			"summary_team": 0.8999999999999999
		}]
	},
	"950440": {
		"nik": "950440",
		"nama": "SUCI GLORIA MONATASHA SILALAHI",
		"band": "VI",
		"posisi": "OFF 3 ENTERPRISE ACCOUNT MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0561224489795917,
			"nilai_komparatif_unit": 1.0595565046664122,
			"summary_team": 0.9857142857142855
		}]
	},
	"950443": {
		"nik": "950443",
		"nama": "ULFAH ADZKIA",
		"band": "VI",
		"posisi": "OFF 3 FINANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8631578947368416,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.095343680709535,
			"nilai_komparatif_unit": 1.098905266962528,
			"summary_team": 0.9454545454545454
		}]
	},
	"950444": {
		"nik": "950444",
		"nama": "WIDYA AYU GALUH SETYAWATI",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8125,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9846153846153847,
			"nilai_komparatif_unit": 0.987816930102971,
			"summary_team": 0.8
		}]
	},
	"950447": {
		"nik": "950447",
		"nama": "RIYANTYARI HAYU PRAMESWARI",
		"band": "VI",
		"posisi": "OFF 3 NTE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8358974358974361,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.022308979364194,
			"nilai_komparatif_unit": 1.025633088199931,
			"summary_team": 0.8545454545454546
		}]
	},
	"950449": {
		"nik": "950449",
		"nama": "SHABRINA ZHAFARINA HADIWIJAYA",
		"band": "VI",
		"posisi": "ACCOUNT MANAGER-2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8547619047619041,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0210179792352503,
			"nilai_komparatif_unit": 1.024337890294168,
			"summary_team": 0.8727272727272727
		}]
	},
	"950450": {
		"nik": "950450",
		"nama": "MUHAMMAD ARDHI PRAKASA",
		"band": "VI",
		"posisi": "OFF 3 ACCESS PARTNERSHIP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666666,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559442,
			"nilai_komparatif_unit": 0.9471256076981612,
			"summary_team": 0.8181818181818182
		}]
	},
	"950451": {
		"nik": "950451",
		"nama": "ALIYA NUR REZKITA",
		"band": "VI",
		"posisi": "OFF 3 ADVALJAR OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8363636363636368,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0446224256292902,
			"nilai_komparatif_unit": 1.0480190881893734,
			"summary_team": 0.8736842105263158
		}]
	},
	"950452": {
		"nik": "950452",
		"nama": "RANGGA EKA SETIAWAN",
		"band": "VI",
		"posisi": "OFF 3 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8571428571428571,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0791666666666668,
			"nilai_komparatif_unit": 1.082675652232,
			"summary_team": 0.9250000000000002
		}]
	},
	"950453": {
		"nik": "950453",
		"nama": "NABILA KHAIRUNNISA ANSORY",
		"band": "VI",
		"posisi": "OFF 3 SELFCARE & DIGITAL INTERACTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0096153846153846,
			"nilai_komparatif_unit": 1.0128982193438667,
			"summary_team": 0.875
		}]
	},
	"950455": {
		"nik": "950455",
		"nama": "INA RIZQIYANA",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9245283018867922,
			"nilai_komparatif_unit": 0.9275344700406727,
			"summary_team": 0.8166666666666668
		}]
	},
	"950456": {
		"nik": "950456",
		"nama": "OKY IYAN PRATAMA",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.071074380165289,
			"nilai_komparatif_unit": 1.07455705309755,
			"summary_team": 0.9818181818181818
		}]
	},
	"950457": {
		"nik": "950457",
		"nama": "IRMA YUNINGSIH",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE LEMBANG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8574712643678148,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9741365715186893,
			"nilai_komparatif_unit": 0.9773040444157909,
			"summary_team": 0.8352941176470587
		}]
	},
	"950459": {
		"nik": "950459",
		"nama": "INDRA ALFIANTO",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE JATIBARANG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0489510489510485,
			"nilai_komparatif_unit": 1.0523617863312897,
			"summary_team": 0.9090909090909092
		}]
	},
	"950460": {
		"nik": "950460",
		"nama": "RIZKI ADHITAMA",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7666666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9723320158102766,
			"nilai_komparatif_unit": 0.9754936210688306,
			"summary_team": 0.7454545454545455
		}]
	},
	"950461": {
		"nik": "950461",
		"nama": "RAJA WIRANSYAH",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.875362318840579,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9791863765373705,
			"nilai_komparatif_unit": 0.9823702692271378,
			"summary_team": 0.857142857142857
		}]
	},
	"950465": {
		"nik": "950465",
		"nama": "HENDRA HARDIANTO PRADANA",
		"band": "VI",
		"posisi": "OFF 3 CCAN ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8400000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9523809523809522,
			"nilai_komparatif_unit": 0.9554776853674569,
			"summary_team": 0.7999999999999999
		}]
	},
	"950466": {
		"nik": "950466",
		"nama": "M. ERIFIANDI",
		"band": "VI",
		"posisi": "OFF 3 OM TRANSPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9703504043126682,
			"nilai_komparatif_unit": 0.9735055662234465,
			"summary_team": 0.857142857142857
		}]
	},
	"950467": {
		"nik": "950467",
		"nama": "M. RENDY",
		"band": "VI",
		"posisi": "OFF 3 OM TRANSPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7833333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9284332688588008,
			"nilai_komparatif_unit": 0.9314521342847164,
			"summary_team": 0.7272727272727273
		}]
	},
	"950468": {
		"nik": "950468",
		"nama": "ALIT DIAN SAEPUDIN",
		"band": "VI",
		"posisi": "OFF 3 ACCESS OM & PARTNERSHIP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969696,
			"nilai_komparatif_unit": 0.9728500069195926,
			"summary_team": 0.8727272727272727
		}]
	},
	"950473": {
		"nik": "950473",
		"nama": "AMALIA PUSPA NIRWANA",
		"band": "VI",
		"posisi": "OFF 3 BUKA ISOLIR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.923076923076923,
			"nilai_komparatif_unit": 0.9260783719715352,
			"summary_team": 0.8
		}]
	},
	"950477": {
		"nik": "950477",
		"nama": "ANGGI AGUSTIAN",
		"band": "VI",
		"posisi": "OFF 3 HC SOLUTION AREA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9023809523809516,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0169175849759435,
			"nilai_komparatif_unit": 1.0202241633173927,
			"summary_team": 0.9176470588235293
		}]
	},
	"950484": {
		"nik": "950484",
		"nama": "CINDY",
		"band": "VI",
		"posisi": "OFF 3 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197862,
			"nilai_komparatif_unit": 0.965696698045184,
			"summary_team": 0.8181818181818182
		}]
	},
	"950487": {
		"nik": "950487",
		"nama": "DECKY YUSUB",
		"band": "VI",
		"posisi": "OFF 3 DATA MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666661,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0139037433155087,
			"nilai_komparatif_unit": 1.0172005219409277,
			"summary_team": 0.9294117647058824
		}]
	},
	"950490": {
		"nik": "950490",
		"nama": "DIASTANTO EKA DESTIARAHMAN",
		"band": "VI",
		"posisi": "OFF 3 APARTMENT & PREMIUM CLUSTER OPERAT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8105263157894733,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0991735537190088,
			"nilai_komparatif_unit": 1.1027475930707888,
			"summary_team": 0.8909090909090909
		}]
	},
	"950491": {
		"nik": "950491",
		"nama": "EKA BAYU SUSILO",
		"band": "VI",
		"posisi": "OFF 3 FINANCE SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857142,
			"nilai_komparatif_unit": 0.9674211564345502,
			"summary_team": 0.7714285714285715
		}]
	},
	"950493": {
		"nik": "950493",
		"nama": "EVA YULIANA DEWI KRISTYANINGRUM",
		"band": "VI",
		"posisi": "OFF 3 SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0519480519480517,
			"nilai_komparatif_unit": 1.0553685342922365,
			"summary_team": 0.9818181818181818
		}]
	},
	"950494": {
		"nik": "950494",
		"nama": "FADLAN SURYA ADI TAMA",
		"band": "VI",
		"posisi": "OFF 3 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8222222222222226,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9729729729729726,
			"nilai_komparatif_unit": 0.9761366623483747,
			"summary_team": 0.8
		}]
	},
	"950495": {
		"nik": "950495",
		"nama": "FIKO RAMADHAN",
		"band": "VI",
		"posisi": "OFF 3 EMPLOYEE & INDUSTRIAL RELATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333331,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9642857142857143,
			"nilai_komparatif_unit": 0.9674211564345503,
			"summary_team": 0.8999999999999998
		}]
	},
	"950500": {
		"nik": "950500",
		"nama": "KAMAL ARIEF",
		"band": "VI",
		"posisi": "OFF 3 OM LOKASI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8285714285714288,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.96551724137931,
			"nilai_komparatif_unit": 0.9686566879242493,
			"summary_team": 0.8
		}]
	},
	"950501": {
		"nik": "950501",
		"nama": "KARINA DWIARINI",
		"band": "VI",
		"posisi": "OFF 3 MIKRO WORKFORCE PLANNING & EVAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8814814814814814,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.920909540286703,
			"nilai_komparatif_unit": 0.9239039417852454,
			"summary_team": 0.8117647058823529
		}]
	},
	"950504": {
		"nik": "950504",
		"nama": "LIZA IKRIMA FAUZI",
		"band": "VI",
		"posisi": "OFF 3 FINANCE SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0084033613445373,
			"nilai_komparatif_unit": 1.011682255094954,
			"summary_team": 0.914285714285714
		}]
	},
	"950505": {
		"nik": "950505",
		"nama": "MARJAN MAULATAUFIK",
		"band": "VI",
		"posisi": "OFF 3 HC RELATION AREA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9023809523809516,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0038801800403545,
			"nilai_komparatif_unit": 1.0071443663517852,
			"summary_team": 0.9058823529411764
		}]
	},
	"950507": {
		"nik": "950507",
		"nama": "MUHAMMAD HANZALA",
		"band": "VI",
		"posisi": "OFF 3 EXECUTIVE SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8766666666666662,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0577255444175602,
			"nilai_komparatif_unit": 1.06116481268083,
			"summary_team": 0.9272727272727272
		}]
	},
	"950508": {
		"nik": "950508",
		"nama": "MUHAMMAD PASHA ALISTON",
		"band": "VI",
		"posisi": "OFF 3 OM CME",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9818181818181815,
			"nilai_komparatif_unit": 0.9850106320060872,
			"summary_team": 0.8181818181818181
		}]
	},
	"950509": {
		"nik": "950509",
		"nama": "MUHAMMAD RIZAL FADHLI WIBOWO",
		"band": "VI",
		"posisi": "OFF 3 PEOPLE ANALYTICS PROJECT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8777777777777769,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0052122114668662,
			"nilai_komparatif_unit": 1.0084807289712374,
			"summary_team": 0.8823529411764706
		}]
	},
	"950510": {
		"nik": "950510",
		"nama": "MUHAMMAD SHOLIKHIN",
		"band": "VI",
		"posisi": "OFF 3 CAREER & TALENT DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8419753086419745,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0060376056581,
			"nilai_komparatif_unit": 1.009308806989161,
			"summary_team": 0.8470588235294116
		}]
	},
	"950512": {
		"nik": "950512",
		"nama": "NAJARUDDIN MUHAMMAD",
		"band": "VI",
		"posisi": "OFF 3 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139859,
			"nilai_komparatif_unit": 0.9892200791514126,
			"summary_team": 0.8545454545454546
		}]
	},
	"950515": {
		"nik": "950515",
		"nama": "NURRIDA AINI ZUHROH",
		"band": "VI",
		"posisi": "OFF 3 EMPLOYEE EXP & MTM SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.92,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9782608695652172,
			"nilai_komparatif_unit": 0.981441752904616,
			"summary_team": 0.8999999999999998
		}]
	},
	"950516": {
		"nik": "950516",
		"nama": "OKE LUTHFIANI HAMZAH",
		"band": "VI",
		"posisi": "OFF 3 SUBSIDIARY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9836065573770489,
			"nilai_komparatif_unit": 0.9868048225926194,
			"summary_team": 0.7999999999999998
		}]
	},
	"950518": {
		"nik": "950518",
		"nama": "PUTU AYU WINNIE QADRINA",
		"band": "VI",
		"posisi": "OFF 3 PERSONNEL COST PAYMENT &TAX REPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8755555555555556,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9626540971718635,
			"nilai_komparatif_unit": 0.9657842340040348,
			"summary_team": 0.8428571428571427
		}]
	},
	"950521": {
		"nik": "950521",
		"nama": "RAMANIA KARTIKANINGTYAS",
		"band": "VI",
		"posisi": "OFF 3 FAULT HANDLING & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8250000000000003,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969694,
			"nilai_komparatif_unit": 0.9728500069195923,
			"summary_team": 0.8
		}]
	},
	"950522": {
		"nik": "950522",
		"nama": "RAYINDITA SIWIE MAZAYANTRI",
		"band": "VI",
		"posisi": "OFF 3 USER REQUIREMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8874999999999998,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0140845070422535,
			"nilai_komparatif_unit": 1.0173818734335176,
			"summary_team": 0.8999999999999999
		}]
	},
	"950523": {
		"nik": "950523",
		"nama": "SABENA TIKA ARSYAWATI",
		"band": "VI",
		"posisi": "OFF 3 EMPLOYEE TERMINATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666657,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0180995475113133,
			"nilai_komparatif_unit": 1.0214099690862533,
			"summary_team": 0.8823529411764706
		}]
	},
	"950525": {
		"nik": "950525",
		"nama": "STEFANUS YUDHA PRASETYA",
		"band": "VI",
		"posisi": "OFF 3 OUTBOND LOGISTIC",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9533333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9890109890109887,
			"nilai_komparatif_unit": 0.992226827112359,
			"summary_team": 0.9428571428571426
		}]
	},
	"950526": {
		"nik": "950526",
		"nama": "TAUFIK YUMNA",
		"band": "VI",
		"posisi": "OFF 3 ACCESS MAINTENANCE & QE PLAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8999999999999999,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9696969696969697,
			"nilai_komparatif_unit": 0.9728500069195927,
			"summary_team": 0.8727272727272727
		}]
	},
	"950529": {
		"nik": "950529",
		"nama": "UMMI ROBBANIYAH MUSHTHOFA",
		"band": "VI",
		"posisi": "OFFICER 3 TRIBE INNOVATOR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.875757575757575,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0343985345003064,
			"nilai_komparatif_unit": 1.0377619533664346,
			"summary_team": 0.9058823529411766
		}]
	},
	"950531": {
		"nik": "950531",
		"nama": "WILDANI DEZA FAHMI",
		"band": "VI",
		"posisi": "OFF 3 FAULT HANDLING & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8035087719298245,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9956331877729258,
			"nilai_komparatif_unit": 0.9988705584147127,
			"summary_team": 0.7999999999999999
		}]
	},
	"950532": {
		"nik": "950532",
		"nama": "ZAKI 'ABDULLAH",
		"band": "VI",
		"posisi": "OFF 3 UNIT PROGRAM & PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9111111111111112,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.007173601147776,
			"nilai_komparatif_unit": 1.0104484962472777,
			"summary_team": 0.9176470588235295
		}]
	},
	"955910": {
		"nik": "955910",
		"nama": "WULAN ALFISAHRI RAMADHAN",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8333333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Below Average",
			"feedback_unit": "Down Below Average",
			"nilai_komparatif": 0.8057142857142856,
			"nilai_komparatif_unit": 0.8083341218208686,
			"summary_team": 0.6714285714285713
		}]
	},
	"960048": {
		"nik": "960048",
		"nama": "KEVIN JONES ANDREAN SINAGA",
		"band": "VI",
		"posisi": "OFF 3 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0805194805194804,
			"nilai_komparatif_unit": 1.0840328648532602,
			"summary_team": 0.9454545454545454
		}]
	},
	"960049": {
		"nik": "960049",
		"nama": "AFIOLA NURHIDAYATI",
		"band": "VI",
		"posisi": "OFF 3 BUDGET OPEX",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9466666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0110663983903418,
			"nilai_komparatif_unit": 1.014353951191156,
			"summary_team": 0.9571428571428569
		}]
	},
	"960057": {
		"nik": "960057",
		"nama": "ZAKARIYAS DUTARAMA NASARANI",
		"band": "VI",
		"posisi": "OFF 3 SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8405797101449269,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0197044334975376,
			"nilai_komparatif_unit": 1.0230200734710193,
			"summary_team": 0.857142857142857
		}]
	},
	"960063": {
		"nik": "960063",
		"nama": "ADEN RIANGGA",
		"band": "VI",
		"posisi": "OFF 3 INDIGO ACCELERATION PROGRAM MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8740740740740738,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9825523429710868,
			"nilai_komparatif_unit": 0.9857471803351052,
			"summary_team": 0.8588235294117645
		}]
	},
	"960064": {
		"nik": "960064",
		"nama": "MOH. YUSUF FEBRIYANTO",
		"band": "VI",
		"posisi": "OFF 3 JOB PROFILE REVIEW & ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8377777777777772,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9127789046653149,
			"nilai_komparatif_unit": 0.9157468688359507,
			"summary_team": 0.764705882352941
		}]
	},
	"960073": {
		"nik": "960073",
		"nama": "DHEA TIRTA FITRIANTI",
		"band": "VI",
		"posisi": "OFF 3 LOGISTIC ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8740740740740743,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.022931206380857,
			"nilai_komparatif_unit": 1.026257338431068,
			"summary_team": 0.8941176470588235
		}]
	},
	"960081": {
		"nik": "960081",
		"nama": "CUT DYTA TRIVIANA",
		"band": "VI",
		"posisi": "OFF 3 ETHICS DOCUMENTATION & REPORT SYST",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666657,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0180995475113133,
			"nilai_komparatif_unit": 1.0214099690862533,
			"summary_team": 0.8823529411764706
		}]
	},
	"960086": {
		"nik": "960086",
		"nama": "HANNY RIANA",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444446,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9349030470914126,
			"nilai_komparatif_unit": 0.9379429494517799,
			"summary_team": 0.7894736842105263
		}]
	},
	"960087": {
		"nik": "960087",
		"nama": "ADITA DHIRA NARESWARI",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL BUSINESS ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8633333333333331,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0755653612796472,
			"nilai_komparatif_unit": 1.0790626369497345,
			"summary_team": 0.9285714285714284
		}]
	},
	"960088": {
		"nik": "960088",
		"nama": "NANDINI KHAIRANI PUTRI",
		"band": "VI",
		"posisi": "OFF 3 REPORTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8790123456790117,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0038003965631204,
			"nilai_komparatif_unit": 1.007064323453019,
			"summary_team": 0.8823529411764706
		}]
	},
	"960092": {
		"nik": "960092",
		"nama": "MUHAMMAD SATRIYO UTOMO MAHARDIKO",
		"band": "VI",
		"posisi": "OFF 3 REGULATORY AFFAIR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9177777777777774,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9998575701467033,
			"nilai_komparatif_unit": 1.003108676661947,
			"summary_team": 0.9176470588235295
		}]
	},
	"960095": {
		"nik": "960095",
		"nama": "HANIFA ANINDITA",
		"band": "VI",
		"posisi": "OFF 3 LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8358974358974363,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9788064696040151,
			"nilai_komparatif_unit": 0.9819891269999333,
			"summary_team": 0.8181818181818181
		}]
	},
	"960098": {
		"nik": "960098",
		"nama": "REZA PRASETYA UTAMA",
		"band": "VI",
		"posisi": "OFF 3  DIGITAL SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.814814814814815,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.999350649350649,
			"nilai_komparatif_unit": 1.0026001075776245,
			"summary_team": 0.8142857142857142
		}]
	},
	"960099": {
		"nik": "960099",
		"nama": "MINETA KARIMA NING ARUM",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8909090909090912,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.010204081632653,
			"nilai_komparatif_unit": 1.0134888305504812,
			"summary_team": 0.9000000000000001
		}]
	},
	"960101": {
		"nik": "960101",
		"nama": "DENNY BINTANG SAPUTRA",
		"band": "VI",
		"posisi": "OFF 3 ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8750000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9974025974025972,
			"nilai_komparatif_unit": 1.0006457214030093,
			"summary_team": 0.8727272727272727
		}]
	},
	"960117": {
		"nik": "960117",
		"nama": "MUTIA TRI RAMADHIAN",
		"band": "VI",
		"posisi": "OFF 3 CCAN FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.84,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9693877551020407,
			"nilai_komparatif_unit": 0.9725397868918758,
			"summary_team": 0.8142857142857142
		}]
	},
	"960120": {
		"nik": "960120",
		"nama": "RAIHAN RAZAFUAD",
		"band": "VI",
		"posisi": "OFF 3 PARTNERSHIP MARKETING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9523809523809521,
			"nilai_komparatif_unit": 0.9554776853674568,
			"summary_team": 0.857142857142857
		}]
	},
	"960123": {
		"nik": "960123",
		"nama": "ISHFI HANNA HAFNI NOORSYIFA",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER LEVERAGING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8928571428571426,
			"nilai_komparatif_unit": 0.8957603300319907,
			"summary_team": 0.7142857142857142
		}]
	},
	"960124": {
		"nik": "960124",
		"nama": "HIZKIA ALFREDO",
		"band": "VI",
		"posisi": "OFF 3 LEGAL & REGULATORY AFFAIR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8404040404040394,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.002918956043957,
			"nilai_komparatif_unit": 1.006180016868628,
			"summary_team": 0.8428571428571427
		}]
	},
	"960125": {
		"nik": "960125",
		"nama": "MUHAMMAD NURRAHMAN",
		"band": "VI",
		"posisi": "OFF 3 ENTERPRISE,OLO&PROJECT SUPERVISION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9926470588235291,
			"nilai_komparatif_unit": 0.9958747198590956,
			"summary_team": 0.8999999999999998
		}]
	},
	"960129": {
		"nik": "960129",
		"nama": "RIZKI ADAM",
		"band": "VI",
		"posisi": "OFF 3 CAREER & TALENT DEVELOPMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8814814814814814,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0543746910528917,
			"nilai_komparatif_unit": 1.0578030637831068,
			"summary_team": 0.9294117647058823
		}]
	},
	"960130": {
		"nik": "960130",
		"nama": "MIZALFIA NURSABRINA",
		"band": "VI",
		"posisi": "OFF 3 LOGISTIK ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.858333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.000571102227299,
			"nilai_komparatif_unit": 1.0038245288417902,
			"summary_team": 0.8588235294117645
		}]
	},
	"960133": {
		"nik": "960133",
		"nama": "THERESIA E SIBURIAN",
		"band": "VI",
		"posisi": "OFF 3 CRM & LEVERAGING CUSTOMER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006493506493506,
			"nilai_komparatif_unit": 1.0097661902178803,
			"summary_team": 0.8857142857142856
		}]
	},
	"960139": {
		"nik": "960139",
		"nama": "QONITA HAULA KINANTI",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL BUSINESS PARENTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8289855072463764,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9650349650349652,
			"nilai_komparatif_unit": 0.968172843424787,
			"summary_team": 0.7999999999999998
		}]
	},
	"960140": {
		"nik": "960140",
		"nama": "HERMAWAN MAULANA",
		"band": "VI",
		"posisi": "OFF 3 CCAN WIFI FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8592592592592596,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9310344827586203,
			"nilai_komparatif_unit": 0.9340618062126689,
			"summary_team": 0.8
		}]
	},
	"960142": {
		"nik": "960142",
		"nama": "YOGA TIKA PRATAMA",
		"band": "VI",
		"posisi": "OFF 3 COLLECTION PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8000000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0178571428571426,
			"nilai_komparatif_unit": 1.0211667762364693,
			"summary_team": 0.8142857142857142
		}]
	},
	"960148": {
		"nik": "960148",
		"nama": "KEMAS ICHWAN JAYA SATRIA",
		"band": "VI",
		"posisi": "OFF 3  DIGITAL SERVICE & WIFI",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.005494505494505,
			"nilai_komparatif_unit": 1.0087639408975648,
			"summary_team": 0.8714285714285712
		}]
	},
	"960150": {
		"nik": "960150",
		"nama": "INTAN RAHMAYUNI",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9115138592750531,
			"nilai_komparatif_unit": 0.91447771006251,
			"summary_team": 0.8142857142857142
		}]
	},
	"960153": {
		"nik": "960153",
		"nama": "MEUTIA NABILA",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9827770478065272,
			"summary_team": 0.8
		}]
	},
	"960156": {
		"nik": "960156",
		"nama": "NI KOMANG YULIA CEMPAKA SARI",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL & WIFI SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9416666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0619469026548671,
			"nilai_komparatif_unit": 1.0653998969584033,
			"summary_team": 1
		}]
	},
	"960157": {
		"nik": "960157",
		"nama": "INDI AULIANA",
		"band": "VI",
		"posisi": "OFF 3 FINANCE SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9249999999999998,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9266409266409267,
			"nilai_komparatif_unit": 0.9296539641413096,
			"summary_team": 0.857142857142857
		}]
	},
	"960161": {
		"nik": "960161",
		"nama": "DEILLA TSAMROTUL FUADAH",
		"band": "VI",
		"posisi": "OFF 3 DATA SCIENTIST",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0699300699300698,
			"nilai_komparatif_unit": 1.0734090220579158,
			"summary_team": 0.9272727272727272
		}]
	},
	"960166": {
		"nik": "960166",
		"nama": "NURUL ARIFAH WAHYUNI",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE UJG BERUNG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8282051282051274,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9000844356881518,
			"nilai_komparatif_unit": 0.9030111229089185,
			"summary_team": 0.7454545454545455
		}]
	},
	"960169": {
		"nik": "960169",
		"nama": "M ADI SETIAWAN",
		"band": "VI",
		"posisi": "OFF 3 QUALITY & CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7833333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9118541033434652,
			"nilai_komparatif_unit": 0.9148190604582037,
			"summary_team": 0.7142857142857144
		}]
	},
	"960170": {
		"nik": "960170",
		"nama": "MARTULAN SURYANTO",
		"band": "VI",
		"posisi": "OFF 3 COMPENSATION SYSTEM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8238095238095232,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1098265895953763,
			"nilai_komparatif_unit": 1.1134352680351414,
			"summary_team": 0.9142857142857141
		}]
	},
	"960176": {
		"nik": "960176",
		"nama": "AULYA PERMATASARI",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.869135802469135,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1505681818181832,
			"nilai_komparatif_unit": 1.1543093343821353,
			"summary_team": 1.0000000000000002
		}]
	},
	"960178": {
		"nik": "960178",
		"nama": "TIARA ANINDITA KUSUMAWARDHANI",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8980392156862743,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9718142119888846,
			"nilai_komparatif_unit": 0.9749741335722557,
			"summary_team": 0.8727272727272727
		}]
	},
	"960180": {
		"nik": "960180",
		"nama": "FAHRENDI RIZKY NASUTION",
		"band": "VI",
		"posisi": "OFF 3 CHANGE MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8933333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9754797441364603,
			"nilai_komparatif_unit": 0.9786515844528616,
			"summary_team": 0.8714285714285712
		}]
	},
	"960186": {
		"nik": "960186",
		"nama": "DAMAS WICAKSI WANGSA",
		"band": "VI",
		"posisi": "ENG 3 MOBILITY & FMC RESEARCH",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9240641711229945,
			"nilai_komparatif_unit": 0.9270688301233764,
			"summary_team": 0.8470588235294116
		}]
	},
	"960187": {
		"nik": "960187",
		"nama": "RIZKY ANDINI PUTRI",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9111111111111114,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0975609756097557,
			"nilai_komparatif_unit": 1.1011297715515203,
			"summary_team": 1
		}]
	},
	"960188": {
		"nik": "960188",
		"nama": "ALIEF AGUNG NAUVAL",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8637037037037024,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9671072545656355,
			"nilai_komparatif_unit": 0.970251871149172,
			"summary_team": 0.8352941176470587
		}]
	},
	"960190": {
		"nik": "960190",
		"nama": "FAUZIA RAMADHANI",
		"band": "VI",
		"posisi": "OFF 3 TAX OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272723,
			"nilai_komparatif_unit": 1.0260527416730074,
			"summary_team": 0.8999999999999999
		}]
	},
	"960192": {
		"nik": "960192",
		"nama": "NUR HIDAYATI",
		"band": "VI",
		"posisi": "OFF 3 PAYMENT & COLLECTION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9245283018867927,
			"nilai_komparatif_unit": 0.9275344700406731,
			"summary_team": 0.8166666666666669
		}]
	},
	"960193": {
		"nik": "960193",
		"nama": "BINARTI FAUZIAH FITRIANI",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9416666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9267900241351568,
			"nilai_komparatif_unit": 0.9298035464364247,
			"summary_team": 0.8727272727272727
		}]
	},
	"960194": {
		"nik": "960194",
		"nama": "ARIESTA MIRANIA FABIOLA",
		"band": "VI",
		"posisi": "OFF 3 PROMOTION & SALES SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8844444444444447,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9852835606604445,
			"nilai_komparatif_unit": 0.9884872787689705,
			"summary_team": 0.8714285714285712
		}]
	},
	"960198": {
		"nik": "960198",
		"nama": "RIAN GILANG PRABOWO",
		"band": "VI",
		"posisi": "OFF 3 FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9440559440559441,
			"nilai_komparatif_unit": 0.9471256076981611,
			"summary_team": 0.8181818181818182
		}]
	},
	"960201": {
		"nik": "960201",
		"nama": "NURUL FADHILAH",
		"band": "VI",
		"posisi": "OFF 3 ACCESS DATA MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8266666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9677419354838707,
			"nilai_komparatif_unit": 0.9708886157766092,
			"summary_team": 0.7999999999999999
		}]
	},
	"960202": {
		"nik": "960202",
		"nama": "KHARISMA YUDHISTIRA",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.869135802469135,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8995351239669429,
			"nilai_komparatif_unit": 0.9024600250623963,
			"summary_team": 0.7818181818181817
		}]
	},
	"960203": {
		"nik": "960203",
		"nama": "DESINTA PUTRI ELIYANA",
		"band": "VI",
		"posisi": "OFF 3 PERSONNEL COST REPORTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8755555555555556,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.011602610587382,
			"nilai_komparatif_unit": 1.0148919069194944,
			"summary_team": 0.8857142857142857
		}]
	},
	"960207": {
		"nik": "960207",
		"nama": "TSANIYA SILMI RIVAI",
		"band": "VI",
		"posisi": "OFF 3 LOGISTIK & LEGAL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.962566844919786,
			"nilai_komparatif_unit": 0.9656966980451838,
			"summary_team": 0.8181818181818181
		}]
	},
	"960208": {
		"nik": "960208",
		"nama": "PEBBY ROSMAULY",
		"band": "VI",
		"posisi": "OFF 3 ACCESS MAINTENANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.866666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9807692307692305,
			"nilai_komparatif_unit": 0.983958270219756,
			"summary_team": 0.8500000000000001
		}]
	},
	"960209": {
		"nik": "960209",
		"nama": "BUDIYANTO ZHEZA ARDANA",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9827770478065272,
			"summary_team": 0.8
		}]
	},
	"960210": {
		"nik": "960210",
		"nama": "MUHAMAD FARIZ AGUNG",
		"band": "VI",
		"posisi": "OFF 3 SOLUTION INTEGRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0045248868778278,
			"nilai_komparatif_unit": 1.0077911694984352,
			"summary_team": 0.8705882352941177
		}]
	},
	"960212": {
		"nik": "960212",
		"nama": "IQBAL FIRDA RUSDIANSYAH",
		"band": "VI",
		"posisi": "OFF 3 OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9083333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9808173477898245,
			"nilai_komparatif_unit": 0.9840065436961931,
			"summary_team": 0.8909090909090909
		}]
	},
	"960215": {
		"nik": "960215",
		"nama": "ASTRID MARION INDIANI",
		"band": "VI",
		"posisi": "OFF 3 BS OBL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8461538461538458,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9454545454545459,
			"nilai_komparatif_unit": 0.9485287567466033,
			"summary_team": 0.8
		}]
	},
	"960218": {
		"nik": "960218",
		"nama": "CANDRA EVITA TRYARIESTYA",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL & WIFI SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444448,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9473684210526313,
			"nilai_komparatif_unit": 0.9504488554444702,
			"summary_team": 0.8
		}]
	},
	"960220": {
		"nik": "960220",
		"nama": "IRA SAFITRI",
		"band": "VI",
		"posisi": "OFF 3 ACCESS CAPEX QE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9200000000000003,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9937888198757759,
			"nilai_komparatif_unit": 0.9970201934269113,
			"summary_team": 0.9142857142857141
		}]
	},
	"960221": {
		"nik": "960221",
		"nama": "KING ABDUL AZIZ",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9238095238095237,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0206185567010306,
			"nilai_komparatif_unit": 1.0239371690097643,
			"summary_team": 0.9428571428571426
		}]
	},
	"960222": {
		"nik": "960222",
		"nama": "YASYA DWINIDA",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE PLERED",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666659,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.090909090909092,
			"nilai_komparatif_unit": 1.0944562577845427,
			"summary_team": 0.9454545454545454
		}]
	},
	"960223": {
		"nik": "960223",
		"nama": "IRVANSYAH",
		"band": "VI",
		"posisi": "OFF 3 OPERATION & MAINTENANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8304347826086982,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0509281294621577,
			"nilai_komparatif_unit": 1.0543452954573564,
			"summary_team": 0.8727272727272728
		}]
	},
	"960224": {
		"nik": "960224",
		"nama": "SHASHANTI PADMA ASTARI",
		"band": "VI",
		"posisi": "OFF 3 ASSET MGT & FACILITY SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8358974358974363,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0254163014899205,
			"nilai_komparatif_unit": 1.02875051399993,
			"summary_team": 0.857142857142857
		}]
	},
	"960225": {
		"nik": "960225",
		"nama": "AKBAR DANAR ABIWARDANA",
		"band": "VI",
		"posisi": "OFF 3 WIRELESS NODE & IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0196078431372546,
			"nilai_komparatif_unit": 1.0229231690404537,
			"summary_team": 0.8666666666666665
		}]
	},
	"960227": {
		"nik": "960227",
		"nama": "ABU AMAR TANTOWI",
		"band": "VI",
		"posisi": "OFF 3 CNOP FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9860139860139858,
			"nilai_komparatif_unit": 0.9892200791514125,
			"summary_team": 0.8545454545454545
		}]
	},
	"960230": {
		"nik": "960230",
		"nama": "RIVALDI",
		"band": "VI",
		"posisi": "OFF 3 OM TRANSPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0227272727272725,
			"nilai_komparatif_unit": 1.0260527416730076,
			"summary_team": 0.8999999999999999
		}]
	},
	"960232": {
		"nik": "960232",
		"nama": "IWAN SITOHANG",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0279720279720277,
			"nilai_komparatif_unit": 1.031314550604664,
			"summary_team": 0.8909090909090909
		}]
	},
	"960234": {
		"nik": "960234",
		"nama": "FAHMI AFIFI UTAMA",
		"band": "VI",
		"posisi": "OFF 3 CONSUMER FULFILLMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9607843137254901,
			"nilai_komparatif_unit": 0.9639083708265817,
			"summary_team": 0.8166666666666667
		}]
	},
	"960236": {
		"nik": "960236",
		"nama": "ARRUM PRIMA DEWI",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE HEGARMANAH",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8282051282051274,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9878975513650445,
			"nilai_komparatif_unit": 0.9911097690463738,
			"summary_team": 0.8181818181818181
		}]
	},
	"960238": {
		"nik": "960238",
		"nama": "ZAHRA ZETTIRA ZUKHRUFULJANNAH",
		"band": "VI",
		"posisi": "OFF 3 FACILITIES ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8888888888888893,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9397058823529406,
			"nilai_komparatif_unit": 0.9427614014666101,
			"summary_team": 0.8352941176470587
		}]
	},
	"960241": {
		"nik": "960241",
		"nama": "WILLYBRORDUS BIMA SATRIA",
		"band": "VI",
		"posisi": "OFF 3 SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0370370370370372,
			"nilai_komparatif_unit": 1.0404090351778978,
			"summary_team": 0.9333333333333335
		}]
	},
	"960245": {
		"nik": "960245",
		"nama": "HANNI NOVITASARI TANDIARI",
		"band": "VI",
		"posisi": "OFF 3 DEBT MGT & COLLECTION PARTNERSHIP",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365347,
			"nilai_komparatif_unit": 0.991205667427509,
			"summary_team": 0.8727272727272727
		}]
	},
	"960246": {
		"nik": "960246",
		"nama": "IGOR M. FARHAN",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CIRANJANG",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8222222222222224,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9066339066339064,
			"nilai_komparatif_unit": 0.9095818899155311,
			"summary_team": 0.7454545454545455
		}]
	},
	"960248": {
		"nik": "960248",
		"nama": "NINA RESTU ARIANTI",
		"band": "VI",
		"posisi": "OFF 3 PROJECT DATA MGT & ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8166666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9795918367346939,
			"nilai_komparatif_unit": 0.9827770478065272,
			"summary_team": 0.8
		}]
	},
	"960250": {
		"nik": "960250",
		"nama": "WILDAN GUNTUR PRAKOSO",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365347,
			"nilai_komparatif_unit": 0.991205667427509,
			"summary_team": 0.8727272727272727
		}]
	},
	"960251": {
		"nik": "960251",
		"nama": "IRAWATI",
		"band": "VI",
		"posisi": "JUNIOR ACCOUNT MANAGER 2",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0369036903690365,
			"nilai_komparatif_unit": 1.0402752549239205,
			"summary_team": 0.8727272727272727
		}]
	},
	"960257": {
		"nik": "960257",
		"nama": "AKBAR RAHMAN RIZALDI",
		"band": "VI",
		"posisi": "OFF 3 EXTERNAL CONTENT MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8911111111111103,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0033739181458128,
			"nilai_komparatif_unit": 1.0066364583114393,
			"summary_team": 0.8941176470588235
		}]
	},
	"960265": {
		"nik": "960265",
		"nama": "ANISA DYAH PUSPITA",
		"band": "VI",
		"posisi": "AUDITOR PRATAMA III",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8729493891797606,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0309876049580107,
			"nilai_komparatif_unit": 1.0343399329492091,
			"summary_team": 0.8999999999999998
		}]
	},
	"960269": {
		"nik": "960269",
		"nama": "AUFAZ ZIHNI",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER TOUCH POINT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9333333333333335,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0561224489795915,
			"nilai_komparatif_unit": 1.059556504666412,
			"summary_team": 0.9857142857142855
		}]
	},
	"960271": {
		"nik": "960271",
		"nama": "BAMBANG IRAWAN",
		"band": "VI",
		"posisi": "OFF 3 ASSET ACCOUNTING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9133333333333336,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9854014598540143,
			"nilai_komparatif_unit": 0.988605561319978,
			"summary_team": 0.8999999999999999
		}]
	},
	"960272": {
		"nik": "960272",
		"nama": "BILLY ANUGRAH",
		"band": "VI",
		"posisi": "OFF 3 AUDIT QUALITY ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8592592592592581,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9447261663286017,
			"nilai_komparatif_unit": 0.9477980092452098,
			"summary_team": 0.8117647058823529
		}]
	},
	"960274": {
		"nik": "960274",
		"nama": "CUT MAULIDIA NANDA",
		"band": "VI",
		"posisi": "OFF 3 WIRELESS OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8745098039215687,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9979616795760293,
			"nilai_komparatif_unit": 1.0012066214710607,
			"summary_team": 0.8727272727272728
		}]
	},
	"960278": {
		"nik": "960278",
		"nama": "DINAR PRABANDARI",
		"band": "VI",
		"posisi": "OFF 3 FINANCE SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0303030303030303,
			"nilai_komparatif_unit": 1.0336531323520672,
			"summary_team": 0.9272727272727272
		}]
	},
	"960279": {
		"nik": "960279",
		"nama": "ELIDA AULIA HAFIZ",
		"band": "VI",
		"posisi": "OFF 3 RETIREMENT EXPERIENCE PROGRAM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8212121212121206,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0437532946758044,
			"nilai_komparatif_unit": 1.0471471311960696,
			"summary_team": 0.857142857142857
		}]
	},
	"960280": {
		"nik": "960280",
		"nama": "EMI RAHMI",
		"band": "VI",
		"posisi": "OFF 3 ASSET MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.986013986013986,
			"nilai_komparatif_unit": 0.9892200791514127,
			"summary_team": 0.8545454545454546
		}]
	},
	"960284": {
		"nik": "960284",
		"nama": "FEBRINDA RIZKY RAMADHANI",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL & WIFI SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8125,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9846153846153847,
			"nilai_komparatif_unit": 0.987816930102971,
			"summary_team": 0.8
		}]
	},
	"960285": {
		"nik": "960285",
		"nama": "FRANS MICHAEL SIMANJUNTAK",
		"band": "VI",
		"posisi": "OFF 3 BIDDING MANAGEMENT & OBL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8299999999999994,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9638554216867478,
			"nilai_komparatif_unit": 0.9669894647092344,
			"summary_team": 0.8
		}]
	},
	"960290": {
		"nik": "960290",
		"nama": "IBRAHIM MUHAMMAD DIIN",
		"band": "VI",
		"posisi": "OFF 3 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7833333333333337,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9516441005802704,
			"nilai_komparatif_unit": 0.9547384376418339,
			"summary_team": 0.7454545454545455
		}]
	},
	"960299": {
		"nik": "960299",
		"nama": "MUHAMAD RIZKI LAZUARDI IHSAN",
		"band": "VI",
		"posisi": "OFF 3 INTERNAL ASSESSMENT EVALUATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9030303030303026,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8859060402684569,
			"nilai_komparatif_unit": 0.8887866254491921,
			"summary_team": 0.8
		}]
	},
	"960305": {
		"nik": "960305",
		"nama": "NABIH HABIBI NOOR",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL BUSINESS ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8633333333333331,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9762824048538336,
			"nilai_komparatif_unit": 0.9794568550774513,
			"summary_team": 0.8428571428571427
		}]
	},
	"960309": {
		"nik": "960309",
		"nama": "NOVRISCA RIZKY PURNAMASARI",
		"band": "VI",
		"posisi": "OFF 3 SALES ENGINEER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144384,
			"nilai_komparatif_unit": 1.0300764779148628,
			"summary_team": 0.8727272727272728
		}]
	},
	"960310": {
		"nik": "960310",
		"nama": "ODHY ANUGRAH AGUSTA",
		"band": "VI",
		"posisi": "OFF 3 CCAN FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9142857142857144,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0208333333333335,
			"nilai_komparatif_unit": 1.0241526440032431,
			"summary_team": 0.9333333333333335
		}]
	},
	"960313": {
		"nik": "960313",
		"nama": "PRANANDA BANGUN",
		"band": "VI",
		"posisi": "OFF 3 ACCESS NETWORK ELEMENT OM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9133333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0479666319082372,
			"nilai_komparatif_unit": 1.051374168387913,
			"summary_team": 0.9571428571428569
		}]
	},
	"960319": {
		"nik": "960319",
		"nama": "RENGGA SANDITYA",
		"band": "VI",
		"posisi": "OFF 3 SALES & CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0084033613445376,
			"nilai_komparatif_unit": 1.0116822550949542,
			"summary_team": 0.9142857142857141
		}]
	},
	"960325": {
		"nik": "960325",
		"nama": "RIZKY REZA SETIAWAN",
		"band": "VI",
		"posisi": "OFF 3 PLANNING & EVALUATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.858333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0416904625928045,
			"nilai_komparatif_unit": 1.045077591670905,
			"summary_team": 0.8941176470588235
		}]
	},
	"960336": {
		"nik": "960336",
		"nama": "YOSIA BAGARIANG",
		"band": "VI",
		"posisi": "OFF 3 NETWORK DEPLOYMENT & PROJECT SUPER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8805555555555549,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0220820189274455,
			"nilai_komparatif_unit": 1.0254053897855178,
			"summary_team": 0.8999999999999999
		}]
	},
	"970008": {
		"nik": "970008",
		"nama": "MENTARI RIZKI AMELIA",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE BATUJAJAR",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8574712643678148,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9996170049789366,
			"nilai_komparatif_unit": 1.0028673292797854,
			"summary_team": 0.857142857142857
		}]
	},
	"970020": {
		"nik": "970020",
		"nama": "ADITIA RAHMADI ARNATHA",
		"band": "VI",
		"posisi": "OFF 3 OFFICE ADMINISTRATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8303030303030305,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.006011163589523,
			"nilai_komparatif_unit": 1.0092822789423566,
			"summary_team": 0.8352941176470587
		}]
	},
	"970021": {
		"nik": "970021",
		"nama": "MONICA ROSAVINA",
		"band": "VI",
		"posisi": "OFF 3 FINANCE SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9249999999999998,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.048648648648649,
			"nilai_komparatif_unit": 1.0520584027532491,
			"summary_team": 0.9700000000000001
		}]
	},
	"970022": {
		"nik": "970022",
		"nama": "SELMA KHAIRUNNISA",
		"band": "VI",
		"posisi": "OFF 3 FINANCE SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8133333333333334,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0362997658079625,
			"nilai_komparatif_unit": 1.0396693666600814,
			"summary_team": 0.8428571428571427
		}]
	},
	"970031": {
		"nik": "970031",
		"nama": "LIDYA STEPHANIE",
		"band": "VI",
		"posisi": "OFF 3 LEVERAGING CUSTOMER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8208333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9081679741578219,
			"nilai_komparatif_unit": 0.9111209455668267,
			"summary_team": 0.7454545454545455
		}]
	},
	"970037": {
		"nik": "970037",
		"nama": "ALDI WIRANATA",
		"band": "VI",
		"posisi": "ENG 3 FIBER ACCESS QUALITY ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8138888888888886,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8817506524794221,
			"nilai_komparatif_unit": 0.8846177261273974,
			"summary_team": 0.7176470588235293
		}]
	},
	"970040": {
		"nik": "970040",
		"nama": "WILDAN ALMAS ZUFAR",
		"band": "VI",
		"posisi": "OFF 3 CCAN FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.85,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0909090909090908,
			"nilai_komparatif_unit": 1.0944562577845416,
			"summary_team": 0.9272727272727272
		}]
	},
	"970041": {
		"nik": "970041",
		"nama": "DEWI SUCILIA KURNIAWATI",
		"band": "VI",
		"posisi": "OFF 3 ACCESS MAINTENANCE & QE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8370370370370372,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0860820595333869,
			"nilai_komparatif_unit": 1.0896135309801853,
			"summary_team": 0.9090909090909092
		}]
	},
	"970042": {
		"nik": "970042",
		"nama": "RIZKY SAM RAMADHAN",
		"band": "VI",
		"posisi": "OFF 3 COMMAND CONTROL",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9583687340696684,
			"nilai_komparatif_unit": 0.9614849367452981,
			"summary_team": 0.8545454545454546
		}]
	},
	"970045": {
		"nik": "970045",
		"nama": "REZA IZZUDDIN",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.01010101010101,
			"nilai_komparatif_unit": 1.0133854238745754,
			"summary_team": 0.9090909090909092
		}]
	},
	"970046": {
		"nik": "970046",
		"nama": "SYIFA HANIFAH",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE JL CAGAK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8722222222222226,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0005790387955988,
			"nilai_komparatif_unit": 1.0038324912163945,
			"summary_team": 0.8727272727272727
		}]
	},
	"970048": {
		"nik": "970048",
		"nama": "AMANINTIA SYAIRA AULIA",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE TURANGGA",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9499999999999998,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0526315789473686,
			"nilai_komparatif_unit": 1.0560542838271896,
			"summary_team": 1
		}]
	},
	"970050": {
		"nik": "970050",
		"nama": "ABDUL KHAMID RIDWANUDDIN",
		"band": "VI",
		"posisi": "OFF 3 OM IP NETWORK & SWITCHING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8916666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9787595581988102,
			"nilai_komparatif_unit": 0.9819420630590278,
			"summary_team": 0.8727272727272727
		}]
	},
	"970054": {
		"nik": "970054",
		"nama": "BRIAN ANJAR WAHYU RAHARJO",
		"band": "VI",
		"posisi": "OFF 3 TELKOMGROUP ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144382,
			"nilai_komparatif_unit": 1.0300764779148626,
			"summary_team": 0.8727272727272727
		}]
	},
	"970058": {
		"nik": "970058",
		"nama": "ADITYA RIZKI FAUZI",
		"band": "VI",
		"posisi": "OFF 3 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9625668449197858,
			"nilai_komparatif_unit": 0.9656966980451837,
			"summary_team": 0.8181818181818181
		}]
	},
	"970063": {
		"nik": "970063",
		"nama": "NANDANI PUTRI TAVITA",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8500000000000003,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8983957219251334,
			"nilai_komparatif_unit": 0.9013169181755046,
			"summary_team": 0.7636363636363637
		}]
	},
	"970064": {
		"nik": "970064",
		"nama": "TRIFANDI WIBOWO",
		"band": "VI",
		"posisi": "OFF 3 CCAN FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8637037037037024,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0420240137221284,
			"nilai_komparatif_unit": 1.0454122273649529,
			"summary_team": 0.8999999999999999
		}]
	},
	"970065": {
		"nik": "970065",
		"nama": "RAHMA NUR AULIASARI",
		"band": "VI",
		"posisi": "OFF 3 ADVALJAR OPERATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7809523809523812,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8963414634146342,
			"nilai_komparatif_unit": 0.8992559801004085,
			"summary_team": 0.7000000000000002
		}]
	},
	"970066": {
		"nik": "970066",
		"nama": "FAUZAN GHIFARI PRAKOSO",
		"band": "VI",
		"posisi": "OFF 3 VALUE ADDED RESELLER",
		"category": [{
			"code": "DE-1-01",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9977008687726399,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0069930069930069,
			"nilai_komparatif_unit": 1.0093135512969915,
			"summary_team": 0.8727272727272728
		}]
	},
	"970067": {
		"nik": "970067",
		"nama": "KARRINA SWASTIKANINGTYAS",
		"band": "VI",
		"posisi": "OFF 3 BGES SALES ENGINEER, BIDDING & PM",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8980392156862743,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.020742358078603,
			"nilai_komparatif_unit": 1.0240613729361367,
			"summary_team": 0.9166666666666667
		}]
	},
	"970070": {
		"nik": "970070",
		"nama": "RAHMADINA ARIFAZHARI",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9500000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9878542510121456,
			"nilai_komparatif_unit": 0.9910663278993622,
			"summary_team": 0.9384615384615385
		}]
	},
	"970072": {
		"nik": "970072",
		"nama": "IKHBAL MUSTOFA",
		"band": "VI",
		"posisi": "OFF 3 LOGISTIK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8416666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9504950495049503,
			"nilai_komparatif_unit": 0.9535856503469273,
			"summary_team": 0.8
		}]
	},
	"970075": {
		"nik": "970075",
		"nama": "AS'AT RAHMAT SETIAWAN",
		"band": "VI",
		"posisi": "OFF 3 ACCESS PROVISIONING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.925324675324675,
			"nilai_komparatif_unit": 0.9283334329422449,
			"summary_team": 0.8142857142857142
		}]
	},
	"970078": {
		"nik": "970078",
		"nama": "VANIA DWINDA OKTAVIANA",
		"band": "VI",
		"posisi": "OFF 3 BUSINESS OBL & DELIVERY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8545454545454549,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.8723404255319145,
			"nilai_komparatif_unit": 0.875176901171681,
			"summary_team": 0.7454545454545455
		}]
	},
	"970083": {
		"nik": "970083",
		"nama": "ADHITYA CAESARALI QASTHARI",
		"band": "VI",
		"posisi": "OFF 3 EXECUTIVE DATA & REPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8368421052631614,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9981502034776132,
			"nilai_komparatif_unit": 1.0013957583712385,
			"summary_team": 0.8352941176470587
		}]
	},
	"970087": {
		"nik": "970087",
		"nama": "AHMAD FAUZI HAQQONI",
		"band": "VI",
		"posisi": "OFF 3 TGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8277777777777782,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9664429530201338,
			"nilai_komparatif_unit": 0.9695854095809359,
			"summary_team": 0.8
		}]
	},
	"970089": {
		"nik": "970089",
		"nama": "ALFIN YURISMAN",
		"band": "VI",
		"posisi": "OFF 3 SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9066666666666668,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0084033613445376,
			"nilai_komparatif_unit": 1.0116822550949542,
			"summary_team": 0.9142857142857141
		}]
	},
	"970090": {
		"nik": "970090",
		"nama": "ALFONSO PARNINGOTAN GULTOM",
		"band": "VI",
		"posisi": "OFF 3 TGROUP FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8633333333333328,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9898209898209904,
			"nilai_komparatif_unit": 0.9930394616963995,
			"summary_team": 0.8545454545454546
		}]
	},
	"970096": {
		"nik": "970096",
		"nama": "AZHARA AZIZA RAHANDA",
		"band": "VI",
		"posisi": "HEAD OF REPRESENTATIVE OFFICE CILIMUS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8833333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9879931389365352,
			"nilai_komparatif_unit": 0.9912056674275095,
			"summary_team": 0.8727272727272727
		}]
	},
	"970098": {
		"nik": "970098",
		"nama": "BUNGA ALIKA FIRDAUSI",
		"band": "VI",
		"posisi": "OFF 3 TRANSACT&STRAT CONTRACT COMPLIANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.856249999999999,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9892657793044238,
			"nilai_komparatif_unit": 0.9924824458741757,
			"summary_team": 0.8470588235294119
		}]
	},
	"970099": {
		"nik": "970099",
		"nama": "CAHYA BUANA PUTRI",
		"band": "VI",
		"posisi": "OFF 3 MARKETING PROJECT PLANNING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0519031141868522,
			"nilai_komparatif_unit": 1.055323450412777,
			"summary_team": 0.8941176470588235
		}]
	},
	"970100": {
		"nik": "970100",
		"nama": "CHICIO ZEVENIA SEMBIRING",
		"band": "VI",
		"posisi": "OFF 3 PARTNERSHIP MARKETING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9999999999999998,
			"nilai_komparatif_unit": 1.0032515696358297,
			"summary_team": 0.8999999999999999
		}]
	},
	"970102": {
		"nik": "970102",
		"nama": "CLAUDIA SHANTIKA PRIMASWARI",
		"band": "VI",
		"posisi": "OFF 3 PENGELOLAAN PROG.KERJA&EVAL.USULAN",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7999999999999999,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9318181818181819,
			"nilai_komparatif_unit": 0.9348480535242961,
			"summary_team": 0.7454545454545455
		}]
	},
	"970103": {
		"nik": "970103",
		"nama": "CORNELIA REGINA SINTA MAHARANI",
		"band": "VI",
		"posisi": "OFF 3 DOMESTIC INTERCONNECTION BILLING",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8222222222222225,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9729729729729724,
			"nilai_komparatif_unit": 0.9761366623483745,
			"summary_team": 0.7999999999999998
		}]
	},
	"970112": {
		"nik": "970112",
		"nama": "FENITA NABILAH",
		"band": "VI",
		"posisi": "OFF 3 QUALITY & GOVERNANCE SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.83030303030303,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9351653069987121,
			"nilai_komparatif_unit": 0.9382060621154307,
			"summary_team": 0.776470588235294
		}]
	},
	"970114": {
		"nik": "970114",
		"nama": "GIGIH BAGAS WICAKSONO",
		"band": "VI",
		"posisi": "OFF 3 CUSTOMER CARE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9000000000000001,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.01010101010101,
			"nilai_komparatif_unit": 1.0133854238745754,
			"summary_team": 0.9090909090909092
		}]
	},
	"970118": {
		"nik": "970118",
		"nama": "IVAN FERNALDY",
		"band": "VI",
		"posisi": "OFF 3 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0181818181818179,
			"nilai_komparatif_unit": 1.0214925072655718,
			"summary_team": 0.9333333333333331
		}]
	},
	"970125": {
		"nik": "970125",
		"nama": "M. FAIZ AZHARI",
		"band": "VI",
		"posisi": "OFF 3 CCAN ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666669,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9890109890109885,
			"nilai_komparatif_unit": 0.9922268271123588,
			"summary_team": 0.857142857142857
		}]
	},
	"970128": {
		"nik": "970128",
		"nama": "MUHAMMAD IKHSAN RIVKI AZHAR",
		"band": "VI",
		"posisi": "OFF 3 INFRA OPERATION SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8800000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9740259740259736,
			"nilai_komparatif_unit": 0.9771930873076261,
			"summary_team": 0.857142857142857
		}]
	},
	"970130": {
		"nik": "970130",
		"nama": "MUHAMMAD SYIFA AL-MUWAFFAQ HADI",
		"band": "VI",
		"posisi": "OFF 3 CCAN FULFILLMENT & ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8245614035087716,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0584139264990333,
			"nilai_komparatif_unit": 1.061855433084577,
			"summary_team": 0.8727272727272727
		}]
	},
	"970133": {
		"nik": "970133",
		"nama": "NABILAH ZAKI LISMIA",
		"band": "VI",
		"posisi": "OFF 3 IT MANAGEMENT SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8444444444444444,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0150375939849623,
			"nilai_komparatif_unit": 1.0183380594047897,
			"summary_team": 0.857142857142857
		}]
	},
	"970134": {
		"nik": "970134",
		"nama": "NABILLA MAHARANI",
		"band": "VI",
		"posisi": "OFF 3 WOC 24/7",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8637037037037024,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0420240137221284,
			"nilai_komparatif_unit": 1.0454122273649529,
			"summary_team": 0.8999999999999999
		}]
	},
	"970140": {
		"nik": "970140",
		"nama": "PAIAN FERNANDO SIMARMATA",
		"band": "VI",
		"posisi": "OFF 3 SERVICE DELIVERY & VAT SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8400000000000002,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0034013605442174,
			"nilai_komparatif_unit": 1.0066639899407133,
			"summary_team": 0.8428571428571427
		}]
	},
	"970145": {
		"nik": "970145",
		"nama": "RAHAYU EKA SULISTIYANI",
		"band": "VI",
		"posisi": "OFF 3 HC SERVICE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9166666666666666,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0115702479338844,
			"nilai_komparatif_unit": 1.014859439036575,
			"summary_team": 0.9272727272727272
		}]
	},
	"970146": {
		"nik": "970146",
		"nama": "RATNA KENCANA PUTRI",
		"band": "VI",
		"posisi": "OFF 3 INDIGO STARTUP ASSESSMENT MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8740740740740738,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 1.0094715852442675,
			"nilai_komparatif_unit": 1.012753952399081,
			"summary_team": 0.8823529411764706
		}]
	},
	"970151": {
		"nik": "970151",
		"nama": "RIFQI RAFIANSYAH PRASETYO",
		"band": "VI",
		"posisi": "OFF 3 CAPEX MANAGEMENT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8207407407407394,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.0791645177926783,
			"nilai_komparatif_unit": 1.082673496370798,
			"summary_team": 0.8857142857142857
		}]
	},
	"970161": {
		"nik": "970161",
		"nama": "TALITHA AZARINE",
		"band": "VI",
		"posisi": "OFF 3 PORTFOLIO RESTRUCTURING ANALYSIS",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9111111111111112,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9878048780487803,
			"nilai_komparatif_unit": 0.9910167943963684,
			"summary_team": 0.8999999999999999
		}]
	},
	"970165": {
		"nik": "970165",
		"nama": "WIDYA PRAMESTI",
		"band": "VI",
		"posisi": "OFF 3 DIGITAL & WIFI SUPPORT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8637037037037024,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9758637588826281,
			"nilai_komparatif_unit": 0.9790368478497177,
			"summary_team": 0.8428571428571427
		}]
	},
	"970169": {
		"nik": "970169",
		"nama": "YONITA ERSALINA LEKSONO",
		"band": "VI",
		"posisi": "OFF 3 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9466666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9718309859154929,
			"nilai_komparatif_unit": 0.9749909620404543,
			"summary_team": 0.9199999999999999
		}]
	},
	"970170": {
		"nik": "970170",
		"nama": "YOSUA ARINTO WICAKSONO",
		"band": "VI",
		"posisi": "OFF 3 INDIGO STARTUP INTEGRATION MGT",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.877777777777778,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9516008935219654,
			"nilai_komparatif_unit": 0.9546950900927701,
			"summary_team": 0.8352941176470587
		}]
	},
	"980005": {
		"nik": "980005",
		"nama": "ZAHRA RIZKY DWITA ANISSA",
		"band": "VI",
		"posisi": "OFF 3 PERFORMANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8666666666666667,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9519230769230771,
			"nilai_komparatif_unit": 0.955018321095646,
			"summary_team": 0.8250000000000002
		}]
	},
	"980006": {
		"nik": "980006",
		"nama": "VEBERIA PANJAITAN",
		"band": "VI",
		"posisi": "OFF 3 DATA CUSTOMER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8499999999999992,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Above Average",
			"nilai_komparatif": 1.0267379679144395,
			"nilai_komparatif_unit": 1.030076477914864,
			"summary_team": 0.8727272727272727
		}]
	},
	"980011": {
		"nik": "980011",
		"nama": "CAESA CRISTY",
		"band": "VI",
		"posisi": "OFF 3 LOGISTIK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9444444444444444,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9342560553633218,
			"nilai_komparatif_unit": 0.9372938539850314,
			"summary_team": 0.8823529411764706
		}]
	},
	"980012": {
		"nik": "980012",
		"nama": "NUR SUBHAN",
		"band": "VI",
		"posisi": "OFF 3 OM IP NETWORK",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.7888888888888893,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Above Average",
			"feedback_unit": "Beyond Average",
			"nilai_komparatif": 1.1062740076824578,
			"nilai_komparatif_unit": 1.1098711346547459,
			"summary_team": 0.8727272727272727
		}]
	},
	"980015": {
		"nik": "980015",
		"nama": "FAIRUZ HABIBAH RAMDHANI",
		"band": "VI",
		"posisi": "OFF 3 BES TERITORY SALES",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8874999999999998,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9979879275653923,
			"nilai_komparatif_unit": 1.001232954807589,
			"summary_team": 0.8857142857142856
		}]
	},
	"980016": {
		"nik": "980016",
		"nama": "ANISA EKA DESTIYANA",
		"band": "VI",
		"posisi": "OFF 3 CRM & LEVERAGING CUSTOMER",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.858333333333333,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9320388349514568,
			"nilai_komparatif_unit": 0.9350694241265992,
			"summary_team": 0.8
		}]
	},
	"980019": {
		"nik": "980019",
		"nama": "BELTIEN HANNY PRAMASTERINA",
		"band": "VI",
		"posisi": "OFF 3 SUBSIDIARY",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.9111111111111112,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9813486370157818,
			"nilai_komparatif_unit": 0.9845395604460654,
			"summary_team": 0.8941176470588235
		}]
	},
	"980020": {
		"nik": "980020",
		"nama": "CITRA PUTRI ADIYANTI",
		"band": "VI",
		"posisi": "OFF 3 DATA ASSURANCE",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8753623188405791,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Below Average",
			"nilai_komparatif": 0.9139072847682124,
			"nilai_komparatif_unit": 0.9168789179453285,
			"summary_team": 0.7999999999999998
		}]
	},
	"980026": {
		"nik": "980026",
		"nama": "MAULIDYA ELISKA",
		"band": "VI",
		"posisi": "OFF 3 VERIFICATION",
		"category": [{
			"code": "MD-1-03",
			"kriteria": 0.8622222222222221,
			"kriteria_bp": 0.9967589688027997,
			"feedback": "Average",
			"feedback_unit": "Average",
			"nilai_komparatif": 0.9941089837997054,
			"nilai_komparatif_unit": 0.9973413983861342,
			"summary_team": 0.857142857142857
		}]
	}
};