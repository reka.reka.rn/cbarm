// import { cbaCategoryInfo } from "../dummy/cbarmKategori";
import { cbaBehaviorInfo } from "../dummy/cbarmBehavior";
// import { DataBehavior1 } from "../dummy/cbarmB1";
// import { DataBehavior2 } from "../dummy/cbarmB2";
// import { DataBehavior3 } from "../dummy/cbarmB3";
// import { DataBehavior4 } from "../dummy/cbarmB4";
// import { DataBehavior5 } from "../dummy/cbarmB5";
// import { DataBehavior6 } from "../dummy/cbarmB6";
// import { DataBehavior7 } from "../dummy/cbarmB7";
// import { DataBehavior8 } from "../dummy/cbarmB8";
// import { DataBehavior9 } from "../dummy/cbarmB9";
// import { DataBehavior10 } from "../dummy/cbarmB10";
// import { DataBehavior11 } from "../dummy/cbarmB11";
// import { DataBehavior12 } from "../dummy/cbarmB12";
// import { DataBehavior13 } from "../dummy/cbarmB13";
// import { DataBehavior14 } from "../dummy/cbarmB14";
// import { DataBehavior15 } from "../dummy/cbarmB15";
// import { DataBehavior16 } from "../dummy/cbarmB16";
import { cbark1 } from "../dummy/cbark1";
import { cbark2 } from "../dummy/cbark2";
import { cbark3 } from "../dummy/cbark3";
import { cbark4 } from "../dummy/cbark4";
import { cbark5 } from "../dummy/cbark5";
import { cbark6 } from "../dummy/cbark6";
import { cbark7 } from "../dummy/cbark7";
import { cbark8 } from "../dummy/cbark8";
import { cbark9 } from "../dummy/cbark9";
import { cbark10 } from "../dummy/cbark10";
import { cbark11 } from "../dummy/cbark11";
import { cbark12 } from "../dummy/cbark12";
import { cbark13 } from "../dummy/cbark13";
import { cbark14 } from "../dummy/cbark14";
import { cbark15 } from "../dummy/cbark15";
import { cbark16 } from "../dummy/cbark16";
import { cbark17 } from "../dummy/cbark17";
import { cbark18 } from "../dummy/cbark18";
import { cbark19 } from "../dummy/cbark19";
import { cbark20 } from "../dummy/cbark20";
import { cbark21 } from "../dummy/cbark21";
import { cbark22 } from "../dummy/cbark22";
import { cbark23 } from "../dummy/cbark23";
import { cbark24 } from "../dummy/cbark24";
import { cbark25 } from "../dummy/cbark25";
import { cbark26 } from "../dummy/cbark26";
import { userInfo, teamInfo, leaderInfo } from "../dummy/employeeInfo";

export const getReportBehavior = (val) => {
  // if (DataBehavior1[val]) { return DataBehavior1[val]; }
  // if (DataBehavior2[val]) { return DataBehavior2[val]; }
  // if (DataBehavior3[val]) { return DataBehavior3[val]; }
  // if (DataBehavior4[val]) { return DataBehavior4[val]; }
  // if (DataBehavior5[val]) { return DataBehavior5[val]; }
  // if (DataBehavior6[val]) { return DataBehavior6[val]; }
  // if (DataBehavior7[val]) { return DataBehavior7[val]; }
  // if (DataBehavior8[val]) { return DataBehavior8[val]; }
  // if (DataBehavior9[val]) { return DataBehavior9[val]; }
  // if (DataBehavior10[val]) { return DataBehavior10[val]; }
  // if (DataBehavior11[val]) { return DataBehavior11[val]; }
  // if (DataBehavior12[val]) { return DataBehavior12[val]; }
  // if (DataBehavior13[val]) { return DataBehavior13[val]; }
  // if (DataBehavior14[val]) { return DataBehavior14[val]; }
  // if (DataBehavior15[val]) { return DataBehavior15[val]; }
  // if (DataBehavior16[val]) { return DataBehavior16[val]; }
  return cbaBehaviorInfo[val];
}

export const getReportCategory = (val) => {
  if (cbark1[val]) { return cbark1[val]; }
  if (cbark2[val]) { return cbark2[val]; }
  if (cbark3[val]) { return cbark3[val]; }
  if (cbark4[val]) { return cbark4[val]; }
  if (cbark5[val]) { return cbark5[val]; }
  if (cbark6[val]) { return cbark6[val]; }
  if (cbark7[val]) { return cbark7[val]; }
  if (cbark8[val]) { return cbark8[val]; }
  if (cbark9[val]) { return cbark9[val]; }
  if (cbark10[val]) { return cbark10[val]; }
  if (cbark11[val]) { return cbark11[val]; }
  if (cbark12[val]) { return cbark12[val]; }
  if (cbark13[val]) { return cbark13[val]; }
  if (cbark14[val]) { return cbark14[val]; }
  if (cbark15[val]) { return cbark15[val]; }
  if (cbark16[val]) { return cbark16[val]; }
  if (cbark17[val]) { return cbark17[val]; }
  if (cbark18[val]) { return cbark18[val]; }
  if (cbark19[val]) { return cbark19[val]; }
  if (cbark20[val]) { return cbark20[val]; }
  if (cbark21[val]) { return cbark21[val]; }
  if (cbark22[val]) { return cbark22[val]; }
  if (cbark23[val]) { return cbark23[val]; }
  if (cbark24[val]) { return cbark24[val]; }
  if (cbark25[val]) { return cbark25[val]; }
  if (cbark26[val]) { return cbark26[val]; }
  // cbaCategoryInfo[val]
};

export const getAtasan = (val) => leaderInfo[val];

export const isLeader = (val) => (teamInfo[val] ? true : false);
export const hasLeader = (val) => (leaderInfo[val] ? true : false);

export const getListTeam = (val) => {
  let result = [];
  let arr = [];
  let atasan = val;
  const isleader = isLeader(val);
  const hasleader = hasLeader(val);

  if (hasleader) {
    atasan = getAtasan(val);
    arr = arr.concat(teamInfo[atasan]);
    arr.unshift(atasan);
  }
  if (isleader) {
    arr = arr.concat(teamInfo[val]);;
  }

  arr.map((item) => {
    let infoUser = userInfo[item];
    let d = {
      nik: item,
      nama: infoUser.nama,
      band: infoUser.band,
      posisi: infoUser.posisi,
      label : (item === atasan) ? "Leader":null
    }
    if (item === val) {
      d.label = "It's You";
    }
    result.push(d);
    return true;
  });

  // console.log(arr);
  return result;
};

export const getSearchNik = (val) => {
  let result = [];
  let arr = [val];

  arr.map((item) => {
    let infoUser = userInfo[item];
    let d = {
      nik: item,
      nama: infoUser.nama,
      band: infoUser.band,
      posisi: infoUser.posisi,
      label : null
    }
    result.push(d);
    return true;
  });
  
  return result;
}
