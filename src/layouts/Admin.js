import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

// components
import Sidebar from "../components/Sidebar/Sidebar.js";
import HeaderStats from "../components/Headers/HeaderStats.js";
import FooterAdmin from "../components/Footers/FooterAdmin.js";

// views

import Report from "../views/admin/Report.js";
import ReportBehavior from "../views/admin/ReportBehavior.js";
import ReportCategory from "../views/admin/ReportCategory.js";
import Playbook from "../views/admin/Playbook.js";
import Faq from "../views/admin/Faq.js";

export default function Admin() {
  return (
    <>
      <Sidebar />
      <div className="flex flex-col justify-between md:ml-64 bg-slate-100 min-h-screen h-full">
        <HeaderStats />
        <div className="px-4 md:px-10 mx-auto w-full -mt-56 mb-auto">
          <div>
            <Switch>
              <Route path="/admin/report" exact component={Report} />
              <Route path="/admin/report/b/:nik" exact component={ReportBehavior} />
              <Route path="/admin/report/k/:nik" exact component={ReportCategory} />
              <Route path="/admin/playbook" exact component={Playbook} />
              <Route path="/admin/faq" exact component={Faq} />
              <Redirect from="/admin" to="/admin/report" />
            </Switch>
          </div>
        </div>
        <FooterAdmin/>
      </div>
    </>
  );
}
