import React, { useState } from "react";
import Card from "../../components/Cards/Card";
import { getListTeam, getSearchNik } from "../../utils/api/api";
import { useHistory } from "react-router-dom";

export default function Report() {
  const history = useHistory();
  const [logedUser] = useState(localStorage.getItem('logged_user') || "960170");
  const [inputs, setInputs] = useState({nik: ""});
  const [team, setTeam] = useState(getListTeam(logedUser));

  const toDetailReport = (nik, type) => {
    history.push(`/admin/report/${type}/${nik}`);
  };

  const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    
    setInputs(values => ({...values, [name]: value}));
    
    // console.log(value);
    if (name === "nik" && value === ""){
      setTeam(getListTeam(logedUser));
    } else if (name === "nik" && value.length === 6){
      // console.log("list team",getSearchNik(value));
      setTeam(getSearchNik(value));
    }
  }

  return (
    <>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-12 px-4">
          <Card>
            <div className="py-10 rounded">
              <div className="mb-0 px-4 py-3 border-0">
                <div className="flex flex-wrap items-center">
                  <div className="relative w-full px-4 max-w-full flex-grow flex-1">
                    <form>
                      <div className="grid grid-cols-12 gap-4">
                        <h3 className="col-span-10 font-semibold text-lg text-slate-700">
                          Data Users
                        </h3>
                        <input
                          type="number"
                          className="col-span-2 border-1 px-3 py-3 placeholder-slate-300 text-slate-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                          placeholder="Search NIK"
                          name="nik" 
                          value={inputs.nik || ""} 
                          onChange={handleChange}
                        />
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div className="block w-full overflow-x-auto">
                <table className="items-center w-full bg-transparent border-collapse">
                  <thead>
                    <tr>
                      <th
                        rowSpan="2"
                        className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100"
                      >No</th>
                      <th
                        rowSpan="2"
                        className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100"
                      >NIK</th>
                      <th
                        rowSpan="2"
                        className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100"
                      >Nama</th>
                      <th
                        rowSpan="2"
                        className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100"
                      >Band</th>
                      <th
                        rowSpan="2"
                        className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100"
                      >Posisi</th>
                      <th
                        colSpan="2"
                        className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-center bg-slate-50 text-slate-500 border-slate-100"
                      >Direct Feedback</th>
                    </tr>
                    <tr>
                      <th
                        className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100"
                      >Kompetensi Leadership</th>
                      <th
                        className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100"
                      >Behavior</th>
                    </tr>
                  </thead>
                  <tbody>
                    {team.map((user, index) => (
                      <tr key={user.nik}>
                        <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                          <p>{index + 1}</p>
                        </td>
                        <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                          {user.nik}
                        </td>
                        <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                          {user.nama}
                          <span className={"rounded bg-lime-200 p-2 ml-4 "+(user.label?"": "hidden")}>{user.label}</span>
                        </td>
                        <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                          {user.band}
                        </td>
                        <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                          {user.posisi}
                        </td>
                        <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                          <button
                            onClick={() => toDetailReport(user.nik, "k")}
                            className="button is-info is-small bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded"
                          >
                            See Detail
                          </button>
                        </td>
                        <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                          <button
                            onClick={() => toDetailReport(user.nik, "b")}
                            className="button is-info is-small bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded"
                          >
                            See Detail
                          </button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </Card>
        </div>
      </div>
    </>
  );
}
