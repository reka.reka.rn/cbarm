import React, { useState } from "react";
import Card from "../../components/Cards/Card";
// import { cbaCategoryInfo } from "../../utils/dummy/cbarmKategori";
import { getReportCategory } from "../../utils/api/api";
import { useParams, Link } from "react-router-dom";

export default function Report() {
  const { nik } = useParams();
  const [inputs, setInputs] = useState({nik: "", nikDisplay: nik});
  const [cba, setCba] = useState(getReportCategory(nik));

  const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    
    setInputs(values => ({...values, [name]: value}));
    
    if (name === "nik" && value === ""){
      setInputs(values => ({...values, nikDisplay: nik}));
      setCba(getReportCategory(nik));
    } else if (name === "nik" && value.length === 6){
      const listCba = getReportCategory(value);
      
      setInputs(values => ({...values, nikDisplay: value}));
      if (listCba){
        setCba(getReportCategory(value));
      }
    }
  }

  return (
    <>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-12 px-4">
          <Card>
            <div className="py-10 rounded">
              <div className="mb-0 px-4 py-3 border-0">
                <div className="flex flex-wrap items-center">
                  <div className="relative w-full px-4 max-w-full flex-grow flex-1">
                    <form>
                      <div className="grid grid-cols-12 gap-4">
                        <h3 className="col-span-1 font-semibold text-lg text-slate-700">
                          Search Users
                        </h3>
                        <input
                          type="number"
                          className="col-span-2 border-1 px-3 py-3 placeholder-slate-300 text-slate-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                          placeholder="Search NIK"
                          name="nik" 
                          value={inputs.nik || ""} 
                          onChange={handleChange}
                        />
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <div className="rounded mb-0 px-4 py-5 border-0">
              <div className="flex flex-wrap items-center">
                <div className="relative w-full px-4 max-w-full flex-grow flex-1">
                  <div className="grid grid-cols-12 gap-4">
                    <h3 className="font-semibold text-lg text-slate-700">
                      NAMA
                    </h3>
                    <label className="col-span-5">{cba.nama}</label>
                    <div className="col-span-6 flex">
                      <Link
                        type="button"
                        to="/admin/report"
                        className="button is-info is-small bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded ml-auto"
                      >
                        Back
                      </Link>
                    </div>
                  </div>
                  <div className="grid grid-cols-12 gap-4">
                    <h3 className="font-semibold text-lg text-slate-700">
                      NIK
                    </h3>
                    <label className="col-span-5">{inputs.nikDisplay}</label>
                  </div>

                  <div className="grid grid-cols-12 gap-4 mt-8">
                    <h3 className="col-span-12 font-bold text-indigo-400">
                      A. DIGITAL LEADERSHIP
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 mt-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      1.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Menunjukkan inisiatif untuk menganalisis informasi penting yang terkait dengan transformasi digital.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      2.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Membangun opini yang komprehensif dengan mempertimbangkan berbagai data dari penggunaan teknologi digital dalam proses pengambilan keputusan yang mendukung transformasi digital.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      3.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Mendapatkan sudut pandang yang menyeluruh untuk mendapatkan pendekatan terbaik dalam transformasi digital.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>

                  <div className="grid grid-cols-12 gap-4 mt-8">
                    <h3 className="col-span-12 font-bold text-indigo-400">
                      B. GLOBAL BUSINESS SAVVY
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 mt-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      4.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Menetapkan arah strategi bisnis dengan menggunakan pengetahuan dan situasi global.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      5.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Menumbuhkan budaya pemanfaatan ekosistem digital dalam bentuk kebijakan/standar.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      6.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Memunculkan inisiatif untuk mendukung pengembangan bisnis agar berdaya saing dan meningkatkan nilai tambah.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>

                  <div className="grid grid-cols-12 gap-4 mt-8">
                    <h3 className="col-span-12 font-bold text-indigo-400">
                      C. CUSTOMER FOCUS
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 mt-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      7.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Merespon kebutuhan maupun pain points pelanggan/user berupa langkah yang bersifat strategis.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      8.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Merespon perubahan kebutuhan pelanggan maupun harapan bisnis dengan mengusulkan tindakan perbaikan.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      9.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Memberikan arahan yang terkait dengan pencarian data akan perubahan pola pikir maupun peluang yang muncul dari pelanggan/user.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>

                  <div className="grid grid-cols-12 gap-4 mt-8">
                    <h3 className="col-span-12 font-bold text-indigo-400">
                      D. BUILDING STRATEGIC PARTNERSHIP
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 mt-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      10.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Membina relasi secara berkelanjutan dengan berbagai mitra strategis eksisting maupun potensial dalam mencapai tujuan organisasi.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      11.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Memadukan masukan dari berbagai pendapat pihak lain untuk mewujudkan kerja sama yang saling menguntungkan.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      12.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Menyesuaikan diri dalam berbagai situasi yang mendukung kerja sama strategis dengan pihak lain.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>

                  <div className="grid grid-cols-12 gap-4 mt-8">
                    <h3 className="col-span-12 font-bold text-indigo-400">
                      E. STRATEGIC ORIENTATION
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 mt-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      13.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Melakukan pemantauan dan memanfaatkan informasi mengenai kemampuan internal serta situasi eksternal dalam merumuskan tujuan strategis organisasi.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      14.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Memanfaatkan berbagai sumber daya untuk mencapai tujuan strategis.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      15.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Memanfaatkan data/informasi secara komprehensif, untuk mengenali maupun memitigasi dampak/risiko jangka panjang dari strategi organisasi.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>

                  <div className="grid grid-cols-12 gap-4 mt-8">
                    <h3 className="col-span-12 font-bold text-indigo-400">
                      F. DRIVING EXECUTION
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 mt-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      16.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Membangun cara-cara yang terintegrasi untuk mengelola berbagai sumber daya bagi pertumbuhan organisasi.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      17.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Mengevaluasi risiko berdasarkan data dan menyiapkan alternatif solusi dengan mempertimbangkan kebijakan yang relevan.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      18.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Memberikan panduan untuk implementasi tindakan yang ditujukan untuk mencapai tujuan organisasi, dengan mempertimbangkan prioritas, sumber daya, kapabilitas lainnya atau kemungkinan keberhasilannya
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>

                  <div className="grid grid-cols-12 gap-4 mt-8">
                    <h3 className="col-span-12 font-bold text-indigo-400">
                      G. DRIVING INNOVATION
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 mt-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      19.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Memberikan arahan untuk munculnya ide atau implementasi inovasi dengan mengalokasikan sumber daya serta terbuka terhadap proses perbaikan dalam implementasi ide/inovasi baru.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      20.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Memunculkan tindakan atau arahan untuk keluar dari zona nyaman, seperti mendorong orang lain mencari alternatif tindakan, menerapkan cara kerja baru, atau memberikan tantangan.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      21.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Menetapkan kebijakan untuk penerapan perilaku inovasi dengan memperhitungkan risiko agar dapat diterapkan di organisasi.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>

                  <div className="grid grid-cols-12 gap-4 mt-8">
                    <h3 className="col-span-12 font-bold text-indigo-400">
                      H. DEVELOPING ORGANIZATIONAL CAPABILITIES
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 mt-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      22.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Merancang lingkungan kerja (berupa sistem, struktur, kebijakan, program, dan lain-lain) untuk pengembangan talent yang relevan dengan tren bisnis.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      23.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Mengembangkan mekanisme (sistem, program, kebijakan, dan lain-lain) untuk mendukung produktivitas organisasi.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      24.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Mendorong munculnya peluang pembelajaran baru dengan memanfaatkan beragam sumber daya guna meningkatkan kapasitas dan kapabilitas organisasi.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>

                  <div className="grid grid-cols-12 gap-4 mt-8">
                    <h3 className="col-span-12 font-bold text-indigo-400">
                      I. LEADING CHANGE
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 mt-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      25.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Mengembangkan tindakan yang terarah untuk meminimalkan resistensi atau dampak negatif dari perubahan yang ditujukan untuk membagun transparansi dalam divisi/organisasi.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      26.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Menunjukkan diri sebagai role model yang dapat mendorong pihak lain untuk melakukan perubahan yang berdampak di tingkat organisasi.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      27.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Membangun sistem atau kebijakan untuk memberikan penghargaan bagi individu yang melakukan langkah perubahan untuk mendukung peningkatan employee engagement terhadap divisi/organisasi.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>

                  <div className="grid grid-cols-12 gap-4 mt-8">
                    <h3 className="col-span-12 font-bold text-indigo-400">
                      J. MANAGING DIVERSITY
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 mt-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      28.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Memunculkan inisiatif (program kerja, tindakan, usulan, dan lain-lain) untuk mendorong keberagaman dalam organisasi.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      29.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Memanfaatkan keberagaman dalam merumuskan strategi atau kebijakan untuk mencapai tujuan strategis organisasi.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      30.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Mengelola & mengantisipasi potensi masalah yang disebabkan oleh keragaman dalam organisasi.
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>

                  <div className="grid grid-cols-12 gap-4 mt-8 bg-indigo-400 text-white -mx-4 px-4 py-2">
                    <h3 className="col-span-9 font-bold">
                    OVERALL DIRECT FEEDBACK KOMPETENSI LEADERSHIP
                    </h3>
                    <h3 className="font-semibold text-center">
                    {cba.category[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                    {cba.category[0].feedback_unit}
                    </h3>
                  </div>

                </div>
              </div>
            </div>
          </Card>
        </div>
      </div>
    </>
  );
}
