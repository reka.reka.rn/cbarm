import React from "react";

import Card from "../../components/Cards/Card.js";

export default function Report() {
  return (
    <>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-12 px-4">
          <Card>
            <iframe className="w-full min-h-screen" title="playbook-embed" 
              src="https://drive.google.com/file/d/150JMHSfQy1IPkm2kMFTCXiBn5rk5ghbA/preview" 
              referrerpolicy="unsafe-url" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true" allowpaymentrequest=""
              allow="midi; geolocation; microphone; camera; display-capture; encrypted-media;" 
              sandbox="allow-modals allow-forms allow-scripts allow-same-origin allow-popups allow-top-navigation-by-user-activation allow-downloads" ></iframe>
          </Card>
        </div>
      </div>
    </>
  );
}
