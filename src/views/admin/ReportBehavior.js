import React, { useState } from "react";
import Card from "../../components/Cards/Card";
// import { cbaBehaviorInfo } from "../../utils/dummy/cbarmBehavior";
import { getReportBehavior } from "../../utils/api/api";
import { useParams, Link } from "react-router-dom";

export default function Report() {
  const { nik } = useParams();
  const [inputs, setInputs] = useState({nik: "", nikDisplay: nik});
  const [cba, setCba] = useState(getReportBehavior(nik));

  const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    
    setInputs(values => ({...values, [name]: value}));
    
    if (name === "nik" && value === ""){
      setInputs(values => ({...values, nikDisplay: nik}));
      setCba(getReportBehavior(nik));
    } else if (name === "nik" && value.length === 6){
      const listCba = getReportBehavior(value);
      
      setInputs(values => ({...values, nikDisplay: value}));
      if (listCba){
        setCba(getReportBehavior(value));
      }
    }
  }

  return (
    <>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-12 px-4">
          <Card>
            <div className="py-10 rounded">
              <div className="mb-0 px-4 py-3 border-0">
                <div className="flex flex-wrap items-center">
                  <div className="relative w-full px-4 max-w-full flex-grow flex-1">
                    <form>
                      <div className="grid grid-cols-12 gap-4">
                        <h3 className="col-span-1 font-semibold text-lg text-slate-700">
                          Search Users
                        </h3>
                        <input
                          type="number"
                          className="col-span-2 border-1 px-3 py-3 placeholder-slate-300 text-slate-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                          placeholder="Search NIK"
                          name="nik" 
                          value={inputs.nik || ""} 
                          onChange={handleChange}
                        />
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <div className="rounded mb-0 px-4 py-5 border-0">
              <div className="flex flex-wrap items-center">
                <div className="relative w-full px-4 max-w-full flex-grow flex-1">
                  <div className="grid grid-cols-12 gap-4">
                    <h3 className="font-semibold text-lg text-slate-700">
                      NAMA
                    </h3>
                    <label className="col-span-5">{cba.nama}</label>
                    <div className="col-span-6 flex">
                      <Link
                        type="button"
                        to="/admin/report"
                        className="button is-info is-small bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded ml-auto"
                      >
                        Back
                      </Link>
                    </div>
                  </div>
                  <div className="grid grid-cols-12 gap-4">
                    <h3 className="font-semibold text-lg text-slate-700">
                      NIK
                    </h3>
                    <label className="col-span-5">{inputs.nikDisplay}</label>
                  </div>

                  <div className="grid grid-cols-12 gap-4 mt-8">
                    <h3 className="col-span-12 font-bold text-indigo-400">
                      A. AMANAH
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 mt-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      1.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                      Sikap Assessee terhadap memenuhi janji dan komitmen
                    </h3>
                    <h3 className="font-semibold text-center">
                      {cba.behavior[0].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                      {cba.behavior[0].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      2.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                      Sikap Assessee terhadap tanggung jawab dalam mengambil keputusan dan bertindak
                    </h3>
                    <h3 className="font-semibold text-center">
                      {cba.behavior[1].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                      {cba.behavior[1].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      3.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                      Sikap Assessee terhadap nilai moral dan etika di lingkungan kerja, keluarga dan masyarakat
                    </h3>
                    <h3 className="font-semibold text-center">
                      {cba.behavior[2].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                      {cba.behavior[2].feedback_unit}
                    </h3>
                  </div>

                  <div className="grid grid-cols-12 gap-4 mt-8">
                    <h3 className="col-span-12 font-bold text-indigo-400">
                      B. KOMPETEN
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 mt-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      4.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                      Sikap Assessee dalam meningkatkan kompetensi
                    </h3>
                    <h3 className="font-semibold text-center">
                      {cba.behavior[3].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                      {cba.behavior[3].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      5.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                      Sikap Assessee dalam membantu orang lain dalam belajar
                    </h3>
                    <h3 className="font-semibold text-center">
                      {cba.behavior[4].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                      {cba.behavior[4].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      6.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                      Sikap Assessee dalam menyelesaikan tugas
                    </h3>
                    <h3 className="font-semibold text-center">
                      {cba.behavior[5].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                      {cba.behavior[5].feedback_unit}
                    </h3>
                  </div>

                  <div className="grid grid-cols-12 gap-4 mt-8">
                    <h3 className="col-span-12 font-bold text-indigo-400">
                      C. HARMONIS
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 mt-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      7.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Sikap Assessee dalam menghargai orang lain
                    </h3>
                    <h3 className="font-semibold text-center">
                      {cba.behavior[6].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                      {cba.behavior[6].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      8.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Sikap Assessee dalam menolong orang lain
                    </h3>
                    <h3 className="font-semibold text-center">
                      {cba.behavior[7].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                      {cba.behavior[7].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      9.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Sikap Assessee dalam membangun lingkungan kerja
                    </h3>
                    <h3 className="font-semibold text-center">
                      {cba.behavior[8].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                      {cba.behavior[8].feedback_unit}
                    </h3>
                  </div>

                  <div className="grid grid-cols-12 gap-4 mt-8">
                    <h3 className="col-span-12 font-bold text-indigo-400">
                      D. LOYAL
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 mt-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      10.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Sikap Assessee dalam menjaga nama baik saat menjalankan aktivitas kerja
                    </h3>
                    <h3 className="font-semibold text-center">
                      {cba.behavior[9].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                      {cba.behavior[9].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      11.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Sikap Assessee dalam mencapai tujuan yang lebih besar
                    </h3>
                    <h3 className="font-semibold text-center">
                      {cba.behavior[10].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                      {cba.behavior[10].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      12.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Sikap Assessee dalam menjalankan tugas dan tanggung jawab
                    </h3>
                    <h3 className="font-semibold text-center">
                      {cba.behavior[11].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                      {cba.behavior[11].feedback_unit}
                    </h3>
                  </div>

                  <div className="grid grid-cols-12 gap-4 mt-8">
                    <h3 className="col-span-12 font-bold text-indigo-400">
                      E. ADAPTIF
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 mt-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      13.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Sikap Assessee dalam menyesuaikan diri terhadap perubahan (bisnis, teknologi dan cara kerja)
                    </h3>
                    <h3 className="font-semibold text-center">
                      {cba.behavior[12].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                      {cba.behavior[12].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      14.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Sikap Assessee dalam melakukan perbaikan berkelanjutan
                    </h3>
                    <h3 className="font-semibold text-center">
                      {cba.behavior[13].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                      {cba.behavior[13].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      15.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Sikap Assessee dalam bertindak untuk menemukan cara-cara baru
                    </h3>
                    <h3 className="font-semibold text-center">
                      {cba.behavior[14].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                      {cba.behavior[14].feedback_unit}
                    </h3>
                  </div>

                  <div className="grid grid-cols-12 gap-4 mt-8">
                    <h3 className="col-span-12 font-bold text-indigo-400">
                      F. KOLABORATIF
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 mt-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      16.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Sikap Assessee dalam memberi kesempatan kepada orang lain untuk berkontribusi
                    </h3>
                    <h3 className="font-semibold text-center">
                      {cba.behavior[15].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                      {cba.behavior[15].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      17.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Sikap Assessee dalam membangun kerjasama
                    </h3>
                    <h3 className="font-semibold text-center">
                      {cba.behavior[16].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                      {cba.behavior[16].feedback_unit}
                    </h3>
                  </div>
                  <div className="grid grid-cols-12 gap-4 border-dashed border-b py-2 border-indigo-400">
                    <h3 className="text-right">
                      18.
                    </h3>
                    <h3 className="col-span-8 font-semibold">
                    Sikap Assessee dalam pemanfaatan berbagai sumber daya
                    </h3>
                    <h3 className="font-semibold text-center">
                      {cba.behavior[17].nilai_komparatif_unit.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                      {cba.behavior[17].feedback_unit}
                    </h3>
                  </div>

                  <div className="grid grid-cols-12 gap-4 mt-8 bg-indigo-400 text-white -mx-4 px-4 py-2">
                    <h3 className="col-span-9 font-bold">
                    OVERALL DIRECT FEEDBACK BEHAVIOR/ PERILAKU KERJA
                    </h3>
                    <h3 className="font-semibold text-center">
                      {cba.overall.toFixed(3)}
                    </h3>
                    <h3 className="col-span-2 font-semibold text-center">
                      {cba.feedback_overall}
                    </h3>
                  </div>

                </div>
              </div>
            </div>
          </Card>
        </div>
      </div>
    </>
  );
}
